const fs = require('fs')
const path = require('path')
const folder = '../public/templates'

fs.readdir(folder, (err, files) => {
    files.forEach(file => {
        let fileContent = fs.readFileSync(folder + '/' + file, "utf8"),
            frontendRegular = '',
            backendRegular = '',
            backendHost = '',
            frontendHost = '';

        if(/http:\/\/localhost:8081/.test(fileContent)){
            backendRegular = /http:\/\/localhost:8081/g;
            backendHost = 'https://api.works.4side.xyz';
        }

        if(/http:\/\/localhost:8000/.test(fileContent)){
            frontendRegular = /http:\/\/localhost:8000/g;
            frontendHost = 'https://client.works.4side.xyz';
        }

        if(/https:\/\/client\.works\.4side\.xyz/.test(fileContent)) {
            frontendRegular = /https:\/\/client\.works\.4side\.xyz/g;
            frontendHost = 'http://localhost:8880';
        }

        if(/https:\/\/api\.works\.4side\.xyz/.test(fileContent)) {
            backendRegular = /https:\/\/api\.works\.4side\.xyz/g;
            backendHost = 'http://localhost:8080';
        }

        if( frontendRegular && backendRegular && backendHost && frontendHost){

            let replacedContent = fileContent.replace(frontendRegular, frontendHost);

            replacedContent = replacedContent.replace(backendRegular, backendHost);

            fs.writeFile(folder + '/' + file, replacedContent, (err, written) => {
                if (!err) {
                    console.log('File ' + file + ' was modified');
                } else {
                    console.log('Fuck off!!!');
                }
            });
        }

    });
})
