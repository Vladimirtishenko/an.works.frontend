const fs = require('fs')
const path = require('path')
const direction = '../public/js/configuration'
const name = 'quiz.json'

const args = require('minimist')(process.argv.slice(2));

const content = {
	backend: args.b || 'http://localhost:8001',
	frontend: args.f || 'http://localhost:8080',
}

if (!fs.existsSync(direction)){
    fs.mkdirSync(direction);
}

fs.writeFile(path.join(__dirname, direction, name), JSON.stringify(content), (err) => {
    if (err) throw err;

    console.log("The file was succesfully saved!");
});
