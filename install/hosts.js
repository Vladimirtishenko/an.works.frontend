const fs = require('fs')
const path = require('path')
const direction = '../public/js/configuration'
const name = 'host.json'

const args = require('minimist')(process.argv.slice(2));

const content = {
	host: args.h || 'http://localhost',
	port: args.p || '',
	origin: (args.h || 'http://localhost') + (args.p ? (':' + args.p) : '')
}

if (!fs.existsSync(direction)){
    fs.mkdirSync(direction);
}

fs.writeFile(path.join(__dirname, direction, name), JSON.stringify(content), (err) => {
    if (err) throw err;

    console.log("The file was succesfully saved!");
});
