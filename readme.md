# Require

- NodeJS 8+
- MongoDb 4+

# Install

```
npm install
```

# Development install

```
cd ./install
node host.js -p "BACKEND_PORT" // This command create and save file to /public/js/configuration/host.json. Available params -h "BACKEND_HOST" -p "BACKEND_PORT". Just "-p" option is important to development mode, option "-h" fill automatically.

node quiz.js // This command create and save file to /public/js/configuration/quiz.json with development params.
```

# Production install

```
cd ./install
node host.js -p "BACKEND_PORT" -h "BACKEND_HOST"  // This command create and save file to /public/js/configuration/host.json. Available params -h "BACKEND_HOST" -p "BACKEND_PORT". Options "-p" and "-h" are important to fill.

node quiz.js -b "BACKEND_HOST" -f "FRONTEND_HOST" // This command create and save file to /public/js/configuration/quiz.json with development params. For production mode on the server for "-b" and "-f" values will be the similar. Both option are important!
```

# Launch Development

```
npm run dev
```

# Launch Production

```
npm run prod
```

# Templates. Change the hosts from an.works to localhost or vise versa

```
cd install
node registration.host.js
```
