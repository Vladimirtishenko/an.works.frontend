module.exports = {
  apps : [{
    name   : "frontend",
    script : "./app.js",
    watch : ['app.js', 'public']
  }]
}
