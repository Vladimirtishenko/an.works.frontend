(() => {

	window._$ = (cl, parent) => {
		let doc = parent || document,
        el = doc.querySelectorAll(cl);

	    if(el && el.length > 1){
	      return el;
	    } else if(el){
	      return el[0];
	    } else {
	      return null;
	    }
	}

	window.random = () => {
	    return new Date().getTime().toString() + Math.floor(Math.random()*1000000);
	}

	String.prototype.isJson = function() {
	    try {
	        JSON.parse(this);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}

	String.prototype.getUrlParameter = function(name) {
	    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	    let results = regex.exec(this);
	    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	};

	Array.prototype.clone = function() {
		return this.slice(0);
	};

	if (!Element.prototype.matches) {
	  Element.prototype.matches = Element.prototype.matchesSelector ||
	    Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector ||
	    Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector;
		}

	if (!Element.prototype.closest) {
		Element.prototype.closest = function(css) {
	      var node = this;

	      while (node) {
	        if (node.matches(css)) return node;
	        else node = node.parentElement;
	      }
	      return null;
	    };
	}

})()
