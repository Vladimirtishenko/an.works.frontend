import React from "react";
import {Link} from 'react-router-dom';

import Header from "../includes/header.jsx";
import Footer from "../includes/footerNew.jsx";

const Error = (props) => {
    return (
        <div className="fl fl--dir-col fl--justify-b h-100">
            <Header/>
                <div className="fl--1">
                    <div className="container height--100 fl overflow--h">
                        <div className="fl--self-c width--100 padding--yc-30">
                            <img src="../../img/cloud1.svg" alt="" className="cloud1"/>
                            <div className="relative">
                                <h2 className="font--500 font--30 font--color-secondary margin--b-25">
                                    Looks like something went wrong...
                                </h2>
                                <p className="font--14 font--color-secondary margin--b-45">
                                    We are working on a problem. It does not take much time:)
                                </p>
                                <img src="../../img/cloud2.svg" alt="" className="cloud2"/>
                                <div className="fl fl--dir-col relative margin--b-60">
                                    <Link
                                        className="link fl--self-st font--500 font--color-white bg--blue margin--b-10 font--14 padding--xc-15 padding--yc-10 font--center min-width--180-px" to={"/"}
                                    >
                                        Заберите меня домой
                                    </Link>
                                    <Link
                                        className="link fl--self-st font--500 font--color-white bg--blue font--14 padding--xc-15 padding--yc-10 font--center min-width--180-px" to={"/rating"}
                                    >
                                        Перейти к рейтингу
                                    </Link>
                                    <img src="../../img/cloud3.svg" alt="" className="cloud3"/>
                                </div>
                            </div>
                            <img src="../../img/dog.svg" alt="" className="dog"/>
                        </div>
                    </div>
                </div >
            <Footer />
        </div>
        
    )
}

export default Error;
