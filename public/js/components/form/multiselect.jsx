import React from 'react'
import uniqid from 'uniqid';
import Multiselect from 'react-widgets/lib/Multiselect';
import { string, array, bool } from 'prop-types'

import { ValidatorComponent } from '../../libraries/validation/index.js';

class MultiselectComponent extends ValidatorComponent {
    static propTypes = {
        name: string.isRequired,
        list: array.isRequired,
        addToValidateForm: bool,
        justState: bool
	}

    static defaultProps = {
        value: "",
        limit: 30,
        limitText: '',
        errorMessages: [],
        validators: [],
        validatorListener: () => {},

        addToValidateForm: true,
        isValid: true
	}

    constructor(props) {
        super(props);
        this.state = {
            ...props,
            ...{id: uniqid(this.props.name + '-')}
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props),
            { value: nextPropsValue, justState } = this.props,
            {value: nextStateValue} = prevState;

        if (!comparing) {

            let value ='';

            if(justState){
                value = nextPropsValue === nextStateValue ? this.state.value : nextStateValue;
            }else{
                value = nextPropsValue;
            }

            this.setState({
                ...this.state,
                ...this.props,
                value,
            });
        }
    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    onKeysUp(event){

        let value = event && event.target && event.target.value;

        if (value && this.state.onKeysUp) {
            this.state.onKeysUp(value);
        }
    }

    onChangeValue(value) {
        if (value) {
            if(value.length > this.props.limit){
                this.setState({
                    ...this.state,
                     limitError: true
                 });
                return;
            } 
            this.makeValid()
            this.setState({
                ...this.state,
                 value,
                 limitError: false
             });
        }

        this.props.onMultiSelectChange &&
            this.props.onMultiSelectChange(value);
    }

    errorText() {
        const { isValid, value } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }
    textLimit() {
        let {limitError} = this.state;
        const {limitText = '',limit} = this.props;
        if (!limitError) {
            return null;
        }
        return (
            <div className="validation__error-msg">
                {limitText.replace('${#}',limit)}
            </div>
        );
    }

    render(){

        let value = this.state.value && _.map(this.state.value, (item) => {return item && item.label || item}) || "",
            stringValue = value && value.join(","),
            {isValid} = this.state;

        return (
            <div className={this.state.wrapperClass}>
                {(this.state.label && (
                    <label className={this.state.labelClass}>
                        {this.state.label}
                    </label>
                )) ||
                    null}
                <div className={`fl fl--dir-col ${this.state.className} ${isValid ? '': ' multiselect--no-valid'}`}>
                        <Multiselect
                            dropUp
                            filter='contains'
                            data={this.state.list}
                            textField="name"
                            valueField="label"
                            id={this.state.id}
                            value={this.state.value || null}
                            onChange={::this.onChangeValue}
                            onKeyUp={::this.onKeysUp}
                            placeholder={this.state.placeholder}
                        />
                    {this.errorText()}
                    {this.textLimit()}
                </div>
                <input type="hidden" value={stringValue} name={this.state.name} />
                {this.state.children || null}
            </div>
        );
    }
}

export default MultiselectComponent;
