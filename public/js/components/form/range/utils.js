/**
 * Captialize a string
 * @ignore
 * @param {string} string
 * @return {string}
 */
export function captialize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Clamp a value between a min and max value
 * @ignore
 * @param {number} value
 * @param {number} min
 * @param {number} max
 * @return {number}
 */
export function clamp(value, min, max) {
  return Math.min(Math.max(value, min), max);
}


/**
 * Calculate the distance between pointA and pointB
 * @ignore
 * @param {Point} pointA
 * @param {Point} pointB
 * @return {number} Distance
 */
export function distanceTo(pointA, pointB) {
  const xDiff = (pointB.x - pointA.x) ** 2;
  const yDiff = (pointB.y - pointA.y) ** 2;

  return Math.sqrt(xDiff + yDiff);
}

/**
 * Check if a value is defined
 * @ignore
 * @param {*} value
 * @return {boolean}
 */
export function isDefined(value) {
  return value !== undefined && value !== null;
}

/**
 * Check if a value is a number
 * @ignore
 * @param {*} value
 * @return {boolean}
 */
export function isNumber(value) {
  return typeof value === 'number';
}

/**
 * Check if a value is an object
 * @ignore
 * @param {*} value
 * @return {boolean}
 */
export function isObject(value) {
  return value !== null && typeof value === 'object';
}

/**
 * Calculate the absolute difference between two numbers
 * @ignore
 * @param {number} numA
 * @param {number} numB
 * @return {number}
 */
export function length(numA, numB) {
  return Math.abs(numA - numB);
}
