import React from "react";
import InputRange from "./range.jsx";
// import { string, number } from 'prop-types'

class Range extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ...props };
    }

    labelFormater(value) {
        const {valueList} = this.state;

        if (valueList) {
            return valueList[value];
        } else {
            return value;
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    onChange(value) {

        const {onChange} = this.state;

        this.setState({ value });

        if(onChange){
            onChange(value)
        }

    }

    render() {

        const {
            wrapperClass,
            label,
            labelClass,
            inputWrapperClass,
            max,
            min,
            value,
            step,
            name,
            dots,
            dotsCount,
            onChange,
            reverse
        } = this.state;

        return (
            <div className={wrapperClass}>
                {(label && (
                    <label className={labelClass}>
                        {label}
                    </label>
                )) ||
                    null}
                <div className={inputWrapperClass}>
                    <InputRange
                        reverse={reverse}
                        formatLabel={value => `${::this.labelFormater(value)}`}
                        maxValue={max}
                        minValue={min}
                        value={value}
                        step={step}
                        name={name}
                        dots={dots}
                        dotsCount={dotsCount}
                        onChange={::this.onChange}
                    />
                </div>
            </div>
        );
    }
}

export default Range;
