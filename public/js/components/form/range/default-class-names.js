/**
 * Default CSS class names
 * @ignore
 * @type {InputRangeClassNames}
 */
const DEFAULT_CLASS_NAMES = {
  activeTrack: 'input-range__track input-range__track--active input-range__track--position',
  disabledInputRange: 'input-range input-range--disabled',
  inputRange: 'input-range',
  labelContainer: 'input-range__label-container',
  maxLabel: 'input-range__label input-range__label--max',
  minLabel: 'input-range__label input-range__label--min',
  slider: 'input-range__slider',
  sliderContainer: 'input-range__slider-container',
  track: 'input-range__track input-range__track--background',
  valueLabel: 'input-range__label input-range__label--value',
  labelStartPosition: 'input-range__label--position-start',
  labelMiddlePosition: 'input-range__label--position-middle',
  labelEndPosition: 'input-range__label--position-end'
};

export default DEFAULT_CLASS_NAMES;
