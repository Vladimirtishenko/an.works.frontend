import React from "react";
// import 'react-widgets/dist/css/react-widgets.css';
import DropdownList from "react-widgets/lib/DropdownList";
import { string, array } from "prop-types";

class DropDownListComponent extends React.Component {
    static propTypes = {
        name: string.isRequired,
        list: array.isRequired
    };

    static defaultProps = {
        value: ""
    };

    constructor(props) {
        super(props);
        this.state = { ...DropDownListComponent.defaultProps, ...props };
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    onChangeValue(value) {
        if (this.state.onChange) {
            this.state.onChange(value);
        }

        if (value && value.label) {
            this.setState({ value: value.label });
        }
    }

    render() {
        let value =
            this.state.list && Array.isArray(this.state.list)
                ? _.find(this.state.list, item => {
                      if (item.label == this.state.value) return item;
                  })
                : "";

        return (
            <div className={this.state.wrapperClass}>
                {(this.state.label && (
                    <label className={this.state.labelClass}>
                        {this.state.label}
                    </label>
                )) ||
                    null}
                <DropdownList
                    filter
                    data={this.state.list}
                    textField="name"
                    valueField="label"
                    allowCreate="onFilter"
                    defaultValue={value}
                    onChange={::this.onChangeValue}
                />
                <input
                    type="hidden"
                    name={this.state.name}
                    value={this.state.value}
                />
            </div>
        );
    }
}

DropDownListComponent.propTypes = {};

export default DropDownListComponent;
