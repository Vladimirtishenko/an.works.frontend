import React from 'react'
import uniqid from 'uniqid';
import { string } from 'prop-types'

class StaticItem extends React.Component {

	static propTypes = {
		label: string,
		labelClass: string,
		labelIcon: string,
		wrapperClass: string,
		staticClass: string,
		staticText: string,
	}
	
	static defaultProps = {

		label: '',
		labelClass: 'form__label',
        labelIcon: 'icon',
		wrapperClass: '',
        staticClass: '',
		staticText: '',
		skills: false

	}
	constructor(props){
		super(props);
		
		this.state = {
			...StaticItem.defaultProps,
			...this.props,
			...{id: uniqid(this.props.name + '-')}
		}
		
	}
	
	componentDidUpdate(prevProps){
		
		let comparing = _.isEqual(prevProps, this.props);
		
		if(!comparing) {
			this.setState({...this.state, ...this.props});
		}

	}
	
	render(){
		
		return (
			<div className={this.state.wrapperClass}>
                
				{this.state.label && <div className={this.state.labelClass} htmlFor={this.state.id}>
                    <span className={this.state.labelIcon}></span>
					<span className={this.state.labelTextSpan}>
						{this.state.label}
					</span>
                </div> || null}
				<div 
					className={this.state.staticClass} 
					id={this.state.id}>
					<span className={this.state.staticTextWrapper}>
						{this.state.staticText}
					</span>
				</div>
            </div>
		)
	}
}

export default StaticItem;
