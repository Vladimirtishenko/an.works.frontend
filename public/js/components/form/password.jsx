import React from 'react'
import uniqid from 'uniqid';
import { string, bool, array } from 'prop-types'
import { ValidatorComponent } from '../../libraries/validation/index.js';

class Password extends ValidatorComponent {

    static propTypes = {
        name: string.isRequired,
        required: bool,
        label: string,
        inputClass: string,
        labelClass: string,
        wrapperClass: string,
        inputWrapperClass: string,
        placeholder: string,
        value: string,
        type: string,
        errorMessages: array,
        validators: array,
        addToValidateForm: bool,
        iconAvailable: bool,
        step: string
    }

	static defaultProps = {
		required: false,
		label: '',
        type: 'password',
        inputClass: 'input',
        iconAvailable: false,
		labelClass: 'label',
		wrapperClass: '',
        value: '',
        errorMessages: [],
        validators: [],
        validatorListener: () => {},
        addToValidateForm: true,
        isValid: true
	}

	constructor(props){
		super(props);

		this.state = {
			...props,
			...{id: uniqid(props.name + '-')}
		}

    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    errorText() {
        const { isValid, value, errorClass, errorClassInput} = this.state;

        if (isValid) {
            return null;
        }
        return (
            <div className={`validation__error-msg ${errorClass}`}>
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    onChange(event){
        const { target: { value = '' } } = event;

        this.makeValid();

        this.setState({
            value: value
        });


    }
    showPassword(e) {
        this.setState({
            type: this.state.type === 'text' ? 'password' : 'text'
        });
    }

	render(){
        const {
            wrapperClass,
            labelClass,
            id,
            label,
            inputWrapperClass,
            value,
            inputClass,
            name,
            placeholder,
            type,
            iconAvailable,
            isValid
        } = this.state;

		return (
			<div className={wrapperClass}>
				<label
					className={labelClass}
					htmlFor={id}
				>
					{label}
				</label>
                <div className={inputWrapperClass}>
                <div className="relative">
                    <input
                        onChange={::this.onChange}
                        value={value}
                        className={`${inputClass} ${isValid ? '': 'input--no-valid'}`}
                        id={id}
                        name={name}
                        placeholder={placeholder}
                        type={type}
                    />
                {value && <span className={`icon icon--eye${type === 'text' ? '_hide' : ''} pointer-text icon--input`} onClick={::this.showPassword}></span> || null}
                </div>
                {this.errorText()}
                </div>
			</div>
		)
	}
}

export default Password;
