import React from 'react'
// import uniqid from 'uniqid';
import { string } from 'prop-types'

class StaticSocial extends React.Component {

	static propTypes = {
		label: string,
		labelClass: string,
		wrapperClass: string,
		staticClass: string,
		staticText: string,
		additionalText: string,
		additional: string
	}
	
	static defaultProps = {

		label: '',
		labelClass: 'form__label',
		wrapperClass: '',
        staticClass: '',
		staticText: '',
		additional: 'font--12 font--color-secondary nowrap--all'

	}
	constructor(props){
		super(props);
		
		this.state = {
			...StaticSocial.defaultProps,
			...this.props
		}
		
	}
	
	componentDidUpdate(prevProps, prevState) {
		let comparingProps = _.isEqual(prevProps, this.props);
		if (!comparingProps) {
			this.setState({
					...this.state,
					...this.props
				});
		}
	}

	render(){
		
		return (
			<div className={this.state.wrapperClass}>

                <div className={this.state.labelClass}>
					<span className={this.state.labelTextSpan}>
						{this.state.label}
					</span>
                </div>

				<div 
					className={this.state.staticClass} 
					id={this.state.id}>
					<span className={this.state.staticTextWrapper}>
                        {this.state.staticText}
					</span>
                    {this.state.additionalText && <span className={this.state.additional}> {this.state.additionalText} </span>}
				</div>

            </div>
		)
	}
}

export default StaticSocial;
