import React from 'react';
import { string, bool } from 'prop-types'
import uniqid from 'uniqid';

import { ValidatorComponent } from '../../libraries/validation/index.js';

class Textarea extends ValidatorComponent {

    static propTypes = {
        name: string.isRequired,
        classes: string,
        label: string,
        value: string,
        labelClass: string,
        textAreaWrapper: string,
        textAreaClass: string,
        placeholder: string,
        addToValidateForm: bool
    }

    static defaultProps = {

		classes: '',
        labelClass: '',
        textAreaWrapper: '',
        textAreaClass: '',
        rows:'10',
		name: '',
        label: '',

        placeholder: '',
        isValid: true,
        value: '',

        errorMessages: [],
        validators: [],
        validatorListener: () => {},

        addToValidateForm: true,

        readonly: false

	}
	constructor(props){
		super(props);

		this.state = {
			...Textarea.defaultProps,
			...this.props,
            ...{id: uniqid(this.props.name + '-')}
		}

	}

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    errorText() {
        const { isValid, value } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    onChange(event){

        let value = event && event.target && event.target.value || '';

        this.makeValid()

        this.setState({
            value: value
        })

    }

    render(){

        let {name,
            label,
            value,
            placeholder,
            classes,
            labelClass,
            textAreaWrapper,
            textAreaClass,
            readonly,
            maxlength,
            wrapperClass,
            rows,
            isValid
        } = this.state,
            readOnly = readonly ? {readOnly: 'readonly'} : {};

        return (
            <div className={wrapperClass}>
              <div className={classes}>
                  <label className={labelClass}>{label}</label>
                  <div className={textAreaWrapper}>
                    <textarea
                        id={this.state.id}
                        onChange={::this.onChange}
                        rows={rows}
                        className={`${textAreaClass} ${isValid ? '': 'textarea--no-valid'}`}
                        value={value}
                        name={name}
                        placeholder={placeholder}
                        maxLength={maxlength}
                        {...readonly}>
                    </textarea>
                    {this.errorText()}
                  </div>
                  {this.state.children || null}
              </div>
              {/* <div className="clear"></div> */}
            </div>
        )

    }

}

export default Textarea;
