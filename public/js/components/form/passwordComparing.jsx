import React from 'react'
import uniqid from 'uniqid';
import { string, bool, array, oneOfType, func } from 'prop-types'
import { ValidatorComponent } from '../../libraries/validation/index.js';

class Password extends ValidatorComponent {

    static propTypes = {
        value: string,
        errorMessages: oneOfType([array]),
        validators: oneOfType([array]),
        validatorListener: func,
        addToValidateForm: bool,
        list: oneOfType([array]).isRequired
    }

	static defaultProps = {
        value: '',
        iconAvailable: false,
        errorMessages: [],
        validators: [],
        validatorListener: () => {},
        addToValidateForm: true,
        isValid: true
	}

	constructor(props){
		super(props);

		this.state = {
			...props,
			...{id: uniqid('comparing-')},
            temporary: {}
		}

    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    errorText() {
        const { isValid, value, errorClass, errorClassInput} = this.state;

        if (isValid) {
            return null;
        }
        return (
            <div className={`validation__error-msg ${errorClass}`}>
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    onChange(event){

        const { target: { value = '', name } } = event,
              { temporary, value: stateValue, list } = this.state;

        temporary[name] = value;

        const newList =  _.map(list, (item) => {
            if(name == item.name){
                return {...item, value: value}
            }
            return item;
        })

        this.makeValid();

        this.setState((prevState) => ({
            ...prevState,
            temporary: temporary,
            value: JSON.stringify(temporary),
            list: newList
        }));


    }

    showPassword(name) {

        const { list } = this.state,
                newList = _.map(list, (item) => {
                    if(name == item.name){
                        return {...item, type: item.type === 'text' ? 'password' : 'text'}
                    }
                    return item;
                })
        this.setState((prevState) => ({
            ...prevState,
            list: newList
        }));
    }

	render(){

        const {
            wrapperClass,
            labelClass,
            inputWrapperClass,
            inputClass,
            validators,
            errorMessages,
            list,
            isValid,
            wrapbtnClass
        } = this.state;
        
		return (
            <React.Fragment>
                {
                    list.length && list.map((item, i) => {
                        return (
                            <div key={i} className={wrapperClass}>
                				<label
                					className={labelClass}
                					htmlFor={item.name} 
                				>
                					{item.label}
                				</label>
                                <div className={inputWrapperClass}>
                                    <div className="relative">
                                        <input
                                            type={item.type}
                                            onChange={::this.onChange}
                                            className={`${inputClass} ${isValid ? '': 'input--no-valid'}`}
                                            id={item.name}
                                            name={item.name}
                                            placeholder={item.placeholder}
                                        />
                                        {item.value &&<span className={`icon icon--eye${item.type === 'text' ? '_hide' : ''} pointer-text icon--input`} onClick={() => {this.showPassword(item.name)}}></span> || null}
                                    </div>
                                    {item.name == 'repeat-password' && this.errorText() || null}
                                </div>
                			</div>
                        )
                    })
                }
            </React.Fragment>
		)
	}
}

export default Password;
