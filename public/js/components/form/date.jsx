import React from 'react'
import uniqid from 'uniqid'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker-bem';

import 'react-datepicker-bem/css/react-datetime-bem.css'

class Dates extends React.Component {

	static defaultProps = {
		required: false,

		label: '',

		inputClass: 'input',
		labelClass: 'label',
		wrapperClass: ''
	}

	constructor(props){
		super(props);

		this.state = {
				...Dates.defaultProps,
				...this.props,
				...{id: uniqid(this.props.name + '-')}
		}

	}

	render(){

		let date = this.state.value ? {value: new Date(this.state.value)} : {}

		return (
			<div className={this.state.wrapperClass}>
				<label
					className={this.state.labelClass}
					htmlFor={this.state.id}
				>
					{this.state.label}
				</label>
				<DatePicker
					timeFormat={this.state.timeFormat}
					inputProps={{name: this.state.name}}
					closeOnSelect={true}
					{...date}
				/>
			</div>
		)
	}
}

Dates.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  required: PropTypes.bool,
  label: PropTypes.string,
  inputClass: PropTypes.string,
  labelClass: PropTypes.string,
  wrapperClass: PropTypes.string,
  value: PropTypes.string
};

export default Dates;
