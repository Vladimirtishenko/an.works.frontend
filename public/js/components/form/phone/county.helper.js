// Country model:
// [
//    Country name,
//    Regions,
//    iso2 code,
//    International dial code,
//    Format (if available),
//    Order (if >1 country with same dial code),
//    Area codes (if >1 country with same dial code)
// ]
//
// Regions:
// ['america', 'europe', 'asia', 'oceania', 'africa']
//
// Sub-regions:
// ['north-america', 'south-america', 'central-america', 'carribean',
//  'european-union', 'ex-ussr', 'middle-east', 'north-africa']

import rawAllCountries from '../../../databases/general/countryNumberData.json'

let allCountryCodes = {};

function addCountryCode(iso2, dialCode, priority) {
  if (!(dialCode in allCountryCodes)) {
    allCountryCodes[dialCode] = [];
  }
  const index = priority || 0;
  allCountryCodes[dialCode][index] = iso2;
};

const allCountries = [].concat(...rawAllCountries.map((country) => {
  const countryItem = {
    name: country[0],
    regions: country[1],
    iso2: country[2],
    dialCode: country[3],
    format: country[4] || undefined,
    priority: country[5] || 0,
    hasAreaCodes: country[6] ? true : false,
  };

  const areaItems = [];

  country[6] && country[6].map((areaCode) => {
    const areaItem = {...countryItem};
    areaItem.regions = country[1];
    areaItem.dialCode = country[3] + areaCode;
    areaItem.isAreaCode = true;

    areaItems.push(areaItem);

    addCountryCode(country[2], areaItem.dialCode);
  });

  addCountryCode(
    countryItem.iso2,
    countryItem.dialCode,
    countryItem.hasAreaCodes
  );

  return (areaItems.length > 0) ? [countryItem, ...areaItems] : [countryItem];
}));

module.exports = {
  allCountries: allCountries,
  allCountryCodes: allCountryCodes
};
