import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import countryData from './county.helper.js';
import uniqid from 'uniqid';

import { ValidatorComponent } from '../../../libraries/validation/index.js';

class PhoneInput extends ValidatorComponent {

  static propTypes = {
    excludeCountries: PropTypes.arrayOf(PropTypes.string),
    onlyCountries: PropTypes.arrayOf(PropTypes.string),
    preferredCountries: PropTypes.arrayOf(PropTypes.string),
    defaultCountry: PropTypes.string,

    value: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,

    containerStyle: PropTypes.object,
    inputStyle: PropTypes.object,
    buttonStyle: PropTypes.object,
    dropdownStyle: PropTypes.object,

    containerClass: PropTypes.string,
    inputClass: PropTypes.string,
    buttonClass: PropTypes.string,
    dropdownClass: PropTypes.string,
    searchClass: PropTypes.string,

    autoFormat: PropTypes.bool,
    disableAreaCodes: PropTypes.bool,
    disableCountryCode: PropTypes.bool,
    disableDropdown: PropTypes.bool,
    enableLongNumbers: PropTypes.bool,
    countryCodeEditable: PropTypes.bool,
    enableSearchField: PropTypes.bool,

    regions: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.string)
    ]),

    inputExtraProps: PropTypes.object,
    localization: PropTypes.object,

    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onClick: PropTypes.func,
    onKeyDown: PropTypes.func,
    isValid: PropTypes.func,

    errorMessages: PropTypes.array,
    validators: PropTypes.array
  }

  static defaultProps = {
    excludeCountries: [],
    onlyCountries: [],
    preferredCountries: [],
    defaultCountry: '',

    value: '',
    placeholder: '+380 (**) ***-****',
    disabled: false,

    containerStyle: {},
    inputStyle: {},
    buttonStyle: {},
    dropdownStyle: {},

    containerClass: 'tel-input',
    inputClass: '',
    buttonClass: '',
    dropdownClass: '',
    searchClass: '',

    autoFormat: true,
    disableAreaCodes: false,
    isValid: (inputNumber) => {
      return _.some(countryData.allCountries, (country) => {
        return _.startsWith(inputNumber, country.dialCode) || _.startsWith(country.dialCode, inputNumber);
      });
    },
    disableCountryCode: false,
    disableDropdown: false,
    enableLongNumbers: false,
    countryCodeEditable: true,
    enableSearchField: false,

    regions: '',

    inputExtraProps: {},
    localization: {},

    onEnterKeyPress: () => {},

    isModernBrowser: document.createElement ? (
      Boolean(document.createElement('input').setSelectionRange)
    ) : false,

    keys: {
      UP: 38, DOWN: 40, RIGHT: 39, LEFT: 37, ENTER: 13,
      ESC: 27, PLUS: 43, A: 65, Z: 90, SPACE: 32
    },

      errorMessages: [],
      validators: [],
      validatorListener: () => {}

  }

  constructor(props) {
    super(props);
    let filteredCountries = countryData.allCountries;

    if (props.disableAreaCodes) filteredCountries = this.deleteAreaCodes(filteredCountries);
    if (props.regions) filteredCountries = this.filterRegions(props.regions, filteredCountries);

    const onlyCountries = this.excludeCountries(
      this.getOnlyCountries(props.onlyCountries, filteredCountries), props.excludeCountries);

    const preferredCountries = _.filter(filteredCountries, (country) => {
      return _.some(props.preferredCountries, (preferredCountry) => {
        return preferredCountry === country.iso2;
      });
    });

    const inputNumber = props.value || '';

    let countryGuess;
    if (inputNumber.length > 1) {
      countryGuess = this.guessSelectedCountry(inputNumber.substring(1, 6), onlyCountries, props.defaultCountry) || 0;
    } else if (props.defaultCountry) {
      countryGuess = _.find(onlyCountries, {iso2: props.defaultCountry}) || 0;
    } else {
      countryGuess = 0;
    }

    const countryGuessIndex = _.findIndex(this.allCountries, countryGuess);
    const dialCode = (
      inputNumber.length < 2 &&
      countryGuess &&
      !_.startsWith(inputNumber.replace(/\D/g, ''), countryGuess.dialCode)
    ) ? countryGuess.dialCode : '';

    let value = (inputNumber === '' && countryGuess === 0) ? '' :
        this.formatNumber(
          (props.disableCountryCode ? '' : dialCode) + inputNumber.replace(/\D/g, ''),
          countryGuess.name ? countryGuess.format : undefined
        );

    this.state = {
        ...{id: uniqid(props.name + '-')},
        ...PhoneInput.defaultProps,
        ...props,
        value: value,
        onlyCountries,
        preferredCountries,
        selectedCountry: countryGuess,
        highlightCountryIndex: countryGuessIndex,
        queryString: '',
        showDropdown: false,
        freezeSelection: false,
        debouncedQueryStingSearcher: _.debounce(this.searchCountry, 250),
        searchValue: '',
    };
  }

  componentDidMount() {
    if (document.addEventListener) {
      document.addEventListener('mousedown', this.handleClickOutside);
      document.addEventListener('keydown', this.handleKeydown);
    }
    this.attach(this);
  }
  componentDidUpdate(prevProps){
    let comparingProps = _.isEqual(prevProps, this.props);

    if (!comparingProps) {

        this.setState({ ...this.state, ...this.props });
        
    }
  }

  componentWillUnmount() {
    if (document.removeEventListener) {
      document.removeEventListener('mousedown', this.handleClickOutside);
      document.removeEventListener('keydown', this.handleKeydown);
    }
    this.detach(this);
  }

  deleteAreaCodes = (filteredCountries) => {
    return filteredCountries._.filter((country) => {
      return country.isAreaCode !== true;
    });
  }

  filterRegions = (regions, filteredCountries) => {
    if (typeof regions === 'string') {
      const region = regions;
      return filteredCountries._.filter((country) => {
        return country.regions._.some((element) => {
          return element === region;
        });
      });
    }

    return filteredCountries._.filter((country) => {
      const matches = regions._.map((region) => {
        return country.regions._.some((element) => {
          return element === region;
        });
      });
      return matches._.some(el => el);
    });
  }

  getOnlyCountries = (onlyCountriesArray, filteredCountries) => {
    if (onlyCountriesArray.length === 0) return filteredCountries;

    return filteredCountries._.filter((country) => {
      return onlyCountriesArray._.some((element) => {
        return element === country.iso2;
      });
    });
  }

  excludeCountries = (selectedCountries, excludedCountries) => {
    if (excludedCountries.length === 0) {
      return selectedCountries;
    } else {
      return _.filter(selectedCountries, (selCountry) => {
        return !_.includes(excludedCountries, selCountry.iso2);
      });
    }
  }

  getProbableCandidate = _.memoize((queryString) => {
    if (!queryString || queryString.length === 0) {
      return null;
    }
    const probableCountries = _.filter(this.state.onlyCountries, (country) => {
      return _.startsWith(country.name.toLowerCase(), queryString.toLowerCase());
    }, this);
    return probableCountries[0];
  });

  guessSelectedCountry = _.memoize((inputNumber, onlyCountries, defaultCountry) => {
    const secondBestGuess = _.find(onlyCountries, {iso2: defaultCountry}) || {};
    if (_.trim(inputNumber) === '') return secondBestGuess;

    const bestGuess = _.reduce(onlyCountries, (selectedCountry, country) => {
      if (_.startsWith(inputNumber, country.dialCode)) {
        if (country.dialCode.length > selectedCountry.dialCode.length) {
          return country;
        }
        if (country.dialCode.length === selectedCountry.dialCode.length && country.priority < selectedCountry.priority) {
          return country;
        }
      }
      return selectedCountry;
    }, {dialCode: '', priority: 10001}, this);

    if (!bestGuess.name) return secondBestGuess;
    return bestGuess;
  });

  updateDefaultCountry = (country) => {
    const newSelectedCountry = _.find(this.state.onlyCountries, {iso2: country});
    this.setState({
      defaultCountry: country,
      selectedCountry: newSelectedCountry,
      formattedNumber: this.props.disableCountryCode ? '' : '+' + newSelectedCountry.dialCode
    });
  }

  updateFormattedNumber(number) {
    const { onlyCountries, defaultCountry } = this.state;
    let countryGuess;
    let inputNumber = number;
    let formattedNumber = number;

    if (!inputNumber.startsWith('+')) {
      countryGuess = _.find(onlyCountries, {iso2: defaultCountry});
      const dialCode = countryGuess && !_.startsWith(inputNumber.replace(/\D/g, ''), countryGuess.dialCode) ? countryGuess.dialCode : '';
      formattedNumber = this.formatNumber(
        (this.props.disableCountryCode ? '' : dialCode) + inputNumber.replace(/\D/g, ''),
        countryGuess ? countryGuess.format : undefined
      );
    }
    else {
      inputNumber = inputNumber.replace(/\D/g, '');
      countryGuess = this.guessSelectedCountry(inputNumber.substring(0, 6), onlyCountries, defaultCountry);
      formattedNumber = this.formatNumber(inputNumber, countryGuess.format);
    }

    this.setState({ selectedCountry: countryGuess, formattedNumber });
  }

  scrollTo = (country, middle) => {
    if (!country)
      return;

    const container = this.dropdownRef;

    if (!container || !document.body)
      return;

    const containerHeight = container.offsetHeight;
    const containerOffset = container.getBoundingClientRect();
    const containerTop = containerOffset.top + document.body.scrollTop;
    const containerBottom = containerTop + containerHeight;

    const element = country;
    const elementOffset = element.getBoundingClientRect();

    const elementHeight = element.offsetHeight;
    const elementTop = elementOffset.top + document.body.scrollTop;
    const elementBottom = elementTop + elementHeight;

    let newScrollTop = elementTop - containerTop + container.scrollTop;
    const middleOffset = (containerHeight / 2) - (elementHeight / 2);

    if (elementTop < containerTop) {
      // scroll up
      if (middle) {
        newScrollTop -= middleOffset;
      }
      container.scrollTop = newScrollTop;
    }
    else if (elementBottom > containerBottom) {
      // scroll down
      if (middle) {
        newScrollTop += middleOffset;
      }
      const heightDifference = containerHeight - elementHeight;
      container.scrollTop = newScrollTop - heightDifference;
    }
  }

  formatNumber = (text, patternArg) => {
    const { disableCountryCode, enableLongNumbers, autoFormat } = this.props;

    let pattern;
    if (disableCountryCode && patternArg) {
      pattern = patternArg.split(' ');
      pattern.shift();
      pattern = pattern.join(' ');
    } else {
      pattern = patternArg;
    }

    if (!text || text.length === 0) {
      return disableCountryCode ? '' : '+';
    }

    if ((text && text.length < 2) || !pattern || !autoFormat) {
      return disableCountryCode ? text : `+${text}`;
    }

    const formattedObject = _.reduce(pattern, (acc, character) => {
      if (acc.remainingText.length === 0) {
        return acc;
      }

      if (character !== '.') {
        return {
          formattedText: acc.formattedText + character,
          remainingText: acc.remainingText
        };
      }

      return {
        formattedText: acc.formattedText + _.head(acc.remainingText),
        remainingText: _.tail(acc.remainingText)
      };
    }, {
      formattedText: '',
      remainingText: text.split('')
    });

    let formattedNumber;
    if (enableLongNumbers) {
      formattedNumber = formattedObject.formattedText + formattedObject.remainingText.join('');
    } else {
      formattedNumber = formattedObject.formattedText;
    }

    if (formattedNumber.includes('(') && !formattedNumber.includes(')')) formattedNumber += ')';
    return formattedNumber;
  }

  cursorToEnd = () => {
    const input = this.numberInputRef;
    input.focus();
    if (this.props.isModernBrowser) {
      const len = input.value.length;
      input.setSelectionRange(len, len);
    }
  }

  getElement = (index) => {
    return this[`flag_no_${index}`];
  }

  getCountryData = () => {
    if (!this.state.selectedCountry) return {}
    return {
      name: this.state.selectedCountry.name || '',
      dialCode: this.state.selectedCountry.dialCode || '',
      countryCode: this.state.selectedCountry.iso2 || ''
    }
  }

  handleFlagDropdownClick = () => {
    if (!this.state.showDropdown && this.props.disabled) return;

    if (this.state.preferredCountries.includes(this.state.selectedCountry)) {
      this.setState({
        showDropdown: !this.state.showDropdown,
        highlightCountryIndex: _.findIndex(this.state.preferredCountries, this.state.selectedCountry)
      }, () => {
        if (this.state.showDropdown) {
          this.scrollTo(this.getElement(this.state.highlightCountryIndex));
        }
      });
    }
    else {
      this.setState({
        showDropdown: !this.state.showDropdown,
        highlightCountryIndex: _.findIndex(this.state.onlyCountries, this.state.selectedCountry)
      }, () => {
        if (this.state.showDropdown) {
          this.scrollTo(this.getElement(this.state.highlightCountryIndex + this.state.preferredCountries.length));
        }
      });
    }
  }

  handleInput = (e) => {
    let formattedNumber = this.props.disableCountryCode ? '' : '+';
    let newSelectedCountry = this.state.selectedCountry;
    let freezeSelection = this.state.freezeSelection;

    if(!this.props.countryCodeEditable) {
        const updatedInput = '+' + newSelectedCountry.dialCode;
        if (e.target.value.length < updatedInput.length) {
            return;
        }
    }

    if (e.target.value.replace(/\D/g, '').length > 15) {
      return;
    }

    if (e.target.value === this.state.value) {
      return;
    }

    // ie hack
    if (e.preventDefault) {
      e.preventDefault();
    } else {
      e.returnValue = false;
    }

    if (e.target.value.length > 0) {
      const inputNumber = e.target.value.replace(/\D/g, '');

      if (!this.state.freezeSelection || this.state.selectedCountry.dialCode.length > inputNumber.length) {
        newSelectedCountry = this.guessSelectedCountry(inputNumber.substring(0, 6), this.state.onlyCountries, this.state.defaultCountry);
        freezeSelection = false;
      }
      formattedNumber = this.formatNumber(inputNumber, newSelectedCountry.format);
    }

    let caretPosition = e.target.selectionStart;
    const oldFormattedText = this.state.value;
    const diff = formattedNumber.length - oldFormattedText.length;

    this.makeValid();

    this.setState({
      value: formattedNumber,
      freezeSelection: freezeSelection,
      selectedCountry: newSelectedCountry.dialCode
        ? newSelectedCountry
        : this.state.selectedCountry
    }, () => {
      if (this.props.isModernBrowser) {
        if (diff > 0) {
          caretPosition = caretPosition - diff;
        }

        const lastChar = formattedNumber.charAt(formattedNumber.length - 1);

        if (lastChar == ')') {
          this.numberInputRef.setSelectionRange(formattedNumber.length - 1, formattedNumber.length - 1);
        }
        else if (caretPosition > 0 && oldFormattedText.length >= formattedNumber.length) {
          this.numberInputRef.setSelectionRange(caretPosition, caretPosition);
        }
      }

      if (this.props.onChange) {
        this.props.onChange(this.state.formattedNumber, this.getCountryData());
      }
    });
  }

  handleInputClick = (e) => {
    this.setState({ showDropdown: false });
    if (this.props.onClick) this.props.onClick(e, this.getCountryData());
  }

  handleFlagItemClick = (country) => {
    const currentSelectedCountry = this.state.selectedCountry;
    const nextSelectedCountry = _.find(this.state.onlyCountries, country);

    const unformattedNumber = this.state.value.replace(' ', '').replace('(', '').replace(')', '').replace('-', '');
    const newNumber = unformattedNumber.length > 1 ? unformattedNumber.replace(currentSelectedCountry.dialCode, nextSelectedCountry.dialCode) : nextSelectedCountry.dialCode;
    const formattedNumber = this.formatNumber(newNumber.replace(/\D/g, ''), nextSelectedCountry.format);

    this.setState({
      showDropdown: false,
      selectedCountry: nextSelectedCountry,
      freezeSelection: true,
      value: formattedNumber
    }, () => {
      this.cursorToEnd();
      if (this.props.onChange) {
        this.props.onChange(formattedNumber, this.getCountryData());
      }
    });
  }

  handleInputFocus = (e) => {
    // if the input is blank, insert dial code of the selected country
    if (this.numberInputRef) {
      if (this.numberInputRef.value === '+' && this.state.selectedCountry && !this.props.disableCountryCode) {
        this.setState({
          value: '+' + this.state.selectedCountry.dialCode
        }, () => setTimeout(this.cursorToEnd, 10));
      }
    }

    this.setState({ placeholder: '' });

    this.props.onFocus && this.props.onFocus(e, this.getCountryData());
    setTimeout(this.cursorToEnd, 10);
  }

  handleInputBlur = (e) => {
    if (!e.target.value) this.setState({ placeholder: this.props.placeholder });
    this.props.onBlur && this.props.onBlur(e, this.getCountryData());
  }

  getHighlightCountryIndex = (direction) => {
    const highlightCountryIndex = this.state.highlightCountryIndex + direction;

    if (highlightCountryIndex < 0 || highlightCountryIndex >= (this.state.onlyCountries.length + this.state.preferredCountries.length)) {
      return highlightCountryIndex - direction;
    }

    return highlightCountryIndex;
  }

  searchCountry = () => {
    const probableCandidate = this.getProbableCandidate(this.state.queryString) || this.state.onlyCountries[0];
    const probableCandidateIndex = _.findIndex(this.state.onlyCountries, probableCandidate) + this.state.preferredCountries.length;

    this.scrollTo(this.getElement(probableCandidateIndex), true);

    this.setState({queryString: '', highlightCountryIndex: probableCandidateIndex});
  }

  handleKeydown = (e) => {
    const { keys } = this.props;
    if (!this.state.showDropdown || this.props.disabled) return;
    const { target: { id } } = e;
    if (id === 'search-box') return;

    if (e.preventDefault) {
      e.preventDefault();
    } else {
      e.returnValue = false;
    }

    const moveHighlight = (direction) => {
      this.setState({
        highlightCountryIndex: this.getHighlightCountryIndex(direction)
      }, () => {
        this.scrollTo(this.getElement(
          this.state.highlightCountryIndex + this.state.preferredCountries.length
        ), true);
      });
    }

    switch (e.which) {
      case keys.DOWN:
        moveHighlight(1);
        break;
      case keys.UP:
        moveHighlight(-1);
        break;
      case keys.ENTER:
        this.handleFlagItemClick(this.state.onlyCountries[this.state.highlightCountryIndex], e);
        break;
      case keys.ESC:
        this.setState({
          showDropdown: false
        }, this.cursorToEnd);
        break;
      default:
        if ((e.which >= keys.A && e.which <= keys.Z) || e.which === keys.SPACE) {
          this.setState({
            queryString: this.state.queryString + String.fromCharCode(e.which)
          }, this.state.debouncedQueryStingSearcher);
        }
    }
  }

  handleInputKeyDown = (e) => {
    const { keys } = this.props;
    if (e.which === keys.ENTER) {
      this.props.onEnterKeyPress(e);
    }

    if (this.props.onKeyDown) this.props.onKeyDown(e);
  }

  handleClickOutside = (e) => {
    if (this.dropdownRef && !this.dropdownContainerRef.contains(e.target)) {
      this.state.showDropdown && this.setState({ showDropdown: false });
    }
  }

  handleSearchChange = (e) => {
    const { currentTarget: { value: searchValue } } = e;
    this.setState({ searchValue });
  }

  getCountryDropdownList = () => {
    const {
      state: {
          preferredCountries,
          onlyCountries,
          highlightCountryIndex,
          showDropdown,
          searchValue,
          enableSearchField,
          searchClass,
          selectedCountry,
          localization,
          dropdownClass
      }
    } = this;

    const countryIsPreferred = preferredCountries.includes(selectedCountry);
    const allCountries = preferredCountries.concat(onlyCountries);

    const sanitizedSearchValue = searchValue.trim().toLowerCase();
    const filteredCountries = (enableSearchField && sanitizedSearchValue)
      ? [...new Set(allCountries._.filter(({ name, iso2, dialCode }) =>
        [`${name}`, `${iso2}`, `+${dialCode}`]._.some(field => field.toLowerCase().includes(sanitizedSearchValue))))]
      : allCountries;

    let countryDropdownList = _.map(filteredCountries, (country, index) => {
      const itemClasses = classNames({
        'tel-input__country-list-item': true,
        preferred: country.iso2 === 'tel-input--phone-us' || country.iso2 === 'tel-input--phone-gb',
        active: country.iso2 === 'tel-input--phone-us',
        'tel-input__country--highlight': countryIsPreferred ? highlightCountryIndex === index : highlightCountryIndex === index - preferredCountries.length
      });

      const inputFlagClasses = `tel-input__flag tel-input__flag--list tel-input--phone-${country.iso2}`;

      return (
        <li
          ref={el => this[`flag_no_${index}`] = el}
          key={`flag_no_${index}`}
          data-flag-key={`flag_no_${index}`}
          className={itemClasses}
          data-dial-code="1"
          data-country-code={country.iso2}
          onClick={() => this.handleFlagItemClick(country)}
        >
          <div className={inputFlagClasses}/>
          <span className='tel-input__country-name'>{
              localization[country.name] != undefined ?
              ocalization[country.name] : country.name
          }</span>
          <span className='tel-input__dial-code'>{'+' + country.dialCode}</span>
        </li>
      );
    });

    const dashedLi = (<li key={'dashes'} className='tel-input__divider'/>);
    (preferredCountries.length > 0) &&
    countryDropdownList.splice(preferredCountries.length, 0, dashedLi);

    const dropDownClasses = classNames({
      [dropdownClass]: true,
      'tel-input__country-list': true,
      'tel-input--hide': !showDropdown
    });

    return (
      <ul
        ref={el => this.dropdownRef = el}
        className={dropDownClasses}
        style={this.props.dropdownStyle}
      >
        {enableSearchField && (
          <li
            className={classNames({
              search: true,
              [searchClass]: searchClass,
            })}
          >
            <label>
              <span
                className={classNames({
                  'tel-input__search-emoji': true,
                  [`${searchClass}-emoji`]: searchClass,
                })}
                role="img"
                aria-label="Magnifying glass"
              >
                &#128270;
              </span>
              <input
                className={classNames({
                  'tel-input__search-box': true,
                  [`${searchClass}-box`]: searchClass,
                })}
                id="search-box"
                type="search"
                placeholder="search"
                autoFocus="true"
                value={searchValue}
                onChange={this.handleSearchChange}
              />
            </label>
          </li>
        )}
        {countryDropdownList.length > 0
          ? countryDropdownList
          : (
            <li className="no-entries-message">
              <span>No entries to show.</span>
            </li>
          )}
      </ul>
    );
  }

  errorText() {
      const { isValid, value, errorClass } = this.state;

      if (isValid) {
          return null;
      }

      return (
          <div className={`validation__error-msg ${errorClass}`}>
              {this.getErrorMessage && this.getErrorMessage()}
          </div>
      );
  }

  render() {
    const { selectedCountry, showDropdown, value,isValid } = this.state;
    const disableDropdown = this.props.disableDropdown;

    const arrowClasses = classNames({"tel-input__arrow": true, "tel-input__arrow--up": showDropdown});
    const inputClasses = classNames({
      [this.props.inputClass]: true,
      "tel-input__form-control": true
    });

    const flagViewClasses = classNames({
      [this.props.buttonClass]: true,
      "tel-input__flag-dropdown": true,
      "tel-input__flag-dropdown--open": showDropdown
    });
    const inputFlagClasses = `tel-input__flag tel-input__flag--main tel-input--phone-${selectedCountry.iso2}`;
    return (
        <div className={this.state.wrapperClass}>
            <label
                className={this.state.labelClass}
                htmlFor={this.state.id}
            >
                {this.state.label}
            </label>
            <div className={this.state.inputWrapperClass}>
                <div
                  className={`${this.props.containerClass} ${isValid ? '': 'tel-input--no-valid'}`} 
                  style={this.props.containerStyle}>
                  <input
                    name={this.state.name}
                    id={this.state.id}
                    className={inputClasses}
                    style={this.props.inputStyle}
                    onChange={this.handleInput}
                    onClick={this.handleInputClick}
                    onFocus={this.handleInputFocus}
                    onBlur={this.handleInputBlur}
                    value={value}
                    ref={el => this.numberInputRef = el}
                    onKeyDown={this.handleInputKeyDown}
                    placeholder={this.state.placeholder}
                    disabled={this.props.disabled}
                    type="tel"
                    {...this.props.inputExtraProps}
                  />

                  <div
                    className={flagViewClasses}
                    style={this.props.buttonStyle}
                    onKeyDown={this.handleKeydown}
                    ref={el => this.dropdownContainerRef = el}
                  >
                    <div
                      onClick={disableDropdown ? undefined : this.handleFlagDropdownClick}
                      className='tel-input__selected-flag'
                      title={selectedCountry ? `${selectedCountry.name}: + ${selectedCountry.dialCode}` : ''}
                    >
                      <div className={inputFlagClasses}>
                        {!disableDropdown && <div className={arrowClasses}></div>}
                      </div>
                    </div>
                    {showDropdown && this.getCountryDropdownList()}
                  </div>
                </div>
                {this.errorText()}
            </div>
        </div>
    );
  }
}

export default PhoneInput;
