import React from 'react'
import { object, string } from 'prop-types'
import uniqid from 'uniqid'
import Combobox from './combobox.jsx'
import Number from './number.jsx'
import Hidden from './hidden.jsx'
import { ValidatorComponent } from '../../libraries/validation/index.js';

class DateCustom extends ValidatorComponent {

    static propTypes = {
        day: object,
        month: object,
        year: object
    }

	static defaultProps = {
		day: {
            available: true,
            value: '',
			hidden: false,
            name: '',
            required: true,
            className:  'number col-3 col-lg-3 padding--0 padding--r-5',
            options: [],
            static: '',
        },
        month: {
            available: true,
            value: '',
            name: '',
            required: true,
			hidden: false,
            className: '',
            options: [],
            static: '',
            wrapperClass:'col-5 col-md-5 col-lg-5 col-xl-5 padding--0  combobox'
        },
        year: {
            available: true,
            value: '',
			hidden: false,
            name: '',
            required: true,
            className: 'number col-4 col-lg-4 col-xl-4 padding--0 padding--l-5',
            options: [],
            static: '',
            maxYear: new Date().getFullYear() - 16
        },

        wrapperClass: '',
        coverClass: '',
        labelClass: '',
        isValid: true,
        label: '',
        value: '',

        errorMessages: [],
        validators: [],
        validatorListener: () => {},
        addToValidateForm: true
	}

    constructor(props){
        super(props)

        this.state = {
            ...props,
            day: {...DateCustom.defaultProps.day, ...props.day, id: uniqid(props.day.name + '-') },
            month: {...DateCustom.defaultProps.month, ...props.month, id: uniqid(props.month.name + '-')},
            year: {...DateCustom.defaultProps.year, ...props.year, id: uniqid(props.year.name + '-')},
            value: props.day.value + '/' + props.month.value + '/' + props.year.value,
            ...{id: uniqid(props.name + '-')}
        }
    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

	renderInputTypeHidden(obj = {}){

		return (
			<Hidden
				value={obj.value}
				name={obj.name}
			/>
		)

    }
        
    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({
                 ...this.state,
                 ...this.props,
                day: {...DateCustom.defaultProps.day, ...this.props.day, id: uniqid(this.props.day.name + '-') },
                month: {...DateCustom.defaultProps.month, ...this.props.month, id: uniqid(this.props.month.name + '-')},
                year: {...DateCustom.defaultProps.year, ...this.props.year, id: uniqid(this.props.year.name + '-')},
                value: this.props.day.value + '/' + this.props.month.value + '/' + this.props.year.value,
                ...{id: uniqid(this.props.name + '-')}
            });
        }
    }
    onChangeValue(object){

        let {order, value} = object,
            {value: stateValue, onChange} = this.state,
            arrayFromValue = stateValue.split('/'),
            data = {
                0: 'day',
                1: 'month',
                2: 'year'
            };
            arrayFromValue[order] = value && value.label || value;

            let newValue = arrayFromValue.join('/');
            if(onChange){
                onChange(newValue)
            }
            this.setState({
                ...this.state,
                [data[order]]: {
                    ...this.state[data[order]], 
                    value:  value && value.label || value
                },
                value: newValue
            })
        
    }

	renderInputTypeNumber(obj = {}, required = {}, values, className, order, isValid){
		return (
			<Number
                parentAction={{
                    onChangeValue: ::this.onChangeValue,
                    order: order
                }}
                addToValidateForm={false}
                wrapperClass={`${className} ${isValid ? '': 'number--no-valid'}`}
				value={obj.value}
				name={obj.name}
				min={values.min}
				max={values.max}
				{...required}
			/>
		)

	}

    errorText() {
        const { isValid, value } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    render (){

        let {day, month, year, year:{maxYear},isValid} = this.state,
            dayRequired = day.required ? {required: "required"} : {},
            monthRequired = month.required ? {required: "required"} : {},
            yearRequired = year.required ? {required: "required"} : {};

        return (<div className={this.state.wrapperClass}>
                    <label
                        className={this.state.labelClass}
                    >
                        {this.state.label}
                    </label>
                    <div className={this.state.coverClass}>
                        <div className="row margin--l-0 margin--r-0">
                            {
								this.state.day.available ?
									::this.renderInputTypeNumber(
                                        day,
                                        dayRequired,
                                        {min: 1, max: 31},
                                        day.className,
                                        0,
                                        isValid
                                    ) : this.state.day.hidden ? ::this.renderInputTypeHidden(day) : null
                            }
                            {
								this.state.month.available &&
                                <Combobox
                                    parentAction={{
                                        onChangeValue: ::this.onChangeValue,
                                        order: 1
                                    }}
                                    addToValidateForm={false}
                                    wrapperClass={`${month.wrapperClass} ${isValid ? '': 'combobox--no-valid'}`}
	                                name={month.name}
	                                list={month.options}
									value={month.value}
                                	{...monthRequired}
                            	/> || null
							}
                            {
                                this.state.year.available ?
                                    ::this.renderInputTypeNumber(
                                        year,
                                        yearRequired,
                                        {min: 1, max: maxYear},
                                        year.className,
                                        2,
                                        isValid
                                    ) : this.state.year.hidden ? ::this.renderInputTypeHidden(year) : null
                            }
                        </div>
                        {this.errorText()}
                    </div>
            </div>)
    }

}

export default DateCustom;
