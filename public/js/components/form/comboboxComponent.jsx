import React from "react";
import uniqid from "uniqid";

export default class ComboboxComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isExpandedFilters: false,
            ...props
        };
        this.__refs = null;
        this.handleClickOutside = ::this.handleClickOutside;
    }
    static defaultProps = {
        valueClass:"comboboxComponent__name-size-14"
    }
    handleExpandFiltellrs() {
        this.setState({
            ...this.state,
            isExpandedFilters: !this.state.isExpandedFilters
        });
    }
    closeExpandFilters() {
        this.setState({
            ...this.state,
            isExpandedFilters: false
        });
    }

    componentDidMount() {
		document.addEventListener('mousedown', this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event) {
		if (this.__refs && !this.__refs.contains(event.target)) {
			this.closeExpandFilters();
		}
    }

    handleChangeValue(event) {

        const { target: { dataset: { label = '' } = {} } = {} } = event,
        { filters, onChange } = this.props;

        if(!label) return

        onChange(label)

        this.setState({
            ...this.state,
            isExpandedFilters: false
        });

    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    setToName(value,list){
        value = String(value);
        const index = _.findIndex(list, {label: value}),
              obj = index > -1 ? list[index] : null;
        if(obj) {
            return obj.name;
        }
        return '';
    }
    
    render() {

        const {
            isExpandedFilters,
            valueClass,
            list
        } = this.state;
        const {value} = this.props;

        return (
            <div
                className={`comboboxComponent ${this.props.className}`}
                ref={(refs) => { this.__refs = refs; }}
                onClick={
                    this.handleClickOutside
                }
            >
                <div
                    className={`comboboxComponent__name pointer ${valueClass}`}
                    onClick={() => this.handleExpandFiltellrs()}
                >
                    {this.setToName(value,list)}
                </div>
                <ul
                    className={`comboboxComponent__list ${this.props.classList} ${
                        isExpandedFilters
                            ? "comboboxComponent__list--expanded"
                            : ""
                    }`}
                >
                
                    {
                        list.map((item, i) => {
                            return (
                                <li
                                    key={uniqid(`${item.label}-${i}`)}
                                    data-label={item.label}
                                    className="comboboxComponent__list-item"
                                    onClick={::this.handleChangeValue}
                                >
                                    {item.name} 
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }
}
