import React from "react";
import uniqid from 'uniqid';
import { string, array, bool } from "prop-types";
import { ValidatorComponent } from '../../libraries/validation/index.js';

class OneCheckbox extends ValidatorComponent {
    static propTypes = {
        value: array.isRequired,
        required: bool,
        label: string,
        inputClass: string,
        labelClass: string,
        wrapperClass: string,
        wrapperAllCheckbox: string,
        readonly: bool,
    };

    static defaultProps = {
        required: false,
        label: "",
        inputClass: "",
        labelClass: "checkbox__label",
        wrapperClass: "checkbox",
        checked: true,
        value:{
            checkboxIcon: "checkbox__checkmark"
        },
        errorMessages: [],
        validators: [],
        validatorListener: () => {},

        addToValidateForm: true,
        readonly: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            ...OneCheckbox.defaultProps,
            ...this.props,
            value: [props.value[0]],
            ...{id: uniqid(this.props.name + '-')}
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    errorText() {
        const { isValid } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg validation__error-msg--left">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    handleChange(label) {
        const {value, onChange, readonly} = this.state;
        if(readonly === true){
            return;
        }

        let newValueMap = value.map((item, i) => {
            if(item.label == label){
                item.value = !item.value
                item.checked = !item.checked
            }
            return item;
        });

        this.setState({
            ...this.state,
            value: newValueMap
        });

        if (newValueMap.some(el => el.checked)) {
            this.makeValid();
        }

        if(onChange){
            onChange(newValueMap);
        }
    }
    
    render() {

        const {
            wrapperClass,
            labelWrapperClass,
            labelClass,
            id,
            label,
            subLabel,
            subLabelClass,
            wrapperAllCheckbox,
            children
        } = this.state;
        let {value} = this.state,
        checked = value[0].checked ? "checked" : "";
        return (
            <div className={wrapperClass}>
                <div className={labelWrapperClass}>
                    <label
                        className={labelClass}
                        htmlFor={id}
                    >
                        {label}
                    </label>
                    { subLabel &&
                            <span className={subLabelClass}>
                            {subLabel}
                        </span>  || null
                    }
                </div>
                   <div className={wrapperAllCheckbox}>
                        <span
                            className={`checkbox__inner-wrapper ${ value[0].innerWrapperClass} ${checked}`}
                            onClick={() => this.handleChange(value[0].label)}
                            data-title={value[0].title}
                        >
                            <input
                                type="checkbox"
                                className={`checkbox__input ${value[0].classNameInput}`}
                                name={value[0].name}
                                value={value[0].value || ""}
                                checked={value[0].checked || false}
                                readOnly={true}
                            />
                            <span className={value[0].checkboxIcon || "checkbox__checkmark"} />
                            {(value[0].sub && <span className={value[0].subClass}>{value[0].sub}</span>) || null}
                        </span>
                        {this.errorText()}
                   </div>

                    {children || null}
            </div>
        );
    }
}

export default OneCheckbox;
