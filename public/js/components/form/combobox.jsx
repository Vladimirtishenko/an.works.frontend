import React from "react";
import Combobox from "react-widgets/lib/Combobox";
import {string, array, bool} from "prop-types";
import uniqid from 'uniqid';

import { ValidatorComponent } from '../../libraries/validation/index.js';

class ComboboxComponent extends ValidatorComponent {
    static propTypes = {
        name: string.isRequired,
        list: array.isRequired,
        justState: bool
    };

    static defaultProps = {
        value: "",

        errorMessages: [],
        validators: [],
        validatorListener: () => {},

        addToValidateForm: true,
        isValid: true
    };

    constructor(props) {
        super(props);
        this.state = {
            ...props,
            ...{id: uniqid(this.props.name + '-')}
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props),
            { value: nextPropsValue, justState } = this.props,
            {value: nextStateValue} = prevState;

        if (!comparing) {
            
            let value ='';

            if(justState){
                value = prevState.value !== this.state.value ? this.state.value : nextStateValue == '' ? nextPropsValue : nextStateValue;
            }else{
                value = nextPropsValue;
            }

            this.setState({
                ...this.state,
                ...this.props,
                value,
            });
        }
    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);

    }

    componentWillUnmount(){
        this.detach(this);
    }

    onChangeValue(value) {

        this.makeValid()
        this.setState({ value: value.label || value || ComboboxComponent.defaultProps.value });

        if (this.state.onChange) {
            this.state.onChange(value);
        }

        if(this.state.parentAction){
			let {onChangeValue, order} = this.state.parentAction;
			onChangeValue && onChangeValue({value: value, order: order});
		}

    }

    errorText() {
        const { isValid, value } = this.state;
        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    render() {

        const {
                list,
                value: currentValue,
                disabled: stateDisabled,
                wrapperClass,
                label,
                labelClass,
                className,
                placeholder,
                name,
                children,
                id,
                open,
                isValid
            } = this.state,
            valueInArray = currentValue && list && Array.isArray(this.state.list) && _.find(list, item => {
                if (item.label == currentValue) return item.label;
            }),
            value = valueInArray ? valueInArray : currentValue,
            stringValue = valueInArray && valueInArray.label || "",
            disabled = stateDisabled ? { disabled: true } : {};
        return (
            <div className={wrapperClass}>
                {(label && (
                    <label className={labelClass}>
                        {label}
                    </label>
                )) ||
                    null}
                <div className={`fl fl--dir-col ${className} ${isValid ? '': 'combobox--no-valid'}` }>
                    <div className="combobox__wrap-input">
                    <Combobox
                        filter='contains'
                        {...disabled}
                        open={open}
                        id={id}
                        data={list}
                        textField="name"
                        valueField="label"
                        value={value}
                        autoComplete="no-password"
                        placeholder={placeholder}
                        onChange={::this.onChangeValue}
                        messages={{
                            emptyList: "Данные не загружены",
                            emptyFilter: "Данные не найдены"
                        }}
                    />
                    </div>
                    {this.errorText()}
                </div>
                <input
                    type="hidden"
                    name={name}
                    readOnly
                    value={stringValue}
                />
                {children || null}
            </div>
        );
    }
}

export default ComboboxComponent;
