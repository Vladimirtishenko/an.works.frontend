import React from "react";
import { string, array, bool } from "prop-types";
import { ValidatorComponent } from "../../libraries/validation/index.js";
import uniqid from 'uniqid'

class Radio extends ValidatorComponent {
    static propTypes = {
        name: string.isRequired,
        value: array.isRequired,
        type: string,
        required: bool,
        label: string,
        inputClass: string,
        labelClass: string,
        wrapperClass: string,
        innerRadiosWrapperClass: string,
        wrapperClassRadio: string
    };

    static defaultProps = {
        required: false,
        label: "",
        inputClass: "input",
        labelClass: "label",
        wrapperClass: "",
        innerRadiosWrapperClass: "",
        wrapperClassRadio: "fl fl--dir-col padding--md-0",
        errorMessages: [],
        validators: [],
        validatorListener: () => {},
        addToValidateForm: true
    };

    constructor(props) {
        super(props);

        this.state = {
            ...props,
            ...{id: uniqid(props.name + '-')}
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    componentDidMount() {
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    handleChange(value) {

        const {value: listValue, onChange} = this.state,
              newValueMap = listValue.map((item, i) => {
                  if(item.value == value){
                      item.checked = true
                  } else {
                      item.checked = false
                  }
                  return item;
              });

        this.setState({
            ...this.state,
            value: newValueMap
        });

        if (newValueMap.some(el => el.checked)) {
            this.makeValid();
        }

        if(onChange){
            onChange(newValueMap);
        }
    }

    errorText() {
        const { isValid } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    renderOneOf() {

        const {name, value} = this.state;

        return value.map((item, i) => {
            return (
                <div
                    key={i}
                    className={`radio__inner-wrapper  ${
                        item.innerWrapperClass
                    } ${item.checked ? "checked" : ""}`}
                    onClick={() => this.handleChange(item.value)}
                >
                    <input
                        type="radio"
                        name={name}
                        value={item.value || ""}
                        className={`radio__input`}
                        checked={item.checked || false}
                        readOnly={true}
                    />
                    <span className="radio__checkmark" />
                    <span>{item.sub}</span>
                </div>
            );
        });
    }

    render() {

        const {
            wrapperClass,
            labelClass,
            id,
            label,
            wrapperClassRadio
        } = this.state;

        return (
            <div className={`radio ${wrapperClass}`}>
                <label
                    className={labelClass}
                    htmlFor={id}
                >
                    {label}
                </label>

                <div className={this.state.wrapRadio}>
                    <div
                        className={`fl fl--justify-b ${
                            wrapperClassRadio
                        }`}
                    >
                        {this.renderOneOf()}
                    </div>
                    {this.errorText()}
                </div>
            </div>
        );
    }
}

export default Radio;
