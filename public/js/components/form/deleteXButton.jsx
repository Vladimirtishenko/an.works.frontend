import React from "react";
import PropTypes from "prop-types";

export const DeleteXButton = props => (
    <span
        className={`title title--removable title--bigger font--color-secondary pointer btn--delete ${ 
            props.className
        }`}
        onClick={props.onClick}
        data-id={props["data-id"]}
        data-option={props["data-option"]}
        data-action={props["data-action"]}
        data-period={props["data-period"]}
    >
        {props.text}
    </span>
);

DeleteXButton.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string,
    "data-id": PropTypes.string,
    "data-option": PropTypes.string,
    "data-action": PropTypes.string,
    "data-period": PropTypes.string
};
