import React from 'react'
import uniqid from 'uniqid';
import { string, bool, array } from 'prop-types'
import { ValidatorComponent } from '../../libraries/validation/index.js';

class Text extends ValidatorComponent {

    static propTypes = {
        name: string.isRequired,
        type: string,
        required: bool,
        label: string,
        inputClass: string,
        labelClass: string,
        wrapperClass: string,
        inputWrapperClass: string,
        placeholder: string,
        extraChildClass: string,
        extraChildText: string,
        readonly: bool,
        inputType: string,
        inputIcon: string,
        errorMessages: array,
        validators: array,
        addToValidateForm: bool,
        step: string,
        justState: bool
    }

	static defaultProps = {
		type: 'text',
		required: false,
		label: '',
        inputType: '',
        errorClassInput: '',
        inputClass: 'input',
        inputIcon: 'icon icon--input',
        iconAvailable: false,
        iconPush: () => {},
		labelClass: 'label',
		wrapperClass: '',
        childAvailable: false,
        extraChildClass: '',
        extraChildText: '',
        childPush: () => {},
        childId: '',
        labelIcon: 'icon',
        readonly: false,
        value: '',
        justState: false,
        isValid: true,
        errorMessages: [],
        validators: [], 
        validatorListener: () => {},

        addToValidateForm: true

	}

	constructor(props){
		super(props);

		this.state = {
				...Text.defaultProps,
				...this.props,
                ...{id: uniqid(this.props.name + '-')}

		}

	}

	componentDidUpdate(prevProps, prevState){

		const comparingProps = _.isEqual(prevProps, this.props),
              { value: nextPropsValue, justState } = this.props,
              {value: nextStateValue} = prevState;

		if(!comparingProps) {

            let value ='';
            
            if(justState){
                value = prevState.value !== this.state.value ? this.state.value : nextStateValue == '' ? nextPropsValue : nextStateValue;
            }else{
                value = nextPropsValue;
            }

			this.setState({
                ...this.state,
                ...this.props,
                value
            })
		}

	}
    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    errorText() {
        const { isValid, value, errorClass,errorClassInput} = this.state;

        if (isValid) {
            return null;
        }
        return (
            <div className={`validation__error-msg ${errorClass}`}>
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    onChange(event){
        const  {onChange} = this.props;
        let value = event && event.target && event.target.value || '';

        this.makeValid();

        this.setState({
            ...this.state,
            value: value
        });

        if(onChange){
            onChange(value);
        }

    }

	render(){

		let required = this.state.required ? {required: 'required'} : {},
            readonly = this.state.readonly ? {readOnly: 'readonly'} : {},
            step = this.state.step ? {step: this.state.step} : {},
            {isValid} = this.state;

		return (
			<div className={this.state.wrapperClass}>
				<label
					className={this.state.labelClass}
					htmlFor={this.state.id}
				>
					{this.state.label}
				</label>
                <div className={this.state.inputWrapperClass} data-title={this.state.dataTitle}>
                    <input
                        ref={(r) => { this.input = r }}
                        onChange={::this.onChange}
                        value={this.state.value}
                        className={`${this.state.inputClass} ${this.state.errorClassInput} ${isValid ? '': 'input--no-valid'}`}
                        id={this.state.id}
                        name={this.state.name}
                        placeholder={this.state.placeholder}
                        type={this.state.inputType}
                        {...required}
                        {...readonly}
                        {...step}
                    />
                    {this.errorText()}
                </div>
                {this.props.children || null}
			</div>
		)
	}
}

export default Text;
