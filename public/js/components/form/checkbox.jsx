import React from "react";
import uniqid from 'uniqid';
import { string, array, bool } from "prop-types";
import { ValidatorComponent } from '../../libraries/validation/index.js';

class Checkbox extends ValidatorComponent {
    static propTypes = {
        value: array.isRequired,
        required: bool,
        label: string,
        inputClass: string,
        labelClass: string,
        wrapperClass: string,
        wrapperAllCheckbox: string,
        readonly: bool,
    };

    static defaultProps = {
        required: false,

        label: "",

        inputClass: "",
        labelClass: "checkbox__label",
        wrapperClass: "checkbox",
        checked: true,
        value:{
            checkboxIcon: "checkbox__checkmark"
        },
        errorMessages: [],
        validators: [],
        validatorListener: () => {},

        addToValidateForm: true,
        readonly: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            ...Checkbox.defaultProps,
            ...this.props,
            ...{id: uniqid(this.props.name + '-')}
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    errorText() {
        const { isValid } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className="validation__error-msg validation__error-msg--left">
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    handleChange(label) {
        const {value, onChange, readonly} = this.state;
        if(readonly === true)
        {
            return;
        }
        let newValueMap = value.map((item, i) => {
                  if(item.label == label){
                      item.value = !item.value
                      item.checked = !item.checked
                  }
                  return item;
              });

        this.setState({
            ...this.state,
            value: newValueMap
        });

        if (newValueMap.some(el => el.checked)) {
            this.makeValid();
        }

        if(onChange){
            onChange(newValueMap);
        }
    }

    renderOneOf() {

        const {value} = this.state;
        return value.map((item, i) => {
            const classChecked = item.checked ? "checked" : "",
                  key = uniqid(`${item.label}-`);

            return (
                <span
                    key={key}
                    id={key}
                    className={`checkbox__inner-wrapper ${ item.innerWrapperClass} ${classChecked}`}
                    onClick={() => this.handleChange(item.label)}
                    data-title={item.title}
                >
                    <input
                        type="checkbox"
                        className={`checkbox__input ${item.classNameInput}`}
                        name={item.name}
                        value={item.value || ""}
                        checked={item.checked || false}
                        readOnly={true}
                    />
                    <span className={item.checkboxIcon || "checkbox__checkmark"} />
                    {(item.sub && <span className={item.subClass}>{item.sub}</span>) || null}
                </span>
            );
        });
    }

    render() {

        const {
            wrapperClass,
            labelWrapperClass,
            labelClass,
            id,
            label,
            subLabel,
            subLabelClass,
            wrapperAllCheckbox,
            children
        } = this.state;


        return (
            <div className={wrapperClass}>
                <div className={labelWrapperClass}>
                    <label
                        className={labelClass}
                        htmlFor={id}
                    >
                        {label}
                    </label>
                    { subLabel &&
                            <span className={subLabelClass}>
                            {subLabel}
                        </span>  || null
                    }
                </div>
                   <div className={wrapperAllCheckbox}>
                        {this.renderOneOf()}
                        {this.errorText()}
                   </div>

                    {children || null}
            </div>
        );
    }
}

export default Checkbox;
