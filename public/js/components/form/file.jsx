import React from 'react'
import { string, bool } from 'prop-types'

class File extends React.Component {
		
	static propTypes = {
		name: string.isRequired,
		required: bool,
		label: string,
		inputClass: string,
		labelClass: string,
		wrapperClass: string,
	  }	 

	static defaultProps = {
		required: false,
		
		label: '',

		inputClass: 'input',
		labelClass: 'label',
		wrapperClass: ''
	}

	constructor(props){
		super(props);

		this.state = {
			...File.defaultProps, 
			...this.props
		}
	}

	renderOneOf(){

		return this.state.radio.map((item, i) => {
			return 
		})

	}

	render(){

		return (
			<div className={this.state.wrapperClass}>
				<label 
					className={this.state.labelClass} 
					htmlFor={this.state.id}
				>
					{this.state.label}
				</label>
				<input 
					type='file' 
					name={item.name}
				/>
			</div>
		)
	}
}

export default File;