import React from "react";
import simpleNumberLocalizer from "react-widgets-simple-number";
import NumberPicker from "react-widgets/lib/NumberPicker";

import { ValidatorComponent } from "../../libraries/validation/index.js";

simpleNumberLocalizer();
 
class Number extends ValidatorComponent {
    static defaultProps = {
        min: 0,
        max: 0,

        errorMessages: [],
        validators: [],
        validatorListener: () => {},

        addToValidateForm: true,
        isValid: true
    };

    constructor(props) {
        super(props);

        this.state = {
            ...Number.defaultProps,
            ...props
        };
    }

    componentDidMount() {
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    componentDidUpdate(prevProps) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    onChange(value) {
        const { parentOnChange, patterns, min } = this.props;

        if(patterns  && patterns.test(value)) return;
        
        this.makeValid()
        if (this.state.parentAction) {
            let { onChangeValue, order } = this.state.parentAction;
            onChangeValue && onChangeValue({ value: value, order: order });
        }

        parentOnChange && parentOnChange(value);

        if (value > min) {
            this.setState({
                value: value
            });
        }

    }

    errorText() {
        const { isValid, value, errorClass} = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div className={`validation__error-msg ${errorClass}`}>
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    render() {

        const {
            wrapperClass,
            label,
            labelClass,
            min,
            max,
            value,
            name,
            inputWrapperClass,
            isValid
        } = this.state;

        return (
            <div className={wrapperClass}>
                {(label && (
                    <label className={labelClass}>
                        {label}
                    </label>
                )) ||
                    null}
                <div className={inputWrapperClass}>
                    <div className={isValid ? '':'number--no-valid'}>
                        <NumberPicker
                            this
                            min={min}
                            max={max}
                            format="-"
                            onChange={::this.onChange}
                            value={value}
                            name={name}
                            inputProps={{type: 'number'}}
                        />
                    </div>
                    {this.errorText()}
                </div>
            </div>
        );
    }
}

export default Number;
