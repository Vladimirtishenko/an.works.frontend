import React from "react";
import PropTypes from "prop-types";

export const DeleteButton = props => (
    <span
        className={`font--underlined font--color-secondary pointer ${
            props.className
        }`}
        onClick={e => props.onClick(e)}
        data-id={props["data-id"]}
        data-mark={props["data-mark"]}
    >
        {props.text}
    </span>
);

DeleteButton.propTypes = {
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string,
    "data-id": PropTypes.string,
    "data-mark": PropTypes.string
};
