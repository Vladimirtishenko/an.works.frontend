import React from 'react';
import Checkbox from "./checkbox.jsx";
import { oneOfType, object, bool } from 'prop-types'
import Combobox from './combobox.jsx'
import Number from './number.jsx'

import {
    transformToStringValue,
    transformToStringValueNow
} from '../../helpers/date.helper.js'

import uniqid from 'uniqid';
import { ValidatorComponent } from '../../libraries/validation/index.js';

import {
    preparationMonth
} from "../../helpers/date.helper.js";

class DateFromTo extends ValidatorComponent {

    static propTypes = {
        start: oneOfType([object]),
        end: oneOfType([object]),
        now: oneOfType([object])
    }

    static defaultProps = {
        start: {
            month: undefined,
            year: undefined,
            min: 1990
        },
        end: {
            month: undefined,
            year: undefined,
            min: 1990
        },
        now: {
            value: false
        },
        value: '',
        errorMessages: [],
        validators: [],
        validatorListener: () => {},
        addToValidateForm: true
    }

    constructor(props){
        super(props);

        this.state = {
            ...props,

            start: {
                list: preparationMonth({...props.translation}) || [],
                ...DateFromTo.defaultProps.start,
                ...props.start
            },

            end: {
                list: preparationMonth({...props.translation}) || [],
                ...DateFromTo.defaultProps.end,
                ...props.end
            },

            now: {
                ...DateFromTo.defaultProps.now,
                ...props.now
            },

            id: uniqid(props.name + '-'),

            value: props.now.value ? transformToStringValueNow(props.start, props.now) : transformToStringValue(props.start, props.end)

        };

    }

    componentDidMount(){
        this.state.addToValidateForm && this.attach(this);
    }

    componentWillUnmount(){
        this.detach(this);
    }

    onChangeDate(){

        const { end, start, now } = this.state,
              newNow = {...now, value: !now.value},
              newEnd = {...end, month: undefined, year: undefined};

        this.setState({
            ...this.state,
            now: newNow,
            end: newEnd,
            value: transformToStringValueNow(start, newNow)
        })

        this.makeValue(start, newEnd, newNow);

    }

    returnAvailableMonth(value) {

        const { translation } = this.props,
                preparedMonth = preparationMonth({...translation}),
                newListOfMonth = preparedMonth.filter( (item) => {
                    if(parseInt(item.label) > parseInt(value)){
                        return item;
                    }
                });

        return newListOfMonth;

    }

    onChangeValue(obj) {

        const { start, end, now } = this.state,
              { value, order } = obj,
              self = this;

        let newStartValue = {...start},
            newEndValue = {...end};

        let actions = {
            'start.month'() {

                if( (start.year && end.year) && start.year == end.year){

                    const monthList = self.returnAvailableMonth(value.label);

                    newEndValue = {
                        ...newEndValue,
                        list: monthList
                    }

                }

                if(end.month > value.label) {

                    newEndValue = {
                        ...newEndValue,
                        value: ''
                    }
                }

                newStartValue = {
                    ...newStartValue,
                    month: value.label
                }

            },
            'start.year'() {

                if(end.year && end.year > value){

                    newEndValue = {
                        ...newEndValue,
                        year: undefined
                    }

                }

                if(value){
                    newEndValue = {
                        ...newEndValue,
                        year: undefined,
                        min: value
                    }
                }

                newStartValue = {
                    ...newStartValue,
                    year: value
                }

            },
            'end.month'() {
                const { label } = value;

                if(start.month && start.month && end.year >= +label){
                    newEndValue = {
                        ...newEndValue,
                        month: value.label,
                        year: start.year + 1
                    }
                } else {
                    newEndValue = {
                        ...newEndValue,
                        month: value.label
                    }
                }


            },
            'end.year'() {

                if(start.year && start.month && start.year > value) {
                    const monthList = self.returnAvailableMonth(start.month),
                          newValue = _.findIndex(monthList, {label: end.month}) > -1 ? end.month : undefined;

                    newEndValue = {
                        ...newEndValue,
                        list: monthList,
                        month: newValue,
                        year: start.year
                    }

                }
                
                if(start.year && start.month && start.year == value && +start.month < +end.month) {
                    newEndValue = {
                        ...newEndValue,
                        year: start.year
                    }
                }

                if(start.year && start.month && start.year == value && +start.month >= +end.month) {
                    newEndValue = {
                        ...newEndValue,
                        year: start.year + 1
                    }
                }

                if(value > start.year) {
                    const monthList = self.returnAvailableMonth(0);

                    newEndValue = {
                        ...newEndValue,
                        list: monthList,
                        year: value
                    }
                }

                if (start.month >= end.month && start.year && !end.year ){
                    newEndValue = {
                        ...newEndValue,
                        year: start.year + 1
                    }
                }

            }
        }

        actions[order] && actions[order]();

        this.setState({
            ...this.state,
            start: newStartValue,
            end: newEndValue,
            // value: transformToStringValue(newStartValue, newEndValue)
            value: newEndValue.month !== undefined && newEndValue.year !== undefined ? transformToStringValue(newStartValue, newEndValue) : transformToStringValueNow(newStartValue, now)
        })

        this.makeValue(newStartValue, newEndValue, now);

    }



    makeValue(start, end, now = {}){

        const { month: startMonth, year: startYear } = start,
              { month: endMonth, year: endYear } = end,
              { value: nowValue = false } = now,
              { onChange } = this.props;

        if(startMonth && startYear && nowValue){
            onChange && onChange({start: [startMonth, startYear], now: nowValue});
            return;
        }

        if(startMonth && startYear && endMonth && endYear && (endYear => startYear)){
            onChange && onChange({start: [startMonth, startYear], end: [endMonth, endYear]});
            return;
        }

    }

    errorText() {
        const { isValid, value , now:{value:valueNow}} = this.state;
        if (isValid) {
            return null;
        }
        return ( 
            <div className={`validation__error-msg margin--r-25-p ${valueNow ? 'margin--md-r-305': 'margin--md-r-55'}`}>
                {this.getErrorMessage && this.getErrorMessage()}
            </div>
        );
    }

    renderPartOfPeriod(key, className){

        const i = uniqid(`${key}-`),
              now = (new Date()).getFullYear(),
              keys = this.state[key],
              { min, list, month, year, name } = keys,
            isValid = this.state.isValid;

        return (
            <div className={className}>
                <Combobox
                    parentAction={{
                        onChangeValue: ::this.onChangeValue,
                        order: `${key}.month`
                    }}
                    wrapperClass="col-5 col-md-7 padding--0 combobox"
                    addToValidateForm={false}
                    list={list}
                    name={name}
                    value={month}
                    isValid={isValid}
                />
                <Number
                    parentAction={{
                        onChangeValue: ::this.onChangeValue,
                        order: `${key}.year`
                    }}
                    wrapperClass='number col-4 col-md-5 padding--0 padding--l-5'
                    name={name}
                    addToValidateForm={false}
                    value={year}
                    min={min}
                    max={now}
                    isValid={isValid}
                />
            </div>
        )
    }

    render(){

        const { now: { value, name } = {}, wrapperClass ,labelCLass, wrapperClassStart, wrapperClassEnd, label,wrapperClassCheck } = this.state,
                checked = value ? {defaultChecked: 'checked'} : {},
                classChecked = value ? "checked" : "";

        return (
            <div className={wrapperClass}>
                <div className="fl fl--wrap fl--align-c width--100">
                    {this.renderPartOfPeriod('start', wrapperClassStart)}
                    {
                        value ? null : (
                            <React.Fragment>
                                <span className="line-date show--md margin--b-10"></span>
                                {this.renderPartOfPeriod('end', wrapperClassEnd)}
                            </React.Fragment>
                        )
                    }
                </div>
                {this.errorText()}
               <div className={wrapperClassCheck}>
                    <label className={`checkbox ${labelCLass || null}`}>
                        <span
                            className={`checkbox__inner-wrapper fl--inline fl--align-c ${classChecked}`}
                        >
                            <input
                                type="checkbox"
                                name={name}
                                onChange={::this.onChangeDate}
                                {...checked}
                                className="checkbox__input"
                            />
                            <span className="checkbox__checkmark"/>
                            <span className="font--14 font--color-primary">{label}</span>
                        </span>
                    </label>
               </div>
            </div>
        )

    }

}

export default DateFromTo;
