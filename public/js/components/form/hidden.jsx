import React from 'react'
import uniqid from 'uniqid';
import { string } from 'prop-types'

class Hidden extends React.Component {

	static propTypes = {
		name: string.isRequired
	}

	static defaultProps = {

	}

	constructor(props){
		super(props);

		this.state = {
			...Hidden.defaultProps,
			...this.props
		}

	}

	componentDidMount(prevProps){

		let comparing = _.isEqual(prevProps, this.props);

		if(!comparing) {
			this.setState({...this.state, ...this.props})
		}

	}

	render(){

		return (
            <input
                type="hidden"
                value={this.state.value}
                name={this.state.name}
            />
        )
	}
}

export default Hidden;
