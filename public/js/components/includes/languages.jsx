import React from 'react';
import { connect } from 'react-redux'

// Change languages
import * as languages from '../../modules/i18n/actions/i18n.action.js'

// Configiration rules
import rules from '../../configuration/rules.json'

import Combobox from '../form/combobox.jsx';

function mapStateToProps (state) {
  return {
    ...state.i18n
  }
}

@connect(mapStateToProps, {...languages})
class Language extends React.Component {

  render(){

    let {defaultLanguage, availableLanguage, descriptionLanguage} = this.props;

    return (
        <div>
            <Combobox
                value={defaultLanguage}
                list={descriptionLanguage}
                onChange={{action: this.props.changeLanguage, name: "value"}}
                name='language'
            />
        </div>
    )

  }

}

export default Language;
