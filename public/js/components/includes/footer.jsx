import React from 'react';
import { connect } from 'react-redux'
import { object } from 'prop-types'
import Language from './languages.jsx'
import * as i18n from '../../modules/i18n/actions/i18n.action.js'

function mapStateToProps(state) {
  return {
    ...state.i18n
  }
}

@connect(mapStateToProps, { ...i18n })
class Footer extends React.Component {

	static propTypes = {
		translation: object.isRequired
	}

  render(){

    return (
      <footer className="padding--yc-30 fl--0">
        <div className="container">
          <div className="row">
            <div className="col-6 col-md-3">
              <a className="logo logo--footer" href="#">
                <img src="../../img/logo.svg" className="img-fluid" alt="logo"></img>
              </a>
              <ul className="nav-list">
                <li className="nav-list__item"><a href="#" className="link nav-list__link">{this.props.translation.aboutUs}</a></li>
                <li className="nav-list__item"><a href="#" className="link nav-list__link">{this.props.translation.terms}</a></li>
              </ul>
            </div>
            <div className="col-12 col-md-3 order-3 order-md-2">
              <ul className="nav-list">
                <li className="nav-list__item"><a href="#" className="link nav-list__link">{this.props.translation.terms}</a></li>
                <li className="nav-list__item"><a href="#" className="link nav-list__link">{this.props.translation.privacy}</a></li>
                <li className="nav-list__item"><a href="#" className="link nav-list__link">{this.props.translation.cookies}</a></li>
                <li className="nav-list__item"><a href="#" className="link nav-list__link">GDPR</a></li>
              </ul>
            </div>
            <div className="col-6 col-md-3 order-2 order-md-3">
              <ul className="nav-list margin--md-bottom-0">
                <li className="nav-list__item">
                  <a href="#" className="link nav-list__link fl fl--align-c">
                    <span className="icon icon--message icon--footer-link"></span>
                    {this.props.translation.review}
                  </a>
                </li>
                <li className="nav-list__item">
                  <a href="#" className="link nav-list__link fl fl--align-c">
                    <span className="icon icon--faq icon--footer-link"></span>
                    FAQ
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-12 col-md-3 order-4">
              <p className="margin--b-10">{this.props.translation.selectLanguage}</p>
              <div style={{maxWidth: 175 + 'px'}} className="combobox">
                <Language />
              </div>
            </div>
          </div>
        </div>
      </footer>
    )

  }

}

export default Footer;
