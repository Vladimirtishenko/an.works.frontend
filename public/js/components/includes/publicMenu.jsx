import React from 'react';
import {Link} from 'react-router-dom';
import { connect } from "react-redux";
import * as activityActions from "../../modules/profile/common/actions/activity.action.js";
import { bindActionCreators } from "redux";

function mapStateToProps(state) {
    return {
        activity: state.activity,
        i18n: state.i18n
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...activityActions
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class PublickMenu extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const {
            i18n:{
                translation
            } = {},
            activity:{
                isBurgerMenuOpen
            } = {},
            actions:{
                registrationModalOpen
            }
        } = this.props;
        return(
            <React.Fragment>
                {isBurgerMenuOpen && (
                    <div className="side-menu side-menu--padding-0">
                        <ul className="navigation">
                            <li className="navigation__item">
                                <span className="navigation__link " onClick={registrationModalOpen}>
                                    {translation.registration}
                                </span>
                            </li>
                            <li className="navigation__item">
                                <Link className="navigation__link" to={"/login"}>
                                    {translation.logIn}
                                </Link>
                            </li>
                        </ul>
                    </div>
                ) || null}
            </React.Fragment>
        )
    }
}