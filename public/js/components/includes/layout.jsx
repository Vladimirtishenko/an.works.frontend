import React from "react";

// Layout components
import Header from "./header.jsx";
import Footer from "./footerNew.jsx";

class LayoutMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="fl fl--dir-col fl--justify-b min-h-100 overflow--h">
              <Header {...this.props} /> 
                  <div className="padding--yc-30 fl--1">
      				{this.props.children}
      			  </div>
              <Footer {...this.props} />
            </div>
        );
    }
}

export default LayoutMain;
