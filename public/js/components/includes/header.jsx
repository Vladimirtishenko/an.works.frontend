import React from "react";
import { object } from "prop-types";
import { connect } from "react-redux";
import {Link} from 'react-router-dom';
// Log out
import * as oauth from "../../modules/oauth/actions/oauth.action.js";
import * as activityActions from "../../modules/profile/common/actions/activity.action.js"; 
import { bindActionCreators } from "redux";

// Language component

function mapStateToProps(state) {
    return {
        ...state.oauth,
        ...state.i18n,
        ...state.activity
    };
}

function mapDispatchToProps(dispatch) {
    return {
        ...oauth,
        actions: bindActionCreators(
            {
                ...oauth,
                ...activityActions
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
class Header extends React.Component {
    static propTypes = {
        translation: object.isRequired
    };
    static defaultProps = {
        container: 'container'
    };
    handleChangeHamburgerMenuStatus(status) {
        this.props.actions.updateBurgerMenuStatus(status);
    }

    render() {
        const { isBurgerMenuOpen, container, actions:{registrationModalOpen}} = this.props;
        return (
            <nav className={`navbar navbar--white`}>
               <div className={`${container} fl fl--align-base fl--justify-b`}>
                    <Link className="logo--navbar" to={"/"}>
                        <img src="../../img/logo.svg" className="img-fluid" alt="logo"></img>
                    </Link>
                    {this.props.isAuthenticating && (
                        <div className="fl fl--align-c">
                            {/* 
                            TODO: no mvp not delete
                            <span className="icon icon--notif icon--incoming icon--main-color font--20 margin--r-20 margin--lg-r-10"></span>
                            <div className="link font--color-blue font--12 margin--r-20 show--lg">Уведомления</div> */}
                            <div
                                className="link link--grey font--12 show--lg"
                                onClick={::this.props.actions.logout}
                            >
                                {this.props.translation.sing_out}
                            </div>
                        </div>
                        ) || (
                            <div className="fl fl--align-c">
                                <div
                                    className="link link--grey font--12 show--lg margin--r-10"
                                    onClick={registrationModalOpen}
                                >
                                    {this.props.translation.registration}
                                </div>
                                <Link
                                    className="link link--grey font--12 show--lg"
                                    to={'/login'}
                                >
                                    {this.props.translation.logIn}
                                </Link>
                            </div>
                        )
                    }
                    <span
                        className={`${
                            !isBurgerMenuOpen
                                ? "humburger"
                                : "icon icon--cross"
                        } hide--lg pointer`}
                        onClick={() =>
                            this.handleChangeHamburgerMenuStatus(
                                !isBurgerMenuOpen
                            )
                        }
                    />       
                </div>
            </nav>
        );
    }
}

export default Header;
