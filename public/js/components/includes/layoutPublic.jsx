import React from "react";
import {connect} from "react-redux";
import { bindActionCreators } from "redux";
// Layout components
import Header from "./header.jsx";
import Footer from "./footerNew.jsx";
import RegistPopup from "../../modules/profile/common/components/modal/registration.jsx";
import Notification from '../../libraries/notification/index.jsx';

import * as activityActions from "../../modules/profile/common/actions/activity.action.js"; 
import PublickMenu from "./publicMenu.jsx";

function mapStateToProps (state) {
    return {
      ...state
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...activityActions
            },
            dispatch
        ),
        dispatch
    };
}
@connect(mapStateToProps, mapDispatchToProps)
class LayoutMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {activity:{registrationModal},actions:{registrationModalClose}} = this.props;
        return (
            <div className="fl fl--dir-col fl--justify-b min-h-100 overflow--h">
                <Header {...this.props} />
                <div className="relative fl--dir-col fl fl--1">
                    {this.props.children}
                    <Notification onError={{component: null}} onSuccess={{component: null}} />
                    {registrationModal && <RegistPopup i18n={this.props.i18n} registrationModalClose={registrationModalClose} /> || null}
                    <PublickMenu/>
                </div>
                <Footer {...this.props} />
            </div>
        );
    }
}

export default LayoutMain;
