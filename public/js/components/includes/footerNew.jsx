import React from "react";
import {Link} from "react-router-dom";
import { connect } from "react-redux";
import { object } from "prop-types";
import Language from "./languages.jsx";
import * as i18n from "../../modules/i18n/actions/i18n.action.js";
import links from "../../configuration/links.json"

function mapStateToProps(state) {
    return {
        ...state.i18n
    };
}

@connect(
    mapStateToProps,
    { ...i18n }
)
class FooterNew extends React.Component {
  static propTypes = {
      translation: object.isRequired
  };

  static defaultProps = {
    container: 'container'
  }
  render(){
    let {container} = this.props;
    return (
      <footer className="padding--yc-30 print_disable">
        <div className={`${container}`}>
          <div className="row margin--b-20">
            <div className="col-md-3 margin--b-35 fl fl--justify-c fl--justify-md-st">
              <a className="logo logo--footer" href="#">
                <img src="../../img/logo.svg" className="img-fluid" alt="logo"></img>
              </a>
            </div>
            <div className="col-md-3 col-xl-4 margin--b-20 padding--xl-l-30">
              <ul className="list--style-type-none fl fl--dir-col fl--align-st">
                <li className="margin--b-10">
                    <a href={links.about} className="link font--color-secondary" target="_blanck">{this.props.translation.aboutUs}</a>
                </li>
                <li className="margin--b-10">
                    <a href={links.termsOfUse} className="link font--color-secondary" target="_blanck">{this.props.translation.terms}</a>
                </li>
              </ul>
            </div>
            <div className="col-md-3 margin--b-20 padding--md-0">
              <ul className="list--style-type-none fl fl--dir-col fl--align-st margin--md-bottom-0">
                <li className="margin--b-10">
                {/* {
                  localStorage.getItem('authToken') && (
                    <Link to="/contact" className="link font--color-secondary pointer fl fl--align-c">
                      <span className="icon icon--message font--18 font--color-blue padding--r-10 "></span>
                        {this.props.translation.contactUse}
                    </Link>
                  ) || (
                    <a href="#" className="link font--color-secondary pointer fl fl--align-c">
                      <span className="icon icon--message font--18 font--color-blue padding--r-10 "></span>
                        {this.props.translation.contactUse}
                    </a>
                  )
                } */}
                  <a href="#" target="_blanck" className="link font--color-secondary pointer fl fl--align-c">
                    <span className="icon icon--message font--18 font--color-blue padding--r-10 "></span>
                      {this.props.translation.contactUse}
                  </a>
                </li>
                <li className="margin--b-10">
                  <a href={links.faq} target="_blanck" className="link font--color-secondary pointer fl fl--align-c">
                    <span className="icon icon--faq font--18 font--color-blue padding--r-10"></span>
                    {this.props.translation.faq}
                  </a>
                </li>
              </ul>
            </div>
            {/* <div className="col-md-3 col-xl-2 padding--md-l-0">
              <p className="margin--b-10 l-h--1">{this.props.translation.selectLanguage}</p>
              <div className="combobox">
                <Language />
              </div>
            </div> */}
          </div>
          <div className="row fl--justify-b fl--align-c border--t-1-gray-block padding--t-25">
            <ul className="fl fl--justify-md-end fl--justify-c col-md-6 list--style-type-none order-md-1 fl margin--b-20 margin--md-0">
                <li className="margin--r-25">
                    <a target="_blanck" href={links.instagram}><span className="icon--instagram-footer icon font--20 icon--social "></span></a>
                </li>
                <li className="margin--r-25">
                    <a target="_blanck" href={links.telegram}><span className="icon--Telegram-footer icon font--20 icon--social "></span></a>
                </li>
                <li className="margin--r-25">
                    <a target="_blanck" href={links.facebook}><span className="icon--facebook-footer icon font--20 icon--social "></span></a>
                </li>
                <li>
                    <a target="_blanck" href={links.linkedin}><span className="icon--linkedin-footer icon font--20 icon--social "></span></a>
                </li>
            </ul>
          </div>
        </div>
      </footer>
    )

  }

}

export default FooterNew;
