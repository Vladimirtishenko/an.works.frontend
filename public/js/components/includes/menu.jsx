import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

// Log out
import * as activityActions from "../../modules/profile/common/actions/activity.action.js";
import { bindActionCreators } from "redux";

// Language component

function mapStateToProps(state) {
    return {
        ...state.activity
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...activityActions
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Menu extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            close: "navigation--collapsed-close",
            open: "navigation--collapsed-open"
        };
    }

    handleClickLink() {
        const { actions: { updateBurgerMenuStatus } } = this.props;
        updateBurgerMenuStatus(false);
    }

    changeCollapsedState(event) {
        if (!event && !event.target) return;

        const { actions: { collapsedMenu }, collapsed } = this.props;
        collapsedMenu(!collapsed);
    }

    renderDeepList(item, step, child) {

        const { collapsed } = this.props,
                className = collapsed ? "navigation--collapsed-open" : "navigation--collapsed-close";

        return (
            <ul
                className={`navigation navigation__deepList ${
                    className
                }`}
            >
                <Menu data={item.children} step={step} child={child} />
            </ul>
        );
    }

    renderListItems(item, i, className, step) {
        const { isBurgerMenuOpen } = this.props;

        return (
            <li
                className={`navigation__item ${
                    item.separate ? "navigation__item--separate " : ""
                }`}
                key={i + Math.random()}
                onClick={isBurgerMenuOpen ? e => this.handleClickLink(e) : null}
            >
                {(item.path && !item.step) || item.step <= step ? (
                    <Link
                        to={item.path}
                        className={`navigation__link  nowrap--xl ${className}`}
                    >
                        {item.name}
                    </Link>
                ) : item.step >= step ? (
                    <span
                        className={`navigation__link   navigation__link--hidden ${className}`}
                    >
                        {item.name}
                    </span>
                ) : item.hide ? (
                    <span className="navigation__link  navigation__link--hidden">
                        {item.name}
                    </span>
                ) : item.children ? (
                    <span
                        onClick={::this.changeCollapsedState}
                        className="navigation__link "
                    >
                        {item.name}
                    </span>
                ) : (
                    <span className="navigation__link">{item.name}</span>
                )}
                {item.children && ::this.renderDeepList(item, step, true)}
            </li>
        );
    }

    setCircle(data, step, url) {
        return data.map((item, i) => {
            let className =
                item.path && url.indexOf(item.path.slice(1)) == 0
                    ? "navigation__link--active"
                    : "";

            return ::this.renderListItems(item, i, className, step);
        });
    }

    render() {
        const url = location.pathname.slice(1).split("/");
        let { data, step, child, main } = this.props;

        const { isBurgerMenuOpen, translation = {}, logout } = this.props;

        return (
            <React.Fragment>
                {(main && (
                    <ul className={`navigation`}>
                        {::this.setCircle(data, step, url)}
                        <li className="navigation__item navigation__item--last-item hide--lg">
                            <span
                                className="navigation__link  nowrap--xl"
                                onClick={logout}
                            >
                                {translation.sing_out || null}
                            </span>
                        </li>
                    </ul>
                )) ||
                    ::this.setCircle(data, step, url)}
            </React.Fragment>
        );
    }
}
