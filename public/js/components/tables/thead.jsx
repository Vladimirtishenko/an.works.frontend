import React from 'react'
import uniqid from 'uniqid';
import { string, bool, array } from 'prop-types'
class Thead extends React.Component {

    static defaultProps = {
        theadClass: "respons-flex-table__thead respons-flex-table__thead--admin-theme",
        theadTrClass: "respons-flex-table__tr respons-flex-table__tr--admin-theme"

	}
    

	render () {

		let {translation, defaultLanguage} = this.props.i18n,
            {fields, actions} = this.props.schema,
            {theadClass,theadTrClass} = this.props,
            cells = [],
            classNameItem = null;
		_.forIn(fields, (field, key) => {
            cells.push (
                <th key={uniqid()} className={`respons-flex-table__th ${field.classNameTh}`}>{field.name}</th> //translation[field.name]
            )

        })

		cells.push (

			<td key={uniqid()} className={`respons-flex-table__td`}></td>
 
		)
        return (
            
            <thead className={theadClass}>
                <tr className={theadTrClass}>
                    {cells}
                </tr>
            </thead>

        )
	}

}

export default Thead;
