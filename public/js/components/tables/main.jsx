import React from 'react'
import Tbody from './tbody.jsx';
import Thead from './thead.jsx';

class Table extends React.Component {

    render () {

        return (
                <table className="respons-flex-table box--white">
                    <Thead i18n={this.props.i18n} schema={this.props.schema} />
                    <Tbody i18n={this.props.i18n} data={this.props.data} schema={this.props.schema} actions={this.props.actions} />
                </table>
        );
    }

}


export default Table;
