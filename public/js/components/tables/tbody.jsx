import React from 'react';
import {Link} from 'react-router-dom';
import Actions from './actions.jsx'
import uniqid from 'uniqid';


class Cell extends React.Component {

    render () {
        let usersTemplate = this.props.data.map((item, i) => {
            return (
                <tr key={i} data-id={item.id} className="respons-flex-table__tr">
                    {this.template(item)}
                </tr>
            );
        });
        return (
            <tbody className="respons-flex-table__tbody">
               {usersTemplate}
            </tbody>
        );
    }

    recursionSearch(obj, need){

        if(!obj) return false;

        let search = null;

        _.cloneDeepWith(obj, function(item) {
            if(item[need]){
              search = item[need];
            }
        });

        return search;

    }

    updateFields(rules, key, sorted){
        let lang = this.props.language ? langsHelper(this.props.language)['label'] : null,
            value = sorted[key],
            values = null;

        if(rules.decorateFunction && typeof rules.decorateFunction === 'function')
        {
            value = rules.decorateFunction(value);
        }
        switch(rules.type){
            case "String":
                values = value || '-';
                break;
            case "Number":
                values = typeof value !== "undefined" && value !== null ? value : '-';
                break;
            case "Image":
                values = value ? `<img src="${server.origin + '/' + value}" />` : '-';
                break;
            case "Object":
                if(!key) return '-';
                values = value ? (this.recursionSearch(value[lang], key) || "-") : "-";
                break;
            case "Array":
                values = value && value.length && value.join(", ") || '-';
                break;
            default:
                values = null;
        }
        return values && <td key={uniqid(key + '-')} className={`respons-flex-table__td ${rules.classNameTd}`}  dangerouslySetInnerHTML={{__html: values}}></td> || null

    }

    template (item) {
        let idKey = this.props.schema.entityIdKey || null;
        let entitySubKey = this.props.schema.entitySubKey || null
        let {fields, actions} = this.props.schema,
            sorted = _.pick(item, _.keys(fields)),
            id = item[idKey] || item._id || item.label || item.postId,
            cells = [],
            subId = item[entitySubKey] || '',
            field;

        _.forIn(fields, (rules, key) => {
            cells.push( ::this.updateFields(rules, rules.name || null, item) );

        })

        cells.push(<Actions key={uniqid(id + '-')} id={encodeURIComponent(id)} subid={subId} {...this.props} />)

        return cells;

    }

}

export default Cell;
