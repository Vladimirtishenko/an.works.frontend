import React from 'react';
import {Link} from 'react-router-dom';

import {checkUrl, checkEvent} from '../../helpers/regexEvent.helper.js';

class Actions extends React.Component {

    render () {
        let {actions} = this.props.schema,
            keys = Object.keys(actions),
            id = this.props.id,
            subId = this.props.subid,
            actionToReturn;

        if(keys){
            actionToReturn = keys.map((key, i) => {

                let url = checkUrl(actions[key], id, subId),
                    event = checkEvent(actions[key], this.props.actions);

                    return (
                        <Link key={i} to={url} className="table__action">
                            <i data-id={id} {...event} data-art={JSON.stringify(event)} className="">{key}</i>
                        </Link>
                    );
            })

        }

        return (
            <td className="respons-flex-table__td respons-flex-table--w-10">
                {actionToReturn}
            </td>
        );

    }


}

export default Actions;
