import React from "react";

export const Circle = props => {
    const { size = 50, progress = 0, circleWidth } = props;
    const center = size / 2;
    const strokeWidth = circleWidth ? circleWidth : size * 0.15;
    const radius = size / 2 - strokeWidth / 2;
    const circumference = 2 * Math.PI * radius;
    const offset = ((100 - progress) / 100) * circumference;

    const style = {
        strokeDashoffset: offset
    };

    return (
        <svg
            className="circle"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
            <circle
                className="circle__background"
                cx={center}
                cy={center}
                r={radius}
                strokeWidth={strokeWidth}
            />
            <circle
                className="circle__fill"
                style={style}
                cx={center}
                cy={center}
                r={radius}
                strokeWidth={strokeWidth}
                strokeDasharray={circumference}
            />
        </svg>
    );
};
