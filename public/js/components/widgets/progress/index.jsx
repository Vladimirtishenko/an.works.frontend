import React from "react";
import { Circle } from "./circle.jsx";

export class ProgressCircle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            value: props.value,
            size: props.size,
            circleWidth: props.circleWidth
        };

        this.progress = this.progress.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            const delta = this.state.progress + 1;
            this.progress(delta);

            if (delta >= this.state.value) {
                clearInterval(this.interval);
            }
        }, 10);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    progress(value = 0) {
        this.setState({
            progress: value % (100 + 1)
        });
    }

    handleChange(event) {
        this.progress(event.target.value);
    }

    render() {
        const { progress = 0 } = this.state;
        const { contentClass } = this.props;

        return (
            <div className="progress-circle">
                <div className="progress-circle__modal">
                    <Circle {...this.state} />
                    <div className="progress-circle__percent">
                        <div
                            className={`progress-circle__content ${contentClass}`}
                        >
                            {progress}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
