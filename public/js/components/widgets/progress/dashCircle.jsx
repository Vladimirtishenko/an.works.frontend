import React from "react";

export const DashCircle = props => {
    const { size = 50, circleWidth } = props;
    const center = size / 2;
    const strokeWidth = circleWidth ? circleWidth : size * 0.15;
    const radius = size / 2 - strokeWidth / 2;

    return (
        <svg
            className="dash-circle"
            width={size}
            height={size}
            viewBox={`0 0 ${size} ${size}`}
        >
            <circle
                className="dash-circle__circle"
                stroke="white"
                fill="transparent"
                r={radius}
                cx={center}
                cy={center}
                strokeWidth={strokeWidth}
            />
        </svg>
    );
};
