import React from "react";
import { Circle } from "./circle.jsx";
import { DashCircle } from "./dashCircle.jsx";
import language from "../../../databases/languages/languageSkills.json";



export class LanguageCircle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            value: this.convertValue(props.value),
            size: props.size,
            circleWidth: props.circleWidth
        };

        this.progress = this.progress.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            const delta = this.state.progress + 1;
            this.progress(delta);

            if (delta >= this.state.value) {
                clearInterval(this.interval);
            }
        }, 10);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    convertValue(val) {
        return ((val * 100) / Number(language.length - 1)).toFixed(0);
    }

    convertValueToText(val) {
       const lvl = language.map((index,i) =>{
            return index.name;
       });

        return lvl[val];
    }

    progress(value = 0) {
        this.setState({
            progress: value % (100 + 1)
        });
    }

    handleChange(event) {
        this.progress(event.target.value);
    }

    render() {
        const { className, languageName, value, contentClass } = this.props,
        name = this.convertValueToText(value);

        return (
            <div className={`progress-circle ${className}`}>
                <div className="progress-circle__modal">
                    <Circle {...this.state} />
                    <DashCircle {...this.state} />
                    <div className="progress-circle__percent">
                        <div
                            className={`progress-circle__content ${contentClass}`}
                        >
                            <span className="font--14 margin--b-2">
                                {languageName}
                            </span>
                            <span className="font--10 font--color-inactive max-width--80 font--center">
                                {name}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
