import React from 'react'

class ToolTip extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props,
            toolTipMain:{
                zIndex: 1,
                visibility: 'hidden',
            },
            flag:{
                zIndex: 1
            }
        };
        this.__refs = null; 
    }

    componentDidMount() {
		document.addEventListener('touchstart', this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('touchstart', this.handleClickOutside);
    }
    handleClickOutside = (event) =>{
        if(!event.targetTouches[0]) return;
		if (this.__refs && !this.__refs.contains(event.target)) {
			this.toolTipClose(event);
		}else{
            this.toolTipOpen(event);
        }
    }

    render() {
        const {flag,toolTipMain} = this.state;
        return (
            <div className={`tooltip fl fl--justify-end fl--justify-md-st ${this.props.wrapper}`}>
                <div className="pseudo-icon__info relative  margin--t-2" ref={(refs) => { this.__refs = refs; }} onMouseOver={::this.toolTipOpen} onMouseOut={::this.toolTipClose}>
                { this.props.description && 
                    <div className="tooltip-main" style={toolTipMain}>
                        <div className="tooltip-main__wrap">
                            {/* <div className="tooltip-main__caption">{this.props.caption || null}</div> */}
                            <div className="tooltip-main__description">{this.props.description || null}</div>
                            <div className="tooltip-main__flag" style={flag}></div>
                        </div>
                        </div>
                        || null 
                    }
                </div>
            </div>
      )
    }


    flagPosition(tooltipRect,cardRect,toolTipMain){
            let obj = {};

            if((tooltipRect.left - cardRect.left + (tooltipRect.width / 2)) >  (toolTipMain.width / 2) &&
                (cardRect.right - tooltipRect.right + (tooltipRect.width / 2)) > (toolTipMain.width / 2)){
                obj.left = 'calc(50% - 7.5px)';
            }else{
                obj.left = toolTipMain.width - ((cardRect.right - tooltipRect.right) + (tooltipRect.width / 2) + 7.5);
            }
            if(tooltipRect.top - cardRect.top > toolTipMain.height + 16 && tooltipRect.top > toolTipMain.height  + 16){
                obj.bottom = '-10px';
                obj.top = 'auto';
            }else{
                obj.transform = 'rotate(180deg)';
                obj.bottom = 'auto';
                obj.top = '-10px';
            }
            return obj;
    }

    positioToolTipMainLeft(tooltipRect,cardRect,toolTipMain){

        let left;
        
        if(((tooltipRect.left - cardRect.left + (tooltipRect.width / 2)) >  (toolTipMain.width / 2)) &&  
            (( cardRect.right - tooltipRect.right + (tooltipRect.width / 2)) > ( toolTipMain.width / 2))){
            left = (-(toolTipMain.width / 2) + (tooltipRect.width / 2));
        }
        if( ((tooltipRect.left - cardRect.left + (tooltipRect.width / 2)) >  (toolTipMain.width / 2)) && 
            (( cardRect.right - tooltipRect.right + (tooltipRect.width / 2)) < ( toolTipMain.width / 2))){
            left = (-(toolTipMain.width - (tooltipRect.width + (cardRect.right - tooltipRect.right))));
        }
        if( ((tooltipRect.left - cardRect.left + (tooltipRect.width / 2)) <  (toolTipMain.width / 2)) &&  
            (( cardRect.right - tooltipRect.right + (tooltipRect.width / 2)) > ( toolTipMain.width / 2))){
            left = (cardRect.left - tooltipRect.left);
        }
        return left;
    }

    positioToolTipMainTop(tooltipRect,cardRect,toolTipMain){
        let top;
        
        if(tooltipRect.top - cardRect.top > toolTipMain.height + 16 && tooltipRect.top > toolTipMain.height  + 16){
            top = -(toolTipMain.height  + 16);
        }else{ 
            top = (tooltipRect.height  + 16);
        }
        return top;
    }

    toolTipOpen(event){
        if(!event.target.children[0]) return;

        let tooltipRect = event.target.getBoundingClientRect(); 
        let cardRect = event.target.closest('.card') && event.target.closest('.card').getBoundingClientRect() || {left:0,width:0};
        let toolTipMain =  event.target.children[0].getBoundingClientRect();

        let positionLeftMain = this.positioToolTipMainLeft(tooltipRect,cardRect,toolTipMain);
        let positionTopMain = this.positioToolTipMainTop(tooltipRect,cardRect,toolTipMain);
    
        let flag = this.flagPosition(tooltipRect,cardRect,toolTipMain);

        this.setState({
            toolTipMain:{
                zIndex: '1000',
                visibility: 'visible',
                left: positionLeftMain,
                top: positionTopMain,
                position: 'absolute'
            },
            now:{
                visibility: 'visible'
            },
            flag:flag
        })

    }
    toolTipClose(event){
        if(!event.target.children[0]) return;
        this.setState({  
            toolTipMain:{
                zIndex: '-1',
                visibility: 'hidden',
                position: 'fixed'
            }
        }) 
    }
}

export default ToolTip;