import React from 'react'

class Modal extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			show: true
		}
	}

	hide(){
		this.setState({show:false})
	}

	render(){
		return (
			<div>
				{
					this.state.show &&
						<div className="modal fl fl--justify-c fl--align-c">
							<div className="modal__content">
								<span className="modal__close" onClick={this.props.onCancel || ::this.hide}>
									<i className="material-icons">close</i>
								</span>
								{this.props.children}
							</div>
						</div> || null
				}
			</div>
		)
	}
}

export default Modal;
