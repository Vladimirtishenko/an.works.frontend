import React from 'react'

 const loaderAnim = ()=>{
    return(
        <div className="loader-fading-circle">
            <div className="loader-circle loader-circle-1"></div>
            <div className="loader-circle loader-circle-2"></div>
            <div className="loader-circle loader-circle-3"></div>
            <div className="loader-circle loader-circle-4"></div>
            <div className="loader-circle loader-circle-5"></div>
            <div className="loader-circle loader-circle-6"></div>
            <div className="loader-circle loader-circle-7"></div>
            <div className="loader-circle loader-circle-8"></div>
            <div className="loader-circle loader-circle-9"></div>
            <div className="loader-circle loader-circle-10"></div>
            <div className="loader-circle loader-circle-11"></div>
            <div className="loader-circle loader-circle-12"></div>
        </div>
    )
};
export default loaderAnim