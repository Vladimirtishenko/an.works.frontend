import React from 'react';

const Loader = (props) => {
    const { error, timedOut, pastDelay, retry } = props;
    if (error) {
        const { message, stack } = error;
        return  (<div>
                    Error!
                    <button onClick={ retry }>Retry</button>
                    <p>{message}</p>
                    <code>{stack}</code>
                </div>)
    } else if (timedOut) {
        return <div>Taking a long time... <button onClick={ retry }>Retry</button></div>;
    } else if (pastDelay) {
        return (<div className="cssload-loader">
                    <div className="cssload-inner cssload-one"></div>
                    <div className="cssload-inner cssload-two"></div>
                    <div className="cssload-inner cssload-three"></div>
                </div>)
    } else {
        return null;
    }
}
export default Loader;
