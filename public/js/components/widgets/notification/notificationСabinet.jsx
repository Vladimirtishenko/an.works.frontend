import React from 'react';

export const NotificationСabinet = (props = {}) => (
    
    <div className="info info--shadow info--rounded bg--white margin--b-20">
        <div className="fl">
            <div className={`info__icon info__icon--sm info__icon--bordered info__icon--${props.type}`}>
                <span className="icon icon--alarm" />
            </div>
            <div className="info__content padding--20 font--color-secondary">
                {props.children}
            </div>
        </div>
    </div>

);
