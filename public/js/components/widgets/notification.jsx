import React from 'react'

class  Notification extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			show: true
		}
	}

	hide(){
		this.setState({show:false})
	}

	render(){
		return (
			<div className="box--rounded shadow--table max-width--300">
				{this.state.show &&
						<div className="fl fl--justify-c fl--align-c fl--dir-col">
							<div className="">
								{this.props.children}
							</div>
							<div className="padding--yc-15 font--center border--t-1-gray-notif width--100"> 
								<span className="font--color-blue font--14 pointer">Смотреть всё</span>
							</div>
						</div>
                	|| null}
			</div>
		)
	}
}

export default Notification;
