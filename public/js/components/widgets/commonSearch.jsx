import React from "react";

export const CommonSearch = props => (
    <div className={`commonSearch ${props.wrapperClass}`}>
        <input
            type="search"
            className="commonSearch__input"
            placeholder="Search"
        />
        <span className="commonSearch__icon icon icon--search" />
    </div>
);
