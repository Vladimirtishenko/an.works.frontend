import React from "react";
import {Link} from 'react-router-dom';

import Header from "../includes/header.jsx";
import Footer from "../includes/footerNew.jsx";
import Combobox from "../form/combobox.jsx";
import Textarea from "../form/textarea.jsx";
import errorMessage from "../../databases/errorMessage/ru/errorMessage";
import Text from "../form/text.jsx";
import selectMessageSubject from "../../databases/selectMessageSubject/selectMessageSubject.json"
import { connect } from "react-redux";
import * as i18n from "../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n,
        oauth: state.oauth
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class ContactUser extends React.Component {

    onChangeValue(event) {
        this.setState({value: event.target.value})
    }
    render() {
        const {
            i18n:{
                translation = {}
            } = {},
            oauth:{
                user:{
                    email = ''
                } = {}
            } = {}
        } = this.props;
        return (
            <div className="fl fl--dir-col fl--justify-b height--100">
                <Header/>
                <div className="fl fl--align-c fl--1 box--white">
                    <div className="container padding--yc-30">
                        <div className="col-12 col-xl-10 margin--auto-0 padding--0 padding--lg-xc-15">
                            <h1 className=" font--color-blue font--uppercase col-12 margin--b-35 font--color-blue font--500 font--18 padding--0" >{translation.contactUse}</h1>
                            <form  className=" form fl fl--wrap margin--b-50" action="" >
                                <div className="col-12 col-lg-6 padding--0 padding--md-r-0 margin--b-20">
                                    <div className="margin--b-10">
                                        <Text                                    
                                            placeholder="mymail@mail.com"
                                            validators={["required"]}
                                            errorMessages={[ translation.errorMessageRquired]}
                                            wrapperClass="row fl--align-c margin--b-10 margin--0 width--100"
                                            labelClass="label form__label font--14 col-md-6 padding--l-0"
                                            inputWrapperClass="col-md-6 padding--0"
                                            inputClass="input form__input"
                                            value={email}
                                            label={translation.mail+ '*'}
                                            name="email"
                                        />
                                    </div>
                                    <div className="margin--b-20">
                                        <Combobox
                                            validators={["combobox"]}
                                            errorMessages={[errorMessage.combobox]}
                                            name="Тема"
                                            wrapperClass="row fl--align-c combobox margin--0 width--100"
                                            labelClass="label form__label font--14 col-md-6 padding--md-l-0"
                                            className="padding--0 col-md-6"
                                            label="Тема сообщения*"
                                            list={selectMessageSubject}
                                            placeholder={translation.selectMessageSubject}
                                            value=""
                                        />
                                    </div>
                                    <div className="fl fl--align-c fl--justify-b margin--b-30">
                                        <label className="fl--wrap font--14 fl fl--justify-b fl--align-c min-width--100-p" htmlFor="add-file" id="lable-file">
                                            <span className="icon--file font--color-secondary margin--b-10 margin--lg-0">
                                                {translation.addFile}
                                            </span>
                                            <input style={{display: 'none'}} type="file"  id="add-file" />
                                            <div className="btn btn--file margin--auto-l">
                                                <span className="teble--middle">{translation.chooseFile}</span>
                                            </div>
                                        </label>
                                    </div>
                                    <p className="font--14 font--color-secondary">{translation.answerToEmailAddress}</p>
                                </div>
                                <div className="col-12 col-lg-6 padding--0 padding--lg-l-15 margin--lg-b-30">
                                    <Textarea
                                        rows={6}
                                        placeholder="Сообщение"
                                        validators={["maxStringLength:500","minStringLength:20"]}
                                        errorMessages={[translation.errorMessageMaxLeng500,translation.errorMessageMinLeng20]}
                                        name="massage"
                                        classes="row no-gutters margin--b-20" 
                                        textAreaWrapper="col-12"
                                        textAreaClass="form__text-area"
                                    />
                                    <div className="fl">
                                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--auto-0 margin--lg-0 margin--lg-l-auto">{translation.send}</button>
                                    </div>
                                </div>
                            </form>
                            <div className="fl margin-minus-xc-15 margin--b-50">
                                <div className="col-3 col-sm-3 col-md-5 fl fl--align-c padding--0">
                                    <div className="hr--1-light-gray"></div>
                                </div>
                                <div className="col-6 col-sm-6 col-md-2">
                                    <ul className="social fl fl--justify-b ">
                                        <li className="social__item">
                                            <a className="social__link font--color-primary font--22 icon--telegram" href="" > </a>
                                        </li>
                                        <li className="social__item">
                                            <a className="social__link font--color-primary font--22 icon--viber" href="" > </a>
                                        </li>
                                        <li className="social__item">
                                            <a className="social__link font--color-primary font--22 icon--skype" href="" > </a>
                                        </li>
                                        <li className="social__item">
                                            <a className="social__link font--color-primary font--22 icon--linkedIn" href="" > </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-3 col-sm-3 col-md-5 fl fl--align-c padding--0">
                                    <div className="hr--1-light-gray"></div>
                                </div>
                            </div>
                            <div className="col-lg-11 fl fl--wrap margin--auto-0">
                                <div className="col-sm-12 col-md-6 margin--b-20">
                                    <address className="row margin--b-10 font--14 font--center font--md-left font--normal" >
                                        <div className=" col-md-5 col-lg-5 font--500 font--color-primary">{translation.address}:</div>
                                        <ul className="col-md-7 col-lg-7 padding--lg-r-15 font--14">
                                            <li className="width--100 display--b-l margin--b-5 font--color-secondary">{translation.index},</li>
                                            <li className="width--100 display--b-l margin--b-5 font--color-secondary">{translation.city_kh},</li>
                                            <li className="width--100 display--b-l margin--b-5 font--color-secondary">{translation.office_location}</li>
                                        </ul>
                                    </address>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <div className="row margin--b-10 font--14 font--center font--md-left">
                                        <div className="col-md-5 col-lg-5 font--500 font--color-primary">{translation.phone}:</div>
                                        <ul className="col-md-7 col-lg-7 padding--lg-r-15 font--14"> 
                                            <li className="width--100 display--b-l margin--b-5 font--color-secondary"> +38 (073) 9118181 </li>
                                            <li className="width--100 display--b-l margin--b-5 font--color-secondary"> +38 (066) 9118181 </li>
                                            <li className="width--100 display--b-l font--color-secondary"> +38 (097) 9118181 </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default ContactUser;