import React from "react";

class FilterBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            ...props
        };
    }

    handleOpenFilter() {
        this.setState({
            ...this.state,
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    render() {
        const { title, titleSvg, children } = this.props;
        const { isOpen } = this.state;
        const arrowRotateClass = isOpen ? "rotate--90" : "rotate--minus-90";
        const bodyShowClass = isOpen ? "show--block" : "show--none";

        return (
            <div className="border--b-2-grey">
                <div
                    className="padding--yc-25 padding--xc-30 bg--white pointer-text "
                    onClick={() => this.handleOpenFilter()}
                >
                    <div className="fl fl--justify-b fl--align-c">
                        <h3 className="font--16 font--bold fl fl--align-c">
                            {title} {titleSvg && titleSvg}
                        </h3>
                        <span
                            className={`icon icon--Rating_arrow ${arrowRotateClass}`}
                        />
                    </div>
                </div>
                <div
                    className={`padding--b-25 padding--xc-30 font--14 font--bold ${bodyShowClass}`}
                >
                    {children}
                </div>
            </div>
        );
    }
}

export default FilterBlock;
