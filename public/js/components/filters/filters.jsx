import React from "react";
import { connect } from "react-redux";
import "react-accessible-accordion/dist/fancy-example.css";
import {isMobileOnly} from "react-device-detect";

// Filters Components
import TechnologyFilter from "./spreadFilters/technology.flt.jsx";
import LevelFilter from "./spreadFilters/level.flt.jsx";
import TestsFilter from "./spreadFilters/tests.flt.jsx";
import SkillsFilter from "./spreadFilters/skills.flt.jsx";
import LanguagesFilter from "./spreadFilters/languages.flt.jsx";
import SallaryFilter from "./spreadFilters/sallary.flt.jsx";
import OccupationFilter from "./spreadFilters/occupation.flt.jsx";
import LocationFilter from "./spreadFilters/location.flt.jsx";

import ItExperienceFilter from "./spreadFilters/itExperience.flt.jsx";
import ExperienceFilter from "./spreadFilters/experience.flt.jsx";

import EducationFilter from "./spreadFilters/education.flt.jsx";
import AdditionalEducationFilter from "./spreadFilters/additionalEducation.flt.jsx";

import GenderFilter from "./spreadFilters/gender.flt.jsx";
import AgesFilter from "./spreadFilters/age.flt.jsx";
import DaysLeftFilter from "./spreadFilters/daysLeft.flt.jsx";
import OpensFilter from "./spreadFilters/opens.flt.jsx";

import { DeleteButton } from "../form/deleteButton.jsx";

import {
    clearAll
} from '../../helpers/filters/clear.helper.js';

import {
    mergeGeneralFilters,
    normalizeFilterToSending, 
    destuctingFiltersToComponent
} from '../../helpers/filters/filter.helper.js'; 

import {
    normalizeTestingToSending,
    destuctingTestsToComponent
} from '../../helpers/filters/testing.helper.js';

import * as i18n from "../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class FiltersComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };

        this.staticTestResult = {}
        this.staticFilters = {}
    }

    componentDidMount(){
        if(isMobileOnly){
            this.setState({
                ...this.state,
                isOpen: false
            })
        }
    }
    handleOpen() {
        this.setState({
            ...this.state,
            isOpen: !this.state.isOpen
        });
    }

    onSubmitFilters() {
        const {
                filters,
                actions: { getQueue }
            } = this.props,
            toNormalizeFilter = normalizeFilterToSending(this.staticFilters),
            toNormalizeTesting = normalizeTestingToSending(this.staticTestResult),
            mergedFilters = mergeGeneralFilters(filters, {
                    filter: toNormalizeFilter,
                    testResults: toNormalizeTesting
                });

        getQueue(null, mergedFilters);

    }


    appendFilterToObject(name, value, object) {

        if(name && value){
            object[name] = value;
        }

        if(name && !Object.keys(value).length){
            delete object[name];
        }

    }

    createNewSingleFilterObject(name, value){
        this.appendFilterToObject(name, value, this.staticFilters)
    }

    createNewSingleResultForTest(name, value) {
        this.appendFilterToObject(name, value, this.staticTestResult)
    }

    onRemoveAll(){

        const {
                filters,
                actions: {getQueue}
              }  = this.props,
              newFilters = clearAll(filters);

        getQueue(null, newFilters);

    }

    render() {
        const { isOpen } = this.state,
              {
                filters,
                knowledges,
                mainSkill,
                testHierarchy: {tests = []} = {},
                i18n:{translation = {}} = {}
              } = this.props,
              bodyOpenClass = isOpen ? "show--block" : "show--none",
              filterDenormalized = destuctingFiltersToComponent(filters),
              testsDenormalized = destuctingTestsToComponent(filters);

        this.staticFilters = filterDenormalized;
        this.staticTestResult = testsDenormalized;

        const {
            age,
            location,
            daysLeft,
            education,
            experience,
            experienceIt,
            gender,
            level,
            occupation,
            opens,
            sallary,
            technology,
            skill,
            languages
        } = this.staticFilters,
        {
            testing
        } = this.staticTestResult;
        
        return (
            <div className="box--white box--rounded margin--b-15">
                <div
                    className="relative fl fl--wrap fl--justify-b fl--align-c padding--yc-15 padding--xc-30 shadow--small"
                >
                    <span className="font--uppercase font--14 font--bold pointer-text"
                        onClick={::this.handleOpen}
                    >
                        <span className="icon icon--filter icon--filter-custom font--12 font--color-blue" />
                        {translation.filter}
                    </span>
                    <DeleteButton 
                        className="font--12 padding--l-15"
                        onClick={::this.onRemoveAll}
                        text={translation.cleanOut ? translation.cleanOut : ' '}
                    />
                </div>
                <div className={`${bodyOpenClass}`}>

                    <LocationFilter 
                        value={location}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <TechnologyFilter 
                        mainSkill={mainSkill}
                        knowledges={knowledges}
                        value={technology}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <LevelFilter
                        value={level}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <TestsFilter
                        tests={tests}
                        value={testing}
                        onChange={::this.createNewSingleResultForTest}
                    />

                    <SkillsFilter
                        filters={this.staticFilters}
                        knowledges={knowledges}
                        value={skill}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <LanguagesFilter
                        value={languages}
                        knowledges={knowledges}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <SallaryFilter 
                        value={sallary}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    {/* 
                    TODO: NOT MVP
                    <OccupationFilter
                        value={occupation}
                        onChange={::this.createNewSingleFilterObject}
                    /> */}

                    <ItExperienceFilter
                        value={experienceIt}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <ExperienceFilter
                        value={experience}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <EducationFilter
                        value={education}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <GenderFilter
                        value={gender}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <AgesFilter
                        value={age}
                        onChange={::this.createNewSingleFilterObject}
                    />

                    <DaysLeftFilter
                        value={daysLeft}
                        onChange={::this.createNewSingleFilterObject}
                    />
                    {/* 
                    TODO: NOT MVP
                    <OpensFilter
                        value={opens}
                        onChange={::this.createNewSingleFilterObject}
                    /> */}

                    <div className="fl fl--justify-end fl--align-c padding--yc-15 padding--xc-30 shadow--small">
                        <DeleteButton
                            className="font--12 padding--l-15"
                            onClick={::this.onRemoveAll}
                            text={translation.cleanOut ? translation.cleanOut : ' '}
                        />
                    </div>
                    <button onClick={::this.onSubmitFilters} className="btn btn--primary padding--yc-15 padding--xc-30   shadow--small width--100 pointer--text sticky b--0">
                        {translation.applyFilters}
                    </button>
                </div>
            </div>
        );
    }
}
