import React from "react";

import top from "../../databases/general/top.json";

import {
    normalizeLimit,
    convertLimitToValue
} from '../../helpers/filters/limit.helper.js'

import {
    getOneOf,
    changeOneOf,
    deteleOneOf
} from '../../helpers/filters/filter.helper.js'

import LimitFilter from './publicFilters/limit.jsx'

import { connect } from "react-redux";
import * as i18n from "../../modules/i18n/actions/i18n.action.js";
function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class TableFilters extends React.Component {
    constructor(props) {
        super(props);
    }

    changeLimit(label){

        const { filters, actions: { getQueue } } = this.props,
                newFiltersWithLimit = normalizeLimit(filters, label);

        getQueue(null, newFiltersWithLimit);

    }

    changeTop(name, value){

        const { filters, actions: {getQueue} } = this.props,
                newFiltersWithTop = changeOneOf(filters, {[name]: value}),
                normolizeFiltersWithTop = deteleOneOf(newFiltersWithTop,'offset');
        if(value == 0){
            getQueue(); 
            return;
        }
        getQueue(null, normolizeFiltersWithTop);

    }

    render() {

        const { filters, totalAmount,i18n:{translation = {}} = {} } = this.props,
              limitToValue = convertLimitToValue(filters),
              topFilter = getOneOf(filters, 'rank') || 0;


        return (
            <div className="row margin--xc-0">
                <div className="col-md-12 col-lg-5 font--12 row no-gutter fl--align-center padding--b-10 padding--lg-t-10">
                    
                    <span className="fl fl--align-c margin--r-15 font--color-inactive">
                        {translation.jobSeekers}<span className="font--14 margin--l-5">{totalAmount}</span>
                    </span>
                    <LimitFilter
                        className="padding--xc-5"
                        value={limitToValue}
                        onChange={::this.changeLimit}
                    />
                </div>
                <div className="col-md-12 col-lg-7 table-top-filters padding--yc-10 padding--xc-0">
                    <span className="icon icon--filter padding--t-5 margin--r-5 font--12 font--color-blue " />
                    <ul className="list">
                        {top.map((el, index) => (
                            <li
                                key={index}
                                className={`list__item list__item--inline min-width--50-px`}
                            >
                                <button
                                    type="button"
                                    className={`btn ${el.label == topFilter ? 'btn--primary' : 'btn--white'} font--12 pointer width--100`}
                                    onClick={() => this.changeTop('rank', el.label)}
                                >
                                    {el.name}
                                </button>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }
}
