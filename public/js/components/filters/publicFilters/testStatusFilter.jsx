import React from 'react'
import ComboboxComponent from "../../form/combobox.jsx";

export default class TestStatus extends React.PureComponent {
    render() {

        const {value, onChange} = this.props;

        return (
            <div className="padding--xc-15 padding--md-xc-30 margin--t-20 margin--b-20 margin--xl-b-0">
                <span className="margin--r-10 font--12 font--color-secondary">
                    показать:
                </span>
                <ComboboxComponent
                name="testStatus"
                wrapperClass="combobox-filter"
                onChange={onChange}
                list={[
                    { label: "passed", name: "Пройденные" },
                    { label: "fail", name: "Не пройденны" }
                ]}
                value={value || "passed"}
            />
            </div>
        )
    }
}
