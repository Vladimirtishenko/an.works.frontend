import React from 'react'
import ComboboxComponent from "../../form/comboboxComponent.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class Limit extends React.PureComponent {
    render() {

        const {value, className, onChange,i18n:{translation = {}} = {}} = this.props;

        return (
            <div className="fl fl--align-c">
                <span className="font--14 font--color-secondary">{translation.displayContacts}{" "}</span>
                <ComboboxComponent  
                    name="limit"
                    list={[
                        { label: "10", name: "10" },
                        { label: "20", name: "20" },
                        { label: "30", name: "30" },
                        { label: "40", name: "40" },
                        { label: "50", name: "50" }
                    ]}
                    className={className}
                    valueClass="comboboxComponent__name-size-14"
                    classList="comboboxComponent__list--sm-left"
                    value={value}
                    onChange={(value) => {
                        onChange(value)
                    }}
                />
            </div>
        )
    }
}
