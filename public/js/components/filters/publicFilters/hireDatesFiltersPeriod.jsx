import DateCustom from "../../../components/form/dateCustom.jsx";

export default class HireDatesFilterPeriod extends React.Component {
    render() {
        <div className="hr--top font--12 font--color-secondary font--500 margin--b-10">
            <div className="margin--b-10 margin--t-5 ">
                <div
                    className="fl fl--justify-b pointer"
                    onClick={() => this.handleExpandPeriod()}
                >
                    <span>Выбрать период</span>
                    <span
                        className={`icon icon--Rating_arrow margin--r-10 ${
                            isExpandedPeriod
                                ? "rotate--minus-90"
                                : "rotate--90"
                        }`}
                    />
                </div>
                {isExpandedPeriod && (
                    <div className="margin--yc-10">
                        <DateCustom
                            wrapperClass="fl fl--align-c  margin--b-10"
                            labelClass="label form__label font--14 margin--r-15"
                            coverClass="padding--md-0"
                            day={{
                                name: "day"
                                // value: date.day
                            }}
                            month={{
                                name: "month",
                                // value: date.month,
                                options: monthPrepare,
                                static: "Месяц"
                            }}
                            year={{
                                name: "year"
                                // value: date.year
                            }}
                            label="С"
                        />
                        <DateCustom
                            wrapperClass="fl fl--align-c  margin--b-10"
                            labelClass="label form__label font--14 margin--r-15"
                            coverClass="padding--md-0"
                            day={{
                                name: "day"
                                // value: date.day
                            }}
                            month={{
                                name: "month",
                                // value: date.month,
                                options: monthPrepare,
                                static: "Месяц"
                            }}
                            year={{
                                name: "year"
                                // value: date.year
                            }}
                            label="По"
                        />
                    </div>
                )}
            </div>
        </div>
    }
}
