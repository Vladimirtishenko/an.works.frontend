import React from "react";
import uniqid from "uniqid";
import {string, object, number, func} from "prop-types";

import periods from "../../../databases/general/periods.json";

import {
    changeOneOf,
    getOneOf,
} from "../../../helpers/filters/filter.helper.js"

import {
    transformToRange
} from "../../../helpers/date.helper.js"

export default class HireDatesFilter extends React.Component {
    static propsType = {
        value: string,
        action: func.inRequired,
        filters: string,
        companyId: number.isRequired
    }

    static defaultProps = {
        value: "currentMonth",
        filters: ''
    }

    constructor(props) {
        super(props);
        this.state = {
            isExpandedFilters: false,
            ...props
        };
        this.__refs = null;
        this.handleClickOutside = ::this.handleClickOutside;
    }

    handleExpandFilters() {
        this.setState({
            ...this.state,
            isExpandedFilters: !this.state.isExpandedFilters
        });
    }
    closeExpandFilters() {
        this.setState({
            ...this.state,
            isExpandedFilters: false
        });
    }
    handleChangeValue(event) {
        const { target: { dataset: { label = '' } = {} } = {} } = event,
              { filters, onChange,createdDate } = this.props;
        if(!label) return;
        const dates = {...transformToRange(label)},
              normalizedToFilters = changeOneOf(filters, {label: label, createdDate: dates});

        if(onChange){
            onChange(normalizedToFilters)
        }

        this.setState({
            ...this.state,
            isExpandedFilters: false
        });

    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    setToName(value){
        const index = _.findIndex(periods, {label: value}),
              obj = index > -1 ? periods[index] : null;

        if(obj) {
            return obj.name;
        }

        return '';

    }

    componentDidMount() {
		document.addEventListener('mousedown', this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
    }
    handleClickOutside(event) {
		if (this.__refs && !this.__refs.contains(event.target)) {
			this.closeExpandFilters();
		}
    }
    
    render() {

        const {
            isExpandedFilters,
            isExpandedPeriod,
            value,
            filters
        } = this.state,
        getFilterValue = getOneOf(filters, 'label') || '',
        name = this.setToName(getFilterValue || value);

        return (
            <div
                className="hireDatesFilters"
                ref={(refs) => { this.__refs = refs; }}
                onClick={
                    this.handleClickOutside
                }
            >
                <div
                    className="hireDatesFilters__display pointer"
                    onClick={() => this.handleExpandFilters()}
                >
                    {name}
                </div>
                <div
                    className={`hireDatesFilters__list ${
                        isExpandedFilters
                            ? "hireDatesFilters__list_expanded"
                            : ""
                    }`}
                >
                    {
                        periods.map((item, i) => {
                            return (
                                <div
                                    key={uniqid(`${item.label}-${i}`)}
                                    data-label={item.label}
                                    className="font--12 font--color-secondary font--500 margin--b-10 pointer"
                                    onClick={::this.handleChangeValue}
                                >
                                    {item.name}
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}
