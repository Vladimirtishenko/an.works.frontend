import React from "react";
// import ComboboxComponent from "../../form/combobox.jsx";
import ComboboxComponent from "../../form/comboboxComponent.jsx";

import {
    labelsToDefaults
} from "../../../helpers/mapperSkills.helper.js";

// Configuration
import ratingConfuguration from '../../../configuration/rating.json';

import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class TechnologyChoise extends React.PureComponent {
    constructor(props){
        super(props);
    } 

    onChange(value){

        if(!value) return;

        const {
            actions: { getQueue, getSkillTestHierarchy,saveRating },
            positionLevel,
        } = this.props;
        let { skill, level } = labelsToDefaults({skill: value, level: positionLevel}, ratingConfuguration);
        level = level == 'trainee' ? "junior" :  level;
        getQueue(level + "_" + skill, false);
        getSkillTestHierarchy(skill);
        saveRating(level + "_" + skill);
    }

    render() {
        let {knowledges = [], skill, i18n:{translation = {}} = {} } = this.props,
            sortOfKnowledges = knowledges.filter((item, i) => { if(item.isMain == true) return item });
        return (
            <div className="fl fl--align-md-c col-md-5 fl--wrap fl--dir-col fl--dir-md-row">
                <span className="nowrap--all font--20 margin--r-10">
                    {translation.rankingByTechnology}
                </span>{" "}
                <ComboboxComponent
                    name="filter.mainSkill"
                    valueClass="comboboxComponent__name-size-20"
                    list={sortOfKnowledges}
                    value={skill}
                    onChange={::this.onChange}
                />
            </div>)
    }
}
