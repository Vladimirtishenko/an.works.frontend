import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Range from "../../form/range/index.jsx";
import { oneOfType, object } from 'prop-types'

import {
    extendsSingleSkill
} from '../../../helpers/mapperSkills.helper.js'
import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class TechnologyFilter extends React.Component {

    static propsTypes = {
        value: oneOfType([object])
    }

    static defaultProps = {
        value: {},
        name: "technology",
        mainSkill: "",
        min: 0,
        max: 100,
        step: 1
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {

        const {value, min, max} = this.state;

        return Object.keys(value).length ? value : {min: min, max: max}

    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    onChange(value){

        const {onChange, name} = this.state;

        if(onChange){
            onChange(name, value)
        }

        this.setState({
            ...this.state,
            value
        })

    }

    render(){

        const {
            name,
            mainSkill,
            min,
            max,
            step,
            knowledges,
            value
        } = this.state,
        label = extendsSingleSkill(mainSkill, knowledges),
        {i18n:{translation = {}} = {}} = this.props;

        return(
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title={translation.technology}
                // titleSvg={<span className="pseudo-icon__info margin--t-2 margin--l-2" />}
            >
                <h5 className="margin--b-15">
                    {label && label.name}{" "}
                    <span className="font--color-inactive">
                        ({translation.scores})
                    </span>
                </h5>
                <Range
                    onChange={::this.onChange}
                    wrapperClass="padding--t-20"
                    name={name}
                    max={max}
                    min={min}
                    value={this.getValue}
                    step={step}
                />
            </FilterBlock>
        )
    }

}
