import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import { oneOfType, array } from 'prop-types'
import MultiselectComponent from "../../form/multiselect.jsx";
import Range from "../../form/range/index.jsx";
import uniqid from 'uniqid';
import { DeleteButton } from "../../../components/form/deleteButton.jsx";
import languageSkills from "../../../databases/languages/languageSkills.json";

import {
    mapperSkillsSorting
} from '../../../helpers/mapperSkills.helper.js'

import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class LanguagesFilter extends React.Component {

    static propsTypes = {
        value: oneOfType([array])
    }

    static defaultProps = {
        name: "languages",
        value: [],
        knowledges: []
    }

    constructor(props){
        super(props);
        this.state = {
            ...props,
            knowledges: mapperSkillsSorting(this.props.knowledges, [{isLang: true}])
        }
    }

    get getValue() {
        return this.state.value
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({
                ...this.state,
                ...this.props,
                knowledges: mapperSkillsSorting(this.props.knowledges, [{isLang: true}])
            })
		}

	}

    onChangeRange(value, label){

        const { value: oldValues, onChange, name} = this.state,
                newValue = oldValues.map((item, i) => {
                  if(item.label == label){
                      item.data = value;
                  }
                  return item;
              });

        if(onChange) {
            onChange(name, newValue);
        }


    }

    onMultiSelectChange(value) {

        const {value: oldValues, onChange, name} = this.state,
              newValue = [];

          value.forEach((item, i) => {
              const index = _.findIndex(oldValues, {label: item.label}),
                    obj =  index > -1 ? oldValues[index] : null;

              if(obj){
                  newValue.push(obj);
              } else {
                  newValue.push({
                      label: item.label,
                      name: item.name,
                      data: 0
                  })
              }

          })

        if(onChange) {
          onChange(name, newValue);
        }

        this.setState({
            ...this.state,
            value: newValue
        });
    }

    normalizeRangeData() {
        const language = {};

        for (var i = 0; i < languageSkills.length; i++) {
            language[languageSkills[i].label] = languageSkills[i].name;
        }

        return language;
    }

    removeField(label){

        const { value, onChange, name } = this.state,
                newValue = value.filter((item) => {
                    if(item.label !== label) {
                        return item;
                    }
                })

        if(onChange) {
          onChange(name, newValue);
        }

        this.setValue = newValue;

    }

    renderRange(el, index, type) {

        const { data, label, name } = el,
                valueList = ::this.normalizeRangeData(languageSkills),
            {i18n:{translation = {}} = {}} = this.props;


        return (
            <div className="fl fl--wrap margin--b-15 relative" key={uniqid(`${label}-`)}>
                <div className="fl width--100 fl--justify-st padding--t-17">
                    <h5 className="width--30 margin--b-5 font--14 padding--r-10">{name}</h5>
                    <Range 
                        reverse={true}
                        wrapperClass="width--70 d-block width--100 "
                        labelClass="font--12 nowrap--pre-wrap"
                        min={0}
                        max={4}
                        step={1}
                        valueList={valueList}
                        value={data}
                        dots={true}
                        onChange={(value) => {this.onChangeRange(value, label)}}
                    />
                </div>
                <DeleteButton
                    className="margin--l-auto font--12 padding--xc-5"
                    onClick={(event) => { ::this.removeField(label) }}
                    text={translation.delete}
                />
            </div>
        );

    }

    render(){

        const {
            value,
            onChange,
            name,
            knowledges
        } = this.state,
        {i18n:{translation = {}} = {}} = this.props;

        return(
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title={`${translation.languages || '' } (${translation.level && translation.level.toLowerCase() || ''})`}
            >
                <MultiselectComponent
                    name={name}
                    list={knowledges}
                    wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
                    className="col-md-12"
                    value={value}
                    onMultiSelectChange={::this.onMultiSelectChange}
                    placeholder={translation.languages}
                />
                {value &&
                    value.map((el, index) =>
                        this.renderRange(el, index)
                    )
                    || null
                }
            </FilterBlock>
        )
    }

}
