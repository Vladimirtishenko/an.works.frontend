import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import MultiselectComponent from "../../form/multiselect.jsx";
import { array, string } from 'prop-types'

import {
    mapperSkillsSorting,
    extendsSkillPloainArrayToExtends,
    transformDataToArray
} from '../../../helpers/mapperSkills.helper.js'

import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class SkillsFilter extends React.Component {

    static propsTypes = {
        value: array,
        name: string
    }

    static defaultProps = {
        value: [],
        name: "skill",
        knowledges: []
    }

    constructor(props){
        super(props);
        this.state = {
            ...props,
            knowledges: mapperSkillsSorting(props.knowledges, [{isLang: false}])
        }
    }


    get getValue() {
        return this.state.value
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({
                ...this.state,
                ...this.props,
                knowledges: mapperSkillsSorting(this.props.knowledges, [{isLang: false}])
            })
		}

	}

    normalizeValueData(){
        const { knowledges, value } = this.state,
                normalized = extendsSkillPloainArrayToExtends(knowledges, value);

        return normalized;

    }

    onChange(value){

        const {onChange, name} = this.state,
              denormalized = transformDataToArray(value);

        if(onChange){
            onChange(name, denormalized)
        }

        this.setState({
            ...this.state,
            value: denormalized
        })

    }

    render(){

        const {
            name,
            value,
            knowledges
        } = this.state,
        {i18n:{translation = {}} = {}} = this.props;

        return(
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title={translation.skillsDeclared}
            >
                <MultiselectComponent
                    onMultiSelectChange={::this.onChange}
                    name={name}
                    list={knowledges}
                    wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
                    className="col-md-12"
                    value={this.normalizeValueData(value)}
                    placeholder={translation.additionalSkills}
                />
            </FilterBlock>
        )
    }

}
