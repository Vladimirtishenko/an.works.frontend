import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Range from "../../form/range/index.jsx";
import { object } from 'prop-types'
import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class SallaryFilter extends React.Component {

    static propsTypes = {
        value: object
    }

    static defaultProps = {
        value: {},
        name: "sallary",
        min: 0,
        max: 1000,
        step: 10
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {

        const {value, min, max} = this.state;

        return Object.keys(value).length ? value : {min: min, max: max}

    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    onChange(value){

        const {onChange, name} = this.state;

        if(onChange){
            onChange(name, value)
        }

        this.setState({
            ...this.state,
            value
        })

    }

    render(){

        const {
            name,
            min,
            max,
            step,
            value
        } = this.state,
        {i18n:{translation = {}} = {}} = this.props;

        return(
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title={translation.salary}
            >
                <Range
                    wrapperClass="padding--t-20"
                    name={name}
                    max={max}
                    min={min}
                    value={this.getValue}
                    step={step}
                    onChange={::this.onChange}
                />
            </FilterBlock>
        )
    }

}
