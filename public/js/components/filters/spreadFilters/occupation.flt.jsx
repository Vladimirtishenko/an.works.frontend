import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Checkbox from "../../form/checkbox.jsx";
import { array } from 'prop-types'

import occupationDictionary from '../../../databases/general/occupation.json'

export default class OccupationFilter extends React.Component {

    static propsTypes = {
        value: array
    }

    static defaultProps = {
        name: "occupation",
        value: []
    };

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {
        return this.state.value;
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    compareValues(){
        const dictionary = occupationDictionary,
              { value } = this.state,
              newValues = dictionary.map((item, i) => {
                  item.innerWrapperClass = "margin--b-15";
                  item.name = item.label;

                  if(value.indexOf(item.label) > -1){
                      item.checked = true
                      item.value = true
                  }

                  return item;

              })

        return newValues;

    }

    simplifyValue(value){

        const {name} = this.state,
              filtered = [];

       value.forEach((item, i) => {
           if(item.checked){
               filtered.push(item.label)
           }
       })

       return filtered;

    }

    onChange(value){

        const { onChange, name} = this.state,
                denormalized = this.simplifyValue(value)

        if(onChange){
            onChange(name, denormalized)
        }

        this.setState({
            ...this.state,
            value: denormalized
        })

    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    render(){

        const {value} = this.state;

        return(
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title="Вид занятости"
            >
                <Checkbox
                    onChange={::this.onChange}
                    wrapperAllCheckbox="fl fl--dir-col"
                    value={::this.compareValues()}
                    required={false}
                />
            </FilterBlock>
        )
    }

}
