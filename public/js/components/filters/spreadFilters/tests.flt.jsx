import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import MultiselectComponent from "../../form/multiselect.jsx";
import Range from "../../form/range/index.jsx";
import uniqid from 'uniqid';
import { DeleteButton } from "../../../components/form/deleteButton.jsx";
import { oneOfType, array, string } from 'prop-types'

import {
    mapperSkillsSorting
} from '../../../helpers/mapperSkills.helper.js'

import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class TestsFilter extends React.Component {

    static propsTypes = {
        value: oneOfType([array]),
        name: string
    }

    static defaultProps = {
        name: "testing",
        value: []
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {
        return this.state.value
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({
                ...this.state,
                ...this.props
            })
		}

	}

    onChangeRange(value, label){

        const { value: oldValues, onChange, name} = this.state,
                newValue = oldValues.map((item, i) => {
                  if(item.label == label){
                      item.data = value;
                  }
                  return item;
              });


        if(onChange) {
            onChange(name, newValue);
        }


    }

    onMultiSelectChange(value) {

        const {value: oldValues, onChange, name} = this.state,
              newValue = [];

          value.forEach((item, i) => {
              const index = _.findIndex(oldValues, {label: item.label}),
                    obj =  index > -1 ? oldValues[index] : null;

              if(obj){
                  newValue.push(obj);
              } else {
                  newValue.push({
                      label: item.label,
                      name: item.name,
                      data: {
                          min: 0,
                          max: 100
                      }
                  })
              }

          })

        if(onChange) {
          onChange(name, newValue);
        }

        this.setState({
            ...this.state,
            value: newValue
        });
    }


    removeField(label){

        const { value, onChange, name } = this.state,
                newValue = value.filter((item) => {
                    if(item.label !== label) {
                        return item;
                    }
                })

        if(onChange) {
          onChange(name, newValue);
        }

        this.setValue = newValue;

    }

    renderRange(el, index, type) {

        const {data, label, name} = el,
            {i18n:{translation = {}} = {}} = this.props;
        return (
            <div className="margin--b-30" key={uniqid(`${label}-`)}>
                <h5 className="fl fl--justify-b margin--b-10">
                    <span>
                        {name}{" "}
                        <span className="font--color-inactive">({translation.scores})</span>
                    </span>
                    <DeleteButton
                        className="font--12 padding--xc-5"
                        onClick={(value) => {this.removeField(label)}}
                        text={translation.delete}
                    />
                </h5>
                <Range
                    onChange={(value) => {this.onChangeRange(value, label)}}
                    wrapperClass="padding--t-20"
                    name={label}
                    max={100}
                    min={0}
                    step={1}
                    value={data}
                />
            </div>
        );
    }

    normalizeTestsData(tests){

        const data = [];

        _.forEach(tests, (item) => {
            let obj = {}

            obj.name = item.title
            obj.label = item.testLabel

            data.push(obj);
        })

        return data;

    }

    normalizeTestsDataToValue(value = null, tests = null){

        const data = [];

        if(!tests || !value) return [];

        _.forEach(value, (item) => {

            const find = _.find( tests, {id: item.label} );

            if(find){
                let obj = {}

                obj.name = find.title
                obj.label = item.label
                obj.data = item.data

                data.push(obj);
            }

        })

        return data;

    }

    render() {
        const {
            name,
            tests,
            value,
            type
        } = this.state,
        listValues = this.normalizeTestsDataToValue(value, tests),
        {i18n:{translation = {}} = {}} = this.props;

        return (
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title={translation.skillsTesting}
            >
                <MultiselectComponent
                    name={name}
                    list={this.normalizeTestsData(tests)}
                    wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
                    className="col-md-12"
                    value={value}
                    onMultiSelectChange={::this.onMultiSelectChange}
                    placeholder={translation.TestingBySkills}
                />
            {
                listValues.length &&
                    listValues.map((el, index) =>
                        this.renderRange(el, index, type)
                ) || null
            }
            </FilterBlock>
        );
    }

}
