/*

This example can launch this component

<div>
    <h3 className="margin--b-5">Другая страна</h3>
    <MultiselectComponent
        wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
        className="col-md-12"
        name="relocation.abroad.country"
        list={countries}
        value={countriesValue}
        onMultiSelectChange={::this.onChangeSelectByAbroad}
        placeholder="Страна"
    />
    {
        abroad && abroad.length && abroad.map((item, i) => { return <LocationSingle onRemoveAbroad={::this.onRemoveAbroad} onChangeAbroad={::this.onChangeAbroad} key={i} item={item} />}) || null
    }
</div>

*/


import React from 'react'
import MultiselectComponent from "../../form/multiselect.jsx";
import uniqid from 'uniqid';
import { DeleteButton } from "../../form/deleteButton.jsx";

import countries from '../../../databases/general/countries.json'

import {
    findCitiesFromEmptyArray
} from '../../../helpers/locationMapper.helper.js'

class RelocationSingle extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            ...props,
            list: []
        }

    }

    getCountryName(country){
        const index = _.findIndex(countries, {label: country}),
              obj = index > -1 ? countries[index] : {};

        return obj.name || '';
    }

    componentDidMount(){
        this.uploadCities()
    }

    async uploadCities(){

        const { item: {cities, country} } = this.state,
                citiesArray = await findCitiesFromEmptyArray(country);

        this.setState({
            ...this.state,
            list: citiesArray
        })

    }

    onChange(value){

        const newValue = [],
              { item, onChangeAbroad } = this.state,
              cloned = {...item};

        _.forEach(value, (item) => {
            newValue.push(item.label)
        })

        cloned.cities = newValue;

        if(onChangeAbroad) {
            onChangeAbroad(cloned);
        }

    }

    render() {

        const { item: { country, cities }, list, onRemoveAbroad } = this.state,
                name = this.getCountryName(country);

        return (
            <div className="margin--b-15">
                <div className="fl fl--align-c fl--justify-b margin--b-5">
                    <h5 className="font--16">{name}</h5>
                    <DeleteButton
                        className="font--12 padding--xc-5"
                        onClick={() => {onRemoveAbroad(country)}}
                        text="Удалить"
                    />
                </div>
                <MultiselectComponent
                    wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
                    className="col-md-12"
                    name="relocation.abroad.cities"
                    list={list}
                    value={[...cities]}
                    placeholder="Город"
                    onMultiSelectChange={::this.onChange}
                />
            </div>
        );

    }


}

export default RelocationSingle;
