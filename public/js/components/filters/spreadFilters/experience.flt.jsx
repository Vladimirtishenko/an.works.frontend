import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Checkbox from "../../form/checkbox.jsx";
import { oneOfType, array, string } from "prop-types";
import { DeleteButton } from "../../form/deleteButton.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)

export default class EducationFilter extends React.Component {

    static propsTypes = {
        value: array
    };

    static defaultProps = {
        name: "experience",
        value: [],
        lists: [
            {
                name: "Есть",
                label: "yes"
            }
        ]
    };

    constructor(props) {
        super(props);
        this.state = {
            ...props
        };
    }

    get getValue() {
        return this.state.value;
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    comparingValue(values){

        const { value } = this.state,
                {lists} = this.props;

        const newValue = lists.map((item, i) => {
                let names = item.name.slice();
                item.innerWrapperClass = "margin--b-15";
                item.sub = (names == item.name) ? item.sub == undefined ? names : item.sub : names;
                item.name = item.label;
              if(values.length) {

                  if(value.indexOf(item.label) > -1){
                      item.checked = true
                      item.value = true
                  }
              } else {
                  item.checked = false
                  item.value = false
              }

                return item;

            })
        return newValue;

    }

    simplifyValue(value){

        const {name} = this.state,
              filtered = [];

       value.forEach((item, i) => {
           if(item.checked){
               filtered.push(item.label)
           }
       })

       return filtered;

    }

    onChange(value){

        const {onChange, name} = this.state;

        if(onChange){
            onChange(name, this.simplifyValue(value))
        }

        this.setState({
            ...this.state,
            value
        })

    }
 
    render() {

        const { value } = this.state,
            {i18n:{translation = {}} = {}} = this.props;

        return (
            <FilterBlock 
                isOpen={value && Object.keys(value).length ? true : false }
                title={translation.experience}>
                <Checkbox 
                    onChange={::this.onChange}
                    labelClass="fl fl--dir-col"
                    wrapperAllCheckbox="fl fl--dir-col"
                    value={::this.comparingValue(value)}
                    required={false}
                />
            </FilterBlock>
        );
    }
}
