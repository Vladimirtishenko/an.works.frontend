import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Radio from "../../form/radio.jsx";
import { oneOfType, array, string } from "prop-types";
import { DeleteButton } from "../../form/deleteButton.jsx";

export default class AdditionalEducationFilter extends React.Component {
    static propsTypes = {
        value: oneOfType([string]),
        name: string,
        list: oneOfType([array])
    };

    static defaultProps = {
        value: '',
        name: 'additionalEducation',
        list: [
            {
                value: 'true',
                sub: "Есть",
                checked: false,
            },
            {
                value: 'false',
                sub: "Нет",
                checked: false
            }
        ]
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {
        return this.state.value;
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    comparingValue(){

        const { name, value, list } = this.state;

              const newValue = [...list].filter((item, i) => {

                  if(item.value == value) {
                      item.checked = true;
                  } else {
                      item.checked = false;
                  }

                  if(i == 0){
                      item.innerWrapperClass = "margin--b-15";
                  }

                  return item;

              })

        return newValue;

    }

    resetValue(){

        const { onChange, name } = this.state;

        if(onChange) {
            onChange(name, '')
        }

        this.setValue = ''

    }

    onChange(value){

        let newValue = '';

        const { onChange, name } = this.state;

        value.forEach((item) => {
            if(item.checked){
                newValue = item.value
            }
        })

        if(onChange) {
            onChange(name, newValue)
        }

        this.setValue = newValue

    }

    render() {

        const {name, value} = this.state;

        return (
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title="Доп. образование"
            >
                <div className="fl fl--justify-b">
                    <Radio
                        name={name}
                        innerRadiosWrapperClass="fl--dir-col"
                        value={this.comparingValue()}
                        required={false}
                        onChange={::this.onChange}
                    />
                    <DeleteButton
                        className="font--12 padding--xc-5 padding--l-15"
                        onClick={::this.resetValue}
                        text="Сбросить"
                    />
                </div>
            </FilterBlock>
        );
    }
}
