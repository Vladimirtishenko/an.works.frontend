import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Radio from "../../form/radio.jsx";
import { string } from "prop-types";
import { DeleteButton } from "../../form/deleteButton.jsx";

import {
    comparingGenderValue
} from '../../../helpers/gender.helper.js'
import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";
function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)

export default class GenderFilter extends React.Component {
    static propsTypes = {
        value: string
    };

    static defaultProps = {
        value: '',
        name: 'gender',
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {
        return this.state.value;
    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    resetValue(){

        const { onChange, name } = this.state;

        if(onChange) {
            onChange(name, '')
        }

        this.setValue = ''

    }

    onChange(value){

        let newValue = '';

        const { onChange, name } = this.state;

        value.forEach((item) => {
            if(item.checked){
                newValue = item.value
            }
        })

        if(onChange) {
            onChange(name, newValue)
        }

        this.setValue = newValue

    }

    render() {
        const { name, value } = this.state,
            {i18n:{translation = {}} = {}} = this.props;

        return (
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title={translation.gender}
            >
                <div className="fl fl--justify-b">
                    <Radio
                        onChange={::this.onChange}
                        name={name}
                        innerRadiosWrapperClass="fl--dir-col"
                        value={comparingGenderValue(value, "margin--b-15", 0)}
                        required={false}
                    />
                    <DeleteButton
                        className="font--12 padding--xc-5 padding--l-15"
                        onClick={::this.resetValue}
                        text={translation.reset}
                    />
                </div>
            </FilterBlock>
        );
    }
}
