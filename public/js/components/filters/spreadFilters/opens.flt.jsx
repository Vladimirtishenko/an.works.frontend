import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Range from "../../form/range/index.jsx";
import { object } from 'prop-types'

export default class OpensFilter extends React.Component {
    static propsTypes = {
        value: object
    }

    static defaultProps = {
        value: {},
        name: "opens",
        min: 0,
        max: 5,
        step: 1
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {

        const {value, min, max} = this.state;

        return Object.keys(value).length ? value : {min: min, max: max}

    }

    set setValue(value){
        this.setState({
            ...this.state,
            value
        })
    }

    onChange(value){

        const {onChange, name} = this.state;

        if(onChange){
            onChange(name, value)
        }

        this.setState({
            ...this.state,
            value
        })

    }

    componentDidUpdate(prevProps, prevState){

		let comparingProps = _.isEqual(prevProps, this.props);

		if(!comparingProps) {
			this.setState({...this.state, ...this.props})
		}

	}

    render(){

        const {
            name,
            min,
            max,
            step,
            value
        } = this.state;

        return(
            <FilterBlock
                isOpen={value && Object.keys(value).length ? true : false }
                title="Количество открытий"
            >
                <Range
                    wrapperClass="padding--t-20"
                    name={name}
                    max={max}
                    min={min}
                    value={this.getValue}
                    step={step}
                    onChange={::this.onChange}
                />
            </FilterBlock>
        )
    }

}
