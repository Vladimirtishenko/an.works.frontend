import React from "react";
import FilterBlock from "../FilterBlock.jsx";
import Checkbox from "../../form/checkbox.jsx";
import MultiselectComponent from "../../form/multiselect.jsx";
import LocationSingle from "./locationSingle.flt.jsx";
import countries from '../../../databases/general/countries.json'
import uniqid from 'uniqid';

import { oneOfType, object } from 'prop-types'

import {
    findCitiesFromEmptyArray,
    simplifyCityToArray
} from '../../../helpers/locationMapper.helper.js';

import { connect } from "react-redux";
import * as i18n from "../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class LocationFilter extends React.Component {

    static propsTypes = {
        value: oneOfType([object])
    }

    static defaultProps = {
        value: {},
        db: [],
        name: "location"
    }

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    get getValue() {
        return this.state.value;
    }

    set setValue(value){

        const { onChange, name } = this.state;

        if (onChange && name) {
            onChange(name, value)
        }

        this.setState({
            ...this.state,
            value
        })


    }

    componentDidUpdate(prevProps, prevState){

        const { value: prevValue } = prevProps,
              { value: nextValue } = this.props,
              comparingProps = _.isEqual(prevValue, nextValue);

		if(!comparingProps) {
			this.setState({
                ...this.state,
                value: nextValue
            })
		}

	}

    onChangeCheckbox(value){
        const [ item ] = value,
              { checked, name } = item,
              { value: oldValue } = this.state,
              copyValue = {...oldValue};

        _.set(copyValue, name, checked);

        this.setValue = copyValue;
    }

    onChangeSelect(value, name){

        const { value: oldValue } = this.state,
              copyValue = {...oldValue},
              simplifyValue = simplifyCityToArray(value);

        _.set(copyValue, name, simplifyValue);

        this.setValue = copyValue;

    }

    async getCountryList() {

        const cities = await findCitiesFromEmptyArray(2);

        this.setState({
            ...this.state,
            db: cities
        })

    }

    componentDidMount() {

        this.getCountryList();

    }

    getJustCountry(abroad){

        const countries = []

        _.forEach(abroad, (item) => {
            countries.push(item.country)
        })

        return countries;

    }

    onChangeSelectByAbroad(value){

        const { value: cloneValue, value: {relocation: {abroad}} } = this.state,
              newValue = [],
              cloned = {...cloneValue};

      _.forEach(value, (item) => {
          const index = _.findIndex(abroad, {country: item.label});

          if(index !== -1) {
              newValue.push(abroad[index]);
          } else {
             newValue.push({
                 country: item.label,
                 cities: []
             })
          }

      })

      cloned.relocation.abroad = newValue;

      this.setValue = cloned;


    }

    onRemoveAbroad(country){

        const { value: stateValue, value: {relocation: {abroad}} } = this.state,
              cloned = {...stateValue},
              newValue = _.filter(cloned.relocation.abroad, (item, i) => {
                  if(item.country != country) {
                      return item;
                  }
              });

        _.set(cloned, 'relocation.abroad', newValue);

        this.setValue = cloned;

    }

    onChangeAbroad(value){

        const { value: stateValue, value: {relocation: {abroad}} } = this.state,
              cloned = {...stateValue},
              newValue = _.filter(cloned.relocation.abroad, (item, i) => {
                  if(item.country == value.country) {
                      item.cities = value.cities;
                      return item;
                  }
                  return item;
              });

        _.set(cloned, 'relocation.abroad', newValue);

        this.setValue = cloned;

    }

    render(){

        const {
            value: {
                relocation: {
                    checked: { local: localCheckbox = false, abroad: abroadCheckbox = false } = {},
                    local = [],
                    abroad = []
                } = {},
                cities = []
            },
            db
        } = this.state,
        countriesValue = this.getJustCountry(abroad),
        {i18n:{translation = {}} = {}} = this.props;
        return(
            <FilterBlock
                isOpen={cities && Object.keys(cities).length ? true : false || localCheckbox || abroadCheckbox }
                title={translation.location}
            >
                <div>
                    <h3 className="margin--b-5">{translation.ukr}</h3>
                    <MultiselectComponent
                        name="cities"
                        list={db}
                        value={[...cities]}
                        wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
                        className="col-md-12"
                        placeholder={translation.city}
                        onMultiSelectChange={(value) => {this.onChangeSelect(value, 'cities')}}
                    />
                </div>
                <Checkbox
                    wrapperClass="margin--b-15"
                    labelClass="padding--l-0"
                    value={[
                        {
                            name: "relocation.checked.local",
                            sub: translation.readyRelocate,
                            checked: localCheckbox
                        }
                    ]}
                    required={false}
                    onChange={::this.onChangeCheckbox}
                />
                {
                    localCheckbox ?
                    <React.Fragment>
                        <div>
                            <h3 className="margin--b-5">{translation.inUkr}</h3>
                            <MultiselectComponent
                                wrapperClass="row fl--align-center no-gutters margin--b-20 multiselect"
                                className="col-md-12"
                                name="relocation.local"
                                list={db}
                                value={[...local]}
                                placeholder={translation.city}
                                onMultiSelectChange={(value) => {this.onChangeSelect(value, 'relocation.local')}}
                            />
                        </div>
                    </React.Fragment> : null
                }
                <Checkbox
                    wrapperClass="margin--b-15"
                    labelClass="padding--l-0"
                    value={[
                        {
                            name: "relocation.checked.abroad",
                            sub: translation.readyMoveAbroad,
                            checked: abroadCheckbox
                        }
                    ]}
                    required={false}
                    onChange={::this.onChangeCheckbox}
                />
            </FilterBlock>
        )
    }

}
