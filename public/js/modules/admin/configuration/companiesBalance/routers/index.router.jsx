import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

import BalancesLists from '../containers/balancesLists.ctrl.jsx'
import Balance from '../containers/balance.ctrl.jsx';

const routes = [
	{
			path: "/companies_balance",
			component: BalancesLists
	},
	{
			path: "/companies_balance/view/:id",
			component: Balance
	}
]
export default routes;