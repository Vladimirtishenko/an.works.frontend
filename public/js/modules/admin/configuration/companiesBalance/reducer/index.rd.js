import * as types from '../constants/companyBalance.const';
const initialState = {
    list:[]
};

export default function companies_balances(state = initialState, action) {

    switch (action.type) {

        case types.COMPANIES_BALANCES_LOADED_SUCCESS:
            return {
                ...state,
                list: action.balances
            };
        case types.COMPANY_BALANCE_LOADED_SUCCESS:
            return {
                ...state,
                balance: action.balance
            };
        case types.COMPANIES_BALANCES_LOADED_FAILED:
        case types.COMPANY_BALANCE_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };
        default:
            return state
    }
}