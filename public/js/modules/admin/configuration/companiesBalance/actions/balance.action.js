import * as types from '../constants/companyBalance.const.js';
import API from '../../../../../services/api.js'

export function getBalances() {

    return dispatch => {

        (async () => {

                let balances = await API.__get('companies_balances/', dispatch, types.COMPANIES_BALANCES_LOADED_FAILED);

                if(balances)
                {
                    dispatch({
                        type: types.COMPANIES_BALANCES_LOADED_SUCCESS,
                        balances: balances
                    })
                }
        })();

    }
}

export function getBalance(companyId) {

    return dispatch => {

        (async () => {

            let balance = await API.__get('companies_balance/'+companyId, dispatch, types.COMPANY_BALANCE_LOADED_FAILED)

            if(balance)
            {
                dispatch({
                    type: types.COMPANY_BALANCE_LOADED_SUCCESS,
                    balance: balance
                })
            }
        })();

    }
}

