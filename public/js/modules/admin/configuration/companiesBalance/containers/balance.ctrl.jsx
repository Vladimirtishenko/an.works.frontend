import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
/* Components */
import BalanceView from '../components/item.jsx'

// Actions
import * as ordersActions from '../actions/balance.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        balance: state.configuration.companies_balances.balance || {}
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...ordersActions
        }, dispatch),
        dispatch
    }
}

@connect(mapStateToProps, mapDispatchToProps)
class Balance extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        if(this.props.match.params && this.props.match.params.id)
        {
            this.props.actions.getBalance(this.props.match.params.id || null);
        }
    }

	render() {
		return (
            <BalanceView {...this.props}/>
        );
	}

}

export default Balance;