import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as balanceActions from '../actions/balance.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        balances: state.configuration.companies_balances.list || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...balanceActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import BalanceListView from '../components/list.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class BalancesLists extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getBalances();
    }

	render() {

		return (
            <BalanceListView {...this.props}/>
        );
	}

}

export default BalancesLists;