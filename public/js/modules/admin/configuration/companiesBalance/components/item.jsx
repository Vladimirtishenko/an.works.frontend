import React from 'react'
// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Table from '../../../../../components/tables/main.jsx'


// Componets
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'
import { isArray } from 'util';

class BalanceView extends React.Component {
    constructor(props){
        super(props);
    }

    render () {
        let schema  = {
            history: {
                fields: [
                    {
                        name: 'operation',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-16 font--left',
                        classNameTd:'respons-flex-table--w-16'
                        
                    },
                    {
                        name: 'tokensCount',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-16 font--left',
                        classNameTd:'respons-flex-table--w-16'
                    },
                    {
                        name: 'tokenPrice',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-16 font--left',
                        classNameTd:'respons-flex-table--w-16'
                    },
                    {
                        name: 'totalPrice',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-16 font--left',
                        classNameTd:'respons-flex-table--w-16'
                    },
                    {
                        name: 'orderId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-16 font--left',
                        classNameTd:'respons-flex-table--w-16'
                    },
                    {
                        name: 'createdDate',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-16 font--left',
                        classNameTd:'respons-flex-table--w-16'
                    },
                ],
                actions: {},
            }
        };

        let {history} = schema,
            {i18n} = this.props;
        let balance = this.props.balance || null;
        if(!balance)
        {
            return (
                <div className="row no-gutters fl--align-c margin--b-20">
                    Entity not found
                </div>
            );
        }
        const {
            balance:{
                history:{
                    items = []
                } = {}
            } = {}
        } = this.props;
        let balances = [];
        
        items.forEach(element => {
            element &&  element.orderData ? element.orderData : element.orderData = {}
            const {
                orderData:{
                    orderId = '',
                    orderItem:{
                        currenciesItemRate:{
                            UAH = ''
                        } = {}
                    } = {}
                } = {},
                operation,
                tokensCount,
                createdDate
            }  = element;
            let totalPrice = UAH * tokensCount
            balances.push({
                orderId:orderId,
                totalPrice:totalPrice.toFixed(2),
                tokensCount:tokensCount,
                operation:operation,
                createdDate:createdDate,
                tokenPrice: UAH
            })
        });
        return (
            <div className="col-12">
                <Input 
                    label="Company id"
                    name="companyId"
                    value={ this.props.balance.companyId || ''}
                    readonly={true}
                />
                <Input
                    label="Tokens counts"
                    name="orderItem.count"
                    value={ this.props.balance && this.props.balance.tokensCount || ''}
                    readonly={true}
                />
                <div className="col-12">
                    <Table i18n={i18n} data={balances} schema={history}/>
                </div>

            </div>
        )
    }
}

export default BalanceView;
