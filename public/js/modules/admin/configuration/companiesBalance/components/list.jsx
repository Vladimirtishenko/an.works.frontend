import React from 'react'
import Table from '../../../../../components/tables/main.jsx'

//TODO::create schema
class BalanceListView extends React.Component {
    render(){

        let schema  = {
            balances: {
                fields: [
                    {
                        name: 'companyId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25 font--left',
                        classNameTd:'respons-flex-table--w-25'
                    },
                    {
                        name: 'tokensCount',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25 font--left',
                        classNameTd:'respons-flex-table--w-25'
                    }
                ],
                actions: {
                    edit: 'url[/companies_balance/view/:id]'
                },
                entityIdKey: 'companyId'
            }
        };

        let {balances} = schema,
            {i18n} = this.props;
        return (
            <div className="col-12">
                <Table i18n={i18n} data={this.props.balances} schema={balances}/>
            </div>
        )
    }
}

export default BalanceListView;