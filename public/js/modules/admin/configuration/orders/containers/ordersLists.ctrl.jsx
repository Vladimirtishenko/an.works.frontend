import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as ordersActions from '../actions/orders.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        orders: state.configuration.orders.list || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...ordersActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import OrdersListView from '../components/list.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class OrdersLists extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getOrders();
    }

	render() {

		return (
					<OrdersListView {...this.props}/>
			   );
	}

}

export default OrdersLists;