import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as ordersActions from '../actions/orders.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        order: state.configuration.orders.order || {}
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...ordersActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import OrderView from '../components/item.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class Order extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        if(this.props.match.params && this.props.match.params.id)
        {
            this.props.actions.getOrder(this.props.match.params.id || null);
        }
    }

	render() {
        if(this.props.redirectToList === true)
        {
            this.props.history.push("/orders");
        }
        let data = {isCreation: (this.props.match.params && this.props.match.params.id) ? false : true, ...this.props};
		return (
					<OrderView {...data}/>
			   );
	}

}

export default Order;