import * as types from '../constants/orders.const.js';
import API from '../../../../../services/api.js'

export function getOrders() {

    return dispatch => {

        (async () => {

                let orders = await API.__get('/orders/', dispatch, types.ORDERS_LOADED_FAILED);

                if(orders)
                {
                    dispatch({
                        type: types.ORDERS_LOADED_SUCCESS,
                        orders: orders
                    })
                }
        })();

    }
}

export function getOrder(orderId) {

    return dispatch => {

        (async () => {

            let order = await API.__get('/orders/'+orderId, dispatch, types.ORDER_LOADED_FAILED)

            if(order)
            {
                dispatch({
                    type: types.ORDER_LOADED_SUCCESS,
                    order: order
                })
            }
        })();

    }
}

export function createOrder(data) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let order = await API.__post('POST', 'api/orders/', data, dispatch, types.ORDER_CREATED_FAILED);

            if(order)
            {
                dispatch({
                    type: types.ORDER_CREATED_SUCCESS,
                    order: order
                });
            }

        })();

    }
}

export function updateOrder(data, orderId) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let order = await API.__post('PATCH', 'api/orders/'+orderId, data, dispatch, types.ORDER_UPDATED_FAILED);

            if(order)
            {
                dispatch({
                    type: types.ORDER_UPDATED_SUCCESS,
                    order: order
                })
            }

        })();

    }
}
