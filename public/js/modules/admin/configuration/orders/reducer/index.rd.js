import * as types from '../constants/orders.const';
const initialState = {
    list:[]
};

export default function orders(state = initialState, action) {

    switch (action.type) {

        case types.ORDERS_LOADED_SUCCESS:
            return {
                ...state,
                list: action.orders,
                redirectToList: false
            };
        case types.ORDER_LOADED_SUCCESS:
            return {
                ...state,
                order: action.order
            };
        case types.ORDER_CREATED_SUCCESS:
        case types.ORDER_UPDATED_SUCCESS:
            return {
                ...state,
                order: action.order,
                redirectToList: true
            };
        case types.ORDERS_LOADED_FAILED:
        case types.ORDER_LOADED_FAILED:
        case types.ORDER_UPDATED_FAILED:
            return {
                ...state,
                notification: action.notification
            };
        default:
            return state
    }
}