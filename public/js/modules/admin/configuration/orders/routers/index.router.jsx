
import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

import OrderList from '../containers/ordersLists.ctrl.jsx'
import Order from '../containers/order.ctrl.jsx'

const routes = [
	{
			path: "/orders",
			component: OrderList
	},
	{
			path: "/orders/edit/:id",
			component: Order
	},
	{
			path: "/orders/new",
			component: Order
	}
]
export default routes;