import React from 'react'
import Table from '../../../../../components/tables/main.jsx'
import {Link} from 'react-router-dom';
import uniqid from 'uniqid';
import {dmyDate} from '../../../../../helpers/date.helper.js'
//TODO::create schema
class OrdersListView extends React.Component {


    createNew(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);

        this.props.actions.updateCompany(serializedForm, this.props.company.name);

    }

	render(){

        let schema  = {
            orders: {
                fields: [
                    {
                        name: 'orderId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-18 font--left',
                        classNameTd:'respons-flex-table--w-18'
                    },
                    {
                        name: 'companyId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-18 font--left',
                        classNameTd:'respons-flex-table--w-18'
                    },
                    {
                        name: 'status',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-18 font--left',
                        classNameTd:'respons-flex-table--w-18'
                    },
                    {
                        name: 'countToken',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-18 font--left',
                        classNameTd:'respons-flex-table--w-18'
                    },
                    {
                        name: 'totalOrderPrice',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-18 font--left',
                        classNameTd:'respons-flex-table--w-18'
                    },
                ],
                actions: {
                    edit: 'url[/orders/edit/:id]'
                },
                entityIdKey: 'orderId'
            }
        };

        let {orders} = schema,
            {i18n,orders:data} = this.props,
            dataItems = data.slice().sort((item1, item2) =>{return item1.status == "pending" ? -1 : 0;}); 

            dataItems.map(element => {
                const {
                    orderItem:{
                        count = ''
                    } = {}
                } = element;
                element.countToken = count;
                return element;
            });
		return (
			<div className="col-12">
                <div className="margin--b-20">
                    <Link to={'/orders/new'} className="font--18 font--500"> Сreate New </Link>
                </div>
                {/* <Table i18n={i18n} data={dataItems} schema={orders}/> */}
                <table className="respons-flex-table box--white">
                    <thead className="respons-flex-table__thead respons-flex-table__thead--admin-theme">
                        <tr className="respons-flex-table__tr" >
                            <th className="respons-flex-table__th respons-flex-table--w-15 font--center">
                                createdDate
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-15 font--center">
                                orderId
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-15 font--center">
                                companyId
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-15 font--center">
                                status 
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-15 font--center">
                                countToken
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-15 font--center">
                                totalOrderPrice
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-10 font--center">
                                edit
                            </th>
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                        {
                            dataItems.map((item, i) => {
                                return (
                                    <tr key={uniqid(i + '-')} data-id={item.id} className="respons-flex-table__tr" >
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-15 font--center">
                                            {dmyDate(item.createdDate)}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-15 font--center">
                                            {item.orderId}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-15 font--center">
                                            {item.companyId}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-15 font--center">
                                            {item.status}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-15 font--center">
                                            {item.countToken}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-15 font--center">
                                            {item.totalOrderPrice}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-10 font--center">
                                            <Link to={`/orders/edit/${item.orderId}`} className="font--color-blue link">
                                                edit
                                            </Link>
                                        </td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>
			</div>
		)
	}
}

export default OrdersListView;