import React from 'react'
// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import Textarea from '../../../../../components/form/textarea.jsx';
import Number from '../../../../../components/form/number.jsx';
import BtnSubmit from '../../applicantPprofiles/components/button/btnSave.jsx';

// Componets
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

class OrderView extends React.Component {
    constructor(props){
        super(props);
        //this.state = {...this.props.skillTestsHierarchy}
    }

    submit(event){
        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);
        //should be number
        serializedForm.companyId = parseInt(serializedForm.companyId);
        serializedForm.userInfo = {
            uid: this.props.oauth.user.uid,
            email: this.props.oauth.user.email,
        };
        //serializedForm.currency = 'USD';
        if(this.props.isCreation === true)
        {
            this.props.actions.createOrder(serializedForm);
        }
        else {
            this.props.actions.updateOrder(serializedForm, this.props.order.orderId);
        }

    }

    _getAllowedStatuses(){
        return [
            {label:'canceled', name:'canceled'},
            {label:'in_progress', name:'in_progress'},
            {label:'complete', name:'complete'}]
    }

    _getAllowedCurrencies(){
        return [
            {label:'USD', name:'USD'},
            {label:'UAH', name:'UAH'}]
    }

    render () {
        let {order} = this.props.configuration.orders || {};
        const {
            history:{
                goBack
            }
        } = this.props;
        if(!order && this.props.isCreation === false) {
            return (
                <div className="row no-gutters fl--align-c margin--b-20">
                    Entity not found
                </div>
            );
        }
        return (
            <div className="col-8">
                <div className="link link__go-back icon--Rating_arrow" onClick={goBack}>
                    Назад
                </div>
                <form onSubmit={::this.submit} >
                    <Input 
                        label="Company id"
                        name="companyId"
                        value={ this.props.order.companyId || ''}
                        readonly={!this.props.isCreation}
                        wrapperClass="row fl--align-c margin--b-10"
                        labelClass="label form__label font--14 col-5"
                        inputWrapperClass="col-7 "
                        inputClass="input form__input"
                    />
                    <Combobox
                        label="currency"
                        name="currency"
                        list={this._getAllowedCurrencies()} 
                        value={this.props.order.currency}
                        readonly={!this.props.isCreation}
                        wrapperClass="row fl--align-c margin--b-20 combobox"
                        labelClass="label form__label font--14 col-5"
                        className="col-7"
                    />
                    <Textarea
                        label="Description"
                        name="description"
                        value={ this.props.description || ''}
                        readonly={!this.props.isCreation}
                        classes="row margin--b-20" 
                        labelClass="form__label col-5 font--14"
                        textAreaWrapper="col-7"
                        textAreaClass="form__text-area"
                    
                    />
                    {this._getStatusField()}

                    <Number 
                        label="Tokens counts"
                        name="orderItem.count"
                        value={ this.props.order.orderItem && this.props.order.orderItem.count || 0}
                        readonly={!this.props.isCreation}
                        max={10000}
                        wrapperClass="number row fl--align-c margin--b-10"
                        labelClass="label form__label font--14 col-5"
                        inputWrapperClass="col-7"
                    />
                    <p>Token price will be calculation using currency rate</p>
                    <br/>                    
                    <BtnSubmit
                        text="Сохранить"
                    />
                </form> 
            </div>
        )
    }

    _getStatusField(){
        if(!this.props.isCreation){
            return  <Combobox
                name="status"
                label="status"
                list={this._getAllowedStatuses()}
                value={this.props.order.status}
                wrapperClass="row fl--align-c margin--b-20 combobox"
                labelClass="label form__label font--14 col-5"
                className="col-7"
            />
        }
        return '';
    }
}

export default OrderView;
