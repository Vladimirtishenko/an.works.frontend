import * as types from '../constants/module.const.js';
import API from '../../../../../services/api.js'

export function getList() {

    return dispatch => {

        (async () => {

                let list = await API.__get('/dictionary/workSkills', dispatch, types.WS_LIST_LOADED_FAILED);

                if(list)
                {
                    dispatch({
                        type: types.WS_LIST_LOADED_SUCCESS,
                        workSkills: list
                    })
                }
        })();

    }
}

export function getItem(label) {

    return dispatch => {

        (async () => {

            let item = await API.__get('dictionary/workSkills/'+label, dispatch, types.WS_LOADED_FAILED);

            if(item)
            {
                dispatch({
                    type: types.WS_LOADED_SUCCESS,
                    workSkill: item
                })
            }
        })();

    }
}

export function createItem(data) {
    return dispatch => {
        (async () => {

            let item = await API.__post('POST', 'api/dictionary/workSkills', data, dispatch, types.WS_CREATED_FAILED);

            if(item)
            {
                dispatch({
                    type: types.WS_CREATED_SUCCESS,
                    workSkill: item
                })
            }

        })();

    }
}

export function updateItem(data, label) {
    return dispatch => {
        (async () => {

            let item = await API.__post('PATCH', 'api/dictionary/workSkills/'+label, data, dispatch, types.WS_UPDATE_FAILED);

            if(item)
            {
                dispatch({
                    type: types.WS_UPDATE_SUCCESS,
                    workSkill: item
                })
            }

        })();

    }
}