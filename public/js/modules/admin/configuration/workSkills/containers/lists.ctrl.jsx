import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as companiesActions from '../actions/action.js'

function mapStateToProps(state) {
    return {
        ...state,
        workSkills: state.configuration.ws && state.configuration.ws.workSkills || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...companiesActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import List from '../components/list.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class ListCtrl extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getList();
    }

	render() {

		return (
					<List {...this.props}/>
			   );
	}

}

export default ListCtrl;