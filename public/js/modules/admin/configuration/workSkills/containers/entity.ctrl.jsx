import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as companiesActions from '../actions/action.js'

function mapStateToProps(state) {
    return {
        ...state,
        workSkill: state.configuration.ws.workSkill || {},
        redirectToList: state.configuration.ws.redirectToList || false,
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...companiesActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import ItemView from '../components/item.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class EntityCtrl extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        if(this.props.match.params)
        {
            this.props.actions.getItem(this.props.match.params.id || null);
        }
    }

	render() {
        if(this.props.redirectToList === true)
        {
            this.props.history.push("/workskills");
        }
        let data = {isCreation: (this.props.match.params && this.props.match.params.id) ? false : true, ...this.props};
		return (
					<ItemView {...data}/>
			   );
	}

}

export default EntityCtrl;