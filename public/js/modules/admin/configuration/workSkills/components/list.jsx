import React from 'react'
import Table from '../../../../../components/tables/main.jsx'
import {Link} from 'react-router-dom';

//TODO::create schema
class ListView extends React.Component {


    createNew(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);

        this.props.actions.updateItem(serializedForm, this.props.workSkill.label);

    }

    render(){

        let schema  = {
            workSkills: {
                fields: [
                    {
                        name: 'label',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-22 font--left',
                        classNameTd:'respons-flex-table--w-22'
                    },
                    {
                        name: 'isMain',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-22 font--left',
                        classNameTd:'respons-flex-table--w-22'
                    },
                    {
                        name: 'name',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-22 font--left',
                        classNameTd:'respons-flex-table--w-22'
                    },
                    {
                        name: 'isLang',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-22 font--left',
                        classNameTd:'respons-flex-table--w-22'
                    }
                ],
                actions: {
                    edit: 'url[/workskills/:id]'
                },
                entityIdKey: 'label'
            }
        };
        let {workSkills} = schema,
            {i18n} = this.props;
        return (
            <div className="col-12">
                <Link to={'/workSkills_create/new'}> create new </Link>
                <Table i18n={i18n} data={this.props.workSkills || []} schema={workSkills}/>
            </div>
        )
    }
}

export default ListView;