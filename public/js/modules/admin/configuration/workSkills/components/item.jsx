import React from 'react'
// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Checkbox from '../../../../../components/form/checkbox.jsx';

// Componets
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

class ItemView extends React.Component {

    submit(event){
        event.preventDefault();

        let form = event && event.target;

        if(!form) return;
        let serializedForm = serialize(form);
        serializedForm.isMain = serializedForm.isMain === true;
        serializedForm.isLang = serializedForm.isLang === true;

        if(this.props.isCreation === true)
        {
            this.props.actions.createItem(serializedForm);
        }
        else {
            this.props.actions.updateItem(serializedForm, this.props.workSkill.label);
        }

    }

    render () {
        let {isMain, isLang, label, name} = this.props.workSkill || {};
        if(!this.props.workSkill && this.props.isCreation === false)
        {
            return (
                <div className="row no-gutters fl--align-c margin--b-20">
                    Entity not found
                </div>
            );
        }
        return (
            <form onSubmit={::this.submit} className="col-12">
                <Input 
                    label="Work Skill label"
                    name="label"
                    value={ label || ''}
                    // readonly={!this.props.isCreation}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                />
                <Input
                    label="Name"
                    name="name"
                    value={ name || ''}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                />
                <Checkbox 
                    wrapperClass="checkbox row  fl--align-c margin--b-10"
                    label="Is MainSkill"
                    name="isMain"
                    labelWrapperClass="col-lg-3"
                    value={[
                        {
                            name: `isMain`,
                            value: isMain,
                            checked: isMain,
                            innerWrapperClass: "d-inline_block font--14 font--color-secondary",
                            classNameInput: "checkbox__input--margin-r-10"
                        }
                    ]}
                    // readonly = {!this.props.isCreation}
                />
                <Checkbox
                    wrapperClass="checkbox row  fl--align-c margin--b-10"
                    label="Is lang"
                    name="isLang"
                    labelWrapperClass="col-lg-3"
                    value={[
                        {
                            name: `isLang`,
                            value: isLang,
                            checked: isLang,
                            innerWrapperClass:"d-inline_block font--14 font--color-secondary",
                            classNameInput: "checkbox__input--margin-r-10"
                        }
                    ]}
                    // readonly = {!this.props.isCreation}
                />
                <button className="btn btn--primary font--14">Submit</button>
            </form>
        )
    }
}

export default ItemView;
