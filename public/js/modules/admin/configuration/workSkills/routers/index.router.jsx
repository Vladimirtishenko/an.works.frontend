import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import List from '../containers/lists.ctrl.jsx'
import Enity from '../containers/entity.ctrl.jsx'
const routes = [
	{
		path: "/workskills",
		component: List
	},
	{
		path: "/workskills/:id",
		component: Enity
	},
	{
		path: "/workSkills_create/new",
		component: Enity
	}
]
export default routes;