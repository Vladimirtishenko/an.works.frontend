import * as types from '../constants/module.const';
const initialState = {
    hiringList:[]
};

export default function companies(state = initialState, action) {

    switch (action.type) {

        case types.WS_LIST_LOADED_SUCCESS:
            return {
                ...state,
                workSkills: action.workSkills,
                workSkill: null,
                redirectToList: false
            };
        case types.WS_LOADED_SUCCESS:
            return {
                ...state,
                workSkill: action.workSkill
            };
        case types.WS_CREATED_SUCCESS:
            return {
                ...state,
                workSkill: action.workSkill,
                redirectToList: true
            };
        case types.WS_UPDATE_SUCCESS:
            return {
                ...state,
                workSkill: action.workSkill,
                redirectToList: true
            };
        case types.WS_LIST_LOADED_FAILED:
        case types.WS_LOADED_FAILED:
        case types.WS_UPDATE_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        default:
            return state
    }
}