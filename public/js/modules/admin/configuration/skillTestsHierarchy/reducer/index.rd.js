import * as types from '../constants/testsHierarchy.const';
const initialState = {
    profiles:[]
};

export default function skillsTestsHierarchies(state = initialState, action) {

    switch (action.type) {

        case types.SKILLS_TESTS_HIERARCHIES_LOADED_SUCCESS:
            return {
                ...state,
                list: action.skillsTestsHierarchies
            };
        case types.SKILL_TESTS_HIERARCHY:
            return {
                ...state,
                skillTestsHierarchy: action.skillTestsHierarchy
            };
        case types.SKILLS_TESTS_HIERARCHIES_LOADED_FAILED:
        case types.SKILL_TESTS_HIERARCHY_FAILED:
        case types.SKILL_TESTS_HIERARCHY_UPDATED_FAILED:
            return {
                ...state,
                notification: action.notification
            };
        case types.KNOWLEDGES_LOADED_SUCCESS:
            return {
                ...state,
                knowledges: action.knowledges
            };

        default:
            return state
    }
}
