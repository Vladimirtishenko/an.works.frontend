import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as skillsTestsHierarchiesActions from '../actions/testsHierarchy.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        skillTestsHierarchy: state.configuration.skillsTestsHierarchies.skillTestsHierarchy || {}
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...skillsTestsHierarchiesActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import SkillTestsView from '../components/item.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class SkillTestsHierarchy extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        if(this.props.match.params && this.props.match.params.id)
        {
            this.props.actions.getSkillTestHierarchy(this.props.match.params.id || null);
        }
    }

	render() {
        let data = {isCreation: (this.props.match.params && this.props.match.params.id) ? false : true, ...this.props};
		return (
					<SkillTestsView {...data}/>
			   );
	}

}

export default SkillTestsHierarchy;