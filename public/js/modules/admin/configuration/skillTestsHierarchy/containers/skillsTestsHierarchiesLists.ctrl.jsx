import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

/* Components */
import TestsListView from '../components/list.jsx'

// Actions
import * as skillsTestsHierarchiesActions from '../actions/testsHierarchy.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        skillsTestsHierarchies: state.configuration.skillsTestsHierarchies.list || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...skillsTestsHierarchiesActions
        }, dispatch),
        dispatch
    }
}

@connect(mapStateToProps, mapDispatchToProps)
class SkillsTestsHierarchiesLists extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getTestsHierarchies();
    }

	render() {

		return (
					<TestsListView {...this.props}/>
			   );
	}

}

export default SkillsTestsHierarchiesLists;