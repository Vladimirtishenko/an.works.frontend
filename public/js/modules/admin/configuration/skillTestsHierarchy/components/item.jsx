import React from 'react'
// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import Date from '../../../../../components/form/date.jsx'

// Componets
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

class SkillTestsView extends React.Component {
    constructor(props){
        super(props);
        //this.state = {...this.props.skillTestsHierarchy}
    }

    componentDidMount(){
        if(this.props.configuration.skillsTestsHierarchies && !this.props.configuration.skillsTestsHierarchies.knowledges)
            this.props.actions.getWorkSkills();
    }

    submit(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);
        let tests = [];
        let rates = {};
        for(let i = 0 ; i < 5; i++)
        {
            let levelData = this.__getTestsDataFromStr(i, serializedForm);
            if(levelData)
                tests = tests.concat(this.__getTestsDataFromStr(i, serializedForm));
                rates[i] = serializedForm[i+'_level_ratio'] || 0;
        }

        let formData = {
            mainSkillLabel: serializedForm.skills.mainSkill,
            tests: tests,
            rates: rates
        };

        if(this.props.isCreation === true)
        {
            this.props.actions.createSkillTestHierarchy(formData, this.props.skillTestsHierarchy.mainSkill || formData.mainSkillLabel);
        }
        else {
            this.props.actions.updateSkillTestHierarchy(formData, this.props.skillTestsHierarchy.mainSkill || formData.mainSkillLabel);
        }

    }

    __getTestsDataFromStr(level, object)
    {
        let str = object[level+'_level_test'] || '';
        str = str.split(',');
        if(str.length > 0 && str[0] !== '')
        {
            return str.map(elem => {
                return {
                    level: level,
                    testLabel: elem
                };
            })
        }
    }

    __getTestsDataToStr(level, tests)
    {
        let testsFiltered = tests.filter(el => {
            return el.level == level;
        });
        testsFiltered = testsFiltered.map(el => { return el.testLabel});
        return testsFiltered.join(',');
    }

    render () {
        let {knowledges, skillTestsHierarchy} = this.props.configuration.skillsTestsHierarchies || {};
        if(!skillTestsHierarchy && this.props.isCreation === false)
        {
            return (
                <div className="row no-gutters fl--align-c margin--b-20">
                    Entity not found
                </div>
            );
        }
        skillTestsHierarchy = skillTestsHierarchy || {};
        return (
            <form onSubmit={::this.submit} className="col-12">
                <Combobox
                    wrapperClass=" combobox row no-gutters fl--align-c margin--b-20"
                    labelClass="form__label col-md-3"
                    dropDownClassName="col-md-4"
                    dropDownWrapper="col-md-4"
                    name="skills.mainSkill"
                    value={skillTestsHierarchy && skillTestsHierarchy.mainSkillLabel || null}
                    label="Основной навык"
                    list={
                        knowledges && knowledges.length && knowledges.filter((item, i) => {
                            if(!item.isLang && item.isMain){
                                return item
                            }
                        }) || []
                    }
                    readOnly={!this.props.isCreation}
                />
                <div className="row no-gutters fl--align-c margin--b-20">
                    <div className="col-md-3">
                        <label className="form__label">Уровень 0</label>
                    </div>
                    <div className="col-md-4">
                        <Input label="Ratio" name="0_level_ratio" value={ skillTestsHierarchy.rates && skillTestsHierarchy.rates[0] || 0 }/>
                        <Input label="Test Label" name="0_level_test" value={this.__getTestsDataToStr(0, skillTestsHierarchy.tests || [])}/>
                    </div>
                </div>
                <div className="row no-gutters fl--align-c margin--b-20">
                    <div className="col-md-3">
                        <label className="form__label">Уровень 1</label>
                    </div>
                    <div className="col-md-4">
                        <Input label="Ratio" name="1_level_ratio" value={ skillTestsHierarchy.rates && skillTestsHierarchy.rates[1] || 0 }/>
                        <Input label="Test Label" name="1_level_test" value={this.__getTestsDataToStr(1, skillTestsHierarchy.tests || [])}/>
                    </div>
                </div>
                <div className="row no-gutters fl--align-c margin--b-20">
                    <div className="col-md-3">
                        <label className="form__label">Уровень 2</label>
                    </div>
                    <div className="col-md-4">
                        <Input label="Ratio" name="2_level_ratio" value={ skillTestsHierarchy.rates && skillTestsHierarchy.rates[2] || 0 }/>
                        <Input label="Test Label" name="2_level_test" value={this.__getTestsDataToStr(2, skillTestsHierarchy.tests || [])}/>
                    </div>
                </div>
                <div className="row no-gutters fl--align-c margin--b-20">
                    <div className="col-md-3">
                        <label className="form__label">Уровень 3</label>
                    </div>
                    <div className="col-md-4">
                        <Input label="Ratio" name="3_level_ratio" value={ skillTestsHierarchy.rates && skillTestsHierarchy.rates[3] || 0 }/>
                        <Input label="Test Label" name="3_level_test" value={this.__getTestsDataToStr(3, skillTestsHierarchy.tests || [])}/>
                    </div>
                </div>
                <div className="row no-gutters fl--align-c margin--b-20">
                    <div className="col-md-3">
                        <label className="form__label">Уровень 4</label>
                    </div>
                    <div className="col-md-4">
                        <Input label="Ratio" name="4_level_ratio" value={ skillTestsHierarchy.rates && skillTestsHierarchy.rates[4] || 0 }/>
                        <Input label="Test Label" name="4_level_test" value={this.__getTestsDataToStr(4, skillTestsHierarchy.tests || [])}/>
                    </div>
                </div>
                <button className="btn btn--primary font--14">Submit</button>
            </form>
        )
    }
}

export default SkillTestsView;
