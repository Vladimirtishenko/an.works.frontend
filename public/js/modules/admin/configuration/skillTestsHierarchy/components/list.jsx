import React from 'react'
import Table from '../../../../../components/tables/main.jsx'
import {Link} from 'react-router-dom';

//TODO::create schema
class TestsListView extends React.Component {


    createNew(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);

        this.props.actions.updateCompany(serializedForm, this.props.company.name);

    }

	render(){

        let schema  = {
            skillsTestsHierarchies: {
                fields: [
                    {
                        name: 'mainSkillLabel',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25 font--left',
                        classNameTd:'respons-flex-table--w-25'
                    },
                    {
                        name: 'rates',
                        type: 'Object',
                        classNameTh:'respons-flex-table--w-25  font--left',
                        classNameTd:'respons-flex-table--w-25'
                    }
                ],
                actions: {
                    edit: 'url[/skills/testsHierarchy/edit/:id]'
                },
                entityIdKey: 'mainSkillLabel'
            }
        };

        let {skillsTestsHierarchies} = schema,
            {i18n} = this.props;
		return (
			<div className="col-12">
                <div className="margin--b-20">
                    <Link to={'/skills/testsHierarchy/new'} className="font--18 font--500"> Сreate New </Link>
                </div>
				<Table i18n={i18n} data={this.props.skillsTestsHierarchies} schema={skillsTestsHierarchies}/>
			</div>
		)
	}
}

export default TestsListView;