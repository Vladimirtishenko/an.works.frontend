import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import SkillsTestsHierarchiesLists from '../containers/skillsTestsHierarchiesLists.ctrl.jsx'
import SkillTestsHierarchy from '../containers/skillTestsHierarchy.ctrl.jsx'
const routes = [
	{
			path: "/skills/testsHierarchy",
			component: SkillsTestsHierarchiesLists
	},
	{
			path: "/skills/testsHierarchy/edit/:id",
			component: SkillTestsHierarchy
	},
	{
			path: "/skills/testsHierarchy/new",
			component: SkillTestsHierarchy
	}
]

export default routes;