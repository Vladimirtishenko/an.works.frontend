import * as types from '../constants/testsHierarchy.const.js';
import API from '../../../../../services/api.js'

export function getTestsHierarchies() {

    return dispatch => {

        (async () => {

                let skillsTestsHierarchies = await API.__get('/dictionary/skillsTestsHierarchy/', dispatch, types.SKILLS_TESTS_HIERARCHIES_LOADED_FAILED);

                if(skillsTestsHierarchies)
                {
                    dispatch({
                        type: types.SKILLS_TESTS_HIERARCHIES_LOADED_SUCCESS,
                        skillsTestsHierarchies: skillsTestsHierarchies
                    })
                }
        })();

    }
}

export function getSkillTestHierarchy(mainSkill) {

    return dispatch => {

        (async () => {

            let skillTestsHierarchy = await API.__get('/dictionary/skillsTestsHierarchy/'+mainSkill, dispatch, types.SKILL_TESTS_HIERARCHY_FAILED)

            if(skillTestsHierarchy)
            {
                dispatch({
                    type: types.SKILL_TESTS_HIERARCHY,
                    skillTestsHierarchy: skillTestsHierarchy
                })
            }
        })();

    }
}

export function createSkillTestHierarchy(data, mainSkill) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let skillTestsHierarchy = await API.__post('POST', 'api/dictionary/skillsTestsHierarchy/', data, dispatch, types.SKILL_TESTS_HIERARCHY_UPDATED_FAILED);

            if(skillTestsHierarchy)
            {
                dispatch({
                    type: types.SKILL_TESTS_HIERARCHY_UPDATED_SUCCESS,
                    skillTestsHierarchy: skillTestsHierarchy
                })
            }

        })();

    }
}

export function updateSkillTestHierarchy(data, mainSkill) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let skillTestsHierarchy = await API.__post('PATCH', 'api/dictionary/skillsTestsHierarchy/'+mainSkill, data, dispatch, types.SKILL_TESTS_HIERARCHY_UPDATED_FAILED);

            if(skillTestsHierarchy)
            {
                dispatch({
                    type: types.SKILL_TESTS_HIERARCHY_UPDATED_SUCCESS,
                    skillTestsHierarchy: skillTestsHierarchy
                })
            }

        })();

    }
}

export function getWorkSkills() {
    return dispatch => {
        (async () => {
            let knowledges = await API.__get('dictionary/workSkills/', dispatch, types.KNOWLEDGES_LOADED_FAIL)
            dispatch({
                type: types.KNOWLEDGES_LOADED_SUCCESS,
                knowledges: knowledges
            })

        })();

    }
}
