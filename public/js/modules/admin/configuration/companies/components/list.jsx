import React from 'react'
import CompanyList from './CompanyList.jsx';

//TODO::create schema
class CompaniesListView extends React.Component {
	render(){

        let schema  = {
            companies: {
                fields: [
                    {
                        name: 'companyId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25',
                        classNameTd:'respons-flex-table--w-25'
                    },
                    {
                        name: 'name',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25',
                        classNameTd: 'respons-flex-table--w-25'
                    },
                    {
                        name: 'status',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25',
                        classNameTd:'respons-flex-table--w-25'
                    },
                    {
                        name: 'status',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-25',
                        classNameTd:'respons-flex-table--w-25'
                    }
                    
                ],
                actions: {
                    edit: 'url[/companies/:id]'
                },
                entityIdKey: 'companyId'
            }
        };

        let {companies} = schema,
            {i18n} = this.props;
		return (
			<div className="col-12">
                <CompanyList i18n={i18n} data={this.props.companies} schema={companies}/>
			</div>
		)
	}
}

export default CompaniesListView;