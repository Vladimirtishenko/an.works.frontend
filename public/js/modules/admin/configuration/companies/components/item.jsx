import React from 'react'
// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import Date from '../../../../../components/form/date.jsx';
import BtnSubmit from '../../applicantPprofiles/components/button/btnSave.jsx';
// Componets
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js';
import InfoCompany from './info.jsx';

class CompanyView extends React.Component {
    constructor(props){
        super(props);
        this.state = {...this.props.company}
    }

    submit(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);

        this.props.actions.updateCompany(serializedForm, this.props.company.companyId);

    }

    _getAllowedStatuses(){
        return [
            {label:'waitingApprove', name:'waitingApprove'},
            {label:'active', name:'active'},
            {label:'inActive', name:'inActive'}]
    }

    render () {
        const {
            actions,
            history:{
                goBack
            }
        } = this.props;
        return (
            <div className="container">
                <div className="row fl--dir-col">
                    <div className="link link__go-back icon--Rating_arrow" onClick={goBack}>
                        Назад
                    </div>
                    <div>

                    </div>
                </div>
                <div>
                    <div className="padding--yc-20 border--b-1-gray"> 
                        <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                            Информация
                        </p>
                        <InfoCompany
                            {...this.props}
                        />
                    </div>
                    <div className="padding--yc-20">
                        <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                            Статус
                        </p>
                        <form onSubmit={::this.submit}>
                            <Combobox
                                name="status"
                                wrapperClass="row fl--align-c margin--b-20 combobox"
                                labelClass="label form__label font--14 col-4"
                                className="col-5"
                                label="Статус"
                                list={this._getAllowedStatuses()}
                                value={this.props.company.status}
                            />
                            <BtnSubmit/>
                        </form>
                    </div>
                    <div className="link link__go-back icon--Rating_arrow" onClick={goBack}>
                        Назад
                    </div>
                </div>
            </div>
           
        )
    }
}

export default CompanyView;
