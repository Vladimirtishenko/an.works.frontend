import * as types from '../constants/companies.const';
const initialState = {
    profiles:[]
};

export default function companies(state = initialState, action) {

    switch (action.type) {

        case types.COMPANIES_LOADED_SUCCESS:
            return {
                ...state,
                list: action.companies
            };
        case types.COMPANY_LOADED_SUCCESS:
            return {
                ...state,
                company: action.company
            };
        case types.COMPANY_UPDATED_SUCCESS:
            return {
                ...state,
                company: action.company
            };
        case types.COMPANIES_LOADED_FAILED:
        case types.COMPANY_LOADED_FAILED:
        case types.COMPANY_PROFILE_UPDATED_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        default:
            return state
    }
}