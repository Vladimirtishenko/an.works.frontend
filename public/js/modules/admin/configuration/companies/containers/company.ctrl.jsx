import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as companiesActions from '../actions/companies.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        company: state.configuration.companies.company || {}
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...companiesActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import CompanyView from '../components/item.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class Company extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        if(this.props.match.params)
        {
            this.props.actions.getCompany(this.props.match.params.id || null);
        }
    }

	render() {

		return (
					<CompanyView {...this.props}/>
			   );
	}

}

export default Company;