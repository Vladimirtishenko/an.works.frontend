import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as companiesActions from '../actions/companies.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        companies: state.configuration.companies.list || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...companiesActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import CompaniesListView from '../components/list.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class CompaniesList extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getCompanies();
    }

	render() {

		return (
					<CompaniesListView {...this.props}/>
			   );
	}

}

export default CompaniesList;