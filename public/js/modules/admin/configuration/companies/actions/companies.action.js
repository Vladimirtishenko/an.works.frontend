import * as types from '../constants/companies.const.js';
import API from '../../../../../services/api.js'

export function getCompanies() {

    return dispatch => {

        (async () => {

                let companies = await API.__get('companies', dispatch, types.COMPANIES_LOADED_FAILED);

                if(companies)
                {
                    dispatch({
                        type: types.COMPANIES_LOADED_SUCCESS,
                        companies: companies
                    })
                }
        })();

    }
}

export function getCompany(name) {

    return dispatch => {

        (async () => {

            let company = await API.__get('companies/'+name, dispatch, types.COMPANY_LOADED_FAILED)

            if(company)
            {
                dispatch({
                    type: types.COMPANY_LOADED_SUCCESS,
                    company: company
                })
            }
        })();

    }
}

export function updateCompany(data, companyName) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let companyProfile = await API.__post('PATCH', 'api/companies/'+companyName, data, dispatch, types.COMPANY_PROFILE_UPDATED_FAILED);

            if(companyProfile)
            {
                dispatch({
                    type: types.COMPANY_PROFILE_UPDATED_SUCCESS,
                    company: companyProfile
                })
            }

        })();

    }
}