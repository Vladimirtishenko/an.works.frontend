import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
  Route,
	Switch
} from 'react-router-dom';

import CompaniesList from '../containers/companiesLists.ctrl.jsx'
import Company from '../containers/company.ctrl.jsx'

const routes = [
	{
			path: "/companies",
			component: CompaniesList
	},
	{
			path: "/companies/:id",
			component: Company
	}
]
export default routes;