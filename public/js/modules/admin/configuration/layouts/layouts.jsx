import React from "react";
import SideBar from "../sidebar/components/sidebar.jsx";

class Layout extends React.Component {
    render() {
        let { children } = this.props;

        return (
            <React.Fragment>
                <SideBar />
                {children}
            </React.Fragment>
        );
    }
}

export default Layout;
