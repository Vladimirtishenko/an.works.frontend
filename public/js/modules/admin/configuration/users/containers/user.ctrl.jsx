import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as applicantActions from '../actions/users.action.js'

/* Components */
import UsersListView from '../components/item.jsx'

function mapStateToProps(state) {
    return {
        ...state,
        users: state.configuration.users.list || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...applicantActions
        }, dispatch),
        dispatch
    }
}



@connect(mapStateToProps, mapDispatchToProps)
class User extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        const { 
            match:{
                params :{
                    uid 
                }
            }
        } = this.props;
        this.props.actions.getUser(uid);
    }

	render() {

		return (
            <UsersListView {...this.props}/>
        );
	}

}

export default User;