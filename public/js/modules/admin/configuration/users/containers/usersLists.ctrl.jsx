import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as applicantActions from '../actions/users.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        users: state.configuration.users.list || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...applicantActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import UsersListView from '../components/list.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class UsersList extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getUsers();
    }

	render() {

		return (
					<UsersListView {...this.props}/>
			   );
	}

}

export default UsersList;