import * as types from '../constants/users.const.js';
import API from '../../../../../services/api.js'

export function getUsers() {

    return dispatch => {

        (async () => {

                let users = await API.__get('users', dispatch, types.USERS_LOADED_FAILED);

                if(users)
                {
                    dispatch({
                        type: types.USERS_LOADED_SUCCESS,
                        users: users
                    })
                }
        })();

    }
}
export function getUser(uid) {

    return dispatch => {

        (async () => {

                let users = await API.__get('users/' + uid, dispatch, types.USERS_LOADED_FAILED);

                if(users)
                {
                    dispatch({
                        type: types.USERS_LOADED_SUCCESS,
                        users: users
                    })
                }
        })();

    }
}

export function updateUser(data, uid) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__post(
                "PATCH",
                "api/users/" + uid,
                data,
                dispatch,
                types.USERS_LOADED_FAILED
            );
        })();
    };
}
