import React from 'react'
import Status from './status.jsx'
import Password from './password.jsx'
class Users extends React.Component {

    render () {
        const {
            users,
            users:{
                email,
                name
            } = {},
            actions,
            i18n:{
                translation = ''
            } = {}
        } = this.props;
        return (
            <div className="col-12 fl">
               <div className="col-6 fl fl--wrap">
               <div className="width--100 fl margin--b-20">
                    <div className="col-6">
                        Email                
                    </div>
                    <div className="col-6">
                        {email}
                    </div>
               </div>
               <div className="width--100 fl margin--b-20">
                    <div className="col-6">
                        Name                
                    </div>
                    <div className="col-6">
                        {name}
                    </div>
                </div>
                <div className="width--100 margin--t-30">
                    <Status users={users} actions={actions} />
                </div>
               </div>
               <div className="col-6">
                <Password translation={translation} actions={actions} users={users}/>
                </div>
            </div>
        );
    }  
}

export default Users;
