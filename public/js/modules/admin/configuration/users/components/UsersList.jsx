import React from 'react'
import {Link} from 'react-router-dom'
import Actions from '../../../../../components/tables/actions.jsx'
import uniqid from 'uniqid';
import {dmyDate} from '../../../../../helpers/date.helper.js'
class UsersList extends React.Component {

    static defaultProps = {
            gmtClassName: 'respons-flex-table__td  respons-flex-table--w-10 l-h--1 respons-flex-table--font-14 font--center',
            roleClassName: 'respons-flex-table__td  respons-flex-table--w-15 l-h--1 respons-flex-table--font-14 font--center',
            emailClassName: 'respons-flex-table__td  respons-flex-table--w-30 l-h--1 respons-flex-table--font-14 font--center',
            nameClassName: 'respons-flex-table__td  respons-flex-table--w-20 l-h--1 respons-flex-table--font-14 font--center',
            statusClassName: 'respons-flex-table__td  respons-flex-table--w-15 l-h--1 respons-flex-table--font-14 font--center',
            editClassName: 'respons-flex-table__td  respons-flex-table--w-10 l-h--1 respons-flex-table--font-14 font--center'
    }

    render () {
        const {
            data,
            gmtClassName,
            roleClassName,
            emailClassName,
            nameClassName,
            statusClassName,
            editClassName
        } = this.props;

        return (
                <table className="respons-flex-table box--white">
                    <thead className="respons-flex-table__thead respons-flex-table__thead--admin-theme">
                        <tr className="respons-flex-table__tr" >
                            <th className={gmtClassName}>
                                CreatedDate
                            </th>
                            <th className={roleClassName}>
                                Role
                            </th>
                            <th className={emailClassName}>
                                Email
                            </th>
                            <th className={nameClassName}>
                                name
                            </th>
                            <th className={statusClassName}>
                                isActive
                            </th>
                            <th className={editClassName}>
                                Изменить
                            </th>
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                    {
                        data.length && data.map((item, i) => {
                                return (
                                    <tr key={uniqid(i + '-')} data-id={item.id} className="respons-flex-table__tr" >
                                        <td key={uniqid(i + '-')} className={gmtClassName}>
                                            {dmyDate(item.createdDate)}
                                        </td>
                                        <td key={uniqid(i + '-')} className={roleClassName}>
                                            {item.role}
                                        </td>
                                        <td key={uniqid(i + '-')} className={`${emailClassName} font--color-blue`}>
                                            {item.email}
                                        </td>
                                        <td key={uniqid(i + '-')} className={nameClassName}>
                                            {item.name}
                                        </td>
                                        <td key={uniqid(i + '-')} className={statusClassName}>
                                            {String(item.isActive)}
                                        </td>
                                        <td key={uniqid(i + '-')} className={editClassName}>
                                            <Link to={`/users/${item.uid}`} className="font--color-blue link">
                                                Начать
                                            </Link>
                                        </td>
                                    </tr>
                                );
                        }) || null
                    }

                    </tbody>
                </table>
        );
    }  
}

export default UsersList;
