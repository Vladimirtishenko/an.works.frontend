import React from 'react'
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import BtnSubmit from '../../applicantPprofiles/components/button/btnSave.jsx';
import serialize from '../../../../../helpers/serialize.helper.js';
import Combobox from '../../../../../components/form/combobox.jsx';

class Users extends React.Component {

    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;
        let serializedForm = serialize(form),
            {
                users:{
                    uid 
                }
             } = this.props;
        if(serializedForm.isActive == 'true'){
            serializedForm.isActive = true
        }else if(serializedForm.isActive == 'false'){
            serializedForm.isActive = false
        }
        this.props.actions.updateUser(serializedForm, uid);
    }

    _getAllowedStatuses(){
        return [
            {label:true, name:'true'},
            {label:false, name:'false'}
        ]
    }

    render () {
        const {
            users:{
                isActive
            }
        } = this.props;
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                <Combobox
                    name="isActive"
                    wrapperClass="row fl--align-c margin--b-20 combobox"
                    labelClass="label form__label font--14 col-4"
                    className="col-5"
                    label="Статус"
                    list={this._getAllowedStatuses()}
                    value={isActive}
                />
                <BtnSubmit/>
            </ValidatorForm>
        );
    }  
}

export default Users;
