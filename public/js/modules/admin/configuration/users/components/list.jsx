import React from 'react'
import UsersList from './UsersList.jsx'

//TODO::create schema
class UsersListView extends React.Component {
	render(){

        let schema  = {
            applicants: {
                fields: [
                    {
                        name: 'GMT',
                        type: 'String',
                        classNameTh: 'respons-flex-table--w-10 l-h--1 respons-flex-table--font-14 font--left',
                        classNameTd: 'respons-flex-table__td respons-flex-table__td--no-before  respons-flex-table--w-10 l-h--1 respons-flex-table--font-14 font--color-primary font--upparcase'
                    },
                    {
                        name: 'role',
                        type: 'String',
                        classNameTh: 'respons-flex-table--w-15 l-h--1 respons-flex-table--font-14 font--left',
                        classNameTd: 'respons-flex-table__td respons-flex-table__td--no-before  respons-flex-table--w-15 l-h--1 respons-flex-table--font-14 font--color-primary font--upparcase'
                    },
                    {
                        name: 'email',
                        type: 'String',
                        classNameTh: 'respons-flex-table--w-30 l-h--1 respons-flex-table--font-14 font--left',
                        classNameTd: 'respons-flex-table__td respons-flex-table__td--no-before  respons-flex-table--w-30 l-h--1 respons-flex-table--font-14 font--color-blue font--underlined'
                    },
                    {
                        name: 'name',
                        type: 'String',
                        classNameTh: 'respons-flex-table--w-20 l-h--1 respons-flex-table--font-14 font--center',
                        classNameTd: 'respons-flex-table__td respons-flex-table__td--no-before  respons-flex-table--w-20 l-h--1 respons-flex-table--font-14 font--center font--color-primary font--upparcase'
                    }, 
                    {
                        name: 'isActive',
                        type: 'String',
                        classNameTh: 'respons-flex-table--w-15 l-h--1 respons-flex-table--font-14 font--center',
                        classNameTd: 'respons-flex-table__td respons-flex-table__td--no-before  respons-flex-table--w-15 l-h--1 respons-flex-table--font-14 font--center font--color-primary font--upparcase'
                    }
                ],
                actions: {
                    edit: 'url[/users/:uid]'
                }
            }
        };

        let {applicants} = schema,
            {i18n} = this.props;
		return (
            <div className="col-md-12">
                <UsersList i18n={i18n} data={this.props.users} schema={applicants} />
            </div>
		)
	}
}

export default UsersListView;