import React from 'react'
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import PasswordDoubleField from "../../../../../components/form/passwordComparing.jsx";
import BtnSubmit from '../../applicantPprofiles/components/button/btnSave.jsx';
import serialize from '../../../../../helpers/serialize.helper.js';
import Combobox from '../../../../../components/form/combobox.jsx';

class Users extends React.Component {

    submit(event) {
        event.preventDefault();

        const { target: form } = event,
                serializedForm = serialize(form),
                { password } = serializedForm, 
                { actions: { updateUser }, users: { uid } } = this.props;

        updateUser({password: password}, uid);
    }

    render () {
        const {
            translation = {}
        } = this.props;
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                <PasswordDoubleField 
                    validators={[
                        "required",
                        "isPassword",
                        "isPasswordComparing"
                    ]}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessagePassword, translation.errorMessagePasswordsEqually]}
                    required={true}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    list={[
                        {label: translation.newPassword || '', name: "password", placeholder: translation.newPassword || '', type: "password"},
                        {label: translation.redoNewPassword || '', name: "repeat-password", placeholder: translation.repeatPassword || '', type: "password", btn: true}
                    ]}
                />
                <BtnSubmit/>
            </ValidatorForm>
        );
    }  
}

export default Users;
