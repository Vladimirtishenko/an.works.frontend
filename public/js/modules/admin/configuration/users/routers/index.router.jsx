import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

import UsersList from '../containers/usersLists.ctrl.jsx'
import User from '../containers/user.ctrl.jsx'

const routes = [
	{
			path: "/users",
			component: UsersList
	},
	{
			path: "/users/:uid",
			component: User
	}
]

export default routes;