import * as types from '../constants/users.const';
const initialState = {
    profiles:[]
};

export default function users(state = initialState, action) {

    switch (action.type) {

        case types.USERS_LOADED_SUCCESS:
            return {
                ...state,
                list: action.users
            };
        case types.USERS_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        default:
            return state
    }
}