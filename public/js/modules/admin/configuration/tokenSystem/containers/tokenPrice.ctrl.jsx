import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as ordersActions from '../actions/tokenPrice.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        tokenPrice: state.configuration.token_system.tokenPrice || {},
        tokenPriceHistory: state.configuration.token_system.tokenPriceHistory || [],
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...ordersActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import TokenPriceView from '../components/item.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class TokenPrice extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getTokenPrice();
        this.props.actions.getTokenPriceHistory();
    }

	render() {
		return (
					<TokenPriceView {...this.props}/>
			   );
	}

}

export default TokenPrice;