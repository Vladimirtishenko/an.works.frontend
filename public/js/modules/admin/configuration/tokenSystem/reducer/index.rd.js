import * as types from '../constants/token.const';
const initialState = {
    tokenPriceHistory:[],
    tokenPrice: {}
};

export default function token_system(state = initialState, action) {

    switch (action.type) {

        case types.TOKEN_PRICE_HISTORY_LOADED_SUCCESS:
            return {
                ...state,
                tokenPriceHistory: action.tokenPriceHistory
            };
        case types.TOKEN_PRICE_LOADED_SUCCESS:
        case types.TOKEN_PRICE_UPDATED_SUCCESS:
            return {
                ...state,
                tokenPrice: action.tokenPrice
            };
        case types.TOKEN_PRICE_LOADED_FAILED:
        case types.TOKEN_PRICE_HISTORY_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };
        default:
            return state
    }
}