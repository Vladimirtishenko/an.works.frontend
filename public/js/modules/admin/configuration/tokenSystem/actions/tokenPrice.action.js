import * as types from '../constants/token.const.js';
import API from '../../../../../services/api.js'

export function getTokenPrice() {

    return dispatch => {

        (async () => {

                let tokenPrice = await API.__get('token_price_rate/', dispatch, types.TOKEN_PRICE_LOADED_FAILED);

                if(tokenPrice)
                {
                    dispatch({
                        type: types.TOKEN_PRICE_LOADED_SUCCESS,
                        tokenPrice: tokenPrice
                    })
                }
        })();

    }
}

export function getTokenPriceHistory() {

    return dispatch => {

        (async () => {

            let tokenPriceHistory = await API.__get('token_price_rate_history/', dispatch, types.TOKEN_PRICE_HISTORY_LOADED_FAILED)

            if(tokenPriceHistory)
            {
                dispatch({
                    type: types.TOKEN_PRICE_HISTORY_LOADED_SUCCESS,
                    tokenPriceHistory: tokenPriceHistory
                })
            }
        })();

    }
}

export function createTokenPrice(data) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let tokenPrice = await API.__post('POST', 'api/token_price_rate/', data, dispatch, types.TOKEN_PRICE_UPDATED_FAILED);

            if(tokenPrice)
            {
                dispatch({
                    type: types.TOKEN_PRICE_UPDATED_SUCCESS,
                    tokenPrice: tokenPrice
                });
                getTokenPriceHistory()(dispatch);
            }

        })();

    }
}

