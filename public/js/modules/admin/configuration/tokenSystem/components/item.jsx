import React from 'react'
import uniqid from 'uniqid';
// Form Componets
import {dmyDate} from '../../../../../helpers/date.helper.js';
import Input from '../../../../../components/form/text.jsx';
// import Table from '../../../../../components/tables/main.jsx'
import BtnSubmit from '../../applicantPprofiles/components/button/btnSave.jsx';
import Number from '../../../../../components/form/number.jsx'
// Componets
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

class TokenPriceView extends React.Component {
    constructor(props){
        super(props);
    }

    submit(event){
        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);
        let dataToCreate = {
            currenciesRate:{
                USD: parseFloat(serializedForm.USD),
                UAH: parseFloat(serializedForm.UAH),
            }
        };
        //should be number
        this.props.actions.createTokenPrice(dataToCreate);
    }

    render () {
        let {i18n,tokenPriceHistory} = this.props;
        let tokenPrice = this.props.tokenPrice || null;
        if(!tokenPrice){
            return (
                <div className="row no-gutters fl--align-c margin--b-20">
                    Entity not found
                </div>
            );
        }
        return (
            <div className="col-12">
                <div className="margin--b-20">
                    <form onSubmit={::this.submit} >
                        <Input
                            className="number"
                            label="Cost in USD"
                            name="USD"
                            value={ tokenPrice.currenciesRate && tokenPrice.currenciesRate['USD']}
                            step="0.01"
                            wrapperClass="number row fl--align-c margin--b-10"
                            labelClass="label form__label font--14 col-5"
                            inputWrapperClass="col-7"
                        />

                        <Input
                            className="number" 
                            label="Cost in UAH"
                            name="UAH"
                            value={ tokenPrice.currenciesRate && tokenPrice.currenciesRate['UAH']}
                            inputType="number"
                            step="0.01"
                            wrapperClass="number row fl--align-c margin--b-10"
                            labelClass="label form__label font--14 col-5"
                            inputWrapperClass="col-7"
                        />
                    <BtnSubmit text="Сохранить"/>
                    </form>
                </div>
                <div>
                    <table className="respons-flex-table box--white">
                        <thead className="respons-flex-table__thead respons-flex-table__thead--admin-theme">
                            <tr className="respons-flex-table__tr" >
                                <th className="respons-flex-table__th respons-flex-table--w-30 font--left">
                                    Курс валюты
                                </th>
                                <th className="respons-flex-table__th respons-flex-table--w-30 font--left">
                                    Дата обновления
                                </th>
                            </tr>
                        </thead>
                        <tbody className="respons-flex-table__tbody">
                        {
                            tokenPriceHistory.map((item, i) => {
                                let {UAH,USD} = item.currenciesRate;
                                return (
                                    <tr key={uniqid(i + '-')} data-id={item.id} className="respons-flex-table__tr" >
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-30">
                                            {`USD : ${USD}`} {`UAH : ${UAH}`}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-30">
                                            {dmyDate(item.updatedDate)}
                                        </td>
                                    </tr>
                                );
                            })
                        }
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
}

export default TokenPriceView;
