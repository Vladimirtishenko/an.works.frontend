import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

import TokenPrice from '../containers/tokenPrice.ctrl.jsx';

const routes = [
	{
			path: "/token_price",
			component: TokenPrice
	}
]
export default routes;