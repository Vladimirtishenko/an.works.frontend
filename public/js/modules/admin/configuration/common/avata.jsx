import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import uniqid from 'uniqid';

import * as userAction from './actions/user.action.js'
//helpers
import { getAvatarUrlByUID} from '../../../../helpers//avatar.helper.js'

function mapStateToProps(state) {

    return {
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...userAction
        }, dispatch),
        dispatch
    }
}

@connect(mapStateToProps, mapDispatchToProps)
class Avatar extends React.Component {

    constructor(props){
        super(props); 

        const { userAvatar } = props;

        this.state = {
            avatar: userAvatar
        }
    }
	static defaultProps = {
		load: true,
	}
    componentDidMount(){
        const { uid } = this.props;

        this.loadImage(uid);
    }

    loadImage(uid){

        if(!uid) return;

        const { actions: { updateUserAvatarWithoutRequest } } = this.props,
              img = new Image(),
              src = getAvatarUrlByUID(uid),
              id = uniqid((new Date()).valueOf());

        img.src = src;
        img.onload = () => {
            updateUserAvatarWithoutRequest(`${src}?id=${id}`)
        };

        img.onerror = () => {
            updateUserAvatarWithoutRequest("/img/NoPhoto.jpg");
        };
    }

    componentDidUpdate(prevProps, prevState){

        const { uid:prevUid, uniq: prevUniq, userAvatar: prevAvatar  } = prevProps,
              { uid, uniq: currentUniq, userAvatar: currentAvatar } = this.props;
        if(prevUniq !== currentUniq || prevUid !== uid){
            this.loadImage(uid); 
        }

        if(currentAvatar && prevAvatar !== currentAvatar){
            this.setState({
                avatar: currentAvatar
            })
        }

    }

    readUrl(input){
        const { actions: { updateUserAvatar }, uid  } = this.props;

        if (input.files && input.files[0]) {
            updateUserAvatar(uid, input.files[0])
        }
    }

    onInputChange(event){
        this.readUrl(event.target);
    }

    handleError() {
        this.setState({avatar: "/img/NoPhoto.jpg"});
    }

    render() {
        const { uid, wrapperClass,load } =  this.props,
              { avatar } = this.state;
        return (
            <section className={`avatar avatar--sidebar-admin ${wrapperClass}`}>
                <div className="absolute absolute--r-10 absolute--t-10">
                    <div className="link link--tooltip-anchor-white tooltip-avatar z--1" data-title="опасный нига тут" ></div>
                </div>
                <div className="avatar__img-wrap">
                    <img className="avatar__default img-fluid" id="avatar"src={avatar}></img>
                </div>
                <button className="btn btn--mobile btn--grey">
                    <span className="icon icon--upload"></span>
                    Загрузить фото
                </button>
                { load && 
                    (
                        <div className="avatar__controls padding--yc-5">
                            <div className="avatar__edit">
                                <label className="link icon icon--upload margin--r-15 font--18 font--color-white">
                                    <input onChange={::this.onInputChange} style={{display: 'none'}} type="file" id="avatar_input" accept="image/*"/>
                                </label>
                                {/* TODO: not mvp <div className="link icon icon--delete margin--r-8 font--18 font--color-white"></div> */}
                            </div>
                        </div>
                    ) || null
                }
            </section>
        )
    }
}

export default Avatar;
