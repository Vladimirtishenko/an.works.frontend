import * as types from '../../../../oauth/constants/oauth.const.js';
import API from '../../../../../services/api.js'

import uniqid from 'uniqid';

export function updateUserAvatar(uid, file) {

    return dispatch => {

        (async () => {
            const formData = new FormData();
            formData.append('upload_'+random(), file);

            const updateData = await API.__files(null, formData, "api/users/"+uid+"/avatarUpload");

            if(updateData)
            {
                dispatch({
                    type: types.USER_DATA_UPDATED,
                    uniq: uniqid(),
                    userAvatar: updateData.userAvatar
                })
            }
        })();

    }
}

export function updateUserAvatarWithoutRequest(src) {

    return dispatch => {

        dispatch({
            type: types.USER_DATA_UPDATED,
            userAvatar: src
        })

    }
}
