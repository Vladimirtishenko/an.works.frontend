import React from 'react';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux'

function mapStateToProps(state) {

    return {
        modules: state.configuration.sidebar.modules || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        dispatch
    }
}

@connect(mapStateToProps, mapDispatchToProps)
class SideBar extends React.Component {

    collapsed(item, children){
        return (
            <div className="" >
                <div className="">
                    <p className="">{item.name}</p>
                </div>
                <ul className={``}>
                    {children}
                </ul> 
            </div>

        )

    }

    nonCollapsed(child){

        return (
            <NavLink 
                to={child.url}
                className="padding--xc-10 padding--yc-20 link link--sidebar"
            >
                {child.name}
            </NavLink>
        )
    }

    children(item, j){

        let children = item && item.children && item.children.map((child, i) => {

                return (
                    <li key={i} className="padding--xc-10">
                        {::this.nonCollapsed(child)}
                    </li>
                )

            }) || null;

        return (
            <li key={j} className="list--style-type-none">
                {
                    children ? (::this.collapsed(item, children)) : (::this.nonCollapsed(item))
                }
               
            </li>
        )
    }


    render() {
        let links = this.props.modules.map((item, i) => (this.children(item, i)))

        return (
            <div className="admin-sidebar">
                <nav className="">
                    <ul className="fl list-style-type--none">
                        {links}  
                    </ul>
                </nav>
            </div>

        );
    }


}

export default SideBar;