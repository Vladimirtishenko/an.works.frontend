import * as types from '../constant/sidebar.const.js';

export function active(actives) {
    return {
        type: types.SIDE_BAR_ACTIVE_LINK,
        payload: actives || types.DEFAULT_COMPONENTS
    };
}