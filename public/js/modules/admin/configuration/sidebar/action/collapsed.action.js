import * as types from '../constant/sidebar.const.js';

export function collapsed(id) {
    return {
        type: types.SIDE_BAR_ITEM_COLLAPSED,
        payload: id
    };
}
