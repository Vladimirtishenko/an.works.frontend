import * as types from '../constant/sidebar.const.js';

export function view(view) {
    return {
        type: types.SIDE_BAR_LIST,
        payload: view
    };
}
