import React from 'react'
import {Link} from 'react-router-dom';
import ShowSidebar from '../components/sidebar.jsx';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'

import * as active from '../action/active.action.js'
import * as show from '../action/show.action.js'
import * as collapsed from '../action/collapsed.action.js'

import {DEFAULT_COMPONENTS} from '../constant/sidebar.const.js';


function mapStateToProps (state) {
  return {...state.configuration.sidebar}
}

function mapDispatchToProps(dispatch) {

  return {
          actions: bindActionCreators({
              ...active,
              ...show,
              ...collapsed
          }, dispatch),
          dispatch
        }

}

class Sidebar extends React.Component {

	constructor (props, context) {
        super(props, context);
  }

  componentDidMount(){

      let {pathname} = this.context.router.route.location;

      this.props.actions.active(pathname);
  }

  activeSidebarState(event, url){

      this.props.actions.active(url);
  }

  collapsingSidebarState(event, id){

      this.props.actions.collapsed(id);

  }

	render() {

    return (
        <ShowSidebar 
          collapsingSidebarState={::this.collapsingSidebarState} 
          activeSidebarState={::this.activeSidebarState} 
          {...this.props}
        />
    );
		
	}

}

Sidebar.contextTypes = {
  router: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);