import * as types from '../constant/sidebar.const.js';
import modules from '../../../../../configuration/modules.json'

const initialState = {
    name: 'modules',
    modules: modules,
    view: true,
    notification: null
};

export default function sidebar(state = initialState, action) {
    switch (action.type) {
        case types.SIDE_BAR_STATE:
            return {...state, view: action.payload}

        case types.SIDE_BAR_ACTIVE_LINK:

            let object = state.modules,
                collapsed = 0,
                found = 0;


            (function customizer(obj, parent){

                for (let i = 0; i < obj.length; i++) {

                    if(obj[i].children){

                        ++collapsed;

                        customizer(obj[i].children, 1);

                        if(found && collapsed){
                            obj[i].collapsed = true;
                            --collapsed;
                            if(!collapsed){--found;}
                        }
                    }

                    let url = '/' + action.payload.slice(1).split('/').splice(0,2).join('/');

                    if (url == obj[i].url  && parent){
                        obj[i].active = true;
                        ++found;
                    } else if (url == obj[i].url && !parent){

                        obj[i].active = true;
                    } else {
                        obj[i].active = false;
                    }

                }

            })(object);

            return {
                ...state, 
                modules: [...object]
            }

        case types.SIDE_BAR_ITEM_COLLAPSED:
            return {
                ...state,
                modules: state.modules.map((item) => {
                    if(item.id == action.payload && item.collapsed == false){
                        item.collapsed = true;
                    } else if(item.id == action.payload && item.collapsed == true){
                        item.collapsed = false;
                    }
                    return item;
                })
            }

        default:
            return state;
    }
}