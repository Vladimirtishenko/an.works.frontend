import * as types from '../constants/module.const.js';
import API from '../../../../../services/api.js'

export function getUnresolvedHiringList() {

    return dispatch => {

        (async () => {

                let list = await API.__get('hiring_contacts/unresolved/', dispatch, types.UNRESOLVED_HIRING_LIST_LOADED_FAILED);

                if(list)
                {
                    dispatch({
                        type: types.UNRESOLVED_HIRING_LIST_LOADED_SUCCESS,
                        hiringList: list
                    })
                }
        })();

    }
}

export function getUnresolvedHiring(companyId, profileId) {

    return dispatch => {

        (async () => {

            let hiring = await API.__get('/hiring_contacts/unresolved/admin/'+companyId+'/'+profileId, dispatch, types.UNRESOLVED_HIRING_LOADED_FAILED)

            if(hiring)
            {
                dispatch({
                    type: types.UNRESOLVED_HIRING_LOADED_SUCCESS,
                    hiring: hiring
                })
            }
        })();

    }
}

export function updateHiring(data) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let hiring = await API.__post('PATCH',
                'api//hiring_contacts/unresolved/admin/'+data.companyId+'/'+data.profileId,
                {companyAnswer: data.companyAnswer, applicantAnswer: data.applicantAnswer},
                dispatch,
                types.UNRESOLVED_HIRING_LOADED_FAILED);

            if(hiring)
            {
                dispatch({
                    type: types.UNRESOLVED_HIRING_LOADED_SUCCESS,
                    hiring: hiring
                })
            }

        })();

    }
}