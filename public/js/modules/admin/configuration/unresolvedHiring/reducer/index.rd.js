import * as types from '../constants/module.const';
const initialState = {
    hiringList:[]
};

export default function companies(state = initialState, action) {

    switch (action.type) {

        case types.UNRESOLVED_HIRING_LIST_LOADED_SUCCESS:
            return {
                ...state,
                hiringList: action.hiringList
            };
        case types.UNRESOLVED_HIRING_LOADED_SUCCESS:
            return {
                ...state,
                hiring: action.hiring
            };
        case types.UNRESOLVED_HIRING_UPDATE_SUCCESS:
            return {
                ...state,
                hiring: action.hiring
            };
        case types.UNRESOLVED_HIRING_LIST_LOADED_FAILED:
        case types.UNRESOLVED_HIRING_LOADED_FAILED:
        case types.UNRESOLVED_HIRING_UPDATE_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        default:
            return state
    }
}