import React from 'react'
// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import Date from '../../../../../components/form/date.jsx';
import {dmyDateFull} from '../../../../../helpers/date.helper.js'
// Componets
// Helpers

//TODO:: implement update form
import serialize from '../../../../../helpers/serialize.helper.js'

class CompanyView extends React.Component {
    constructor(props){
        super(props);
        this.state = {...this.props.hiring}
    }

    submit(event){
        const {
            hiring:{
                companyId = '',
                applicantId = ''
            } = {}
        } = this.props;
        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);
        
        serializedForm.companyId = companyId;
        serializedForm.profileId = applicantId;
        this.props.actions.updateHiring(serializedForm);

    }

    _getAllowedStatuses()
    {
        return [
            {label:'hired', name:'Нанят'},
            {label:'notHired', name:'Не нанят'}]
    }

    render () {
        const {
            hiring:{
                createdDate = '',
                companyId = '',
                applicantId = '',
                additionalData:{
                    applicant:{
                        mainSkill = '',
                        positionLevel = '',
                        positionTitle = '',
                        firstName = '',
                        lastName = '',
                        contactInfo:{
                            linkedinAccount = '',
                            phoneNumber = [],
                            skypeAccount = '',
                            telegramAccount = '',
                            viberAccount = ''
                        } = {}
                    } = {},
                    company:{
                        name = ''
                    } = {},
                    recruiter:{
                        lst = ''
                    } = {}
                } = {}
            } = {}
        } = this.props;

        return (
            <div className="container">            
                <div className="margin--b-20">
                    Контакт от {dmyDateFull(createdDate)}
                </div>
                <div className="margin--b-20 font--uppercase">
                    {`${mainSkill} ${positionLevel} ${positionTitle}`}
                </div>
                <form onSubmit={::this.submit} className="width--100">
                    {/* <Input label="Company Id" name="companyId" value={ this.props.hiring && this.props.hiring.companyId || ''} readonly={true} />
                    <Input label="Applicant Id" name="profileId" value={ this.props.hiring && this.props.hiring.applicantId || ''} readonly={true} /> */}
                    <div className="row">
                        <div className="col-6">
                            <div className="row">
                                <div className="label form__label font--14 col-5">
                                    Company, id
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {name}
                                    <span className="display--b margin--t-5">
                                        {companyId}
                                    </span>
                                </div>
                            </div>
                        <Combobox
                            label="Company Answer"
                            name="companyAnswer"
                            wrapperClass="row fl--align-c margin--b-20 combobox"
                            labelClass="label form__label font--14 col-5"
                            className="col-7"
                            list={this._getAllowedStatuses()}
                            value={this.props.hiring && this.props.hiring.companyAnswer}
                        />

                        </div>
                        <div className="col-6">
                            <div className="row">
                                <div className="label form__label font--14 col-5">
                                    Applicant, id
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {`${lastName} ${firstName}`}
                                    <span className="display--b margin--t-5">
                                        {applicantId}
                                    </span>
                                </div>
                            </div>
                            <div className="row margin--b-10">
                                <div className="label form__label font--14 col-5">
                                    Телефоны
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {phoneNumber.map((item,i) =>{
                                        return (
                                            <div key={item} className="margin--b-5">{item}</div>
                                        )
                                    })}
                                </div>
                            </div>
                            <div className="row margin--b-10">
                                <div className="label form__label font--14 col-5">
                                linkedinAccount
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {linkedinAccount}
                                </div>
                            </div>
                            <div className="row margin--b-10">
                                <div className="label form__label font--14 col-5">
                                skypeAccount
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {skypeAccount}
                                </div>
                            </div>
                            <div className="row margin--b-10">
                                <div className="label form__label font--14 col-5">
                                    telegramAccount
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {telegramAccount}
                                </div>
                            </div>
                            <div className="row margin--b-10">
                                <div className="label form__label font--14 col-5">
                                    viberAccount
                                </div>
                                <div className="label form__label font--14 col-7">
                                    {viberAccount}
                                </div>
                            </div>
                            <Combobox
                                wrapperClass="row fl--align-c margin--b-20 combobox"
                                labelClass="label form__label font--14 col-5"
                                className="col-7"
                                label="Applicant Answer"
                                name="applicantAnswer"
                                list={this._getAllowedStatuses()}
                                value={this.props.hiring && this.props.hiring.applicantAnswer}
                            />
                        </div>
                </div>
                    <button className="btn btn--primary font--14 padding--xc-30 padding--yc-10">Submit</button>
                </form>
            </div>
        )
    }
}

export default CompanyView;
