import React from 'react'
import HiringLIst from './hiringList.jsx'

//TODO::create schema
class CompaniesListView extends React.Component {
	render(){

        let schema  = {
            hiringList: {
                fields: [
                    {
                        name: 'createdDate',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-19 font--left',
                        classNameTd:'respons-flex-table--w-19' 
                    },
                    {
                        name: 'companyId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-19 font--left',
                        classNameTd:'respons-flex-table--w-19'
                    },
                    {
                        name: 'applicantId',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-19 font--left',
                        classNameTd:'respons-flex-table--w-19'
                    },
                    {
                        name: 'companyAnswer',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-19 font--left',
                        classNameTd:'respons-flex-table--w-19'
                    },
                    {
                        name: 'applicantAnswer',
                        type: 'String',
                        classNameTh:'respons-flex-table--w-19 font--left',
                        classNameTd:'respons-flex-table--w-19'
                    }
                ],
                actions: {
                    edit: 'url[/unresolved_hiring/:id/sub_entity/:subId]'
                },
                entityIdKey: 'companyId',
                entitySubKey: 'applicantId'
            }
        };

        let {hiringList} = schema,
            {i18n} = this.props;
		return (
			<div className="col-12">
				<HiringLIst i18n={i18n} data={this.props.hiringList}/>
			</div>
		)
	}
}

export default CompaniesListView;