import React from 'react'
import {Link} from 'react-router-dom'
import uniqid from 'uniqid';

import {dmyDateFull} from '../../../../../helpers/date.helper.js'

class ApplicantList extends React.Component {



    static defaultProps = {
        createdDate:{
            classNameTh:'respons-flex-table--w-16 font--center',
            classNameTd:'respons-flex-table--w-16 font--center' 
        },
        company:{
            classNameTh:'respons-flex-table--w-16 font--center',
            classNameTd:'respons-flex-table--w-16 font--center' 
        },
        applicat:{
            classNameTh:'respons-flex-table--w-16 font--center',
            classNameTd:'respons-flex-table--w-16 font--center' 
        },
        companyHired:{
            classNameTh:'respons-flex-table--w-16 font--center',
            classNameTd:'respons-flex-table--w-16 font--center' 
        },
        applicatHired:{
            classNameTh:'respons-flex-table--w-16 font--center',
            classNameTd:'respons-flex-table--w-16 font--center' 
        },
        edit:{
            classNameTh:'respons-flex-table--w-16 font--center',
            classNameTd:'respons-flex-table--w-16 font--center' 
        }
    }


    render () {
        const {
            data = [],
        } = this.props,
        dataItems = data.slice().sort((item1, item2) =>{
                return new Date(item2.createdDate) - new Date(item1.createdDate);
            })

        return (
                <table className="respons-flex-table box--white">
                    <thead className="respons-flex-table__thead respons-flex-table__thead--admin-theme">
                        <tr className="respons-flex-table__tr" >
                            <th className={`respons-flex-table__th ${this.props.createdDate.classNameTh}`}>
                                CreatedDate
                            </th>
                            <th className={`respons-flex-table__th ${this.props.company.classNameTh}`}>
                                Компнания, Id
                            </th>
                            <th className={`respons-flex-table__th ${this.props.applicat.classNameTh}`}>
                                Соискатель, Id 
                            </th>
                            <th className={`respons-flex-table__th ${this.props.companyHired.classNameTh}`}>
                                CompanyAnswer
                            </th>
                            <th className={`respons-flex-table__th ${this.props.applicatHired.classNameTh}`}>
                                ApplicantAnswer
                            </th>
                            <th className={`respons-flex-table__th ${this.props.edit.classNameTh}`}>
                                Верифицировать
                            </th>
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                    {
                        dataItems.map((item, i) => {
                                return (
                                    <tr key={uniqid(i + '-')} data-id={item.id} className="respons-flex-table__tr" >
                                        <td key={uniqid(i + '-')} className={`respons-flex-table__td font--color-primary ${this.props.createdDate.classNameTd}`}>
                                            {dmyDateFull(item.createdDate)}
                                        </td>
                                        <td key={uniqid(i + '-')} className={`respons-flex-table__td respons-flex-table--font-14 font--color-primary ${this.props.company.classNameTd}`}>
                                            {item.additionalData.company.name}
                                            <span className="display--b margin--t-5 font--color-secondary font--14">
                                                {item.companyId}
                                            </span>
                                        </td>
                                        <td key={uniqid(i + '-')} className={`respons-flex-table__td respons-flex-table--font-14 font--color-primary ${this.props.applicat.classNameTd}`}>
                                        {`${item.additionalData.applicant.lastName} ${item.additionalData.applicant.firstName}`}
                                            <span className="display--b margin--t-5 font--color-secondary font--14">
                                                {item.applicantId}
                                            </span>
                                        </td>
                                        <td key={uniqid(i + '-')} className={`respons-flex-table__td font--color-primary ${this.props.companyHired.classNameTd}`}>
                                            {item.companyAnswer}
                                        </td>
                                        <td key={uniqid(i + '-')} className={`respons-flex-table__td font--color-primary ${this.props.applicatHired.classNameTd}`}>
                                            {item.applicantAnswer}
                                        </td>
                                        <td key={uniqid(i + '-')} className={`respons-flex-table__td font--color-primary ${this.props.edit.classNameTd}`}>
                                            <Link to={`/unresolved_hiring/${item.companyId}/sub_entity/${item.applicantId}`} className="font--color-blue link">
                                                Начать
                                            </Link>
                                        </td>
                                    </tr>
                                );
                        })
                    }

                    </tbody>
                </table>
        );
    }

}


export default ApplicantList;
