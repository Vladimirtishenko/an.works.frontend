import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as companiesActions from '../actions/unresolvedHiring.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        hiring: state.configuration.hiring_contacts.hiring || {}
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...companiesActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import ItemView from '../components/item.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class EntityCtrl extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        if(this.props.match.params)
        {
            this.props.actions.getUnresolvedHiring(this.props.match.params.id || null, this.props.match.params.subId || null);
        }
    }

	render() {

		return (
					<ItemView {...this.props}/>
			   );
	}

}

export default EntityCtrl;