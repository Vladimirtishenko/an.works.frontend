import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
  Route,
	Switch
} from 'react-router-dom';

import List from '../containers/lists.ctrl.jsx'
import Enity from '../containers/entity.ctrl.jsx'

const routes = [
	{
			path: "/unresolved_hiring",
			component: List
	},
	{
			path: "/unresolved_hiring/:id/sub_entity/:subId",
			component: Enity
	}
]

export default routes;