
import React from 'react'

import ItExperience from './itExperience.jsx'
import OtherExperience from './otherExperience.jsx'
import BtnSubmit from './button/btnSave.jsx'

import serialize from "../../../../../helpers/serialize.helper.js";

// Inheritance
import Inheritance from "../../../../profile/common/components/parent/main.jsx";

import {
    transformToTotal
} from "../../../../../helpers/date.helper.js";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";


export default class Experience extends Inheritance {
    normalizeDataType(type){
      return type == 'it' ? 'renderExperienceIT' : 'renderExperienceOther';
    }

    renderExperienceIT(key, item = {}){

      return (
          <ItExperience
                key={key}
                item={item}
                removeAction={::this.removeFieldToProps}
                {...this.props}
          />
     )

    }

    renderExperienceOther(key, item = {}){

      return (
          <OtherExperience
              key={key}
              item={item}
              removeAction={::this.removeFieldToProps}
              {...this.props}
          />
      )

    }

    renderPath(type, key, data) {
        const methodType = this.normalizeDataType(type)

        return this[methodType] && this[methodType](key, data) || null;
    }

    setToTotal(key, value){

        const { actions: { experienceRealTimeChange }, applicant: { profile: { experience: {items} } }} = this.props,
              clonedExperience = [...items],
              index = _.findIndex(clonedExperience, {id: key});

        if(index > -1){
            clonedExperience[index].date = value;
        }

        const period = transformToTotal(clonedExperience);

        experienceRealTimeChange(clonedExperience, period);

    }
    submit(event) {

        event.preventDefault();
  
        let form = event.target,
            serializedForm = serialize(form),
            transformObjectToArray = _.values(serializedForm.experience),
            {
                direction,
                profileId,
                experience: {
                    it = 0,
                    other = 0
                },
                actions: {
                    updateProfile
                }
            } = this.props;
  
            updateProfile({experience: {items: transformObjectToArray, it: it, other: other} }, profileId, direction);
      }
    render() {

        const {
            translation,
            experience: {
                items = []
            } = {}
        } = this.props;
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                {
                    items.length && items.map((item) => {
                        return ::this.renderPath(item.type, item.id, item)
                    }) || null
                }
                <BtnSubmit/>
            </ValidatorForm>
        )
    }
}
