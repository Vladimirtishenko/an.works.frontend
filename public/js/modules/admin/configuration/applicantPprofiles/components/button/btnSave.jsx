import React from 'react'
 
export default class BtnSubmit extends React.Component {

    render(){
        const {text} = this.props
        return (
            <div className="fl fl--justify-c">
                <button className="btn btn--primary font--14 padding--10">{text ? text : "Сохранить изменения"}</button>
            </div>
        )
    }
    
};