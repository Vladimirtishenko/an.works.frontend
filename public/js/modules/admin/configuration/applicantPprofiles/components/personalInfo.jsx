import React from 'react'

// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Radio from '../../../../../components/form/radio.jsx';
import DateCustom from '../../../../../components/form/dateCustom.jsx';
import BtnSubmit from './button/btnSave.jsx';

import {
    separateDate,
    preparationMonth
} from '../../../../../helpers/date.helper.js'

import {
    comparingGenderValue
} from '../../../../../helpers/gender.helper.js'
import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

import serialize from "../../../../../helpers/serialize.helper.js";

import { ValidatorForm } from "../../../../../libraries/validation/index.js";

class PersonalInfo extends React.Component {


    constructor(props){
        super(props)
        this.state = {...props}
    }

    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            {profileId } = this.props.profile,
            date =
                serializedForm.birthday &&
                Array.isArray(serializedForm.birthday)
                    ? serializedForm.birthday.reverse()
                    : "",
            birthday = date ? new Date(date).valueOf() : "";

        serializedForm.birthday = birthday;

        this.props.actions.updateProfile(serializedForm, profileId);
    }

 
    render(){

        const {
            i18n: { translation } = {},
            profile:{
                firstName = '',
                lastName = '',
                status,
                gender,
                birthday = null,
            }
            } = this.props,
            date = separateDate(birthday),
            monthPrepare = preparationMonth(translation.month);

        return (
            <ValidatorForm onSubmit={::this.submit} className="form">    
                <Input
                    validators={['required', 'isName']} 
                    errorMessages={[ errorMessage.required, errorMessage.name ]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-5"
                    inputWrapperClass="col-7 "
                    inputClass="input form__input"
                    value={firstName}
                    label="Имя"
                    name="firstName"
                    required={true}
                />
                <Input
                    validators={['required', 'isName' ]}
                    errorMessages={[ errorMessage.required, errorMessage.name ]}
                    wrapperClass="row fl--align-c margin--b-20"
                    labelClass="label form__label font--14 col-5"
                    inputWrapperClass="col-7 "
                    inputClass="input form__input"
                    value={lastName}
                    label="Фамилия"
                    name="lastName"
                    required={true}
                />

                <Radio
                    validators={['radioRequired']}
                    errorMessages={[errorMessage.radio]}
                    wrapperClass="row fl--align-c margin--b-20"
                    labelClass="label form__label font--14 col-5"
                    wrapperClassRadio=""
                    wrapRadio="col-7 "
                    value={comparingGenderValue(gender, 'fl fl--align-c')}
                    label="Пол"
                    name="gender"
                    required={true}
                />

                <DateCustom
                    validators={['required', 'isDate']}
                    errorMessages={[errorMessage.required, errorMessage.isDateRange]}
                    wrapperClass="row fl--align-c margin--b-20"
                    labelClass="label form__label font--14 col-5"
                    coverClass="col-7 "
                    day={{
                        name: "birthday[]",
                        value: date.day
                    }}
                    month={{
                        name: "birthday[]",
                        value: date.month,
                        options: monthPrepare,
                        static: 'Месяц',
                    }}
                    year={{
                        name: "birthday[]",
                        value: date.year
                    }}
                    label="День рождения"
                />
                <BtnSubmit/>
            </ValidatorForm>
        )
    }
}

export default PersonalInfo;
