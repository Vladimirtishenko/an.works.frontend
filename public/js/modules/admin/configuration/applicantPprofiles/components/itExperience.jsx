import React from "react";

import Input from "../../../../../components/form/text.jsx";
import Hidden from "../../../../../components/form/hidden.jsx";
import Checkbox from "../../../../../components/form/checkbox.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import MultySelect from "../../../../../components/form/multiselect.jsx";
import Textarea from "../../../../../components/form/textarea.jsx";
import DateFromTo from "../../../../../components/form/dateFromTo.jsx";

import { DeleteXButton } from "../../../../../components/form/deleteXButton.jsx";

import { normalizeToFromTo } from "../../../../../helpers/date.helper.js";

import Location from "../../../../profile/common/components/editable/location.jsx";

import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

class ItExperience extends React.Component {
    constructor(props){
        super(props);
        let {item} = props;
        this.state = {
            ...props,
            remote: item.remote || false
        }

    }
    getOtherSkills(skl = "") {
        let {
                knowledges
            } = this.props,
            skills = (skl && skl.split(",")) || null,
            newSkillsObject = [];

        if (!skills || !knowledges) return [];

        skills.forEach((item, i) => {
            let key = _.findIndex(knowledges, { label: item, isMain: false }),
                obj = knowledges[key];

            if (obj) {
                newSkillsObject.push(obj);
            }
        });

        return newSkillsObject;
    }
    checkFreelance(value){
        
        const newValue = [...value],
              item = _.head(newValue),
              { checked = false } = item; 

        this.setState( {...this.state, remote: checked } )

    }
    render() {
        const {
                knowledges,
                item: {
                    position,
                    mainSkill,
                    company,
                    location: { country, city } = {},
                    description,
                    id,
                    skills,
                    date: { start: startDate = [], end: endDate = [], now } = {}
                },
                removeAction,
                translation: { month } = {}
            } = this.props,
            otherSkills = ::this.getOtherSkills(skills),
            start = normalizeToFromTo(startDate),
            end = normalizeToFromTo(endDate);
        let {remote} = this.state;
        return (
            <div
                key={id}
                className="experience border--b-2-grey padding--b-20 padding--15 padding--md-30"
            >
                <p className="margin--b-20 margin--md-b-30">
                    <DeleteXButton
                        className="font--16"
                        data-id={id}
                        data-option="experience"
                        data-action="experienceRealTimeChange"
                        data-period="true"
                        onClick={removeAction}
                        text="Работа в сфере IT"
                    />
                </p>
                <Checkbox
                    wrapperClass="checkbox row  fl--align-c margin--b-10 margin--l-0 margin--r-0"
                    value={[
                        {
                            name: `experience.${id}.remote`,
                            value: remote,
                            sub: "Фриланс",
                            checked: remote ? true : false, 
                            innerWrapperClass:
                                "d-inline_block font--14 font--color-secondary",
                            classNameInput: "checkbox__input--margin-r-10"
                        }
                    ]}
                    onChange={::this.checkFreelance}
                />
                <Hidden name={`experience.${id}.type`} value="it" />
                <Input
                    validators={['required']}
                    errorMessages={[errorMessage.required]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={position}
                    name={`experience.${id}.position`}
                    label="Должность"
                />
                <Combobox
                    validators={['required' , 'combobox']}
                    errorMessages={[errorMessage.required , errorMessage.combobox]}
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    className="col-md-6 col-lg-5 padding--md-0"
                    label="Основной навык"
                    value={mainSkill}
                    list={
                        (knowledges &&
                            knowledges.length &&
                            knowledges.filter((item, i) => {
                                if (!item.isLang && item.isMain) {
                                    return item;
                                }
                            })) ||
                        []
                    }
                    name={`experience.${id}.mainSkill`}
                />
                <MultySelect
                    wrapperClass="row  multiselect"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    className="col-md-6 col-lg-5 padding--md-0"
                    name={`experience.${id}.skills`}
                    label="Дополнительные навыки"
                    value={otherSkills}
                    list={
                        (knowledges &&
                            knowledges.length &&
                            knowledges.filter((item, i) => {
                                if (!item.isLang) {
                                    return item;
                                }
                            })) ||
                        []
                    }
                />
                <Input
                    validators={
                        !remote && ['required'] || []
                    }
                    errorMessages={[errorMessage.required]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    name={`experience.${id}.company`}
                    value={company}
                    label="Компания"
                />

                <div className="margin--b-20 dropdownList">
                    <Location
                        name={`experience.${id}.location`}
                        location={{
                            country: country,
                            city: city
                        }}
                    />
                </div>

                <div className="row fl--align-st fl--align-md-c margin--b-20">
                    <div className="label form__label font--14 col-md-4 col-lg-3 margin--md-t-10 margin--lg-t-0">
                        <span className="">Период работы</span>
                    </div>
                    <DateFromTo
                        validators={['required', 'isDateRange']}
                        errorMessages={[errorMessage.required, errorMessage.isDateRange]}
                        start={{
                            ...start,
                            name: `experience[].${id}.date.start[]`
                        }}
                        end={{
                            ...end,
                            name: `experience[].${id}.date.end[]`
                        }}
                        now={{
                            value: now,
                            name: `experience[].${id}.date.now`
                        }}
                        translation={month}
                        wrapperClass="col-md-8 col-lg- padding--md-0"
                        wrapperClassStart="col-12 col-md-9 col-lg-5 padding--0 margin--md-r-10 margin--b-10 fl"
                        wrapperClassEnd="col-12 col-md-9 col-lg-5 padding--0 margin--lg-l-10 margin--b-10 fl"
                        label="Работаю сейчас"
                        wrapperClassCheck="col-12 padding--0"
                    />
                </div>
                <Textarea
                    classes="row margin--b-10 "
                    rows="7"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    textAreaWrapper="col-md-7 col-lg-9 font--12 font--color-light-grey padding--md-0 padding--md-r-15"
                    textAreaClass="form__text-area"
                    placeholder="Опишите ваши обязанности, проекты на которых вы работали"
                    value={description}
                    name={`experience.${id}.description`}
                    label="Описание"
                />
                <Hidden name={`experience.${id}.id`} value={id} />
            </div>
        );
    }
}

export default ItExperience;
