import React from "react";

import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import DateFromTo from "../../../../../components/form/dateFromTo.jsx";
import Hidden from "../../../../../components/form/hidden.jsx";

import { normalizeToFromTo } from "../../../../../helpers/date.helper.js";

import Location from "../../../../profile/common/components/editable/location.jsx";
// /common/components/editable/location.jsx
import { DeleteXButton } from "../../../../../components/form/deleteXButton.jsx";

class GeneralEducation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        const {
                item: {
                    id,
                    location: { country, city, university } = {},
                    degree,
                    specialization,
                    date: { start: startDate = [], end: endDate = [], now } = {}
                },
                removeAction,
               translation: { month }  = {}
            } = this.props,
            start = normalizeToFromTo(startDate),
            end = normalizeToFromTo(endDate);

        return (
            <div
                key={id}
                className="education border--b-2-grey padding--b-20"
            >
            <div>
                учебное заведение
            </div>
                {/* <p className="margin--b-20 margin--md-b-30">
                    <DeleteXButton
                        className="font--16"
                        data-id={id}
                        data-option="educational"
                        data-action="educationalRealTimeChange"
                        onClick={removeAction}
                        text="Диплом"
                    />
                </p> */}
                <Hidden name={`education.${id}.type`} value="university" />
                <Hidden name={`education.${id}.status`} value="true" />
                <Hidden name={`education.${id}.id`} value={id} />
                <div className="margin--b-20 dropdownList">
                    <Location
                        name={`education.${id}.location`}
                        location={{
                            country: country,
                            city: city,
                            university: university
                        }}
                        withUniversity={true}
                    />
                </div>

                <Input
                    validators={['required', 'matchRegexp:[A-Za-zА-Яа-я]' ]}
                    errorMessages={['This field is required', 'Name should be contain latin or cyrillic character']}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={degree}
                    name={`education.${id}.degree`}
                    label="Степень"
                />
                <Input
                    validators={['required', 'matchRegexp:[A-Za-zА-Яа-я]' ]}
                    errorMessages={['This field is required', 'Name should be contain latin or cyrillic character']}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={specialization}
                    name={`education.${id}.specialization`}
                    label="Специальность"
                />
                <div className="row fl--align-st fl--align-md-c margin--b-20">
                    <div className="label form__label font--14 col-md-4 col-lg-3 margin--md-t-10 margin--lg-t-0">
                        <span className="">Период обучения</span>
                    </div>
                    <DateFromTo
                        validators={['required', 'isDateRange']}
                        errorMessages={['This field is required', 'Date should be in right format']}
                        start={{
                            ...start,
                            name: `education[].${id}.date.start[]`
                        }}
                        end={{
                            ...end,
                            name: `education[].${id}.date.end[]`
                        }}
                        now={{
                            value: now,
                            name: `education[].${id}.date.now`
                        }}
                        translation={month}
                        wrapperClass="col-md-8 col-lg-9 padding--md-0"
                        wrapperClassStart="col-12 col-md-9 col-lg-5 padding--0 margin--md-r-10 margin--b-10 fl"
                        wrapperClassEnd="col-12 col-md-9 col-lg-5 padding--0 margin--lg-l-10 margin--b-10 fl"
                        label="Учусь сейчас"
                        wrapperClassCheck="col-12 padding--0"
                    />
                </div>
            </div>
        );
    }
}

export default GeneralEducation;
