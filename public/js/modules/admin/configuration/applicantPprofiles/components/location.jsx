
import React from 'react'
import serialize from "../../../../../helpers/serialize.helper.js";
import Location from "../../../../profile/common/components/editable/location.jsx"
import RelocateLocal from './relocateLocal.jsx'
import RelocateAbroad from './relocateAbroad.jsx'
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import BtnSubmit from './button/btnSave.jsx'

export default class Geography extends React.Component {

    submit() {
        event.preventDefault();
    
        let form = event.target,
            serializedForm = serialize(form),
            placesArray = _.get(
                serializedForm,
                "geography.relocation.abroad.places"
            ),
            places = placesArray ? _.values(placesArray) : [],
            cityArray = _.get(
                serializedForm,
                "geography.relocation.local.cities"
            ),
            cities = cityArray ? cityArray.split(',') : [],
            { direction, profileId } = this.props.profile;
    
        _.set(serializedForm, "geography.relocation.abroad.places", places);
        _.set(serializedForm, "geography.relocation.local.cities", cities);
    
        this.props.actions.updateProfile(
            {
                geography: {
                    ...serializedForm.geography,
                    occupation: this.props.profile.geography.occupation
                }
            },
            profileId,
            direction
        );
    
        this.setState({
            geography: false
        });
    }
    render(){
        
        let { 
                i18n:{translation={}} = {},
                profile:{
                    geography: { country, city, relocation: { local = [], abroad = [] } = { } } = {}
                } = {}
            } = this.props;

        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                 <Location 
                    name="geography" 
                    location={{country, city}}
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="label form__label font--14 col-4 font--14 order-1"
                    className="col-md-6 col-lg-5 order-2 order-md-1"
                />
                <RelocateLocal city={city} location={local} /> 
                <RelocateAbroad location={abroad} />
                <BtnSubmit/>
            </ValidatorForm>
        )
    }
}
