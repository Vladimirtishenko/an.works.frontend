import React from 'react'
import ListTest from '../../../../profile/applicant/components/tests/listTestOfResilts.jsx';


export default class Tests extends React.Component {

    render() {
        const {
            skillTestsHierarchy: {
                tests = [],
            } = {},
            status ="",
            testsResults = [],
            i18n:{ translation = {} } = {}
        } = this.props;
        return (
            <table className="respons-flex-table">
                <thead className="respons-flex-table__thead">
                    <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-25
                            font--left
                        ">
                            <span>
                                {translation.skill}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-13
                            font--center
                        ">
                            <span>
                                {translation.priority}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-15
                            font--center
                        ">
                            <span>
                                {translation.result}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-15
                            font--center
                        ">
                            {translation.time}
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-12
                            font--left
                            font--xl-center
                        ">
                            <span>
                                {translation.valid}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-20
                            font--md-right
                        ">
                            {translation.details}
                        </th>
                    </tr>
                </thead>
                <tbody className="respons-flex-table__tbody">
                    <ListTest testsResults={testsResults} tests={tests} status={status} />
                </tbody>
            </table>
        )
    }
}
