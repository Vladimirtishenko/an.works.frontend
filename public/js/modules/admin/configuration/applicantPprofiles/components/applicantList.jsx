import React from 'react'
import {Link} from 'react-router-dom'
// import Tbody from './tbody.jsx';
import Thead from '../../../../../components/tables/thead.jsx';
import Actions from '../../../../../components/tables/actions.jsx'
import uniqid from 'uniqid';

class ApplicantList extends React.Component {

    render () {
        const {
            data = [],
            schema:{
                actions = {},
                fields = []
            }
        } = this.props,
        dataItems = data.slice().sort((item1, item2) =>{
            if(item1.status == "inPool"){
                return -1;
            }else {
                return 0;
            }
        }); 
        dataItems.sort((item1, item2) =>{
            if(item1.status == "pending"){
                return -1;
            }else {
                return 0;
            }
        })
        dataItems.sort((item1, item2) =>{
            if(item1.status == "waitingApprove"){
                return -1;
            }else {
                return 0;
            }
        })
        return (
                <table className="respons-flex-table box--white">
                    <thead className="respons-flex-table__thead respons-flex-table__thead--admin-theme">
                        <tr className="respons-flex-table__tr" >
                            <th className="respons-flex-table__th respons-flex-table--w-5 font--center">
                                ID
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-27 font--center">
                                Email
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-26 font--center">
                                Имя 
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-10 font--center">
                                Прогресс
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-20 font--center">
                                Статус
                            </th>
                            <th className="respons-flex-table__th respons-flex-table--w-12 font--center">
                                Верифицировать
                            </th>
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                    {
                        dataItems.map((item, i) => {
                                return (
                                    <tr key={uniqid(i + '-')} data-id={item.id} className="respons-flex-table__tr" >
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-5 font--center">
                                            {item.profileId}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-27 font--center">
                                            {item.email}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-26 font--center">
                                            {`${item.firstName} ${item.lastName}`}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-10 font--center">
                                            {item.progress}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-20 font--center">
                                            {item.status}
                                        </td>
                                        <td key={uniqid(i + '-')} className="respons-flex-table__td respons-flex-table--w-12 font--center">
                                            <Link to={`/applicants_profiles/${item.profileId}`} className="font--color-blue link">
                                                Начать
                                            </Link>
                                        </td>
                                    </tr>
                                );
                        })
                    }

                    </tbody>
                </table>
        );
    }

}


export default ApplicantList;
