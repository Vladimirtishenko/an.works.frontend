import React from "react";
import uniqid from "uniqid";
import GeneralEducation from "./generalEducation.jsx";
import Sertificate from "./sertificate.jsx";
import Inheritance from "../../../../profile/common/components/parent/main.jsx";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";

import BtnSubmit from './button/btnSave.jsx'

import serialize from "../../../../../helpers/serialize.helper.js";

export default class Education extends Inheritance {
    normalizeDataType(type) {
        return type == "university" ? "generalEducation" : "sertificate";
    }

    generalEducation(key, item = {}) {
        return (
            <GeneralEducation
                key={key}  
                item={item}
                removeAction={::this.removeFieldToProps}
                {...this.props}
            />
        );
    }

    sertificate(key, item = {}) {
        return (
            <Sertificate
                key={key}
                item={item}
                removeAction={::this.removeFieldToProps}
                {...this.props}
            />
        );
    }
    submit(){
        
    }
    renderPath(type, key, data) {
        const methodType = this.normalizeDataType(type);

        return (this[methodType] && this[methodType](key, data)) || null;
    }

    render() {
        const {
            educational: { items = [] } = {}
        } = this.props;

        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                {items.length &&
                    items.map((item, i) => {
                        return ::this.renderPath(item.type, item.id, item);
                    }) || null
                }
                <BtnSubmit/>
            </ValidatorForm>
        );
    }
}
