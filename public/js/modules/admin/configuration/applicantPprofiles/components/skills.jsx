import React from "react";

import { mapperSkills } from "../../../../../helpers/mapperSkills.helper.js";

import Combobox from "../../../../../components/form/combobox.jsx";
import MultySelect from "../../../../../components/form/multiselect.jsx";
import BtnSubmit from './button/btnSave.jsx';

import levelDictionary from "../../../../../databases/general/level.json";
import positionDictionary from "../../../../../databases/general/position.json";

import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import { skillsComparing } from "../../../../../helpers/mapperSkills.helper.js";
import serialize from "../../../../../helpers/serialize.helper.js";
import Languages from "./languages.jsx";

export default class Skills extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props
        }
    }

    serializeFormData(form) {
        let serializedForm = serialize(form),
            { knowledges, profileId } = this.props,
            skillsObject = skillsComparing(serializedForm, knowledges);

        return { skillsObject, serializedForm, profileId };
    }

    submit(event) {
        event.preventDefault();

        let form = event.target;
        const {
            skillsObject,
            serializedForm
        } = this.serializeFormData(form),
        { profileId } = this.props;
        
        this.props.actions.updateProfile(
            {
                skills: skillsObject.skills,
                mainSkill: skillsObject.mainSkill,
                positionTitle: serializedForm.positionTitle,
                positionLevel: serializedForm.positionLevel
            },
            profileId
        );
    }
    render() {
       
        let {
            knowledges,
            positionTitle = "",
            positionLevel = "",
            mainSkill = "",
            profile = {}
        } = this.props,
            otherSkills = mapperSkills(  
                profile,
                knowledges,
                [],
                [ { isLang: false }],
                "array"
            );
    
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                <Combobox
                    validators={["combobox"]}
                    errorMessages={["Выберите значение из списка"]}
                    name="positionTitle"
                    wrapperClass="row fl--align-c margin--b-20 combobox"
                    labelClass="label form__label font--14 col-5"
                    className="col-7"
                    label="Должность"
                    list={positionDictionary}
                    value={positionTitle}
                />

                <Combobox
                    validators={["combobox"]}
                    errorMessages={["Выберите значение из списка"]}
                    name="positionLevel"
                    wrapperClass="row fl--align-c margin--b-20 combobox"
                    labelClass="label form__label font--14 col-5"
                    className="col-7"
                    label="Уровень"
                    list={levelDictionary}
                    value={positionLevel}
                />

                <Combobox
                    validators={["combobox"]}
                    errorMessages={["Выберите значение из списка"]}
                    name="skills.mainSkill"
                    wrapperClass="row fl--align-c margin--b-20 combobox"
                    labelClass="label form__label font--14 col-5"
                    className="col-7"
                    label="Основной навык"
                    list={
                        (knowledges.length &&
                            [...knowledges].filter((item, i) => {
                                if (item.isMain) {
                                    return item;
                                }
                            })) ||
                        []
                    }
                    value={mainSkill || ""}
                /> 

                <MultySelect
                    name="skills.scope"
                    wrapperClass="row  multiselect margin--b-10"
                    labelClass="label form__label font--14 col-5"                    
                    className="col-7"
                    label="Доп. навыки"
                    list={
                        (knowledges.length &&
                            [...knowledges].filter((item, i) => {
                                if (!item.isLang) {
                                    return item;
                                }
                            })) ||
                        []
                    }
                    value={otherSkills || ""}
                /> 

                <Languages
                    knowledges={knowledges}
                    profile={profile}
                />

                <BtnSubmit/>

            </ValidatorForm>
        );
    }
}
