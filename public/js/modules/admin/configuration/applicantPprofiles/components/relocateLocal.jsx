
import React from 'react'
import { connect } from "react-redux";
import Checkbox from '../../../../../components/form/checkbox.jsx';
import MultySelect from '../../../../../components/form/multiselect.jsx';
import Tooltip from '../../../../../components/widgets/toolTip.jsx';
import tooltipInfo from "../../../../../databases/general/tooltipInfoApplicant.json";
import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
 
@connect(
    mapStateToProps,
    { ...i18n }
)
class RelocateLocal extends React.Component {

    constructor(props){
        super(props);
        let {location} = props;
        this.state = {
            ...props.location,
            globalCitiesList: [],
            type: location.type || false
        }

    }

    static defaultProps = {
        wrapperClass:"row fl--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-4",
        className: "col-md-6 col-xl-5"
    }

    localRelocation(value){

        const newValue = [...value],
              item = _.head(newValue),
              { checked = false } = item;

        this.setState( {...this.state, type: checked } )

    }

    async getCitiesList(item){

        const db = await (
          await import(`../../../../../databases/countries/${item.label}/cities.${item.label}.json`)
        ).default;

        this.setState( {...this.state, globalCitiesList: db } )

    }
    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props,...this.props.location, });
        }
    }
    componentDidMount(){
        this.getCitiesList({label: 2})
    }

    getStringToArrayMapper(cities){
        cities = Array.isArray(cities) ? cities.join(',') : cities;
        let citiesArray = cities && cities.split(',') || [];
        return citiesArray;
    }

    render(){

        let {cities, type, globalCitiesList} = this.state,
            citiesArray = ::this.getStringToArrayMapper(cities);
        const { i18n:{translation = {}} = {} } = this.props;

        return (
            <div className="location__local ">
                <Checkbox
                    wrapperClass="checkbox row margin--b-10 combobox "
                    labelClass="form__checkbox form__label"
                    labelWrapperClass={this.props.labelClass}
                    wrapperAllCheckbox={this.props.className}
                    value={
                        [
                            {
                                name: "geography.relocation.local.type",
                                checked: type ? true : false,
                                value: type ? true : false,
                                innerWrapperClass: "d-inline_block font--14 font--color-secondary",
                                sub: translation.ready
                            }
                        ]
                    }
                    onChange={::this.localRelocation}
                    label={translation.readyRelocate}
                    subLabel={translation.inUkr}
                    subLabelClass="d-block font--12 font--color-inactive"
                >
                    <Tooltip
                        wrapper="
                            col-2
                            tooltip--md-p-xc-15
                        "
                        description={translation.tooltipReadyRelocate}
                    />
                </Checkbox>
                {
                    type && <MultySelect
                        validators={['required']}
                        errorMessages={[translation.errorMessageMultySelect]}
                        wrapperClass="row  multiselect"
                        labelClass={this.props.labelClass}
                        className={this.props.className}
                        name="geography.relocation.local.cities"
                        label="Другие города"
                        list={globalCitiesList}
                        value={citiesArray}
                    /> || null
                }
            </div>
        )

    }

}

export default RelocateLocal;
