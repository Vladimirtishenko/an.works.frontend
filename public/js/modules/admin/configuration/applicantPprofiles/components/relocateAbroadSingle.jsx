
import React from 'react'
import Checkbox from '../../../../../components/form/checkbox.jsx';
import MultySelect from '../../../../../components/form/multiselect.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';

import uniqid from 'uniqid';

import countries from '../../../../../databases/general/countries.json'

class RelocateAbroadSingle extends React.Component {

    constructor(props){
        super(props);

        let normalizeProps = ::this.normalizeData(props) || {};

        this.state = {
            ...normalizeProps,
            removable: props.removable,
            onClick: props.onClick,
            countries: countries,
            globalCitiesList: []
        }

    }

    normalizeData(){

        let {item} = this.props,
            id = item.id ? item.id : uniqid(),
            data = item.data ? item.data : item;

        return {
            id: id,
            data: data
        };
    }

    async citiesUpload(item){

        if(item && item.label){
              const db = await (
                await import(`../../../../../databases/countries/${item.label}/cities.${item.label}.json`)
              ).default;

              this.setState({
                    ...this.state,
                    data: {
                        ...this.state.data,
                        country: item.label
                    },
                    globalCitiesList: db
              })

        } else {
            this.setState({
                  ...this.state,
                  data: {
                      ...this.state.data,
                      country: item
                  }
            })
        }

    }

    getStringToArrayMapper(str){

        let citiesArray = str && str.split(',') || [];
        return citiesArray;

    }

    componentDidMount(){

        let {country} = this.state.data;

        if(country){
            ::this.citiesUpload({label: country})
        }

    }

    render(){

        let {id, data, countries, key, globalCitiesList, removable, onClick} = this.state,
            valueCities = ::this.getStringToArrayMapper(data.cities);

        return (
            <div key={key} className="relative">
                <Combobox
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    className="col-md-6 col-lg-5 padding--md-0"
                    value={data.country}
                    label="Страна"
                    list={countries}
                    onChange={::this.citiesUpload}
                    name={`geography.relocation.abroad.places.${id}.country`}
                />
                <MultySelect
                    wrapperClass="row  multiselect"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    className="col-md-6 col-lg-5 padding--md-0"
                    name={`geography.relocation.abroad.places.${id}.cities`}
                    label="Город"
                    value={valueCities}
                    list={globalCitiesList}
                />
            {removable && <span data-id={id} data-mark="places" 
            className="link link--under link--grey font--12 absolute absolute--t-2 absolute--r-0 absolute--md-r-16-5-p absolute--md-t-45 absolute--lg-r-13-p absolute--lg-t-10 absolute--xl-r-20-p"
            onClick={onClick}>Удалить</span> || null}
            </div>
        )

    }


}

export default RelocateAbroadSingle;
