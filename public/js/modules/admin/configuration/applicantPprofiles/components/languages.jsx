import React from "react";
import Input from "../../../../../components/form/text.jsx";
import Range from "../../../../../components/form/range/index.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import uniqid from "uniqid";
import ParentStep from "../../../../profile/common/components/parent/main.jsx";

import { mapperSkills, mapperSkillsSorting } from "../../../../../helpers/mapperSkills.helper.js";
import languageSkills from '../../../../../databases/languages/languageSkills.json';

class Language extends ParentStep {
    constructor(props) {
        super(props);

        let state = ::this.setData(props);

        this.state = state;
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {
            let state = ::this.setData(this.props);

            this.setState({
                ...this.state,
                ...state
            });
        }
    }
    componentDidMount(){
        let state = ::this.setData(this.props);

        this.setState({
            ...this.state,
            ...state
        });
    }
    setData(props) {
        let extendLanguage = mapperSkills(
                props.profile,
                props.knowledges,
                [],
                [{ isLang: true }],
                "array",
                true
            ),
            copyArray =
                (Array.isArray(extendLanguage) && extendLanguage.slice(0)) ||
                [],
            firstItem = copyArray.shift() || {};

        return {
            knowledges: props.knowledges,
            languages: copyArray ? ::this.setNormalizeData(copyArray) : [],
            firstItem: { id: uniqid(), data: firstItem }
        };
    }

    normalizeRangeData() {
        let language = {};

        for (var i = 0; i < languageSkills.length; i++) {
            language[languageSkills[i].label] = languageSkills[i].name;
        }

        return language;
    }

    renderAnotherComponent(item, knowledges, removed) {
        let valueList = ::this.normalizeRangeData(languageSkills);

        return (
                <div key={item.id} className="relative fl fl--wrap margin--b-20 margin--md-b-30">
                    <div className="skills-language width--100">
                        <Combobox
                            validators={["combobox"]}
                            errorMessages={["Выберите значение из списка"]}
                            name={`language.${item.id}.label`}
                            wrapperClass="row fl--align-c margin--b-20 margin--md-b-40 combobox"
                            labelClass="form__label font--14 col-5"
                            className="col-7"
                            label="Язык"
                            list={
                                knowledges && knowledges.length && knowledges.filter((item, i) => {
                                    if(item.isLang){
                                        return item
                                    }
                                }) || []
                            }
                            value={item.data && item.data.label}
                        >

                        </Combobox>
                        {removed && <span onClick={::this.removeFiled} data-id={item.id} data-mark="languages" className="link link--under link--grey font--12 absolute absolute--t-2 absolute--r-0 absolute--md-r-16-5-p absolute--md-t-45 absolute--lg-r-13-p absolute--lg-t-10 absolute--xl-r-20-p">Удалить</span> || null}
                        <Range
                            wrapperClass="row werae fl--align-c margin--b-10"
                            labelClass="form__label form__label--renge font--14 col-5"
                            inputWrapperClass="col-7"
                            label="Уровень"
                            name={`language.${item.id}.level`}
                            min={0}
                            max={4}
                            step={1}
                            dots={true}
                            value={item.data && item.data.value || 0}
                            valueList={valueList}
                        />

                    </div>
                </div>
        );
    }

    render() {
        const {
                languages = [],
                firstItem,
                knowledges = []
            } = this.state,
            comparingArrayItems = languages.length == mapperSkillsSorting(knowledges, [{isLang: true}]).length ? false : true;
    
        return (
            <React.Fragment>
                {::this.renderAnotherComponent(firstItem, knowledges, false)}
                {(languages &&
                    languages.length &&
                    languages.map((item, i) => {
                        return ::this.renderAnotherComponent(
                            item,
                            knowledges,
                            true
                        );
                    })) ||
                    null}
            </React.Fragment>
        );
    }
}

export default Language;
