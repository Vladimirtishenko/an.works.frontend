import React from "react";
import Checkbox from "../../../../../components/form/checkbox.jsx";
import ParentStep from "../../../../profile/common/components/parent/main.jsx";
import serialize from "../../../../../helpers/serialize.helper.js";
import occupationVocabularies from "../../../../../databases/general/occupation.json";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import BtnSubmit from './button/btnSave.jsx'

class Occupation extends ParentStep {
    constructor(props) {
        super(props);

        this.state = {
            ...props,
            occupationVocabularies: occupationVocabularies
        };
    }

    submit() {
        event.preventDefault();
    
        let form = event.target,
            serializedForm = serialize(form),
            placesArray = _.get(
                serializedForm,
                "geography.relocation.abroad.places"
            ),
            places = placesArray ? _.values(placesArray) : [],
            { 
                profile:{
                    profileId,
                    direction
                } = {} 
            } = this.props;
    
        _.set(serializedForm, "geography.relocation.abroad.places", places);
    
        this.props.actions.updateProfile(
            {
                geography: {
                    ...this.props.profile.geography,
                    occupation: serializedForm.geography.occupation
                }
            },
            profileId,
            direction
        );
    
        this.setState({
            type: false
        });
    }
    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }
    getOccupationList(occupation, name) {
        let { occupationVocabularies } = this.state,
            newOccupationObject = [];

        if (!occupationVocabularies) return {};

        _.forOwn(occupationVocabularies, (item, i) => {
            let { label } = item,
                value = occupation[label] ? { checked: true, value: true } : {checked: false};

            newOccupationObject.push({
                ...item,
                ...value,
                name: `${name}.${item.label}`,
                innerWrapperClass: "margin--b-15 font--14 font--color-primary"
            });
        });

        return newOccupationObject;
    }

    render() {
        let { profile:{
                    geography:{
                        occupation
                    } = {}
                } = {}
            } = this.state,
            getOccupation =
                ::this.getOccupationList(
                    occupation || [],
                    "geography.occupation"
                ) || [];
        const {i18n:{translation = {} } = {}} = this.props;
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                <Checkbox
                    validators={["checkboxRequired"]}
                    errorMessages={[translation.errorMessageCheckbox]}
                    wrapperClass="checkbox row margin--b-10 combobox no-gutters "
                    innerWrapperClass=" margin--xl-b-20"
                    labelClass="form__checkbox form__label"
                    value={getOccupation || []}
                    label={translation.employmentType}
                    labelWrapperClass="form__label col-md-4 font--14 col-lg-4"
                    wrapperAllCheckbox="col-md-5 col-lg-5 fl fl--dir-col"
                />
                <BtnSubmit/>
            </ValidatorForm>
        );
    }
}

export default Occupation;
