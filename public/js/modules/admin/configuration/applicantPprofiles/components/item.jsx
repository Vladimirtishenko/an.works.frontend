import React from 'react'
// Form Componets
import Combobox from '../../../../../components/form/combobox.jsx';


// Componets
import Personal from "./personalInfo.jsx";
import Skills from "./skills.jsx";
import ContactCard from './contactCard.jsx';
import About from './about.jsx';
import Education from './education.jsx';
import Experience from './experience.jsx';
import BtnSubmit from './button/btnSave.jsx';
import Occupation from './occupation.jsx';
import Location from './location.jsx';
import Tests from './test.jsx';
// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

import {
    separateDate,
    preparationMonth
} from '../../../../../helpers/date.helper.js'

import {
    comparingGenderValue
} from '../../../../../helpers/gender.helper.js'
import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";
import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";
import Avatar from "../../common/avata.jsx";


//TODO::create schema
@i18nMonthDecorator
class ProfileView extends React.Component {
    constructor(props){
        super(props);
    }

    submit(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form);

        this.props.actions.updateProfile(serializedForm, this.props.profile.profileId);

    }

    _getAllowedStatuses(){
        return [
            {label:'pending', name:'Pending'},
            {label:'waitingApprove', name:'WaitingApprove'},
            {label:'inPool', name:'InPool'},
            {label:'inPoolExpired', name:'InPoolExpired'}]
    }

    static defaultProps = {
        inputSizeSocial: "col-7",
        labelSizeSocial: "col-5"
    };

    render () {
      const {
            actions,
            i18n: { translation } = {},
            i18n = {},
            knowledges = [],
            profile = {},
            testsResults,
            skillTestsHierarchy,
            profile:{
                uid = '',
                profileId = '',
                firstName = '',
                lastName = '',
                status,
                gender,
                birthday = null,
                contactInfo:{
                    linkedinAccount = '',
                    phoneNumber = [],
                    skypeAccount = '',
                    telegramAccount = '',
                    viberAccount = ''
                } = {},
                positionTitle = '',
                positionLevel = '', 
                mainSkill = '',
                aboutMe:{
                    about = '',
                    waitings = '',
                    achivments = '',
                    hobby = ''
                } = {},
                educational = {},
                experience = {}
            },
            oauth:{
                user: {
                    userAvatar = '',
                    uniq = ''
                } = {}
            } = [],
            history:{
                goBack
            }
        } = this.props;
        return (
            <div className="container">
                <div className="row fl--dir-col">
                <div className="link link__go-back icon--Rating_arrow" onClick={goBack}>
                    Назад
                </div>
                    <Avatar wrapperClass="avatar--admin  margin--auto-xc" uniq={uniq} uid={uid} userAvatar={userAvatar} />  
                    <div className="padding--yc-20 border--b-1-gray">
                        <div className="col-7">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Личная информация
                            </p>
                            
                            <Personal {...this.props}/>
                        </div>
                    </div>
                    <div className="padding--yc-20 border--b-1-gray">
                        <div className="col-7">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Контактные данные
                            </p>
                            <ContactCard
                                actions={actions}
                                linkedinAccount={linkedinAccount}
                                phoneNumber={phoneNumber}
                                skypeAccount={skypeAccount}
                                telegramAccount={telegramAccount}
                                viberAccount={viberAccount}
                                profileId={profileId}
                            />
                        </div>
                    </div>
                    <div className="padding--yc-20 border--b-1-gray">
                        <div className="col-7">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                НАВЫКИ
                            </p>
                            
                            <Skills
                                actions={actions}
                                positionTitle={positionTitle}
                                positionLevel={positionLevel}
                                mainSkill={mainSkill}
                                knowledges={knowledges}
                                profile={profile}
                                profileId={profileId}
                            />
                        </div>
                    </div>
                    <div className="padding--yc-20 border--b-1-gray">
                        <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                            ТЕСТИРОВАНИЕ
                        </p>
                        <div className="box--white">
                            <Tests 
                                status={status}
                                skillTestsHierarchy={skillTestsHierarchy}
                                testsResults={testsResults}
                                i18n={i18n}
                            />
                        </div>
                    </div>
                        <div className="padding--yc-20 border--b-1-gray">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Опыт работы
                            </p>
                                {
                                    experience.items && experience && experience.items.length && 
                                    (<Experience
                                        actions={actions}
                                        experience={experience}
                                        translation={translation}
                                        knowledges={knowledges}
                                        profileId={profileId}
                                    />)
                                        || 
                                    <div className="font--18 font--bold font--color-important">Либо у него нет опыта работы либо зависло, перезагрузите страницу</div>
                                }
                        </div>
                        <div className="padding--yc-20 border--b-1-gray">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Образование
                            </p>                           
                                {
                                    educational.items && educational && educational.items.length && 
                                    (<Education
                                        actions={actions}
                                        educational={educational}
                                        translation={translation}
                                        profileId={profileId}
                                    />)
                                        || 
                                    <div className="font--18 font--bold font--color-important">Либо у него нет образования либо зависло, перезагрузите страницу</div>
                                }
                        </div>
                        <div className="padding--yc-20 border--b-1-gray">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Локация
                            </p>                           
                                <Location
                                   {...this.props}
                                />
                        </div>
                        <div className="padding--yc-20 border--b-1-gray">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Вид занятости
                            </p>                           
                                <Occupation
                                   {...this.props}
                                />
                        </div>
                        
                        <div className="padding--yc-20 border--b-1-gray">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                О СЕБЕ
                            </p> 
                            <About 
                                actions={actions}
                                about={about}
                                waitings={waitings}
                                achivments={achivments}
                                hobby={hobby}
                                profileId={profileId}
                            />
                        </div>

                        <div className="padding--yc-20">
                            <p className="margin--b-15 font--color-primary font--500 font--uppercase">
                                Статус
                            </p>
                            <form onSubmit={::this.submit}>
                                <Combobox
                                    name="status"
                                    wrapperClass="row fl--align-c margin--b-20 combobox"
                                    labelClass="label form__label font--14 col-5"
                                    className="col-7"
                                    label="Статус"
                                    list={this._getAllowedStatuses()}
                                    value={status}
                                />
                                <BtnSubmit/>
                            </form>
                        </div>
                        <div className="link link__go-back icon--Rating_arrow" onClick={goBack}>
                            Назад
                        </div>
                    </div>
                </div>
        )
    }
}

export default ProfileView;
