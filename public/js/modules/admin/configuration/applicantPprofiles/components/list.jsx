import React from 'react'
import ApplicantList from './applicantList.jsx'

//TODO::create schema
class ProfilesListView extends React.Component {
	render(){

        let schema  = {
            applicants: {
                fields: [
                    {
                        profileId: 'profileId',
                        type: 'String'
                    },
                    {
                        name: 'email',
                        type: 'String'
                    },
                    {
                        name: 'progress',
                        type: 'Number'
                    },
                    {
                        name: 'status',
                        type: 'String'
                    }
                ],
                actions: {
                    edit: 'url[/applicants_profiles/:id]'
                }
            }
        };

        let {applicants} = schema,
            {i18n} = this.props;
		return (
            <div className="col-md-9">
                <ApplicantList i18n={i18n} data={this.props.profiles} schema={applicants} actions={applicants.actions}/>
            </div>
		)
	}
}

export default ProfilesListView;