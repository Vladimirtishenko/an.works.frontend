import React from 'react'

import Input from '../../../../../components/form/text.jsx';
import Hidden from '../../../../../components/form/hidden.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import Textarea from '../../../../../components/form/textarea.jsx';
import DateFromTo from '../../../../../components/form/dateFromTo.jsx'
import { DeleteXButton } from "../../../../../components/form/deleteXButton.jsx";

import {
    normalizeToFromTo
} from "../../../../../helpers/date.helper.js";

import Location from "../../../../profile/common/components/editable/location.jsx";

class OtherExperience extends React.Component {
    render () {

        const {
                knowledges,
                item: {
                    position,
                    company,
                    location: { country, city } = {},
                    description,
                    id,
                    date: { start: startDate = [], end: endDate = [], now } = {}
                },
                removeAction,
                 translation: { month } = {}
            } = this.props,
            start = normalizeToFromTo(startDate),
            end = normalizeToFromTo(endDate);

      return (
            <div key={id} className="experience border--b-2-grey padding--b-20 padding--15 padding--md-30">
                  <p className="margin--b-20 margin--md-b-30">
                    <DeleteXButton
                        className="font--16"
                        data-id={id}
                        data-option="experience"
                        data-action="experienceRealTimeChange"
                        data-period="true"
                        onClick={removeAction}
                        text="Другая сфера"
                    />
                </p>
                <Hidden name={`experience.${id}.type`} value="other" />
                <Input
                    validators={['required']}
                    errorMessages={['This field is required']}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={position}
                    name={`experience.${id}.position`}
                    label="Должность" />
                <Input
                    validators={['required']}
                    errorMessages={['This field is required']}
                     wrapperClass="row fl--align-c margin--b-10"
                     labelClass="label form__label font--14 col-md-4 col-lg-3"
                     inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                     inputClass="input form__input"
                    value={company} name={`experience.${id}.company`}
                    label="Компания" />

                <div className="margin--b-20 dropdownList">
                    <Location
                        name={`experience.${id}.location`}
                        location={{
                            country: country,
                            city: city
                        }}
                    />
                </div>
                <div className="row fl--align-st fl--align-md-c margin--b-20">
                    <div className="label form__label font--14 col-md-4 col-lg-3 margin--md-t-10 margin--lg-t-0">
                        <span className="">Период работы</span>
                    </div>
                    <DateFromTo 
                        validators={['required', 'isDateRange']}
                        errorMessages={['This field is required', 'Date should be in right format']}
                        start={{
                            ...start,
                            name: `experience[].${id}.date.start[]`
                        }}
                        end={{
                            ...end,
                            name: `experience[].${id}.date.end[]`,
                            min: 1980
                        }}
                        now={{
                            value: now,
                            name: `experience[].${id}.date.now`
                        }}
                        translation={month}
                        wrapperClass="col-md-8 col-lg-9 padding--md-0"
                        wrapperClassStart="col-12 col-md-9 col-lg-5 padding--0 margin--md-r-10 margin--b-10 fl"
                        wrapperClassEnd="col-12 col-md-9 col-lg-5 padding--0 margin--lg-l-10 margin--b-10 fl"
                        label="Работаю сейчас"
                        wrapperClassCheck="col-12 padding--0"
                    />
                </div>
                <Textarea
                    classes="row margin--b-10 "
                    rows="7"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    textAreaWrapper="col-md-7 col-lg-9 font--12 font--color-light-grey padding--md-0 padding--md-r-15"
                    textAreaClass="form__text-area"
                    placeholder="Опишите ваши обязанности, проекты на которых вы работали"
                    value={description}
                    name={`experience.${id}.description`}
                    label="Описание"
                />
                <Hidden
                    name={`experience.${id}.id`}
                    value={id}
                />
            </div>
        )
    }
}

export default OtherExperience;
