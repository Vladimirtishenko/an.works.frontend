import React from "react";

import Input from "../../../../../components/form/text.jsx";
import BtnSubmit from "./button/btnSave.jsx";

import serialize from "../../../../../helpers/serialize.helper.js";

import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import Phones from "../../../../profile/common/components/editable/phones.jsx";

class ContactCard extends React.Component {

    static defaultProps = {
        inputSizeSocial: "col-7",
        labelSizeSocial: "col-5"
    };

    constructor(props) {
        super(props);
    }
    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            {profileId } = this.props;

        this.props.actions.updateProfile(serializedForm, profileId);
    }

    render() { 
        let {
            telegramAccount,
            viberAccount,
            skypeAccount,
            linkedinAccount
        } = this.props;
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">

                <Phones
                    phones={this.props}
                    {...this.props} 
                    labelPhoneSize="label form__label font--14 col-5"
                    inputPhoneSize="col-7 padding--md-xc-15"
                    positionBtnRemove="absolute--r-minus-10-p"
                />

                <Input 
                    validators={["isTelegram"]}
                    errorMessages={[
                        "Telegram username @mafia or +3808005553535"
                    ]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold icon--telegram form__label--icon form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    } margin--b-15 margin--md-b-0`}
                    inputClass="input form__input"
                    label="Telegram"
                    value={telegramAccount}
                    name="contactInfo.telegramAccount"
                />
               
               <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold icon--viber form__label--icon form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    }`}
                    inputClass="input form__input"
                    label="Viber"
                    value={viberAccount}
                    name="contactInfo.viberAccount"
                />

                <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold form__label--icon icon--skype form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    }`}
                    inputClass="input form__input"
                    label="Skype"
                    value={skypeAccount}
                    name="contactInfo.skypeAccount"
                />

                <Input
                    validators={["isUrl"]}
                    errorMessages={[
                        "LinkedIn link should be like in example https://www.example.com"
                    ]}
                    wrapperClass="row fl--align-c margin--b-20"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold form__label--icon icon--linkedIn form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    }`}
                    inputClass="input form__input"
                    label="LinkedIn"
                    value={linkedinAccount}
                    name="contactInfo.linkedinAccount"
                />
                <BtnSubmit/>
            </ValidatorForm>
        );
    }
}

export default ContactCard;
