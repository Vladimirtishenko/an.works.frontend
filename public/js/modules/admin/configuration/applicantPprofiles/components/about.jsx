import React from 'react'
import Textarea from '../../../../../components/form/textarea.jsx';
import BtnSubmit from './button/btnSave.jsx';

import serialize from "../../../../../helpers/serialize.helper.js";

import { ValidatorForm } from "../../../../../libraries/validation/index.js";


export default class About extends React.Component {

    serializeFormData(form) {
        let serializedForm = serialize(form),
            { profileId } = this.props;

        return {
            serializedForm,
            profileId
        };
    }

    submit(event) {
        event.preventDefault();

        let form = event.target;
        const { serializedForm, profileId} = this.serializeFormData(form);
        
        this.props.actions.updateProfile(
            { aboutMe: serializedForm.aboutMe ? serializedForm.aboutMe :  { about: '' } },
            profileId
        ); 
    }
    
    render (){

        let { 
            about,
            waitings,
            achivments,
            hobby
        } = this.props;

        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                <Textarea
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-5 font--14"
                    textAreaWrapper="col-7"
                    textAreaClass="form__text-area"
                    label="О себе"
                    name="aboutMe.about"
                    rows="7" 
                    value={about}
                />
                <Textarea
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-5 font--14"
                    textAreaWrapper="col-7"
                    textAreaClass="form__text-area"
                    label="Ожидание от работы"
                    name="aboutMe.waitings"
                    rows="7"
                    value={waitings}
                />
                <Textarea
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-5 font--14"
                    textAreaWrapper="col-7"
                    textAreaClass="form__text-area"
                    label="Достижения"
                    name="aboutMe.achivments"
                    rows="7"
                    value={achivments}
                />
                <Textarea
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-5 font--14"
                    textAreaWrapper="col-7"
                    textAreaClass="form__text-area"
                    label="Хобби"
                    name="aboutMe.hobby"
                    rows="7"
                    value={hobby}
                />
                <BtnSubmit/>
            </ValidatorForm>
        )
    }
}
