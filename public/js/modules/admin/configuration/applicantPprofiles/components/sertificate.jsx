import React from "react";

import Hidden from "../../../../../components/form/hidden.jsx";

// Form Componets
import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import DateFromTo from "../../../../../components/form/dateFromTo.jsx";

// Componets
import { normalizeToFromTo } from "../../../../../helpers/date.helper.js";

import { DeleteXButton } from "../../../../../components/form/deleteXButton.jsx";

import sertificateList from "../../../../../databases/general/sertificate.json";

import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

class Certificate extends React.Component {
    render() {
        const {
                item: {
                    id,
                    location: { country, city } = {},
                    degree,
                    mark,
                    sertificateType,
                    organisation,
                    name,
                    number,
                    date: { start: startDate = [], end: endDate = [], now } = {}
                },
                removeAction,
                translation: { month } = {}
            } = this.props,
            start = normalizeToFromTo(startDate),
            end = normalizeToFromTo(endDate);

        return ( 
            <div
                key={id}
                className="education border--b-2-grey padding--b-20"
            >
                <div>
                    Сертификат
                </div>
                {/* <p className="margin--b-20 margin--md-b-30">
                    <DeleteXButton
                        className="font--16"
                        data-id={id}
                        data-option="educational"
                        data-action="educationalRealTimeChange"
                        onClick={removeAction}
                        text="Сертификат"
                    />
                </p> */}

                <Hidden name={`education.${id}.type`} value="certificate" />
                <Hidden name={`education.${id}.id`} value={id} />
                <Hidden name={`education.${id}.status`} value="true" />

                <Input
                    validators={['required', 'isName']}
                    errorMessages={[errorMessage.required , errorMessage.name]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={name}
                    label="Название сертификата"
                    name={`education.${id}.name`}
                />
                <Combobox
                    validators={['required', 'combobox']}
                    errorMessages={[errorMessage.required , errorMessage.combobox]}
                    name={`education.${id}.sertificateType`}
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    className="col-md-6 col-lg-5 padding--md-0"
                    label="Тип сертификата"
                    list={sertificateList || []} 
                    value={sertificateType}
                />
                <Input
                     validators={['required', 'isName']}
                     errorMessages={[errorMessage.required , errorMessage.name]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={organisation}
                    label="Организация"
                    name={`education.${id}.organisation`}
                />
                <Input
                    validators={['required']}
                    errorMessages={[errorMessage.required]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={number}
                    label="Номер сертификата"
                    name={`education.${id}.number`}
                />
                <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={mark}
                    label="Оценка"
                    name={`education.${id}.mark`}
                />
                <div className="row fl--align-st fl--align-md-c margin--b-20">
                    <div className="label form__label font--14 col-md-4 col-lg-3 margin--md-t-10 margin--lg-t-0">
                        <span className="">Период действия</span>
                    </div>
                    <DateFromTo
                        validators={['required', 'isDateRange']}
                        errorMessages={[errorMessage.required , errorMessage.isDateRange]}                    
                        start={{
                            ...start,
                            name: `education[].${id}.date.start[]`
                        }}
                        end={{
                            ...end,
                            name: `education[].${id}.date.end[]`
                        }}
                        now={{
                            value: now,
                            name: `education[].${id}.date.now`
                        }}
                        translation={month}
                        wrapperClass="col-md-8 col-lg-9 padding--md-0"
                        wrapperClassStart="col-12 col-md-9 col-lg-5 padding--0 margin--md-r-10 margin--b-10 fl"
                        wrapperClassEnd="col-12 col-md-9 col-lg-5 padding--0 margin--lg-l-10 margin--b-10 fl"
                        label="Этот сертификат является бессрочным"
                        wrapperClassCheck="col-12 padding--0"
                    />
                </div>
            </div>
        );
    }
}

export default Certificate;
