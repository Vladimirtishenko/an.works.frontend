import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as applicantActions from '../actions/profile.action.js'

function mapStateToProps(state) {
    return {
        ...state,
        profiles: state.configuration.applicant_profiles.profiles || []
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...applicantActions
        }, dispatch),
        dispatch
    }
}

/* Components */
import ProfilesListView from '../components/list.jsx'

@connect(mapStateToProps, mapDispatchToProps)
class ProfilesLists extends React.Component {

	constructor (props, context) {
        super(props, context);
    }

    componentDidMount(){
        this.props.actions.getProfiles();
    }

	render() {

		return (
            <ProfilesListView {...this.props}/>
        );
	}

}

export default ProfilesLists;