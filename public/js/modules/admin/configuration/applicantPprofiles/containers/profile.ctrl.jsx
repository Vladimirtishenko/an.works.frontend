import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

/* Components */
import ProfileView from '../components/item.jsx'

// Actions
import * as applicantActions from '../actions/profile.action.js'
import * as applicantActionsTests from "../../../../profile/common/actions/quiz.action.js";


function mapStateToProps(state) {
    return {
        ...state,
        profile: state.configuration.applicant_profiles.profile || {},
        knowledges: state.configuration.applicant_profiles.knowledges || [],
        testsResults: state.profile.applicant.testsResults || [],
        skillTestsHierarchy: state.profile.applicant.skillTestsHierarchy || []
    }
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...applicantActions,
            ...applicantActionsTests
        }, dispatch),
        dispatch
    }
}



@connect(mapStateToProps, mapDispatchToProps)
class Profile extends React.Component {

	constructor (props, context) {
        super(props, context);
    }
    componentDidUpdate(prevProps,prevState){
        let {configuration:{applicant_profiles:{profile:{mainSkill:currentSkill} = {} } = {} } = {}} = this.props,
            {configuration:{applicant_profiles:{profile:{mainSkill:prevSkill}= {} } = {} } = {}} = prevProps;

        if(currentSkill && currentSkill != prevSkill){
           this.props.actions.getSkillTestHierarchy(currentSkill);
        }
    }

    componentDidMount(){
        const {
            actions:{
                getProfile,
                getProfileResults
            }={},
            match:{
                params
            } = {}
        } = this.props;
        if(params){
            getProfile(params.id || null);
            getProfileResults(params.id);
        }
        this.props.actions.getWorkSkills();
    }
    
	render() {
		return (
            <ProfileView {...this.props}/>
        );
	}

}

export default Profile;