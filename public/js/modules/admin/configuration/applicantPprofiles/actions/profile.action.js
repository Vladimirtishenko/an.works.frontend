import * as types from '../constants/profile.const.js';
import API from '../../../../../services/api.js'

export function getProfiles() {

    return dispatch => {

        (async () => {

                let profiles = await API.__get('applicants_profiles', dispatch, types.PROFILES_LOADED_FAILED)

                if(profiles)
                {
                    dispatch({
                        type: types.PROFILES_LOADED_SUCCESS,
                        profiles: profiles
                    })
                }
        })();

    }
}

export function getProfile(profileId) {

    return dispatch => {

        (async () => {

            let profile = await API.__get('applicants_profiles/'+profileId, dispatch, types.PROFILE_LOADED_FAILED)

            if(profile)
            {
                dispatch({
                    type: types.PROFILE_LOADED_SUCCESS,
                    profile: profile
                })
            }
        })();

    }
}

export function updateProfile(profileData, profileId) {

    return dispatch => {

        (async () => {

            let profile = await API.__post('PATCH','api/applicants_profiles/'+profileId, profileData, dispatch, types.PROFILE_LOADED_FAILED)

            if(profile)
            {
                dispatch({
                    type: types.PROFILE_LOADED_SUCCESS,
                    profile: profile
                })
            }
        })();

    }
}

export function getWorkSkills() {
    return dispatch => {
        (async () => {
            let knowledges = await API.__get(
                "dictionary/workSkills/",
                dispatch,
                types.KNOWLEDGES_LOADED_FAILED
            );
            
            if(knowledges){
                dispatch({
                    type: types.KNOWLEDGES_LOADED_SUCCESS,
                    knowledges: knowledges
                });
            }
        })();
    };
}