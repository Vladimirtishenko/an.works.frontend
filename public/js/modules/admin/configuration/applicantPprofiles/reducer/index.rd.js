import * as types from '../constants/profile.const';
const initialState = {
    profiles:[]
};

export default function applicant_profiles(state = initialState, action) {
    switch (action.type) {

        case types.PROFILES_LOADED_SUCCESS:

            action.profiles.map((item) => {
                item._id = item.profileId
            });
            return {
                ...state,
                profiles: action.profiles
            };
        case types.PROFILES_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        case types.PROFILE_LOADED_SUCCESS:
            return {
                ...state,
                profile: action.profile
            };
        case types.PROFILE_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };
        case types.KNOWLEDGES_LOADED_SUCCESS:
            return {
                ...state,
                knowledges: action.knowledges
            };
        default:
            return state
    }
}