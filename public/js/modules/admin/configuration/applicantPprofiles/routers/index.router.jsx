import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

import ProfilesLists from '../containers/profilesLists.ctrl.jsx';
import Profile from '../containers/profile.ctrl.jsx'
const routes = [
	{
			path: "/applicants_profiles",
			component: ProfilesLists
	},
	{
			path: "/applicants_profiles/:id",
			component: Profile
	}
]
export default routes;