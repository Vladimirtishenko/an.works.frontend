import { combineReducers } from 'redux'

import sidebar from './sidebar/reducer/sidebar.rd.js'
import applicant_profiles from './applicantPprofiles/reducer/index.rd.js'
import users from './users/reducer/index.rd.js'
import companies from './companies/reducer/index.rd.js'
import skillsTestsHierarchies from './skillTestsHierarchy/reducer/index.rd.js'
import orders from './orders/reducer/index.rd.js'
import companies_balances from './companiesBalance/reducer/index.rd.js'
import token_system from './tokenSystem/reducer/index.rd.js'
import hiring_contacts from './unresolvedHiring/reducer/index.rd.js'
import ws from './workSkills/reducer/index.rd.js'

export default combineReducers({
	sidebar,
    applicant_profiles,
    users,
    companies,
    skillsTestsHierarchies,
    orders,
    companies_balances,
    token_system,
    ws,
    hiring_contacts
});