import React from 'react';
import {
  browserHistory,
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import ApplicantsProfiles from './applicantPprofiles/routers/index.router.jsx';
import Users from './users/routers/index.router.jsx';
import Companies from './companies/routers/index.router.jsx';
import SkillsTestsHierarchy from './skillTestsHierarchy/routers/index.router.jsx';
import Orders from './orders/routers/index.router.jsx';
import Sidebar from './sidebar/components/sidebar.jsx';
import CompaniesBalances from './companiesBalance/routers/index.router.jsx';
import TokenSystem from './tokenSystem/routers/index.router.jsx';
import Hiring from './unresolvedHiring/routers/index.router.jsx';
import WS from './workSkills/routers/index.router.jsx';
import Footer from '../../../components/includes/footerNew.jsx'
import Header from '../../../components/includes/header.jsx';

let routes = [].concat(ApplicantsProfiles,Users,Companies,SkillsTestsHierarchy,Orders,CompaniesBalances,TokenSystem,Hiring,WS);

export const ConfigurationRoute = (props) => {
  return (
    <Router>
        <div className="fl fl--dir-col fl--justify-b min-h-100 overflow--h">
          <Header {...props}/>
          <div className="bg--white">
            <div className="container">
              <Sidebar {...props}/> 
            </div>
          </div>
            <div className="fl--1 padding--yc-30">
              <div className="container">
                <div className="row">
                  <Switch>
                    {
                      routes.map((route, i) => (
                          <Route key={i} path={route.path} exact component={route.component} />
                      ))
                    }
                  </Switch>
                </div>
              </div>
            </div>
            <Footer {...props}/>
        </div>
      </Router>
    );
}