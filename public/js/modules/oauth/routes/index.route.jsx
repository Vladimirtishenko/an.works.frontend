import React from 'react'
import { Switch,Redirect, Route } from 'react-router-dom'
import Login from '../containers/oauth.ctrl.jsx'
import Raiting from '../../publicPages/containers/rating.ctrl.jsx'
import Cv from '../../publicPages/containers/cv.ctrl.jsx'

export const OauthRouter = () => {
	return (
		<Switch>
			<Route path="/login" name="oauth" exact component={Login} />
			<Route path="/rating/:name" name="rating" exact component={Raiting} />
			<Route path="/cv/:profileId" name="rating" exact component={Cv} />
			<Route path="*" exact render={() => (<Redirect to="/login" />)} /> 
		</Switch>
	)
}