import React from 'react'
import Input from '../../../components/form/text.jsx'

export const RestorePassword = (props = {}) => (

  <div className="container">
    <div className="row">
      <div className="offset-md-2"></div>
      <div className="col-12 col-md-8">
        <div className="box box--login box--white shadow--small margin--auto-0">
          <h2 className="font--light font--center margin--b-30">Обновление пароля</h2>
          <form className="form-auth"
            onSubmit={props.submit}
          >
            <p className="font--hint font--center">Пожалуйста создайте новый пароль:</p>

            <Input
              inputClass="input"
              placeholder="Новый пароль"
              name="newPassword"
            />
            <Input
              inputClass="input"
              placeholder="Подтвердить пароль"
              name="confirmNewPassword"
            />

            <div className="font--center margin--t-40">
              <button className="btn btn--primary font--14">Пожалуйста</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

)
