import React from 'react'
import Input from '../../../components/form/text.jsx'
import Checkbox from '../../../components/form/checkbox.jsx'

// Helpers
import serialize from '../../../helpers/serialize.helper.js'
import {validator} from '../../../helpers/validator.helper.js';

export default class ForgotPassword extends React.PureComponent {


    submit(event){

         event.preventDefault();

        if(!event && !event.target) return;

        let form = event.target,
            serializedForm = serialize(form),
            validate = validator({email: 'isEmail'}, serializedForm);

        if(validate){
          this.props.loginFailed(validate);
          return;
        }

        this.props.onChange({target: {component: 'ApprovementNotice'}} )

    }

    render() {

        let {onChange, translation} = this.props;

        return (
            <div className="container">
                <div className="row">
                  <div className="offset-md-2"></div>
                  <div className="col-12 col-md-8">
                    <div className="box box--login box--white shadow--small margin--auto-0">
                      <h2 className="font--light font--center margin--b-30">Проблемы со входом?</h2>
                      <form className="form-auth" onSubmit={::this.submit}>
                        <p className="font--hint">Пожалуйста введите свой email чтобы восстановить пароль</p>
                          <Input
                            inputClass="input"
                            placeholder="Ваш email"
                            name="email"
                          />
                        <div className="margin--t-10">
                          <span onClick={onChange} data-component="LoginForm" className="font--14 font--color-primary pointer-text">Назад к логину</span>
                        </div>
                        <div className="font--center margin--t-20">
                          <button className="btn btn--primary font--14">Восстановить</button>
                        </div>
                      </form>
                      
                    </div>
                  </div>
                </div>
              </div>
        )
    }

}
