import React from 'react'
import Input from '../../../components/form/text.jsx'

// Helpers
import serialize from '../../../helpers/serialize.helper.js'
import {validator} from '../../../helpers/validator.helper.js';
import { ValidatorForm } from "../../../libraries/validation/index.js"

export default class LoginForm extends React.PureComponent {

    submit(event){
      event.preventDefault();

      let form = event.target,
          serializedForm = serialize(form);
          // serializedForm.userName  = serializedForm.userName.trim();
          serializedForm.userName  = serializedForm.userName;
      let validate = validator({userName: 'isEmail', password: 'matches|[a-zA-Z0-9]{4,}'}, serializedForm);
          if(validate){
            this.props.loginFailed(validate);
            return;
          }

          this.props.login(serializedForm)

    }

    render() {

        let {onChange, translation} = this.props;

        return ( 
            <div className="container">
                <div className="row">
                  <div className="offset-md-2"></div>
                  <div className="col-12 col-md-8">
                    <div className="box box--login box--white shadow--small">
                      <h2 className="font--light font--center margin--b-30">Авторизоваться</h2>
                      <ValidatorForm className="form-auth" onSubmit={::this.submit}>
                          <Input
                            // validators={['required', 'isEmail']}
                            // errorMessages={[ translation.errorMessageRquired, translation.errorMessageEmail]}
                            wrapperClass="margin--b-20"
                            inputClass="input"
                            placeholder="Login"
                            name="userName"
                          />
                          <Input
                            // validators={['required', 'isPassword']}
                            // errorMessages={[ translation.errorMessageRquired, translation.errorMessagePassword]}
                            wrapperClass="margin--b-20"
                            inputClass="input"
                            placeholder="Password"
                            name="password"
                            inputType="password"
                          />
                          {/* <div className="fl fl--justify-b margin--b-30">
                            <span onClick={onChange} data-component="ForgotPassword" className="link font--14 font--color-primary">Забыли пароль?</span>
                          </div> */}
                          <div className="font--center">
                            <button className="btn btn--primary font--14 padding--yc-10 padding--xc-20">{translation.sing_in}</button>
                          </div>
                        </ValidatorForm>
                    </div>
                  </div>
                </div>
              </div>
        )
    }

}
