import React from 'react'


export default class ApprovementNotice extends React.PureComponent {

    render(){

        let {onChange} = this.props;

        return (
            <div className="container">
              <div className="row">
                <div className="offset-md-2"></div>
                <div className="col-12 col-md-8">
                  <div className="box box--login box--white shadow--small margin--auto-0">
                    <h2 className="font--light font--center margin--b-30">Заявка принята</h2>
                    <p className="font--hint font--center">
                      На ваш email отправлено письмо с информацией для востановления пароля
                    </p>
                    <p className="font--hint font--center margin--t-30">
                      Не пришло письмо, <span onClick={onChange} data-component="ForgotPassword">отправить заново</span>
                    </p>
                    <p onClick={onChange} data-component="LoginForm" className="font--hint font--center margin--t-30">
                      Вернуться к логин форме
                    </p>
                  </div>
                </div>
              </div>
            </div>
        )
    }

}
