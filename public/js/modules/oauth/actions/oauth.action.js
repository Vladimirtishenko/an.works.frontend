import jwtDecode from 'jwt-decode'
import * as types from '../constants/oauth.const.js';
import Api from '../../../services/api.js'

export function login(credentials) {

    return dispatch => {

        (async () => {

          const authentification = await Api.__post( null, 'api/oauth', credentials, dispatch, types.LOGIN_FAILURE);

          if(authentification && authentification.auth){
            localStorage.authToken = authentification.token;

            dispatch({
                type: types.LOGIN_SUCCESS,
                user: jwtDecode(authentification.token),
                isAuthenticating: authentification.auth
            })

          }

        })();

    }
}

export function loginFailed(message) {

  return dispatch => {
    dispatch({
         type: types.LOGIN_FAILURE,
         notification: {
           type: 'error',
           view: 'notice',
           message: message
         }
     })
  }
}

export function logout() {

    return dispatch => {

        dispatch({
            type: types.LOG_OUT
        })

    }
}
