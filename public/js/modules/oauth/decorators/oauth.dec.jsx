import { connect } from 'react-redux'
import React from 'react'

import * as oauthActions from '../actions/oauth.action.js'

import {OauthRouter} from '../routes/index.route.jsx'

const withUser = (WrapedComponent) => {

    @connect(null, {...oauthActions})
    class oauthComponent extends React.Component {

    constructor(props, context){
      super(props, context);
      this.state = {autentification: this.props.oauth.isAuthenticating};
    }

    UNSAFE_componentWillMount(){

      // if(!this.state.autentification) {
      //   this.props.history.push('/login')
      // }

    }

    componentDidUpdate(prevProps, prevState){

        if(this.props.oauth.user && !this.state.autentification){
            this.setState({autentification: this.props.oauth.isAuthenticating});
            this.props.history.push('/')
        }

        if(!this.props.oauth.user && this.state.autentification) {
            this.setState({autentification: this.props.oauth.isAuthenticating});
            this.props.history.push('/login')
        }

    }

    render() {

      if(this.state.autentification && this.props.oauth.user){
        return (
          <WrapedComponent {...this.props} />
        )
      }

      return (
        <OauthRouter />
      )

    }

  }

  return oauthComponent;

}
export default withUser;
