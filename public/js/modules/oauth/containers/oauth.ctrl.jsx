import { connect } from 'react-redux'
import React from 'react'

import LoginForm from '../components/login.jsx' 
import ForgotPassword from '../components/forgotPassword.jsx'
import RestorePassword from '../components/restorePassword.jsx'
import ApprovementNotice from '../components/approvementNotice.jsx'
import TopLayout from '../../../components/includes/layoutPublic.jsx'

// Actions
import * as oauthActions from '../actions/oauth.action.js'

function mapStateToProps(state) {
    return {
    	...state.oauth,
      ...state.i18n
    }
}

@connect(mapStateToProps, {...oauthActions})
export default class Login extends React.Component {


    constructor(props){

        super(props);

        this.state = {
            component: LoginForm
        }
    }

  changeProfileWhichNeed(event){

      let target = event && event.target,
          renderComponent = target.dataset && target.dataset.component || target.component;

      if(!renderComponent) return;

      let component = {
          LoginForm: LoginForm,
          RestorePassword: RestorePassword,
          ForgotPassword: ForgotPassword,
          ApprovementNotice: ApprovementNotice
      }

      if(renderComponent && component[renderComponent]){
          this.setState({
              component: component[renderComponent]
          })
      }


  }

  render(){

    let Component = this.state.component;
    
    return (
      <TopLayout>
          <div className="container">
            <a href="/" className="link link__go-back icon--Rating_arrow">Назад</a>
          </div>
          <div className="fl--1 fl fl--justify-c fl--dir-col">
            <div className="fl fl--align-c h-100">
              {Component && <Component {...this.props} onChange={::this.changeProfileWhichNeed} /> || null}
            </div>
          </div>
       </TopLayout> 
    )
  }

}
