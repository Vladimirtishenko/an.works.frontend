import jwtDecode from 'jwt-decode';
import {
    LOGIN_REQUEST,
    LOGIN_FAILURE,
    LOGIN_SUCCESS,
    LOG_OUT,
    USER_DATA_UPDATED
} from '../constants/oauth.const.js';
import API from '../../../services/api.js';


const initialState = (token => ({
    isAuthenticating: token ? true : false,
    user: token ? jwtDecode(token) : null,
    fetch: false
}))(localStorage.authToken);

export default function oauth(state = initialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                fetch: action.fetch
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                fetch: false,
                notification: action.notification
            };
        case LOGIN_SUCCESS:
            return {
                fetch: false,
                user: action.user,
                isAuthenticating: action.isAuthenticating,
            };
        case USER_DATA_UPDATED:
            return {
                ...state,
                user: {
                    ...state.user,
                    uniq: action.uniq,
                    userAvatar: action.userAvatar
                }
            };
        case LOG_OUT:
            return {
                user: null,
                isAuthenticating: false,
            };
        default:
            return state
    }
}
