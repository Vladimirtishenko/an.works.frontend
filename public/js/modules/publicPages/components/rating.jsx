import React from "react";
import { Link } from "react-router-dom";

import TableFilters from "../../../components/filters/tableFilters.jsx";

import FiltersComponent from "../../../components/filters/filters.jsx";
import { RatingTable } from "../../profile/common/components/not-editable/ratingTable/ratingTable.jsx";
import Pagination from "../../profile/common/components/pagination.jsx";


// // Filters
import TechnologyChoise from "../../../components/filters/publicFilters/technologyChoise.jsx";

// Helpers
import {
    convertLimitToValue
} from '../../../helpers/filters/limit.helper.js'

import {
    setQueueLabel
} from '../../../helpers/queue.helper.js'

export default class RaitingComponent extends React.Component {
    constructor(props) {
        super(props);
        this.pageTopRef = React.createRef();
        this.state = {
            languages: [],
            participateRatings: false,
            notHired: false
        }

    }

    onScrollTop() {
        window.scrollTo(0, 0);
    }

    onChangePaginatin(filters){
        const { actions: { getQueue }} = this.props;
        getQueue(null, filters);
    }

    render() {
        const {
            applicant: {
                filters,
                profile: {
                    mainSkill,
                    positionLevel,
                } = {},
                knowledges,
                queue = {},
                queue: { totalCount, queueLabel } = {},
                skillTestsHierarchy
            } = {},
            actions,
            history: { goBack },
            i18n:{translation = {}} = {}
        } = this.props,
        { participateRatings, notHired } = this.state,
        limitToValue = convertLimitToValue(filters),
        skill = setQueueLabel(queueLabel, mainSkill);
        return (
            <React.Fragment>
                <div className="container-fluid relative" ref={this.pageTopRef}>
                    <div className="row fl fl--align-c margin--b-30">
                        <div className="col-md-4 col-lg-3 margin--b-15 margin--md-b-0">
                            <a
                                className="link link__go-back icon--Rating_arrow link__go-back_blue link--blue font--12 margin--sm-b-25 margin--md-b-15"
                                onClick={goBack}
                            >
                                {translation.back}
                            </a>
                        </div> 
                        <TechnologyChoise
                            actions={actions}
                            positionLevel={positionLevel}
                            knowledges={knowledges}
                            skill={skill}
                        />
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-lg-3">
                            <FiltersComponent
                                actions={actions}
                                mainSkill={skill}
                                testHierarchy={skillTestsHierarchy}
                                knowledges={knowledges}
                                filters={filters}
                            />
                        </div>
                        <div className="col-md-8 col-lg-9">

                            <TableFilters
                                totalAmount={totalCount}
                                actions={actions}
                                filters={filters}
                            />

                            <div className="row-container scroll--auto margin--xc-0">
                                <RatingTable 
                                    actions={actions}
                                    filters={filters}
                                    queue={queue}
                                    knowledges={knowledges}
                                    isCompany={false}
                                    skillTestsHierarchy={skillTestsHierarchy}
                                    i18n={this.props.i18n}
                                />
                            </div>

                            <div className="row-container fl fl--justify-end">
                                {
                                    (totalCount > limitToValue) && <Pagination
                                        count={totalCount}
                                        actions={::this.onChangePaginatin}
                                        filters={filters}
                                        wrapperClass="pagination margin--yc-10"
                                    /> || null
                                }
                            </div>
                        </div>
                    </div>
                    <div className="anchor" onClick={() => this.onScrollTop()}>
                        <span className={`icon icon--Rating_arrow rotate--90`} />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
