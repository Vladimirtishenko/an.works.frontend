import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as companyAction from "../../profile/company/actions/company.action.js";
import * as contact from "../../profile/common/actions/contact.action.js";
import * as actionQuiz from "../../profile/common/actions/quiz.action.js";

import CVComponent from "../../profile/common/components/not-editable/cv/cvCompany.jsx";

// layout
import TopLayout from "../../../components/includes/layoutPublic.jsx";
// import {isMobile} from "react-device-detect";
// import Left from "../../components/layouts/leftAside.jsx";
// import {linksProfileSchema} from "../../schema/linksProfile.sc.js";

function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction,
                ...contact,
                ...actionQuiz
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class CV extends React.Component {
    componentDidMount() {
        const {
            match:{
                params:{
                    profileId = ''
                } 
            },
            actions: { getQueue, getWorkSkillsDictionary, getContactProfileCv},
        } = this.props;
        getContactProfileCv(profileId);
        getWorkSkillsDictionary();
    }

    componentDidUpdate(prevProps) {
        
        if(!this.props.companyProfile.cvContact && !prevProps.companyProfile.cvContact) return

        const { companyProfile: { cvContact:{ mainSkill: prevStatus} = {} } = {} } = prevProps,
        { 
            companyProfile: { cvContact:{ mainSkill: currentStatus} = {} } = {},
            actions: { getSkillTestHierarchy}
        } = this.props;
        
        if(currentStatus !== prevStatus){
            getSkillTestHierarchy(currentStatus);
        }
        
    }
    componentWillUnmount(){
        const {
            actions: { deletContactProfileCv}
        } = this.props;
        
        if(deletContactProfileCv){
            deletContactProfileCv();
        }
    }
    // renderContent = () => {
    //     if (isMobile) {
    //         return <Left sideBarClassName="margin--t-44" mobileMenu={true}  {...this.props} links={linksProfileSchema(this.props.i18n.translation)} />
    //     }
    //     return null
    // };
    render() {

        return (
            <TopLayout>
                {/* {this.renderContent()} */}
                <div className="padding--yc-30 fl--1">
                    <CVComponent {...this.props} />
                </div>
            </TopLayout>
        );
    }
}
