import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// // Actions
import * as applicantQuiz from "../../profile/common/actions/quiz.action.js";
import {
    saveRating,
    getWorkSkills,
    getQueue
} from "../../profile/applicant/actions/profile.action.js";

import RaitingComponent from "../components/rating.jsx";

// // Configuration
import ratingConfuguration from '../../../configuration/rating.json';

import TopLayout from "../../../components/includes/layoutPublic.jsx";

// import Left from "../../components/layouts/leftAside.jsx";
// import {isMobile} from 'react-device-detect';


function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                saveRating,
                getWorkSkills,
                getQueue,
                ...applicantQuiz
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Rating extends React.Component {

    componentDidMount() {
        const {
            actions: {getQueue, getWorkSkills, getSkillTestHierarchy},
            location: {search},
            match:{
                params:{
                    name
                } = {}
            } = {},
            applicant:{ratings}
        } = this.props,
        { skill, level } = ratingConfuguration,
        skills = name ? `junior_${name}` : `${level}_${skill}`;
        getQueue(ratings ? ratings : skills, search.slice(1));
        getWorkSkills();
        getSkillTestHierarchy(name);
    }

    componentDidUpdate(prevProps) {

        const { applicant: { 
                filters: prevFilters 
            },
            applicant:{
                ratings: prevRating
            }
        } = prevProps,
            { history: {
                  replace,
                  push
                },
                applicant: { 
                    filters: nextFilters
                },
                match:{
                    params:{
                        name
                    }
                },
                applicant:{
                    ratings
                }
            } = this.props,
            { level } = ratingConfuguration;
        
        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

        if(prevRating !== ratings || ratings && (ratings.includes('junior_') || ratings.includes(`${level}_`)) && ratings.slice(7) !== name){
            push('/rating/' + ratings.slice(7))
        }
    }

    // renderContent = () => {
    //     if (isMobile) {
    //         return <Left mobileMenu={true} {...this.props} links={linksProfileSchema(this.props.i18n.translation)} />
    //     }
    //     return null
    // };

    render() {
        return (
            <TopLayout container="container-fluid">
                {/* {this.renderContent()} */}
                <div className="padding--yc-30 fl--1">
                    <RaitingComponent {...this.props} />
                </div>
            </TopLayout>
        );
    }
}
