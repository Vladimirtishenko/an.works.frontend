import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as companyAction from "../../actions/company.action.js";
import * as companyAdminAction from "../../actions/company_admin.action.js";

import RecrutersComponent from "../../components/profile/recruters/recruterList.jsx";

// layout
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import Layout from "../../components/layouts/layout.jsx";
import TopLayout from "../../../../../components/includes/layout.jsx";

import {
    transformToRange
} from "../../../../../helpers/date.helper.js";

import queryObject from "../../../../../helpers/queryObject.js";


function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n,
        notification: state.notification
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction,
                ...companyAdminAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Recruters extends React.Component {
    componentDidMount() {
        let {
            companyProfile: { company: { name } = {}, companyId },
            actions: {
                getCompanyEmployees,
                getCompanyRegistrationEmployeeWaitingToApprove,
                getStatisticRecruter,
                getBalance
            },
            location: {search}
        } = this.props;

        if (!name && companyId) return;

        let filtersNormolize = queryObject.stringify({
            filters:JSON.stringify({label: 'currentMonth',createdDate: transformToRange('currentMonth')})     
        }),
        filterHire =  search.slice(1) ? search.slice(1) : filtersNormolize; 

        getCompanyEmployees(companyId, search.slice(1));
        // getCompanyRegistrationEmployeeWaitingToApprove(name);
        getStatisticRecruter(companyId, filterHire);
        getBalance(companyId, search.slice(1));
    }
    
    componentDidUpdate(prevProps) {

        const { companyProfile: { filters: prevFilters } } = prevProps,
              { history: {replace}, location: {search}, companyProfile: { filters: nextFilters } } = this.props;

        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }

    render() {
        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                    progressBar={false}
                >
                    <RecrutersComponent {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}
