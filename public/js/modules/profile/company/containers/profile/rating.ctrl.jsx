import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Components
// import RaitingComponent from "../../components/profile/rating.jsx";
import RaitingComponent from "../../../common/components/not-editable/ratingTable/rating.jsx";
// Actions
import * as companyAction from "../../actions/company.action.js";
import * as actionQuiz from "../../../common/actions/quiz.action.js";

// Docorator
import withKnowledgesDictionary from '../../decorators/knowledges.dec.jsx'

// Helper
import { mapperSkillsSorting } from "../../../../../helpers/mapperSkills.helper.js";

// Layout
import TopLayout from "../../../../../components/includes/layout.jsx";

// Configuration
import ratingConfuguration from '../../../../../configuration/rating.json';
import Left from "../../components/layouts/leftAside.jsx";
import {linksProfileSchema} from "../../schema/linksProfile.sc.js";
import {isMobile} from "react-device-detect";

function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction,
                ...actionQuiz
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    {pure: false}
)
@withKnowledgesDictionary
export default class Rating extends React.Component {
    componentDidMount() {
        const {
            actions: { getQueue, getSkillTestHierarchy, getBalance, getHireHistory},
            location: {search},
            companyProfile:{companyId,ratings}
        } = this.props,
        { skill, level } = ratingConfuguration;
        getQueue(ratings ? ratings : level + '_' + skill, search.slice(1));
        getSkillTestHierarchy(skill);
        getBalance(companyId, search.slice(1)); 
        getHireHistory(companyId, search.slice(1));
    }

    componentDidUpdate(prevProps) {

        const { companyProfile: { filters: prevFilters } } = prevProps,
              { history: {replace}, companyProfile: { filters: nextFilters } } = this.props;


        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }
    renderContent = () => {
        if (isMobile) {
            return <Left sideBarClassName="margin--t-44" mobileMenu={true}  {...this.props} links={linksProfileSchema(this.props.i18n.translation)} />
        }
        return null
    };

    render() {

        return (
            <TopLayout container="container-fluid">
                {this.renderContent()}
                <RaitingComponent {...this.props} />
            </TopLayout>
        );
    }
}
