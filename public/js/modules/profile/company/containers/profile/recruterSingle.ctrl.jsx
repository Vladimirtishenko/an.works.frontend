import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as companyAction from "../../actions/company.action.js";
import * as companyAdminAction from "../../actions/company_admin.action.js";

import RecruterSingleComponent from "../../components/profile/recruters/recruterSingle.jsx";

// Loyouts
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import Layout from "../../components/layouts/layout.jsx";
import TopLayout from "../../../../../components/includes/layout.jsx";

import {
    changeOneOf,
    getOneOf
} from "../../../../../helpers/filters/filter.helper.js" 

import {
    transformToRange
} from "../../../../../helpers/date.helper.js";

function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction,
                ...companyAdminAction
            },
            dispatch
        ),
        dispatch 
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class RecruterSingle extends React.Component {

    checkFilter(){
        const {
            location: { search }
        } = this.props;
       let data =  getOneOf(search.slice(1), 'createdDate');

       return data ? {createdDate: data} : {label: 'currentMonth', createdDate: transformToRange('currentMonth')}

    }
    componentDidMount() {

        const {
            match: { params: { id } },
            actions: { getHireHistory, getStatisticRecruter, getEmployeeProfile, getBalance },
            companyProfile: { companyId },
            location: { search }
        } = this.props,
        setBuyerToFilters = changeOneOf(search.slice(1), {
            'buyerUid': id,
            ...this.checkFilter()
        });
        getStatisticRecruter(companyId,setBuyerToFilters)
        getEmployeeProfile(id, companyId);
        getHireHistory(companyId, setBuyerToFilters);
        getBalance(companyId, search.slice(1));

    }

    componentDidUpdate(prevProps) {

        const { companyProfile: { filters: prevFilters } } = prevProps,
              { history: {replace}, location: {search}, companyProfile: { filters: nextFilters } } = this.props;

        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }

    render() {
        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                    progressBar={false}
                >
                    <RecruterSingleComponent {...this.props}/>
                </Layout>
            </TopLayout>
        );
    }
}
