import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as companyAction from "../../actions/company.action.js";

import PaymentsHistoryComponent from "../../components/profile/paymentsHistrory.jsx";

// layout
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import Layout from "../../components/layouts/layout.jsx";
import TopLayout from "../../../../../components/includes/layout.jsx";
import {
    transformToRange
} from "../../../../../helpers/date.helper.js";

import queryObject from "../../../../../helpers/queryObject.js";
function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class PaymentsHistory extends React.Component {

    componentDidMount() {
        const {
            companyProfile: { companyId },
            actions: {getBalance},
            location: {search}
        } = this.props;
        let filtersNormolize = queryObject.stringify({
            filters:JSON.stringify({label: 'currentMonth',createdDate: transformToRange('currentMonth')})     
        }),
        filterHire =  search.slice(1) ? search.slice(1) : filtersNormolize;
        getBalance(companyId, filterHire);
    }

    componentDidUpdate(prevProps) {

        const { companyProfile: { filters: prevFilters } } = prevProps,
              { history: {replace}, companyProfile: { filters: nextFilters } } = this.props;

        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }

    render() {
        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                    progressBar={false}
                >
                    <PaymentsHistoryComponent {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}
