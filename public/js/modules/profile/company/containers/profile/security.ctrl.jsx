import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as companyAction from "../../actions/company.action.js";

import SecurityComponent from "../../components/profile/security.jsx";
// layout
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import Layout from "../../components/layouts/layout.jsx";

import TopLayout from "../../../../../components/includes/layout.jsx";

function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Security extends React.Component {
    render() {
        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                    progressBar={false}
                >
                    <SecurityComponent {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}
