import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as companyAction from "../../actions/company.action.js";

// layout
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import Layout from "../../components/layouts/layout.jsx";
import PaymentComponent from "../../components/profile/payments.jsx";
import TopLayout from "../../../../../components/includes/layout.jsx";

//helpers
import serialize from '../../../../../helpers/serialize.helper.js'

// HOC
import scriptLoader from '../../../../../hoc/script.hoc.jsx' 

function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...companyAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
@scriptLoader("https://api.fondy.eu/static_common/v1/checkout/ipsp.js")
export default class Payments extends React.Component {

    componentDidMount(){

        const { actions: {getTokenPrice, getBalance}, companyProfile: {companyId, tokenPrice} } = this.props;

        if(!tokenPrice){
            getTokenPrice();
        }

        getBalance(companyId)

    }

    createCheckoutPage(event)
    {
        event.preventDefault();
        const form = event && event.target,
              serializedForm = serialize(form),
              { companyProfile, actions: {getPaymentCheckoutData} } = this.props;

        serializedForm.companyId = parseInt(companyProfile.companyId);

        getPaymentCheckoutData({orderData: serializedForm});
    }

    componentWillUnmount(){
        const {actions: {removePaymentData}} = this.props;

        removePaymentData();
    }

    componentDidUpdate(prevProps){

        const { companyProfile: { checkoutData: prevCheckoutData } } = prevProps,
              { history, actions: {getBalance, sendReceivedData}, companyProfile: {companyId,  checkoutData: nextCheckoutData } } = this.props;

        let comparingProps = _.isEqual(prevCheckoutData, nextCheckoutData);

        if(!comparingProps && nextCheckoutData){

            const url = nextCheckoutData.checkout_url;
            $ipsp('checkout').unbind('callback');
            $ipsp('checkout').config({
                'wrapper':'#checkout',
                'styles' : {
                    'body':{'overflow':'hidden'},
                    '.page-section-shopinfo':{display:'none'},
                    '.page-section-footer':{display:'none'},
                    '.card.card-front':{
                        'width': '315px',
                        'height': '200px',
                        'box-shadow': '0 3px 10px rgba(0, 0, 0, 0.03)',
                        'border-radius': '10px',
                        'border':' 1px solid #cdd1d7',
                        'background-color': '#ffffff'
                    },
                    '.container.page-pne > .form-group, .btn-row':{
                        'width' : '300px',
                        'height': '40px'
                    },
                    '.btn-lime':{
                        'background-color' : '#528be6',
                    },
                    '.btn-lime.disabled, .btn-yellow.disabled':{
                        'background-color' : '#CBDCF7'
                    },
                    '.btn-lime:hover, .btn-yellow:hover, .btn-lime.disabled:hover, .btn-yellow.disabled:hover':{
                        'background-color' : '#6497E9'
                    },
                    '.page-section-overview .container':{
                        'width' : '100%',
                        'max-width' : '100%' 
                    },
                    '.order-amount .txt-amount':{
                        'color' : '#3a3a3c',
                        'font-size': '24px',
                        'font-weight': '500',
                    },
                    '.card.card-back':{
                        'top': '50px',
                        'left':'225px',
                        'box-shadow':'0 3px 10px rgba(0, 0, 0, 0.03)',
                        'border': '1px solid #cdd1d7',
                        'border-radius': '10px',
                        'background-color': '#ffffff',
                        'width':'315px',
                        'height':'200px'
                    },
                    '.card #credit_card_number_row':{
                        'padding-top':'25px',
                    },
                    '.card #credit_card_expire_row':{
                        'padding-top':'20px',
                    },
                    '.card #credit_card_number_row .control-label, .card #credit_card_expire_row .control-label':{
                        'display':'block',
                        'font-size':'14px',
                        'color':'#6e6e7d',
                        'margin-bottom':'8px',
                        'text-transform':'none'
                    },
                    '#credit_card_expire_row':{
                        'padding-top':'20px'
                    },
                    '.card.card-back .stripe':{
                        'background-color': '#cdd1d7',
                        'height':'50px'
                    },
                    '.cards':{
                        'box-shadow':'none',
                        'margin-bottom' : '145px',
                    },
                    '.pages-checkout .container.page-pne .infobox':{
                        'top': 'auto',
                        'bottom': '-115px',
                        'font-size': '12px',
                        'color': '#a7a7af',
                        'background': 'transparent',
                        'width': '330px',
                        'right': '27px',
                        'left': 'auto',
                    },
                    '.page-status .page-section-status .row-group' : {
                        'width' : '690px'
                    },

                    '.page-status .page-section-status .row .col.col-value':{
                        'font-size' : '12px'
                    }
                    // '@media all and (max-width 767px){.page-status:{font-size: 12px}}': ' font'
                    // :{
                    //     '.page-status .page-section-status .row .col.col-value':{
                    //               'font-size' : '12px'
                    //       }
                    // }
                }
            }).scope(function(){
                this.width("100%");
                this.height(800);
                this.addCallback(function(res){
                     const { response_status, action, url } = res;

                        if(action == 'redirect'){
                            this.loadUrl(url);
                            getBalance(companyId);
                        }
                });
                this.loadUrl(url);
                // this.setCssStyle({
                //     // '.pages-checkout':{'background-color':'#000'}
                // });
            });
        }

    }

    render() {
        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                    progressBar={false}
                >
                    <PaymentComponent submit={::this.createCheckoutPage} {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}
