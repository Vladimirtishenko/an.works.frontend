import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Steps from '../../components/smart/steps.smart.jsx'

import companyProfileProgress from '../../decorators/profile.dec.jsx';

// Actions
import * as companyAction from '../../actions/company.action.js'

function mapStateToProps(state) {

    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
			...companyAction
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps)
@companyProfileProgress
class Index extends React.Component {

  render(){

    return <Steps {...this.props} />

  }

}

export default Index;
