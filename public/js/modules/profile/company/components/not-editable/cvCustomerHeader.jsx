import React from "react";

export const CVCustomerHeader = props => (
    <div className="box--white box--rounded shadow--box margin--b-15 padding--15 fl fl--align-c fl--justify-b fl--wrap">
        <div className="fl fl--align--c margin--b-10 margin--md-b-0">
            <span className="font--24 font--500 l-h--1 box--rounded font--highlight-grey padding--xc-15 padding--t-5 padding--b-7 margin--r-5">
                2
            </span>
            <span className="d--inline_block font--12 font--color-secondary">
                Токена за
                <br />
                открытие контактов
            </span>
        </div>
        <div className="fl fl--align-c fl--wrap">
            <ul className="list font--12 font--color-secondary margin--r-30 margin--b-10 margin--md-b-0">
                <li
                    className="list-item list__item--inline-flex fl--align-c margin--r-30 pointer"
                    onClick={() => window.print()}
                >
                    <span className="icon icon--Print font--18 font--color-blue margin--r-5 " />
                    Распечатать
                </li>
                <li className="list-item list__item--inline-flex fl--align-c">
                    <span className="icon icon--export font--18 font--color-blue margin--r-5" />
                    Экспотр
                </li>
            </ul>
            <button
                type="button"
                className="btn btn--primary padding--xc-15 padding--yc-10 pointer"
            >
                Открыть
            </button>
        </div>
    </div>
);
