import React from 'react'

// Componets
import {Card} from '../../../common/components/card.dumb.jsx'

import StaticItem from '../../../../../components/form/staticItem.jsx';
import genderList from '../../../../../databases/general/gender.json'
import recruterList from '../../../../../databases/general/mainPositionRecruter.json';

import Avatar from '../../../common/components/not-editable/avatar.jsx'

import {
    separateDate,
    preparationMonth,
    getTextMonth
} from '../../../../../helpers/date.helper.js'

import {
    getGenderStaticName
} from '../../../../../helpers/gender.helper.js'


export default class RecruiterMain extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            ...props
        } 
    }

    checkPositionRecruter(value){
        let position = '';
        recruterList.forEach((item, i) =>{
            if(item.label == value){
                position = item.name;
            }
         }
        );
        return position;
    }
    render(){
        let {onEdit, translation, profile, profile: { positionTitle = '', gender = ''}, user: { uid, uniq } } = this.props,
            date = separateDate(profile.birthday),
            monthPrepare = preparationMonth(translation.month),
            datePutTogether = date.day + ' ' + getTextMonth(date.month, monthPrepare) + ' ' + date.year,
            genderRealName = getGenderStaticName(gender),
            position = this.checkPositionRecruter(positionTitle);

        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">
                    Ведущий рекрутер
                </h2>
                <Card
                    icon="personal"
                    iconRight="edit"
                    link={translation.edit}
                    linkAction={onEdit}
                    linkComponent="info"
                    title={translation.personalInfo}
                    cardPadding="padding--15 padding--md-30"
                >
                    {/* <div className="row"> */}
                        {/*
                        TODO: NOT MVP
                         <div className="col-md-3">
                            <Avatar
                                uniq={uniq}
                                uid={uid}
                                alt="hr"
                                className="img-fluid"
                            />
                        </div> */}
                        {/* <div className="col-md-9"> */}
                            <div className="row word-break--word">
                                <div className="col-6 col-sm-4">
                                    <div className="font--color-inactive font--12 margin--b-15">{translation.firstName}</div>
                                        <StaticItem
                                            staticClass="font--14 form__static-input  margin--b-15 font--capitalized "
                                            staticText={`${profile.firstName} ${profile.lastName}`}
                                        />
                                </div>
                                <div className="col-6 col-sm-4">
                                    <div className=" font--capitalized font--color-inactive font--12 margin--b-15">{translation.birthday}</div>
                                    <StaticItem
                                        staticClass="font--14 font--color-primary"
                                        staticText={datePutTogether}
                                    />
                                </div>
                                <div className="col-6 col-sm-4">
                                    <div className=" font--capitalized font--color-inactive font--12 margin--b-15">{translation.gender}</div>
                                    <StaticItem
                                        staticClass="font--14 font--color-primary"
                                        staticText={genderRealName}
                                    />
                                </div>
                                <div className="col-6 col-sm-4 col-lg-8">
                                    <div className=" font--capitalized font--color-inactive font--12 margin--b-15">{translation.workPosition}</div>
                                        <StaticItem
                                            staticClass="font--14 font--color-primary"
                                            staticText={position}
                                        />
                                </div>
                            </div>
                        {/* </div>
                    </div> */}
                </Card>
            </React.Fragment>
        )
    }
}
