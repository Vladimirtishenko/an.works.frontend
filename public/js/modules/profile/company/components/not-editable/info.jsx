import React from "react";

import { Card } from "../../../common/components/card.dumb.jsx";

import countries from "../../../../../databases/general/countries.json";

import { separateDate } from "../../../../../helpers/date.helper.js";
import { Notifications } from "../../../common/components/not-editable/notifications.jsx";
import bonusVocabularies from '../../../../../databases/general/bonus.json';
import {normalizeStaff} from "../../../../../helpers/staff.helper.js"
export default class InfoNotEditable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            country: "",
            city: ""
        };
    }

    componentDidMount() {
        let {
                companyProfile: {
                    company: { companyInfo }
                }
            } = this.props,
            { country, city } = companyInfo || undefined,
            countryIndex = country
                ? _.findIndex(countries, { label: country })
                : null,
            countrySingleObject =
                countryIndex > -1 ? countries[countryIndex] : null;

        if (countrySingleObject) {
            this.citiesUpload(countrySingleObject, city);
        }
    }

    componentDidUpdate(prevProps){
        let {
            companyProfile: {
                company: { 
                    companyInfo:{
                        city: citiesCurrent,
                        country: countryCurrent
                    }
                }
            }
        } = this.props,
        {
            companyProfile: {
                company: { 
                    companyInfo:{
                        city: citiesPrev,
                        country: countryPrev
                    }
                }
            }
        } = prevProps,
        { db } = this.state;

        if(citiesCurrent !== citiesPrev || countryCurrent !== countryPrev){
            
            const  countryIndex = citiesCurrent ? _.findIndex(db, { label: citiesCurrent }) : null, 
                    countrySingleObject = countryIndex > -1 ? db[countryIndex] : null;
            
            if(!countrySingleObject) return;

            this.setState((prevState) => ({
                ...prevState,
                city: countrySingleObject.name
            }))

        }

    }

    async citiesUpload(country, city) {
        const db = await (await import(`../../../../../databases/countries/${
            country.label
        }/cities.${country.label}.json`)).default;

        let citiesIndex = city ? _.findIndex(db, { label: city }) : null,
            citiesName = citiesIndex > -1 ? db[citiesIndex] : {};
        this.setState({
            country: country.name,
            city: citiesName.name,
            db
        });
    }

    getBonusList(bonus){
        if(!bonus) return '';

        let bonusListArr = [];
        _.forEach(bonusVocabularies, (item, i) => {          
            if(bonus[item.label]){
                bonusListArr.push(
                    <span key={i} className={`margin--l-5 margin--r-5 margin--b-5 bonus-static bonus-static--${item.name}`} data-title={item.title}></span>
                );
            }
        })
        return bonusListArr;
    }

    render() {
        let { i18n:{translation = {}} = {}, companyProfile:{company:{companyInfo:{staff = "", birthday = "", bonuses = "", website = "", description = "", domains = [], previlages = {}} = {} ,name = ""} = {}} = {} , onEdit} = this.props || {},
            date = separateDate(birthday),
            { country, city } = this.state,
            joinLocation = country && city ? country + ", " + city : "",
            bonusList = ::this.getBonusList(previlages),
            staffNumber = normalizeStaff(staff);
        return (
            <React.Fragment>
                 <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">
                    {translation.employerProfile}
                </h2>
                {/* 
                TODO: not mvp
                <Notifications {...this.props} /> */}

                <div className="form">
                    <Card
                        icon="company_info"
                        iconRight="edit"
                        link={translation.edit}
                        linkAction={onEdit}
                        linkComponent="info"
                        title={translation.employersInfo} 
                    >
                        <div className="padding--15 padding--md-30 word-break--word">
                            <div className="row">
                                <div className="col-sm-4 col-lg-3 margin--b-15 margin--sm-bottom-0">
                                    <div className="font--capitalized font--14 font--color-primary margin--b-10">
                                        {name}
                                    </div>
                                    <div className="link font--12 margin--b-10 font--color-blue">
                                       { website}
                                    </div>
                                    {domains && domains.map((item, i) => {
                                            return (
                                                <div
                                                    key={i}
                                                    className="link font--12 margin--b-10 font--color-blue"
                                                >
                                                    {item}
                                                </div>
                                            );
                                        })}
                                </div>
                                <div className="col-sm-8 col-lg-9">
                                    <ul className="breadcrumbs margin--b-10">
                                        <li className="breadcrumbs__item font--color-primary font--14">
                                            {joinLocation}
                                        </li>
                                        <li className="breadcrumbs__item font--color-secondary font--14">
                                            {(date && date.year) || ""}
                                        </li>
                                        <li className="breadcrumbs__item font--color-secondary font--14">
                                            {`${staffNumber} ${translation.staffs}`}
                                        </li>
                                    </ul>
                                    <div className="font--12 font--color-secondary">
                                        {description}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr className="hr--thin margin--0" />
                        <div className="padding--15 padding--md-30 word-break--word">
                            <div className="row">
                                <div className="col-sm-4 col-lg-3 margin--b-15 margin--sm-bottom-0">
                                    <div className="font--capitalized font--12 font--color-inactive margin--b-10">
                                        {translation.bonuses}
                                    </div>
                                </div>
                                <div className="col-sm-8 col-lg-9 padding--lg-r-0 "> 
                                    <div className="fl fl--wrap margin--b-10 margin-minus-xc-5">
                                        {bonusList}
                                    </div>
                                    <div className="font--12 font--color-secondary ">
                                        {bonuses}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Card>
                </div>
            </React.Fragment>
        );
    }
}
