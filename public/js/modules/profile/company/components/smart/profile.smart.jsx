import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ProfileCtrl from '../admin_components/profile.ctrl.jsx'
// Actions
import * as companyAdminActions from '../../actions/company_admin.action.js'

function mapStateToProps(state) {
    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators({
            ...companyAdminActions
        }, dispatch),
        dispatch
    }
}

@connect(mapStateToProps, mapDispatchToProps)
class Profile extends React.Component {

  constructor(props){
    super(props);
  }

  render(){
    return <ProfileCtrl {...this.props} />;
  }

}

export default Profile;
