import React from "react";

// Form Componets
import Input from "../../../../../components/form/text.jsx";
import Bonus from "../../../../../components/form/checkbox.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import Range from "../../../../../components/form/range/index.jsx";
import Textarea from "../../../../../components/form/textarea.jsx";
import DateCustom from "../../../../../components/form/dateCustom.jsx";

import { Card } from "../../../common/components/card.dumb.jsx";
// Additional components
import Domains from '../editable/domain.jsx';
import Location from '../../../common/components/editable/location.jsx';
import bonusVocabularies from '../../../../../databases/general/bonus.json';
import numberOfStaff from '../../../../../databases/general/numberOfStaff.json';

import {
    separateDate,
    preparationMonth
} from "../../../../../helpers/date.helper.js";

import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

export default class InfoEditable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ...props,
            bonusVocabularies: bonusVocabularies
        }

    }
    getBonusList(bonus, name){

        let {bonusVocabularies} = this.state,
            newBonusObject = [];

        if(!bonusVocabularies) return {};

        _.forOwn(bonusVocabularies, (item, i) => {

            let { label } = item,
                value = bonus[label] ? {checked: true, value: true}: {};
            newBonusObject.push({...item, ...value,
                name: `${name}.${item.label}`,
                title: item.title,
                classNameInput:'checkbox__input--bonus',
                checkboxIcon:`checkbox__checkmark--bonus checkbox__checkmark--bonus-${item.name}`,
                innerWrapperClass: "checkbox__inner-wrapper--bonus margin--r-5 margin--l-5 display--b-l"});
        })

        return newBonusObject;

    }
    normalizeRangeData() {
        let language = {};

        for (var i = 0; i < numberOfStaff.length; i++) {
            language[numberOfStaff[i].label] = numberOfStaff[i].desc;
        }

        return language;
    }
    render(){
        let {
                companyProfile: {
                    company : {
                        companyInfo: {
                            birthday = '',
                            domains = [],
                            city = '',
                            website = '',
                            country = '',
                            description = '',
                            bonuses = '',
                            previlages=[],
                            staff = 1
                        } = {},
                        name = ''
                    } = {},
                    profile
                },
                i18n:{translation = {}} = {}
            }= this.props,
            date = separateDate(birthday || new Date()),{day,month,year} = date,
            monthPrepare = preparationMonth(translation.month),
            getBonus = ::this.getBonusList(previlages, 'companyInfo.previlages') || [],
            dateYear = new Date().getFullYear(),
            valueList = ::this.normalizeRangeData(numberOfStaff),
            staffs = Number(staff);

        return (
            <Card icon="company_info" title={translation.employersInfo} cardPadding="padding--15 padding--md-30">
                <Input
                    validators={["required"]}
                    errorMessages={[ translation.errorMessageRquired]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={name}
                    label={translation.companyName+ '*'}
                    name="company.name"
                    readonly={true}
                />
                <Input
                    validators={["required", "isUrl"]}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageWebSite]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    inputClass="input form__input"
                    value={website}
                    label={translation.webSite + '*'}
                    name="companyInfo.website"
                />

                <Domains domains={domains} translation={translation} />

                <Location
                    name="companyInfo"
                    location={{ country: country, city: city }}
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="form__label col-10 col-md-4 col-lg-3 font--14 order-1"
                    className="col-md-6 col-lg-5 padding--md-0 order-2 order-md-1"
                />
                <DateCustom
                    validators={["required", "isDate"]}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageIsDateRange]}
                    wrapperClass="row fl--align-c margin--b-30"
                    labelClass="label form__label font--14 col-md-4 col-lg-3"
                    coverClass="col-md-7 col-xl-5 padding--md-0"
                    day={{
                        name: "companyInfo.birthday[]",
                        value: day,
                        available: false
                    }}
                    month={{
                        name: "companyInfo.birthday[]",
                        value: month,
                        options: monthPrepare,
                        static: "Месяц",
                        available: false
                    }}
                    year={{
                        name: "companyInfo.birthday[]",
                        value: year,
                        className: "number col-4 col-md-3 padding--0",
                        maxYear: dateYear
                    }}
                    label={translation.foundationDate+ '*'}
                />

                <Range
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="form__label col-md-4 col-lg-3 form__label--renge font--14"
                    inputWrapperClass="col-md-6 col-lg-5 padding--md-0"
                    name="companyInfo.staff"
                    min={0}
                    max={5}
                    step={1}
                    value={staffs}
                    label={translation.numberOfStaff+ '*'}
                    dots={true}
                    valueList={valueList}
                />

                <Textarea
                    validators={["required", "minStringLength:100", "maxStringLength:2000"]}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageMinLeng100, translation.errorMessageMaxLeng2000]}
                    classes="row margin--b-20"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    textAreaWrapper="col-md-8 col-lg-9 padding--md-0 padding--md-r-15"
                    textAreaClass="form__text-area"
                    label={translation.companyDescription+ '*'}
                    rows="7"
                    name="companyInfo.description"
                    value={description}
                    required={true}
                />
                <Bonus
                    wrapperClass="checkbox row fl--align-c margin--b-20"
                    labelWrapperClass="label form__label font--14 col-md-4 col-lg-3"
                    wrapperAllCheckbox="col-md-8 col-lg-9 padding--md-0  margin-minus-xc-5"
                    isValid={true}
                    value={getBonus || []}
                    label={translation.bonuses}
                />
                <Textarea
                    validators={["maxStringLength:500"]}
                    errorMessages={[translation.errorMessageMaxLeng500]}
                    classes="row margin--b-20"
                    labelClass="form__label col-md-4 col-lg-3 font--14"
                    textAreaWrapper="col-md-8 col-lg-9 padding--md-0 padding--md-r-15"
                    textAreaClass="form__text-area"
                    label={translation.additionalBonuses}
                    rows="7"
                    value={bonuses}
                    name="companyInfo.bonuses"
                />
            </Card>
        );
    }
}
