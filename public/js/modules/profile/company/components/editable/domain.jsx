import React from "react";
import Input from "../../../../../components/form/text.jsx";
import uniqid from "uniqid";
import ParentStep from "../../../common/components/parent/main.jsx";

import { DeleteButton } from "../../../../../components/form/deleteButton.jsx";
import Tooltip from "../../../../../components/widgets/toolTip.jsx";
import tooltipInfo from "../../../../../databases/general/tooltipInfoCompany.json";
import rule from "../../../../../configuration/rules.json";
class Domains extends ParentStep {
    constructor(props) {
        super(props);

        let { domains } = props,
            arrayDomains =
                domains && Array.isArray(domains)
                    ? domains.slice(0)
                    : domains
                    ? [domains]
                    : [],
            firstAdditionalDomain =
                Array.isArray(arrayDomains) && arrayDomains.length
                    ? arrayDomains.shift()
                    : "";

        this.state = {
            first: firstAdditionalDomain,
            domains: arrayDomains ? ::this.setNormalizeData(arrayDomains) : []
        };
    }

    static defaultProps = {
        labelClass: "form__label col-10 col-md-4 col-lg-3 font--14 order-1",
        inputWrapperClass: "col-md-6 col-lg-5 padding--md-0 order-2 order-md-1",
        dopLabelClass: "label form__label font--14 col-md-4 col-lg-3",
        dopInputWrapperClass: "col-md-6 col-lg-5 padding--md-0",
        deleteBtn: "link link--under link--grey font--12 absolute--lg absolute--lg-r-25-p absolute--lg-yc-center margin--md-auto-l margin--lg-0 margin--t-5 margin--l-auto margin--r-15 margin--md-r-17-p"
    }

    createAnotherField() {
        let id = uniqid(),
            domains = this.state.domains;

        domains.push({ id: id });

        this.setState({ domains: domains });
    }

    removeDomains(event) {
        let id =
            event.target && event.target.dataset && event.target.dataset.id;

        if (id) {
            let domains = _.filter(this.state.domains, item => {
                if (item.id != id) {
                    return item;
                }
            });

            this.setState({ domains: domains });
        }
    }

    render() {
        let { first, domains } = this.state,
        limitDomains = rule.limitDomains > domains.length,
        {translation = {}} = this.props;

        return (
            <div>
                <Input
                    wrapperClass="row  fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.inputWrapperClass}
                    inputClass="input form__input"
                    label={translation.additionalDomainName}
                    value={first}
                    name="companyInfo.domains[]"
                >
                 <Tooltip
                        wrapper="
                            margin--b-10
                            margin--md-b-0
                            col-2
                            order-1
                        "
                        description={translation.tooltipDomainName}
                    />
                </Input>

                {(domains &&
                    domains.map((item, i) => {
                        return (
                            <div
                                className="margin--b-20 relative"
                                key={item.id}
                            >
                                <Input
                                    wrapperClass="row fl--align-c margin--b-10"
                                    labelClass={this.props.dopLabelClass}
                                    inputWrapperClass={this.props.dopInputWrapperClass}
                                    inputClass="input form__input"
                                    name="companyInfo.domains"
                                    label={translation.additionalDomainName}
                                    value={item.data}
                                />
                                <DeleteButton
                                    className={this.props.deleteBtn}
                                    onClick={::this.removeDomains}
                                    data-id={item.id}
                                    text={translation.delete}
                                />
                            </div>
                        );
                    })) ||
                    null}
                {
                    limitDomains && (
                        <div className="row margin--b-20">
                            <div className="offset-md-4 col-md-8 offset-lg-3 padding--md-0">
                                <span
                                    className="padding--r-20 link font--12 font--color-blue"
                                    onClick={::this.createAnotherField}
                                >
                                    <span className="icon icon--add" />
                                        {translation.addDomain}
                                </span>
                            </div>
                        </div>
                    ) || null
                }

            </div>
        );
    }
}

export default Domains;
