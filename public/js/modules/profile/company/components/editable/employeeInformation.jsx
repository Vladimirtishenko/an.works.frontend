import React from "react";

// Form Componets
import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import DateCustom from "../../../../../components/form/dateCustom.jsx";
import StaticItem from "../../../../../components/form/staticItem.jsx";
import Radio from "../../../../../components/form/radio.jsx";
import Avatar from "../../../common/components/editable/avatar.jsx";

import { Card } from "../../../common/components/card.dumb.jsx";

import mainPositionRecruter from "../../../../../databases/general/mainPositionRecruter.json";

import {
    separateDate,
    preparationMonth
} from "../../../../../helpers/date.helper.js";

import { comparingGenderValue } from "../../../../../helpers/gender.helper.js";

import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

export default class EmployeeInformation extends React.Component {

    static defaultProps = {
        wrapperClass:"row fll--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-3",
        inputWrapperClass: "col-md-6 col-lg-5 col-xl-4 padding--md-0"
    }
    
    render() {
        let { translation, profile:{
            firstName = '',
            lastName = '',
            gender = '',
            positionTitle = '',
            birthday = ''
            } = {}
        } = this.props || {},
            date = separateDate(birthday),
            monthPrepare = preparationMonth(translation.month);

        return (
            <Card
                icon="personal" 
                title={translation.personalInfo}
                cardPadding="padding--15 padding--md-30"
            >
                {/* <div className="row"> */}
                    {/*
                    TODO: NOT MVP 
                    <div className="col-md-3">
                        <Avatar wrapperClass="avatar--recruter" />
                    </div> */}
                    {/* <div className="col-md-8 padding--md-l-0"> */}
                        <Input
                            validators={["required", "isName",'maxStringLength:50']}
                            errorMessages={[translation.errorMessageRquired, translation.errorMessageName,translation.errorMessageMaxLeng50 ]}
                            wrapperClass={`${this.props.wrapperClass} margin--b-10`}
                            labelClass={this.props.labelClass}
                            inputWrapperClass={this.props.inputWrapperClass}
                            inputClass="input form__input"
                            value={firstName}
                            label={translation.firstName + '*'}
                            name="profile.firstName"
                        />
                        <Input
                            validators={[ "required","isName",'maxStringLength:50']}
                            errorMessages={[translation.errorMessageRquired, translation.errorMessageNameTitle,translation.errorMessageMaxLeng50 ]}
                            wrapperClass={`${this.props.wrapperClass} margin--b-10`}
                            labelClass={this.props.labelClass}
                            inputWrapperClass={this.props.inputWrapperClass}
                            inputClass="input form__input"
                            value={lastName}
                            label={translation.lastName + '*'}
                            name="profile.lastName"
                        />
                        <Radio
                            validators={["radioRequired"]}
                            errorMessages={[translation.errorMessageRadio]}
                            wrapperClass={`${this.props.wrapperClass} margin--b-20`}
                            labelClass={this.props.labelClass}
                            label={translation.gender + '*'}
                            name="profile.gender"
                            required={true}
                            value={comparingGenderValue(
                                gender,
                                "fl fl--align-c"
                            )}
                            wrapperClassRadio="padding--md-0"
                            wrapRadio={this.props.inputWrapperClass}
                        />

                        <DateCustom
                            validators={["required", "isDate"]}
                            errorMessages={[translation.errorMessageRquired , translation.errorMessageIsDateRange]}
                            wrapperClass={`${this.props.wrapperClass} margin--b-10`}
                            labelClass={this.props.labelClass}
                            coverClass={this.props.inputWrapperClass}
                            day={{
                                name: "profile.birthday[]",
                                value: date.day,
                            }}
                            month={{
                                name: "profile.birthday[]",
                                value: date.month,
                                options: monthPrepare,

                            }}
                            year={{
                                name: "profile.birthday[]",
                                value: date.year,
                            }}
                            label={translation.birthday + '*'}
                        />
                        <Combobox
                            validators={["combobox"]}
                            errorMessages={[translation.errorMessageCombobox]}
                            name="profile.positionTitle"
                            wrapperClass="row  fl--align-c combobox"
                            labelClass={this.props.labelClass}
                            className={this.props.inputWrapperClass}
                            label={translation.workPosition + '*'}
                            list={mainPositionRecruter}
                            value={positionTitle}
                        />
                    {/* </div> */}
                {/* </div> */}
            </Card>
        );
    }
}
