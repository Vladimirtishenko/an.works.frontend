import React from "react";

// Not Editable
import InfoAboutMainRecruiter from "../not-editable/recruterMainInfo.jsx";
import ContactMainRecruiter from "../../../common/components/not-editable/contact.jsx";

// Editable
import ContactCard from "../../../common/components/editable/contactCard.jsx";
import EmployeeInformation from "../editable/employeeInformation.jsx";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";

@i18nMonthDecorator
export default class EmployeeMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            info: false,
            contact: false
        };
    }

    changeView(event) {
        let component =
            (event &&
                event.target &&
                event.target.dataset &&
                event.target.dataset.component) ||
            null;

        if (component) {
            this.setState({
                [component]: !this.state[component]
            });
        }
    }

    propsPersonalMapper(props) {
        let personal = ["firstName", "lastName", "birthday", "gender","positionTitle"],
            object = {};

        _.forEach(personal, (item, i) => {
            if (props[item]) {
                object[item] =
                    item == "birthday"
                        ? new Date(props[item]).valueOf()
                        : props[item];
            }
        });

        return object;
    }

    submitInfo(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        const serializedForm = serialize(form),
            {
                oauth: { user: { email } },
                companyProfile: {
                    companyId,
                    profile,
                    profile: { contactInfo }
                },
                actions: { updateEmployeeProfile }
            } = this.props,
            date =
                serializedForm.profile.birthday &&
                Array.isArray(serializedForm.profile.birthday)
                    ? serializedForm.profile.birthday.reverse()
                    : "",
            birthday = date ? new Date(date.join('/')).valueOf() : "";
        serializedForm.profile.birthday = birthday;

        if (_.isEqual(serializedForm.profile, this.propsPersonalMapper(profile)) ) {
            return this.setState({ info: false });
        }

        updateEmployeeProfile(
            { ...serializedForm.profile, contactInfo },
            companyId,
            email
        );
    }

    submitContact(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            {
                oauth: { user: { email } },
                companyProfile: {
                    companyId,
                    profile,
                    profile: { contactInfo: currentContactInfo }
                },
                actions: { updateEmployeeProfile }
            } = this.props,
            { contactInfo } = serializedForm || {};

        if (_.isEqual(contactInfo, currentContactInfo)) {
            return this.setState({ contact: false });
        }

        updateEmployeeProfile(
            { ...profile, contactInfo: contactInfo },
            companyId,
            email
        );
    }
    componentDidMount() {
        if (
            this.props.companyProfile &&
            !this.props.companyProfile.telegramRegistrationUrl
        ) {
            this.props.actions.isTelegramVerified();
            this.props.actions.getTelegramRegistrationUrl();
        }
    }
    componentDidUpdate(prevProps, prevStep) {
        let {
                companyProfile: { profile: nextProfile }
            } = this.props,
            {
                companyProfile: { profile: prevProfile }
            } = prevProps,
            prevPersonalInfo = this.propsPersonalMapper(prevProfile),
            nextPersonalInfo = this.propsPersonalMapper(nextProfile),
            contactPrev = nextProfile && nextProfile.contactInfo,
            contactNext = prevProfile && prevProfile.contactInfo;

        if (!_.isEqual(prevPersonalInfo, nextPersonalInfo)) {
            this.setState({
                info: false
            });
        }

        if (!_.isEqual(contactPrev, contactNext)) {
            this.setState({
                contact: false
            });
        }
    }

    render() {
        let { info, contact } = this.state,
            {
                actions,
                companyProfile: {
                    profile,
                    telegramRegistrationUrl,
                    telegramVerification,
                    telegramVerificationFailed
                },
                i18n: { translation },
                oauth: { user }
            } = this.props;

        return (
            <div className="form">
                {info ? (
                    <ValidatorForm onSubmit={::this.submitInfo} className="form">
                        <EmployeeInformation
                            translation={translation} 
                            profile={profile}
                        />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                {translation.save}
                            </button>
                        </div>
                    </ValidatorForm>
                ) : (
                    <InfoAboutMainRecruiter
                        translation={translation}
                        profile={profile}
                        onEdit={::this.changeView}
                        user={user}
                    />
                )}
                {contact ? (
                     <ValidatorForm onSubmit={::this.submitContact} className="form">
                        <ContactCard
                            actions={actions}
                            contactInfo={(profile && profile.contactInfo) || {}} 
                            inputPhoneSize="col-md-6 col-lg-5 col-xl-4"
                            inputSizeSocial="col-md-6 col-lg-5 col-xl-4"
                            positionBtnRemove="margin--md-r-17-p absolute--lg-r-25-p absolute--xl-r-35-p "
                            wrapBtnTelegram="col-7 col-md-2 offset-md-8 padding--md-0 margin--md-t-10 offset-lg-6 margin--xl-0 padding--xl-xc-15 "
                            telegramVerification={telegramVerification}
                            telegramRegistrationUrl={ 
                                telegramRegistrationUrl && telegramRegistrationUrl.url
                            }
                            telegramVerificationFailed={telegramVerificationFailed}
                        />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                {translation.save}
                            </button>
                        </div>
                    </ValidatorForm>
                ) : (
                    <ContactMainRecruiter  
                        onEdit={::this.changeView}
                        contactInfo={(profile && profile.contactInfo) || {}}
                        notEdit ={true}
                        telegramVerification={telegramVerification}
                        telegramVerificationFailed={telegramVerificationFailed}
                    />
                )}
            </div>
        );
    }
}
