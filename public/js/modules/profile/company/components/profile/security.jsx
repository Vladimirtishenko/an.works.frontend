import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";
import Password from "../../../../../components/form/password.jsx";
import PasswordDoubleField from "../../../../../components/form/passwordComparing.jsx";
import Text from "../../../../../components/form/text.jsx";
import ActiveEmail from "../../../../../modules/profile/applicant/components/modals/activeEmail.jsx";
import { ValidatorForm } from '../../../../../libraries/validation/index.js';
import serialize from "../../../../../helpers/serialize.helper.js";

class Security extends React.Component {
    constructor(props) {
        super(props);
        //{/* TODO: no mvp not delete */}
        // this.state = {
        //     isActiveEmailPopupOpen: false
        // };
    }
    //{/* TODO: no mvp not delete */}
    // handleChangeActiveEmailPopupStatus(status) {
    //     this.setState({
    //         ...this.state,
    //         isActiveEmailPopupOpen: status
    //     });
    // }
    onSubmit(event){
        const { target: form } = event,
                serializedForm = serialize(form),
                { password, oldPassword } = serializedForm, 
                { actions: { updateProfilePassword }, oauth: { user: { uid } } } = this.props;

        updateProfilePassword({oldPassword: oldPassword,password: password}, uid);

    }
    render() {
        const {i18n:{translation = {}} = {}} = this.props;
        // const { isActiveEmailPopupOpen } = this.state;
        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">
                    {translation.security}
                </h2>
                <div className="form">
                    {/* TODO: no mvp not delete */}
                    {/* <Card
                        icon="email"
                        title="Адрес электронной почты"
                        cardPadding="padding--15 padding--md-30"
                    >
                        <form action="">
                            <Text
                                wrapperClass="row fl--align-c margin--b-10"
                                labelClass="label form__label font--14 col-md-4 col-lg-3"
                                inputWrapperClass="col-md-5 col-lg-4 padding--md-0"
                                inputClass="input form__input"
                                errorClass="margin--r-30"
                                label="Почта"
                                name="email"
                            />
                        </form>
                    </Card> */}

                    <Card
                        icon="password"
                        title={translation.password}
                        cardPadding="padding--15 padding--md-30"
                    >
                        <ValidatorForm onSubmit={::this.onSubmit}>
                            <Password
                                validators={[
                                    "required",
                                    "isPassword"
                                ]}
                                errorMessages={[ translation.errorMessageRquired,translation.errorMessagePassword]}
                                wrapperClass="row fl--align-c margin--b-10"
                                labelClass="label form__label font--14 col-md-4 col-lg-3"
                                inputWrapperClass="col-md-6 col-lg-4 padding--md-0"
                                inputClass="input form__input"
                                name="oldPassword"
                                required={true}
                                label={translation.currentPassword}
                                placeholder={translation.currentPassword}
                            />
                             <PasswordDoubleField 
                                validators={[
                                    "required",
                                    "isPasswordComparing",
                                    "isPassword"
                                ]}
                                errorMessages={[translation.errorMessageRquired, translation.errorMessagePasswordsEqually, translation.errorMessagePassword]}
                                required={true}
                                wrapperClass="row fl--align-c margin--b-10"
                                labelClass="label form__label font--14 col-md-4 col-lg-3"
                                inputWrapperClass="col-md-6 col-lg-4 padding--md-0"
                                inputClass="input form__input"
                                list={[
                                    {label: translation.newPassword || '', name: "password", placeholder: translation.newPassword || '', type: "password"},
                                    {label: translation.redoNewPassword || '', name: "repeat-password", placeholder: translation.repeatPassword || '', type: "password", btn: true}
                                ]}
                            />
                             <div className="row">
                                <div  className="col-7 col-md-4 col-lg-3 col-xl-2 offset-md-6 offset-lg-4 offset-xl-5 padding--md-r-0 padding--lg-r-15">
                                <button className="btn btn--primary width--100 font--14 pointer padding--10">{translation.save}</button>
                                </div> 
                             </div>
                        </ValidatorForm>
                    </Card>
                </div>
                {/* TODO: no mvp not delete */}
                {/* {isActiveEmailPopupOpen && (
                    <ActiveEmail
                        closePopup={() =>
                            this.handleChangeActiveEmailPopupStatus(false)
                        }
                    />
                )} */}
            </React.Fragment>
        );
    }
}

export default Security;
