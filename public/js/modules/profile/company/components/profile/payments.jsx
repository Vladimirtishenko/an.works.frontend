import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";
import Number from "../../../../../components/form/number.jsx";
import Radio from "../../../../../components/form/radio.jsx";
import ComboboxComponent from "../../../../../components/form/combobox.jsx";
import Hidden from "../../../../../components/form/hidden.jsx";
import Checkbox from "../../../../../components/form/checkbox.jsx";
import { DeleteButton } from "../../../../../components/form/deleteButton.jsx";
import { Link } from "react-router-dom";

import { ValidatorForm } from '../../../../../libraries/validation/index.js';
import TokensCount from "../../../common/components/tokensCount.jsx";

export default class PaymentsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bidsCount: 0
        };
    }

    onChangeBidsCount(value) {
        this.setState({
            ...this.state,
            bidsCount: value ? value : 0
        });
    }
    checkStatus(status){
        if(!status) return false;

        switch(status) {
            case 'pending':
            case 'waitingApprove':
            case 'inActive':
                return false;
                break;
            default: return true;
        }
    }

    render() {
        const { bidsCount } = this.state,
              {
                  submit,
                  companyProfile: {
                      company:{
                        status = ''
                      },
                      balance: {
                          tokensCount
                      } = {},
                      checkoutData,
                      tokenPrice: {
                          currenciesRate: { UAH, USD } = {}
                      } = {}
                  },
                  i18n:{translation = {}} = {}
              } = this.props,
            statusCompany = this.checkStatus(status);

        return (
            <div className="container paddi">
                <div className="row fl fl--align-c">
                    <div className="col-md-8 col-xl-9 padding--md-20 padding--xc-30 margin--b-20 margin--md-b-35"> 
                        <span className="font--18 font--500 ">{translation.payment}</span>
                    </div>
                    <div className="col-md-4 col-xl-3 padding--0 fl fl--justify-end"> 
                        <TokensCount tokensCount={tokensCount}/>
                    </div>
                </div>
                <div className="row">
                    <div className="form width--100">
                        <ValidatorForm onSubmit={submit} className="form">
                            <Card
                                icon="payment_details"
                                title={translation.paymentDetails}
                                cardPadding="fl fl--dir-col padding--15 padding--md-30"
                            >
                               { statusCompany && 
                                   ( <div className="row fl fl--wrap margin--b-25">
                                        <span className="col-lg-2 col-md-2 font--14 font--color-secondary padding--t-0 padding--xl-t-10 padding--r-0">
                                            {translation.bIDSQuantity}:
                                        </span>
                                        <Number 
                                            validators={["required", "matchRegexp:[^0]+" ]}
                                            errorMessages={[ translation.errorMessageRquired, translation.errorMessageNumber]} 
                                            wrapperClass="col-lg-2 col-md-2 col-12 number width--90-px  font--24 font--bold margin--lg-r-30 margin--b-15"
                                            min={0}
                                            max={999}
                                            name="orderItem.count"
                                            patterns={/^\d+\.\d{0,2}$/}
                                            value={bidsCount}
                                            required={true}
                                            parentOnChange={value =>
                                                this.onChangeBidsCount(value)
                                            }
                                        />
                                        <Hidden name="currency" value="UAH"/>
                                        <div className="col-lg-3 col-md-4 color-border-box margin--b-15">
                                            <span className="color-border-box__value">
                                                {(bidsCount * UAH).toFixed(2)} UAH
                                            </span>
                                            <span className="color-border-box__bottom-label">
                                                x {UAH} UAH per BID
                                            </span>
                                        </div>
                                        <div className="col-lg-3 col-md-4 color-border-box margin--b-15">
                                            <span className="color-border-box__value color-border-box__value_disabled">
                                                {bidsCount * USD} USD
                                            </span>
                                            <span className="color-border-box__bottom-label">
                                               {translation.uSDEquivalent}
                                            </span>
                                        </div>
                                        <button type="submit" className="height--px-40 btn btn--theme-blue width--px-170 margin--l-15">{translation.issuePayment}</button>
                                    </div>  
                                   ) || (
                                       <div className="font--14 font--color-primary margin--b-15 margin--md-b-30">
                                            <p className="margin--b-10">{translation.paymantVerification}</p>
                                            <p>{translation.validationApplicant}</p>
                                       </div>
                                   )
                               }
                                <div className="hr--top padding--t-20 fl fl--align-c margin-minus-xc-15 margin--md-minus-xc-30 padding--xc-15 padding--md-l-30">
                                    <span className="pseudo-icon__info margin--r-5" />
                                    <span className="font--12 font--hint-grey">
                                        {translation.onePayment}
                                    </span>
                                </div>
                            </Card>
                        </ValidatorForm>
                    </div>
                    {checkoutData && <div className="form col-12 padding--0">
                        <Card
                            id="checkout"
                            icon="payment"
                            title={translation.paymentDetails}
                        >

                        </Card>
                    </div> || null}
                    <div className="col-12 ">
                        <div className="d--block font--14 font--color-secondary padding--xc-15">
                            <span>
                                {translation.dataIsProtected}
                                <br />
                                {translation.questionsAboutPayments}{" "}
                                <Link
                                    to="/contact"
                                    className="link font--color-primary link--under"
                                >
                                    {translation.supportService}
                                </Link>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row" />
            </div>
        );
    }
}
