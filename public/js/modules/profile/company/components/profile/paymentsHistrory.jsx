import React from 'react';

import {Card} from '../../../common/components/card.dumb.jsx'
import {Link} from 'react-router-dom'

// Filters
import LimitFilter from "../../../../../components/filters/publicFilters/limit.jsx";
import HireDatesFilter from "../../../../../components/filters/publicFilters/hireDatesFilter.jsx";

import CountOfTotal from '../../../common/components/countOfTotal.jsx';
import Pagination from "../../../common/components/pagination.jsx";
import TokensCount from "../../../common/components/tokensCount.jsx";

import {
    normalizeLimit,
    convertLimitToValue
} from '../../../../../helpers/filters/limit.helper.js';
import {
    convertOffsetToValue
} from '../../../../../helpers/filters/offset.helper.js';
import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js';

import {
    dmyDate
} from '../../../../../helpers/date.helper.js';

import {
    deteleOneOf
} from "../../../../../helpers/filters/filter.helper.js" 

export default class PaymentsHistory extends React.Component {

    changeLimit(label){

        const { companyProfile: { filters, companyId }, actions: {getBalance} } = this.props,
                newFiltersWithLimit = normalizeLimit(filters, label);

        getBalance(companyId, newFiltersWithLimit);

    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { companyProfile: { filters, companyId }, actions: {getBalance} } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getBalance(companyId, filtersNormalizedWithNewOrderBy);

    }

    onChangePeriod(filters){
        const { actions: { getBalance }, companyProfile: {companyId} } = this.props,
        deletOffsetToFilters = deteleOneOf(filters,'offset'); 

        getBalance(companyId, deletOffsetToFilters);
    }

    onChangePagination(filters){
        const { actions: { getBalance }, companyProfile: {companyId} } = this.props;

        getBalance(companyId, filters);
    }

    _getTypeStr(item){
        const {
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        return item.operation == "income" ? item.orderData && item.orderData.paymentMethod ==  "fondy" ? translation.buyTokens : translation.presentTokens : translation.buyContact
    }

    changePagination(filters){

        const {
            companyProfile: { companyId },
            actions: {getBalance}
        } = this.props;

        getBalance(companyId, filters);

    } 

    render() {

        const { i18n:{translation = {}} = {}, actions , companyProfile: { filters = '', balance: { history: { items = [], totalCount } = {}, tokensCount = 0 } = {} } } = this.props,
                limitToValue = convertLimitToValue(filters),
                justOrderBy = orderBy(filters),
                offsetTovalue =  convertOffsetToValue(filters);
        return (
            <div>
                <div className="fl fl--justify-end">
                    <TokensCount tokensCount={tokensCount} />
                </div>
                <Card
                        icon="payment_details"
                        title={translation.balanceDetails}
                    >
                    <div className="padding--md-xc-30 bg--light-gray padding--xc-15 padding--yc-20 padding--xl-yc-25 fl fl--justify-b fl--align-c fl--wrap hr--bottom-theme-light-gray">
                        <div className="fl fl--align-c fl--wrap">
                            <LimitFilter
                                className="padding--xc-5"
                                onChange={::this.changeLimit}
                                value={limitToValue}
                            />
                            <div className="font--color-secondary font--14 margin--t-10 margin--sm-l-10 margin--sm-t-0">
                                {`${translation.balanceDetails} ${translation.for} `}
                                <HireDatesFilter
                                     onChange={::this.onChangePeriod}
                                     filters={filters}
                                />
                            </div>
                        </div>
                    </div>
                    <table className="respons-flex-table"> 
                        <thead className="respons-flex-table__thead">
                            <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                                <th className=" respons-flex-table__th respons-flex-table--xl-w-20 font--left">
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="date"
                                        // data-value={justOrderBy.date || ""}
                                        // className={`${orderingByClass(justOrderBy.date)}`}
                                    >
                                        {translation.date}
                                    </span>
                                </th>
                                <th className=" respons-flex-table__th respons-flex-table--xl-w-20">
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="type"
                                        // data-value={justOrderBy.type || ""}
                                        // className={`${orderingByClass(justOrderBy.type)}`}
                                    >
                                        {translation.transactionType}
                                    </span>
                                </th>
                                <th className=" respons-flex-table__th respons-flex-table--xl-w-20">
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="tokens"
                                        // data-value={justOrderBy.tokens || ""}
                                        // className={`${orderingByClass(justOrderBy.tokens)}`}
                                    >
                                        {translation.numberOfToken}
                                    </span>
                                </th>
                                <th className=" respons-flex-table__th respons-flex-table--xl-w-20">
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="initiator"
                                        // data-value={justOrderBy.initiator || ""}
                                        // className={`${orderingByClass(justOrderBy.initiator)}`}
                                    >
                                        {translation.originator}
                                    </span>
                                </th>
                                <th className=" respons-flex-table__th respons-flex-table--xl-w-20">
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="status"
                                        // data-value={justOrderBy.status || ""}
                                        // className={`${orderingByClass(justOrderBy.status)}`}
                                    >
                                        {translation.condition}
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody className="respons-flex-table__tbody">
                            { 
                                items.length && items.map( (item, i) => {
                                    return (
                                        <tr key={i} className="respons-flex-table__tr flexy-table--padding-30">
                                            <td data-title={translation.date} className=" respons-flex-table__td respons-flex-table--xl-w-20 respons-flex-table--md-w-33 respons-flex-table--w-50  ">
                                                <span className="font--12 font--color-secondary">
                                                    {dmyDate(item.createdDate)}
                                                </span>
                                            </td>
                                            <td data-title={translation.transactionType} className=" word-break--word respons-flex-table__td font--xl-center respons-flex-table--xl-w-20 respons-flex-table--md-w-33 respons-flex-table--w-50  ">
                                                <span className="font--12 font--color-green">
                                                    {this._getTypeStr(item)}
                                                </span>
                                            </td>
                                            <td data-title={translation.numberOfToken} className="word-break--word respons-flex-table__td font--xl-center respons-flex-table--xl-w-20 respons-flex-table--md-w-33 respons-flex-table--w-50  ">
                                                <span className="font--14 font--500 font--color-green">
                                                    {item.tokensCount}
                                                </span>
                                            </td>
                                            <td data-title={translation.originator} className="word-break--word respons-flex-table__td font--xl-center respons-flex-table--xl-w-20 respons-flex-table--md-w-33 respons-flex-table--w-50  ">
                                                <span className="font--12 font--underlined font--color-primary">
                                                    {item.initiatorData.email}
                                                </span>
                                            </td>
                                            <td data-title={translation.condition} className="word-break--word respons-flex-table__td font--xl-center respons-flex-table--xl-w-20 respons-flex-table--md-w-33 respons-flex-table--w-50  ">
                                                <span className="font--10 font--uppercase font--500 font--color-green font--uppercase">
                                                    {translation.completed}
                                                </span>
                                            </td>
                                         </tr>
                                  ) }) 
                                    ||  <tr className="respons-flex-table__tr"><td className="respons-flex-table__td respons-flex-table__td--no-before ">{translation.paymantHistory}</td></tr>
                                  } 
                        </tbody>
                        <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-20  padding--yc-10 fl--wrap">
                            <tr className="d-block width--100">
                                <td className="fl fl--justify-b fl--align-c fl--wrap">
                                    <CountOfTotal text={translation.opiration} limit={limitToValue} total={totalCount} offset={offsetTovalue} />
                                    <div className="fl fl--justify-end">
                                        {
                                            (totalCount > limitToValue) && <Pagination
                                                count={totalCount}
                                                actions={::this.onChangePagination}
                                                filters={filters}
                                                wrapperClass="pagination margin--yc-10"
                                            /> || null
                                        }
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </Card>
            </div>
        )
    }
}
