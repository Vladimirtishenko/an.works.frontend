import React from "react";

import { Card } from "../../../common/components/card.dumb.jsx";
import HireTableRow  from "../../../common/components/not-editable/hireHistory/hireTableRow.jsx";
import Pagination from "../../../common/components/pagination.jsx";

import { TableControls } from "../../../common/components/not-editable/tableControls.jsx";
import { HireContactsInfo } from "../../../applicant/components/modals/hireContactsInfo.jsx";
import CountOfTotal from '../../../common/components/countOfTotal.jsx';

// Filters
import LimitFilter from "../../../../../components/filters/publicFilters/limit.jsx";
import HireDatesFilter from "../../../../../components/filters/publicFilters/hireDatesFilter.jsx";
import RecruiterTableRow from "../../../common/components/not-editable/recruiterSingle/recruiterTableRow.jsx";

import {
    normalizeLimit,
    convertLimitToValue
} from '../../../../../helpers/filters/limit.helper.js';

import {
    convertOffsetToValue
} from '../../../../../helpers/filters/offset.helper.js';

import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js';

import {
    deteleOneOf
} from "../../../../../helpers/filters/filter.helper.js";
export default class hiringHistory extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            needClass: true,
            isContactsShown: false,
            shownContactId: undefined
        };
    }

    handleShowContacts(status, id) {
        this.setState({
            ...this.state,
            isContactsShown: status,
            shownContactId: status ? id : undefined
        });
    }

    changeLimit(label){

        const { companyProfile: { filters, companyId }, actions: {getHireHistory} } = this.props,
                newFiltersWithLimit = normalizeLimit(filters, label);

        getHireHistory(companyId, newFiltersWithLimit);

    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { companyProfile: { filters, companyId }, actions: {getHireHistory} } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getHireHistory(companyId, filtersNormalizedWithNewOrderBy);

    }

    onChangePeriod(filters){
        const { actions: { getHireHistory }, companyProfile: {companyId} } = this.props,
        deletOffsetToFilters = deteleOneOf(filters,'offset');
        getHireHistory(companyId, deletOffsetToFilters);
    }
    onChangePagination(filters){
        const { actions: { getHireHistory }, companyProfile: {companyId} } = this.props;

        getHireHistory(companyId, filters);
    }
    addClassName() {
        this.setState({
            ...this.state,
            needClass: false
        });
    }

    render() { 

        const { i18n:{translation = {}} = {}, actions, companyProfile: { applicantsList: { items = [], totalCount } = {}, filters } } = this.props,
                limitToValue = convertLimitToValue(filters),
                offsetTovalue =  convertOffsetToValue(filters),
              { isContactsShown, shownContactId } = this.state,
                justOrderBy = orderBy(filters),
                shownContact = shownContactId && items.find(c => c.applicantId === shownContactId);
        return (
            <div>
                <h1 className="padding--xc-30 margin--b-20 margin--md-b-35 l-h--1 font--18 font--500">
                    {translation.hiringHistory}
                </h1>
                <Card
                    icon="payment_details"
                    title={translation.hiringHistory}
                >
                    <div className="padding--xl-xc-30 bg--light-gray padding--xc-15 padding--yc-20 padding--xl-yc-25 fl fl--justify-b fl--align-c fl--wrap hr--bottom-theme-light-gray">
                        <div className="fl fl--align-c fl--wrap">
                            <LimitFilter
                                className="padding--xc-5"
                                onChange={::this.changeLimit}
                                value={limitToValue}
                            />
                            <div className="font--color-secondary font--14">
                                {`${translation.hiringHistory} ${translation.for} `}
                                <HireDatesFilter
                                     onChange={::this.onChangePeriod}
                                     filters={filters}
                                />
                            </div>
                        </div>
                    </div>
                    <table className="respons-flex-table">
                        <thead className="respons-flex-table__thead">
                            <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-14
                                            font--left
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="date"
                                        // data-value={justOrderBy.date || ""}
                                        // className={`${orderingByClass(justOrderBy.date)}`}
                                    >
                                        {translation.openingDate}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-14
                                            font--left
                                        "
                                >
                                    {translation.recruiter}
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-14
                                            font--left
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="applicant"
                                        // data-value={justOrderBy.applicant || ""}
                                        // className={`${orderingByClass(justOrderBy.applicant)}`}
                                    >
                                        {`${translation.applicant},${translation.id || ''}`}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-17
                                            font--left
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="position"
                                        // data-value={justOrderBy.position || ""}
                                        // className={`${orderingByClass(justOrderBy.position)}`}
                                    >
                                        {translation.workPosition}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-10
                                            font--left
                                        "
                                >
                                    <span>{translation.inContacts}</span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-10
                                            respons-flex-table--xl-j-center
                                            font--left
                                            font--xl-center
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="price"
                                        // data-value={justOrderBy.price || ""}
                                        // className={`${orderingByClass(justOrderBy.price)} respons-flex-table__sort-des`}
                                    >
                                        {translation.bidsPerContact}
                                    </span>
                                </th> 
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-15
                                            font--left
                                            font--xl-center
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="status"
                                        // data-value={justOrderBy.status || ""}
                                        // className={`${orderingByClass(justOrderBy.status)}`}
                                    >
                                        {translation.condition}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-5
                                            font--left
                                        "
                                >
                                    <span className="">{translation.myCV}</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody className="respons-flex-table__tbody">
                            {
                                items.length && items.map((el, i) => (
                                    <RecruiterTableRow  
                                        key={i} 
                                        data={el} 
                                        handleShowContacts={(status, id) =>
                                            this.handleShowContacts(status, id)
                                        }
                                        actions={actions}
                                    />
                                )) || <tr className="respons-flex-table__tr"><td className="respons-flex-table__td respons-flex-table__td--no-before ">{translation.infoStatistics}</td></tr> 
                            }
                        </tbody>
                        <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-20  padding--yc-10 fl--wrap">
                            <tr className="d-block width--100">
                                <td className="fl fl--justify-b fl--align-c fl--wrap">
                                    <CountOfTotal limit={limitToValue} total={totalCount} offset={offsetTovalue} />
                                    <div className="fl fl--justify-end">
                                        {
                                            (totalCount > limitToValue) && <Pagination
                                                count={totalCount}
                                                actions={::this.onChangePagination}
                                                filters={filters}
                                                wrapperClass="pagination margin--yc-10"
                                            /> || null
                                        }
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    {isContactsShown && shownContact && (
                        <HireContactsInfo 
                            data={shownContact.additionalData.applicant}
                            closeContacts={() => this.handleShowContacts(false)}
                        />
                    )}
                </Card>
            </div>
        );
    }
}
