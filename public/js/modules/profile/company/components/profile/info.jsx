import React from "react";

import serialize from "../../../../../helpers/serialize.helper.js";

// Editable
import InfoEditable from "../editable/info.jsx";

// Not Editable
import InfoNotEditable from "../not-editable/info.jsx";
import { ValidatorForm } from "../../../../../libraries/validation/index.js"; 
export default class InfoProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            info: false
        };
    }

    changeView() {
        this.setState({
            info: !this.state.info
        });
    }

    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            { companyId } = this.props.companyProfile || {},
            date =
                serializedForm.companyInfo.birthday &&
                Array.isArray(serializedForm.companyInfo.birthday)
                    ? serializedForm.companyInfo.birthday.reverse()
                    : "",
            birthday = date ? new Date(date.join('/')).valueOf() : "";

        serializedForm.companyInfo.birthday = birthday;

        this.props.actions.updateCompany(serializedForm, companyId);

        this.setState({
            info: false
        });
    }

    componentDidUpdate(prevProps) {
        let {
                companyProfile: {
                    company: { companyInfo: companyInfoPrev }
                }
            } = prevProps,
            {
                companyProfile: {
                    company: { companyInfo: companyInfoNext }
                }
            } = this.props;

        if (!_.isEqual(companyInfoPrev, companyInfoNext)) {
            this.setState({
                info: false
            });
        }
    }

    render() {
        let { info } = this.state,
        {i18n:{translation = {}} = {}} = this.props;

        return (
            <div className="form">
                {info ? (
                    <ValidatorForm onSubmit={::this.submit} className="form">
                        <InfoEditable {...this.props} />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                {translation.save}
                            </button>
                        </div>
                    </ValidatorForm>
                ) : (
                    <InfoNotEditable
                        onEdit={::this.changeView}
                        {...this.props}
                    />
                )}
            </div>
        );
    }
}
