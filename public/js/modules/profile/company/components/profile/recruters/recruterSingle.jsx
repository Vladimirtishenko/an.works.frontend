import React from "react";
import {Link} from "react-router-dom"
import Pagination from "../../../../common/components/pagination.jsx";

import RecruiterInfo from "../../../../common/components/not-editable/recruiterSingle/recruiterInfo.jsx";
import RecruiterHires from "../../../../common/components/not-editable/recruiterSingle/recruiterHires.jsx";
import RecruiterTokens from "../../../../common/components/not-editable/recruiterSingle/recruiterTokens.jsx";

import { TableControls } from "../../../../common/components/not-editable/tableControls.jsx";
import  RecruiterTableRow from "../../../../common/components/not-editable/recruiterSingle/recruiterTableRow.jsx";
import CountOfTotal from '../../../../common/components/countOfTotal.jsx';
import TokensCount from "../../../../common/components/tokensCount.jsx";

// Filters
import LimitFilter from "../../../../../../components/filters/publicFilters/limit.jsx";
import HireDatesFilter from "../../../../../../components/filters/publicFilters/hireDatesFilter.jsx";

import {
    normalizeLimit,
    convertLimitToValue
} from '../../../../../../helpers/filters/limit.helper.js';

import {
    convertOffsetToValue
} from '../../../../../../helpers/filters/offset.helper.js';

import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../../helpers/filters/orderBy.helper.js';

import {
    changeOneOf,
    deteleOneOf
} from "../../../../../../helpers/filters/filter.helper.js";
import {
    _getAggregatedHiringData
} from './../../../../../../helpers/aggregateData.helper.js';

export default class RecruiterSingle extends React.Component {

    changeLimit(label){

        const { companyProfile: { filters, companyId }, actions: {getHireHistory} } = this.props,
                newFiltersWithLimit = normalizeLimit(filters, label);

        getHireHistory(companyId, newFiltersWithLimit);
    }

    onChangePeriod(filters){
        const {
            actions: { getHireHistory, getStatisticRecruter},
            companyProfile: {companyId},
            match: { params: { id } }
        } = this.props;

        let setBuyerToFilters = changeOneOf(filters, {'buyerUid': id}),
            deletOffsetToFilters = deteleOneOf(filters,'offset');
        getStatisticRecruter(companyId,deletOffsetToFilters)
        getHireHistory(companyId, deletOffsetToFilters);

    }
    onChangePagination(filters){
        const {
            actions: { getHireHistory },
            companyProfile: {companyId},
            match: { params: { id } }
        } = this.props;

        let setBuyerToFilters = changeOneOf(filters, {'buyerUid': id})
        getHireHistory(companyId, setBuyerToFilters);
    }

    updateHiringStatus(data){

      const  {
            match:{
                params:{
                    id
                }
            },
            location:{
                search
            },
            actions:{
                updateHiringStatus
            }
        } = this.props;
        
        let setBuyerToFilters = changeOneOf(search.slice(1), {'buyerUid': id})

        updateHiringStatus(data, setBuyerToFilters);

    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { companyProfile: { filters, companyId }, actions: {getHireHistory} } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getHireHistory(companyId, filtersNormalizedWithNewOrderBy);
    }

    render() {
        let {
                i18n:{
                    translation = {}
                } = {},
                companyProfile: {
                    statisticRecruter = {},
                    balance:{
                        tokensCount = 0
                    } = {},
                    applicantsList:{
                        items = [],
                        totalCount = 0
                    } = {},
                    employee = {},
                    filters
                },
                actions,
                location:{
                    search
                },
                match: { params: { id } }
            } = this.props,
            limitToValue = convertLimitToValue(filters),
            justOrderBy = orderBy(filters),
            offsetTovalue =  convertOffsetToValue(filters),
            statistic = _getAggregatedHiringData(statisticRecruter[id]); 

        return (
            <div>
                <div className="row">
                    <div className="col-md-6 col-lg-7 col-xl-8 margin--b-20 margin--md-b-0">
                        <Link to="/recruters" className="link fl--inline fl--align-c font--12 font--uppercase font--color-blue">
                            <span className="icon icon--carret_down font--8 rotate rotate--90 margin--r-10"></span>
                            {translation.backToRecruiters}
                        </Link>
                    </div>
                    <div className="col-md-6 col-lg-5 col-xl-4 fl fl--justify-end">
                        <TokensCount tokensCount={tokensCount} />
                    </div>
                </div>  

                <div className="box--white box--rounded  margin--t-30">
                    <div className="row no-gutters hr--bottom-theme-light-gray">
                        <div className="col-lg-5">
                            <div className="padding--30">
                                <div className="fl fl--dir-col fl--dir-md-row fl--align-c fl--align-md-stretch fl--justify-md-c">
                                    <RecruiterInfo
                                        data={employee}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <div className="row no-gutters">
                                <div className="col-md-6 hr--lg-left-theme-light-gray hr--top-theme-light-gray hr--lg-top-none">
                                    <RecruiterHires
                                        data={statistic}
                                    />
                                </div>
                                <div className="col-md-6 hr--lg-left-theme-light-gray hr--top-theme-light-gray hr--lg-top-none">
                                    <RecruiterTokens
                                        data={statistic}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="padding--xl-xc-30 bg--light-gray padding--xc-15 padding--yc-20 padding--xl-yc-25 fl fl--justify-b fl--align-c fl--wrap">
                        <div className="fl fl--align-c fl--wrap">
                            <LimitFilter
                                className="padding--xc-5"
                                onChange={::this.changeLimit}
                                value={limitToValue}
                            />
                            <div className="font--color-secondary font--14 margin--t-10 margin--md-t-0">
                                {`${translation.hiringHistory} ${translation.for} `}
                                <HireDatesFilter
                                     onChange={::this.onChangePeriod}
                                     filters={filters}
                                />
                            </div>
                        </div>
                        {/*
                         TODO: no mvp
                        <TableControls /> */}
                    </div>
                    <table className="respons-flex-table">
                        <thead className="respons-flex-table__thead">
                            <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-14
                                            font--left
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="openDate"
                                        // data-value={justOrderBy.openDate || ""}
                                        // className={`${orderingByClass(justOrderBy.openDate)}`}
                                    >
                                    {translation.openingDate}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-14
                                            font--left
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="applicant"
                                        // data-value={justOrderBy.applicant || ""}
                                        // className={`${orderingByClass(justOrderBy.applicant)}`}
                                    >
                                        {`${translation.applicant},${translation.id || ''}`}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-17
                                            font--left
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="position"
                                        // data-value={justOrderBy.position || ""}
                                        // className={`${orderingByClass(justOrderBy.position)}`}
                                    >
                                        {translation.workPosition}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-25
                                            font--left
                                        "
                                >
                                        <span>{translation.contacts}</span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-10
                                            respons-flex-table--xl-j-center
                                            font--left
                                            font--xl-center 
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="price"
                                        // data-value={justOrderBy.price || ""}
                                        // className={`${orderingByClass(justOrderBy.price)} respons-flex-table__sort-des`}
                                    >
                                        {translation.bidsPerContact}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-15
                                            font--left
                                            font--xl-center
                                        "
                                >
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="status"
                                        // data-value={justOrderBy.status || ""}
                                        // className={`${orderingByClass(justOrderBy.status)}`}
                                    >
                                        {translation.condition}
                                    </span>
                                </th>
                                <th
                                    className="
                                            respons-flex-table__th
                                            respons-flex-table--xl-w-5
                                            font--left
                                        "
                                >
                                    <span className="">{translation.myCV}</span>
                                    
                                </th>
                            </tr>
                        </thead>
                        <tbody className="respons-flex-table__tbody">
                            {
                                items.length && items.map((el, i) => (
                                    <RecruiterTableRow key={i} data={el} search={search} id={id} hireHistory={false} updateHiringStatus={::this.updateHiringStatus}/>
                                )) || <tr className="respons-flex-table__tr"><td className="respons-flex-table__td respons-flex-table__td--no-before ">{translation.infoStatistics}</td></tr> 
                            }
                        </tbody>
                        <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-20  padding--yc-20 fl--wrap">
                            <tr className="d-block width--100">
                                <td className="fl fl--align-c fl--justify-b fl--wrap">
                                    <CountOfTotal limit={limitToValue} total={totalCount} offset={offsetTovalue} />
                                    <div className="fl fl--justify-end">
                                        {
                                            (totalCount > limitToValue) && <Pagination
                                                count={totalCount}
                                                actions={::this.onChangePagination}
                                                filters={filters}
                                                wrapperClass="pagination margin--yc-10"
                                            /> || null
                                        }
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        );
    }
}
