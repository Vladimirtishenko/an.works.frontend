import React from 'react'
import {Link} from 'react-router-dom'
import recruterList from '../../../../../../databases/general/mainPositionRecruter.json';
 
import Avatar from '../../../../common/components/not-editable/avatar.jsx'
import {
    dmyDate
    } from '../../../../../../helpers/date.helper.js'; 

export default class RecruitersMainBlock extends React.Component {

    constructor(props){
        super(props);
        
    }

    checkPositionRecruter(value){
        let position = '';
        recruterList.forEach((item, i) =>{
            if(item.label == value){
                position = item.name;
            }
         }
        );
        return position;
    }

    setInactive(){
        const { actions: { blockEmployee }, item: { companyId, uid } } = this.props;
        blockEmployee({uid: uid}, companyId);
    }
    setActive(){
        const { actions: { unBlockEmployee }, item: { companyId, uid } } = this.props;
        unBlockEmployee({uid: uid}, companyId);
    }
    render(){

        const {
                item: {
                    profile: {
                        firstName,
                        lastName,
                        positionTitle
                    },
                    email,
                    hiringData,
                    hiringData:{
                        hired,
                        waitingHiring,
                        totalContacts,
                        spendTokensCount 
                    } = {},
                    uid,
                    createdDate = "",
                    deactivatedDate = "",
                    role
                },
                deactivated
            } = this.props,
            position = this.checkPositionRecruter(positionTitle),
            name = firstName && lastName ? `${firstName} ${lastName}` : email,
            roleClass = (role == 'company_admin') ? 'main' : 'secondary';
       
        if(deactivated){
            return (
                <tr className={`respons-flex-table__tr word-break--word respons-flex-table__tr--${roleClass}`}>
                    <td className="
                        respons-flex-table__td
                        respons-flex-table__td--no-before
                        respons-flex-table--w-85
                        respons-flex-table--xl-w-30
                        word-break--word
                        order-md-1
                    ">
                        <Link to={`/recruters/${uid}`} className="fl fl--align-c">
                            <div className="box--img-thumbnail margin--r-15">
                                <Avatar
                                    uid={uid}
                                    alt="hr"
                                    className="img-fluid"
                                />
                            </div>
                            <div>
                                <div className="font--14 font--color-primary margin--b-5 word-break--word">
                                    {name}
                                </div>
                                <div className="font--12 font--color-inactive">{position}</div>
                                <div className="font--12 font--color-secondary">HR</div>
                            </div>
                        </Link>
                    </td>
                    <td
                        data-title="Дата регистрации"
                        className="
                            respons-flex-table__td
                            respons-flex-table--w-50
                            respons-flex-table--xl-w-25
                            font--xl-center
                            order-md-2
                            order-xl-1
                        "
                    >
                        <span className="font--14 font--500 font--color-primary">
                           {dmyDate(createdDate)}
                        </span>
                    </td>
                    <td data-title="Дата деактивации"
                        className="
                            respons-flex-table__td
                            respons-flex-table--w-50
                            respons-flex-table--xl-w-30
                            font--xl-center
                            order-md-2
                            order-xl-1
                        "
                    >
                        <span className="font--14 font--500 font--color-primary">
                            {dmyDate(deactivatedDate)}
                        </span>
                    </td>
                    {/* <td data-title="Нанято"
                        className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-7
                        font--xl-center"
                    >
                        <span className="font--14 font--500 font--color-primary">
                            {hired}
                        </span>
                    </td>
                    <td data-title="Куплено контактов"
                        className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-14
                        font--xl-center"
                    >
                        <span className="font--14 font--500 font--color-primary">
                            {totalContacts}
                        </span>
                    </td>
                    <td data-title="Потрачено токенов"
                        className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-15
                        font--xl-center"
                    >
                        <span className="font--14 font--500 font--color-primary">
                            {spendTokensCount}
                        </span>
                    </td> */}
                    <td data-title=""
                        className="
                            respons-flex-table__td
                            respons-flex-table--md-j-center
                            respons-flex-table__td--no-before
                            respons-flex-table--w-50
                            respons-flex-table--md-w-15
                            respons-flex-table--font-10
                            font--color-secondary
                            font--md-right
                            order-sm-1
                        "
                    >
                        <span onClick={::this.setActive} className="font--color-primary pointer-text font--12 font--underlined">
                            Восстановить
                        </span>
                    </td>
            </tr>
        )
        }else{
            return (
                <tr className={`respons-flex-table__tr respons-flex-table__tr--${roleClass}`}>
                    <td className="
                        respons-flex-table__td
                        respons-flex-table__td--no-before
                        respons-flex-table--w-90
                        respons-flex-table--xl-w-25
                        fl--order-1
    
                    ">
                        <Link to={`/recruters/${uid}`} className="fl fl--align-c">
                            <div className="box--img-thumbnail margin--r-15">
                                <Avatar
                                    uid={uid}
                                    alt="hr"
                                    className="img-fluid"
                                />
                            </div>
                            <div>
                                <div className="font--14 font--color-primary margin--b-5 word-break--word">
                                    {name}
                                </div>
                                <div className="font--12 font--color-inactive">{position}</div>
                                <div className="font--12 font--color-secondary">HR</div>
                            </div>
                        </Link>
                    </td>
                    <td data-title="Нанято" className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-18
                        font--xl-center
                        fl--order-3">
                        <span className="font--14 font--color-primary">
                            {hired}
                        </span>
                    </td>
                    <td data-title="Активные интервью" className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-18
                        font--xl-center
                        fl--order-3
                    ">
                        <span className="font--14 font--color-primary">
                            {waitingHiring}
                        </span>
                    </td>
                    <td data-title="Куплено контактов" className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-18
                        font--xl-center
                        fl--order-3
                    ">
                        <span className="font--14 font--color-primary">
                            {totalContacts}
                        </span>
                    </td>
                    <td data-title="Потрачено токенов" className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--xl-w-18
                        font--xl-center
                        fl--order-3
                    ">
                        <span className="font--14 font--color-primary">
                            {spendTokensCount}
                        </span>
                    </td>
                   {/* <td data-title="Доступно токенов" className="respons-flex-table__td">
                         <span className="font--14 font--color-primary">
                           remove this column
                        </span>
                    </td>
                    <td data-title="Лимит в месяц" className="respons-flex-table__td">
                        <span className="font--14 font--color-primary">
                        <div className="input-spin">
                            <button className="input-spin__button"></button>
                            <input className="input-spin__input" min="0" name="quantity" defaultValue="10" type="number"/>
                            <button className="input-spin__button input-spin__button--plus"></button>
                        </div>
                        </span>
                    </td> */}
                    {
                        roleClass != 'main' && (
                            <td className="
                                respons-flex-table__td
                                respons-flex-table--md-j-center
                                respons-flex-table__td--no-before
                                respons-flex-table--w-10
                                respons-flex-table--xl-w-3
                                fl--order-2
                                fl--order-xl-4
                                respons-flex-table--font-10
                                font--color-secondary
                                font--right
                            ">
                                <span onClick={::this.setInactive} className="icon icon--delete link link--grey-h-b font--16" />
                            </td>
                        ) 
                    }
                    
                </tr>
            )
        }
    }
}
