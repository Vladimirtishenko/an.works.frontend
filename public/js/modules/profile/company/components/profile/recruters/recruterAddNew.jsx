import React from "react";
import Modal from "../../../../../../components/widgets/modal.jsx";
import Input from "../../../../../../components/form/text.jsx";

import Notification from "../../../../../../libraries/notification/index.jsx";
import serialize from "../../../../../..helpers/serialize.helper.js";

export default class RecruitersAddNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            append: false
        };
    }

    closeAppendModal() {
        this.setState({
            append: false
        });
    }

    openWindowForNewEmployee(event) {
        if (!event && !event.target) return;

        this.setState({
            append: true
        });
    }

    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            {
                companyProfile: { company }
            } = this.props;

        this.props.actions.inviteNewEmployee(
            serializedForm.email,
            company.name
        );

        this.setState({
            append: false
        });
    }

    render() {
        let { append } = this.state,
        {i18n:{translation = {}} = {}} = this.props;

        return (
            <React.Fragment>
                <Notification
                    onError={{ component: null }}
                    onSuccess={{ component: null }}
                />
                {(append && (
                    <Modal onCancel={::this.closeAppendModal}>
                        <form className="form" onSubmit={::this.submit}>
                            <Input
                                wrapperClass="row fl--align-c no-gutters"
                                name="email"
                                label="Email нового рекрутера:"
                                labelClass="label form__label font--14"
                                inputClass="input form__input"
                            />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </form>
                    </Modal>
                )) ||
                    null}
            </React.Fragment>
        );
    }
}
