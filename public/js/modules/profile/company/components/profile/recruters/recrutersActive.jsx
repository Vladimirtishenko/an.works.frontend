import React from 'react'

// Compoennts
import InviteEmail from "../../../../applicant/components/modals/inviteEmail.jsx";
import RecruterRow from "./recruterRow.jsx";

// Filters
import HireDatesFilter from "../../../../../../components/filters/publicFilters/hireDatesFilter.jsx";

// Helpers
import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../../helpers/filters/orderBy.helper.js'

import {
    _getAggregatedHiringData,
    getEmptyAggregatedHiringData
} from './../../../../../../helpers/aggregateData.helper.js';

export default class RecruiterActive extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isNewRecruitePopupOpen: false,
            statistic:{}
        }
    }
    componentDidUpdate(prevProps, prevState) {
        const  comparing = _.isEqual(prevProps ,this.props);

        if(!comparing){
            this.setState(
                {
                    ...this.state,
                    statistic: this.getQuantityRecruiter()
                }
            )
        }
    }

    handleChangeRecruiterPopupStatus(status) {
        this.setState({
            ...this.state,
            isNewRecruitePopupOpen: status
        });
    }

    componentDidMount(){
        this.setState({
            ...this.state,
            statistic: this.getQuantityRecruiter()
        });
    }

    onChangePeriod(filters){
        const { actions: { getStatisticRecruter }, companyProfile: {companyId} } = this.props;
        getStatisticRecruter(companyId, filters)
    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { companyProfile: { filters, companyId }, actions: {getCompanyEmployees} } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getCompanyEmployees(companyId, filtersNormalizedWithNewOrderBy);
    }

    _getRecruiterHiring(uid){
        const {
            companyProfile:{
                statisticRecruter = ''
            } = {}
        } = this.props;

        return  _getAggregatedHiringData(statisticRecruter[uid])
    }

    getQuantityRecruiter(){
        const { 
            companyProfile:{
                employeeList = [],
                statisticRecruter = ''
            },
        } = this.props;
        let statistic = {
            recruter: 0,
            waiting: 0
        };
        employeeList.forEach(item => {
            if(item.profile && item.profile.status !== 'deactivated'){
                statistic.recruter++;
                statistic.waiting += statisticRecruter[item.uid] && statisticRecruter[item.uid].waiting.countContacts || 0;
            }

        });
        return statistic;
    }

    render(){
        const { isNewRecruitePopupOpen, statistic } = this.state,
              { i18n:{translation = {}} = {}, companyProfile: { employeeList = [], companyId, filters }, actions } = this.props,
              justOrderBy = orderBy(filters);
        return (
            <React.Fragment>
                <div className="box--light-grey relative z--5">
                    <div className="padding--15 padding--md-30">
                        <div className="fl fl--justify-md-b fl--dir-col fl--dir-md-row">
                            <div className="font--14 font--color-secondary margin--b-10 margin--md-0">
                                {translation.recruitersList + ' '}
                                <HireDatesFilter
                                     onChange={::this.onChangePeriod}
                                     filters={filters}
                                     createdDate={true}
                                />
                            </div>
                            <span
                                onClick={() =>
                                    this.handleChangeRecruiterPopupStatus(
                                        true
                                    )
                                }
                                className="font--14 link"
                            >
                                <span className="icon icon--add" />
                                    {translation.addRecruiter}
                            </span>
                        </div>
                    </div>
                </div>
                <table className="respons-flex-table">
                    <thead className="respons-flex-table__thead">
                        <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-25

                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="employee"
                                    // data-value={justOrderBy.employee || ""}
                                    // className={`${orderingByClass(justOrderBy.employee)}`}
                                >
                                    {translation.recruiter}
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-50
                                respons-flex-table--xl-w-18
                                font--xl-center

                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="hired"
                                    // data-value={justOrderBy.hired || ""}
                                    // className={`${orderingByClass(justOrderBy.hired)}`}
                                >
                                    {translation.hireds}
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-50
                                respons-flex-table--xl-w-18
                                font--xl-center

                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="activeInterview"
                                    // data-value={justOrderBy.activeInterview || ""}
                                    // className={`${orderingByClass(justOrderBy.activeInterview)}`}
                                >
                                    {translation.progressInterviews}
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-50
                                respons-flex-table--xl-w-18
                                font--xl-center

                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="bought"
                                    // data-value={justOrderBy.bought || ""}
                                    // className={`${orderingByClass(justOrderBy.bought)}`}
                                >
                                    {translation.contactsPurchased}
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-50
                                respons-flex-table--xl-w-18
                                font--xl-center

                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="spend"
                                    // data-value={justOrderBy.spend || ""}
                                    // className={`${orderingByClass(justOrderBy.spend)}`}
                                >
                                    {translation.bidsSpent}
                                </span>
                            </td>
                            <td className="flexy-table__td">
                                <span className="font--12 font--color-secondary" />
                            </td>
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                        {
                            (employeeList.length &&
                                employeeList.map((item, i) => {
                                    const { profile: { status } } = item;
                                    item.hiringData = this._getRecruiterHiring(item.uid);

                                    if(status !== 'deactivated') {
                                        return (
                                            <RecruterRow
                                                item={item}
                                                actions={actions}
                                                key={i}
                                            />
                                        )
                                    }
                                    return null;
                                })
                            ) || null
                        }
                    </tbody>
                    <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-20  padding--yc-15 fl--wrap">
                        <tr className="width--100 fl fl--wrap">
                            <td className="fl fl--justify-b fl--align-c fl--wrap padding--r-20">
                                <span className="font--14 font--color-inactive">
                                    {`${translation.recruiter} ${statistic.recruter}`}
                                </span>
                            </td>
                            <td className="fl fl--justify-b fl--align-c fl--wrap">
                                <span className="font--14 font--color-inactive">
                                    {`${translation.progressInterviews} ${statistic.waiting}`}
                                </span>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {isNewRecruitePopupOpen && (
                    <InviteEmail
                        {...this.props}
                        closePopup={() =>
                            this.handleChangeRecruiterPopupStatus(false)
                        }
                    />
                )}
            </React.Fragment>
        )
    }
}
