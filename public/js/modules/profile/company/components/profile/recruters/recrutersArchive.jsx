import React from 'react'

// Componennts
import RecruterRow from "./recruterRow.jsx";
// Helpers
import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../../helpers/filters/orderBy.helper.js'

import Avatar from '../../../../common/components/not-editable/avatar.jsx'
import {
    _getAggregatedHiringData,
    getEmptyAggregatedHiringData
} from './../../../../../../helpers/aggregateData.helper.js';
export default class ArchiveRecruters extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            aggregatedHiringList: []
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { companyProfile: { applicantsList: { items: currentItems = [] } = {} }} = this.props,
              { companyProfile: { applicantsList: { items: prevItems = [] } = {} }} = prevProps,
              comparing = _(currentItems).differenceWith(prevItems, _.isEqual).isEmpty();

        if(!comparing)
        {
            this.setState(prevState => (
                {
                    ...prevState
                }
            ))
        }
    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { companyProfile: { filters, companyId }, actions: {getCompanyEmployees} } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getCompanyEmployees(companyId, filtersNormalizedWithNewOrderBy);
    }

    setActive(){
        const { actions: { unBlockEmployee }, item: { companyId, uid } } = this.props;
        unBlockEmployee({uid: uid}, companyId);
    }

    _getRecruiterHiring(uid){
        const {
            companyProfile:{
                statisticRecruter = ''
            } = {}
        } = this.props;

        return  _getAggregatedHiringData(statisticRecruter[uid])
    }

    render(){
        const { i18n:{translation = {}} = {},companyProfile: { employeeList = [], companyId, filters },actions } = this.props,
                justOrderBy = orderBy(filters);
        return (
            <React.Fragment>
                <div className="box--light-grey">
                    <div className="padding--15 padding--md-30">
                        <div className="font--14 font--color-secondary">
                            {translation.infoArchiveRecruiters}
                        </div>
                    </div>
                </div>
                <table className="respons-flex-table">
                    <thead className="respons-flex-table__thead">
                        <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-30

                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="employee"
                                    // data-value={justOrderBy.employee || ""}
                                    // className={`${orderingByClass(justOrderBy.employee)}`}
                                >
                                    {translation.recruiter}
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-25
                                font--center
                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="registrationDate"
                                    // data-value={justOrderBy.registrationDate || ""}
                                    // className={`${orderingByClass(justOrderBy.registrationDate)} respons-flex-table__sort-des`}
                                >
                                    {translation.registeredSince}
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-30
                                font--center
                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="deactivationDate"
                                    // data-value={justOrderBy.deactivationDate || ""}
                                    // className={`${orderingByClass(justOrderBy.deactivationDate)} respons-flex-table__sort-des`}
                                >
                                  {translation.deactivationDate}
                                </span>
                            </td>
                            {/* <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-7
                                font--center
                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="hired"
                                    // data-value={justOrderBy.hired || ""}
                                    // className={`${orderingByClass(justOrderBy.hired)} respons-flex-table__sort-des`}
                                >
                                    Нанято
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-14
                                font--center
                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="bought"
                                    // data-value={justOrderBy.bought || ""}
                                    // className={`${orderingByClass(justOrderBy.bought)} respons-flex-table__sort-des`}
                                >
                                    Куплено контактов
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-15
                                font--center
                            ">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="spend"
                                    // data-value={justOrderBy.spend || ""}
                                    // className={`${orderingByClass(justOrderBy.spend)} respons-flex-table__sort-des`}
                                >
                                    Потрачено токенов
                                </span>
                            </td>
                            <td className="
                                respons-flex-table__th
                                respons-flex-table--w-90
                                respons-flex-table--xl-w-10

                            " /> */}
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                        {
                                (employeeList.length &&
                                    employeeList.map((item, i) => {
                                        const { profile: { status } } = item;

                                        item.hiringData = this._getRecruiterHiring(item.uid);

                                        if(status == 'deactivated') {
                                            return (
                                                <RecruterRow
                                                    item={item}
                                                    actions={actions}
                                                    key={i}
                                                    deactivated={true}
                                                />
                                            )
                                        }
                                        return null;
                                    })
                                ) || null
                            }
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}
