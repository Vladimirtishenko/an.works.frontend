import React from "react";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import Combobox from "../../../../../../components/form/combobox.jsx";

// Components
import RecrutersArchive from "./recrutersArchive.jsx";
import RecrutersActive from "./recrutersActive.jsx";

// Modals
import TestsAdd from "../../../../applicant/components/modals/testsAdd.jsx";

export default class RecruiterList extends React.Component {
    constructor(props){
        super(props);
        this.tabs = {}
    }

    onSelect(curent, prev) {
        if(curent == prev) return;
        const { companyProfile: { filters, companyId }, actions: {getCompanyEmployees} } = this.props;

        if(filters) {
            this.tabs[prev] = filters;
        }

        if(this.tabs[curent]){
            getCompanyEmployees(companyId, this.tabs[curent]);
        } else {
            getCompanyEmployees(companyId, '');
        }

    }

    render() {
        const {i18n:{translation = {}} = {}} = this.props; 
        return (
            <React.Fragment>
                 <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">
                    {translation.recruiters}
                </h2>
                <Tabs onSelect={::this.onSelect} className="box--white">
                    <TabList className="tablist tablist--two-tabs shadow--small fl fl--nowrap">
                        <Tab>{translation.activeRecruiters}</Tab>
                        <Tab>{translation.archive}</Tab>
                    </TabList>

                    <TabPanel>
                        <RecrutersActive {...this.props} /> 
                    </TabPanel>
                    <TabPanel>
                        <RecrutersArchive {...this.props} />
                    </TabPanel>
                </Tabs>
            </React.Fragment>
        );
    }
}
