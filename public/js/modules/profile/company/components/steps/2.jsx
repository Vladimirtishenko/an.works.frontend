import React from 'react'

// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import DateCustom from '../../../../../components/form/dateCustom.jsx'
import StaticItem from '../../../../../components/form/staticItem.jsx';
import Radio from '../../../../../components/form/radio.jsx';

// Componets
import {Card} from '../../../common/components/card.dumb.jsx'
import ContactCard from '../../../common/components/editable/contactCard.jsx'
import EmployeeInformation from '../editable/employeeInformation.jsx'

// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

import i18nMonthDecorator from '../../../../i18n/decorators/month.dec.jsx'

// Inheritance
import Inheritance from '../../../common/components/parent/main.jsx'
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
@i18nMonthDecorator
export class Second extends Inheritance {

    constructor(props){
        super(props);
    }

    componentDidMount() {
        if (
            this.props.companyProfile &&
            !this.props.companyProfile.telegramRegistrationUrl
        ) {
            this.props.actions.isTelegramVerified();
            this.props.actions.getTelegramRegistrationUrl();
        }
    }
    
    submit(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            { companyProfile: { direction, company, companyId }, oauth: { user: { email } } } = this.props,
            date = serializedForm.profile.birthday &&
                    Array.isArray(serializedForm.profile.birthday) ?
                    serializedForm.profile.birthday.reverse() : '',
            birthday = date ? (new Date( date.join('/') )).valueOf() : '';

        serializedForm.profile.birthday = birthday;

        let {profile, contactInfo} = serializedForm || {},
            data = {...profile, contactInfo: contactInfo};

        this.props.actions.updateEmployeeProfile(data, companyId, email, direction);

    }

    render () {

        let {translation} = this.props.i18n || {},
            {
                actions,
                profile,
                telegramRegistrationUrl,
                telegramVerification,
                telegramVerificationFailed
            } = this.props.companyProfile || {};
        return (

            <ValidatorForm onSubmit={::this.submit} className="form">
                <h2 className="font--18 font--color-primary font--500 margin--b-27">Ведущий рекрутер</h2>

                <EmployeeInformation profile={profile} translation={translation} />

                <ContactCard
                    actions={actions}
                    contactInfo={profile && profile.contactInfo || {}}
                    inputPhoneSize="col-md-5 col-lg-4"
                    inputSizeSocial="col-md-5 col-lg-4"
                    positionBtnRemove="margin--md-r-25-p absolute--lg-r-33-p"
                    telegramVerification={telegramVerification}
                    telegramRegistrationUrl={ 
                        telegramRegistrationUrl && telegramRegistrationUrl.url
                    }
                    telegramVerificationFailed={telegramVerificationFailed}
                />

                <div className="fl fl--justify-c fl--align-c">
                    <span className="link link--grey font--14 margin--r-30" onClick={::this.prevStep}>{translation.prev_step}</span>
                    <button className="padding--yc-10 btn btn--primary font--14">{translation.send_data}</button>
                </div>
            </ValidatorForm>
        )
    }
}

export default Second;
