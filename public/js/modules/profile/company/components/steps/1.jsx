import React from 'react'

// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

import InfoEditable from '../editable/info.jsx'

import i18nMonthDecorator from '../../../../i18n/decorators/month.dec.jsx'
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
@i18nMonthDecorator
export default class First extends React.Component {

    constructor(props){
        super(props);
    }

    submit(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            {companyProfile} = this.props || null,
            {direction} = this.props.companyProfile || {},
            date = serializedForm.companyInfo.birthday && Array.isArray(serializedForm.companyInfo.birthday) ?
                    serializedForm.companyInfo.birthday.reverse() : '',
                    birthday = date ? (new Date( date.join('/') )).valueOf() : '';

        serializedForm.companyInfo.birthday = birthday;
        this.props.actions.updateCompany(serializedForm, companyProfile.companyId, direction);

    }

    render () {

        let {translation} = this.props.i18n;
        return (

             <ValidatorForm onSubmit={::this.submit} className="form">
                <h2 className="font--18 font--color-primary font--500 margin--b-27">Профиль работодателя</h2>

                <InfoEditable {...this.props} />

                <div className="fl fl--justify-c">
                    <button className="padding--yc-10 btn btn--primary font--14">{translation.next_step}</button>
                </div>
            </ValidatorForm>

        )
    }
}
