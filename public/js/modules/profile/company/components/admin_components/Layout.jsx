import React from 'react'
import Left from './LeftSideBar.jsx'

class Layout extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row fl--justify-c">
                </div>
                <div className="row">
                    <div className="col-md-2 padding--0">
                        <Left {...this.props}/>
                    </div>
                    <div className="col-md-8">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default Layout;
