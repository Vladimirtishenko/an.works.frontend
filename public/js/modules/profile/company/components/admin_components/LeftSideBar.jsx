import React from 'react'
import Avatar from '../../../common/components/editable/avatar.jsx'
import {
    Accordion,
    AccordionItem,
    AccordionItemTitle,
    AccordionItemBody,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
class LeftAside extends React.Component {

    goToWaitingList(){
        this.props.actions.goTo("employeeWaitingToApprove");
    }

    goToList(){
        this.props.actions.goTo("");
    }

    render() {
        return (
            <aside className="box--white box--rounded">
                <Avatar/>
                <div className="user-info">
                    <h3>{this.props.oauth.user.email}</h3>
                    <div>{this.props.oauth.user.email}</div>
                </div>
                <Accordion>
                    <AccordionItem>
                        <AccordionItemTitle onClick={::this.goToList}>
                            <h3>Company employ list</h3>
                        </AccordionItemTitle>
                    </AccordionItem>
                    <AccordionItem>
                        <AccordionItemTitle onClick={::this.goToWaitingList}>
                            <h3>Waiting to approveEmployees</h3>
                        </AccordionItemTitle>
                    </AccordionItem>
                </Accordion>
            </aside>
        )
    }
}

export default LeftAside;
