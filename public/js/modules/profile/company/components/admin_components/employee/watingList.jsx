import React from 'react'
import Table from '../../../../../../components/tables/main.jsx'
// Form Componets
import Input from '../../../../../../components/form/text.jsx';
//TODO::create schema
class WaitingList extends React.Component {

    componentDidMount(){
        this.props.actions.getCompanyRegistrationEmployeeWaitingToApprove(this.props.companyProfile.company.name);
    }

    addNewRecruiter()
    {
        let elementVal = document.getElementsByName("recruiterEmail")[0].value || '';
        //email
        this.props.actions.inviteNewEmployee(elementVal, this.props.companyProfile.company.name);
    }

	render(){

        let schema  = {
            companies: {
                fields: [
                    {
                        name: 'email',
                        type: 'String'
                    }
                ],
                entityIdKey: 'email',
                actions:[]
            }
        };

        let {companies} = schema,
            {i18n} = this.props;

		return (
			<div className="col-12">
				<Table i18n={i18n} data={this.props.companyProfile.employeeWatingList || []} schema={companies} actions={[]}/>
                <Input wrapperClass="row fl--align-c no-gutters margin--b-20"
                       labelClass="label form__label font--14 col-md-3"
                       inputWrapperClass="col-md-4"
                       inputClass="input form__input"
                       label="Email"
                       name="recruiterEmail"/>

                <div className="row no-gutters margin--b-20">
                    <div className="offset-md-3 col-md-9">
                            <span className="padding--r-20 link" onClick={::this.addNewRecruiter}>
                                <span className="icon icon--add"></span>
                                Add new recruiter
                            </span>
                    </div>
                </div>
			</div>
		)
	}
}

export default WaitingList;