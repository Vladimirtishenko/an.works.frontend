import React from 'react'
import Table from '../../../../../../components/tables/main.jsx'
// Form Componets
import Input from '../../../../../../components/form/text.jsx';
//TODO::create schema
class EmployeeList extends React.Component {

    componentDidMount(){
        this.props.actions.getCompanyEmployees(this.props.companyProfile.company.name);
    }

	render(){

        let schema  = {
            companies: {
                fields: [
                    {
                        name: 'email',
                        type: 'String'
                    },
                    {
                        name: 'profile.role',
                        type: 'String'
                    }
                ],
                entityIdKey: 'email',
                actions:[]
            }
        };

        let {companies} = schema,
            {i18n} = this.props;

		return (
			<div className="col-12">
				<Table i18n={i18n} data={this.props.companyProfile.employeeList || []} schema={companies} actions={[]}/>
			</div>
		)
	}
}

export default EmployeeList;