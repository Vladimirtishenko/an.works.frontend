import React from 'react'
import WaitingList from "./employee/watingList.jsx"
import EmployeeList from "./employee/employeeList.jsx"
// Components
import Layout from './Layout.jsx'

class ProfileCtrl extends React.Component {
    render() {

        if(this.props.companyProfile.company.status == "waitingApprove")
        {
            return <div>Waiting to approve</div>
        }
        switch (this.props.companyProfile.direction)
        {
            case 'employeeWaitingToApprove':
                return <Layout {...this.props}>
                    <WaitingList {...this.props} />
                </Layout>
            default:
                return <Layout {...this.props}>
                    <EmployeeList {...this.props} />
                </Layout>
        }
    }
}

export default ProfileCtrl;