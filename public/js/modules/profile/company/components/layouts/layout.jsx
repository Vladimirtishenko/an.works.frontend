import React from "react";
import Left from "./leftAside.jsx";

class Layout extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-2  padding--0 position_static z--1001">
                        <Left sideBarClassName="margin--t-44" {...this.props} />
                    </div>
                    <div className="col-12 col-lg-10">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default Layout;
