import * as types from '../constants/profile.const.js';
import { SET_NOTIFICATION }  from '../../../../libraries/notification/constant/notification.const.js'
import API from '../../../../services/api.js'

export function getCompanyProfile(email) {

    return dispatch => {

        (async () => {

            let companyProfile = await API.__get('employee_company', dispatch, SET_NOTIFICATION);

            if (companyProfile) {
                companyProfile.step = _getStep(companyProfile.company || {});
                companyProfile.profile.email = email;

                dispatch({
                    type: types.COMPANY_PROFILE,
                    profile: companyProfile
                })
            }
        })();

    }
}

export function getWorkSkillsDictionary() {
    return dispatch => {
        (async () => {
            let knowledges = await API.__get('dictionary/workSkills/', dispatch, SET_NOTIFICATION)
            dispatch({
                type: types.KNOWLEDGES_LOADED,
                knowledges: knowledges
            })
        })();
    }
}

export function updateStep(step) {

    return dispatch => {

        dispatch({
            type: types.UPDATE_STEP_DIRECTION_FOR_COMPANY,
            direction: step
        })

    }

}


export function updateCompany(data, companyId, direction) {
    return dispatch => {
        (async () => {

            let companyProfile = await API.__post('PATCH', 'api/companies/' + companyId, data, dispatch, SET_NOTIFICATION);

            if (companyProfile) {

                let data = {
                    type: types.COMPANY_PROFILE_UPDATED_SUCCESS,
                    company: companyProfile,
                    step: _getStep(companyProfile)
                };

                if (direction) {
                    data.direction = ++direction;
                }

                dispatch(data)
            }

        })();

    }
}
export function updateEmployeeProfile(data, companyId, email, direction) {
    return dispatch => {
        (async () => {

            const employee = await API.__post(
                    'PATCH',
                    `api/companies/${companyId}/employee/${email}`,
                    { profile: data},
                    dispatch,
                    SET_NOTIFICATION
            ),
            companyProfile = employee && await API.__get(
                    'employee_company',
                    dispatch,
                    SET_NOTIFICATION
            ),
            { company } = companyProfile;



            if (companyProfile) {
                companyProfile.step = _getStep(company);

                dispatch({
                    type: types.COMPANY_PROFILE,
                    profile: companyProfile
                })
            }

        })();

    }
}

export function createCompanyEmployee(data, companyId, email) {
    return dispatch => {
        (async () => {
            //TODO:: map to props

            let employee = await API.__post('PATCH', 'api/companies/' + companyId + '/employee/' + email, data, dispatch, SET_NOTIFICATION);
            if (employee) {
                getCompanyProfile(email)(dispatch);
            }

        })();

    }
}

export function getQueue(name, filters) {
    return (dispatch, getState) => {
        (async () => {

            const { profile: { companyProfile: { filters: stateFilters, queue: { queueLabel: oldName } = {} } } } = getState();

            const flt = filters === true ? stateFilters : filters ? filters : '',
                  newName = name ? name : oldName,
                  stringFlt = flt && ('?' + flt) || '';

            dispatch({
                type: types.CANDIDATS_QUEUE_LOAD_SUCCESS,
                queue: {...queue, queueFetch: true},
                filters: flt
            });

            let queue = await API.__get('queue_filter/'+newName + stringFlt, dispatch, SET_NOTIFICATION);

            dispatch({
                type: types.CANDIDATS_QUEUE_LOAD_SUCCESS,
                queue: {...queue, queueFetch: false},
                filters: flt
            })

        })();

    }
}

export function buyApplicant(profileId, queueLabel, companyId) {

    return dispatch => {

        (async () => {

            let result = await API.__post('POST', 'api/buy_applicant', {
                profileId: profileId,
                queueLabel: queueLabel,
                companyId: companyId
            }, dispatch, SET_NOTIFICATION);

            if (result) {

                dispatch({
                    type: types.CANDIDAT_BUY_SUCCESS,
                    buyResult: result,
                    notification: {
                        type: 'success',
                        view: 'notice',
                        message: 'Сontact was purchased!'
                    }
                })
            }
        })();

    }
}

export function getHireHistory(companyId, filters = '') {

    return (dispatch, getState) => {
        (async () => {

            const { profile: {companyProfile: {filters: stateFilters = ''}} } = getState(),
                  flt = filters === true ? stateFilters : filters ? filters : '',
                  stringFilters = flt ? '?' + filters : '',
                  applicantsList = await API.__get('hiring_contacts/filter/company/' + companyId + stringFilters, dispatch, null);

            dispatch({
                type: types.APLICANT_LIST_LOAD_SUCCESS,
                applicantsList: applicantsList,
                filters: flt
            })

        })();

    }
}

export function getOpenContact(companyId, stringFilters = '') {

    return (dispatch, getState) => {
        (async () => {
            const { profile: {companyProfile: {filters} } }= getState();
            const applicantsList = await API.__get('hiring_contacts/filter/company/' + companyId +'?'+ stringFilters, dispatch, null);

            dispatch({
                type: types.APLICANT_LIST_LOAD_SUCCESS,
                applicantsList: applicantsList,
                filters: filters
            })

        })();

    }
}

export function getStatisticRecruter(companyId, stringFilters = '', noFilter) {

    return (dispatch,getState) => {
        const { profile: {companyProfile: {filters} } }= getState();
        (async () => {
            const statisticRecruter = await API.__get('view/company/'+ companyId+'/recruiters/statistics'+'?'+ stringFilters, dispatch, null);

            dispatch({
                type: types.RECRUTER_STATISTIC_LOAD_SUCCESS,
                statisticRecruter: statisticRecruter,
                filters: noFilter ? filters : stringFilters
            })

        })();

    }
}
export function getBalance(companyId, filters = '') {

    return (dispatch, getState) => {

        (async () => {
            const { profile: {companyProfile: {filters: stateFilters = ''}} } = getState(),
            flt = filters === true ? stateFilters : filters ? filters : '',
            stringFilters = flt ? '?' + filters : '';
            let balance = await API.__get('companies_balance/'+ companyId + stringFilters, dispatch, SET_NOTIFICATION)

            if(balance)
            {
                dispatch({ 
                    type: types.COMPANY_BALANCE_LOADED_SUCCESS,
                    balance: balance,
                    filters: flt
                })
            }
        })();

    }
}

export function sendReceivedData(data, companyId) {

    return dispatch => {

        (async () => {

            let response = await API.__post('POST', 'api/payments/fondy/receive', data);

            if(response)
            {
                getBalance(companyId)(dispatch);
                // dispatch({
                //     type: types.COMPANY_BALANCE_LOADED_SUCCESS,
                //     balance: balance
                // })
            }
        })();

    }
}

export function updateProfilePassword(data, uid) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__post(
                "PATCH",
                "api/users/" + uid,
                data,
                dispatch,
                SET_NOTIFICATION
            );

            if (userProfile) {
                dispatch({
                    type: SET_NOTIFICATION,
                    notification: {
                        type: 'success',
                        view: 'notice',
                        message: 'Password was updated!'
                    }
                });
            }
        })();
    };
}


export function updateHiringStatus(data, filters) {

    return (dispatch , getState) => {
        (async () => {

            let res = await API.__post('PATCH', 'api/hiring_contacts/company/', data, dispatch, SET_NOTIFICATION);
            if (res) {
                getHireHistory(data.companyId, filters)(dispatch,getState);
            }

        })();

    }
}


export function getEmployeeProfile(uid, companyId) {

    return dispatch => {

        (async () => {

            let employeeProfile = await API.__get('companies/' + companyId + '/employees/' + uid, dispatch, SET_NOTIFICATION);

            if (employeeProfile) {
                employeeProfile.step = _getStep(employeeProfile.profile || {});
                dispatch({
                    type: types.COMPANY_EMPLOYEE_LOADED_SUCCESS,
                    employee: employeeProfile
                })
            }
        })();

    }
}

export function getPaymentCheckoutData(data) {

    return dispatch => {
        (async () => {

            let checkoutData = await API.__post('POST', 'api/payments/fondy', data, dispatch, SET_NOTIFICATION);
            dispatch({
                type: types.GET_CHECKOUT_SUCCESS,
                checkoutData: checkoutData
            })

        })();

    }
}

export function removePaymentData() {

    return dispatch => {
        dispatch({
            type: types.GET_CHECKOUT_SUCCESS,
            checkoutData: null
        })

    }
}

export function getTokenPrice() {

    return dispatch => {

        (async () => {

            let tokenPrice = await API.__get('token_price_rate/', dispatch, SET_NOTIFICATION);

            if(tokenPrice)
            {
                dispatch({
                    type: types.TOKEN_PRICE_LOADED_SUCCESS,
                    tokenPrice: tokenPrice
                })
            }
        })();

    }
}

export function getTelegramRegistrationUrl() {
    return dispatch => {
        (async () => {

            let telegramRegistrationUrl = await API.__post('POST', 'api/telegram_registration/', {}, dispatch, SET_NOTIFICATION);

            if (telegramRegistrationUrl) {
                dispatch({
                    type: types.TELERGAM_BOT_URL_LOADED_SUCCESS,
                    telegramRegistrationUrl: telegramRegistrationUrl
                });
            }
        })();
    };
}

export function isTelegramVerified() {
    return dispatch => {
        (async () => {

            let telegramVerification = await API.__get('user_telegram/', dispatch, SET_NOTIFICATION);

            if (telegramVerification) {

                dispatch({
                    type: types.TELERGAM_VERIFICATION_LOADED,
                    telegramVerification: telegramVerification
                })
            }

        })();

    }
}


export function verificateTelegramToken(token, uid) {
    return dispatch => {
        (async () => {

            let telegramVerification = await API.__post('POST', 'api/telegram_verification', {
                token: token
            }, dispatch, SET_NOTIFICATION);

            if (telegramVerification) {

                dispatch({
                    type: types.TELERGAM_VERIFICATION_LOADED,
                    telegramVerification: telegramVerification
                })
            }

        })();

    }
}

export function blockEmployee(data, cid){
    return dispatch => {

        (async () => {

            let blocked = await API.__post('POST', `api/companies/${cid}/block_employee`, data, dispatch, SET_NOTIFICATION);

            if (blocked) {
                dispatch({
                    type: types.BLOCK_EMPLOYEE,
                    uid: data.uid,
                    status: 'deactivated'
                })
            }
        })();

    }
}

export function unBlockEmployee(data, cid){
    return dispatch => {

        (async () => {

            let blocked = await API.__post('POST', `api/companies/${cid}/unblock_employee`, data, dispatch, SET_NOTIFICATION);

            if (blocked) {
                dispatch({
                    type: types.UN_BLOCK_EMPLOYEE,
                    uid: data.uid,
                    status: 'active'
                    
                })
            }
        })();

    }
}
export function saveRating(value){

    return dispatch => {
        dispatch({
            type: types.SAVE_RATING,
            rating: value,
        })

    }

}

function _getStep(company = {}) {
    switch (company.status) {
        case 'pending':
            return 1;
            break;
        case 'adminInformationNeeded':
            return 2;
            break;
        default:
            return 0;
    }
}
