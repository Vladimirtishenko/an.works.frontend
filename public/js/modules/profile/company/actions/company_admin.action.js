import * as types from '../constants/profile.const.js';
import API from '../../../../services/api.js'
import registrationLink from '../../../../configuration/registration.json';

export function getCompanyEmployees(companyId, filters = '') {

    return (dispatch, getState) => {

        (async () => {

            const { profile: {companyProfile: {filters: stateFilters = ''}} } = getState(),
                  flt = filters === true ? stateFilters : filters ? filters : '',
                  listOfEmployees = await API.__get('companies/'+companyId+'/employees/', dispatch, types.COMPANY_ADMIN_EMPLOYEES_LIST_LOADED_FAILED);

            if(listOfEmployees)
            {
                dispatch({
                    type: types.COMPANY_ADMIN_EMPLOYEES_LIST_LOADED_SUCCESS,
                    employeeList: listOfEmployees,
                    filters: flt
                })
            }
        })();

    }
}


export function getCompanyRegistrationEmployeeWaitingToApprove(companyName) {

    return dispatch => {

        (async () => {

            let listOfEmployees = await API.__get('registration/company_employee_list/'+companyName, dispatch, types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_FAILED);

            if(listOfEmployees)
            {
                dispatch({
                    type: types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_SUCCESS,
                    employeeWatingList: listOfEmployees
                })
            }
        })();

    }
}

export function inviteNewEmployee(recrEmail, companyName) {

    return dispatch => {

        (async () => {

            let data  = { email: recrEmail, companyName: companyName, redirectUrl: registrationLink.recruiter},
                res = await API.__post('POST', 'api/registration/companyEmployee', data, dispatch, types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_FAILED),
                listOfEmployees = res && await API.__get('registration/company_employee_list/'+companyName, dispatch, types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_FAILED);

            if(listOfEmployees)
            {
                dispatch({
                    type: types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_SUCCESS,
                    employeeWatingList: listOfEmployees,
                    notification: {
                        type: 'success',
                        view: 'notice',
                        message: 'Recruter was added and waiting approval'
                    }
                })
            }
        })();

    }
}

//change direction state
export function goTo(direction) {

    return dispatch => {
        dispatch({
            type: types.COMPANY_ADMIN_DIRECTION_CHANGED,
            direction: direction
        })

    }
}
