
export const linksProfileSchema = (translation) => {

    return [
        {
            name: translation.personalInfo, 
            path: '/'
        },
        {
            name: translation.leadRecruiter, 
            path: '/employee'
        },
        {
            name: translation.payment,
            path: '/payments'
        },
        {
            name: translation.balanceDetails,
            path: '/payments-history'
        },
        {
            name: translation.ranking,
            path: '/rating'
        },
        {
            name: translation.recruiters,
            path: "/recruters"
        },
        {
            name: translation.hiringHistory, 
            path: "/hire-history"
        },
        {
            separate: true,
            name: translation.security,
            path: '/security'
        }
    ]

}
