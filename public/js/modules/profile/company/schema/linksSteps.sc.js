
export const linksStepsSchema = (translation) => {

    return [
        {
            step: 1,
            name: translation.personalInfo, 
            path: '/steps/1'
        },
        {
            step: 2,
            name: translation.leadRecruiter, 
            path: '/steps/2'
        },
        {
            name: translation.payment,
            hide: true
        },
        {
            name: translation.balanceDetails,
            hide: true
        },
        {
            name: translation.ranking,
            hide: true
        },
        {
            name: translation.recruiters,
            hide: true
        },
        {
            name: translation.hiringHistory, 
            hide: true
        },
        {
            separate: true,
            name: translation.security,
            path: '/security'
        }
    ]

}
