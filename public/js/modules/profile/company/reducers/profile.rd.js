import * as types from '../constants/profile.const.js';
import {CONTACT_PROFILE_APPLICANT}from '../../common/constants/contact.const.js';

const initialState = {};

export default function companyProfile(state = initialState, action) {

    switch (action.type) {

        case types.COMPANY_PROFILE:
            return {
                ...state,
                ...action.profile
            };

        case types.COMPANY_BALANCE_LOADED_SUCCESS:
            return {
                ...state,
                balance: action.balance,
                filters: action.filters
            };
        case types.COMPANY_EMPLOYEE_LOADED_SUCCESS:
            return {
                ...state,
                employee: action.employee
            };

        case types.COMPANY_PROFILE_UPDATED_SUCCESS:
            return {
                ...state,
                company: action.company,
                step: action.step,
                direction: action.direction
            };

        case types.COMPANY_ADMIN_DIRECTION_CHANGED:
            return {
                ...state,
                direction: action.direction
            };

        case types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_SUCCESS:
            return {
                ...state,
                employeeWatingList: action.employeeWatingList
            };

        case types.COMPANY_ADMIN_EMPLOYEES_LIST_LOADED_SUCCESS:
            return {
                ...state,
                employeeList: action.employeeList,
                filters: action.filters
            };

        case types.UPDATE_STEP_DIRECTION_FOR_COMPANY:
            return {
                ...state,
                direction: action.direction
            }

        case types.CANDIDATS_QUEUE_LOAD_SUCCESS:
            return {
                ...state,
                queue: action.queue,
                filters: action.filters
            };
        case types.GET_CHECKOUT_SUCCESS:
            return {
                ...state,
                checkoutData: action.checkoutData
            };
        case types.TOKEN_PRICE_LOADED_SUCCESS:
            return {
                ...state,
                tokenPrice: action.tokenPrice
            };
        case types.TELERGAM_VERIFICATION_LOADED:
            return {
                ...state,
                telegramVerification: action.telegramVerification,
                telegramVerificationFailed: false
            }
        case types.TELERGAM_VERIFICATION_FAILED:
            return {
                ...state,
                telegramVerificationFailed: action.notification
            }
        case types.TELERGAM_BOT_URL_LOADED_SUCCESS:
            return {
                ...state,
                telegramRegistrationUrl: action.telegramRegistrationUrl
            };
        case types.APLICANT_LIST_LOAD_SUCCESS:
            return {
                ...state,
                applicantsList: action.applicantsList,
                filters: action.filters
            }

        case types.KNOWLEDGES_LOADED:
            return {
                ...state,
                knowledges: action.knowledges
            }

        case types.SKILL_TESTS_HIERARCHY:
            return {
                ...state,
                skillTestsHierarchy: action.skillTestsHierarchy
            }

        case types.BLOCK_EMPLOYEE:
        case types.UN_BLOCK_EMPLOYEE:
            const { employeeList } = state,
                    list = _.map(employeeList, (item) => {
                        if(item.uid == action.uid){
                            return {...item, profile: {...item.profile, status: action.status}}
                        }
                        return item;
                    })
            return {
                ...state,
                employeeList: list
            }
        case CONTACT_PROFILE_APPLICANT:
            return {
                ...state,
                cvContact: action.applicantInfo
                }
        case types.CANDIDAT_BUY_SUCCESS:
                const {balance} = state;
                return{
                    ...state,
                    balance : {...balance, tokensCount: action.buyResult && action.buyResult.tokensCount}
                }
        case types.RECRUTER_STATISTIC_LOAD_SUCCESS:
            return{
                ...state,
                statisticRecruter: action.statisticRecruter,
                filters: action.filters
            }
        case types.SAVE_RATING:
            return{
                ...state,
                ratings : action.rating
            }
        case types.COMPANY_ADMIN_EMPLOYEES_WAITING_LIST_LOADED_FAILED:
        default:
            return state
    }
}
