import { connect } from 'react-redux'
import React from 'react'

const CompanyProgress = (WrapedComponent) => {

  class ProfileProgressComponent extends React.Component {
    componentDidMount(){

        const { match: { params: { step } }, companyProfile: { step: edgeStep }, actions: { updateStep } } = this.props,
                routesStep = step ? step : 1,
                direction = routesStep > edgeStep ? edgeStep : routesStep;

        updateStep && updateStep(direction);

    }

    shouldComponentUpdate(nextProps, nextState){

        const { companyProfile: { direction: prevDirection } } = this.props,
              { companyProfile: { direction: currentDirection, step: edgeStep }, match: { params: { step } }, history } = nextProps;

        if(edgeStep == 0) {
            history.push('/');
            return false;
        }

        if(currentDirection != prevDirection){
            history.push(`/steps/${currentDirection}`);
            return false;
        }

        if( !step || (step > edgeStep) ){
            history.push(`/steps/${edgeStep}`);
            return false;
        }


        return true;

    }

    componentDidUpdate(){

        const { match: { params: { page, step } }, companyProfile: { direction = null }, actions: { updateStep } } = this.props;

        if(step != direction){
            updateStep && updateStep(step);
        }

    }

    render() {

        const { companyProfile: { direction = null } } = this.props;

        return direction ? <WrapedComponent {...this.props} /> : null;

    }

  }

  return ProfileProgressComponent;

}
export default CompanyProgress;
