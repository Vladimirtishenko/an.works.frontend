import { connect } from 'react-redux'
import React from 'react'

const companyProfilePathComponent = (WrapedComponent) => {

  class PathComponent extends React.Component {

    constructor(props, context){
        super(props);

        this.state = {
            router: null
        }
    }

    componentDidMount(){

        let {email} = this.props.oauth.user || {}
        this.props.actions.getCompanyProfile(email);

    }

    componentDidUpdate(prevProps, prevState){

        let {step} = this.props.companyProfile || {},
            {step: previousStep} = prevProps.companyProfile || {};

        if(step != previousStep){

            let path = step > 0 ? 'step' : 'page';

            this.setState({
                router: path
            })
        }

    }

    render() {

        let {router} = this.state;

        return router && <WrapedComponent {...this.props} router={router} /> || null;

    }

  }

  return PathComponent;

}
export default companyProfilePathComponent;
