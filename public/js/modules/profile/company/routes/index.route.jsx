import React from 'react'
import { Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Loadable from 'react-loadable';
import Loading from '../../../../components/widgets/loader.jsx'

import pathComponent from '../decorators/path.dec.jsx';

import Notification from '../../../../libraries/notification/index.jsx'
import i18n from '../../../i18n/decorators/i18n.dec.jsx';
// Actions
import * as companyAction from '../actions/company.action.js'

const Steps = Loadable({
  loader: () => import('./step.route.jsx'),
  loading: Loading,
  delay: 500,
  timeout: 10000
}),

Page = Loadable({
  loader: () => import('./profile.route.jsx'),
  loading: Loading,
  delay: 500,
  timeout: 10000
});

function mapStateToProps(state) {

    return {
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
			...companyAction
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps, null, {pure: false})
@pathComponent
@i18n
export default class UserProfileRoute extends React.Component {

	render() {

		let Component = this.props.router == 'page' ? Page : Steps;

		return (
      <React.Fragment>
          <Notification onError={{component: null}} onSuccess={{component: null}} />
          <Component {...this.props} />
      </React.Fragment>
)
	}


}
