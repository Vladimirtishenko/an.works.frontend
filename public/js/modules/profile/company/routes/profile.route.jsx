import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import uniqid from 'uniqid';

import Info from "../containers/profile/info.ctrl.jsx";
import Employee from "../containers/profile/employee.ctrl.jsx";
import Payments from "../containers/profile/payments.ctrl.jsx";
import PaymentsHistory from "../containers/profile/paymentsHistory.ctrl.jsx";
import Rating from "../containers/profile/rating.ctrl.jsx";
import Recruters from "../containers/profile/recruters.ctrl.jsx";
import RecruterSingle from "../containers/profile/recruterSingle.ctrl.jsx";
import Security from "../containers/profile/security.ctrl.jsx";
import HireHistory from "../containers/profile/hireHistory.ctrl.jsx";
import CV from "../containers/profile/cv.ctrl.jsx";
import ContactUser from "../../../../components/contactUser/ContactUser.jsx";

import Error from "../../../../components/error/404.jsx"

const routes = [
    {
        path: "/",
        component: Info
    },
    {
        path: "/employee",
        component: Employee
    },
    {
        path: "/payments",
        component: Payments
    },
    {
        path: "/payments-history",
        component: PaymentsHistory
    },
    {
        path: "/rating",
        component: Rating
    },
    {
        path: "/recruters",
        component: Recruters
    },
    {
        path: "/recruters/:id",
        component: RecruterSingle
    },
    {
        path: "/security",
        component: Security
    },
    {
        path: "/hire-history",
        component: HireHistory
    },
    {
        path: "/cv/:profileId",
        component: CV
    },
    {
        path: "/contact",
        component: ContactUser
    },
    {
        path: "*",
        component: Error
    }
];

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate(){
        window.scrollTo(0,0);
    }
    render() {
        return (
            <Switch>
                {
                    routes.map((route, i) => {
                        const Component = route.component;
                        return (<Route
                                    key={i}
                                    path={route.path}
                                    exact
                                    render={(props) => {
                                        return <Component {...props} />
                                        }
                                    }
                                />)
                    })
                }
            </Switch>
        );
    }
}
