import React from "react";
import { Route, Switch } from "react-router-dom";

import Layout from "../components/layouts/layout.jsx";
import TopLayout from "../../../../components/includes/layout.jsx";
import Index from "../containers/steps/index.ctrl.jsx";
import Security from "../containers/profile/security.ctrl.jsx";
import CV from "../containers/profile/cv.ctrl.jsx";

import { linksStepsSchema } from "../schema/linksSteps.sc.js";

const routes = [
    {
        path: "/steps/:step?",
        component: Index
    },
    {
        path: "/security",
        component: Security
    },
    {
        path: "*",
        component: Index
    },
    {
        path: "/cv/:profileId",
        component: CV
    }
];

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate(){
        window.scrollTo(0,0);
    }

    render() {
        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksStepsSchema(this.props.i18n.translation)}
                    main={false}
                    progressBar={true}
                >
                    <Switch>
                        {routes.map((route, i) => (
                            <Route
                                key={i}
                                path={route.path}
                                exact
                                component={route.component}
                            />
                        ))}
                    </Switch>
                </Layout>
            </TopLayout>
        );
    }
}
