import React from 'react'
import Loadable from 'react-loadable';
import Loading from '../../components/widgets/loader.jsx'

const UserProfileRoute = Loadable({
  loader: () => import('./applicant/routes/index.route.jsx'),
  loading: Loading,
  delay: 500,
  timeout: 10000
}),

CompanyProfileRoute = Loadable({
  loader: () => import('./company/routes/index.route.jsx'),
  loading: Loading,
  delay: 500,
  timeout: 10000
}),

RecruiterProfileRoute = Loadable({
  loader: () => import('./recruiter/routes/index.route.jsx'),
  loading: Loading,
  delay: 500,
  timeout: 10000
});

export const IncludedProfile = (props) => {

  let role = props.oauth && props.oauth.user && props.oauth.user.role || '';

  switch (role) {
      case 'candidate':
        return <UserProfileRoute />;
      case 'company_admin':
        return <CompanyProfileRoute />;
      case 'company_user':
          return <RecruiterProfileRoute />;
      default:
        return null;
  }

}
