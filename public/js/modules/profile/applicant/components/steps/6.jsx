import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";

import serialize from "../../../../../helpers/serialize.helper.js";

// Editable components
import Occupation from "../editable/occupation.jsx";
import Geography from "../editable/geography.jsx";

// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";

import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import NotificationBackStep from '../profile/notification/notificationBackStep.jsx';
class Six extends Inheritance {
    constructor(props) {
        super(props);
     }

    serializeFormData(form) {
        let serializedForm = serialize(form),
            placesArray = _.get(
                serializedForm,
                "geography.relocation.abroad.places"
            ),
            places = placesArray ? _.values(placesArray) : [],
            cityArray = _.get(
                serializedForm,
                "geography.relocation.local.cities"
            ),
            cities = cityArray ? cityArray.split(',') : [],
            { direction, profileId } = this.props.applicant.profile;

        _.set(serializedForm, "geography.relocation.abroad.places", places);
        _.set(serializedForm, "geography.relocation.local.cities", cities);
        return {
            serializedForm,
            profileId,
            direction
        };
    }

    handlePrevStepClick(event) {
        let form = event.target.parentNode.parentNode;
        const { serializedForm, profileId, direction } = this.serializeFormData(
            form
        );

        this.props.actions.updateProfileWithoutSubmit(
            { geography: serializedForm.geography },
            profileId,
            direction
        );
    }

    submit(event) {
        event.preventDefault();

        let form = event.target;
        const { serializedForm, profileId, direction } = this.serializeFormData(
            form
        );

        this.props.actions.updateProfile(
            { geography: serializedForm.geography },
            profileId,
            direction
        );
    }

    render() {

        let { translation } = this.props.i18n,
            { geography } =
                this.props.applicant && this.props.applicant.profile
                    ? this.props.applicant.profile
                    : {};

        return (
            <React.Fragment>
                <NotificationBackStep/>
                <ValidatorForm onSubmit={::this.submit} className="form">
                    <Geography i18n={this.props.i18n} geography={geography} /> 

                    <Occupation i18n={this.props.i18n} occupation={geography.occupation} />

                    <div className="fl fl--justify-c fl--align-c">
                        <span
                            className="link link--grey font--14"
                            onClick={e => this.handlePrevStepClick(e)}
                        >
                            {translation.prev_step}
                        </span>
                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--l-auto margin--md-l-30">
                            {translation.next_step}
                        </button>
                    </div>
                </ValidatorForm>
            </React.Fragment>
        );
    }
}

export default Six;
