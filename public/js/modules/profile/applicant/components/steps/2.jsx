import React from "react";
import uniqid from "uniqid";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";
import { skillsComparing } from "../../../../../helpers/mapperSkills.helper.js";

// Additional components
import Language from "../editable/languages.jsx";
import Skills from "../editable/skills.jsx";
import SkillsDidntFind from "../modals/skillsDidntFind.jsx";
// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";

import { ValidatorForm } from '../../../../../libraries/validation/index.js';
import NotificationBackStep from '../profile/notification/notificationBackStep.jsx';
class Second extends Inheritance {
    constructor(props) {
        super(props);
        this.state = {
            languages: [],
            isSkillsPopupOpen: false
        };
    }

    componentDidMount() {
        if (this.props.applicant && !this.props.applicant.knowledges)
            this.props.actions.getWorkSkills();
    }
    handleChangeSkillsPopupStatus() {
        this.setState({
            ...this.state,
            isSkillsPopupOpen: !this.state.isSkillsPopupOpen
        });
    }

    serializeFormData(form) {
        let serializedForm = serialize(form),
            { knowledges } = this.props.applicant,
            { direction, profileId } = this.props.applicant.profile,
            skillsObject = skillsComparing(serializedForm, knowledges);

        return { skillsObject, serializedForm, profileId, direction };
    }

    handlePrevStepClick(event) {
        let form = event.target.parentNode.parentNode;
        const {
            skillsObject,
            serializedForm,
            profileId,
            direction
        } = this.serializeFormData(form);

        this.props.actions.updateProfileWithoutSubmit(
            {
                skills: skillsObject.skills,
                mainSkill: skillsObject.mainSkill,
                positionTitle: serializedForm.positionTitle,
                positionLevel: serializedForm.positionLevel
            },
            profileId,
            direction
        );
    }

    submit(event) {
        event.preventDefault();

        let form = event.target;
        const {
            skillsObject,
            serializedForm,
            profileId,
            direction
        } = this.serializeFormData(form);

        this.props.actions.updateProfile(
            {
                skills: skillsObject.skills,
                mainSkill: skillsObject.mainSkill,
                positionTitle: serializedForm.positionTitle,
                positionLevel: serializedForm.positionLevel
            },
            profileId,
            direction
        );
    }

    render() {
        let {
                i18n: { translation },
                applicant: { profile, knowledges }
            } = this.props,
            { languages, isSkillsPopupOpen } = this.state;

        return (
            <React.Fragment>
                <NotificationBackStep/>
                <ValidatorForm onSubmit={::this.submit} className="form">
                    <Skills 
                        i18n={this.props.i18n}
                        knowledges={knowledges}
                        profile={profile}
                        openSkillPopUp={::this.handleChangeSkillsPopupStatus}
                    />
                    <Language i18n={this.props.i18n} knowledges={knowledges} profile={profile} />

                    <div className="fl fl--justify-c fl--align-c">
                        <span
                            className="link link--grey font--14"
                            onClick={e => this.handlePrevStepClick(e)}
                        >
                            {translation.prev_step}
                        </span>
                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--l-auto margin--md-l-30">
                            {translation.next_step}
                        </button>
                    </div>
                </ValidatorForm>
                {isSkillsPopupOpen && (
                    <SkillsDidntFind
                        closePopup={::this.handleChangeSkillsPopupStatus}
                    />
                )}
            </React.Fragment>
        );
    }
}

export default Second;
