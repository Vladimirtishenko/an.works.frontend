import React from "react";
import uniqid from "uniqid";
import { Card } from "../../../common/components/card.dumb.jsx";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";

import Education from "../editable/education.jsx";

// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";
import UniversityDidntFind from "../modals/universityDidntFind.jsx";

import { transformToTotal } from "../../../../../helpers/date.helper.js";
import { ValidatorForm } from '../../../../../libraries/validation/index.js';
import NotificationBackStep from '../profile/notification/notificationBackStep.jsx';
class Five extends Inheritance {
    constructor(props) {
        super(props);
        this.state = {
            languages: [],
            isUniversityDidntFindPopupOpen: false
        };
    }
    handleChangeUniversityDidntFindPopupStatus() {
        this.setState({
            ...this.state,
            isUniversityDidntFindPopupOpen: !this.state.isUniversityDidntFindPopupOpen
        });
    }

    serializeFormData(form) {
        let serializedForm = serialize(form),
            transformObjectToArray = _.values(serializedForm.education),
            {
                applicant: {
                    profile: { direction, profileId }
                }
            } = this.props,
            { certificate, university } = transformToTotal(
                transformObjectToArray
            );

        return {
            transformObjectToArray,
            university,
            certificate,
            profileId,
            direction
        };
    }

    handlePrevStepClick(event) {
        let form = event.target.parentNode.parentNode;
        const {
            transformObjectToArray,
            university,
            certificate,
            profileId,
            direction
        } = this.serializeFormData(form);

        this.props.actions.updateProfileWithoutSubmit(
            {
                educational: {
                    items: transformObjectToArray,
                    university: university,
                    certificate: certificate
                }
            },
            profileId,
            direction
        );
    }

    submit(event) {
        event.preventDefault();

        let form = event.target;
        const {
            transformObjectToArray,
            university,
            certificate,
            profileId,
            direction
        } = this.serializeFormData(form);

        this.props.actions.updateProfile(
            {
                educational: {
                    items: transformObjectToArray,
                    university: university,
                    certificate: certificate
                }
            },
            profileId,
            direction
        );
    }

    render() {
        let { translation } = this.props.i18n;
        const { isUniversityDidntFindPopupOpen } = this.state;
        return (
            <React.Fragment>
                <NotificationBackStep/> 
                <ValidatorForm onSubmit={::this.submit} className="form">
                    <Education {...this.props}  openEducationPopUp={::this.handleChangeUniversityDidntFindPopupStatus} />
                    <div className="fl fl--justify-c fl--align-c">
                        <span
                            className="link link--grey font--14"
                            onClick={e => this.handlePrevStepClick(e)}
                        >
                            {translation.prev_step}
                        </span>
                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--l-auto margin--md-l-30">
                            {translation.next_step}
                        </button>
                    </div>
                </ValidatorForm>
                {isUniversityDidntFindPopupOpen && (
                    <UniversityDidntFind 
                        closePopup={::this.handleChangeUniversityDidntFindPopupStatus}
                    />
                )}
            </React.Fragment>
        );
    }
}

export default Five;
