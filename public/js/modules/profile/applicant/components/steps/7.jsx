import React from "react";
import Textarea from "../../../../../components/form/textarea.jsx";
import { Card } from "../../../common/components/card.dumb.jsx";

import serialize from "../../../../../helpers/serialize.helper.js";

// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
// Editable
import About from "../editable/about.jsx";
import NotificationBackStep from '../profile/notification/notificationBackStep.jsx';
class Seven extends Inheritance {
    serializeFormData(form) {
        let serializedForm = serialize(form),
            { direction, profileId } = this.props.applicant.profile;

        return {
            serializedForm,
            profileId,
            direction
        };
    }

    handlePrevStepClick(event) {
        let form = event.target.parentNode.parentNode;
        const { serializedForm, profileId, direction } = this.serializeFormData(
            form
        );

        this.props.actions.updateProfileWithoutSubmit(
            { aboutMe: serializedForm.aboutMe },
            profileId,
            direction
        );
    }

    submit(event) {
        event.preventDefault();

        let form = event.target;
        const { serializedForm, profileId } = this.serializeFormData(form);
        
        this.props.actions.updateProfile(
            { aboutMe: serializedForm.aboutMe ? serializedForm.aboutMe :  { about: '' } },
            profileId,
            0
        ); 
    }

    render() {
        let { translation } = this.props.i18n,
            { aboutMe: about } =
                this.props.applicant && this.props.applicant.profile
                    ? this.props.applicant.profile
                    : {};

        return (
            <React.Fragment>
                <NotificationBackStep/>
                <ValidatorForm onSubmit={::this.submit} className="form">
                    <About i18n={this.props.i18n} about={about} {...this.props} />
                    <div className="fl fl--justify-c fl--align-c">
                        <span
                            className="link link--grey font--14"
                            onClick={e => this.handlePrevStepClick(e)}
                        >
                            {translation.prev_step}
                        </span>
                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--l-auto margin--md-l-30">
                            {translation.send_data}
                        </button>
                    </div>
                </ValidatorForm>
            </React.Fragment>
        );
    }
}

export default Seven;
