import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";
import uniqid from "uniqid";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";

import Experience from "../editable/experience.jsx";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";

// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";

import { ValidatorForm } from '../../../../../libraries/validation/index.js';
import NotificationBackStep from '../profile/notification/notificationBackStep.jsx';
@i18nMonthDecorator
class Four extends Inheritance {
    componentDidMount() {
        const { applicant: { knowledges }, actions: { getWorkSkills } } = this.props;

        !knowledges && getWorkSkills();
  }

    submit(event) {

      event.preventDefault();

      let form = event.target,
          serializedForm = serialize(form),
          transformObjectToArray = _.values(serializedForm.experience),
          {
              applicant: {
                  profile: {
                      direction,
                      profileId,
                      experience: {
                          it = 0,
                          other = 0
                      }
                  }
              },
              actions: {
                  updateProfile
              }
          } = this.props;

          updateProfile({experience: {items: transformObjectToArray, it: it, other: other} }, profileId, direction);
    }

    serializeFormData(form) {
        let serializedForm = serialize(form),
            transformObjectToArray = _.values(serializedForm.experience),
            {
                applicant: {
                    profile: {
                        direction,
                        profileId,
                        experience: { it = 0, other = 0 }
                    }
                }
            } = this.props;

        return { transformObjectToArray, it, other, profileId, direction };
    }


    handlePrevStepClick(event) {
        let form = event.target.parentNode.parentNode;
        const {
            transformObjectToArray,
            it,
            other,
            profileId,
            direction
        } = this.serializeFormData(form);

        this.props.actions.updateProfileWithoutSubmit(
            {
                experience: {
                    items: transformObjectToArray,
                    it: it,
                    other: other
                }
            },
            profileId,
            direction
        );
    }


    render() {

        let {translation} = this.props.i18n;

        return (
            <React.Fragment>
                <NotificationBackStep/>
                <ValidatorForm onSubmit={::this.submit} className="form">
                   <Experience {...this.props} />

                    <div className="fl fl--justify-c fl--align-c">
                        <span
                            className="link link--grey font--14"
                            onClick={e => this.handlePrevStepClick(e)}
                        >
                            {translation.prev_step}
                        </span>
                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--l-auto margin--md-l-30">
                            {translation.next_step}
                        </button>
                    </div>
                </ValidatorForm>
            </React.Fragment>    
            );
    }
}

export default Four;
