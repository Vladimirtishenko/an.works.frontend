import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";
import { Link } from "react-router-dom";

import ListTest from '../../components/tests/listTestOfResilts.jsx';

import {NotificationСabinet} from "../../../../../components/widgets/notification/notificationСabinet.jsx";
import TestStatusFilter from '../../../../../components/filters/publicFilters/testStatusFilter.jsx';

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";

import {
    changeOneOf,
    getOneOf,
} from "../../../../../helpers/filters/filter.helper.js";

import {
    orderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js';

// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";
import { TestsAdd } from "../modals/testsAdd.jsx";

class Third extends Inheritance {
    constructor(props) {
        super(props);
        this.state = {
            isOverlayed: false,
            showPopup: false,
            testUid: null,
            isTestsAddPopupOpen: false,
            mainTest: null
        };
    }

    handleChangeTestsAddPopupStatus(status) {
        this.setState({
            ...this.state,
            isTestsAddPopupOpen: status
        });
    }

    componentDidMount() {

        const {
            applicant: {
                skillTestsHierarchy,
                testsResults,
                profile: {
                    mainSkill,
                    profileId
                }
            },
            actions: {
                getSkillTestHierarchy,
                getProfileResults,
                getTestsFromService
            },
            location: { search }
        } = this.props;

        getSkillTestHierarchy(mainSkill);
        getProfileResults(profileId);
        getTestsFromService()

    }

    submit(event) {
        event.preventDefault();

        let form = event.target,
            serializedForm = serialize(form),
            { applicant: { profile: { direction, skills = [] } }, oauth: { user } } = this.props,
            newSkills = skills.map(item => {
                return { ...item, rate: item.rate + 1 * 15 };
            });

        this.props.actions.updateStep(4);
    }

    filterOnChange(value){
        const { actions: { getSkillTestHierarchy }, applicant: { profile: { profileId }, filters } } = this.props,
                normalizedToFilters = changeOneOf(filters, {testStatus: value.label});

        getSkillTestHierarchy(profileId, normalizedToFilters);
    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { actions: { getTestList, getSkillTestHierarchy }, applicant: { profile: { profileId }, filters } } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getSkillTestHierarchy(profileId, filtersNormalizedWithNewOrderBy);
    }

    notificationStatus(){
        let {
            applicant:{
                testsResults = [],
                skillTestsHierarchy:{
                    tests = []
                }
           },
           i18n:{translation = {}} = {}
        } = this.props;
        let res = testsResults.find(resEl => {
            return tests[0].testLabel == resEl.testUid;
        });
        if(!testsResults.length || !res){
            return(
                <NotificationСabinet type="warning">
                    {translation.ruleToContinueRegistrationPassBasicTest}
                </NotificationСabinet>
            )
         }else if(res && res.result >= 30){
            return(
                <NotificationСabinet type="information">
                    {translation.ruleImproveRankingTakeAddTests}
                </NotificationСabinet >
            )
        }else if(res && res.result < 30){
            return(
                <NotificationСabinet type="success">
                    {translation.ruleChooseAnotherBasicSkill}
                </NotificationСabinet>
            )
        }
    }
    render() {

        const {
                applicant: {
                    filters,
                    skillTestsHierarchy: {
                        tests = [],
                        host = ''
                    } = {},
                    testsResults = [],
                    skillTestsHierarchy,
                    profile:{
                        status =""
                    }
                },
                i18n: { translation }
            } = this.props,
            { isTestsAddPopupOpen } = this.state,
              getFilterValue = getOneOf(filters, 'testStatus') || '',
              justOrderBy = orderBy(filters);
        return (
            <div>
                <form
                    onSubmit={::this.submit}
                    name="form"
                    className="padding--t-20"
                >
                    {skillTestsHierarchy && this.notificationStatus() || null}
                    <Card icon="test" title={translation.testingSkills}>
                        {/* TODO: not mvp <TestStatusFilter value={getFilterValue} onChange={::this.filterOnChange}/> */}
                        <table className="respons-flex-table">
                            <thead className="respons-flex-table__thead">
                                <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                                    <th
                                        className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-25
                                        font--left
                                    "
                                    >
                                        <span
                                            // onClick={::this.sortingTable}
                                            // data-sort="skill"
                                            // data-value={justOrderBy.skill || ""}
                                            // className={`${orderingByClass(justOrderBy.skill)}`}
                                        >
                                            {translation.skill}
                                        </span>
                                    </th>
                                    <th className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-13
                                        font--center
                                    ">
                                        <span
                                            // onClick={::this.sortingTable}
                                            // data-sort="priority"
                                            // data-value={justOrderBy.priority || ""}
                                            // className={`${orderingByClass(justOrderBy.priority)}`}
                                        >
                                            {translation.priority}
                                        </span>
                                    </th>
                                    <th className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-15
                                        font--center
                                    ">
                                        <span
                                            // onClick={::this.sortingTable}
                                            // data-sort="result"
                                            // data-value={justOrderBy.result || ""}
                                            // className={`${orderingByClass(justOrderBy.result)}`}
                                        >
                                            {translation.result}
                                        </span>
                                    </th>
                                    <th className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-15
                                        font--center
                                    ">
                                        {translation.time}
                                    </th>
                                    <th className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-12
                                        font--left
                                        font--xl-center
                                    ">
                                        <span
                                            // onClick={::this.sortingTable}
                                            // data-sort="available"
                                            // data-value={justOrderBy.available || ""}
                                            // className={`${orderingByClass(justOrderBy.available)}`}
                                        >
                                            {translation.valid}
                                        </span>
                                    </th>
                                    {/*
                                    TODO: not mvp
                                    <th
                                        className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-15
                                        font--center
                                    "
                                    >
                                        Публиковать
                                    </th> */}
                                    <th className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-20
                                        font--md-right
                                    ">
                                        {translation.details}
                                    </th>
                                </tr>
                            </thead>

                            <tbody className="respons-flex-table__tbody">
                                <ListTest
                                    testsResults={testsResults}
                                    tests={tests}
                                    status={status}
                                    mainText={true}
                                />
                            </tbody>
                            {/*
                            TODO: not mvp
                            <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-15 padding--t-20 padding--b-30 fl--wrap">
                                <tr>
                                    <td className="">
                                        <span
                                            className="font--color-blue font--12 padding--r-20 icons--Add icons--size-16 icons--top-3 icons--margin-r-10 pointer-text "
                                            onClick={() =>
                                                this.handleChangeTestsAddPopupStatus(
                                                    true
                                                )
                                            }
                                        >
                                            Добавить тест
                                        </span>
                                    </td>
                                </tr>
                            </tfoot> */}
                        </table>
                    </Card>
                    <div className="fl fl--justify-c fl--align-c">
                        <span
                            onClick={::this.prevStep}
                            className="link link--grey font--14"
                        >
                            {translation.prev_step}
                        </span>
                        <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--l-auto margin--md-l-30">
                            {translation.next_step}
                        </button>
                    </div>
                </form>
                {isTestsAddPopupOpen && (
                    <TestsAdd
                        onClose={() =>
                            this.handleChangeTestsAddPopupStatus(false)
                        }
                    />
                )}
            </div>
        );
    }
}

// НЕ УДАЛЯТЬ!!!!, это верстка таблицы с корзиной 1.03.2019
// return (
//     <tr
//         key={i}
//         className="respons-flex-table__tr font--color-secondary"
//     >
//         <td
//             className="
//             respons-flex-table__td
//             respons-flex-table__td--no-before
//             respons-flex-table--w-90
//             respons-flex-table--xl-w-25
//             l-h--1
//             fl--order-1
//             respons-flex-table--font-14
//             font--color-blue
//         ">
//             {item.testLabel}
//         </td>
//         <td
//             data-title="Приоритет"
//             className="
//             respons-flex-table__td
//             respons-flex-table--w-50
//             respons-flex-table--md-w-30
//             respons-flex-table--xl-w-15
//             fl--order-3
//             fl--order-md-2
//             font--color-inactive
//             font--xl-center
//         ">
//             {item.level + 1}
//         </td>
//         <td
//             data-title="Результат"
//             className="
//             respons-flex-table__td
//             respons-flex-table--w-50
//             respons-flex-table--md-w-40
//             respons-flex-table--xl-w-10
//             fl--order-4
//             fl--order-md-3
//             font--color-inactive
//         ">
//             {res ? (
//                 res.result
//             ) : (
//                 <button
//                     id={item.testLabel}
//                     onClick={::this.togglePopup}
//                 >
//                     Пройти(пример)
//                 </button>
//             )}
//         </td>
//         <td
//             data-title="Время"
//             className="
//             respons-flex-table__td
//             respons-flex-table--w-50
//             respons-flex-table--md-w-10
//             respons-flex-table--xl-w-15
//             fl--order-5
//             fl--order-md-4
//             font--color-inactive
//             font--xl-center
//         ">
//             {(res && res.time) || "-"}
//         </td>
//         <td
//             data-title="Действ. до"
//             className="
//             respons-flex-table__td
//             respons-flex-table--xl-w-10
//             font--left
//             respons-flex-table--w-50
//             respons-flex-table--md-w-30
//             fl--order-6
//             fl--order-lg
//             font--color-inactive
//         ">
//             {(res && dmyDate(res.expiredDate).toUTCString()) ||
//                 "-"}
//         </td>
//         <td
//             data-title="Публиковать"
//             className="
//             respons-flex-table__td
//             respons-flex-table--xl-w-15
//             respons-flex-table--md-w-30
//             fl--order-7
//             font--color-inactive
//             font--xl-center
//         ">
//              <Checkbox
//                 wrapperClass="checkbox"
//                 labelClass="form__checkbox form__label"
//                 value={
//                     [
//                         {
//                             name: "geography.relocation.local.type",
//                             checked: false,
//                             innerWrapperClass: "d-inline_block font--14 font--color-secondary",
//                             value: true
//                         }
//                     ]
//                 }
//             />
//         </td>
//         <td className="
//             respons-flex-table__td
//             respons-flex-table--md-j-center
//             respons-flex-table__td--no-before
//             respons-flex-table--xl-w-10
//             respons-flex-table--md-w-20
//             fl--order-2
//             fl--order-md-5
//             fl--order-xl-7
//             respons-flex-table--font-10
//             font--color-secondary
//             font--right
//         ">
//             {res ? (
//                 <span>
//                     Следующая попытка{" "}
//                     {dmyDate(res.nextTryDate).toUTCString()}
//                 </span>
//             ) : (
//                 <span className="icon icon--delete link link--grey-h-b font--16" />
//             )}
//         </td>
//     </tr>
// );
// });
export default Third;