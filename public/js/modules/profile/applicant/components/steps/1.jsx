import React from "react";

// Additional components
import ContactCard from "../../../common/components/editable/contactCard.jsx";
import Personal from "../editable/personal.jsx";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";
import Number from "../../../../../components/form/number.jsx";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";

import { ValidatorForm } from "../../../../../libraries/validation/index.js";

@i18nMonthDecorator
class First extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOverlayed: false,
            data: props.applicant &&
                props.applicant.profile && { ...props.applicant.profile }
        };
    }

    componentDidMount() {
        if (
            this.props.applicant &&
            !this.props.applicant.telegramRegistrationUrl
        ) {
            this.props.actions.isTelegramVerified();
            this.props.actions.getTelegramRegistrationUrl();
        }
    }

    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            { direction, profileId } = this.props.applicant.profile,
            date =
                serializedForm.birthday &&
                Array.isArray(serializedForm.birthday)
                    ? serializedForm.birthday.reverse()
                    : "",
            birthday = date ? new Date(date[0], date[1]-1, date[2]).valueOf() : "";

        serializedForm.birthday = birthday;

        this.props.actions.updateProfile(serializedForm, profileId, direction);
    }

    render() {
        let { translation } = this.props.i18n,
            {
                profile,
                telegramRegistrationUrl,
                telegramVerification,
                telegramVerificationFailed
            } = this.props.applicant || {};
        return (
            <ValidatorForm onSubmit={::this.submit} className="form">
                <Personal {...this.props} />

                <ContactCard
                    i18n={this.props.i18n}
                    actions={this.props.actions || {}}
                    contactInfo={(profile && profile.contactInfo) || {}}
                    telegramVerification={telegramVerification}
                    telegramRegistrationUrl={
                        telegramRegistrationUrl && telegramRegistrationUrl.url
                    }
                    telegramVerificationFailed={telegramVerificationFailed}
                />

                <div className="fl fl--justify-c fl--align-c">
                    <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--t-10">
                        {translation.next_step}
                    </button>
                </div>
            </ValidatorForm>
        );
    }
}

export default First;
