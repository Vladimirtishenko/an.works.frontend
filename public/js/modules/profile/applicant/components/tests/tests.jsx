import React from 'react'

import StaticItem from '../../../../../components/form/staticItem.jsx';
import ComboboxComponent from '../../../../../components/form/combobox.jsx';
import Checkbox from '../../../../../components/form/checkbox.jsx';
import {Card} from '../../../common/components/card.dumb.jsx'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import MyTests from './myTests.jsx'
import Archive from './archive.jsx'
import TestStatusFilter from '../../../../../components/filters/publicFilters/testStatusFilter.jsx'

import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js'

import {
    changeOneOf,
    getOneOf,
} from "../../../../../helpers/filters/filter.helper.js"

class Tests extends React.Component {
    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { actions: { getTestList }, applicant: { profile: { profileId }, filters } } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getTestList(profileId, filtersNormalizedWithNewOrderBy);
    }

    onClearFilter(){
        const { actions: { getTestList }, applicant: { profile: { profileId } } } = this.props;
        getTestList(profileId);
    }

    filterOnChange(value){
        const { actions: { getTestList }, applicant: { profile: { profileId }, filters } } = this.props,
                normalizedToFilters = changeOneOf(filters, {testStatus: value.label});
 
        getTestList(profileId, normalizedToFilters);
    }

    render() {
        const { applicant: { profile, filters }, i18n:{ translation = {} } = {} } = this.props,
              getFilterValue = getOneOf(filters, 'testStatus') || '';

        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">{translation.tests}</h2>
                <Tabs onSelect={::this.onClearFilter} className="box--white box--rounded shadow--table">
                    <TabList className="tablist tablist--two-tabs shadow--small">
                        <Tab >{translation.myTests}</Tab>
                        <Tab >{translation.archive}</Tab>
                    </TabList>

                    <TabPanel>
                        {/*
                        TODO: no mvp 
                        <TestStatusFilter value={getFilterValue} onChange={::this.filterOnChange}/> */}
                        <MyTests onChange={::this.sortingTable} filters={filters} {...this.props}/> 
                    </TabPanel>
                    <TabPanel>
                        <Archive onChange={::this.sortingTable} filters={filters} {...this.props}/>
                    </TabPanel>
                </Tabs>
            </React.Fragment>
        )
    }

}

export default Tests;
