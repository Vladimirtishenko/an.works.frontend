import React from 'react'
import Checkbox from '../../../../../components/form/checkbox.jsx';
import hosts from '../../../../../configuration/quiz.json';
import {
    dmyDate,
    transformToDays
} from '../../../../../helpers/date.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class MainTest  extends React.Component{
    render(){
        const {item,res = {}, main, mainText = '', i18n:{translation = {}} = {}} = this.props;
        return(
            <tr
             className={`respons-flex-table__tr  ${main ? "respons-flex-table__tr--main" : ''}  font--color-secondary`}
            >
            <td
                className="
                respons-flex-table__td
                respons-flex-table__td--no-before
                respons-flex-table--w-50
                respons-flex-table--xl-w-25
                l-h--1
                fl--order-1
                respons-flex-table--font-14
                font--color-blue
                padding--r-10
                word-break--word
            ">
                {item.title || translation.testNotFound}
                {
                   mainText && (
                    <span className="
                        font--10
                        font--color-inactive
                        l-h--1-4
                    ">
                        {translation.mainSkill}
                    </span>
                   ) || res && res.pastDate && (
                        <span className="
                            font--10
                            font--color-inactive
                            l-h--1-4
                        ">
                            {`${translation.passed} ${dmyDate(res.pastDate)}`}
                        </span>
                    )
                }
            </td>
            <td
                data-title={translation.priority}
                className="
                respons-flex-table__td
                respons-flex-table--w-50
                respons-flex-table--md-w-30
                respons-flex-table--xl-w-13
                fl--order-3
                fl--order-md-2
                font--color-primary
                font--500
                font--xl-center
            ">
                   {item.level + 1}
            </td>
            <td
                data-title={translation.result}
                className="
                respons-flex-table__td
                respons-flex-table--w-50
                respons-flex-table--xl-w-15
                fl--order-4
                fl--order-md-3
                font--color-primary
                font--500
                font--xl-center
                fl--align-xl-c
            ">
                {
                    res && res.result >= 0 && (
                        <span
                        className={ res.passed ? "respons-flex-table--passed" : "respons-flex-table--failed"}>
                            {res.result}
                        </span>
                    ) || (
                        <a
                            className="link--theme-blue max-width--80 width--100 font--center self--xl-center"
                            href={`${hosts.frontend}/ru/quiz/${item.testLabel}/view/${localStorage.authToken}?lead=${location.pathname}`}
                        >
                            {translation.passThe}
                        </a>
                    ) 
                }
    
            </td>
            <td
                data-title={translation.time}
                className="
                respons-flex-table__td
                respons-flex-table--w-50
                respons-flex-table--md-w-30
                respons-flex-table--xl-w-15
                fl--order-5
                fl--order-md-4
                font--color-primary
                font--xl-center
            ">
                {(res && res.time) || "-"}
            </td>
            <td
                data-title={translation.valid}
                className="
                respons-flex-table__td
                font--left
                respons-flex-table--w-50
                respons-flex-table--md-w-20
                respons-flex-table--xl-w-12
                fl--order-6
                fl--order-lg
                font--color-primary
                font--xl-center
            "> 
                {(res && res.expiredDate && dmyDate(res.expiredDate)) ||
                    "-"}
            </td>
            {/* 
            TODO: not mvp
            <td
                data-title="Публиковать"
                className="
                respons-flex-table__td
                respons-flex-table--w-50
                respons-flex-table--xl-w-15
                respons-flex-table--md-w-30
                fl--order-7
                font--color-primary
                font--xl-center
            ">
                <Checkbox
                    wrapperClass="checkbox"
                    labelClass="form__checkbox form__label"
                    value={
                        [
                            {
                                name: "geography.relocation.local.type",
                                checked: false,
                                innerWrapperClass: "d-inline_block font--14 font--color-secondary",
                                value: true
                            }
                        ]
                    }
                />
            </td> */}
            <td
                data-title={translation.details}
                className="
                respons-flex-table__td
                respons-flex-table--md-j-center
                respons-flex-table--w-50
                respons-flex-table--md-w-20
                respons-flex-table--xl-w-20
                fl--order-2
                fl--order-xl-7
                respons-flex-table--font-10
                font--color-secondary
                font--xl-right
                font--left
                fl--align-xl-end
            ">
                {res && res.nextTryDate && (transformToDays(res.nextTryDate) < 0 ?
                ( <a className="link--theme-blue max-width--100 width--100 font--center self--xl-center"
                        href={`${hosts.frontend}/ru/quiz/${item.testLabel}/view/${localStorage.authToken}?lead=${location.pathname}`}
                    >
                        {translation.passTheRepeat}
                    </a> 
                ) :
                (
                    <span>
                        {translation.nextTry}{" "}
                        <span className="display--b">
                        {dmyDate(res.nextTryDate)}
                        </span>
                    </span>
                ))
                    ||
                (
                    ' - '
                )}
            </td>
        </tr>
        )
    }
    
};