import React from 'react'
import { connect } from "react-redux";
import {
    orderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js'

import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class Tests extends React.Component {
    render() {
        const { 
            filters,
            onChange, 
            applicant:{
                skillTestsHierarchy: {
                    tests = []
                } = {},
                testArchive = []
            },
            i18n:{
                translation = {}
            }
        } = this.props,
                justOrderBy = orderBy(filters);
        return (
            <table className="respons-flex-table">
                <thead className="respons-flex-table__thead">
                <tr className="respons-flex-table__tr">
                    <th className="respons-flex-table__th font--left respons-flex-table--xl-w-33">
                        <span
                            // onClick={onChange}
                            // data-sort="skill"
                            // data-value={justOrderBy.skill || ""}
                            // className={`${orderingByClass(justOrderBy.skill)}`}
                        >
                            {translation.skill}
                        </span>
                    </th>
                    <th className="respons-flex-table__th font--center respons-flex-table--xl-w-33">
                        <span
                            // onClick={onChange}
                            // data-sort="result"
                            // data-value={justOrderBy.result || ""}
                            // className={`${orderingByClass(justOrderBy.result)}`}
                        >
                            {translation.result}
                        </span>
                    </th>
                    <th className="respons-flex-table__th font--center respons-flex-table--xl-w-33">
                        {translation.time}
                    </th>
                </tr>
                </thead>
                <tbody className="respons-flex-table__tbody">
                    <tr className="respons-flex-table__tr shadow--small">
                       <td className="respons-flex-table__td font--color-secondary">
                            {translation.testArchive}
                       </td>
                    </tr>
                {/* <tr className="responsive-table__tr shadow--small">
                    <td data-title="Навык" className="responsive-table__td responsive-table--tests responsive-table--available">
                        JavaScript
                        <div className="font--10 font--color-secondary">Пройден 10.10.2018</div>
                    </td>
                    <td data-title="Результат" className="responsive-table__td responsive-table--tests font--center">98</td>
                    <td data-title="Время" className="responsive-table__td responsive-table--tests font--12 font--center">00:25</td>
                </tr>
                <tr className="responsive-table__tr">
                    <td data-title="Навык" className="responsive-table__td responsive-table--tests responsive-table--available">
                        Node.js
                        <div className="font--10 font--color-secondary">Пройден 10.10.2018</div>
                    </td>
                    <td data-title="Результат" className="responsive-table__td responsive-table--tests font--center">23</td>
                    <td data-title="Время" className="responsive-table__td responsive-table--tests font--12 font--center">00:13</td>
                </tr>
                <tr className="responsive-table__tr">
                    <td data-title="Навык" className="responsive-table__td responsive-table--tests responsive-table--available">
                        English
                        <div className="font--10 font--color-secondary">Пройден 10.10.2018</div>
                    </td>
                    <td data-title="Результат" className="responsive-table__td responsive-table--tests font--center">98</td>
                    <td data-title="Время" className="responsive-table__td responsive-table--tests font--12 font--center">00:20</td> 
                </tr>*/}
                </tbody>
            </table>
        )
    }
}
