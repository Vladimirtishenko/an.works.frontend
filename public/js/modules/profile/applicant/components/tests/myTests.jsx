import React from 'react'
import Checkbox from '../../../../../components/form/checkbox.jsx';
import ListTest from './listTestOfResilts.jsx';
import {
    orderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js';
import {
    statusUser
} from '../../../../../helpers/statusUser.helper.js';

export default class Tests extends React.Component {

    render() {

        const {
             filters,
             onChange,
             applicant: {
                skillTestsHierarchy: {
                    tests = [],
                    host = ''
                } = {},
                profile:{
                    status =""
                },
                testsResults = []
             },
             i18n:{ translation = {} } = {}
        } = this.props,
        justOrderBy = orderBy(filters);
        return (
            <table className="respons-flex-table">
                <thead className="respons-flex-table__thead">
                    <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-25
                            font--left
                        ">
                            <span
                                // onClick={onChange}
                                // data-sort="skill"
                                // data-value={justOrderBy.skill || ""}
                                // className={`${orderingByClass(justOrderBy.skill)}`}
                            >
                                {translation.skill}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-13
                            font--center
                        ">
                            <span
                                // onClick={onChange}
                                // data-sort="priority"
                                // data-value={justOrderBy.priority || ""}
                                // className={`${orderingByClass(justOrderBy.priority)}`}
                            >
                                {translation.priority}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-15
                            font--center
                        ">
                            <span
                                // onClick={onChange}
                                // data-sort="results"
                                // data-value={justOrderBy.results || ""}
                                // className={`${orderingByClass(justOrderBy.results)}`}
                            >
                                {translation.result}
                            </span>
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-15
                            font--center
                        ">
                            {translation.time}
                        </th>
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-12
                            font--left
                            font--xl-center
                        ">
                            <span
                                // onClick={onChange}
                                // data-sort="available"
                                // data-value={justOrderBy.available || ""}
                                // className={`${orderingByClass(justOrderBy.available)}`}
                            >
                                {translation.valid}
                            </span>
                        </th>
                        {/* <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-15
                            font--center
                        ">
                            Публиковать
                        </th> */}
                        <th className="
                            respons-flex-table__th
                            respons-flex-table--xl-w-20
                            font--md-right
                        ">
                            {translation.details}
                        </th>
                    </tr>
                </thead>
                <tbody className="respons-flex-table__tbody">
                    <ListTest testsResults={testsResults} tests={tests} status={status} />
                </tbody>
                {/*
                TODO: not mvp
                <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-15 padding--t-20 padding--b-30 fl--wrap">
                    <tr>
                        <td className="">
                            <span className="padding--r-20 link">
                                <span className="icon icon--add" />
                                Добавить тест
                            </span>
                        </td>
                    </tr>
                </tfoot> */}
            </table>
        )
    }
}
