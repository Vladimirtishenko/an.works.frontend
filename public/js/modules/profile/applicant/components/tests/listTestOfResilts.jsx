import React from 'react'
import MainTest from './mainTest.jsx';
import {
    statusUser
} from '../../../../../helpers/statusUser.helper.js'; 

export default class getListOfResults extends React.Component {

    render() {
        const {
            testsResults = [],
            tests = [] ,
            status = "",
            mainText = ''
        } = this.props;
        return (
            <React.Fragment>
                {
                    tests.map((item, i) => {
                        let res = testsResults.find(resEl => {
                            return item.testLabel == resEl.testUid;
                        });
                        if(res && item.level == 0){
                            return (
                                <MainTest item={item} res={res} main={true} key={i}/>
                            )
                        }else if(!res && !statusUser(status)){
                            return (
                                null
                            )
                        }else if(!res && item.level == 0 ){
                            return (
                                <MainTest item={item} res={res} mainText={mainText} main={true} key={i}/>
                            )
                        }else {
                            return (
                                <MainTest item={item} res={res} key={i}/>
                            )
                        }
                    })
                }
            </React.Fragment>
        )
    }
}