import React from "react";

const CVTabHeader = props => (
    <div className="cv-tabs">
        {props.mockTabs.map((tab, index) => (
            <div
                key={index}
                className={`cv-tabs__tab ${
                    index === props.tabIndex ? "cv-tabs__tab_selected" : ""
                } `}
                onClick={() => props.handleChangeTab(index)}
            >
                {tab}
            </div>
        ))}
    </div>
);
export default CVTabHeader;