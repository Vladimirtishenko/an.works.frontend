import React from 'react'
import StaticItem from '../../../../../components/form/staticItem.jsx';
import {Card} from '../../../common/components/card.dumb.jsx';
import genderList from '../../../../../databases/general/gender.json';

import {
    separateDate,
    preparationMonth,
    getTextMonth
    } from '../../../../../helpers/date.helper.js'

import {
    getGenderStaticName
} from '../../../../../helpers/gender.helper.js'
export default class Personal extends React.Component {

    render() { 

        let { 
            i18n:{
                translation = {},
                translation:{
                    month = "",
                }
            },
            applicant:{
                profile:{
                    firstName = "",
                    lastName = "",
                    birthday = "",
                    gender = ""
                }},
            onEdit,
            notEdit
        } = this.props,

            date = separateDate(birthday),
            monthPrepare = preparationMonth(month),
            genderRealName = getGenderStaticName(gender),
            datePutTogether = date.day + ' ' + getTextMonth(date.month, monthPrepare) + ' ' + date.year,
            edit = notEdit ? {iconRight:"edit", link:translation.edit || '', linkAction: onEdit} : {};

        return (
            <Card
                icon="personal"
                {...edit}
                linkComponent="personal"
                title={translation.personalInfo}
                cardPadding="padding--15 padding--md-30"
            >
                <div className="row">
                    <div className="col-12 col-sm-4 margin--b-15 margin--sm-bottom-0">
                        <div className="font--capitalized font--color-inactive  font--12 margin--b-15">{translation.firstName}</div>
                        <StaticItem
                            staticClass="font--14 font--color-primary font--capitalized word-break--word"
                            staticText={`${firstName} ${lastName}`}
                        />

                    </div>
                    <div className="col-6 col-sm-4">
                        <div className="font--capitalized font--color-inactive  font--12 margin--b-15">{translation.birthday}</div>
                        <StaticItem
                            staticClass="font--14 font--color-primary word-break--word"
                            staticText={datePutTogether}
                        />

                    </div>
                    <div className="col-6 col-sm-4">
                        <div className="font--capitalized font--color-inactive  font--12 margin--b-15">{translation.gender}</div>
                        <StaticItem
                            staticClass="font--14 font--color-primary font--capitalized word-break--word"
                            staticText={genderRealName}
                        />
                    </div>
                </div>
            </Card>
        )
    }
}
