import React from 'react'

import RelocationAbroadSingle from './reloationAbroadSingle.jsx'

export default class RelocationAbroad extends React.Component {

    constructor(props){
        super(props);
    }

    render (){

        let { geography: { relocation: { abroad } = {}, country } = {}, i18n:{translation = {}} = {} } = this.props,
            {places, type} = abroad;
        return (
            <div>
                {
                    type && <div>
                                <div className="font--14 margin--b-10">{translation.readyMoveAbroad}</div>
                                {
                                    places && places.map((item ,i) => {
                                        return  <RelocationAbroadSingle key={i} places={item} />
                                    })
                                }
                            </div> || null
                }
            </div>
        )
    }
}
