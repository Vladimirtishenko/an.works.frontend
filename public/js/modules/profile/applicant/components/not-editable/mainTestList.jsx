import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'

import {Link} from 'react-router-dom'

export default class MainTestList extends React.Component {

    render(){
        return (
            <Card icon="test" title="Тесты">
                <div className="padding--15 padding--md-30 font--12">
                    <span className="font--500">У вас есть непройденные тесты. </span>
                    <span className="font--color-secondary">Пройдите их для повышения рейтинговой оценки</span>
                </div>
                <hr className="hr hr--thin margin--0"/>

                <div className="padding--xc-30 font--12 ">

                </div>

                <table className="responsive-table">
                    <thead className="responsive-table__thead shadow--small">
                    <tr className="responsive-table__tr">
                        <th className="responsive-table__th responsive-table--tests font--left width--25">Название</th>
                        <th className="responsive-table__th responsive-table--tests font--center">Приоритет</th>
                        <th className="responsive-table__th responsive-table--tests font--center">Время</th>
                        <th className="responsive-table__th responsive-table--tests font--center">Количество <br/> вопросов</th>
                        <th className="responsive-table__th responsive-table--tests width--px-90 font--right">Детали</th>
                    </tr>
                    </thead>
                    <tbody className="responsive-table__tbody">
                    <tr className="responsive-table__tr font--color-secondary">
                        <td data-title="Название" className="responsive-table__td responsive-table--tests responsive-table--available">PHP</td>
                        <td data-title="Приоритет" className="responsive-table__td responsive-table--tests font--center"> 1 </td>
                        <td data-title="Время" className="responsive-table__td responsive-table--tests font--12 font--center">00:20</td>
                        <td data-title="Количество вопросов" className="responsive-table__td responsive-table--tests font--12 font--center">100</td>
                        <td className="responsive-table__td responsive-table--tests font--10 font--color-secondary font--right">
                            <span className="icon icon--delete link link--grey"></span>
                        </td>
                    </tr>
                    <tr className="responsive-table__tr font--color-secondary">
                        <td data-title="Название" className="responsive-table__td responsive-table--tests responsive-table--available">Node.js</td>
                        <td data-title="Приоритет" className="responsive-table__td responsive-table--tests font--center"> 3 </td>
                        <td data-title="Время" className="responsive-table__td responsive-table--tests font--12 font--center">00:20</td>
                        <td data-title="Количество вопросов" className="responsive-table__td responsive-table--tests font--12 font--center">88</td>
                        <td className="responsive-table__td responsive-table--tests font--10 font--color-secondary font--right">
                            <span className="icon icon--delete link link--grey"></span>
                        </td>
                    </tr>
                    <tr className="responsive-table__tr font--color-secondary">
                        <td data-title="Название" className="responsive-table__td responsive-table--tests responsive-table--available">Аглийский</td>
                        <td data-title="Приоритет" className="responsive-table__td responsive-table--tests font--center"> 2 </td>
                        <td data-title="Время" className="responsive-table__td responsive-table--tests font--12 font--center">00:20</td>
                        <td data-title="Количество вопросов" className="responsive-table__td responsive-table--tests font--12 font--center">77</td>
                        <td className="responsive-table__td responsive-table--tests font--10 font--color-secondary font--right">
                            <span className="icon icon--delete link link--grey"></span>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colSpan="5" className="padding--t-20 responsive-table--tests">
                            <Link to="/tests" className="padding--r-20 link link--grey"> Ко всем тестам </Link>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </Card>
        )
    }
}
