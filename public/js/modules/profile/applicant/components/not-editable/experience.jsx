import React from 'react'

import StaticItem from '../../../../../components/form/staticItem.jsx';
import {Card} from '../../../common/components/card.dumb.jsx'

import {skillsFromExperienceMapper, mapperSkillsLabel} from '../../../../../helpers/mapperSkills.helper.js'
import {createDatebyFormatMonthYear, valueOfFormatForSorting } from '../../../../../helpers/date.helper.js'

import Inheritance from '../../../common/components/parent/main.jsx'

export default class Experience extends Inheritance {
    constructor(props){
        super(props);
        this.state = {}
    }
    experienceIT(item, i){
        const {location:{country , city} = {},  date: { start = [], end = [], now }, skills: listSkills, mainSkill, position, remote, company, description, name } = item,
              { translations, knowledges } = this.props,
                skills = skillsFromExperienceMapper(mainSkill, listSkills),
                joinSkills = mapperSkillsLabel(skills, knowledges, [{isMain: true}, {isMain: false}]),
                dateStart = createDatebyFormatMonthYear(start, 'MMMM YYYY'),
                dateEnd = createDatebyFormatMonthYear(end, 'MMMM YYYY') || (now ? translations.presenTime : ''),
                key = 'city' + city + country;
        this.citiesUpload(country, city);

        return(
            <div key={i} className="hr--box hr--fullwidth word-break--word">
                <div className="padding--xc-15 padding--md-xc-30">
                    <div className="fl fl--justify-b fl--align-c margin--b-15">
                        <h2 className="font--color-primary font--14 font--400">{position}</h2>
                        <span className="font--12 font--color-secondary nowrap--all">{remote ? translations.freelance : ''}</span>
                    </div>
                    <ul className="list">
                        {
                            joinSkills && joinSkills.map((prop, i) => {
                                return (<li
                                            key={i}
                                            className="font--highlight-grey list__item list__item--inline padding--5 l-h--1 font--14 font--color-primary"
                                        >
                                            {prop.name}
                                        </li>)
                            })
                        }
                    </ul>
                    <ul className="breadcrumbs margin--b-10">
                        {company && <li className="breadcrumbs__item font--color-primary font--14">{company}</li>}
                        <li className="breadcrumbs__item font--color-secondary font--14">{`${dateStart} - ${dateEnd}`}</li>
                        <li className="breadcrumbs__item font--color-secondary font--14">Украина, {this.state[key]}</li>
                    </ul>
                    <p className="font--12 font--color-secondary">
                        {description}
                    </p>
                </div>
            </div>
        )
    }

    experienceOther(item, i){
        const {
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        const { date: { start = [], end = [], now }, position, company, description, location:{country , city} = {}  } = item,
                dateStart = createDatebyFormatMonthYear(start, 'MMMM YYYY'),
                dateEnd = createDatebyFormatMonthYear(end, 'MMMM YYYY') || (now ? translation.presenTime : ''),
                key = 'city' + city + country;
        this.citiesUpload(country, city);
        return (
            <div key={i} className="hr--box hr--fullwidth word-break--word">
                <div className="padding--xc-15 padding--md-xc-30">
                    <div className="fl fl--justify-b fl--align-c margin--b-15">
                        <h2 className="font--color-primary font--14 font--400">{position}</h2>
                        <span className="font--12 font--color-secondary"></span>
                    </div>
                    <ul className="breadcrumbs margin--b-10">
                        <li className="breadcrumbs__item font--color-primary font--14">{company}</li>
                        <li className="breadcrumbs__item font--color-secondary font--14">{`${dateStart} - ${dateEnd}`}</li>
                        <li className="breadcrumbs__item font--color-secondary font--14">Украина, {this.state[key]}</li>
                    </ul>
                    <p className="font--12 font--color-secondary">
                        {description}
                    </p>
                </div>
            </div>
        )
    }

    async citiesUpload(country, city){

        let keyCity = 'city' + city + country;

        if(!country && !city ){
            return
        } else if(this.state[keyCity] != undefined){
            return;
        };

        const db = await (
          await import(`../../../../../databases/countries/${country}/cities.${country}.json`)
        ).default;
            
        let cityNormalize = _.find(db, {label: city} );


        if(cityNormalize){
            this.setState({
                ...this.state,
                [keyCity]: cityNormalize.name
            })
        }
    }

    render(){

        const { experience: experienceList, onEdit, notEdit, i18n:{ translation = {} } = {} } = this.props,
                experience = this.sortByDate([...experienceList]),
                edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};
        return (
            <Card
                icon="exp"
                {...edit}
                linkComponent="experience"
                title="опыт работы"
                cardPadding="padding--15 padding--md-30"
            >
                {
                    experience && experience.map((item, i) => {

                        if(item.type == 'it') {
                            return ::this.experienceIT(item, i)
                        } else {
                            return ::this.experienceOther(item, i)
                        }

                    })
                }
            </Card>
        )
    }
}
