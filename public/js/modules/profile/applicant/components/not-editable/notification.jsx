import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'

class Notification extends React.Component {

    render(){
        return (
            <Card
                icon="notif"
                title="уведомления"
            >
                <div className="info bg--white info--lined">
                    <div className="fl">
                        <div className="info__icon info__icon--lg info__icon--warning">
                            <span className="icon icon--cross"></span>
                        </div>
                        <div className="info__content padding--20">
                            Вы не прошли тест по JavaScript. Вы сможете повторно пройти тест 25.10.2018
                            <div className="font--12 font--color-secondary">25.09.2018</div>
                        </div>
                    </div>
                </div>
                <div className="info bg--white info--lined">
                    <div className="fl">
                        <div className="info__icon info__icon--lg info__icon--success">
                            <span className="icon icon--check-mark"></span>
                        </div>
                        <div className="info__content padding--20">
                            Ваши данные рассмотрены
                            <div className="font--12 font--color-secondary">25.09.2018</div>
                        </div>
                    </div>
                </div>
                <div className="info bg--white info--lined">
                    <div className="fl">
                        <div className="info__icon info__icon--lg info__icon--information">
                            <span className="icon icon--alarm"></span>
                        </div>
                        <div className="info__content padding--20">
                            Вы успешно зарегестрировались в проектеs
                            <div className="font--12 font--color-secondary">25.09.2018</div>
                        </div>
                    </div>
                </div>
                <div className="link padding--15 padding--md-30 font--12">Смотреть всё</div>
            </Card>
        )
    }

}

export default Notification;
