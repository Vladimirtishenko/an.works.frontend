import React from 'react'

import StaticItem from '../../../../../components/form/staticItem.jsx';

import {Card} from '../../../common/components/card.dumb.jsx'

import levelDictionary from '../../../../../databases/general/level.json'
import positionDictionary from '../../../../../databases/general/position.json' 

import {extendsSingleSkill} from '../../../../../helpers/mapperSkills.helper.js' 

export default class Skills extends React.Component {

    levelDictionaryMapper(label){

        let data = extendsSingleSkill(label, levelDictionary);

        return data && data.name || '';

    }

    positionDictionaryMapper(label){

        let data = extendsSingleSkill(label, positionDictionary);

        return data && data.name || '';

    }

    render() {

        let {i18n:{translation = {}} , skills, mainSkill:{name = ''}, positionTitle = '', positionLevel = '',onEdit,notEdit} = this.props,
            role = this.levelDictionaryMapper(positionLevel) + ' ' + name + ' '+  this.positionDictionaryMapper(positionTitle),
            edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};

        return(
            <Card
                icon="details"
                {...edit}
                linkComponent="technicalSkills"
                title={translation.details}
                cardPadding="padding--15 padding--md-30"
            >
                <div className="row">
                    <div className="col-sm-5 margin--b-15 margin--sm-bottom-0">
                        <div className="font--capitalized font--color-inactive  font--12 margin--b-15">{translation.workPosition}</div>
                        <StaticItem
                            staticClass="font--14 font--color-primary"
                            staticText={role} 
                        />

                    </div>
                    <div className="col-sm-7">
                        <div className="font--capitalized font--color-inactive  font--12 margin--b-15">{skills.length ? translation.skills : ''}</div>
                        <ul className="list margin--b-minus-10">
                            {
                                skills && skills.map((item, i) => {
                                    return <li key={i} className="font--highlight-grey list__item list__item--inline padding--10 font--color-primary l-h--1 font--14">{item.name}</li>
                                })
                            }
                        </ul>
                    </div>
                </div>
            </Card>
        )
    }
}
