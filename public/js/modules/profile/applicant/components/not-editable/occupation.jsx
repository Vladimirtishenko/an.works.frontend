import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";

import occupationVocabularies from '../../../../../databases/general/occupation.json'  

export default class EmploymentsType extends React.Component {
    setOccupationList(occupation) {
        if (!occupation) return "";

        let occupationList = [];

        _.forEach(occupationVocabularies, (item, i) => { 
            if(occupation[item.label]) {
                occupationList.push(item.sub);
            }   
        })
        return occupationList.join(', ');
    }

    render() {
        let { occupation, onEdit,notEdit, i18n:{translation={}}} = this.props,
            occupationList = ::this.setOccupationList(occupation);
        const edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};
        return (
            <Card
                icon="type_of_employment"
                {...edit}
                linkComponent="type"
                title={translation.employmentType}
                cardPadding="padding--15 padding--md-30"
            >
                <p className="font--14 font--color-primary">{occupationList}</p>
            </Card>
        );
    }
}
