import React from 'react'
import StaticItem from '../../../../../components/form/staticItem.jsx'

import countries from '../../../../../databases/general/countries.json';
import {normalizeCountryAndCities, normalizeCity} from '../../../../../helpers/locationMapper.helper.js'
export default class Location extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            country: '',
            city: ''
        }
    }

    componentDidMount(){

        let {geography} = this.props,
            {country, city} = geography || undefined,
            countryIndex = country ? _.findIndex(countries, {'label': country} ) : null,
            countryName = countryIndex > -1 ? countries[countryIndex] : {};

        if(country){
            this.citiesUpload(countryName, city);
        }

    }
    componentDidUpdate(prevProps){

        const {geography:{country : countryCurrent, city: citiesCurrent}} = this.props,
            { geography:{country: countryPrev , city: citiesPrev } } = prevProps,
            { db } = this.state;

        if(citiesCurrent !== citiesPrev || countryCurrent !== countryPrev){
            
            const cities = citiesCurrent && normalizeCity(citiesCurrent, db);

            if(!cities) return;

            this.setState((prevState) => ({
                ...prevState,
                ...cities
            }))

        }

    }

    async citiesUpload(country, city){

        const db = await (
          await import(`../../../../../databases/countries/${country.label}/cities.${country.label}.json`)
        ).default;

        let citiesList = normalizeCity(city, db);

        if(citiesList){
            this.setState({
                ...citiesList,
                country: country.name,
                db
            })
        }
 

    }

    render (){

        let { country: countrySingle, city } = this.state,
            address = countrySingle + ' (' + city + ')';
        const { i18n:{translation={}} = {} } = this.props;

        return (
            <div className="col-md-4 margin--b-15 margin--md-bottom-0">
                <div className="font--color-inactive font--12 margin--b-10">{translation.placeOfResidence}</div>
                <StaticItem
                    staticClass="font--14 font--color-primary"
                    staticText={address}
                />
            </div>
        )
    }
}
