import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'
import StaticItem from '../../../../../components/form/staticItem.jsx'

import Location from './location.jsx'
import RelocationLocal from './relocationLocal.jsx'
import RelocationAbroad from './relocationAbroad.jsx'
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
 
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class Geography extends React.Component {
    render(){

        let {geography,onEdit,notEdit, i18n:{translation={}} = {}} = this.props;
        const edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};
        return (
            <Card
                icon="geagraphy"
                {...edit}
                linkComponent="geography"
                title={translation.location}
                cardPadding="padding--15 padding--md-30"
            >
                <div className="row">
                    <Location i18n={this.props.i18n} geography={geography} /> 
                    <div className="col-md-4">
                        <RelocationLocal i18n={this.props.i18n} geography={geography} /> 
                        <RelocationAbroad i18n={this.props.i18n} geography={geography} />
                    </div>
                </div>
            </Card>
        )
    }
}
