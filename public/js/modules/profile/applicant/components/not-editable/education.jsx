import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'

import {createDatebyFormatMonthYear} from '../../../../../helpers/date.helper.js'
import Inheritance from '../../../common/components/parent/main.jsx'

export default class EducationNotEditable extends Inheritance {
    constructor(props){
        super(props);

        this.state = {
            ...props.location,
            globalUniversityList: []
        }
    }
    university(item, i){

        const { date: { start = [], end = [], now = '' }, location:{university = '',country , city} = {} } = item,
              { i18n: { translation = {} } = {}  } = this.props,
              dateStart = createDatebyFormatMonthYear(start, 'MMMM YYYY'),
              dateEnd = createDatebyFormatMonthYear(end, 'MMMM YYYY') || (now ? translation.stillStudying || '' : ''),
              universities = this.checkNameUniversity(university),
              key = 'city' + city + country;
              
        this.citiesUpload(country, city);

        return (
            <div key={i} className="hr--box hr--fullwidth word-break--word">
                <div className="padding--xc-15 padding--md-xc-30">
                    <ul className="breadcrumbs margin--b-10">
                        <li className="breadcrumbs__item font--color-primary font--14">{universities}</li>
                        <li className="breadcrumbs__item font--color-secondary font--14">
                            {`${dateStart} - ${dateEnd}`}
                        </li>
                    </ul>
                    <p className="font--12 font--color-primary">
                        Украина, {this.state[key]}, {item.degree}, {item.specialization}
                    </p>
                </div>
            </div>
        )
    }
    async getUniversityList(item){

        const db = await (
          await import(`../../../../../databases/countries/${item.label}/universities.${item.label}.json`)
        ).default;
        this.setState( {...this.state, globalUniversityList: db } )
    }

    async citiesUpload(country, city){

        let keyCity = 'city' + city + country;

        if(!country && !city ){
            return
        } else if(this.state[keyCity] != undefined){
            return;
        };

        const db = await (
          await import(`../../../../../databases/countries/${country}/cities.${country}.json`)
        ).default;
            
        let cityNormalize = _.find(db, {label: city} );


        if(cityNormalize){
            this.setState({
                ...this.state,
                [keyCity]: cityNormalize.name
            })
        }
    }
    
    componentDidMount(){
        this.getUniversityList({label: 2});
    }
    checkNameUniversity(number){
        const {globalUniversityList} = this.state;
        let university = '';
        globalUniversityList.forEach((item, i) =>{
            if(item.label == number){
                university = item.name;
            }
         }
        );
        return university;
    }
    sertificate(item, i){
        const { date: { start = [], end = [], now = '' } } = item,
              { i18n: { translation = {} } = {} } = this.props,
              dateStart = createDatebyFormatMonthYear(start, 'MMMM YYYY'),
              dateEnd = createDatebyFormatMonthYear(end, 'MMMM YYYY') || (now ? translation.certificationNotExpire || '' : '');
        return (
            <div key={i} className="hr--box hr--fullwidth word-break--word">
                <div className="padding--xc-15 padding--md-xc-30">
                    <ul className="breadcrumbs margin--b-10">
                        <li className="breadcrumbs__item font--color-primary font--14">{item.organisation}</li>
                        <li className="breadcrumbs__item font--color-secondary font--14">
                            {`${dateStart} - ${dateEnd}`}
                        </li>
                    </ul>
                    <p className="font--12 font--color-primary">
                        {`${item.name}, ${item.number} ${item.mark ? ','+ item.mark : ''}`}
                    </p>
                </div>
            </div>
        )
    }

    render () {
        let {i18n: { translation = {} } = {}, education: educationList, onEdit, notEdit } = this.props,
              education = this.sortByDate([...educationList]),
              edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};

        return (
            <Card
                icon="education"
                {...edit}
                linkComponent="education"
                title={translation.education}
                cardPadding="padding--15 padding--md-30"
            >
            {
                education.length && education.map((item, i) => {

                    if(item.type == 'university') {
                        return ::this.university(item, i)
                    } else {
                        return ::this.sertificate(item, i)
                    }

                }) || null
            }
            </Card>
        )
    }
}
