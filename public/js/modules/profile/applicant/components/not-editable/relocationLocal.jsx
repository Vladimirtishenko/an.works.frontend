import React from 'react'
import StaticItem from '../../../../../components/form/staticItem.jsx'

import {normalizeCountryAndCities, normalizeCity} from '../../../../../helpers/locationMapper.helper.js'

// Inheritance
import Inheritance from '../../../common/components/parent/main.jsx'

export default class RelocationLocal extends Inheritance {

    constructor(props){
        super(props);
        this.state = {
            country: '',
            city: []
        }
    }

    componentDidMount(){

        let { geography: { relocation: { local }, country } } = this.props;

        if(!local || !country) return;

        let obj = normalizeCountryAndCities(country, local.cities);

        if(obj){
            this.citiesUpload(obj.country, obj.cities);
        }

    }

    componentDidUpdate(prevProps){

        const { geography: { relocation: { local: { cities: citiesCurrent } }, country } } = this.props,
              { geography: { relocation: { local: { cities: citiesPrev } } } } = prevProps,
              { db } = this.state;

        if(citiesCurrent !== citiesPrev){

            const cities = citiesCurrent && normalizeCity(citiesCurrent, db);

            if(!cities) return;

            this.setState((prevState) => ({
                ...prevState,
                ...cities
            }))

        }

    }

    async citiesUpload(country, cities){

        const db = await (
          await import(`../../../../../databases/countries/${country.label}/cities.${country.label}.json`)
        ).default;

        let citiesList = normalizeCity(cities, db);

        if(citiesList){
            this.setState({
                ...citiesList,
                country: country.name,
                db
            })
        }

    }

    render(){

        let { geography: { relocation: { local: { type } = {} } = {}, country } = {},i18n:{translation={}} = {} } = this.props,
            { country: countrySingle, city } = this.state,
            mergedLocation = countrySingle + ' (' + city + ')';

        return (
            <div>
                {
                    type && <div>
                                <div className="font--color-inactive  font--12 margin--b-10">{translation.readyRelocate}</div>
                                <StaticItem
                                    wrapperClass="margin--b-15"
                                    staticClass="font--14 form__static-input"
                                    staticText={mergedLocation}
                                />
                            </div> || null
                }
            </div>
        )
    }

}
