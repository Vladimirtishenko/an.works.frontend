import React from 'react'
import StaticItem from '../../../../../components/form/staticItem.jsx'

import {normalizeCountryAndCities, normalizeCity} from '../../../../../helpers/locationMapper.helper.js'

// Inheritance
import Inheritance from '../../../common/components/parent/main.jsx'

export default class RelocationAbroadSingle extends Inheritance {

    constructor(props){
        super(props);
        this.state = {
            country: '',
            city: ''
        }
    }

    componentDidMount(){

        let {places: {cities, country}} = this.props;

        if(!cities && !country) return;

        let obj = normalizeCountryAndCities(country, cities);

        if(obj){
            this.citiesUpload(obj.country, obj.cities);
        }

    }

    async citiesUpload(country, cities){

        const db = await (
          await import(`../../../../../databases/countries/${country.label}/cities.${country.label}.json`)
        ).default;

        let citiesList = normalizeCity(cities, db);

        if(citiesList){
            this.setState({
                ...citiesList,
                country: country.name
            })
        }


    }

    render (){

        let {country, city} = this.state,
            mergedLocation = country + ' (' + city + ')';

        return (
            <StaticItem
                wrapperClass="margin--b-15"
                staticClass="font--14 font--color-primary"
                staticText={mergedLocation}
            />
        )
    }
}
