import React from 'react'

import StaticItem from '../../../../../components/form/staticItem.jsx';

import {Card} from '../../../common/components/card.dumb.jsx'

import languageSkills from '../../../../../databases/languages/languageSkills.json'

export default class Language extends React.Component {

    getDataFromValueList(value){

        let name = '';

        languageSkills.forEach((item, i) => {
            if(item.label == value){
                name = item.name;
            }
        });

        return name;

    }

    render() {

        let {skills,onEdit,notEdit, i18n:{translation = {}} = {}} = this.props,
        edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};

        return(
            <Card
                icon="lang"
                {...edit}
                linkComponent="languageSkills"
                title={translation.languages}
                cardPadding="padding--15 padding--md-30"
            >
                <div className="row">
                    {
                        skills && skills.map((item, i) => {
                            return <div key={i} className="col-6 col-md-3 margin--b-15 margin--md-bottom-0">
                                        <StaticItem
                                            staticClass="font--14 form__static-input"
                                            staticText={item.name}
                                        />
                                    <div className="form__label font--capitalized font--14 margin--t-15">{this.getDataFromValueList(item.value || 0)}</div>
                                    </div>
                        })
                    }
                </div>
            </Card>
        )
    }
}
