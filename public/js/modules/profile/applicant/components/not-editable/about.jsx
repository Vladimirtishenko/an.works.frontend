import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'


export default class About extends React.Component {

    render() {

        const {about:{
                about = "",
                waitings = "",
                achivments = "",
                hobby = "",
                }, 
                onEdit, 
                notEdit,
                i18n:{
                    translation = {}
                }
            } = this.props || {},
        edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};
        return (
            <Card
                icon="about_me"
                {...edit}
                linkComponent="about"
                title={translation.about}
                cardPadding="padding--15 padding--md-30"
            >
            {
                about && (
                    <div className="hr--box hr--fullwidth word-break--word">
                        <div className="padding--xc-15 padding--md-xc-30">
                            <div className="font--capitalized font--color-inactive  font--12 margin--b-10">{translation.about}</div>
                            <p className="font--14">
                                {about}
                            </p>
                        </div>
                    </div>
                )
            }
                
            {
              waitings &&  (
                    <div className="hr--box hr--fullwidth word-break--word">
                        <div className="padding--xc-15 padding--md-xc-30">
                            <div className="font--capitalized font--color-inactive  font--12 margin--b-10">{translation.workExpectations}</div>
                            <p className="font--14">
                                {waitings}
                            </p>
                        </div>
                    </div>)
            }
            {
                achivments && (
                    <div className="hr--box hr--fullwidth word-break--word">
                        <div className="padding--xc-15 padding--md-xc-30">
                            <div className="font--capitalized font--color-inactive  font--12 margin--b-10">{translation.achievements}</div>
                            <p className="font--14">
                                {achivments}
                            </p>
                        </div>
                    </div>
                )
            }
            {
                hobby && (
                    <div className="hr--box hr--fullwidth word-break--word">
                        <div className="padding--xc-15 padding--md-xc-30">
                            <div className="font--capitalized font--color-inactive  font--12 margin--b-10">{translation.hobby}</div>
                            <p className="font--14">
                                {hobby}
                            </p>
                        </div>
                    </div>
                )
            }
            </Card>
        )
    }

}
