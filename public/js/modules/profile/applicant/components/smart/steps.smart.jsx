import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Loadable from 'react-loadable';
import Loading from '../../../../../components/widgets/loader.jsx'

class Steps extends React.Component {

  constructor(props){
    super(props);

    this.state = {
        component: null
    }

  }

  componentDidMount(){

      let { applicant: { profile: { direction } } } = this.props,
          { component = {} } = this.state;

      if( !component || !component[direction] ){
          this.setState({
              component: {
                  [direction]: this.setAsyncComponent(direction)
              }
          })
      }
  }

  setAsyncComponent(step){

      return Loadable({
        loader: () => import(`../steps/${step}.jsx`),
        loading: Loading,
        delay: 500,
        timeout: 10000
      })
  }

  componentDidUpdate(prevProps, prevState){

      const { applicant: { profile: { direction: currentDirection = null } } } = this.props,
            { applicant: { profile: { direction: previousDirection = null } } } = prevProps,
            { component } = this.state;

      if(currentDirection != previousDirection && !component[currentDirection]){
          this.setState({
              component: {...this.state.component, [currentDirection]: this.setAsyncComponent(currentDirection)}
          })
      }

  }

  componentWillUnmount(){
      this.setState({
        component: null
      });
  }

  render(){

    let { component } = this.state,
        { applicant: { profile: { direction = null } } } = this.props;

    if(!component || !direction || !component[direction]) return null;

    let Component = component[direction];

    return ( <Component {...this.props} /> );

  }

}

export default Steps;
