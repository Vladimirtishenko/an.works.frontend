import React from 'react'

import {stepsSchema} from '../../schema/steps.sc.js'
import {Link} from "react-router-dom";


class TopProgress extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            step: props.applicant && props.applicant.profile && props.applicant.profile.step,
            direction: props.applicant && props.applicant.profile && props.applicant.profile.direction,
        }
    }

    stepsRunner(){

        return stepsSchema(this.props.i18n.translation);
    }

    componentDidUpdate(prevProps, prevState){

        let profilePrev = prevProps.applicant.profile,
            profileNext = this.props.applicant.profile,
            comparing = _.isEqual(profilePrev, profileNext);

            if(!comparing) {
    			this.setState({
                    step: profileNext.step,
                    direction: profileNext.direction
                })
    		}

    }

    render() {

        let steps = this.stepsRunner(),
            length = steps.length - 1;

        return (
            <div className="progress-wrpapper-steps">
                <div className="scroll--x-auto padding--yc-5">
                    <div className="progress-steps progress-steps--sm margin--b-20">
                        {steps.map((item, i) => {

                            let passed, active = '',
                                last = i == length ? 'progress-steps__item--last' : '';

                            if(this.state.step == item.id){
                                active = 'progress-steps__item--is-active';
                            }

                            if(this.state.step > item.id){
                                passed = 'progress-steps__item--is-complete';
                            }

                            return (
                                <Link to={item.path} key={i} className={`progress-steps__item ${passed} ${active} ${last}`} data-step={item.id}>
                                    <span className="progress-steps__line"></span>
                                    <span className="progress-steps__link">{item.name}</span>
                                </Link>
                            )

                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default TopProgress;
