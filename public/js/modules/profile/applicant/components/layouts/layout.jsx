import React from "react";
import Left from "./leftAside.jsx";
import ProgressProfileSidebar from "./right-sidebars/rightAside.jsx";
import Top from "./topBarSection.jsx";

import ExperienceSideBar from "./right-sidebars/experienceSideBar.jsx";

const sidebars = {
    progressProfile: ProgressProfileSidebar,
    experience: ExperienceSideBar
};

class Layout extends React.Component {
    render() {
        let { progressBar, rightComponentName } = this.props;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-2  padding--0 position_static z--1001">
                        <Left {...this.props} />
                    </div>
                    <div className="col-md-9 col-lg-8">
                        {(progressBar && <Top {...this.props} />) || null}
                        {this.props.children}
                    </div>
                    <div className="col-md-3 col-lg-2 padding--md-0">
                        <aside className="fl fl--dir-col fl--align-c show--md-fl padding--t-50">
                            {/* <a className="link font--color-blue font--12  margin--b-25 margin--auto-xc margin--lg-minus-xc-15 margin--xl-auto-xc fl fl--align-c">
                            <span className="icon--eye font--20 padding--r-10"></span>Моё резюме</a> */}
                            {rightComponentName &&
                                rightComponentName.map((item, i) => {
                                    if (sidebars[item]) {
                                        let Component = sidebars[item];
                                        return (
                                            <Component
                                                key={i}
                                                {...this.props}
                                            />
                                        );
                                    }
                                })}
                        </aside>
                    </div>
                </div>
            </div>
        );
    }
}

export default Layout;
