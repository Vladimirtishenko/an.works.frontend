import React from "react";
import Left from "./leftAside.jsx";
import ProgressProfileSidebar from "./right-sidebars/rightAside.jsx";
import { Link } from "react-router-dom";
import ExperienceSideBar from "./right-sidebars/experienceSideBar.jsx";
import RightAsideStatus from "./right-sidebars/rightAsideStatus.jsx";
import RightCurcleProgress from "./right-sidebars/rightAside.jsx";
import {statusUser} from "../../../../../helpers/statusUser.helper.js";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";
import * as actionHints from "../../../common/actions/hint.action.js";
import * as applicantActions from "./../../../applicant/actions/profile.action.js";
import Hint from "../../../common/components/hint/hint.jsx";
const sidebars = {
    progressProfile: ProgressProfileSidebar,
    experience: ExperienceSideBar
};
function mapStateToProps(state) {
    return {
        i18n: state.i18n,
        hints: state.hints
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...i18n,
                ...applicantActions,
                ...actionHints
            },
            dispatch
        ),
        dispatch
    };
}
@connect(
    mapStateToProps,
    mapDispatchToProps
)
class Layout extends React.Component {
    static defaultProps = {
        mainClass: "col-md-9 col-lg-8 fl--order-1",
        rightAside: true
    };



    render() {
        let {
            actions:{
                updateHintLayoutRating
            },
            applicant:{
                profile:{ 
                    status 
                } = {}
            } = {},
            mainClass,
            rightAside,
            rightComponentName,
            i18n:{
                translation = {}
            } = {},
            hints:{
                hintProfileRating
            }
        } = this.props;
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-2 padding--0 position_static">
                            <Left {...this.props} />
                        </div>
                        <div className={mainClass}>{this.props.children}</div>
                        {(rightAside && (
                            <div className="col-md-3 col-lg-2 padding--md-0 fl--order-md-2">
                                <aside className="fl fl--dir-col fl--align-c">                               
                                    <Link to="/cvu" className="link font--color-blue font--12  margin--b-25 margin--auto-xc margin--lg-minus-xc-15 margin--xl-auto-xc fl fl--align-c">
                                        <span className="icon--eye font--20 padding--r-10"></span>
                                        {translation.myСV}
                                    </Link>
                                    <RightAsideStatus {...this.props}/>
                                    {
                                        rightComponentName && rightComponentName.map((item, i) => {
                                            if(sidebars[item]){
                                                let Component = sidebars[item];
                                                return (
                                                    <Component
                                                        key={i}
                                                        {...this.props}
                                                    />
                                                );
                                            }
                                        }) 
                                    }
                                    {statusUser(status) &&<RightCurcleProgress/>  || null}
                                </aside>
                            </div>
                        )) || null}
                    </div>
                </div>
                {rightAside && hintProfileRating && <Hint closeHint={()=>{updateHintLayoutRating(false)}} />}
            </React.Fragment>            
        );
    }
}

export default Layout;
