import React from 'react'
import {Link} from 'react-router-dom'
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n,
        hints: state.hints
    };
}

@connect(
    mapStateToProps,
    { ...i18n }
)
class ParticipateRating extends React.Component {
    render() {
        const {
            i18n:{
                translation = {}
            } = {},
            hints:{
                hintProfileRating
            }
        } = this.props;
        
        return (
            <div className={`box--rounded box--white shadow--box padding--xc-15 padding--yc-20 margin--b-20 width--100 ${hintProfileRating ? 'hint--block-active':''}`}>
                <p className="font--14 font--color-primary margin--b-20">{translation.ranking}</p>
                <p className="font--12 font--color-secondary margin--b-20">
                    {translation.canStartRanking}
                </p>
                <Link to="/rating" className="display--b">
                    <div className="font--color-white font--center btn--theme-blue width--100 height--px-40 l-h--px-35 font--12 font--500">{translation.start}</div>
                </Link>
            </div>
        )
    }
}

export default ParticipateRating;