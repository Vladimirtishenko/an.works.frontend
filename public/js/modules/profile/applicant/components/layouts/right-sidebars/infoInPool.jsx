import React from 'react'
import {Link} from 'react-router-dom'
import  configuration from '../../../../../../configuration/rules.json';
import {
    transformToDays
    } from '../../../../../../helpers/date.helper.js';
import {
    mapperNameMainSckill
    } from '../../../../../../helpers/mapperSkills.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}

@connect(
    mapStateToProps,
    { ...i18n }
)
class InfoInPool extends React.Component {
    
    constructor(props){
        super(props);
        this.positionOpenCircle = ["173, 195","199, 157","205, 115","194, 75","167, 44","127, 25"];
        this.positionCircl = ["71px, 26.8px","69, 26.8","62, 29.8","57.2, 31.2","55.2, 32.2","47.2, 35.2","41.2, 39.2","38.2, 41.2","34.2, 43.2","30.2, 47.2","29.2, 48.2","26.5, 50.2","23.2, 53.2","22.5, 54.2","18.5, 58.2","18, 58.6","16.2, 62.2","14.4, 63.2","9.5, 70.7","7.5, 74.2","4.5, 78.2","2.5, 81.7","1.5, 85.4","-0.5, 90","-1.5, 93","-2.3, 97","-3.3, 101","-4.3, 105","-4.1, 109","-4.4, 113","-5, 117","-5, 121","-4.8, 125","-4.5, 129","-4.4, 133","-4, 135","-3.4, 139","-2.5, 144","-1.6, 147","-0.6, 151","0.8, 155","2.6, 159","3.7, 162","5.8, 166","8.3, 170","10, 173","12, 177","14, 180","17, 184","20, 187","24, 191","26, 195"];
    }

    render() {
        const { rank = '', expiredDate = '', openCount = '', knowledges = [] , mainSkill = '',i18n:{translation = {}} = {}} = this.props,
        date = configuration.daysOfPool - transformToDays(expiredDate)  == -1 ? 1 : configuration.daysOfPool - transformToDays(expiredDate),
        sckills = mapperNameMainSckill(knowledges, mainSkill);
        let openCircl = (135 + (((350 - 170)  / 4) * openCount)),
            positionCircl = rank ? rank > 50 ? 0 : (500 + (rank == 1 ? 0 * 4 : rank * 4)) : 700,
            dayRankingCircle = (350 / 30) * date;
        return (
            <div className="box--rounded box--white shadow--box font--center margin--b-20 width--100">
                <div className='border--b-1-gray-block padding--xc-15  padding--yc-15'>
                <p className="font--center">
                        <span className="font--14 font--color-secondary display--b ">{translation.ranking}</span>
                        <span className="font--18 font--color-primary font--500 display--b">{sckills}</span>
                    </p>
                </div>
                <div className="in-pool padding--t-20">
                    <div className="fl fl--justify-c">
                        <span className="font--14 font--color-secondary margin--r-25 margin---md-r-10">{translation.position}</span>
                        <span className="font--14 font--color-secondary margin--l-25 margin--md-l-10">{translation.openings}</span>
                    </div>
                    <div className="in-pool__wrap-svg">
                    <span className="in-pool__day">
                        {translation.days} <br/> {translation.inPool}
                        <br/>
                        {date}
                    </span>
                        <svg  xmlns="http://www.w3.org/2000/svg" height="100%" width="100%" viewBox="-25 0 250 250">
                            {/* stroke-dashoffset="350" */}
                            {/* stroke-dashoffset="0" */}
                            <path 
                                d="M 59 167.5 a 62 62 0 1 1 80.93 0"
                                className="in-pool--path-shadow" 
                                strokeLinecap="round"
                                fill="none"
                            />
                            <path
                                d="M41 169.5a77 77 0 1 1 117.93 0"
                                className="in-pool__bg"
                                stroke="#F5F7FA"
                                strokeLinecap="round"
                                fill="none"
                            />
                            <path
                                d="M41 169.5a77 77 0 1 1 117.93 0"
                                className="in-pool__meter"
                                style={{stroke: "url(#MyGradient)"}}
                                fill="none"
                                strokeDasharray="350"
                                strokeLinecap="round"
                                strokeDashoffset={dayRankingCircle}
                            />

                            {/* strokeDashoffset="700" */}
                            {/* strokeDashoffset="500" */}
                            <path
                                d="M 60 183 a 100 98 0 0 1 20 -172" 
                                className="in-pool__left-line in-pool__background"
                                strokeWidth="15"
                                strokeDasharray="350"
                                fill="none"
                                strokeDashoffset="0"
                                strokeLinecap="round"
                                />
                            <path
                                d="M 60 183 a 100 98 0 0 1 20 -172"
                                className="in-pool__left-line"
                                style={{stroke: "url(#SVGID_1_)"}}
                                strokeWidth="15"
                                strokeDasharray="350"
                                strokeDashoffset={positionCircl}
                                strokeLinecap="round"
                                fill="none"
                            />
                            {/* strokeDashoffset="135" */}
                            {/* strokeDashoffset="350" */}
                            <path
                                d="M 122 46 a 100 98 0 0 1 20 172"
                                className="in-pool__right-line in-pool__background"
                                strokeWidth="15"
                                strokeDasharray="350"
                                fill="none"
                                strokeDashoffset="0"
                                strokeLinecap="round"
                            />
                            <path
                                d="M 122 46 a 100 98 0 0 1 20 172"
                                className="in-pool__right-line"
                                stroke="#528BE6"
                                strokeWidth="15"
                                strokeDasharray="350"
                                strokeDashoffset={openCircl}
                                strokeLinecap="round"
                                fill="none"
                            />
                            <line x1="192" y1="155" x2="205" y2="161" strokeWidth="3" stroke="#ffffff"/>
                            <line x1="198" y1="118" x2="213" y2="116" strokeWidth="3" stroke="#ffffff" />
                            <line x1="187" y1="78" x2="200" y2="71" strokeWidth="3" stroke="#ffffff"/>
                            <line x1="161" y1="49" x2="170" y2="37" strokeWidth="3" stroke="#ffffff" />
                            <text  x="160" y="220"className="in-pool__limit-text" >5</text>
                            <g transform={`translate(${rank ? rank > 50 ? this.positionCircl[50] : this.positionCircl[rank] : this.positionCircl[50]})`}>
                                <circle className="in-pool__circle-position-open" strokeWidth="1px" r="15"/>
                                <text textAnchor="middle" transform="translate(0, -1)" alignmentBaseline="central">{rank}</text>
                            </g>
                            <g transform={`translate(${this.positionOpenCircle[openCount] ? this.positionOpenCircle[openCount] : '173, 195' })`}>
                                <circle className="in-pool__circle-position-open" r="10"/>
                                <text textAnchor="middle" transform="translate(0, -1)" alignmentBaseline="central">{openCount}</text>
                            </g>
                            <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="38.5486" y1="126.2274" x2="38.5486" y2="1.342">
                                <stop  offset="0.2408" stopColor = "#CBD0D8"/>
                                <stop  offset="0.3665" stopColor = "#B6CCEF"/>
                                <stop  offset="0.6099" stopColor = "#8DC7D5"/>
                                <stop  offset="0.7315" stopColor = "#6CCDCA"/>
                                <stop  offset="0.8731" stopColor = "#4CD49D"/>
                                <stop  offset="1" stopColor = "#6CD44C"/>
                            </linearGradient>
                            <linearGradient id="MyGradient">
                                <stop  offset="0.1639" stopColor = "#6CD543"/>
                                <stop  offset="0.5621" stopColor = "#F6F85D"/>
                                <stop  offset="0.95" stopColor = "#FB661E"/>
                                <stop  offset="1" stopColor = "#FA5064"/>
                            </linearGradient>
                            <text className="in-pool--st4" x="43" y="100" >7</text>
                            <text className="in-pool--st6" x="95" y="68" >15</text>
                            <text className="in-pool--st7" x="145" y="105">23</text>
                            <text className="in-pool--st5" x="132" y="170" >30</text>
                        </svg>
                    </div>
                </div>

                {/* <div className="padding--xc-15  padding--yc-10">
                    <div className="fl fl--justify-b margin--b-10">
                        <p className="font--uppercase font--12 font--color-secondary">
                            {translation.position}
                        </p>
                        <span className="font--16 font--color-primary font--500">{rank || 0}</span>
                    </div>
                    <div className="fl fl--justify-b margin--b-10">
                        <p className="font--uppercase font--12 font--color-secondary">
                            {translation.openings}
                            <span className="font--12 font--color-inactive display--b font--left margin--b-5 font--transform-none font--500">
                                {`${translation.fromAmount} ${configuration.openings}`}
                            </span>
                        </p>
                        <span className="font--16 font--color-primary font--500">{openCount || 0}</span>
                    </div>
                    <div className="fl fl--justify-b margin--b-10">
                        <p className="font--uppercase font--12 font--color-secondary">
                            {translation.daysInRanking}
                            <span className=" font--12 font--color-inactive display--b font--left margin--b-5 font--transform-none font--500">
                                {`${translation.fromAmount} ${configuration.daysOfPool}`}
                            </span>
                        </p>
                        <span className="font--16 font--color-primary font--500">{date || 0}</span>
                    </div>
                </div> */}
                <div className='border--t-1-gray-block font--center padding--t-10 padding--b-15'>
                    <Link to="/rating" className="font--12 font--color-blue line--1 link">
                        {translation.goToRating}
                    </Link>
                </div>
            </div>
        )
    }
}

export default InfoInPool;