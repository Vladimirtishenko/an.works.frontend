import React from "react";

import { connect } from "react-redux";
import CircularProgressBar from "../../progress/progressRound.jsx";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        ...state.profile,
        i18n: state.i18n
    };
}

@connect(
    mapStateToProps,
   { ...i18n }
)
class RightAside extends React.Component {

    searchProgress(need) {
        let activeProfile = 'applicant',
            progress = 0;
        (function findInObject(props) {
            _.forOwn(props, (value, key) => {
                if (need == key) {
                    progress = value;
                }

                if (
                    value &&
                    typeof value == "object" &&
                    Object.keys(value).length > 0
                ) {
                    findInObject(value);
                }
            });
        })(this.props[activeProfile]);

        return progress;
    }

    render() {
        let percentage = this.searchProgress("progress");
        const {
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        return (
            <div className="box--white shadow--box box--rounded padding--15 margin--b-15 show--md width--100">
                <span className="font--12 font--lg-14 font--color-primary">
                    {translation.profileFilled}
                </span>
                <CircularProgressBar
                    colors={["#E54D7C", "#8B4CEA", "#6598F8", "#69D460"]}
                    lineWidth={6}
                    trackWidth={2}
                    startPosition={Math.PI / 2}
                    radius={60}
                    value={percentage}
                />
                <p className="font--12 font--color-secondary">
                    {translation.ruleProfileFilled}
                </p>
            </div>
        );
    }
}

export default RightAside;
