import React from 'react'
import { string } from 'prop-types'
import { connect } from "react-redux";
import {pluralizeYear,pluralizeMonth} from '../../../../../../helpers/pluralize.helper.js';
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        i18n: state.i18n
    };
}

@connect(
    mapStateToProps,
    { ...i18n }
)
class ExperienceSideBar extends React.Component {

    static propTypes = {

    }

    static defaultProps = {

    }

    constructor(props){
        super(props);
    }

    monthToYear(value){

        const year = parseInt(value / 12),
              month = value % 12;

        return {year, month};

    }

    render(){

        const { 
            applicant: { 
                profile: {
                    experience: { 
                        it = 0, 
                        other = 0 
                    } = {} 
                } 
            },
            i18n:{
                translation = {}
            } = {}
            } = this.props,
              { month: monthIT, year: yearIT } = this.monthToYear(it),
              { month: monthOther, year: yearOther } = this.monthToYear(other);

        return (
           <React.Fragment>
               <div className="fl fl--align-c fl--justify-b display--md-b box--rounded box--white shadow--box padding--xc-15 padding--yc-20 margin--b-20 border--b-4-blue width--100">
                    <p className="font--10 font--color-secondary margin--md-b-10 font--uppercase">{translation.experienceInIT}</p>
                    <p className="font--20 font--md-24 font--color-primary font--500">
                        {yearIT} <span className="margin--r-10 font--12">{pluralizeYear(yearIT)}</span>
                        {monthIT} <span className="margin--r-10 font--12">{pluralizeMonth(monthIT)}</span>
                    </p>
                </div>
                <div className="fl fl--align-c fl--justify-b display--md-b box--rounded box--white shadow--box padding--xc-15 padding--yc-20 margin--b-20 border--b-4-gray width--100">
                    <p className="font--10 font--color-secondary margin--md-b-10 font--uppercase">{translation.experienceInOther}</p>
                    <p className="font--20 font--md-24 font--color-primary font--500">
                        {yearOther} <span className="margin--r-10 font--12">{pluralizeYear(yearOther)}</span>
                        {monthOther} <span className="margin--r-10 font--12">{pluralizeMonth(monthOther)}</span>
                    </p>
                </div>
           </React.Fragment>


        )
    }
}

export default ExperienceSideBar;
