import React from 'react'
import { pluralizeDays } from '../../../../../../helpers/pluralize.helper.js'
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}

@connect(
    mapStateToProps,
    { ...i18n }
)
class FillProfile extends React.Component {
 
    render() {
        
        const {inPoolBanExpiredDate, i18n:{translation = {}} = {}} = this.props,
            days = pluralizeDays(inPoolBanExpiredDate),
            daysText = inPoolBanExpiredDate && inPoolBanExpiredDate <= -0 ? 'onWaitingLIstToday' : inPoolBanExpiredDate == 1 ? 'onWaitingListTomorrow' : 'onWaitingList',
            daysShow = daysText === 'onWaitingList' ? true : false;
        return (
            <div className="box--rounded box--white shadow--box padding--xc-15 padding--yc-20 margin--b-20 width--100">
                <p className="font--12 font--color-primary margin--b-10 nowrap--pre-wrap">{translation[daysText]}</p>
                {/* <p className="font--12 font--color-secondary margin--b-10">
                    до конца пребывания в списке ожидания:
                </p> */}
                {
                    daysShow && (
                        <p className="font--18 font--700 font--color-primary l-h--px-35">{
                            inPoolBanExpiredDate && days
                            ||
                            <span className="font--10 font--color-failed">Произошла ошибка, свяжитесь с техподдержкой, для выяснения данной проблемы</span>
                        }</p>
                    ) || null
                }
                
            </div>
        )
    }
}

export default FillProfile;