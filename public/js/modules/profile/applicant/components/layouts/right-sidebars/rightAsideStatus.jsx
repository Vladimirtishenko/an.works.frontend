import React from 'react'
import { Link } from 'react-router-dom'
import PendingData from './pendingData.jsx'
import WaitingList from './waitingList.jsx'
import ParticipateRating from './participateRating.jsx'
import FillProfile from './fillProfile.jsx'
import InfoInPool from './infoInPool.jsx'
import {
    transformToDays
    } from '../../../../../../helpers/date.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n,}
)
export default class RightAsideStatus extends React.PureComponent {
    constructor(props){
        super(props);
    }
    componentDidMount() {
        if (this.props.applicant && !this.props.applicant.knowledges){
            this.props.actions.getWorkSkills();
        }
    }
    
    componentDidUpdate(prevProps, prevState){
        let comparingProps = _.isEqual(prevProps, this.props),
            {
                applicant:{
                    profile:{
                        status,
                        progress
                    }
                }
            } = this.props;
		if(!comparingProps && status == 'pending' && progress == 100) {
			this.checkHintRating();
		}
    }

    checkHintRating(){
        let storage = localStorage.getItem('hint');
        if(storage && !storage.layoutRating) return;
        const {actions:{updateHintLayoutRating} = {}} = this.props;
        updateHintLayoutRating(true);
    }

    render(){
        const {
            applicant: {
                knowledges = [],
                profile: {
                    status,
                    inPoolBanExpiredDate,
                    progress
                },
                statisticData:{
                    queueData:{
                        rank = '',
                        expiredDate = '',
                        mainSkill = ''
                    } = {},
                    openCount = ''
                } = {}
            }
        } = this.props;
       let waiting = transformToDays(inPoolBanExpiredDate);
        switch(status) {
            case 'pending':
                if (progress != 100) return (<FillProfile/>)
                return (
                    <ParticipateRating/>
                )
                break;
            case 'inPool':
                return (
                    <InfoInPool 
                        rank={rank}
                        expiredDate={expiredDate}
                        openCount={openCount}
                        mainSkill={mainSkill}
                        knowledges={knowledges}
                    />
                )
                break;
            case 'waitingApprove':
                return (
                    <PendingData/> 
                )
                break;
            case 'inPoolExpired':
            case 'hiredNotInPlatform':
                    return (
                        <WaitingList inPoolBanExpiredDate={waiting}/> 
                    )
                break;
        }

    }
}
