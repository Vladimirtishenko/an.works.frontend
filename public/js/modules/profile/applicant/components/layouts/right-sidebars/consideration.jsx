import React from 'react'
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class Consideration extends React.Component {
    render() {
        const {
            i18n:{translation = {}} = {}
        } = this.props;
        return (
            <div className="box--rounded box--white shadow--box padding--xc-15 padding--yc-20 margin--b-20 width--100">
                <p className="font--14 font--color-primary margin--b-20">{translation.ranking}</p>
                <p className="font--12 font--color-secondary">
                    {translation.validationApplicant}
                </p>
            </div>
        )
    }
}

export default Consideration;
