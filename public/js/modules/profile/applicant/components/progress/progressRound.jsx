import React from 'react'

import CircleProgressBar from '../../../../../libraries/progress/circle.js'

class CircularProgressBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        ...props,
        textValue: 0
    };
    this.drawing = ::this.drawing;
  }

  componentDidMount(){

      let value = this.state.value / 100;

     this.circleProgressBar = new CircleProgressBar(this.__refs, this.state);

     this.setValue(value);
     this.setListener();

  }

  componentDidUpdate(prevProps, prevState){

      if(this.props.value !== prevProps.value){

          let value = this.props.value / 100;

          this.setValue(value);
          this.setState({...this.state, value: this.props.value, textValue: parseInt(this.props.value) } )

      }
  }

  componentWillUnmount(){
      this.__refs.removeEventListener('circleProgressBar.afterFrameDraw', this.drawing)
  }

  setListener(canvas){
      this.__refs.addEventListener('circleProgressBar.afterFrameDraw', this.drawing)
  }

  setValue(value){
      this.circleProgressBar.setValue(value);
  }

  drawing(e){
      let process = e.detail.progress,
          realValue = this.state.value;

      this.setState({...this.state, textValue: parseInt(realValue * process) } )
  }

  render() {
    return (
        <div className="relative margin--b-25 margin--t-15">
            <span className=" absolute absolute--center font--24 font--color-secondary font--700 nowrap--all">{this.state.textValue} %</span>
            <canvas className="progress-bar width--100" width="130" height="130" ref={(node) => {this.__refs = node}}></canvas>
        </div>
    );
  }
}

export default CircularProgressBar;
