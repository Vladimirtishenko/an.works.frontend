import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import Checkbox from "../../../../../components/form/checkbox.jsx";
import ComboboxComponent from "../../../../../components/form/combobox.jsx";
import Pagination from "../../../common/components/pagination.jsx";
import { DeleteButton } from "../../../../../components/form/deleteButton.jsx";
import { CommonSearch } from "../../../../../components/widgets/commonSearch.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class TestsAdd extends React.Component {
    render() {
        const { onClose } = this.props;
        return (
            <PopUp
                classHeader="pop-up__head_disabled"
                title={<span />}
                poUpMainClass="padding--xc-10 padding--yc-15 font--left"
                footer={<span />}
                footerWrapper="pop-up__footer_disabled"
                wrapperClass=""
            >
                <div className="fl fl--dir-col padding--20">
                    <CommonSearch wrapperClass="width--px-220 margin--b-10" />
                    <div className="fl--align-c">
                        Отображать на странице{" "}
                        <ComboboxComponent
                            name="skill name"
                            wrapperClass="no-gutters text-combobox text-combobox__small min-width--20-px padding--l-5"
                            list={[
                                { label: "10", name: "10" },
                                { label: "20", name: "20" },
                                { label: "30", name: "30" },
                                { label: "40", name: "40" },
                                { label: "50", name: "50" }
                            ]}
                            value="10"
                        />
                    </div>
                </div>
                <table className="responsive-table ">
                    <thead className="hr--top hr--bottom">
                        <tr className="responsive-table__tr">
                            <th className="responsive-table__th padding--yc-10" />
                            <th className="responsive-table__th_14 padding--yc-10">
                                Навык
                            </th>
                            <th className="responsive-table__th_14 padding--yc-10">
                                Длительность
                            </th>
                        </tr>
                    </thead>
                    <tbody className="responsive-table__tbody hr--bottom">
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                        <tr className="responsive-table__tr">
                            <td className="responsive-table__td">
                                <Checkbox
                                    wrapperClass="font--center"
                                    labelClass="padding--l-0"
                                    value={[
                                        {
                                            value: "true",
                                            name: "",
                                            sub: "",
                                            checked: false
                                        }
                                    ]}
                                    required={false}
                                />
                            </td>
                            <td className="responsive-table__td">Javascript</td>
                            <td className="responsive-table__td">00:24</td>
                        </tr>
                    </tbody>
                </table>
                <div className="fl fl--dir-col padding--yc-10 font--12 font--color-inactive font--center">
                    <span>Тестов 10 из 20</span>
                    <Pagination
                        wrapperClass="pagination margin--yc-10 fl--justify-c"
                        pages={20}
                        selectedPage={5}
                    />
                    <div className="fl fl--align-c fl--justify-c padding--yc-10">
                        <DeleteButton
                            className="font--12 padding--xc-5 padding--l-15 margin--r-15"
                            data-id={""}
                            data-mark={""}
                            onClick={() => onClose()}
                            text="Отмена"
                        />
                        <button
                            className="btn btn--primary font--14 padding--y-10 padding--x-20"
                            onClick={() => onClose()}
                        >
                            Добавить
                        </button>
                    </div>
                </div>
            </PopUp>
        );
    }
}
