import React from 'react'
import PopUp from '../../../common/components/modal/pop-up.jsx'
import Input from '../../../../../components/form/text.jsx';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class BuyContact extends React.Component {
    render() {
        return (
            <PopUp
                title="пригласить в платформу нового рекрутера"
                poUpMainClass="padding--yc-20 padding--md-xc-45 padding--md-yc-30 font--center"
                icon="email margin--r-15 font--16"
                classHeader="fl fl--align-c fl--justify-c"
            >
                <p className="font--12 font--color-primary margin--b-20 ">
                    {translation.infoOnWaiting + ':'}
                </p>
                    
                 <ul className="fl fl--align-c fl--justify-c">
                     <li className="fl fl--dir-col padding--xc-15 padding--yc-5 border--gray-1 margin--r-5">
                         <span className="font--24 font--700 font--color-primary">13</span>
                         <span className="font--12 font--color-inactive">{translation.days}</span>
                     </li>
                     <li className="fl fl--dir-col padding--xc-15 padding--yc-5 border--gray-1">
                        <span className="font--24 font--700 font--color-primary">07</span>
                        <span className="font--12 font--color-inactive">{translation.hours}</span>
                     </li>
                 </ul>
            </PopUp> 
        )
    }
}

export default BuyContact;
