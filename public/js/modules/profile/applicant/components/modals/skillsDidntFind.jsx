import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import Textarea from "../../../../../components/form/textarea.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class SkillsDidntFind extends React.Component {
    render() {
        const { closePopup, i18n: { translation = {} } = {} } = this.props;

        return (
            <PopUp
                title="Не нашли нужный навык?"
                poUpMainClass="padding--yc-20 padding--md-xc-45 padding--md-yc-30 font--center"
                closePopup={closePopup}
            >
                <p className="font--14 font--color-secondary margin--b-20 padding--xc-5">
                    {translation.weImproveServiceForYou}
                </p>
                <Textarea
                    classes="margin--b-30 fl fl--justify-c"
                    labelClass="form__label font--14"
                    rows="7"
                    textAreaWrapper="padding--0 col-11 col-md-12"
                    textAreaClass="form__text-area"
                    name="aboutMe.achivments"
                    placeholder="Введите название нового навыка"
                    maxlength="300"
                />
                <div className="fl fl--align-c">
                    <button className="btn btn--primary font--14 font--500 padding--yc-10 padding--xc-15 margin--auto">
                        {translation.submitApplication}
                    </button>
                </div>
            </PopUp>
        );
    }
}

export default SkillsDidntFind;
