import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export class HireContactsInfo extends React.Component {
    render() {
        const { i18n:{translation = {}} = {},data:{firstName = "",lastName = "",profileId = "",contactInfo:{phoneNumber = [], telegramAccount = "", viberAccount = "", skypeAccount = "",linkedinAccount = ""},email = ""} = {}, closeContacts } = this.props;

        return (
            <PopUp
                title={
                    <div className="fl fl--dir-col font--14 font--500">
                        <div>
                            <span className="margin--r-5">{firstName}</span>
                            <span>{lastName}</span>
                        </div>
                        <span>{profileId}</span>
                    </div>
                }
                poUpMainClass="padding--xc-45 padding--yc-30 font--center"
                classHeader="fl fl--align-c fl--justify-c"
                footer={
                    <span
                        className="font--14 font--color-secondary font--underlined pointer l-h--1"
                        onClick={() => closeContacts()}
                    >
                        Закрыть
                    </span>
                }
            >
              <div className="container padding--0">
                    {
                        phoneNumber.length && (
                            <div className="row margin--0 font--12 font--color-secondary font--500 margin--b-5">
                                <div className="col-6 font--left padding--l-0">
                                    {translation.phone}:
                                </div>
                                <div className="col-6 font--left padding--r-0 font--bold word-break--word">
                                    {phoneNumber[0]}
                                </div>
                            </div>
                        ) || null
                    }

                    {
                        email && (
                            <div className="row margin--0 font--12 font--color-secondary font--500 margin--b-5">
                                <div className="col-6 font--left padding--l-0">{translation.emailAddress}:</div>
                                <div className="col-6 font--left padding--r-0">
                                    <a
                                        href="mailto:hr@anworks.com"
                                        className="link font--color-blue word-break--word"
                                    >
                                        {email}
                                    </a>
                                </div>
                            </div>
                        ) || null
                    }
                    
                    {
                        telegramAccount && (
                            <div className="row margin--0 font--12 font--color-secondary font--500 margin--b-5">
                                <div className="col-6 font--left padding--l-0">
                                    {translation.telegram}:
                                </div>
                                <div className="col-6 font--left padding--r-0">
                                    <a href="#" className="link font--color-blue word-break--word">
                                        {telegramAccount}
                                    </a>
                                </div>
                            </div>
                        ) || null
                    }
                   
                    {
                        viberAccount && (
                            <div className="row margin--0 font--12 font--color-secondary font--500 margin--b-5">
                                <div className="col-6 font--left padding--l-0">{translation.viber}:</div>
                                <div className="col-6 font--left padding--r-0">
                                    <a href="#" className="link font--color-blue word-break--word">
                                        {viberAccount}
                                    </a>
                                </div>
                            </div>
                        ) || null
                    }
                    
                    {
                        skypeAccount && (
                            <div className="row margin--0 font--12 font--color-secondary font--500 margin--b-5">
                                <div className="col-6 font--left padding--l-0">{translation.skype}:</div>
                                <div className="col-6 font--left padding--r-0">
                                    <a href="#" className="link font--color-blue word-break--word">
                                        {skypeAccount}
                                    </a>
                                </div>
                            </div>
                        ) || null
                    }

                    {
                        linkedinAccount && (
                            <div className="row margin--0 font--12 font--color-secondary font--500 margin--b-5">
                                <div className="col-6 font--left padding--l-0">
                                    {translation.linkedIn}:
                                </div>
                                <div className="col-6 font--left padding--r-0">
                                    <a href="#" className="link font--color-blue">
                                        {linkedinAccount}
                                    </a>
                                </div>
                            </div>
                        ) || null
                    }

                    

                </div>
            </PopUp>
        );
    }
}
