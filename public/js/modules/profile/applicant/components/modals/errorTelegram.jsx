import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class ErrorTelegram extends React.Component {

    render() {
        const {i18n:{translation = {}} = {}} = this.props;
        return (
            <PopUp 
                title={translation.error}
                poUpMainClass="padding--xc-45 padding--yc-30 padding--xc-25 font--center"
                closePopup={() => closePopup()}
                icon="cross margin--r-5 font--16 pop-up__head--icon-important"
                classHeader="fl fl--align-c fl--justify-c"
                titleClass="pop-up__title--important "
            >
                <p className="font--14 font--color-secondary">
                    {translation.textCannotFindTelegram}
                </p>
            </PopUp>
        );
    }
}

export default ErrorTelegram;
