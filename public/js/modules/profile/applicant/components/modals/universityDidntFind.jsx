import React from "react";

import PopUp from "../../../common/components/modal/pop-up.jsx";
import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import Checkbox from "../../../../../components/form/checkbox.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class UniversityDidntFind extends React.Component {

    constructor(props){

        super(props);
        this.state = {
            languages: []
        }

    };

    render() {
        const { closePopup, onSend } = this.props;
        return (
            <PopUp
                title="не нашли учебное заведение?"
                poUpMainClass="padding--xc-20 padding--yc-30 padding--md-xc-45 scroll--y-auto"
                wrapperClass="pop-up--max-530"
                closePopup={closePopup}
            >
                <div className="font--14 margin--b-20 font--center font--color-secondary">
                    Сообщите нам, и мы постараемся улучшить сервис для вас
                </div>
                {/* TODO:  no-mvp 
                <Checkbox 
                    wrapperClass="checkbox font--left margin--b-10"
                    value={[ 
                        {
                            name: "Educational-institution",
                            label: "Educational-institution",
                            sub: "Учебное заведение",
                            innerWrapperClass: "fl fl--align-c",
                            subClass: "font--14 l-h--1",
                            innerWrapperClass:"margin--b-10 font--14 font--color-primary",
                            checked:false
                        },
                        {
                            name: "Faculty",
                            label: "Faculty",
                            sub: "Факультет",
                            innerWrapperClass: "fl fl--align-c",
                            subClass: "font--14 l-h--1",
                            innerWrapperClass:"margin--b-10 font--14 font--color-primary",
                            checked:false
                        },
                        {
                            name: "degree",
                            label: "degree",
                            sub: "Степень",
                            innerWrapperClass: "fl fl--align-c",
                            subClass: "font--14 l-h--1",
                            innerWrapperClass: "font--14 font--color-primary",
                            checked:false                           
                        }
                    ]}
                    label="Добавить"
                    labelWrapperClass="form__label col-md-5 font--14"
                    wrapperAllCheckbox="col-md-7 fl fl--dir-col padding--md-l-0"
                    wrapperClass="checkbox row margin--b-10 combobox "
                    innerWrapperClass=" margin--xl-b-20"
                    labelClass="form__checkbox form__label"
                    
                /> */}
                <Combobox
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="form__label col-md-5 font--14"
                    className="col-md-7 padding--md-l-0"
                    value=""
                    label="Страна"
                    list={[
                        { name: "Украина", label: "ua" },
                        { name: "Россия", label: "ru" }
                    ]}
                    name=""
                />
                <Combobox
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass="form__label col-md-5 font--14"
                    className="col-md-7 padding--md-l-0"
                    value=""
                    label="Город"
                    list={[
                        { name: "Харьков", label: "kh" },
                        { name: "Киев", label: "ky" }
                    ]}
                    name=""
                />
                <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="form__label col-md-5 font--14"
                    inputWrapperClass="col-md-7 padding--md-l-0"
                    inputClass="input form__input"
                    value=""
                    label="Учебное заведение"
                    name=""
                />
                {/*
                TODO: no mvp
                 <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="form__label col-md-5 font--14"
                    inputWrapperClass="col-md-7 padding--md-l-0"
                    inputClass="input form__input"
                    value=""
                    label="Факультет"
                    name=""
                />
                <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass="form__label col-md-5 font--14"
                    inputWrapperClass="col-md-7 padding--md-l-0"
                    inputClass="input form__input"
                    value=""
                    label="Степень"
                    name=""
                /> */}
                <div className="margin--t-20 fl fl--align-c">
                    <button
                        className="btn btn--primary font--14 font--500 padding--yc-10 padding--xc-15 margin--auto"
                        onClick={() => onSend()}
                    >
                        Отправить
                    </button>
                </div>
            </PopUp>
        );
    }
}

export default UniversityDidntFind;
