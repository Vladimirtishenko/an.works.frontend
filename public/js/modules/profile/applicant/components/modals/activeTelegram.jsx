import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import Input from "../../../../../components/form/text.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class ActiveTelegram extends React.Component {
    //TODO:: success message
    stateButtonTelegram(value){

        //fuck this rerender
        // this.setState({
        //     ...this.state,
        //     tokenValue: value
        // });
        this.tokenValue = value;
    };
    render() {
        const {
            onClose,
            onclickAction,
            verificed:{
                username,
                result
            } = {},
            isFailed,
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        return (
            <PopUp
                title={translation.textTelegramVerificationCode}
                poUpMainClass="padding--yc-20 padding--md-xc-45 padding--xc-15 padding--md-yc-30 font--center"
                closePopup={() => onClose()}
            >
            {
                result == 'Token is valid' &&
                (<div>
                    <span className="icon--verified icon--verified-font-50 fl--inline fl--align-c font--color-green font--24 margin--b-15"></span>
                    <p className="font--16 font--color-primary font--center">
                        {translation.activeTelegam}
                    </p>
                    <p className="font--16 font--color-primary font--center">{username}</p>
                </div>)
                ||
                (
                    <div>
                        <p className="font--14 font--color-secondary margin--b-20 ">
                            {translation.textEnterTelegramCode}
                        </p>
                        <Input
                            wrapperClass="row fl--align-c fl--justify-c margin--b-20"
                            inputWrapperClass="col-8"
                            inputClass="input form__input input--place-c font--center"
                            errorClass="margin--r-30"
                            name="telegram_active_code"
                            placeholder={translation.enterCode}
                            onChange={(val) => { this.stateButtonTelegram(val) }}
                        />
                        {
                            isFailed &&
                            <p className="font--14 font--color-secondary margin--b-20 ">
                                {translation.incorrectCode}
                            </p>
                        }
                        <button onClick={
                                    (e) => {
                                        e.preventDefault();
                                        onclickAction(this.tokenValue);
                                    }
                                    }
                        className="btn btn--theme-blue font--500 font--14 padding--xc-20 padding--yc-10 margin--b-15">{translation.confirm}</button>
                        <p className="font--14 font--color-secondary margin--b-10">{translation.textNotReceiveTelegramCode}</p>
                        <p className="font--14 font--color-secondary">{translation.pleaseCheckTheDetails}</p>
                    </div>
                )
            }
               
            </PopUp>
        );
    }
}

export default ActiveTelegram;
