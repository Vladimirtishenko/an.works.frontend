import React from 'react'
import PopUp from '../../../common/components/modal/pop-up.jsx';
import {
    dmyDate
} from '../../../../../helpers/date.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class StatusContact extends React.Component {
    render() {
        const {
            data:{
                callToUpdateHiringRow,
                openPopUpHired,
                closePopUpContact,
                firstName = '',
                lastName = '',
                createdDate = '',
                name = '',
                applicantId = '',
                companyId = '',
                contactInfo:{
                    phoneNumber = []
                } = {}
            } = {},
            i18n:{
                translation = {}
            } = {}
        } = this.props;

        return (
            <PopUp
                title={translation.openContactStatusNotSpecified}
                poUpMainClass="padding--xc-45 padding--yc-30 font--center"
                titleClass="pop-up__title--important pop-up__title--12 font--md-14"
                icon="alarm margin--r-15 font--16 pop-up__head--icon-important"
                classHeader="fl fl--align-c fl--justify-c"
                footer={
                    <React.Fragment> 
                        <button 
                            onClick={e => {
                                openPopUpHired(true,{companyId:companyId, applicantId:applicantId})
                            }}
                            className="font--uppercase margin--xc-10 btn btn--theme-blue font--10 width--px-100 height--px-20"
                        >
                            {translation.hired}
                        </button>
                        <button 
                            onClick={e => {
                                callToUpdateHiringRow(false, companyId, applicantId)
                                closePopUpContact();
                            }}
                            className="font--uppercase margin--xc-10 btn font--10 width--px-100 height--px-20 btn--theme-white"
                        >
                            {translation.notHired}
                        </button>
                    </React.Fragment>
                }
            >
                <p className="font--12 font--color-primary margin--b-20 ">
                    {translation.statusHiredApplicant}
                </p>
                    
                 <ul className="list--style-type-none">
                    <li className="font--14 margin--b-5 font--500 font--color-primary">{firstName+" "+lastName}</li>
                    <li className="font--14 margin--b-5 font--color-blue font--uppercase">{name}</li>
                    <li className="font--12 margin--b-15 font--color-secondary">{dmyDate(createdDate)}</li>
                    <li className="font--14 margin--b-5 font--500 font--color-primary">{phoneNumber[0]}</li>
                    <li className="font--14 font--500 font--color-blue">mail@mail.com</li>
                 </ul>
            </PopUp> 
        )
    }
}

export default StatusContact;
