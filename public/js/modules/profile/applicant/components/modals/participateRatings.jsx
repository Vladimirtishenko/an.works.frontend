import React from 'react'
import PopUp from '../../../common/components/modal/pop-up.jsx'
import Number from '../../../../../components/form/number.jsx';
import Combobox from '../../../../../components/form/combobox.jsx';
import Checkbox from '../../../../../components/form/oneCheckbox.jsx';
import { ValidatorForm } from '../../../../../libraries/validation/index.js';

// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class ParticipateRanking extends React.Component {

    onSubmit(event){
        const { target: form } = event,
                serializedForm = serialize(form),
              { updateProfileByComponent, profileId } = this.props,
              data = {status: 'waitingApprove', ...serializedForm};

            updateProfileByComponent(data, profileId);
    }

    render() {
        const { changeModalVisibility, i18n:{translation = {}} = {} } = this.props;
 
        return (
            <ValidatorForm onSubmit={::this.onSubmit} className="form">
                <PopUp
                    title={translation.startParticipateInRanking}
                    poUpMainClass="padding--yc-20  padding--xc-15 padding--md-xc-30 padding--md-yc-30"
                    closePopup={(e) => { changeModalVisibility('participateRatings') } }
                >
                    <Number
                        validators={["required", "matchRegexp:[^0]+" ]}
                        errorMessages={[translation.errorMessageRquired, translation.errorMessageSallary ]}
                        wrapperClass="row number fl--align-c margin--b-10 "
                        inputWrapperClass="col-md-8 "
                        labelClass="label form__label font--14 col-md-4 padding--r-0"
                        label={translation.salary}
                        min={0}
                        max={1000}
                        name="wishedSalary"
                        required={true}
                    />

                    <div className="col-md-8 offset-md-4 font--12 font--color-secondary margin--b-20 padding--0 padding--md-l-15">Желаемая зарплата до налогообложения в месяц. Не более 1000$</div>

                    <Combobox
                        validators={["required","combobox"]}
                        errorMessages={[ translation.errorMessageRquired, translation.errorMessageCombobox ]}
                        name="goingToStart"
                        wrapperClass="row  fl--align-c margin--b-30 combobox"
                        labelClass="form__label col-md-4 font--14  padding--r-0"
                        className="col-md-5"
                        label={translation.readyStartWork}
                        list={_.range(1, 31).map(item => ({label:item,name:item}))}
                        required={true}
                    />

                    <Checkbox
                        validators={["checkboxRequired"]}
                        errorMessages={[translation.errorMessageOneCheckbox]}
                        wrapperClass="checkbox row  fl--align-c margin--b-10 margin--b-20 margin--l-0 margin--r-0"
                        name="agreeSale"
                        id="agreeSale"
                        value={[
                            {
                                name: 'agreeSale',
                                value: "agreeSale",
                                label:'agreeSale',
                                sub: translation.ruleDesiredSalary,
                                checked: false,
                                subClass: "font--color-primary font--just",
                                innerWrapperClass: "font--14 font--color-secondary fl ",
                                classNameInput: "checkbox__input--margin-r-20"
                            }
                        ]
                        }
                        required={true}
                    />
                        <button className="margin--auto-xc btn btn--b btn--theme-blue font--14 width--px-200 height--px-40">{translation.participate}</button>
                </PopUp>
            </ValidatorForm>
        )
    }
}

export default ParticipateRanking;
