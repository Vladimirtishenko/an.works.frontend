import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import Textarea from "../../../../../components/form/textarea.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class NotHiredWorks extends React.Component {
    render() {
        const { changeModalVisibility, handlerHireThroughOtherService, i18n:{translation = {}} = {} } = this.props;

        return (
            <PopUp
                title="Нанят не через платформу"
                poUpMainClass="padding--yc-20 padding--md-xc-45 padding--md-yc-30 font--center"
                closePopup={(e) => {changeModalVisibility('notHired')}}
            >
               <p className="font--14 font--color-secondary margin--b-10 padding--xc-25">{translation.confirmHiredNotInPlatform}</p>
               <p className="font--14 font--color-secondary margin--b-25 padding--xc-25">{translation.completeParticipationRating}</p>
                <div className="fl fl--align-c">
                    <button onClick={handlerHireThroughOtherService} className="btn btn--primary font--14 font--500 padding--yc-10 padding--xc-15 margin--auto">
                        Подтвердить
                    </button>
                </div>
            </PopUp>
        );
    }
}

export default NotHiredWorks;
