import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import Textarea from "../../../../../components/form/textarea.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class NotHiredWorks extends React.Component {
    render() {
        const { i18n:{translation = {}} = {},status, closePopUp, confirmHired,closePopUpAll, configHired:{applicantId,companyId} = {}} = this.props;

        return (
            <PopUp
                title="Наняты на работу"
                poUpMainClass="padding--yc-20 padding--md-xc-45 padding--md-yc-30 font--center"
                closePopup={()=>{closePopUp(false)}}
            >
               <p className="font--14 font--color-secondary margin--b-10 padding--xc-25">{translation.confirmHiredNotInPlatform}</p>
               {
                   status == "inPool" && (
                        <p className="font--14 font--color-secondary margin--b-25 padding--xc-25">{translation.completeParticipationRating}</p>
                   ) || null
               }
               
                <div className="fl fl--align-c">
                    <button onClick={()=>{
                        if(companyId && applicantId){
                            confirmHired(true, companyId, applicantId);
                            closePopUpAll();
                        }
                    }}
                    className="btn btn--primary font--14 font--500 padding--yc-10 padding--xc-15 margin--auto">
                        {translation.confirm}
                    </button>
                </div>
            </PopUp>
        );
    }
}

export default NotHiredWorks;
