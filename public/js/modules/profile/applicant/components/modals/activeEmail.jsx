import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class ActiveEmail extends React.Component {
    render() {
        const { closePopup } = this.props;

        return (
            <PopUp
                title="Ссылка на подтверждения почты отправлена"
                poUpMainClass="padding--yc-20 padding--md-xc-45 padding--md-yc-30 font--center"
                closePopup={() => closePopup()}
            >
                <p className="font--14 font--color-secondary margin--b-20 ">
                    На ваш емайл отправлено письмо для портверждения вашей новой
                    почты
                </p>

                <p className="font--14 font--color-secondary">
                    Не пришло письмо,{" "}
                    <span className="font--color-blue font--underlined pointer">
                        отправить заново
                    </span>
                </p>
            </PopUp>
        );
    }
}

export default ActiveEmail;
