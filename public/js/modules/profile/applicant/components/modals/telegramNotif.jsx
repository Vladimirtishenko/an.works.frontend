import React from 'react'
import PopUp from '../../../common/components/modal/pop-up.jsx'
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class TelegramNotif extends React.Component {
    render() {
        const { onClose, onclickAction, i18n:{translation = {}} = {} } = this.props;
        return (
            <PopUp
                title="Telegram для получения уведомлений"
                poUpMainClass="padding--xc-45 padding--yc-30 font--center"
                classHeader="fl fl--align-c fl--justify-c"
                closePopup={() => onClose()}
            >
                <img src="../img/telegram-notif.png" alt=""/>
                <p className="font--14 font--color-secondary margin--b-20">{translation.textTelegramClickStart}</p>
                <button onClick={
                    (e) => {e.preventDefault(); onclickAction(e)}
                } className="btn btn--theme-blue font--500 font--14 padding--xc-20 padding--yc-10">{translation.openTelegram}</button>
            </PopUp> 
        )
    }
}

export default TelegramNotif;
