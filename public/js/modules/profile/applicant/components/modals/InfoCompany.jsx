import React from 'react'
import PopUp from '../../../common/components/modal/pop-up.jsx';
import {normalizeStaff} from "../../../../../helpers/staff.helper.js";
import countries from '../../../../../databases/general/countries.json';
import {normalizeCity} from '../../../../../helpers/locationMapper.helper.js';
import {separateDate} from '../../../../../helpers/date.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class InfoCompany extends React.Component {

    constructor(props){
        super(props),
        this.state = {

        }
    }


    componentDidMount(){

        const {
            companyInfo:{
                additionalData:{
                    company:{
                        companyInfo:{
                            city = '',
                            country = ''
                        }
                    }
                }
            } = {}
        } = this.props,
            countryIndex = country ? _.findIndex(countries, {'label': country} ) : null,
            countryName = countryIndex > -1 ? countries[countryIndex] : {};

        if(country){
            this.citiesUpload(countryName, city);
        }

    }

    async citiesUpload(country, city){

        const db = await (
          await import(`../../../../../databases/countries/${country.label}/cities.${country.label}.json`)
        ).default;

        let citiesList = normalizeCity(city, db);

        if(citiesList){
            this.setState({
                ...citiesList,
                country: country.name,
                db
            })
        }
    }

    render() {
        const {
            companyInfo:{
                additionalData:{
                    company:{
                        name = '',
                        companyInfo:{
                            website = '',
                            staff = '',
                            birthday = '' 
                        }
                    }
                }
            } = {},
            closePopUp,
            i18n:{
                translation = {}
            } = {}
        } = this.props,
        normalizesStaff = normalizeStaff(staff), 
        { country: countrySingle, city } = this.state,
        address = countrySingle + ' ' + city,
        date = separateDate(birthday);

        return (
            <PopUp
                title={name}
                poUpMainClass="padding--md-xc-45 padding--xc-15 padding--yc-30 font--center"
                titleClass="font--color-blue"
                classHeader="fl fl--align-c fl--justify-c"
                closePopup={closePopUp}
            >
                 <ul className="list--style-type-none fl fl--wrap">
                 <li className="font--14 margin--b-5 width--50 font--left font--color-primary padding--r-10 word-break--word">{translation.mainOffice}</li>
                    <li className="font--14 margin--b-5 width--50 font--left font--color-secondary word-break--word">{address}</li>
                    <li className="font--14 margin--b-5 width--50 font--left font--color-primary padding--r-10 word-break--word">{translation.foundationDate}</li>
                    <li className="font--14 margin--b-5 width--50 font--left font--color-secondary word-break--word">{date.year}</li>
                    <li className="font--14 margin--b-5 width--50 font--left font--color-primary padding--r-10 word-break--word">{translation.staffs}</li>
                    <li className="font--14 margin--b-5 width--50 font--left font--color-secondary word-break--word">{normalizesStaff}</li>
                    <li className="font--14 margin--b-5 width--50 font--left font--color-primary padding--r-10 word-break--word">{translation.webSite}</li>
                    <li className="width--50 font--left">
                        <a className="font--14 margin--b-5 font--color-blue word-break--word" href={website} target="_blank"> 
                            {website}
                        </a>
                    </li>
                 </ul>
            </PopUp> 
        )
    }
}

export default InfoCompany;
