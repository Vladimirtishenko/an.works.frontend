import React from "react";
import PopUp from "../../../common/components/modal/pop-up.jsx";
import Input from "../../../../../components/form/text.jsx";
import serialize from "../../../../../helpers/serialize.helper.js";

import { ValidatorForm } from "../../../../../libraries/validation/index.js"; 
import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class InviteEmail extends React.Component {
    submit(event) {
        event.preventDefault();

        let form = event && event.target;

        if (!form) return;

        let serializedForm = serialize(form),
            {
                companyProfile: { company }
            } = this.props;

        this.props.actions.inviteNewEmployee(
            serializedForm.email,
            company.name
        );

        this.props.closePopup();
    }

    render() {
        const { closePopup, i18n:{translation = {}} = {} } = this.props;
        return ( 
            <ValidatorForm onSubmit={::this.submit} className="form">
                <PopUp
                    title={translation.inviteRecruiter}
                    poUpMainClass="padding--xc-45 padding--yc-30 font--center"
                    closePopup={() => closePopup()}
                >
                    <p className="font--14 font--color-secondary margin--b-20 ">
                        {translation.mail}
                    </p>
                    <Input
                        validators={[
                            "required",
                            "isEmail"
                        ]}
                        errorMessages={[translation.errorMessageRquired, translation.errorMessageEmail]}
                        wrapperClass="row fl--align-c fl--justify-c margin--b-20"
                        inputWrapperClass="col-12 col-md-10"
                        inputClass="input form__input"
                        name="email"
                    />
                    <div className="fl fl--align-c">
                        <button className="btn btn--primary font--14 font--500 padding--yc-10 padding--xc-15 margin--auto">
                            {translation.invite}
                        </button>
                    </div>
                </PopUp>
            </ValidatorForm>
        );
    }
}

export default InviteEmail;
