import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'

import NotificationNotEditable from '../not-editable/notification.jsx'
import NotificationTestListNotEditable from '../not-editable/mainTestList.jsx'

class Notification extends React.Component {

    render(){
        return (
            <div className="form">
                <NotificationNotEditable {...this.props} />
                <NotificationTestListNotEditable  {...this.props} />
            </div>
        )
    }

}

export default Notification;
