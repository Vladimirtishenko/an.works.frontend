import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";
import StaticItem from "../../../../../components/form/staticItem.jsx";

// Not Editable
import EmploymentsTypeNotEditable from "../not-editable/occupation.jsx";
import GeographyNotEditable from "../not-editable/geography.jsx";

// Editable
import EmploymentsTypeEditable from "../editable/occupation.jsx";
import GeographyEditable from "../editable/geography.jsx";

import serialize from "../../../../../helpers/serialize.helper.js";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";

import {statusUser} from "../../../../../helpers/statusUser.helper.js";
class Location extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            geography: false,
            type: false
        };
    }

    changeView(event) {
        let component =
            (event &&
                event.target &&
                event.target.dataset &&
                event.target.dataset.component) ||
            null;

        if (component) {
            this.setState({
                [component]: !this.state[component]
            });
        }
    }

    submitGeography() {
        event.preventDefault();

        let form = event.target,
            serializedForm = serialize(form),
            placesArray = _.get(
                serializedForm,
                "geography.relocation.abroad.places"
            ),
            places = placesArray ? _.values(placesArray) : [],
            cityArray = _.get(
                serializedForm,
                "geography.relocation.local.cities"
            ),
            cities = cityArray ? cityArray.split(',') : [],
            { direction, profileId } = this.props.applicant.profile;

        _.set(serializedForm, "geography.relocation.abroad.places", places);
        _.set(serializedForm, "geography.relocation.local.cities", cities);

        this.props.actions.updateProfile(
            {
                geography: {
                    ...serializedForm.geography,
                    occupation: this.props.applicant.profile.geography
                        .occupation
                }
            },
            profileId,
            direction
        );

        this.setState({
            geography: false
        });
    }

    submitOccupation() {
        event.preventDefault();

        let form = event.target,
            serializedForm = serialize(form),
            placesArray = _.get(
                serializedForm,
                "geography.relocation.abroad.places"
            ),
            places = placesArray ? _.values(placesArray) : [],
            { direction, profileId } = this.props.applicant.profile;

        _.set(serializedForm, "geography.relocation.abroad.places", places);

        this.props.actions.updateProfile(
            {
                geography: {
                    ...this.props.applicant.profile.geography,
                    occupation: serializedForm.geography.occupation
                }
            },
            profileId,
            direction
        );

        this.setState({
            type: false
        });
    }

    componentDidUpdate(prevProps, prevStep) {
        let aboutPrev =
                (prevProps.applicant.profile &&
                    prevProps.applicant.profile.geography) ||
                {},
            aboutNext =
                (this.props.applicant.profile &&
                    this.props.applicant.profile.geography) ||
                {},
            prevOccupation = aboutPrev.occupation,
            nextOccupation = aboutNext.occupation,
            prevGeography = { ...aboutPrev, occupation: "without occupation" },
            nextGeography = { ...aboutNext, occupation: "without occupation" };

        if (!_.isEqual(prevGeography, nextGeography)) {
            this.setState({
                geography: false
            });
        }
        if (!_.isEqual(prevOccupation, nextOccupation)) {
            this.setState({
                type: false
            });
        }
    }

    render() {
        const { geography, type } = this.state,
              {
                  applicant: {
                      profile: {
                          status = '',
                          geography: geographyList
                      }
                  },
                  i18n:{
                      translation = {}
                  } = {}
              } = this.props;

        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1 show--md">
                    {translation.location}
                </h2>
                <div className="form">
                    {geography ? (
                        <ValidatorForm onSubmit={::this.submitGeography}>
                            <GeographyEditable
                                geography={geographyList}
                            />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </ValidatorForm>
                    ) : (
                        <GeographyNotEditable 
                            onEdit={::this.changeView}
                            geography={geographyList}
                            notEdit={statusUser(status)}
                        />
                    )}
                    {type ? (
                        <ValidatorForm onSubmit={::this.submitOccupation}>
                            <EmploymentsTypeEditable
                                i18n={this.props.i18n}
                                occupation={
                                    geographyList && geographyList.occupation
                                }
                            />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </ValidatorForm>
                    ) : (
                        <EmploymentsTypeNotEditable
                            i18n={this.props.i18n}
                            occupation={
                                geographyList && geographyList.occupation
                            }
                            onEdit={::this.changeView}
                            notEdit={statusUser(status)}
                        />
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default Location;
