import React from "react";
import { connect } from "react-redux";
import {extendsSingleSkill} from '../../../../../../helpers/mapperSkills.helper.js'
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)


export default class NotificationParticipateRating extends React.PureComponent {

    render() {
        const {
            i18n:{translation = {}} = {},
            mainSkill,
            knowledges
        } = this.props,
        { name = '' } = extendsSingleSkill(mainSkill, knowledges);
        let failedText = translation.faildeTest && translation.faildeTest.replace('{skill}',name);
        return (
            <div className="info info--shadow info--rounded bg--white margin--b-20">
                <div className="fl">
                    <div className="info__icon info__icon--sm info__icon--bordered info__icon--warning">
                        <span className="icon icon--alarm" />
                    </div>
                    <div className="info__content padding--20 font--color-secondary">
                        {failedText}
                        {translation.ruleChooseAnotherBasicSkillPlatform}
                    </div>
                </div>
            </div>
        )
    }
}