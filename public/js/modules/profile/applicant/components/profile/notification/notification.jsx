import React from 'react'

// Notifications
import NotificationLocationAndSkills from "./notificationLocationAndSkills.jsx";
import NotificationOpening from "./notificationOpening.jsx";
import NotificationunfilledProfile from "./notificationunfilledProfile.jsx";
import NotificationDesireSalary from "./notificationDesireSalary.jsx";
import NotificationConsiderationData from "./notificationConsiderationData.jsx";
import NotificationRatingExpectation from "./notificationRatingExpectation.jsx";
import NotificationParticipateRating from "./notificationParticipateRating.jsx";
import NotificationMainTestFailed from "./notificationMainTestFailed.jsx";
import NotificationNotMainTest from "./notificationNotMainTest.jsx";
import rules from '../../../../../../configuration/rules.json';

import {
    transformToNdaysAgo,
    transformToDays
} from '../../../../../../helpers/date.helper.js'

export default class Notifications extends React.PureComponent {
    findTestResult(){
        const {
            applicant: {
                skillTestsHierarchy:{
                    tests = []
                } = {},
               testsResults,
               
            }
        } = this.props;

        let mainTest = _.findIndex(tests, {level: 0}),
            mainResult = mainTest != -1 ? testsResults && testsResults[_.findIndex(testsResults,{testUid: tests[mainTest].testLabel})] : false;
        return mainResult;
    }

    render(){
        const {
            applicant: {
                profile: {
                    geography,
                    mainSkill,
                    status,
                    progress,
                    wishedSalary,
                    inPoolBanExpiredDate
                },
                skillTestsHierarchy:{
                    tests = []
                } = {},
                knowledges ,
                statisticData: {
                   openCount = 0,
                   contactsList = [],
                   queueData: {
                       expiredDate,
                       rank = '0'
                   } = {}
               } = {},
               testsResults = [],
               
            }
        } = this.props,
        { from = '', to = '' } = transformToNdaysAgo(expiredDate, 30, 'DD.MM'),
        waiting = transformToDays(inPoolBanExpiredDate),
        testResult = this.findTestResult();
        switch(status) {
            case 'pending':
                if(progress !== 100){
                    return (<NotificationunfilledProfile />)
                    break;
                }
                
                if(testResult != undefined && !testResult){
                    return null;
                    break;
                }
                if(testResult && testResult.result < rules.minValueResultTest){
                    return (<NotificationMainTestFailed knowledges={knowledges} mainSkill={mainSkill} />);
                    break;
                }
                if(testResult == undefined){
                    return (<NotificationNotMainTest />) 
                    break;
                }
                // debugger;
                return (<NotificationParticipateRating/>);
                break;
            case 'inPool':
                return (
                    <React.Fragment>
                        <NotificationLocationAndSkills 
                            rank={rank}
                            date={{from, to}}
                            geography={geography}
                            knowledges={knowledges}
                            mainSkill={mainSkill}
                        />
                        <NotificationOpening
                            opens={openCount}
                        />
                        <NotificationDesireSalary
                            sallary={wishedSalary}
                        />
                    </React.Fragment>
                )
                break;
            case 'inPoolExpired':
            case 'hiredNotInPlatform':
                return (
                    <NotificationRatingExpectation
                        waiting={waiting}
                    />
                )
                break;
            case 'waitingApprove':
                return (<NotificationConsiderationData/>)
                break;

        }

    }
}
