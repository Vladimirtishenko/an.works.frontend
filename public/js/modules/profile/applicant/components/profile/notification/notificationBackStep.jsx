
import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";
import {NotificationСabinet} from "../../../../../../components/widgets/notification/notificationСabinet.jsx"


function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)

export default class NotificationBackStep extends React.PureComponent {
    render() {
        const {i18n:{translation = {}} = {}} = this.props;
        return (
            <NotificationСabinet type="information"> 
               {translation.infoSavedDataInNextStep} 
            </NotificationСabinet>
        )
    }
}

