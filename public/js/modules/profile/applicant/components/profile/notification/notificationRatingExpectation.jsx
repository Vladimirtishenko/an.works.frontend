import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)

export default class NotificationRatingExpectation extends React.PureComponent {

    render() {

        const { waiting, i18n:{translation = {}} = {} } = this.props;

        return (
            <div className="info info--shadow info--rounded bg--white margin--b-20">
                <div className="fl">
                    <div className="info__icon info__icon--sm info__icon--bordered info__icon--information">
                        <span className="icon icon--alarm" />
                    </div>
                    <div className="info__content padding--20 font--color-secondary">
                        {`${translation.infoOnWaitingList} ${waiting} ${translation.days}`}
                    </div>
                </div>
            </div>
        )
    }
}
