import React from "react";
import {extendsSingleSkill} from '../../../../../../helpers/mapperSkills.helper.js'
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)

export default class NotificationLocationAndSkills extends React.PureComponent {
    constructor(props){
        super(props);

        // this.state = {
        //     cityName: ""
        // }
    }
    // TODO: NOT MVP
    // componentDidMount(){

    //     this.mapLocation();

    // }

    // async mapLocation(){
    //     let {geography: {city, country}} = this.props;

    //     if(country && city){
    //         const db = await (await import(`../../../../../../databases/countries/${
    //             country
    //         }/cities.${country}.json`)).default;

    //         let cityIndex = db ? _.findKey(db, { label: city }) : null,
    //             cityObject = cityIndex ? db[cityIndex] : null;

    //         cityObject && this.setState({cityName: cityObject.name || ""})

    //     }

    // }

    render() {

        let { knowledges, mainSkill, date: { from = '', to = '' },rank, i18n:{translation = {}} = {}} = this.props,
            { name = '' } = extendsSingleSkill(mainSkill, knowledges);
            // { cityName } = this.state;

        return (
            <div className="info info--shadow info--rounded bg--white margin--b-20">
                <div className="fl">
                    <div className="info__icon info__icon--sm info__icon--bordered info__icon--information">
                        <span className="icon icon--alarm" />
                    </div>
                    <div className="info__content padding--20 font--color-secondary">
                        {`${translation.yourLocation} ${rank} ${translation.ratingPositions} `}
                        <span className="font--500">{name}</span>{" "}
                        {/* 
                        TODO: NOT MVP
                        города{" "}
                        <span className="font--500">{cityName}</span>{" "} */}
                        {`${translation.fromTime} ${from} ${translation.to} ${to} ${translation.inRankingRestriction}`}
                    </div>
                </div>
            </div>
        )
    }
}
