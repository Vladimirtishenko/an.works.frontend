import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)


export default class NotificationDesireSalary extends React.PureComponent {
    render() {
        const { sallary,i18n:{translation = {}} = {} } = this.props;
        return (
            <div className="info info--shadow info--rounded bg--white margin--b-20">
                <div className="fl">
                    <div className="info__icon info__icon--sm info__icon--bordered info__icon--success">
                        <span className="icon icon--alarm" />
                    </div>
                    <div className="info__content padding--20 font--color-secondary">
                        {translation.rankingSallary} {" "}
                            <span className="font--color-primary font--500">{sallary}$</span> {" "}
                        {translation.rankingSallaryDesc}
                    </div>
                </div>
            </div>
        )
    }
}
