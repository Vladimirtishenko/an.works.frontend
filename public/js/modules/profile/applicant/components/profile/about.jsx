import React from "react";

import StaticItem from "../../../../../components/form/staticItem.jsx";

import { Card } from "../../../common/components/card.dumb.jsx";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";

// Not Editable
import AboutNotEditable from "../not-editable/about.jsx";

// Edatable
import AboutEditable from "../editable/about.jsx";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";

import {statusUser} from "../../../../../helpers/statusUser.helper.js";
class PersonalInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            about: false
        };
    }

    changeView() {
        this.setState({
            about: !this.state.about
        });
    }

    submit(event) {
        event.preventDefault();

        let form = event.target,
            serializedForm = serialize(form),
            { aboutMe, profileId } = this.props.applicant.profile;

        if (_.isEqual(aboutMe, serializedForm.aboutMe)) {
            return this.setState({
                about: false
            });
        }

        this.props.actions.updateProfileByComponent(
            { aboutMe: serializedForm.aboutMe ? serializedForm.aboutMe :  { about: '' } },
            profileId
        );

        this.setState({
            about: false
        });
    }

    componentDidUpdate(prevProps, prevStep) {
        let aboutPrev =
                (prevProps.applicant.profile &&
                    prevProps.applicant.profile.aboutMe) ||
                {},
            aboutNext =
                (this.props.applicant.profile &&
                    this.props.applicant.profile.aboutMe) ||
                {};

        if (!_.isEqual(aboutPrev, aboutNext)) {
            this.setState({
                about: false
            });
        }
    }

    render() {
        let { about } = this.state,
            {                
                applicant:{
                    profile:{
                        status = '',
                        aboutMe = {}
                    }
                },
                i18n:{
                    translation = {}
                } = {}
            } = this.props;
        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1 show--md">
                    {translation.about}
                </h2>
                <div className="form">
                    {about ? (
                        <ValidatorForm onSubmit={::this.submit}>
                            <AboutEditable i18n={this.props.i18n} about={aboutMe} />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button> 
                            </div>
                        </ValidatorForm>
                    ) : (
                        <AboutNotEditable
                            i18n={this.props.i18n}
                            onEdit={::this.changeView}
                            about={aboutMe}
                            notEdit={statusUser(status)}
                        />
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default PersonalInfo;
