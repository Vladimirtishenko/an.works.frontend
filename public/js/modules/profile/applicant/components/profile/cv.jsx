import React from "react";
import { Link } from "react-router-dom";

import CVPersonalInfo from "../../../common/components/not-editable/cv/cvPersonalInfo.jsx"; 
import CVLanguagesInfo from "../../../common/components/not-editable/cv/cvLanguagesInfo.jsx"; 
import CVTestsInfo from "../../../common/components/not-editable/cv/cvTestsInfo.jsx"; 
import CVSkillsInfo from "../../../common/components/not-editable/cv/cvSkillsInfo.jsx"; 
import CVRealocationInfo from "../../../common/components/not-editable/cv/cvRelocationInfo.jsx"; 
import CVExpirienceInfo from "../../../common/components/not-editable/cv/cvExpirienceInfo.jsx"; 
import CVEducationInfo from "../../../common/components/not-editable/cv/cvEducationInfo.jsx"; 
import CVRatingInfo from "../../../common/components/not-editable/cv/cvRatingInfo.jsx"; 
import CVHobbyInfo from "../../../common/components/not-editable/cv/cvHobbyInfo.jsx";  

import CvCommonAnonimHeader from "../../../common/components/not-editable/cv/cvCommonAnonimHeader.jsx";
import CvCommonHeaderOpen from "../../../common/components/not-editable/cv/cvCommonHeaderOpen.jsx";
import CVTabHeader from "../not-editable/cv/cvTabHeader.jsx";  

import rangeYears from "../../../../../databases/general/rangeOLdSection.json";

import {
    transformToYear
} from '../../../../../helpers/date.helper.js';
export default class CVComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0
        };
    }
    componentDidMount() {
        if (this.props.applicant && !this.props.applicant.knowledges)
            this.props.actions.getWorkSkills();
    }
    handleChangeTab(index) {
        if (index < 0 || index > 1) {
            return;
        }

        this.setState({
            tabIndex: index
        });
    }
    rangeDate(birthday){
        if(!birthday) return null;
        let fullAge = transformToYear(birthday);
        for(let i = 0,len = rangeYears.length; i < len; i++){
            if(rangeYears[i].from <= fullAge && fullAge <= rangeYears[i].to){
                return rangeYears[i];
            }
        }
        
    }
    render() {
        const { tabIndex } = this.state;
        const isYourCv = true,
        {applicant: 
            {   
                skillTestsHierarchy = [],
                knowledges = [],
                profile = {},
                profile:{
                    aboutMe:{
                        hobby = '',
                        waitings = '',
                        achivments = '',
                        about = ''
                    },
                profileId = '',
                firstName = '',
                lastName = '',
                email = '',
                contactInfo = [],
                gender = '',
                birthday = '',
                aboutMe = {},
                geography = {},
                experience = {},
                educational = {},
                positionLevel = '',
                positionTitle = '',
                mainSkill = '',
                skills = [],
                uid = ''
            },
            testsResults = [],
            statisticData = {},
            statisticData:{
                queueData = {},
                queueData:{
                    rank = '',
                    expiredDate = '',
                    goingToStart = '',
                    rate = '',
                    wishedSalary = ''
                } = {},
                openCount = ''
            }= {},
        } = {},
        oauth = {},
        history: { goBack },
        i18n:{
            translation = {}
        } = {}
        } = this.props,
        anonim = tabIndex === 1 ? false : true,
        yersOld = this.rangeDate(birthday);
        return (
            <div className="container">
                <div className="margin--b-30">
                    <span
                        onClick={goBack}
                        className="link link__go-back link__go-back_blue icon--Rating_arrow link--blue font--12 margin--sm-b-15"
                    >
                    
                        {translation.back}
                    </span>
                </div>
                <div className=" box--white box--rounded shadow--box">
                    {isYourCv && (
                        <div className="row">
                            <div className="col-12">
                                <CVTabHeader
                                    handleChangeTab={index =>
                                        this.handleChangeTab(index)
                                    }
                                    tabIndex={tabIndex}
                                    mockTabs={[translation.anonymousCv,translation.fullCv]}
                                />
                            </div>
                        </div>
                    )}
                    <div
                        className="row"
                    >
                        <div className="col-12">
                            <div className="margin--b-15 padding--md-15">
                                <div className="padding--15  padding--md-xc-45">
                                {
                                    anonim && (
                                        <CvCommonAnonimHeader
                                            wishedSalary={wishedSalary}
                                            positionLevel={positionLevel}
                                            positionTitle={positionTitle}
                                            mainSkill={mainSkill}
                                            rank={rank}
                                            geography={geography}
                                            gender={gender}
                                            anonim={anonim}
                                            knowledges={knowledges}
                                            yersOld={yersOld} 
                                        />
                                    )
                                    || (anonim != undefined && anonim == false) && (
                                        <CvCommonHeaderOpen
                                            birthday={birthday}
                                            knowledges={knowledges}
                                            email={email}
                                            gender={gender}   
                                            mainSkill={mainSkill}
                                            positionLevel={positionLevel}
                                            positionTitle={positionTitle}
                                            birthday={birthday}
                                            profileId={profileId}
                                            firstName={firstName}
                                            geography={geography}
                                            lastName={lastName}
                                            contactInfo={contactInfo}
                                            wishedSalary={wishedSalary}
                                            queueData={queueData}
                                            uid={uid}
                                        />
                                    ) 
                                    || <div>error</div>
                                }

                                </div>
                                <div className="fl fl--align-st fl--wrap padding--yc-15 padding--md-xc-30 word-break--word"> 
                                    <div className="fl fl--dir-col width--100 width--md-50 padding--xc-15">
                                        <CVPersonalInfo 
                                            waitings={waitings}
                                            achivments={achivments}
                                            about={about}
                                        />
                                        <CVLanguagesInfo
                                            knowledges={knowledges}
                                            skills={skills}
                                        />
                                        <CVTestsInfo
                                            rate={rate}
                                            skillTestsHierarchy={skillTestsHierarchy}
                                            testsResults={testsResults}
                                        />
                                        <CVSkillsInfo
                                            knowledges={knowledges}
                                            skills={skills}
                                        />
                                        <CVRealocationInfo 
                                            data={geography}
                                        />
                                    </div>
                                    <div className="fl fl--dir-col width--100 width--md-50 padding--xc-15">
                                        <CVExpirienceInfo
                                            data={experience.items}
                                            anonim={anonim}
                                        />
                                        <CVEducationInfo
                                            data={educational.items}
                                        />
                                        {
                                            rank && (
                                                <CVRatingInfo
                                                    rank={rank}
                                                    expiredDate={expiredDate}
                                                    openCount={openCount}
                                                    mainSkill={mainSkill}
                                                    knowledges={knowledges}
                                                    goingToStart={goingToStart}
                                                />
                                            )
                                        }
                                        {
                                            hobby && <CVHobbyInfo data={hobby} /> || null
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
