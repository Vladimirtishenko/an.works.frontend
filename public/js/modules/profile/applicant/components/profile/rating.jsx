import React from "react";
import { Link } from "react-router-dom";

import TableFilters from "../../../../../components/filters/tableFilters.jsx";
import FiltersComponent from "../../../../../components/filters/filters.jsx";
import { RatingTable } from "../../../common/components/not-editable/ratingTable/ratingTable.jsx";
import Pagination from "../../../common/components/pagination.jsx";
import {NotificationСabinet} from "../../../../../components/widgets/notification/notificationСabinet.jsx"
import Hint from '../../../common/components/hint/hint.jsx';
import rules from "../../../../../configuration/rules.json";
// Filters
import TechnologyChoise from "../../../../../components/filters/publicFilters/technologyChoise.jsx";

// Components
import Buttons from "./buttons/ratingButtons.jsx";

// Notifications
import Notification from "./notification/notification.jsx";

import ParticipateRatings from '../modals/participateRatings.jsx';
import NotHiredWorks from '../modals/notHiredWorks.jsx';

// Helpers
import {
    convertLimitToValue
} from '../../../../../helpers/filters/limit.helper.js'

import {
    setQueueLabel
} from '../../../../../helpers/queue.helper.js'

export default class RaitingComponent extends React.Component {
    constructor(props) {
        super(props);
        this.pageTopRef = React.createRef();
        this.state = {
            languages: [],
            participateRatings: false,
            notHired: false,
            mainTestResult: {}
        }

    }

    onScrollTop() {
        window.scrollTo(0, 0);
    }

    findTestResult(){
        const {
            applicant:{
                skillTestsHierarchy:{
                    tests = []
                } = {},
               testsResults = []
            }
        } = this.props;

        let mainTest = _.findIndex(tests, {level: 0}),
            mainResult = mainTest != -1 ? testsResults[_.findIndex(testsResults,{testUid: tests[mainTest].testLabel})] : {};
        this.setState({
            ...this.state,
            mainTestResult: mainResult
        });
    }



    componentDidUpdate(prevProps){

        const { applicant: { profile: {status: currentStatus} } } = this.props,
              { applicant: { profile: {status: prevStatus} } } = prevProps;

        if(currentStatus !== prevStatus){
            this.setState({
                ...this.state,
                participateRatings: false,
                notHired: false
            });
        }
        
        let comparingProps = _.isEqual(prevProps, this.props);

        const {applicant:{ profile:{progress}}} = this.props;

		if(!comparingProps) {
            this.findTestResult();
        }

        if(progress == 100 && this.state.mainTestResult && this.state.mainTestResult.result >= rules.minValueResultTest){
            this.checkHintRating();
        }
    }

    checkHintRating(){
        let storage = JSON.parse(localStorage.getItem('hint'));
        if(storage && storage.layoutParticipateRating) return;
        const { actions:{updateHintParticipateRating}} = this.props;
        updateHintParticipateRating(true);
    }

    changeModalVisibility(modal) {
        if(!modal && this.state[modal] == undefined) return;

        this.setState({
            ...this.state,
            [modal]: !this.state[modal]
        });
    }

    handlerHireThroughOtherService() {
        const { applicant: { profile: { profileId } }, actions: { updateProfileByComponent } } = this.props,
              data = {status: 'hiredNotInPlatform'};

            updateProfileByComponent(data, profileId);
    }
    onChangePaginatin(filters){
        const { actions: { getQueue }} = this.props;
        getQueue(null, filters);
    }

    render() {
        const {
            actions:{
                updateHintParticipateRating
            },
            hints:{
                hintParticipateRating
            },
            applicant: {
                filters,
                profile: {
                    geography,
                    mainSkill,
                    positionLevel,
                    positionTitle,
                    status,
                    progress,
                    profileId
                },
                knowledges,
                queue = {},
                queue: {queueFetch, totalCount, queueLabel } = {},
                skillTestsHierarchy
            },
            actions,
            youId = '',
            history: { goBack },
            i18n:{translation = {}} = {}
        } = this.props,
        { participateRatings, notHired,mainTestResult} = this.state,
        limitToValue = convertLimitToValue(filters),
        skill = setQueueLabel(queueLabel, mainSkill);

        return (
            <React.Fragment>
                <div className="container-fluid relative" ref={this.pageTopRef}>
                    <div className="row fl fl--align-c margin--b-30">
                        <div className="col-md-4 col-lg-3 margin--b-15 margin--md-b-0">
                            <Link
                                className="link link__go-back icon--Rating_arrow link__go-back_blue link--blue font--12 margin--sm-b-25 margin--md-b-15"
                                to={'/'}
                            >
                                {translation.back}
                            </Link>
                        </div> 
                        <TechnologyChoise
                            actions={actions}
                            positionLevel={positionLevel}
                            knowledges={knowledges}
                            skill={skill}
                        />
                        <Buttons 
                            status={status}
                            progress={progress}
                            changeModalVisibility={::this.changeModalVisibility}
                            updateHintParticipateRating={updateHintParticipateRating}
                            testResult={mainTestResult}
                        />
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-lg-3">
                            <FiltersComponent
                                actions={actions}
                                mainSkill={skill}
                                testHierarchy={skillTestsHierarchy}
                                knowledges={knowledges}
                                filters={filters}
                            />
                        </div>
                        <div className="col-md-8 col-lg-9">

                            <Notification {...this.props} /> 

                            <TableFilters
                                totalAmount={totalCount}
                                actions={actions}
                                filters={filters}
                            />

                            <div className="row-container scroll--x-auto margin--xc-0">
                                <RatingTable
                                    actions={actions}
                                    filters={filters}
                                    queue={queue}
                                    knowledges={knowledges}
                                    isCompany={false}
                                    skillTestsHierarchy={skillTestsHierarchy}
                                    youId={youId}
                                    i18n={this.props.i18n}
                                />
                            </div>

                            <div className="row-container fl fl--justify-end">
                                {
                                    (totalCount > limitToValue) && <Pagination
                                        count={totalCount}
                                        actions={::this.onChangePaginatin}
                                        filters={filters}
                                        wrapperClass="pagination margin--yc-10"
                                    /> || null
                                }
                            </div>
                        </div>
                    </div>
                    <div className="anchor" onClick={() => this.onScrollTop()}>
                        <span className={`icon icon--Rating_arrow rotate--90`} />
                    </div>
                </div>
                {
                    participateRatings &&
                    <ParticipateRatings
                        changeModalVisibility={::this.changeModalVisibility}
                        profileId={profileId}
                        {...actions}
                    /> || null
                }
                {
                    notHired &&
                    <NotHiredWorks
                        changeModalVisibility={::this.changeModalVisibility}
                        handlerHireThroughOtherService={::this.handlerHireThroughOtherService}
                    /> || null
                }
                {
                    hintParticipateRating && <Hint closeHint={()=>{updateHintParticipateRating(false)}} />
                }
            </React.Fragment>
        );
    }
}
