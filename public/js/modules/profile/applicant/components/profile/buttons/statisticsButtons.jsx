import React from 'react'
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)

export default class Buttons extends React.PureComponent {
    static defaultProps = {
        discussedClass: "pr margin--xl-l-60 margin--lg-l-40"
    }

    statusName(value){
        const {i18n:{translation = {}} = {}} = this.props;
        const statusNames = {
            hired: translation.hired,
            notHired: translation.notHired,
            waiting: translation.discussed
        };
        return statusNames[value]
    }

    render(){

        const {
            status,
            companyId,
            applicantId,
            callToUpdateHiringRow,
            i18n:{translation = {}} = {}
        } = this.props;

       switch(status) {
            case 'waiting':
                return (
                    <React.Fragment>
                        <button
                            onClick={e => {callToUpdateHiringRow(true, companyId, applicantId)}}
                            className="btn btn--primary padding--y-5 margin--b-5 font--10  btn--line-height-1-7 width--px-100 "
                        >
                            {translation.hired}
                        </button>
                        <button
                            onClick={e => {callToUpdateHiringRow(false, companyId, applicantId)}}
                            className="btn btn--primary-outline padding--y-5 font--10 btn--line-height-1-7 width--px-100"
                        >
                            {translation.notHired}
                        </button>
                    </React.Fragment>
                );
            default:
                return (
                    <span
                        className="font--color-blue font--uppercase font--10"
                    >
                        {this.statusName(status)}
                    </span>
                )
        }

    }
}
