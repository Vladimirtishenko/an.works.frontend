import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";
import rules from "../../../../../../configuration/rules.json";

function mapStateToProps(state) {
    return {
        i18n: state.i18n,
        hints: state.hints
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class Buttons extends React.PureComponent {

    render(){
        const {
            status,
            changeModalVisibility,
            updateHintParticipateRating,
            progress,
            testResult,
            i18n:{
                translation = {}
            } = {},
            hints:{
                hintParticipateRating
            }
        } = this.props;

        switch(status) {
            case 'pending':
                if (progress != 100 || !testResult) return null;
                if(testResult.result < rules.minValueResultTest) return null;
                return (
                    <div className="col-md-3 col-lg-4 fl fl--justify-lg-end fl--wrap margin--t-15 margin--lg-0 offset-md-4">
                        <button
                            className={`btn btn--primary font--14 btn--hired pointer padding--10 ${hintParticipateRating ? 'hint--block-active' : ''}`}
                            onClick={(e) => { changeModalVisibility('participateRatings'); updateHintParticipateRating(false) }}
                        >
                            {translation.participate}
                        </button>
                    </div>
                )
                break;
            case 'inPool':
                return (<div className="col-lg-4 fl fl--justify-lg-end fl--wrap margin--t-15 offset-md-4 margin--lg-0">
                            
                                <button className="btn btn--primary font--14 btn--hired pointer margin--b-10 margin--lg-b-10 margin--xl-b-0  margin--xl-r-10 padding--10 margin--md-r-10 margin--lg-r-0 margin--yc-5 margin--md-0">
                                    <Link to="/statistics" className="font--color-white">
                                        {translation.hiredInPlatform}
                                    </Link>
                                </button>
                            
                            <button
                                className="btn btn--white font--14 btn--hired btn--hired-white pointer padding--10"
                                onClick={(e) => { changeModalVisibility('notHired') }}
                            >
                                {translation.hiredNotInPlatform}
                            </button>
                        </div>)
                break;
            case 'inPoolExpired':
            case 'waitingApprove':
            case 'hiredNotInPlatform':
                return null;
                break;
        }

    }
}
