import React from 'react'

import StaticItem from '../../../../../components/form/staticItem.jsx';

import {Card} from '../../../common/components/card.dumb.jsx'

// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

// Not Editable
import Personal from '../not-editable/personal.jsx'
import Contact from '../../../common/components/not-editable/contact.jsx'

// Edatable
import PersonalEditable from '../editable/personal.jsx'
import ContactEditable from '../../../common/components/editable/contactCard.jsx'

import { ValidatorForm } from "../../../../../libraries/validation/index.js";

import {statusUser} from "../../../../../helpers/statusUser.helper.js";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";

@i18nMonthDecorator
class PersonalInfo extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            personal: false,
            contact: false
        }
    }

    changeView(event){

        let component = event && event.target && event.target.dataset && event.target.dataset.component || null;

        if(component){
            this.setState({
                [component]: !this.state[component]
            })
        }
    }

    submitPersonal(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            {profileId} = this.props.applicant.profile,
            date = serializedForm.birthday && Array.isArray(serializedForm.birthday) ? serializedForm.birthday.reverse() : '',
            birthday = date ? (new Date( date.join('/') )).valueOf() : '';

            serializedForm.birthday = birthday;

        if( _.isEqual(serializedForm, this.propsPersonalMapper(this.props.applicant.profile)) ){
            return this.setState({personal: false})
        }

        this.props.actions.updateProfileByComponent(serializedForm, profileId);

    }

    submitContact(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            {profileId} = this.props.applicant.profile,
            {contactInfo} = this.props.applicant && this.props.applicant.profile || {};

        if( _.isEqual(serializedForm.contactInfo, contactInfo) ){
            return this.setState({contact: false})
        }

        this.props.actions.updateProfileByComponent(serializedForm, profileId);

    }

    propsPersonalMapper(props){

        let personal = ['firstName', 'lastName', 'birthday', 'gender'],
            object = {};


        _.forEach(personal, (item, i) => {
            if(props[item]){
                object[item] = item == 'birthday' ? (new Date(props[item])).valueOf() : props[item];
            }
        })

        return object;


    }
    componentDidMount() {
        if (
            this.props.applicant &&
            !this.props.applicant.telegramRegistrationUrl
        ) {
            this.props.actions.isTelegramVerified();
            this.props.actions.getTelegramRegistrationUrl();
        }
    }

    componentDidUpdate(prevProps, prevStep){

        let prevPersonalInfo = this.propsPersonalMapper(prevProps.applicant.profile),
            nextPersonalInfo = this.propsPersonalMapper(this.props.applicant.profile),
            contactPrev = prevProps.applicant.profile && prevProps.applicant.profile.contactInfo,
            contactNext = this.props.applicant.profile && this.props.applicant.profile.contactInfo;

        if(!_.isEqual(prevPersonalInfo, nextPersonalInfo)){
            this.setState({
                personal: false
            })
        }

        if(!_.isEqual(contactPrev, contactNext)){
            this.setState({
                contact: false
            })
        }

    } 

    render() {

        let {personal, contact} = this.state,
            {
                i18n :{
                    translation = {}
                } = {},
                actions,
                applicant:{
                profile:{
                    status = '',
                    contactInfo = {}
                },
                    telegramRegistrationUrl,
                    telegramVerification,
                    telegramVerificationFailed
                }
            } = this.props;

        return (
            <React.Fragment>
            <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1 show--md">{translation.personalInfo}</h2>
            <div className="form">
                {personal ?
                    <ValidatorForm onSubmit={::this.submitPersonal}>
                        <PersonalEditable {...this.props} />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">{translation.save}</button>
                        </div>
                    </ValidatorForm> :
                    <Personal i18n={this.props.i18n} onEdit={::this.changeView} {...this.props} notEdit={statusUser(status)}/>  
                }
                {contact ?
                     <ValidatorForm onSubmit={::this.submitContact}>
                        <ContactEditable
                            actions={actions}
                            contactInfo={contactInfo}
                            telegramVerification={telegramVerification}
                            telegramRegistrationUrl={
                                telegramRegistrationUrl && telegramRegistrationUrl.url
                            }
                            telegramVerificationFailed={telegramVerificationFailed}
                        />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">{translation.save}</button>
                        </div>
                    </ValidatorForm> :
                    <Contact
                        onEdit={::this.changeView}
                        contactInfo={contactInfo}
                        notEdit={statusUser(status)}
                        telegramVerification={telegramVerification}
                        telegramVerificationFailed={telegramVerificationFailed}
                    />
                }
            </div>
            </React.Fragment>
        )
    }

}

export default PersonalInfo;
