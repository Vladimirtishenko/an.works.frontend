import React from "react";

import StaticItem from "../../../../../components/form/staticItem.jsx";

import { Card } from "../../../common/components/card.dumb.jsx";

// Helpers
import serialize from "../../../../../helpers/serialize.helper.js";
import { skillsComparing } from "../../../../../helpers/mapperSkills.helper.js";

// Not editable
import SkillsNotEditable from "../not-editable/skills.jsx";
import LanguageSkillsNotEditable from "../not-editable/language.jsx";

// Editable
import SkillsEditable from "../editable/skills.jsx";
import LanguageSkillsEditable from "../editable/languages.jsx";
import SkillsDidntFind from "../modals/skillsDidntFind.jsx"
import {
    mapperSkillsLabel,
    extendsSingleSkill
} from "../../../../../helpers/mapperSkills.helper.js";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import {statusUser} from "../../../../../helpers/statusUser.helper.js";
class Skills extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            technicalSkills: false,
            languageSkills: false,
            isSkillsPopupOpen: false
        };
    }
    handleChangeSkillsPopupStatus() {
        this.setState({
            ...this.state,
            isSkillsPopupOpen: !this.state.isSkillsPopupOpen
        });
    }
    changeView(event) {
        let component =
            (event &&
                event.target &&
                event.target.dataset &&
                event.target.dataset.component) ||
            null;

        if (component) {
            this.setState({
                [component]: !this.state[component]
            });
        }
    }

    componentDidMount() {
        if (this.props.applicant && !this.props.applicant.knowledges)
            this.props.actions.getWorkSkills();
    }

    unionLanguageSkills(data) {
        let { knowledges, profile } = this.props.applicant || null,
            { skills: instaledSkills } = profile || {},
            prevSkills = mapperSkillsLabel(instaledSkills, knowledges, [
                { isMain: false },
                { isMain: true }
            ]),
            unionSkills = [
                ...data.skills,
                ...prevSkills.filter(s => s.isLang === false)
            ];

        return unionSkills;
    }

    unionTechnicalSkills(data) {
        let { knowledges, profile } = this.props.applicant || null,
            { skills: instaledSkills } = profile || {},
            prevSkills = mapperSkillsLabel(instaledSkills, knowledges, [
                { isLang: true }
            ]),
            unionSkills = _.unionWith(data.skills, prevSkills, _.isEqual);

        return unionSkills;
    }

    submitTechnica(event){
        let form = event && event.target;

        if (!form) return;
        let kind = 'unionTechnicalSkills';
        

        let serializedForm = serialize(form),
            {
                knowledges,
                profile: { profileId }
            } = this.props.applicant,
            skills = skillsComparing(serializedForm, knowledges);
        let skillsUnion = this[kind](skills);
        this.setState({
            technicalSkills: false
        });
        this.props.actions.updateProfileByComponent(
            {
                skills: skillsUnion,
                positionTitle: serializedForm.positionTitle,
                positionLevel: serializedForm.positionLevel,
                mainSkill: serializedForm.skills.mainSkill
            },
            profileId
        );

        this.setState({
            technicalSkills: false
        });
    }
    submitLanguage(event){
        let form = event && event.target;

        if (!form) return;
        let kind = 'unionLanguageSkills';
        

        let serializedForm = serialize(form),
            {
                knowledges,
                profile: { profileId }
            } = this.props.applicant,
            skills = skillsComparing(serializedForm, knowledges);
        let skillsUnion = this[kind](skills);
        this.setState({
            languageSkills: false
        });

        this.props.actions.updateProfileByComponent(
            {
                skills: skillsUnion
            },
            profileId
        );
    }
    componentDidUpdate(prevProps, prevStep) {
        let prevSkills = prevProps.applicant.profile && {
                skillsUnion: prevProps.applicant.profile.skillsUnion,
                positionTitle: prevProps.applicant.profile.positionTitle,
                positionLevel: prevProps.applicant.profile.positionLevel,
                mainSkill: prevProps.applicant.profile.mainSkill
            },
            nextSkills = this.props.applicant.profile && {
                skillsUnion: this.props.applicant.profile.skillsUnion,
                positionTitle: this.props.applicant.profile.positionTitle,
                positionLevel: this.props.applicant.profile.positionLevel,
                mainSkill: this.props.applicant.profile.mainSkill
            };

        if (!_.isEqual(prevSkills, nextSkills)) {
            this.setState({
                technicalSkills: false
            });
        }
    }

    render() {
        let { skills: instaledSkills } =
            (this.props.applicant && this.props.applicant.profile) || {},
            { technicalSkills, languageSkills ,isSkillsPopupOpen} = this.state,
            { knowledges, profile, profile:{status = ''} } = this.props.applicant || null,
            mainSkill = extendsSingleSkill(
                profile && profile.mainSkill,
                knowledges
            ),
            skillList = mapperSkillsLabel(instaledSkills, knowledges, [
                { isLang: false }
            ]),
            skillLanguage = mapperSkillsLabel(instaledSkills, knowledges, [
                { isMain: false, isLang: true }
            ]),
            {i18n:{translation = {}} = {}} = this.props;

        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1 show--md">
                    {translation.skills}
                </h2>
                <div className="form">
                    {(technicalSkills && ( 
                        <ValidatorForm
                            onSubmit={::this.submitTechnica}
                        >
                            <SkillsEditable 
                                i18n={this.props.i18n}
                                knowledges={knowledges}
                                profile={profile}
                                openSkillPopUp={::this.handleChangeSkillsPopupStatus}
                            />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </ValidatorForm> 
                    )) || (
                        <SkillsNotEditable
                            i18n={this.props.i18n}
                            onEdit={::this.changeView}
                            skills={skillList}
                            mainSkill={mainSkill}
                            positionTitle={profile.positionTitle}
                            positionLevel={profile.positionLevel}
                            notEdit={statusUser(status)}
                        />
                    )}
                    {(languageSkills && (
                        <ValidatorForm
                            onSubmit={::this.submitLanguage}
                        >
                            <LanguageSkillsEditable
                                i18n={this.props.i18n}
                                knowledges={knowledges}
                                profile={profile}
                            />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </ValidatorForm>
                    )) || (
                        <LanguageSkillsNotEditable
                            i18n={this.props.i18n}
                            onEdit={::this.changeView}
                            skills={skillLanguage}
                            notEdit={statusUser(status)}
                        />
                    )}
                </div>
                {isSkillsPopupOpen && (
                    <SkillsDidntFind
                        closePopup={::this.handleChangeSkillsPopupStatus}
                    />
                )}
            </React.Fragment>
        );
    }
}

export default Skills;
