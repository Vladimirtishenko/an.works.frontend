import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Left from "../layouts/leftAside.jsx";

import StatusHired from "../modals/statusContact.jsx";
import HireWork from "../modals/hiredWork.jsx";
import InfoCompany from "../modals/InfoCompany.jsx";

import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js'

import {
    dmyDate
} from '../../../../../helpers/date.helper.js'

import Notification from "./notification/notification.jsx";

import Buttons from './buttons/statisticsButtons.jsx'
class Statistics extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            popUpHired: false,
            confirmPopUpHired: false,
            popUpInfoCompany: false
        };
    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { actions: { getStatisticData }, applicant: { profile: { profileId }, filters } } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getStatisticData(profileId, filtersNormalizedWithNewOrderBy);

    }
    componentDidMount(){
        this.hiringStatusСheck();
    }
    componentDidUpdate(prevProps){
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.hiringStatusСheck();
        }
    }
    
    hiringStatusСheck(){
        const {
            applicant:{
                statisticData:{
                    contactsList = []                    
                } = {}
            } = {}
        } = this.props;
        let find = _.findIndex(contactsList, {applicantAnswer: "waiting", companyAnswer: "hired"})

        if(find == -1) return;

        this.timer = setTimeout(()=>{
            this.setState({        
                ...this.state,
                popUpHired: true,
                companyStatusHired: {
                    ...contactsList[find].additionalData.recruiter,
                    createdDate: contactsList[find].createdDate,
                    name: contactsList[find].additionalData.company.name,
                    applicantId: contactsList[find].applicantId,
                    companyId: contactsList[find].companyId,
                    callToUpdateHiringRow: ::this.callToUpdateHiringRow,
                    openPopUpHired: ::this.changePopUpConfirmHired,
                    closePopUpContact: ::this.closePopUpStatusHired
                }
            })
        },2000); 
    }
    componentWillUnmount(){
        clearTimeout(this.timer);
    }
    closePopUpStatusHired(){
        this.setState({
            ...this.state,
            popUpHired: false
        })
    }
    closePopUpAll(){
        this.setState({
            ...this.state,
            popUpHired: false,
            confirmPopUpHired: false
        })
    }
    changePopUpConfirmHired(status,info){
        this.setState({
            ...this.state,
            configHired:info,
            confirmPopUpHired: status
        })
    }
    popUpInfoCompanyCLose(){
        this.setState(state=>{
            return {
                ...state,
                popUpInfoCompany: !this.state.popUpInfoCompany
            }
        })
    }
    popUpCompany(item){
        this.setState({
            ...this.state,
            companyInfo : item
        });
        this.popUpInfoCompanyCLose();
    }

    render() {
        const {
            applicant: {
                statisticFetch,
                profile:{
                    status
                },
                statisticData: {
                    contactsList = [],
                } = {},
                filters
            },
            i18n:{
                translation = {}
            } = {}
        } = this.props,
        justOrderBy = orderBy(filters);
        let {companyStatusHired,popUpHired, confirmPopUpHired, configHired,popUpInfoCompany,companyInfo} = this.state;

        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">{translation.statistics}</h2>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-lg-12 padding--0">

                            <Notification {...this.props} /> 

                            <div className="card">

                                <table className="respons-flex-table word-break--word">
                                    <thead className="respons-flex-table__thead respons-flex-table__thead--lg-b relative z--1 shadow--table">
                                        <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                                            <th className="
                                                respons-flex-table__th
                                                respons-flex-table--lg-w-20
                                                font--left
                                            ">
                                                <span
                                                    // onClick={::this.sortingTable}
                                                    // data-sort="date"
                                                    // data-value={justOrderBy.date || ""}
                                                    // className={`${orderingByClass(justOrderBy.date)}`}
                                                >
                                                    {translation.openingDate}
                                                </span>
                                            </th>
                                            <th className="
                                                respons-flex-table__th
                                                respons-flex-table--lg-w-15
                                                font--left
                                            ">
                                                <span
                                                    // onClick={::this.sortingTable}
                                                    // data-sort="company"
                                                    // data-value={justOrderBy.company || ""}
                                                    // className={`${orderingByClass(justOrderBy.company)}`}
                                                >
                                                    {translation.company}
                                                </span>
                                            </th>
                                            <th className="
                                                respons-flex-table__th
                                                respons-flex-table--lg-w-20
                                                font--left
                                            ">
                                                {translation.recruiterName}
                                            </th>
                                            <th className="
                                                respons-flex-table__th
                                                respons-flex-table--lg-w-30
                                                font--left
                                            ">
                                                {translation.contactDetails}
                                            </th>
                                            <th className="
                                                respons-flex-table__th
                                                respons-flex-table--lg-w-15
                                                font--right
                                                fl
                                                fl--align-c
                                                fl--justify-end
                                            ">
                                                <span
                                                    // onClick={::this.sortingTable}
                                                    // data-sort="status"
                                                    // data-value={justOrderBy.status || ""}
                                                    // className={`${orderingByClass(justOrderBy.status)}`}
                                                >
                                                    {translation.condition}
                                                </span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className="respons-flex-table__tbody">
                                        {contactsList.length && contactsList.map((item ,i)=> {
                                            return this._getTableRow(item, i);
                                        }) || 
                                            (<tr className="respons-flex-table__tr">
                                                <td className="respons-flex-table__td respons-flex-table__td--no-before">{translation.contactNotOpened}</td>
                                            </tr>)
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    popUpHired && (
                        <StatusHired 
                            data={companyStatusHired}
                        />
                    ) || null 
                }
                {
                    confirmPopUpHired && (
                        <HireWork
                            status={status}
                            closePopUp={::this.changePopUpConfirmHired}
                            configHired={configHired}
                            confirmHired={::this.callToUpdateHiringRow}
                            closePopUpAll={::this.closePopUpAll}
                        />
                    ) || null
                }
                {
                    popUpInfoCompany && (
                        <InfoCompany
                            companyInfo={companyInfo}
                            closePopUp={::this.popUpInfoCompanyCLose}
                        />
                    )
                }
            </React.Fragment>
        );
    }

    _getTableRow(item = {}, i)
    {
        let {
            additionalData: {
                buyerEmail = '',
                recruiter: {
                    firstName = '',
                    lastName = '',
                    contactInfo: {
                        phoneNumber = [],
                        telegramAccount,
                        viberAccount,
                        linkedinAccount
                    } = {}
                } = {},
                company: {
                    name = ''
                } = {}
            }
        } = item,
        {i18n:{translation = {}} = {}} = this.props;

        item.status = item.applicantAnswer;
        return  (
            <tr className="respons-flex-table__tr font--color-primary fl--align-lg-c" key={i} >
                <td
                    data-title={translation.openingDate}
                    className=" respons-flex-table__td 
                                respons-flex-table__td-lg--no-title
                                respons-flex-table--w-100
                                respons-flex-table--md-w-30
                                respons-flex-table--lg-w-20
                                respons-flex-table--font-14
                                fl--order-1
                                fl--order-md-5
                                fl--order-lg-1
                                font--color-primary">
                    {dmyDate(item.createdDate)}
                </td>
                <td
                    data-title={translation.company}
                    className=" respons-flex-table__td
                                respons-flex-table__td-lg--no-title
                                respons-flex-table--w-40
                                respons-flex-table--md-w-30
                                respons-flex-table--lg-w-15
                                fl--order-3
                                fl--order-md-4
                                fl--order-lg-1
                                font--color-blue
                                respons-flex-table--font-14
                                padding--r-10
                            "
                    >
                    <span className="pointer-text fl--self-st" onClick={()=>{
                        ::this.popUpCompany(item)
                    }}>
                        {name}
                    </span>
                </td>
                <td
                    data-title={translation.nameRecruiter}
                    className=" respons-flex-table__td
                                respons-flex-table__td-lg--no-title
                                respons-flex-table--w-60
                                respons-flex-table--md-w-30
                                respons-flex-table--lg-w-20
                                respons-flex-table--font-14
                                padding--r-10
                                fl--order-2
                                fl--order-md-1
                                fl--order-lg-1">
                    {`${firstName} ${lastName}`}
                </td>
                <td
                    data-title={translation.contactDetails}
                    className=" respons-flex-table__td
                                respons-flex-table__td-lg--no-title
                                respons-flex-table--w-100
                                respons-flex-table--md-w-50
                                respons-flex-table--lg-w-30
                                respons-flex-table--font-12
                                padding--r-10
                                fl--order-3
                                fl--order-md-2
                                fl--order-lg-1">

                    <ul className="list-contact">
                        {
                            phoneNumber && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                    <li className="list-contact__name-contact width--40">{translation.phone}</li>
                                        <li className="list-contact__value-contact">
                                            {phoneNumber[0]}
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                        {
                            buyerEmail && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">{translation.email}:</li>
                                        <li className="list-contact__value-contact">
                                            <a href={`mailto:${buyerEmail}`} className="link font--color-blue">
                                                {buyerEmail}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                        {
                            telegramAccount && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">{translation.telegram}:</li>
                                        <li className="list-contact__value-contact">
                                            <span className="font--color-blue">
                                                {telegramAccount}
                                            </span>
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                        {
                            viberAccount && (
                            <li className="margin--b-5">
                                <ul className="fl">
                                    <li className="list-contact__name-contact width--40">{translation.viber}:</li>
                                    <li className="list-contact__value-contact">
                                        <span className="font--color-blue">
                                            {viberAccount}
                                        </span>
                                    </li>
                                </ul>
                            </li>
                            )
                        }
                        {
                            linkedinAccount && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">{translation.linkedIn}:</li>
                                        <li className="list-contact__value-contact">
                                            <a href={linkedinAccount} className="link font--color-blue">
                                                {linkedinAccount}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                        
                    </ul>
                </td>
                <td
                    className=" respons-flex-table__td
                                respons-flex-table--w-50
                                respons-flex-table--md-w-20
                                respons-flex-table--lg-w-15
                                respons-flex-table--font-14
                                respons-flex-table__td--no-before
                                fl--order-3
                                fl--order-md-3
                                fl--order-lg-1
                                font--color-secondary
                                font--md-center
                                fl--align-lg-end
                            ">
                        <span className="fl fl--align-c margin--b-15 font--12 show--lg-r-fl width--px-100">
                            {translation.condition}
                            {/* <span className="pseudo-icon__info pseudo-icon__info--inline margin--l-10"></span> */}
                        </span>
                        <Buttons
                            {...item}
                            callToUpdateHiringRow={::this.callToUpdateHiringRow}
                        />
                </td>
            </tr>
        );
    }

    callToUpdateHiringRow(isHired, companyId, applicantId)
    {
        let data = {
            companyId: companyId,
            applicantId: applicantId,
            applicantAnswer: isHired === true ? 'hired' : 'notHired'// ONe of ['hired', 'notHired']
        };

        this.props.actions.updateHiringStatus(data);
    }
}

export default Statistics;
