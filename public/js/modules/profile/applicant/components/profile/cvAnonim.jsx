import React from "react";
import { Link } from "react-router-dom";

import CVPersonalInfo from "../../../common/components/not-editable/cv/cvPersonalInfo.jsx"; 
import CVLanguagesInfo from "../../../common/components/not-editable/cv/cvLanguagesInfo.jsx"; 
import CVTestsInfo from "../../../common/components/not-editable/cv/cvTestsInfo.jsx"; 
import CVSkillsInfo from "../../../common/components/not-editable/cv/cvSkillsInfo.jsx"; 
import CVRealocationInfo from "../../../common/components/not-editable/cv/cvRelocationInfo.jsx"; 
import CVExpirienceInfo from "../../../common/components/not-editable/cv/cvExpirienceInfo.jsx"; 
import CVEducationInfo from "../../../common/components/not-editable/cv/cvEducationInfo.jsx"; 
import CVRatingInfo from "../../../common/components/not-editable/cv/cvRatingInfo.jsx"; 
import CVHobbyInfo from "../../../common/components/not-editable/cv/cvHobbyInfo.jsx";  
import CVCommonAnonimHeader from "../../../common/components/not-editable/cv/cvCommonAnonimHeader.jsx";
import CVTabHeader from "../not-editable/cv/cvTabHeader.jsx";  

export default class CVComponent extends React.Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {

        if (this.props.applicant && !this.props.applicant.knowledges)
            this.props.actions.getWorkSkills();
    }
    
    render() {
        const  {
            applicant:{
                skillTestsHierarchy = [],
                knowledges = [],
                cvContact:{
                    aboutMe:{
                        hobby = '',
                        waitings = '',
                        achivments = '',
                        about = ''
                    } = {},
                    gender = '',
                    birthday = '',
                    geography = {},
                    geography:{
                        city = '',
                        relocation:{
                            local = ''
                        } = {}
                    } = {},
                    experience = {},
                    educational = {},
                    positionLevel = '',
                    positionTitle = '',
                    mainSkill = '',
                    skills = [],
                    testsResults = [],
                    queueData:{
                        rank = '',
                        expiredDate = '',
                        goingToStart = '',
                        rate = ''
                    } = {},
                    wishedSalary = '',
                    openCount = '',
                    yersOld = {}
                } = {}
            } = {},
            history:{
                goBack
            }
        }= this.props,
        anonim = true;

        return (
            <div className="container">
                <div className="margin--b-30">
                    <span
                        onClick={goBack}
                        className="link link__go-back link__go-back_blue icon--Rating_arrow link--blue font--12 margin--sm-b-15"
                    >
                        Назад
                    </span>
                </div>
                <div className=" box--white box--rounded shadow--box">
                    <div
                        className="row"
                    >
                        <div className="col-12">
                            <div className="margin--b-15 padding--md-15">
                                <div className="padding--15">
                                    {
                                        city && (
                                            <CVCommonAnonimHeader
                                                wishedSalary={wishedSalary}
                                                positionLevel={positionLevel}
                                                positionTitle={positionTitle}
                                                mainSkill={mainSkill}
                                                birthday={birthday}
                                                rank={rank}
                                                geography={geography}
                                                gender={gender}
                                                anonim={anonim}
                                                knowledges={knowledges}
                                                yersOld = {yersOld}
                                            />
                                        )
                                    }
                                    
                                </div>
                                <div className="fl fl--align-st fl--wrap padding--yc-15 padding--md-xc-30 word-break--word">
                                    <div className="fl fl--dir-col width--100 width--md-50 padding--xc-15">
                                        <CVPersonalInfo  
                                            waitings={waitings}
                                            achivments={achivments}
                                            about={about}
                                        />

                                        <CVLanguagesInfo
                                            knowledges={knowledges}
                                            skills={skills}
                                        />
                                        
                                        { skillTestsHierarchy && 
                                            (<CVTestsInfo
                                                rate={rate}
                                                skillTestsHierarchy={skillTestsHierarchy}
                                                testsResults={testsResults}
                                            />)
                                        }
                                          <CVSkillsInfo
                                                knowledges={knowledges}
                                                skills={skills}
                                            />
                                        {
                                            local && (
                                                <CVRealocationInfo 
                                                    data={geography} 
                                                />
                                            )   
                                        }  
                                    </div>
                                    <div className="fl fl--dir-col width--100 width--md-50 padding--xc-15">
                                        <CVExpirienceInfo 
                                            data={experience.items}
                                            anonim={anonim}
                                        />
                                        <CVEducationInfo
                                            data={educational.items}
                                        />
                                       <CVRatingInfo
                                            rank={rank}
                                            expiredDate={expiredDate}
                                            openCount={openCount}
                                            mainSkill={mainSkill}
                                            knowledges={knowledges}
                                            goingToStart={goingToStart}
                                        />
                                        {
                                            hobby && <CVHobbyInfo data={hobby} /> || null
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        );
    }
}
