import React from "react";

import StaticItem from "../../../../../components/form/staticItem.jsx";
import { Card } from "../../../common/components/card.dumb.jsx";

// Editable
import ExperienceEditable from "../editable/experience.jsx";

// Not-Editable
import ExperienceNotEditable from "../not-editable/experience.jsx";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";
import serialize from "../../../../../helpers/serialize.helper.js";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import {statusUser} from "../../../../../helpers/statusUser.helper.js";

@i18nMonthDecorator
class Experience extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            experience: false
        };
    }

    componentDidMount() {
        if (this.props.applicant && !this.props.applicant.knowledges)
            this.props.actions.getWorkSkills();
    }

    submit(event) {
        event.preventDefault();

        let form = event.target,
            serializedForm = serialize(form),
            transformObjectToArray = _.values(serializedForm.experience),
            {
                applicant: {
                    profile: {
                        profileId,
                        experience: { it = 0, other = 0 }
                    }
                },
                actions: { updateProfile }
            } = this.props;

        updateProfile(
            {
                experience: {
                    items: transformObjectToArray,
                    it: it,
                    other: other
                }
            },
            profileId
        );

        this.setState({
            experience: false
        });
    }

    changeView(event) {
        this.setState({
            experience: !this.state.experience
        });
    }

    verifyStatus(items = []) {
        if (!items.length) {
            return false;
        }

        const index = _.findIndex(items, { status: false });

        if (index > -1) {
            return false;
        }

        return true;
    }

    render() {
        let { experience } = this.state,
            {
                applicant: {
                    profile: {
                        experience: { items = [] },
                        status = ''
                    },
                    knowledges
                },
                i18n: { translation }
            } = this.props;

        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1 show--md">
                    {translation.experience}
                </h2>
                <div className="form">
                    {((experience || !this.verifyStatus(items) && statusUser(status)) && (
                        <ValidatorForm
                            data-form="unionTechnicalSkills"
                            onSubmit={::this.submit}
                        >
                            <ExperienceEditable {...this.props} />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </ValidatorForm>
                    )) || (
                        <ExperienceNotEditable
                            i18n={this.props.i18n}
                            onEdit={::this.changeView}
                            knowledges={knowledges}
                            experience={items}
                            translations={translation}
                            notEdit={statusUser(status)}
                        />
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default Experience;
