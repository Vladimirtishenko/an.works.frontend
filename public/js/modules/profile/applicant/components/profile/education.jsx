import React from "react";
import { Card } from "../../../common/components/card.dumb.jsx";

// Not-Editable
import EducationNotEditable from "../not-editable/education.jsx";

// Editable
import EducationEditable from "../editable/education.jsx";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";
import serialize from "../../../../../helpers/serialize.helper.js";
import UniversityDidntFind from "../modals/universityDidntFind.jsx";
import {
    transformToTotal
} from "../../../../../helpers/date.helper.js";
import { ValidatorForm } from "../../../../../libraries/validation/index.js";
import {statusUser} from "../../../../../helpers/statusUser.helper.js";

@i18nMonthDecorator
class Education extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            education: false,
            isUniversityDidntFindPopupOpen: false
        };
    }
    handleChangeUniversityDidntFindPopupStatus() {
        this.setState({
            ...this.state,
            isUniversityDidntFindPopupOpen: !this.state.isUniversityDidntFindPopupOpen
        });
    }
    submit() {
        event.preventDefault();

        let form = event.target,
            serializedForm = serialize(form),
            transformObjectToArray = _.values(serializedForm.education),
            {
                applicant: {
                    profile: { profileId }
                },
                actions: { updateProfile }
            } = this.props,
            { certificate, university } = transformToTotal(
                transformObjectToArray
            );

        updateProfile(
            {
                educational: {
                    items: transformObjectToArray,
                    university: university,
                    certificate: certificate
                }
            },
            profileId
        );

        this.setState({
            education: false
        });
    }

    changeView(event) {
        this.setState({
            education: !this.state.education
        });
    }

    verifyStatus(items = []) {
        if (!items.length) {
            return false;
        }

        const index = _.findIndex(items, { status: false });

        if (index > -1) {
            return false;
        }

        return true;
    }

    render() {
        let { education,isUniversityDidntFindPopupOpen } = this.state,
            { applicant: { profile: { educational: {items = []}, status = '' }}, i18n: { translation } } =this.props;
        return (
            <React.Fragment>
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1 show--md">
                    {translation.education}
                </h2>
                <div className="form">
                    {(( education || !this.verifyStatus(items) && statusUser(status)) && (
                        <ValidatorForm onSubmit={::this.submit} className="form" data-form="unionTechnicalSkills">
                            <EducationEditable {...this.props} openEducationPopUp={::this.handleChangeUniversityDidntFindPopupStatus} />
                            <div className="fl fl--justify-c fl--align-c">
                                <button className="btn btn--primary font--14 padding--y-10 padding--x-20">
                                    {translation.save}
                                </button>
                            </div>
                        </ValidatorForm>
                    )) || (
                        <EducationNotEditable
                            i18n={this.props.i18n}
                            onEdit={::this.changeView}
                            education={items}
                            translations={translation}
                            notEdit={statusUser(status)}
                        />
                    )}
                </div>
                {isUniversityDidntFindPopupOpen && (
                    <UniversityDidntFind
                        closePopup={::this.handleChangeUniversityDidntFindPopupStatus}
                    />
                )}
            </React.Fragment>
        );
    }
}

export default Education;
