import React from "react";
import Input from "../../../../../components/form/text.jsx";
import uniqid from "uniqid";
import ParentStep from "../../../common/components/parent/main.jsx";

import { Card } from "../../../common/components/card.dumb.jsx";
import { DeleteButton } from "../../../../../components/form/deleteButton.jsx";

class Employer extends ParentStep {
    constructor(props) {
        super(props);

        let { employer } = props,
            employerClone = Array.isArray(employer) ? employer.clone() : [];

        this.state = {
            extraEmployer: employerClone
                ? ::this.setNormalizeData(employerClone)
                : []
        };
    }

    render() {
        let { extraEmployer } = this.state;

        return (
            <Card
                icon="unwanted_employer"
                title="Нежелательный работодатель"
                cardPadding="padding--15 padding--md-30"
            >
                {(extraEmployer &&
                    extraEmployer.length &&
                    extraEmployer.map((item, i) => {
                        let value = item.data ? item.data : "";

                        return (
                            <div
                                className="margin--b-20 fl fl--wrap"
                                key={item.id}
                            >
                                <Input
                                    wrapperClass="row no-gutters padding--0 fl--align-c col-lg-7"
                                    labelClass="label form__label font--14 col-md-4 col-lg-4"
                                    name="geography.employer[]"
                                    value={value}
                                    label="Неж. работодатель"
                                    inputWrapperClass="col-md-4 col-lg-7 margin--auto-md-l"
                                    inputClass="input form__input"
                                />
                                <DeleteButton
                                    className="font--12 padding--xc-5 fl--self-c margin--lg-l-25"
                                    data-id={item.id}
                                    data-mark="extraEmployer"
                                    onClick={::this.removeFiled}
                                    text="Удалить"
                                />
                            </div>
                        );
                    })) ||
                    null}
                <div className="row no-gutters margin--b-20">
                    <div className="offset-md-3 col-md-9">
                        <span
                            data-mark="extraEmployer"
                            className="padding--r-20 link"
                            onClick={::this.appendField}
                        >
                            <span className="icon icon--add" />
                            Добавить нежелательного работодателя
                        </span>
                    </div>
                </div>
            </Card>
        );
    }
}

export default Employer;
