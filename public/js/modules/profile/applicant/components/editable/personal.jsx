import React from 'react'

// Form Componets
import Input from '../../../../../components/form/text.jsx';
import Radio from '../../../../../components/form/radio.jsx';
import DateCustom from '../../../../../components/form/dateCustom.jsx';

// Componets
import {Card} from '../../../common/components/card.dumb.jsx'

import {
    separateDate,
    preparationMonth
} from '../../../../../helpers/date.helper.js'

import {
    comparingGenderValue
} from '../../../../../helpers/gender.helper.js'

class Personal extends React.Component {

    static defaultProps = {
        wrapperClass:"row fll--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-3",
        inputWrapperClass: "col-md-7 col-lg-6 col-xl-5 padding--md-0"
    }

    render(){ 

        const {
                i18n: { translation } = {},
                applicant: { profile } = {}
            } = this.props,
            { birthday = null } = profile,
            date = separateDate(birthday),
            monthPrepare = preparationMonth(translation.month);

        return (
            <Card icon="personal" title={translation.personalInfo} cardPadding="padding--15 padding--md-30">
                <Input
                    validators={['required', 'isName','maxStringLength:50']}  
                    errorMessages={[ translation.errorMessageRquired, translation.errorMessageName, translation.errorMessageMaxLeng50 ]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-10`}
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.inputWrapperClass}
                    inputClass="input form__input"
                    value={profile.firstName}
                    label={translation.firstName+'*'}
                    name="firstName"
                >
                </Input>
                <Input
                    validators={['required', 'isName' ,'maxStringLength:50']}
                    errorMessages={[ translation.errorMessageRquired, translation.errorMessageName, translation.errorMessageMaxLeng50 ]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-20`}
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.inputWrapperClass}
                    inputClass="input form__input"
                    value={profile.lastName}
                    label={translation.lastName+'*'}
                    name="lastName"
                />

                <Radio
                    validators={['radioRequired']}
                    errorMessages={[translation.errorMessageRadio]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-20`}
                    labelClass={this.props.labelClass}
                    label={translation.gender+'*'}
                    name="gender"
                    value={comparingGenderValue(profile.gender, 'fl fl--align-c')}
                    wrapperClassRadio="padding--md-0"
                    wrapRadio={this.props.inputWrapperClass}
                />

                <DateCustom
                    validators={['required', 'isDate']}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageisDateRange]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-10"`}
                    labelClass={this.props.labelClass}
                    coverClass={this.props.inputWrapperClass}
                    day={{
                        name: "birthday[]",
                        value: date.day
                    }}
                    month={{
                        name: "birthday[]",
                        value: date.month,
                        options: monthPrepare,
                        static: 'Месяц',
                    }}
                    year={{
                        name: "birthday[]",
                        value: date.year
                    }}
                    label={translation.birthday+'*'}
                />
            </Card>
        )
    }
}

export default Personal;
