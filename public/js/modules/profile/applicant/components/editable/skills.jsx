import React from "react";

import { mapperSkills } from "../../../../../helpers/mapperSkills.helper.js";
import { Card } from "../../../common/components/card.dumb.jsx";

import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import MultySelect from "../../../../../components/form/multiselect.jsx";
import Tooltip from "../../../../../components/widgets/toolTip.jsx";

import levelDictionary from "../../../../../databases/general/level.json";
import positionDictionary from "../../../../../databases/general/position.json";
export default class Skills extends React.Component {
    constructor(props) {
        super(props);
        let {
            profile:{
                mainSkill = ''
            } = {}
        } = props;
        this.state = {
            ...props,
            mainSkillChange: mainSkill
        } 
    }
    static defaultProps = {
        wrapperClass:"row fl--align-c",
        labelClass:"label form__label font--14 col-10 col-md-4 col-lg-3",
        className: "col-md-6 col-xl-5 padding--md-0"
    }
    onChaneMainSkill(value){
        this.setState({
            mainSkillChange: value.label
        })
    }
    render() {
        let {
            i18n: { translation = {} } = {},
            knowledges = [],
            profile = {},
            profile:{
                positionTitle = "",
                positionLevel = "",
                mainSkill = ""
            },
            openSkillPopUp } = this.props,
            otherSkills = mapperSkills(
                profile,
                knowledges,
                [],
                [{ isLang: false }],
                "array"
            );
        let {mainSkillChange} = this.state;
        return (
            <Card
                icon="details"
                title={translation.details}
                cardPadding="padding--15 padding--md-30"
            >
                <Combobox
                    justState
                    validators={["combobox"]}
                    errorMessages={[translation.errorMessageCombobox]}
                    name="positionTitle"
                    wrapperClass={`${this.props.wrapperClass} margin--b-10 combobox`}
                    labelClass={`${this.props.labelClass} order-1`} 
                    className={`${this.props.className} order-2 order-md-1`}
                    label={translation.workPosition+'*'}
                    list={positionDictionary}
                    value={positionTitle}
                >
                    <Tooltip
                        wrapper="
                        margin--b-10
                        margin--md-b-0
                        col-2
                        order-1
                    "
                        description={translation.tooltipPosition}
                    />
                </Combobox>
                <Combobox
                    justState
                    validators={["combobox"]}
                    errorMessages={[translation.errorMessageCombobox]}
                    name="positionLevel"
                    wrapperClass={`${this.props.wrapperClass} margin--b-20 combobox`}
                    labelClass={`${this.props.labelClass} order-1`}
                    className={`${this.props.className} order-2 order-md-1`}
                    label={translation.level+'*'}
                    list={levelDictionary}
                    value={positionLevel}
                >
                    <Tooltip
                        wrapper="
                        margin--b-10
                        margin--md-b-0
                        col-2
                        order-1
                    "
                        description={translation.tooltipExperienceLevel}
                    />
                </Combobox>
                <hr className="hr hr--fullwidth margin--b-20" />
                <Combobox
                    justState
                    validators={["combobox"]}
                    errorMessages={[translation.errorMessageCombobox]}
                    name="skills.mainSkill"
                    wrapperClass={`${this.props.wrapperClass} margin--b-10 combobox`}
                    labelClass={`${this.props.labelClass} order-1`}
                    className={`${this.props.className} order-2 order-md-1`}
                    label={translation.mainSkill+'*'}
                    onChange={::this.onChaneMainSkill}
                    list={
                        (knowledges.length &&
                            [...knowledges].filter((item, i) => {
                                if (item.isMain) {
                                    return item;
                                }
                            })) ||
                        []
                    }
                    value={mainSkill || ""}
                >
                    <Tooltip
                        wrapper="
                            margin--b-10
                            margin--md-b-0
                            col-2
                            order-1
                        "
                        description={translation.tooltipMainSkill}
                    />
                </Combobox>
                <MultySelect
                    justState
                    name="skills.scope"
                    wrapperClass="row  multiselect margin--b-10" 
                    labelClass={`${this.props.labelClass} order-1 fl--self-st relative relative--md-t-10`}
                    className={`${this.props.className} order-2 order-md-1`}
                    label={translation.additionalSkills}
                    limit={30}
                    limitText={translation.errorlimitText}
                    list={
                        (knowledges.length &&
                            [...knowledges].filter((item, i) => {
                                if (!item.isLang && item.label != mainSkillChange ) {
                                    return item;
                                }
                            })) ||
                        []
                    }
                    value={otherSkills || ""}
                >
                    <Tooltip
                        wrapper="
                            fl--self-st
                            relative relative--t-10
                            margin--b-10
                            margin--md-b-0
                            col-2
                            order-1
                        "
                        description={translation.tooltipAdditionalSkills}
                    />
                </MultySelect>
                {/* <div className="row">
                    <div className=" padding--xl-0 offset-xl-3 col-xl-9 font--14 font--color-secondary font--underlined">
                        <span className="pointer" onClick={openSkillPopUp}>
                            Не нашли ваш навык? Напишите нам
                        </span>
                    </div>
                </div> */}
            </Card>
        );
    }
}
