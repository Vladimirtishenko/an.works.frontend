import React from "react";
import Input from "../../../../../components/form/text.jsx";
import Range from "../../../../../components/form/range/index.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import uniqid from "uniqid";
import ParentStep from "../../../common/components/parent/main.jsx";

import { mapperSkills, mapperSkillsSorting } from "../../../../../helpers/mapperSkills.helper.js";
import Tooltip from '../../../../../components/widgets/toolTip.jsx';
import languageSkills from '../../../../../databases/languages/languageSkills.json';
import tooltipInfo from "../../../../../databases/general/tooltipInfoApplicant.json";

import { Card } from "../../../common/components/card.dumb.jsx";

class Language extends ParentStep { 
    constructor(props) {
        super(props);

        let state = ::this.setData(props);

        this.state = state;
    }

    static defaultProps = {
        wrapperClass:"row fl--align-c",
        labelClass:"label form__label font--14 col-10 col-md-4 col-lg-3",
        className: "col-md-6 col-xl-5 padding--md-0"
    }

    componentDidUpdate(prevProps, prevState) {
        let comparing = _.isEqual(prevProps, this.props);

        if (!comparing) {

            let state = ::this.setData(this.props);

            this.setState({
                ...this.state,
                ...state
            });
        }
    }

    setData(props) {
        let extendLanguage = mapperSkills(
                props.profile,
                props.knowledges,
                [],
                [{ isLang: true }],
                "array",
                true
            ),
            copyArray =
                (Array.isArray(extendLanguage) && extendLanguage.slice(0)) ||
                [],
            firstItem = copyArray.shift() || {};

        return {
            knowledges: props.knowledges,
            languages: copyArray ? ::this.setNormalizeData(copyArray) : [],
            firstItem: { id: uniqid(), data: firstItem }
        };
    }

    normalizeRangeData() {
        let language = {};

        for (var i = 0; i < languageSkills.length; i++) {
            language[languageSkills[i].label] = languageSkills[i].name;
        }

        return language;
    }

    renderAnotherComponent(item, knowledges, removed) {
        let valueList = ::this.normalizeRangeData(languageSkills);
        const { i18n: { translation } = {} } = this.props;
        return (
                <div key={item.id} className="relative fl fl--wrap margin--b-20 margin--md-b-30">
                    <div className="skills-language width--100">
                        <Combobox
                            justState
                            validators={["combobox"]}
                            errorMessages={[translation.errorMessageCombobox]}
                            name={`language.${item.id}.label`}
                            wrapperClass={`${this.props.wrapperClass} margin--b-20 margin--md-b-20 combobox`}
                            labelClass={`${this.props.labelClass} order-1`}
                            className={`${this.props.className} order-2 order-md-1`}
                            label={translation.language+'*'}
                            list={
                                knowledges && knowledges.length && knowledges.filter((item, i) => {
                                    if(item.isLang){
                                        return item
                                    }
                                }) || []
                            }
                            value={item.data && item.data.label}
                        >
                        {
                            removed ||

                            <Tooltip
                                wrapper="
                                    tooltip--md-p-xc-15
                                    margin--b-10
                                    margin--md-b-0
                                    col-2
                                    order-1
                                "
                                description={translation.tooltipLanguage}

                             />

                        }

                        </Combobox>
                        {removed && 
                            <span onClick={::this.removeFiled} data-id={item.id} data-mark="languages" className="link link--under link--grey font--12 absolute absolute--t-2 absolute--r-0 absolute--md-r-15-p absolute--md-t-45 absolute--lg-r-10-p absolute--lg-t-10 absolute--xl-r-23-p">
                                {translation.delete}
                            </span> || null}
                        <Range
                            wrapperClass={`${this.props.wrapperClass} margin--b-10 padding--b-10 overflow--h padding--md-t-20`}
                            labelClass={`${this.props.labelClass} form__label--renge`}
                            inputWrapperClass={this.props.className}
                            label={translation.level}
                            name={`language.${item.id}.level`}
                            min={0}
                            max={4}
                            step={1}
                            dots={true}
                            value={item.data && item.data.value || 0}
                            valueList={valueList}
                        />

                    </div>
                </div>
        );
    }

    render() {
        let {
                languages = [],
                firstItem,
                knowledges = []
            } = this.state,
            { i18n: { translation } = {} } = this.props,
            comparingArrayItems = languages.length == mapperSkillsSorting(knowledges, [{isLang: true}]).length - 1 ? false : true;

        return (
            <Card
                icon="lang"
                title={translation.languages}
                cardPadding="padding--15 padding--md-30"
            >
                {::this.renderAnotherComponent(firstItem, knowledges, false)}
                {(languages &&
                    languages.length &&
                    languages.map((item, i) => {
                        return ::this.renderAnotherComponent(
                            item,
                            knowledges,
                            true
                        );
                    })) ||
                    null}
                <div className="row margin--b-20">
                    <div className="offset-md-4 col-md-8 offset-lg-3">
                        { comparingArrayItems && <span
                            data-mark="languages"
                            className="padding--r-20 pointer-text font--12 font--color-blue icons--Add icons--size-16 icons--top-3 icons--margin-r-10"
                            onClick={::this.appendField}
                        >
                            {translation.addLanguage}
                        </span> || null }
                    </div>
                </div>
            </Card>
        );
    }
}

export default Language;
