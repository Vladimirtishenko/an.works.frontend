import React from 'react'
import {Card} from '../../../common/components/card.dumb.jsx'

import Location from '../../../common/components/editable/location.jsx'
import RelocateLocal from './relocateLocal.jsx'
import RelocateAbroad from './relocateAbroad.jsx'

export default class Geography extends React.Component {
    render(){

        let { i18n:{translation={}} = {},geography: { country, city, relocation: { local = [], abroad = [] } = { } } } = this.props;

        return (
            <Card icon="geagraphy" title={translation.location} cardPadding="padding--15 padding--md-30">
                <Location name="geography" location={{country, city}} />
                <RelocateLocal city={city} location={local} /> 
                <RelocateAbroad location={abroad} />
            </Card>
        )
    }
}
