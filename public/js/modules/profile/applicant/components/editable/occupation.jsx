import React from "react";
import Checkbox from "../../../../../components/form/checkbox.jsx";
import ParentStep from "../../../common/components/parent/main.jsx";

import { Card } from "../../../common/components/card.dumb.jsx";

import occupationVocabularies from "../../../../../databases/general/occupation.json";


class Occupation extends ParentStep {
    constructor(props) {
        super(props);

        this.state = {
            ...props,
            occupationVocabularies: occupationVocabularies
        };
    }

    getOccupationList(occupation, name) {
        let { occupationVocabularies } = this.state,
            newOccupationObject = [];

        if (!occupationVocabularies) return {};

        _.forOwn(occupationVocabularies, (item, i) => {
            let { label } = item,
                value = occupation[label] ? { checked: true, value: true } : {checked: false};

            newOccupationObject.push({
                ...item,
                ...value,
                name: `${name}.${item.label}`,
                innerWrapperClass: "margin--b-15 font--14 font--color-primary"
            });
        });

        return newOccupationObject;
    }

    render() {
        let { occupation } = this.state,
            getOccupation =
                ::this.getOccupationList(
                    occupation || [],
                    "geography.occupation"
                ) || [];
        const {i18n:{translation = {} } = {}} = this.props;
        return (
            <Card
                icon="type_of_employment"
                title={translation.employmentType}
                cardPadding="padding--15 padding--md-30"
            >
                <Checkbox
                    validators={["checkboxRequired"]}
                    errorMessages={[translation.errorMessageCheckbox]}
                    wrapperClass="checkbox row margin--b-10 combobox no-gutters "
                    innerWrapperClass=" margin--xl-b-20"
                    labelClass="form__checkbox form__label"
                    value={getOccupation || []}
                    label={translation.employmentType+'*'}
                    labelWrapperClass="form__label col-md-4 font--14 col-lg-3"
                    wrapperAllCheckbox="col-md-5 col-lg-4 fl fl--dir-col"
                />
            </Card>
        );
    }
}

export default Occupation;
