
import React from 'react'
import uniqid from 'uniqid';
import { connect } from "react-redux";
import Checkbox from '../../../../../components/form/checkbox.jsx';
import MultySelect from '../../../../../components/form/multiselect.jsx';
import Tooltip from '../../../../../components/widgets/toolTip.jsx';
import RelocateAbroadSingle from './relocateAbroadSingle.jsx'
import ParentStep from '../../../common/components/parent/main.jsx'
import tooltipInfo from "../../../../../databases/general/tooltipInfoApplicant.json";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
 
@connect(
    mapStateToProps,
    { ...i18n }
)
class RelocateAbroad extends ParentStep {

    constructor(props){
        super(props);

        let {location} = props,
            places = Array.isArray(location.places) && location.places.slice(0) || [],
            firstPlace = Array.isArray(places) && places.shift() || {},
            placesArray = places ? ::this.setNormalizeData(places) : [];

        this.state = {
            places: placesArray,
            firstPlace: firstPlace,
            type: location.type || false
        }

    } 

    static defaultProps = {
        wrapperClass:"row fl--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-3",
        className: "col-md-6 col-xl-5 padding--md-0"
    }
    
    abroadRelocation(value){

        const newValue = [...value],
              item = _.head(newValue),
              { checked = false } = item;

        this.setState( {...this.state, type: checked } )

    }

    render(){
        let {places, type, countries, firstPlace} = this.state,
            countriesMaps = places.map((item, i) => {
                    return <RelocateAbroadSingle
                                removable={true}
                                onClick={::this.removeFiled}
                                key={item.id}
                                item={item || {}}
                            />
                    });
        const { i18n:{translation = {}} = {} } = this.props;

        return (
            <div className="location__abroad">
                <Checkbox
                    wrapperClass="checkbox row margin--b-10"
                    labelClass="form__checkbox form__label"
                    labelWrapperClass={this.props.labelClass}
                    wrapperAllCheckbox={`${this.props.className} col-10`}
                    value={
                        [
                            {
                                name: "geography.relocation.abroad.type",
                                checked: type ? true : false,
                                value: type ? true : false,
                                innerWrapperClass: "d-inline_block font--14 font--color-secondary",
                                sub: translation.ready
                            }
                        ]
                    }
                    onChange={::this.abroadRelocation}
                    label={translation.readyRelocateAbroad}
                >
                 <Tooltip
                        wrapper="
                            col-2
                            tooltip--md-p-xc-15
                        "
                        description={translation.tooltipReadyRelocateAbroad}
                    />
                </Checkbox>
                { /*
                <div>
                    {
                        type &&
                        <div>
                            <RelocateAbroadSingle removable={false} item={firstPlace || {}} />
                            {countriesMaps}
                            <div className="row margin--b-20">
                                <div className="offset-md-4 col-md-8 offset-lg-3 padding--md-0">
                                    <span className="padding--r-20 link font--12 font--color-blue" data-mark="places" onClick={::this.appendField}>
                                        <span className="icon icon--add"></span>
                                        Добавить страну
                                    </span>
                                </div>
                            </div>
                        </div> || null
                    }
                </div> */ }
            </div>
        )

    }

}

export default RelocateAbroad;
