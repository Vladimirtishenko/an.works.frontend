import React from "react";

import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import DateFromTo from "../../../../../components/form/dateFromTo.jsx";
import Hidden from "../../../../../components/form/hidden.jsx";

import { normalizeToFromTo } from "../../../../../helpers/date.helper.js";

import Location from "../../../common/components/editable/location.jsx";
import { DeleteXButton } from "../../../../../components/form/deleteXButton.jsx";

class GeneralEducation extends React.Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        wrapperClass:"row fl--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-3",
        className: "col-md-6 col-xl-5 padding--md-0"
    }

    render() {

        const {openEducationPopUp,
                item: {
                    id,
                    location: { country, city, university } = {},
                    degree,
                    specialization,
                    date: { start: startDate = [], end: endDate = [], now } = {}
                },
                removeAction,
                i18n: { translation = {},translation: { month } = {} } = {}
            } = this.props,
            start = normalizeToFromTo(startDate),
            end = normalizeToFromTo(endDate);

        return (
            <div
                key={id}
                className="education border--b-2-grey padding--b-20 padding--15 padding--md-30"
            >
                <p className="margin--b-20 margin--md-b-30">
                    <DeleteXButton
                        className="font--16"
                        data-id={id}
                        data-option="educational"
                        data-action="educationalRealTimeChange"
                        onClick={removeAction}
                        text={translation.diploma}
                    />
                </p>
                <Hidden name={`education.${id}.type`} value="university" />
                <Hidden name={`education.${id}.status`} value="true" />
                <Hidden name={`education.${id}.id`} value={id} />
                <div className="margin--b-20 dropdownList">
                    <Location
                        labelClass={this.props.labelClass}
                        className={this.props.className}
                        i18n={this.props.i18n}
                        name={`education.${id}.location`}
                        location={{
                            country: country,
                            city: city,
                            university: university
                        }}
                        withUniversity={true}
                    />
                </div>

                <Input
                    validators={['requiredText', 'isName' ]}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageNameTitle]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.className}
                    inputClass="input form__input"
                    value={degree}
                    name={`education.${id}.degree`}
                    label={translation.degree+'*'}
                />
                <Input
                    validators={['requiredText', 'isName' ]}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageNameTitle]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.className}
                    inputClass="input form__input"
                    value={specialization}
                    name={`education.${id}.specialization`}
                    label={translation.studyField+'*'}
                />
                <div className="row fl--align-st fl--align-md-c margin--b-20">
                    <div className="label form__label font--14 col-md-4 col-lg-3 margin--md-t-10 margin--lg-t-0">
                        <span className="">{translation.educationPeriod+'*'}</span>
                    </div>
                    <DateFromTo
                        validators={['required', 'isDateRange']}
                        errorMessages={[translation.errorMessageRquired, translation.errorMessageIsDateRange]}
                        start={{
                            ...start,
                            name: `education[].${id}.date.start[]`
                        }}
                        end={{
                            ...end,
                            name: `education[].${id}.date.end[]`
                        }}
                        now={{
                            value: now,
                            name: `education[].${id}.date.now`
                        }}
                        translation={month}
                        wrapperClass="col-md-8 col-lg-9 padding--md-0"
                        wrapperClassStart="col-12 col-md-9 col-lg-8 col-xl-5 padding--0 margin--md-r-10 margin--b-10 fl"
                        wrapperClassEnd="col-12 col-md-9 col-lg-8 col-xl-5 padding--0 margin--xl-l-10 margin--b-10 fl"
                        label={translation.stillStudying}
                        wrapperClassCheck="col-12 padding--0"
                    />
                </div>
                <div className="row">
                    <div
                        className=" padding--xl-0 offset-xl-3 col-xl-9 font--14 font--color-secondary font--underlined"
                        
                    >
                        {/* <span className="pointer-text" onClick={openEducationPopUp}>
                            Не нашли ваш ВУЗ, факультет или степень? Напишите нам
                        </span> */}
                        <span className="pointer-text">
                           {translation.notFindYourUniversityFacultyDegree}
                            <a href="malito:"> </a>
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default GeneralEducation;
