import React from 'react'
import Textarea from '../../../../../components/form/textarea.jsx';
import {Card} from '../../../common/components/card.dumb.jsx'
import Tooltip from '../../../../../components/widgets/toolTip.jsx';
import tooltipInfo from "../../../../../databases/general/tooltipInfoApplicant.json";
export default class AboutEditable extends React.Component {
    render (){

        let {about , i18n: {translation = {}} = {} } = this.props;

        return (
            <Card icon="about_me" title={translation.about} cardPadding="padding--15 padding--md-30">
                <Textarea
                    validators={["maxStringLength:500","minStringLength:20"]}
                    errorMessages={[translation.errorMessageMaxLeng500,translation.errorMessageMinLeng20]}
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-11 col-md-4 font--14 order-1"
                    textAreaWrapper="col-md-7 order-2 order-md-1"
                    textAreaClass="form__text-area"
                    label={translation.about}
                    name="aboutMe.about"
                    rows="7" 
                    value={about && about.about}
                >
                <Tooltip 
                        wrapper="
                            col-1
                            tooltip--md-p-r-15
                            padding--r-0
                            margin--b-10 
                            margin--md-b-0
                            order-1
                        "
                        description={translation.tooltipSummary}
                    />
                </Textarea>
                <Textarea
                    validators={["maxStringLength:500","minStringLength:20"]}
                    errorMessages={[translation.errorMessageMaxLeng500,translation.errorMessageMinLeng20]}
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-11 col-md-4 font--14 order-1"
                    textAreaWrapper="col-md-7 order-2 order-md-1"
                    textAreaClass="form__text-area"
                    label={translation.workExpectations}
                    name="aboutMe.waitings"
                    rows="7"
                    value={about && about.waitings}
                >
                    <Tooltip 
                        wrapper="
                            col-1
                            tooltip--md-p-r-15
                            padding--r-0
                            margin--b-10 
                            margin--md-b-0
                            order-1
                        "
                        description={translation.tooltipWorkExpectations}
                    />
                </Textarea>
                <Textarea
                    validators={["maxStringLength:500","minStringLength:20"]}
                    errorMessages={[translation.errorMessageMaxLeng500,translation.errorMessageMinLeng20]}
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-11 col-md-4 font--14 order-1"
                    textAreaWrapper="col-md-7 order-2 order-md-1"
                    textAreaClass="form__text-area"
                    label={translation.achievements}
                    name="aboutMe.achivments"
                    rows="7"
                    value={about && about.achivments}
                >
                    <Tooltip 
                        wrapper="
                            col-1
                            tooltip--md-p-r-15
                            padding--r-0
                            margin--b-10 
                            margin--md-b-0
                            order-1
                        "
                        description={translation.tooltipAchievements}
                    />
                </Textarea>
                <Textarea
                    validators={["maxStringLength:500","minStringLength:20"]}
                    errorMessages={[translation.errorMessageMaxLeng500,translation.errorMessageMinLeng20]}
                    classes="row no-gutters margin--b-20" 
                    labelClass="form__label col-11 col-md-4 font--14 order-1"
                    textAreaWrapper="col-md-7 order-2 order-md-1"
                    textAreaClass="form__text-area"
                    label={translation.hobby}
                    name="aboutMe.hobby"
                    rows="7"
                    value={about && about.hobby}
                >
                    <Tooltip 
                        wrapper="
                            col-1
                            tooltip--md-p-r-15
                            padding--r-0
                            margin--b-10 
                            margin--md-b-0
                            order-1
                        "
                        description={translation.tooltipHobby}
                    />
                </Textarea>
            </Card>
        )
    }
}
