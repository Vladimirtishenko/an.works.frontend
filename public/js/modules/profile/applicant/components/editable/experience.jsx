
import React from 'react'
import uniqid from 'uniqid';
import {Card} from '../../../common/components/card.dumb.jsx'

import ItExperience from './itExperience.jsx'
import OtherExperience from './otherExperience.jsx'
import Tooltip from '../../../../../components/widgets/toolTip.jsx';
// Inheritance
import Inheritance from '../../../common/components/parent/main.jsx'

import tooltipInfo from "../../../../../databases/general/tooltipInfoApplicant.json";
import {
    transformToTotal
} from "../../../../../helpers/date.helper.js";

import rule from "../../../../../configuration/rules.json";
export default class Experience extends Inheritance {
    normalizeDataType(type){
      return type == 'it' ? 'renderExperienceIT' : 'renderExperienceOther';
    }

    renderExperienceIT(key, item = {}){

      return (
          <ItExperience
                key={key}
                item={item}
                removeAction={::this.removeFieldToProps}
                setToTotal={::this.setToTotal}
                {...this.props}
          />
     )

    }

    renderExperienceOther(key, item = {}){

      return (
          <OtherExperience
              key={key}
              item={item}
              removeAction={::this.removeFieldToProps}
              setToTotal={::this.setToTotal}
              {...this.props}
          />
      )

    }

    renderPath(type, key, data) {
        const methodType = this.normalizeDataType(type)

        return this[methodType] && this[methodType](key, data) || null;
    }

    setToTotal(key, value){

        const { actions: { experienceRealTimeChange }, applicant: { profile: { experience: {items} } }} = this.props,
              clonedExperience = [...items],
              index = _.findIndex(clonedExperience, {id: key});

        if(index > -1){
            clonedExperience[index].date = value;
        }

        const period = transformToTotal(clonedExperience);

        experienceRealTimeChange(clonedExperience, period);

    }

    render() {

        const {
                i18n: { translation = {} } = {},
                applicant: {
                    profile: {
                        experience: {
                            items = []
                        } = {}
                    }
                }
        } = this.props,
        limitItExperience = items.filter((item) =>{
            return item.type == 'it' ;
        }),
        limitOtherExperience = items.filter((item) =>{
            return item.type == 'other' ;
        });
        return (
            <Card icon="exp" title={translation.experience}>
                    {
                        items.length && items.map((item) => {
                            return ::this.renderPath(item.type, item.id, item)
                        }) || <div className="padding--15 padding--md-30 border--b-2-grey">
                                <p className="margin--b-20 font--14">{translation.addMultipleActivities}</p>
                                <p className="font--14 font--color-secondary">{translation.participationInRankingWithoutWorkExperience}</p>
                        </div>
                    }
                <div className="fl fl--wrap padding--md-xc-30 padding--xc-15 padding--t-20 padding--b-30">
                    {rule.limintExpirience > limitItExperience.length && (
                        <div className="fl padding--r-20 margin--b-10 margin--md-0">
                            <span 
                                className="fl pointer-text font--12 font--color-blue margin--b-10 margin--md-b-0 icons--Add icons--size-16 icons--top-1 icons--margin-r-10"
                                data-type="it"
                                data-option="experience"
                                data-action="experienceRealTimeChange"
                                onClick={::this.appendFieldToProps}
                            >
                                {translation.workInIT}
                            </span>
                            <Tooltip
                                wrapper="margin--l-10"
                                description={translation.tooltipAddITExperience}
                            />
                        </div>
                    ) || null}
                    {rule.limintExpirience > limitOtherExperience.length && (
                        <div className="fl">
                            <span
                                className="fl pointer-text font--12 font--color-blue icons--Add icons--size-16 icons--top-1 icons--margin-r-10"
                                data-type="other"
                                data-option="experience"
                                data-action="experienceRealTimeChange"
                                onClick={::this.appendFieldToProps}
                            >
                                {translation.otherExperience}
                            </span>
                            <Tooltip
                                wrapper="margin--l-10"
                                description={translation.tooltipAddNotITExperience}
                            />
                        </div>
                    ) || null}
                </div>
            </Card>
        )
    }
}
