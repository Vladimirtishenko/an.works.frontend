import React from "react";
import uniqid from "uniqid";
import { Card } from "../../../common/components/card.dumb.jsx";
import Tooltip from "../../../../../components/widgets/toolTip.jsx";
import GeneralEducation from "./generalEducation.jsx";
import Sertificate from "./sertificate.jsx";

// Inheritance
import Inheritance from "../../../common/components/parent/main.jsx";
import tooltipInfo from "../../../../../databases/general/tooltipInfoApplicant.json";

import rule from "../../../../../configuration/rules.json";

export default class Education extends Inheritance {
    normalizeDataType(type) {
        return type == "university" ? "generalEducation" : "sertificate";
    }

    generalEducation(key, item = {}) {
        return (
            <GeneralEducation
                key={key}  
                item={item}
                removeAction={::this.removeFieldToProps}
                {...this.props}
            />
        );
    }

    sertificate(key, item = {}) {
        return (
            <Sertificate
                key={key}
                item={item}
                removeAction={::this.removeFieldToProps}
                {...this.props}
            />
        );
    }

    renderPath(type, key, data) {
        const methodType = this.normalizeDataType(type);

        return (this[methodType] && this[methodType](key, data)) || null;
    }
    render() {
        const {
            i18n: { translation = {} } = {},
            applicant: { profile: { educational: { items = [] } = {} } = {} }
        } = this.props,
        limitGeneralEducation = items.filter((item) =>{
            return item.type == 'university' ;
        }),
        limitSertificate = items.filter((item) =>{
            return item.type == 'certificate' ;
        });
        return (
            <Card icon="education" title={translation.education}>
                {(items.length &&
                    items.map((item, i) => {
                        return ::this.renderPath(item.type, item.id, item);
                    })) || (
                    <div className="padding--15 padding--md-30 border--b-2-grey">
                        <p className="margin--b-20 font--14">
                            {translation.addSeveralEducationTypes}
                        </p>
                        <p className="font--14 font--color-secondary">
                            {translation.participationInRankingEducation}
                        </p>
                    </div>
                )}
                <div className="fl padding--md-xc-30 padding--xc-15 padding--t-20 padding--b-30 fl--wrap">
                {rule.limintExpirience > limitGeneralEducation.length && (
                    <div className="fl padding--r-20 margin--b-10 margin--md-0">
                        <span
                            className="fl icons--Add icons--size-16 icons--top-1 icons--margin-r-10 pointer-text font--12 font--color-blue"
                            data-type="university"
                            data-option="educational"
                            data-action="educationalRealTimeChange"
                            onClick={::this.appendFieldToProps}
                        >
                            {translation.addEducationInformation}
                        </span>
                        <Tooltip
                            wrapper="
                            margin--l-10
                        "
                        description={translation.tooltipAddEducationInformation}
                        />
                    </div>
                ) || null}
                {rule.limintExpirience > limitSertificate.length && (
                    <div
                        className="fl "                       
                    >
                        <span 
                            onClick={::this.appendFieldToProps} 
                            className="fl icons--Add icons--size-16 icons--top-1 icons--margin-r-10 pointer-text font--12 font--color-blue"
                            data-type="certificate"
                            data-option="educational"
                            data-action="educationalRealTimeChange"
                        >
                            {translation.addCertificateInformation}
                        </span>
                        <Tooltip 
                            wrapper="
                            margin--l-10
                        "
                        description={translation.tooltipAddCertificateInformation}
                        />
                    </div>
                ) || null}
                </div>
            </Card>
        );
    }
}
