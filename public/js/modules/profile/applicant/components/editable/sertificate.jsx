import React from "react";

import Hidden from "../../../../../components/form/hidden.jsx";

// Form Componets
import Input from "../../../../../components/form/text.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
import DateFromTo from "../../../../../components/form/dateFromTo.jsx";

// Componets

import { normalizeToFromTo } from "../../../../../helpers/date.helper.js";

import { DeleteXButton } from "../../../../../components/form/deleteXButton.jsx";

import sertificateList from "../../../../../databases/general/sertificate.json";

import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

class Certificate extends React.Component {

    static defaultProps = {
        wrapperClass:"row fl--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-3",
        className: "col-md-6 col-xl-5 padding--md-0"
    }
    
    render() {
        const {
                item: {
                    id,
                    location: { country, city } = {},
                    degree,
                    mark,
                    sertificateType,
                    organisation,
                    name,
                    number,
                    date: { start: startDate = [], end: endDate = [], now } = {}
                },
                removeAction,
                i18n: {translation={}, translation: { month } } = {}
            } = this.props,
            start = normalizeToFromTo(startDate),
            end = normalizeToFromTo(endDate);

        return (
            <div
                key={id}
                className="education border--b-2-grey padding--b-20 padding--15 padding--md-30"
            >
                <p className="margin--b-20 margin--md-b-30">
                    <DeleteXButton
                        className="font--16"
                        data-id={id}
                        data-option="educational"
                        data-action="educationalRealTimeChange"
                        onClick={removeAction}
                        text={translation.certificate}
                    />
                </p>

                <Hidden name={`education.${id}.type`} value="certificate" />
                <Hidden name={`education.${id}.id`} value={id} />
                <Hidden name={`education.${id}.status`} value="true" />

                <Input
                    validators={['requiredText', 'isName']}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageName]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.className}
                    inputClass="input form__input"
                    value={name}
                    label={translation.certificationName+'*'}
                    name={`education.${id}.name`}
                />
                <Combobox
                    validators={['required', 'combobox']}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageCombobox]}
                    name={`education.${id}.sertificateType`}
                    wrapperClass="row  fl--align-c margin--b-10 combobox"
                    labelClass={this.props.labelClass}
                    className={this.props.className}
                    label={translation.certificationType+'*'}
                    list={sertificateList || []} 
                    value={sertificateType}
                />
                <Input
                    validators={['requiredText', 'isName']}
                    errorMessages={[translation.errorMessageRquired, translation.errorMessageNameTitle]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.className}
                    inputClass="input form__input"
                    value={organisation}
                    label={translation.certificationAuthority+'*'}
                    name={`education.${id}.organisation`}
                />
                <Input
                    validators={['requiredText']}
                    errorMessages={[translation.errorMessageRquired]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.className}
                    inputClass="input form__input"
                    value={number}
                    label={translation.licenseNumber+'*'}
                    name={`education.${id}.number`}
                />
                <Input
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.className}
                    inputClass="input form__input"
                    value={mark}
                    label={translation.testScore}
                    name={`education.${id}.mark`}
                />
                <div className="row fl--align-st fl--align-md-c margin--b-20">
                    <div className="label form__label font--14 col-md-4 col-lg-3 margin--md-t-10 margin--lg-t-0">
                        <span className="">{translation.validity+'*'}</span>
                    </div>
                    <DateFromTo
                        validators={['required', 'isDateRange']}
                        errorMessages={[translation.errorMessageRquired, translation.errorMessageIsDateRange]}                    
                        start={{
                            ...start,
                            name: `education[].${id}.date.start[]`
                        }}
                        end={{
                            ...end,
                            name: `education[].${id}.date.end[]`
                        }}
                        now={{
                            value: now,
                            name: `education[].${id}.date.now`
                        }}
                        translation={month}
                        wrapperClass="col-md-8 col-lg-9 padding--md-0"
                        wrapperClassStart="col-12 col-md-9 col-lg-8 col-xl-5 padding--0 margin--md-r-10 margin--b-10 fl"
                        wrapperClassEnd="col-12 col-md-9 col-lg-8 col-xl-5 padding--0 margin--xl-l-10 margin--b-10 fl"
                        label={translation.certificationNotExpire}
                        wrapperClassCheck="col-12 padding--0"
                    />
                </div>
            </div>
        );
    }
}

export default Certificate;
