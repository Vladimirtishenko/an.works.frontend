import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Steps from "../../components/smart/steps.smart.jsx";

import applicantProfileCompProgress from "../../decorators/profile.dec.jsx";

// Actions
import * as applicantActions from "../../actions/profile.action.js";
import * as applicantActionsSteps from "../../actions/step.action.js";
import * as applicantActionsActivity from "../../actions/activity.action.js";
import * as applicantActionsTests from "../../../common/actions/quiz.action.js";

// Layout
import TopLayout from "../../../../../components/includes/layout.jsx";
import Layout from "../../components/layouts/layout.jsx";
import { linksStepsSchema } from "../../schema/linksSteps.sc";

const sidebars = {
    1: ['progressProfile'],
    2: ['progressProfile'],
    3: ['progressProfile'],
    4: ['experience'],
    5: ['progressProfile'],
    6: ['progressProfile'],
    7: ['progressProfile'],
}
function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...applicantActions,
                ...applicantActionsSteps,
                ...applicantActionsActivity,
                ...applicantActionsTests
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
@applicantProfileCompProgress
class Index extends React.Component {
    render() {
        const { applicant:{profile:{direction}} } = this.props,
                sidebar = sidebars[direction];

        return (
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksStepsSchema(this.props.i18n.translation)}
                    progressBar={true}
                    rightComponentName={sidebar}
                >
                    <Steps {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}

export default Index;
