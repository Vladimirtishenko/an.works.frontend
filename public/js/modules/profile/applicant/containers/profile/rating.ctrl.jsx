import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as applicantActions from "../../actions/profile.action.js";
import * as applicantQuiz from "../../../common/actions/quiz.action.js";
import * as hitsActions from "../../../common/actions/hint.action.js";
import RaitingComponent from "../../components/profile/rating.jsx";

// Configuration
import ratingConfuguration from '../../../../../configuration/rating.json'

import TopLayout from "../../../../../components/includes/layout.jsx";
import Left from "../../components/layouts/leftAside.jsx";
import {linksProfileSchema} from "../../schema/linksProfile.sc";
import {isMobile} from 'react-device-detect';

import {
    labelsToDefaults
} from "../../../../../helpers/mapperSkills.helper.js";


function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n,
        hints: state.hints
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...applicantActions,
                ...applicantQuiz,
                ...hitsActions
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Rating extends React.Component {

    componentDidMount() {
        const {
            applicant: {
                profile: { mainSkill, positionLevel, profileId }
            },
            actions: {getQueue, getWorkSkills, getSkillTestHierarchy, getStatisticData,getProfileResults},
            location: {search},
            applicant:{ratings}
        } = this.props;
        let { skill, level } = labelsToDefaults({skill: mainSkill, level: positionLevel}, ratingConfuguration);
        level = level == 'trainee' ? "junior" :  level;
        getQueue( ratings ? ratings : level + '_' + skill, search.slice(1));
        getWorkSkills();
        getSkillTestHierarchy(skill);
        getStatisticData(profileId, search.slice(1));
        getProfileResults(profileId);
    }

    componentDidUpdate(prevProps) {

        const { applicant: { filters: prevFilters } } = prevProps,
              { history: {replace}, applicant: { filters: nextFilters } } = this.props;


        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }

    renderContent = () => {
        if (isMobile) {
            return <Left mobileMenu={true} {...this.props} links={linksProfileSchema(this.props.i18n.translation)} />
        }
        return null
    };

    render() {
        const {
            applicant: {
                profile: {profileId }
            }
        } = this.props;
        return (
            <TopLayout container="container-fluid">
                {this.renderContent()}
                <RaitingComponent youId={profileId} {...this.props} />
            </TopLayout>
        );
    }
}
