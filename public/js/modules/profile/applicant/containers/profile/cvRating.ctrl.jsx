import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as applicantActions from "../../actions/profile.action.js";
import * as applicantQuiz from '../../../common/actions/quiz.action.js'; 
import * as contact from "../../../common/actions/contact.action.js";

import TopLayout from "../../../../../components/includes/layout.jsx";
import CVComponent from "../../components/profile/cvAnonim.jsx";
import {isMobile} from "react-device-detect";
import Left from "../../components/layouts/leftAside.jsx";
import {linksProfileSchema} from "../../schema/linksProfile.sc";


function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...applicantActions,
                ...applicantQuiz,
                ...contact
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class CV extends React.Component {
    componentDidMount() {
        const { match:{
                params:{
                    id = ''
                }
            },
            actions: { getContactProfileCv}
        } = this.props;
        getContactProfileCv(id);

    }
    componentDidUpdate(prevProps) {
        
        if(!this.props.applicant.cvContact && !prevProps.applicant.cvContact) return

        const { applicant: { cvContact:{ mainSkill: prevStatus} = {} } = {} } = prevProps,
        { 
            applicant: { cvContact:{ mainSkill: currentStatus} = {} } = {},
            actions: { getSkillTestHierarchy}
        } = this.props;
        
        if(currentStatus !== prevStatus){
            getSkillTestHierarchy(currentStatus);
        }
        
    }
    componentWillUnmount(){
        const {
            actions: { deletContactProfileCv}
        } = this.props;
        
        if(deletContactProfileCv){
            deletContactProfileCv();
        }
    }
    renderContent = () => {
        if (isMobile) {
            return <Left mobileMenu={true} {...this.props} links={linksProfileSchema(this.props.i18n.translation)} />
        }
        return null
    };
    render() {              
        return (
            <TopLayout>
                {this.renderContent()}
                <CVComponent {...this.props} />
            </TopLayout>
        );
    }
}
