import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import LocationComponent from '../../components/profile/location.jsx'
import TopLayout from "../../../../../components/includes/layout.jsx";
import LayoutProfile from '../../components/layouts/layoutprofile.jsx'
import { linksProfileSchema } from "../../schema/linksProfile.sc.js";

// Actions
import * as applicantActions from '../../actions/profile.action.js'

function mapStateToProps(state) {

    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
			...applicantActions
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Location extends React.Component {
    componentDidMount() {
        const {
            applicant: {
                profile: { profileId }
            },
            actions: { getStatisticData}
        } = this.props;
        getStatisticData(profileId);
    }
  render(){
      return (
          <TopLayout>
              <LayoutProfile {...this.props} links={linksProfileSchema(this.props.i18n.translation)} >
                  <LocationComponent {...this.props} />
              </LayoutProfile>
          </TopLayout>
      )
  }

}
