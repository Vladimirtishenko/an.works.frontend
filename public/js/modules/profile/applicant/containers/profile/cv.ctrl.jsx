import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as applicantActions from "../../actions/profile.action.js";
import * as applicantQuiz from '../../../common/actions/quiz.action.js';
import TopLayout from "../../../../../components/includes/layout.jsx";
import CVComponent from "../../components/profile/cv.jsx";
import {isMobile} from "react-device-detect";
import Left from "../../components/layouts/leftAside.jsx";
import {linksProfileSchema} from "../../schema/linksProfile.sc";

function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...applicantActions,
                ...applicantQuiz
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class CV extends React.Component {
    componentDidMount() {
        const { 
            applicant: {
                profile: { profileId, mainSkill}
            },
            actions: {getProfileResults, getStatisticData, getSkillTestHierarchy}
        } = this.props;
        getStatisticData(profileId);
        getSkillTestHierarchy(mainSkill);
        getProfileResults(profileId);
    }
    renderContent = () => {
        if (isMobile) {
            return <Left mobileMenu={true} {...this.props} links={linksProfileSchema(this.props.i18n.translation)} />
        }
        return null
    };
    render() {              
        return (
            <TopLayout>
                {this.renderContent()}
                <CVComponent {...this.props} />
            </TopLayout>
        );
    }
}
