import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import InformationComponent from '../../components/profile/personalInfo.jsx'
import TopLayout from "../../../../../components/includes/layout.jsx";
import LayoutProfile from '../../components/layouts/layoutprofile.jsx'
import { linksProfileSchema } from "../../schema/linksProfile.sc.js";

// Actions
import * as applicantActions from '../../actions/profile.action.js'
import * as applicantQuiz from "../../../common/actions/quiz.action.js";
import * as applicantActionsActivity from "../../actions/activity.action.js";

import ratingConfuguration from '../../../../../configuration/rating.json'

import {
  labelsToDefaults
} from "../../../../../helpers/mapperSkills.helper.js";
function mapStateToProps(state) {

    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
      ...applicantActions,
      ...applicantActionsActivity,
      ...applicantQuiz
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Information extends React.Component {
  componentDidMount() {
    const {
        applicant: {
            profile: { mainSkill, positionLevel, profileId }
        },
        actions: {getQueue, getStatisticData},
        location: {search}
    } = this.props,
    { skill, level } = labelsToDefaults({skill: mainSkill, level: positionLevel}, ratingConfuguration);

    getQueue(level + "_" + skill, search.slice(1));
    getStatisticData(profileId);
}
  render(){
      return (
          <TopLayout>
            <LayoutProfile {...this.props} links={linksProfileSchema(this.props.i18n.translation)}>
              <InformationComponent {...this.props} /> 
             </LayoutProfile>
          </TopLayout>
      )
  }

}
