import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import SecurityComponent from "../../components/profile/security.jsx";

// Actions 
import * as applicantActions from "../../actions/profile.action.js";

// Layout
import LayoutProfile from '../../components/layouts/layoutprofile.jsx'
import TopLayout from "../../../../../components/includes/layout.jsx";

import { linksProfileSchema } from "../../schema/linksProfile.sc.js";
import { linksStepsSchema } from "../../schema/linksSteps.sc.js";

function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...applicantActions
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Security extends React.Component {
    render() {
        const { type } = this.props,
              linksSchema = type == 'steps' ? linksStepsSchema : linksProfileSchema;
        return (
            <TopLayout>
                <LayoutProfile
                    {...this.props}
                    links={linksSchema(this.props.i18n.translation)}
                >
                    <SecurityComponent {...this.props} />
                </LayoutProfile>
            </TopLayout>
        );
    }
}
