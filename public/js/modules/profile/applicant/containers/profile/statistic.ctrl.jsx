import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as applicantActions from "../../actions/profile.action.js";

// Components
import Statistics from "../../components/profile/statistics.jsx";

// Layout
import LayoutProfile from '../../components/layouts/layoutprofile.jsx'
import TopLayout from "../../../../../components/includes/layout.jsx";

import { linksProfileSchema } from "../../schema/linksProfile.sc.js";
import { linksStepsSchema } from "../../schema/linksSteps.sc.js";

function mapStateToProps(state) {
    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...applicantActions
            },
            dispatch
        ),
        dispatch
    }
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Statistic extends React.Component {

    componentDidMount() {

        const { applicant : { profile: { profileId } }, actions: { getStatisticData }, location: {search} } = this.props;

        getStatisticData(profileId, search.slice(1));

    }

    componentDidUpdate(prevProps){

        const { applicant: { filters: prevFilters } } = prevProps,
              { history: {replace}, applicant: { filters: nextFilters } } = this.props;

        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }
    }

    render() {
        const { type } = this.props,
              linksSchema = type == 'steps' ? linksStepsSchema : linksProfileSchema;

        return (
            <TopLayout>
                <LayoutProfile
                    {...this.props}
                    links={linksSchema(this.props.i18n.translation)}
                    mainClass="col-lg-10"
                    rightAside={false}
                >
                    <Statistics {...this.props} />
                </LayoutProfile>
            </TopLayout>
        );
    }
}
