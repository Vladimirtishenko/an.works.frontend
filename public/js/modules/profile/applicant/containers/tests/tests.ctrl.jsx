import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import TestComponent from '../../components/tests/tests.jsx'
import TopLayout from "../../../../../components/includes/layout.jsx"
import LayoutProfile from '../../components/layouts/layoutprofile.jsx'
import { linksProfileSchema } from "../../schema/linksProfile.sc.js";

// Actions
import * as applicantActions from '../../actions/profile.action.js';
import * as applicantQuiz from '../../../common/actions/quiz.action.js';

function mapStateToProps(state) {

    return {
        applicant: state.profile.applicant,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
            ...applicantActions,
            ...applicantQuiz
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Test extends React.Component {
    componentDidMount() {

        let {
            applicant: {
                profile: { profileId ,mainSkill}
            },
            actions: {getTestList, getProfileResults, getArchiveTestList, getSkillTestHierarchy, getStatisticData},
            location: {search}
        } = this.props;
        getTestList(profileId, search.slice(1));
        getProfileResults(profileId);
        getSkillTestHierarchy(mainSkill);
        getStatisticData(profileId);
        getArchiveTestList(profileId)

    }

    componentDidUpdate(prevProps) {

        const { applicant: { filters: prevFilters } } = prevProps,
              { history: {replace}, applicant: { filters: nextFilters } } = this.props;


        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }

    render(){
        return (
            <TopLayout>
              <LayoutProfile
                  {...this.props}
                  links={linksProfileSchema(this.props.i18n.translation)}
              >
                <TestComponent {...this.props} />
            </LayoutProfile>
            </TopLayout>
        );
    }

}
