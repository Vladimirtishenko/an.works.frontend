import {EXPERIENCE_REAL_TIME_CHANGING, EDUCATIONAL_REAL_TIME_CHANGING} from '../constants/activity.const.js';

export function experienceRealTimeChange(experience, total = {}) {

    return (dispatch, getState) => {

            const { profile: { applicant: { profile: { experience: { it: currentIt, other: currentOther, common: currentCommon } = {} } } } } = getState(),
                  { it = currentIt, common = currentCommon, other = currentOther } = total;

            dispatch({
                type: EXPERIENCE_REAL_TIME_CHANGING,
                items: experience,
                it: it,
                common: common,
                other: other
            })

    }
}

export function educationalRealTimeChange(educational) {

    return (dispatch, getState) => {

            dispatch({
                type: EDUCATIONAL_REAL_TIME_CHANGING,
                items: educational
            })

    }
}
