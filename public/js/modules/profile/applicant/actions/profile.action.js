import * as types from "../constants/profile.const.js";
import { SET_NOTIFICATION }  from '../../../../libraries/notification/constant/notification.const.js'
import API from "../../../../services/api.js";

export function getUserProfile(profileId, step) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__get(
                "applicants_profiles/" + profileId,
                dispatch,
                SET_NOTIFICATION
            );

            if (userProfile) {
                dispatch({
                    type: types.PROFILE_LOADED_SUCCESS,
                    profile: userProfile
                });
            }
        })();
    };
}

export function getWorkSkills() {
    return dispatch => {
        (async () => {
            let knowledges = await API.__get(
                "dictionary/workSkills/",
                dispatch,
                SET_NOTIFICATION
            );
            dispatch({
                type: types.KNOWLEDGES_LOADED_SUCCESS,
                knowledges: knowledges
            });
        })();
    };
}

export function updateProfile(data, profileId, direction) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__post(
                "PATCH",
                "api/applicants_profiles/" + profileId,
                data,
                dispatch,
                SET_NOTIFICATION
            );

            if (userProfile) {
                userProfile.direction = ++direction;

                dispatch({
                    type: types.PROFILE_LOADED_SUCCESS,
                    profile: userProfile
                });
            }
        })();
    };
}

export function updateProfilePassword(data, uid) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__post(
                "PATCH",
                "api/users/" + uid,
                data,
                dispatch,
                SET_NOTIFICATION
            );

            if (userProfile) {
                dispatch({
                    type: SET_NOTIFICATION,
                    notification: {
                        type: 'success',
                        view: 'notice',
                        message: 'Password was updated!'
                    }
                });
            }
        })();
    };
}


export function updateProfileWithoutSubmit(data, profileId, direction) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__get('applicants_profiles/' + profileId, dispatch, SET_NOTIFICATION)

            if (userProfile) {
                userProfile = {
                    ...userProfile,
                    ...data,
                    ...{
                        direction: --direction
                    }
                }

                dispatch({
                    type: types.PROFILE_LOADED_SUCCESS,
                    profile: userProfile
                });
            }
        })();
    };
}


export function updateProfileByComponent(data, profileId) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__post(
                "PATCH",
                "api/applicants_profiles/" + profileId,
                data,
                dispatch,
                SET_NOTIFICATION
            );

            if (userProfile) {
                dispatch({
                    type: types.PROFILE_LOADED_SUCCESS,
                    profile: userProfile
                });
            }
        })();
    };
}


export function getTelegramRegistrationUrl() {
    return dispatch => {
        (async () => {

            let telegramRegistrationUrl = await API.__post('POST', 'api/telegram_registration/', {}, dispatch, SET_NOTIFICATION);

            if (telegramRegistrationUrl) {
                dispatch({
                    type: types.TELERGAM_BOT_URL_LOADED_SUCCESS,
                    telegramRegistrationUrl: telegramRegistrationUrl
                });
            }
        })();
    };
}

export function isTelegramVerified() {
    return dispatch => {
        (async () => {

            let telegramVerification = await API.__get('user_telegram/', dispatch, SET_NOTIFICATION);

            if (telegramVerification) {

                dispatch({
                    type: types.TELERGAM_VERIFICATION_LOADED,
                    telegramVerification: telegramVerification
                })
            }

        })();

    }
}


export function verificateTelegramToken(token, uid) {
    return dispatch => {
        (async () => {

            let telegramVerification = await API.__post('POST', 'api/telegram_verification', {
                token: token
            }, dispatch, SET_NOTIFICATION);

            if (telegramVerification) {

                dispatch({
                    type: types.TELERGAM_VERIFICATION_LOADED,
                    telegramVerification: telegramVerification
                })
            }

        })();

    }
}

export function getQueue(name, filters) {

    return (dispatch, getState) => {
        (async () => {
            const {
                profile: {
                    applicant: {
                        filters: stateFilters,
                        queue: {
                            queueLabel: oldName
                        } = {}
                    }
                }
            } = getState(),
                flt = filters === true ? stateFilters : filters ? filters : '',
                newName = name ? name : oldName,
                stringFlt = flt && ('?' + flt) || '';

            dispatch({
                type: types.RATING_QUEUE_LOAD_SUCCESS,
                queue: {...queue, queueFetch: true},
                filters: flt
            });
            
            const queue = await API.__get("queue_filter/" + newName + stringFlt, dispatch, SET_NOTIFICATION);

            dispatch({
                type: types.RATING_QUEUE_LOAD_SUCCESS,
                queue: {...queue, queueFetch: false},
                filters: flt
            });

        })();
    };
}

export function getStatisticData(profileId, filters = '') {

    return dispatch => {
        (async () => {

            const stringFlt = filters && ('?' + filters) || '';

            let statisticData = await API.__get('view/applicant/statistics/' + profileId + stringFlt, dispatch, SET_NOTIFICATION);

            dispatch({
                type: types.STATISTIC_VIEW_LOADED_SUCCESS,
                statisticData: statisticData,
                filters: filters
            })
        })();
    };
}

export function getTestList(profileId, filters = '') {

    return dispatch => {
        (async () => {

            const stringFlt = filters && ('?' + filters) || '';

            dispatch({
                type: types.TESTS_LIST_LOAD,
                filters: filters
            })

        })();

    }
}
export function getArchiveTestList(profileId) {

    return dispatch => {
        (async () => {

            let testArchive = await API.__get('applicant_tests_results_history/' + profileId, dispatch, SET_NOTIFICATION);

            if(testArchive){
                dispatch({
                    type: types.ARCHIVE_TESTS_LIST_LOAD,
                    testArchive: testArchive
                })    
            }

        })();

    }
}
export function saveRating(value){

    return dispatch => {
        dispatch({
            type: types.SAVE_RATING,
            rating: value,
        })
    }

}

export function updateHiringStatus(data) {
    // let data = {
    //     companyId: 'id',
    //     applicantId: 'id',
    //     applicantAnswer: 'notHired' // ONe of ['hired', 'notHired']
    // };
    return dispatch => {
        (async () => {

            let res = await API.__post('PATCH', 'api/hiring_contacts/applicant/', data, dispatch, SET_NOTIFICATION);
            if (res) {
                getStatisticData(data.applicantId)(dispatch);
            }
        })();

    }
}
