import * as types from '../constants/profile.const.js';

export function updateStep(step) {

    return dispatch => {

        dispatch({
            type: types.UPDATE_STEP_DIRECTION,
            direction: step
        })

    }

}
