import {
    PROFILE_LOADED_SUCCESS,
    KNOWLEDGES_LOADED_SUCCESS,
    TELERGAM_BOT_URL_LOADED_SUCCESS,
    RATING_QUEUE_LOAD_SUCCESS,
    UPDATE_STEP_DIRECTION,
    STATISTIC_VIEW_LOADED_SUCCESS,
    TELERGAM_VERIFICATION_LOADED,
    TELERGAM_VERIFICATION_FAILED,
    KNOWLEDGES_LOADED_FAIL,
    PROFILE_LOADED_FAIL,
    SKILL_TESTS_HIERARCHY_FAILED,
    TESTS_RESULTS_LOADED_FAILED,
    TEST_RESULT_CREATED_FAILED,
    RATING_QUEUE_LOAD_FAILED,
    STATISTIC_VIEW_LOADED_FAILED,
    TESTS_LIST_LOAD,
    SAVE_RATING,
    ARCHIVE_TESTS_LIST_LOAD
} from '../constants/profile.const.js';

import {
    EXPERIENCE_REAL_TIME_CHANGING,
    EDUCATIONAL_REAL_TIME_CHANGING
} from '../constants/activity.const.js';

import {
    SKILL_TESTS_HIERARCHY,
    TESTS_RESULTS_LOADED
} from '../../common/constants/quiz.const.js';

import {
    CONTACT_PROFILE_APPLICANT
}from '../../common/constants/contact.const.js';

const initialState = {
    schemas: {
        tests: {
            fields: [
                {
                    name: 'skill',
                    type: 'String'
                },
                {
                    name: 'result',
                    type: 'String'
                },
                {
                    name: 'time',
                    type: 'String'
                },
                {
                    name: 'valid',
                    type: 'String'
                },
                {
                    name: 'publish',
                    type: 'Checkbox'
                }

            ],
            actions: {
                delete: 'action[onClick][deleteEvent]'
            }
        }
    }
};

export default function profile(state = initialState, action) {

    switch (action.type) {

        case PROFILE_LOADED_SUCCESS:
            return {
                ...state,
                profile: action.profile
            };

        case KNOWLEDGES_LOADED_SUCCESS:
            return {
                ...state,
                knowledges: action.knowledges
            };

        case TESTS_RESULTS_LOADED:
            return {
                ...state,
                testsResults: action.testsResults
            };

        case SKILL_TESTS_HIERARCHY:
            return {
                ...state,
                skillTestsHierarchy: action.skillTestsHierarchy
            };

        case TELERGAM_BOT_URL_LOADED_SUCCESS:
            return {
                ...state,
                telegramRegistrationUrl: action.telegramRegistrationUrl
            };

        case RATING_QUEUE_LOAD_SUCCESS:
            return {
                ...state,
                queue: action.queue,
                filters: action.filters
            }

        case TESTS_LIST_LOAD:
            return {
                ...state,
                filters: action.filters
            }

        case UPDATE_STEP_DIRECTION:
            return {
                ...state,
                profile: {...state.profile, direction: action.direction}
            }
        case STATISTIC_VIEW_LOADED_SUCCESS:
            return {
                ...state,
                statisticData: action.statisticData,
                filters: action.filters
            }
        case TELERGAM_VERIFICATION_LOADED:
            return {
                ...state,
                telegramVerification: action.telegramVerification,
                telegramVerificationFailed: false
            }
        case TELERGAM_VERIFICATION_FAILED:
            return {
                ...state,
                telegramVerificationFailed: action.notification
            }

        case EXPERIENCE_REAL_TIME_CHANGING: {
            return {
                ...state,
                profile: {
                    ...state.profile,
                    experience: {
                        ...state.experience,
                        items: action.items,
                        it: action.it,
                        common: action.common,
                        other: action.other
                    }
                }
            }
        }

        case EDUCATIONAL_REAL_TIME_CHANGING: {
            return {
                ...state,
                profile: {
                    ...state.profile,
                    educational: {
                        ...state.educational,
                        items: action.items
                    }
                }
            }
        }
        case CONTACT_PROFILE_APPLICANT:{
            return {
                ...state,
                cvContact: action.applicantInfo
            }
        }
        case ARCHIVE_TESTS_LIST_LOAD:{
            return{
                ...state,
                testArchive: action.testArchive
            }
        }
        case KNOWLEDGES_LOADED_FAIL:
        case PROFILE_LOADED_FAIL:
        case SKILL_TESTS_HIERARCHY_FAILED:
        case TESTS_RESULTS_LOADED_FAILED:
        case TEST_RESULT_CREATED_FAILED:
        case RATING_QUEUE_LOAD_FAILED:
        case STATISTIC_VIEW_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };
        case SAVE_RATING:
            return{
                ...state,
                ratings : action.rating
            }
        default:
            return state
    }
}
