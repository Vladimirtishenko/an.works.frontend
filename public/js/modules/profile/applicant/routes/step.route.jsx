import React from "react";
import { Route, Switch } from "react-router-dom";

import Index from "../containers/steps/index.ctrl.jsx";

import Rating from "../containers/profile/rating.ctrl.jsx";
import Security from "../containers/profile/security.ctrl.jsx";
import Statistics from "../containers/profile/statistic.ctrl.jsx";
import CVRating from "../containers/profile/cvRating.ctrl.jsx";
import ContactUser from "../../../../components/contactUser/ContactUser.jsx";

const routes = [
    {
        path: "/steps/:step?",
        component: Index
    },
    {
        path: "/rating",
        component: Rating
    },
    {
        path: "/security",
        component: Security
    },
    {
        path: "/statistics",
        component: Statistics
    },
    {
        path: "/cv/:id",
        component: CVRating
    },
    {
        path: "/contact",
        component: ContactUser
    },
    {
        path: "*",
        component: Index
    }
];

export default class Steps extends React.Component {
    componentDidUpdate(){
        window.scrollTo(0,0);
    }
    render() {
        return (
            <Switch>
                {routes.map((route, i) => {
                    const Component = route.component;
                    return (
                        <Route
                            key={i}
                            path={route.path}
                            exact
                            render={
                                props => (
                                    <Component type="steps" {...props} />
                                )
                            }
                        />
                    )
                })}
            </Switch>
        );
    }
}
