import React from 'react'
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

import ProfileInfo from '../containers/profile/personalInformation.ctrl.jsx'
import Education from '../containers/profile/education.ctrl.jsx'
import Skills from '../containers/profile/skills.ctrl.jsx'
import Experience from '../containers/profile/experience.ctrl.jsx'
import Location from '../containers/profile/location.ctrl.jsx'
import Tests from '../containers/tests/tests.ctrl.jsx'
import Rating from '../containers/profile/rating.ctrl.jsx'
import Security from '../containers/profile/security.ctrl.jsx'
import About from '../containers/profile/about.ctrl.jsx'
import Statistics from '../containers/profile/statistic.ctrl.jsx'
import CV from "../containers/profile/cv.ctrl.jsx";
import CVRating from "../containers/profile/cvRating.ctrl.jsx";
import ContactUser from "../../../../components/contactUser/ContactUser.jsx";
import Error from "../../../../components/error/404.jsx"

const routes = [
    {
        path: "/",
        component: ProfileInfo
    },
    {
        path: "/education",
        component: Education
    },
    {
        path: "/skills",
        component: Skills
    },
    {
        path: "/experience",
        component: Experience
    },
    {
        path: "/location",
        component: Location
    },
    {
        path: "/tests",
        component: Tests
    },
    {
        path: "/rating",
        component: Rating
    },
    {
        path: "/security",
        component: Security
    },
    {
        path: "/about",
        component: About
    },
    {
        path: "/statistics",
        component: Statistics
    },
    {
        path: "/cvu",
        component: CV
    },
    {
        path: "/cv/:id",
        component: CVRating
    },
    {
        path: "/contact",
        component: ContactUser
    },
    {
        path: "*",
        component: Error
    }
]

export default class Profile extends React.Component {
    componentDidUpdate(){
        window.scrollTo(0,0);
    }
    render() {
        return (
                <Switch>
                    {
                        routes.map((route, i) => (
                            <Route key={i} path={route.path} exact component={route.component} />
                        ))
                    }
                </Switch>
        )
    }
}
