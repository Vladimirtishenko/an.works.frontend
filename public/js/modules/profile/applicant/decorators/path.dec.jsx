import { connect } from 'react-redux'
import React from 'react'

const applicantPathComponent = (WrapedComponent) => {

  class PathComponent extends React.Component {

    constructor(props, context){
        super(props);

        this.state = {
            router: null
        }
    }

    componentDidMount(){

        let {email, applicantId} = this.props.oauth.user || {};
        this.props.actions.getUserProfile(applicantId);

    }

    componentDidUpdate(prevProps, prevState){

        let {step} = this.props.applicant && this.props.applicant.profile || {},
            {step: previousStep} = prevProps.applicant && prevProps.applicant.profile || {};

        if(step != previousStep){

            let path = step > 0 ? 'step' : 'page';

            this.setState({
                router: path
            })
        }

    }

    render() {

        let {router} = this.state;

        return router && <WrapedComponent {...this.props} router={router} /> || null;

    }

  }

  return PathComponent;

}
export default applicantPathComponent;
