import { connect } from 'react-redux'
import React from 'react'

const applicantProfileCompProgress = (WrapedComponent) => {

  class ProfileProgressComponent extends React.Component {

    constructor(props, context){
        super(props, context);
    }

    componentDidMount(){

        let {step} = this.props.match && this.props.match.params,
            edgeStep = this.props.applicant && this.props.applicant.profile && this.props.applicant.profile.step,
            routesStep = step ? step : 1,
            direction = routesStep > edgeStep ? edgeStep : routesStep;

        this.props.actions.updateStep && this.props.actions.updateStep(direction);

    }

    shouldComponentUpdate(nextProps, nextState){

        let directionPrev = this.props.applicant && this.props.applicant.profile && this.props.applicant.profile.direction || null,
            directionNext = nextProps.applicant && nextProps.applicant.profile && nextProps.applicant.profile.direction || null,
            edgeStep = nextProps.applicant && nextProps.applicant.profile && nextProps.applicant.profile.step,
            {step} = this.props.match && this.props.match.params;

        if(edgeStep == 0) {
            this.props.history.push('/');
            return false;
        }

        if(directionNext != directionPrev){
            this.props.history.replace(`/steps/${directionNext}`);
            return false;
        }

        if( !step || (step > edgeStep) ){
            this.props.history.replace(`/steps/${edgeStep}`);
            return false;
        }

        return true;

    }

    componentDidUpdate(){

        let {page, step} = this.props.match && this.props.match.params,
            directionNext = this.props.applicant && this.props.applicant.profile && this.props.applicant.profile.direction || null;

        if(step != directionNext){
            this.props.actions.updateStep && this.props.actions.updateStep(step);
        }

    }


    render() {

        let direction = this.props.applicant && this.props.applicant.profile && this.props.applicant.profile.direction || null;

        return direction ? <WrapedComponent {...this.props} /> : null;

    }

  }

  return ProfileProgressComponent;

}
export default applicantProfileCompProgress;
