
export const stepsSchema = (translation = {}) => {

    return [
        {
            id: 1,
            path: '/steps/1',
            name: translation.personal
        },
        {
            id: 2,
            path: '/steps/2',
            name: translation.skills
        },
        {
            id: 3,
            path: '/steps/3',
            name: translation.tests
        },
        {
            id: 4,
            path: '/steps/4',
            name: translation.experience
        },
        {
            id: 5,
            path: '/steps/5',
            name: translation.education
        },
        {
            id: 6,
            path: '/steps/6',
            name: translation.location
        },
        {
            id: 7,
            path: '/steps/7',
            name: translation.about
        }
    ]

}
