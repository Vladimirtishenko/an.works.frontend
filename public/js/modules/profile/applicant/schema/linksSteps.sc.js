
export const linksStepsSchema = (translation = {}) => {

    return [
        {
            name: translation.myCV,
            children: [
                {
                    step: 1,
                    name: translation.personalInfo,
                    path: '/steps/1'
                },
                {
                    step: 2,
                    name: translation.skills,
                    path: '/steps/2'
                },
                {
                    step: 4,
                    name: translation.experience,
                    path: '/steps/4'
                },
                {
                    step: 5,
                    name: translation.education,
                    path: '/steps/5'
                },
                {
                    step: 6,
                    name: translation.location,
                    path: '/steps/6'
                },
                {
                    step: 7,
                    name: translation.about,
                    path: '/steps/7'
                }
            ]
        },
        {
            step: 3,
            name: translation.tests,
            path: '/steps/3'
        },
        {
            name: translation.ranking,
            path: '/rating'
        },
        {
            name: translation.statistics,
            hide: true
        },
        {
            separate: true,
            name: translation.security,
            path: '/security'
        }
    ]

}
