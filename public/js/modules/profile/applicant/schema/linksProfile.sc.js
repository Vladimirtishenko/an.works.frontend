
export const linksProfileSchema = (translation = {}) => {

    return [
        {
            name: translation.myCV,
            children: [
                {
                    name: translation.personalInfo,
                    path: '/'
                },
                {
                    name: translation.skills,
                    path: '/skills'
                },
                {
                    name: translation.experience,
                    path: '/experience'
                },
                {
                    name: translation.education,
                    path: '/education'
                },
                {
                    name: translation.location,
                    path: '/location'
                },
                {
                    name: translation.about,
                    path: '/about'
                }
            ]
        },
        {
            name: translation.tests,
            path: '/tests'
        },
        {
            name: translation.ranking,
            path: '/rating'
        },
        {
            name: translation.statistics,
            path: '/statistics'
        },
        {
            separate: true,
            name: translation.security,
            path: '/security'
        }
    ]

}