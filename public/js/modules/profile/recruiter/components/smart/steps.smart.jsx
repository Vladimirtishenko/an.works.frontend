import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Loadable from 'react-loadable';
import Loading from '../../../../../components/widgets/loader.jsx'

class Steps extends React.Component {

  constructor(props){
    super(props);

    this.state = {
        component: null
    }
  }

  componentDidMount(){

      let {direction} = this.props.recruterProfile;

      this.setState({
          component: {
              [direction]: this.setAsyncComponent(direction)
          }
      })
  }

  setAsyncComponent(step){

      return Loadable({
        loader: () => import(`../steps/${step}.jsx`),
        loading: Loading,
        delay: 500,
        timeout: 10000
      })

  }

  componentDidUpdate(prevProps, prevState){

      let directionNext = this.props.recruterProfile && this.props.recruterProfile.direction || null,
          directionPrev = prevProps.recruterProfile && prevProps.recruterProfile.direction || null;

      if(directionNext != directionPrev && !this.state.component[directionNext]){
          this.setState({
              component: {...this.state.component, [directionNext]: this.setAsyncComponent(directionNext)}
          })
      }

  }

  componentWillUnmount(){
      this.setState({
        component: null
      });
  }

  render(){

    let component = this.state.component,
        {direction} = this.props.recruterProfile || null;

    if(!component || !direction || !component[direction]) return null;

    let Component = component[direction];

    return ( <Component {...this.props} /> );

  }

}

export default Steps;
