import React from "react";
import Pagination from "../../../common/components/pagination.jsx";
import RecruiterHires from "../../../common/components/not-editable/recruiterSingle/recruiterHires.jsx";
import StatisticsRecruiterTokens from "../../../common/components/not-editable/recruiterSingle/statisticsRecruiterTokens.jsx";

import {_getAggregatedHiringData, getEmptyAggregatedHiringData} from './../../../../../helpers/aggregateData.helper.js';
import CountOfTotal from '../../../../profile/common/components/countOfTotal.jsx';

import LimitFilter from "../../../../../components/filters/publicFilters/limit.jsx";
import HireDatesFilter from "../../../../../components/filters/publicFilters/hireDatesFilter.jsx";

import RecruiterTableRow from "../../../common/components/not-editable/recruiterSingle/recruiterTableRow.jsx";
import { HireContactsInfo } from "../../../applicant/components/modals/hireContactsInfo.jsx";

import {
    dmyDate
    } from '../../../../../helpers/date.helper.js'; 

import {
    normalizeLimit,
    convertLimitToValue
} from '../../../../../helpers/filters/limit.helper.js';

import {
    convertOffsetToValue
} from '../../../../../helpers/filters/offset.helper.js';

import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from '../../../../../helpers/filters/orderBy.helper.js'

import {
    deteleOneOf
} from "../../../../../helpers/filters/filter.helper.js";

export default class StatisticsComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            needClass: true,
            isContactsShown: false,
            shownContactId: undefined
        };
    }

    handleShowContacts(status, id) {
        this.setState({
            ...this.state,
            isContactsShown: status,
            shownContactId: status ? id : undefined
        });
    }

    changeLimit(label){

        const { recruterProfile: { filters }, actions: {getRecruiterHireHistory} } = this.props,
                newFiltersWithLimit = normalizeLimit(filters, label);

        getRecruiterHireHistory(newFiltersWithLimit);

    }

    sortingTable(event){
        const { target: { dataset: { sort, value } } = {} } = event,
              { recruterProfile: { filters }, actions: {getRecruiterHireHistory} } = this.props,
              filtersNormalizedWithNewOrderBy = normalizeObderBy(filters, sort, value);

        getRecruiterHireHistory(filtersNormalizedWithNewOrderBy);
 
    } 

    onChangePeriod(filters){
        const { actions: { getRecruiterHireHistory } } = this.props,
        deletOffsetToFilters = deteleOneOf(filters,'offset');
        getRecruiterHireHistory(deletOffsetToFilters);
    }

    onChangePagination(filters){

        const { actions: { getRecruiterHireHistory } } = this.props;

        getRecruiterHireHistory(filters);

    }

    addClassName() {
        this.setState({
            ...this.state,
            needClass: false 
        });
    }
    updateHiringStatus(data){
        const {
            actions:{
                updateHiringStatus
            }
        } = this.props;
        updateHiringStatus(data);
    }
    render() {
        const {
            i18n:{
                translation = {}
            } = {},
            actions,
            recruterProfile: { 
                createdDate = '',
                applicantsList: { 
                    items = [],
                    totalCount,
                    balance = '',
                    statistics = {}
                } = {},
                filters, uid = "", applicantsList
            } = {} } = this.props,
            data = items,
            limitToValue = convertLimitToValue(filters),
            offsetTovalue =  convertOffsetToValue(filters),
            { isContactsShown, shownContactId } = this.state,
            justOrderBy = orderBy(filters),
            shownContact = shownContactId && items.find(c => c.applicantId === shownContactId),
            creatDate = dmyDate(createdDate),
            hiresData =  _getAggregatedHiringData(statistics);

        return (
            <div> 
            <p className="margin--b-15 font--18 font--500 font--color-primary padding--xc-30">
                {translation.statistics+" "}
                <span className="display--b font--12 font--color-secondary">
                    {`${translation.inServiceWith} ${creatDate}`}
                </span>
            </p>
            <div className="box--white box--rounded">
                <div className="row no-gutters hr--bottom-theme-light-gray">
                    <div className="row no-gutters width--100">
                        <div className="col-md-6 hr--lg-left-theme-light-gray hr--top-theme-light-gray hr--lg-top-none">
                            {applicantsList && (
                                <RecruiterHires 
                                    data={hiresData}
                                />
                             )}
                        </div>
                        <div className="col-md-6 hr--md-left-theme-light-gray hr--top-theme-light-gray hr--lg-top-none">
                            {applicantsList && (
                                <StatisticsRecruiterTokens
                                    balance={balance}
                                    totalCount={hiresData.spendTokensCount}
                                />
                            )}
                        </div>
                    </div>
                </div>
                <div className="padding--xl-xc-30 bg--light-gray padding--xc-15 padding--yc-20 padding--xl-yc-25 fl fl--justify-b fl--align-c fl--wrap">
                        <div className="fl fl--align-c fl--wrap">
                            <LimitFilter
                                className="padding--xc-5"
                                onChange={::this.changeLimit}
                                value={limitToValue}
                            />
                            <div className="font--color-secondary font--14">
                                {`${translation.hiringHistory} ${translation.for} `}
                                <HireDatesFilter
                                     onChange={::this.onChangePeriod}
                                     filters={filters}
                                />
                            </div>
                        </div>
                    {/*
                    TODO: no mvp 
                    <TableControls /> */}
                </div>
                <table className="respons-flex-table">
                    <thead className="respons-flex-table__thead">
                        <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-20">
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-14
                                        font--left
                                    "
                            >
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="openDate"
                                    // data-value={justOrderBy.openDate || ""}
                                    // className={`${orderingByClass(justOrderBy.openDate)}`}
                                > 
                                    {translation.openingDate}
                                </span>
                            </th>
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-14
                                        font--left
                                    "
                            >
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="applicant"
                                    // data-value={justOrderBy.applicant || ""}
                                    // className={`${orderingByClass(justOrderBy.applicant)}`}
                                >
                                    {`${translation.applicant},${translation.id || ''}`}
                                </span>
                            </th>
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-17
                                        font--left
                                    "
                            >
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="position"
                                    // data-value={justOrderBy.position || ""}
                                    // className={`${orderingByClass(justOrderBy.position)}`}
                                >
                                    {translation.workPosition}
                                </span>
                            </th>
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-25
                                        font--left
                                    "
                            >
                                <span>{translation.contacts}</span>
                            </th>
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-10
                                        respons-flex-table--xl-j-center
                                        font--left
                                        font--xl-center 
                                    "
                            >
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="price"
                                    // data-value={justOrderBy.price || ""}
                                    // className={`${orderingByClass(justOrderBy.price)} respons-flex-table__sort-des`}
                                >
                                    {translation.bidsPerContact}
                                </span>
                            </th>
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-15
                                        font--left
                                        font--xl-center
                                    "
                            >
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="status"
                                    // data-value={justOrderBy.status || ""}
                                    // className={`${orderingByClass(justOrderBy.status)}`}
                                >
                                    {translation.condition}
                                </span>
                            </th>
                            <th
                                className="
                                        respons-flex-table__th
                                        respons-flex-table--xl-w-5
                                        font--left
                                    "
                            >
                                <span className="">{translation.myCV}</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody className="respons-flex-table__tbody">
                        { 
                            data.length && data.map((item , i) => {
                                return <RecruiterTableRow key={i} data={item} handleShowContacts={::this.handleShowContacts} updateHiringStatus={::this.updateHiringStatus} hireHistory={false}/>
                            }) || <tr className="respons-flex-table__tr">
                                    <td className="respons-flex-table__td respons-flex-table__td--no-before ">{translation.infoStatistics}</td>
                                </tr>
                        }
                    </tbody>
                    <tfoot className="respons-flex-table__tfoot fl padding--md-xc-30 padding--xc-20  padding--yc-20 fl--wrap">
                        <tr className="d-block width--100">
                            <td className="fl fl--align-c fl--justify-b fl--wrap">
                                <CountOfTotal limit={limitToValue} total={totalCount} offset={offsetTovalue}/> 
                                <div className="fl fl--justify-end">
                                    {
                                        (totalCount > limitToValue) && <Pagination 
                                            count={totalCount}
                                            actions={::this.onChangePagination}
                                            filters={filters}
                                            wrapperClass="pagination margin--yc-10"
                                        /> || null
                                    }
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {isContactsShown && shownContact && (
                    <HireContactsInfo
                        data={shownContact.additionalData.applicant}
                        applicantInfo={shownContact.additionalData.applicant.contactInfo}
                        closeContacts={() => this.handleShowContacts(false)}
                    />
                )}
            </div>
        </div>
        );
    }
}
