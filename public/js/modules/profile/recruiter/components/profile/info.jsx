import React from 'react'

import StaticItem from '../../../../../components/form/staticItem.jsx';

import {Card} from '../../../common/components/card.dumb.jsx'

// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

// Not Editable
import Personal from '../not-editable/personal.jsx'
import Contact from '../../../common/components/not-editable/contact.jsx'

// Edatable
import PersonalEditable from '../editable/personal.jsx'
import ContactEditable from '../../../common/components/editable/contactCard.jsx'

import { ValidatorForm } from "../../../../../libraries/validation/index.js";

import i18nMonthDecorator from "../../../../i18n/decorators/month.dec.jsx";
@i18nMonthDecorator
class PersonalInfo extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            personal: false,
            contact: false
        }
    }

    changeView(event){

        let component = event && event.target && event.target.dataset && event.target.dataset.component || null;

        if(component){
            this.setState({
                [component]: !this.state[component]
            })
        }
    }
    componentDidMount() {
        if (
            this.props.recruterProfile &&
            !this.props.recruterProfile.telegramRegistrationUrl
        ) {
            this.props.actions.isTelegramVerified();
            this.props.actions.getTelegramRegistrationUrl();
        }
    }
    submitPersonal(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            {email} = this.props.oauth.user,
            {companyId} = this.props.oauth.user,
            {contactInfo} = this.props.recruterProfile && this.props.recruterProfile.profile || {},
            date = serializedForm.birthday && Array.isArray(serializedForm.birthday) ? serializedForm.birthday.reverse() : '',
            birthday = date ? (new Date( date.join('/') )).valueOf() : '',
            data = {
                ...serializedForm,
                contactInfo
            },
            {
                recruterProfile:{
                    profile = ''
                } = {}
            } = this.props;

            serializedForm.birthday = birthday;

        if( _.isEqual(serializedForm, this.propsPersonalMapper(profile)) ){
            return this.setState({personal: false})
        }

        this.props.actions.updateEmployeeProfile(data, companyId, email);

    }

    submitContact(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            {email} = this.props.oauth.user,
            {companyId} = this.props.oauth.user,
            {contactInfo} = this.props.recruterProfile && this.props.recruterProfile.profile || {},
            data = {
                ...this.props.recruterProfile.profile,
                contactInfo: serializedForm.contactInfo
            };

        if( _.isEqual(serializedForm.contactInfo, contactInfo) ){
            return this.setState({contact: false})
        }

        this.props.actions.updateEmployeeProfile(data, companyId, email);

    }

    propsPersonalMapper(props){

        let personal = ['firstName', 'lastName', 'birthday', 'gender', 'positionTitle'],
            object = {};


        _.forEach(personal, (item, i) => {
            if(props[item]){
                object[item] = item == 'birthday' ? (new Date(props[item])).valueOf() : props[item];
            }
        })

        return object;


    }

    componentDidUpdate(prevProps, prevStep){

        let prevPersonalInfo = this.propsPersonalMapper(prevProps.recruterProfile.profile),
            nextPersonalInfo = this.propsPersonalMapper(this.props.recruterProfile.profile),
            contactPrev = prevProps.recruterProfile.profile && prevProps.recruterProfile.profile.contactInfo,
            contactNext = this.props.recruterProfile.profile && this.props.recruterProfile.profile.contactInfo;

        if(!_.isEqual(prevPersonalInfo, nextPersonalInfo)){
            this.setState({
                personal: false
            })
        }

        if(!_.isEqual(contactPrev, contactNext)){
            this.setState({
                contact: false
            })
        }

    }

    render() {

        let {personal, contact} = this.state,
            {
                actions,
                recruterProfile:{
                    telegramRegistrationUrl = '',
                    telegramVerification = '',
                    telegramVerificationFailed = '',
                    profile:{
                        contactInfo = {}
                    }
                },
                i18n:{
                    translation = {}
                } = {}
            } = this.props;

        return (
            <div className="form">
                <h2 className="font--color-primary font--18 font--500 margin--b-32 padding--l-30 l-h--1">
                    Личная Информация
                </h2>
                {personal ?
                    <ValidatorForm onSubmit={::this.submitPersonal}>
                        <PersonalEditable {...this.props} />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">{translation.save}</button>
                        </div>
                    </ValidatorForm> :
                    <Personal onEdit={::this.changeView} {...this.props} />
                }
                {contact ?
                    <ValidatorForm onSubmit={::this.submitContact}>
                        <ContactEditable contactInfo={contactInfo}
                            actions={actions}
                            inputPhoneSize="col-md-6 col-lg-5 col-xl-4"
                            inputSizeSocial="col-md-6 col-lg-5 col-xl-4"
                            wrapBtnTelegram="col-7 col-md-2 offset-md-8 padding--md-0 margin--md-t-10 offset-lg-6 margin--xl-0 padding--xl-xc-15 "
                            positionBtnRemove="margin--md-r-17-p absolute--lg-r-25-p absolute--xl-r-35-p "
                            telegramVerification={telegramVerification}
                            telegramRegistrationUrl={
                                telegramRegistrationUrl && telegramRegistrationUrl.url
                            }
                            telegramVerificationFailed={telegramVerificationFailed}
                        />
                        <div className="fl fl--justify-c fl--align-c">
                            <button className="btn btn--primary font--14 padding--y-10 padding--x-20">{translation.save}</button>
                        </div>
                    </ValidatorForm> :
                    <Contact 
                        onEdit={::this.changeView}
                        contactInfo={contactInfo}
                        notEdit={true}
                        telegramVerification={telegramVerification}
                        telegramVerificationFailed={telegramVerificationFailed}
                    />
                }
            </div>
        )
    }

}

export default PersonalInfo;
