import React from "react";

// Form Componets
import Input from "../../../../../components/form/text.jsx";
import Radio from "../../../../../components/form/radio.jsx";
import DateCustom from "../../../../../components/form/dateCustom.jsx";
import Combobox from "../../../../../components/form/combobox.jsx";
// Componets
import { Card } from "../../../common/components/card.dumb.jsx";

import additionPositionRecruter from "../../../../../databases/general/additionPositionRecruter.json";

import {
    separateDate,
    preparationMonth
} from "../../../../../helpers/date.helper.js";
import {
    comparingGenderValue
} from '../../../../../helpers/gender.helper.js'
class Personal extends React.Component {

    static defaultProps = {
        wrapperClass:"row fll--align-c",
        labelClass:"label form__label font--14 col-md-4 col-lg-3",
        inputWrapperClass: "col-md-6 col-lg-5 col-xl-4 padding--md-0"
    }
    
    render() {
        let { translation = {} } = this.props.i18n,
            { profile } = this.props.recruterProfile || {},
            { birthday } = profile || null,
            date = separateDate(birthday),
            monthPrepare = preparationMonth(translation.month);

        return (
            <Card
                icon="personal"
                title={translation.personalInfo}
                cardPadding="padding--15 padding--md-30"
            >
                <Input
                    validators={["required", "isName","maxStringLength:50"]}
                    errorMessages={[translation.errorMessageRquired,translation.errorMessageName,translation.errorMessageMaxLeng50 ]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-10`}
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.inputWrapperClass}
                    inputClass="input form__input"
                    value={profile && profile.firstName}
                    label={translation.firstName}
                    name="firstName"
                />
                <Input
                    validators={["required", "isName","maxStringLength:50"]}
                    errorMessages={[translation.errorMessageRquired,translation.errorMessageNameTitle,translation.errorMessageMaxLeng50 ]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-10`}
                    labelClass={this.props.labelClass}
                    inputWrapperClass={this.props.inputWrapperClass}
                    inputClass="input form__input"
                    value={profile && profile.lastName}
                    label={translation.lastName}
                    name="lastName"
                />

                <Radio
                    validators={["radioRequired"]}
                    errorMessages={[translation.errorMessageRquired]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-20`}
                    labelClass={this.props.labelClass}
                    wrapRadio={this.props.inputWrapperClass}
                    label={translation.gender}
                    name="gender"
                    value={comparingGenderValue(profile.gender, 'fl fl--align-c')}
                    wrapperClassRadio="padding--md-0"
                />

                <DateCustom
                    validators={["required", "isDate"]}
                    errorMessages={[translation.errorMessageRquired,translation.errorMessageIsDateRange]}
                    wrapperClass={`${this.props.wrapperClass} margin--b-20`}
                    labelClass={this.props.labelClass}
                    coverClass={this.props.inputWrapperClass}
                    day={{
                        name: "birthday[]",
                        value: date.day
                    }}
                    month={{
                        name: "birthday[]",
                        value: date.month,
                        options: monthPrepare,
                        static: "Месяц"
                    }}
                    year={{
                        name: "birthday[]",
                        value: date.year
                    }}
                    label={translation.birthday}
                />
                 <Combobox
                    validators={["required"]}
                    errorMessages={[translation.errorMessageRquired]}
                    name="positionTitle"
                    wrapperClass="row  fl--align-c combobox"
                    labelClass={this.props.labelClass}
                    className={this.props.inputWrapperClass}
                    label={translation.workPosition}
                    list={additionPositionRecruter}
                    value={profile && profile.positionTitle}
                />
            </Card>
        );
    }
}

export default Personal;
