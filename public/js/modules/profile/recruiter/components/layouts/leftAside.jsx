import React from "react";

import Avatar from "../../../common/components/editable/avatar.jsx";
import { Link } from "react-router-dom";

import Menu from "../../../../../components/includes/menu.jsx";

import { connect } from "react-redux";
import * as oauth from "../../../../oauth/actions/oauth.action.js"; 
import * as activityActions from "../../../common/actions/activity.action.js";
import { bindActionCreators } from "redux";

function mapStateToProps(state) {
    return {
        ...state.activity
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {   
                ...oauth,
                ...activityActions
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
class LeftAside extends React.Component {
    constructor(props) {
        super(props);

        let {
                links,
                recruterProfile: { profile = {} } = {},
                recruterProfile = {},
            } = this.props || {},
            { step } = profile || {};

        this.state = {
            links: links,
            step: step,
            profile: profile,
            recruterProfile: recruterProfile
        };
    }

    mappingName(prevProps) {
        let names = ["firstName", "lastName"],
            changed = false,
            { 
                recruterProfile: {
                    profile = {}
                } = {}
            } = this.props,
            {  
                recruterProfile: {
                    profile: prevProfile 
                } = {}
            } = prevProps;

        _.forEach(names, (item, i) => {
            if (profile && prevProfile && profile[item] !== prevProfile[item]) {
                changed = true;
                return changed;
            }
        });

        return changed;
    }

    componentDidUpdate(prevProps, prevState) {
        if (::this.mappingName(prevProps)) {
            this.setState({
                ...this.state,
                profile: this.props.recruterProfile.profile
            });
        }
    }

    render() {
        let { links, profile, step,recruterProfile = {} } = this.state,
            { lastName, firstName } = profile || null,
            {email = ""} = recruterProfile,
            name = lastName && firstName ? firstName + " " + lastName : "";
        const {
            mobileMenu,
            isBurgerMenuOpen,
            i18n:{
                translation = {}
            } = {},
            actions:{
                logout = ()=>{}
            }} = this.props;
        return (
            <aside className={mobileMenu ? 'hide--lg' : null}>
                <div
                    className={`side-menu margin--t-50 shadow--box ${
                        isBurgerMenuOpen ? "" : "side-menu_disabled"
                    }`}
                >
                    <div className="side-menu__top bg--white margin--b-2 margin--lg-b-5">
                        <Avatar wrapperClass="avatar--sidebar margin--auto-xc margin--b-minus-35" />
                        <div className="padding--15 padding--lg-l-25 padding--md-l-15 padding--md-r-15 padding--b-20 padding--r-25">
                            <Link to="/" className="">
                                {(name && (
                                    <div className="font--16 font--500 font--color-primary word-break--word">
                                        {name}
                                    </div>
                                )) ||
                                    null}
                                <div className="link font--12 font--color-secondary word-break--word">
                                    {email}
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="">
                        <Menu
                            name={name}
                            main={true}
                            data={links}
                            step={step} 
                            translation={translation}
                            logout={logout}
                        /> 
                    </div>
                </div>
            </aside>
        );
    }
}

export default LeftAside;
