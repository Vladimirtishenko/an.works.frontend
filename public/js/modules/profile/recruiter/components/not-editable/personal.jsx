import React from 'react'
import StaticItem from '../../../../../components/form/staticItem.jsx';
import {Card} from '../../../common/components/card.dumb.jsx'
import recruterList from '../../../../../databases/general/additionPositionRecruter.json';
import {
    separateDate,
    preparationMonth,
    getTextMonth
} from '../../../../../helpers/date.helper.js'
import {
    getGenderStaticName
} from '../../../../../helpers/gender.helper.js'    

export default class Personal extends React.Component {

    checkPositionRecruter(value){
        let position = '';
        recruterList.forEach((item, i) =>{ 
            if(item.label == value){
                position = item.name;
            }
         }
        );
        return position;  
    }
    render() {
        let {translation = {}} = this.props.i18n,
            {profile:{gender = '',firstName = '', lastName = '',birthday = '',positionTitle = ''},profile={}} = this.props.recruterProfile,
            date = separateDate(birthday),{day,month,year} = date,
            monthPrepare = preparationMonth(translation.month),
            genderRealName = getGenderStaticName(gender),
            datePutTogether = day + ' ' + getTextMonth(month, monthPrepare) + ' ' + year,
            position = this.checkPositionRecruter(positionTitle);
        return (
            <Card
                icon="personal"
                iconRight="edit"
                link={translation.edit}
                linkAction={this.props.onEdit}
                linkComponent="personal"
                title={translation.personalInfo}
                cardPadding="padding--15 padding--md-30"
            >
                <div className="row word-break--all"> 
                    <div className="col-12 col-sm-4 col-lg-3 margin--b-15 margin--sm-bottom-0">
                        <div className="font--capitalized font--color-inactive font--12 margin--b-15">{translation.firstName}</div>
                        <StaticItem
                            staticClass="font--14 form__static-input font--capitalize"
                            staticText={`${firstName} ${lastName}`}
                        />

                    </div>
                    <div className="col-6 col-sm-4 col-lg-3">
                        <div className="font--capitalized font--color-inactive font--12 margin--b-15">{translation.birthday}</div>
                        <StaticItem
                            staticClass="font--14 form__static-input"
                            staticText={datePutTogether}
                        />

                    </div>
                    <div className="col-6 col-sm-4 col-lg-2">
                        <div className="font--capitalized font--color-inactive font--12 margin--b-15">{translation.gender}</div>
                        <StaticItem
                            staticClass="font--14 form__static-input"
                            staticText={genderRealName}
                        />
                    </div>
                    <div className="col-6 col-sm-4 col-lg-4">
                        <div className="font--capitalized font--color-inactive font--12 margin--b-15">{translation.workPosition}</div>
                        <StaticItem
                            staticClass="font--14 form__static-input"
                            staticText={position}
                        />
                    </div>
                </div>
            </Card>
        )
    }
}
