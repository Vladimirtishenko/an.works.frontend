import React from 'react'

// Additional components
import ContactCard from '../../../common/components/editable/contactCard.jsx'
import Personal from '../editable/personal.jsx'

// Helpers
import serialize from '../../../../../helpers/serialize.helper.js'

import i18nMonthDecorator from '../../../../i18n/decorators/month.dec.jsx'

import { ValidatorForm } from '../../../../../libraries/validation/index.js';

@i18nMonthDecorator
class First extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            isOverlayed: false,
            data: props.recruterProfile && props.recruterProfile.profile && {...props.recruterProfile.profile}
        }
    }
    componentDidMount() {
        if (
            this.props.recruterProfile &&
            !this.props.recruterProfile.telegramRegistrationUrl
        ) {
            this.props.actions.isTelegramVerified();
            this.props.actions.getTelegramRegistrationUrl();
        }
    }
    submit(event){

        event.preventDefault();

        let form = event && event.target;

        if(!form) return;

        let serializedForm = serialize(form),
            {email} = this.props.oauth.user,
            {companyId} = this.props.oauth.user,
            {direction} = this.props.recruterProfile,
            date = serializedForm.birthday && Array.isArray(serializedForm.birthday) ? serializedForm.birthday.reverse() : '',
            birthday = date ? (new Date( date.join('/') )).valueOf() : '';
            serializedForm.birthday = birthday;

        this.props.actions.updateEmployeeProfile(serializedForm, companyId, email);

    }

    render () {

        let {translation} = this.props.i18n,
            {
                actions,
                profile,
                telegramRegistrationUrl,
                telegramVerification,
                telegramVerificationFailed
            } = this.props.recruterProfile || {};

        return (

            <ValidatorForm onSubmit={::this.submit} className="form">

                <Personal {...this.props} />

                <ContactCard contactInfo={profile && profile.contactInfo || {}}
                    actions={actions}
                    inputPhoneSize="col-md-5 col-lg-4"
                    inputSizeSocial="col-md-5 col-lg-4"
                    positionBtnRemove="margin--md-r-25-p absolute--lg-r-33-p"
                    telegramVerification={telegramVerification}
                    telegramRegistrationUrl={
                        telegramRegistrationUrl && telegramRegistrationUrl.url
                    }
                    telegramVerificationFailed={telegramVerificationFailed}
                />

                <div className="fl fl--justify-c fl--align-c">
                    <button className="btn btn--primary font--14 padding--y-10 padding--x-20 margin--t-10">{translation.next_step}</button>
                </div>

            </ValidatorForm>
        )
    }
}

export default First;
