import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as recruterAction from '../../actions/recruiter.action.js'

import MainComponent from '../../components/profile/main.jsx'

import {linksProfileSchema} from '../../schema/linksProfile.sc.js'
import TopLayout from '../../../../../components/includes/layout.jsx'
import Layout from '../../components/layouts/layout.jsx'

function mapStateToProps(state) {

    return {
        recruterProfile: state.profile.companyEmployeeProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
			...recruterAction
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps)
export default class Main extends React.Component {
    render(){
        return (
            <TopLayout>
                <Layout {...this.props} links={linksProfileSchema(this.props.i18n.translation)} progressBar={false}>
                    <MainComponent {...this.props}/>
                </Layout>
            </TopLayout>
        )
    }
}
