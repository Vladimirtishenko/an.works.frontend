import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as recruiterAction from "../../actions/recruiter.action.js";

import StatisticsComponent from "../../components/profile/statistics.jsx";
import Layout from "../../components/layouts/layout.jsx";
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import TopLayout from '../../../../../components/includes/layout.jsx'


import queryObject from "../../../../../helpers/queryObject.js";
import {
    transformToRange
} from "../../../../../helpers/date.helper.js";
function mapStateToProps(state) {
    return {
        recruterProfile: state.profile.companyEmployeeProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...recruiterAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Statistics extends React.Component {

    componentDidMount() {
        //TODO:: add filters
        const {
            actions: { getRecruiterHireHistory },
            location: { search }
        } = this.props;

        let filtersNormolize = queryObject.stringify({
            filters:JSON.stringify({label: 'currentMonth',createdDate: transformToRange('currentMonth')})     
        }),
        filterHire =  search.slice(1) ? search.slice(1) : filtersNormolize;

        getRecruiterHireHistory(filterHire);
    }


    componentDidUpdate(prevProps) {

        const { recruterProfile: { filters: prevFilters } } = prevProps,
              { history: {replace}, recruterProfile: { filters: nextFilters } } = this.props;

        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }
    }

    render() {
        return (
            <TopLayout>
                <Layout 
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                    progressBar={false}
                >
                    <StatisticsComponent {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}
