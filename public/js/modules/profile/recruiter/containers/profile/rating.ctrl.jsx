import React from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions 
import * as recruiterAction from "../../actions/recruiter.action.js";
import * as actionQuiz from "../../../common/actions/quiz.action.js"
import RaitingComponent from "../../../common/components/not-editable/ratingTable/rating.jsx";

// Docorator
import withKnowledgesDictionary from '../../decorators/knowledges.dec.jsx'
 
// Helper
import { mapperSkillsSorting } from "../../../../../helpers/mapperSkills.helper.js";

// Layout
import TopLayout from '../../../../../components/includes/layout.jsx';
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import Left from "../../components/layouts/leftAside.jsx";
import {isMobile} from "react-device-detect";

// Configuration
import ratingConfuguration from '../../../../../configuration/rating.json';

function mapStateToProps(state) {
    return {
        recruterProfile: state.profile.companyEmployeeProfile,
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...recruiterAction,
                ...actionQuiz
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
@withKnowledgesDictionary
export default class Rating extends React.Component {
    componentDidMount() {
        const { 
            match: { params: { id } },
            actions: { getQueue,getBalance, getSkillTestHierarchy },
            location: {search},
            oauth:{
                user:{
                    companyId = ''
                } = {}
            } = {},
            companyProfile:{ratings}
        } = this.props,
        { skill, level } = ratingConfuguration;
        getQueue(ratings ? ratings : level + '_' + skill, search.slice(1));
        getBalance(companyId, search.slice(1));
        getSkillTestHierarchy(skill);
    }

    componentDidUpdate(prevProps) {

        const { companyProfile: { filters: prevFilters } } = prevProps,
              { history: {replace}, companyProfile: { filters: nextFilters } } = this.props;


        if(prevFilters !== nextFilters){
            replace('?'+nextFilters)
        }

    }
    renderContent = () => {
        if (isMobile) {
            return <Left sideBarClassName="margin--t-44" mobileMenu={true} {...this.props} links={linksProfileSchema(this.props.i18n.translation)}/>
        }
        return null
    };

    render() {
        return (
                <TopLayout container="container-fluid">
                    {this.renderContent()}
                    <RaitingComponent {...this.props} recruiter={true} />
                </TopLayout>
            )
    }
}
