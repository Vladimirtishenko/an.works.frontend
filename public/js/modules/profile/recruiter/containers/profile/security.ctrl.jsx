import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as recruiterAction from "../../actions/recruiter.action.js";

import Layout from "../../components/layouts/layout.jsx";
import SecurityComponent from "../../components/profile/security.jsx";
import { linksProfileSchema } from "../../schema/linksProfile.sc";
import TopLayout from '../../../../../components/includes/layout.jsx'

function mapStateToProps(state) {
    return {
        recruterProfile: state.profile.companyEmployeeProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...recruiterAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class Security extends React.Component {
    render() {
        return (    
            <TopLayout>
                <Layout
                    {...this.props}
                    links={linksProfileSchema(this.props.i18n.translation)}
                >
                    <SecurityComponent {...this.props} />
                </Layout>
            </TopLayout>
        );
    }
}
