import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import * as recruterAction from "../../actions/recruiter.action.js";
import * as contact from "../../../common/actions/contact.action.js";
import * as actionQuiz from "../../../common/actions/quiz.action.js";

import CVComponent from "../../../common/components/not-editable/cv/cvCompany.jsx";

// layout
import TopLayout from "../../../../../components/includes/layout.jsx";
import {
    changeOneOf
} from "../../../../../helpers/filters/filter.helper.js";
import {isMobile} from "react-device-detect";
import Left from "../../components/layouts/leftAside.jsx";
import {linksProfileSchema} from "../../schema/linksProfile.sc.js";

function mapStateToProps(state) {
    return {
        recruterProfile: state.profile.companyEmployeeProfile,
        companyProfile: state.profile.companyProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...recruterAction,
                ...contact,
                ...actionQuiz
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
export default class CV extends React.Component {
    componentDidMount() {
        const {
            match:{
                params:{
                    profileId = '',
                    id
                } 
            },
            
            location: {search},
            actions: { getQueue, getWorkSkillsDictionary, getRecruiterHireHistory, getContactProfileCv, getBalance},
            companyProfile:{companyId}
        } = this.props,
        setBuyerToFilters = changeOneOf(search.slice(1), {'buyerUid': id});
        getContactProfileCv(profileId);
        getWorkSkillsDictionary(setBuyerToFilters);
    }

    componentDidUpdate(prevProps) {
        
        if(!this.props.companyProfile.cvContact && !prevProps.companyProfile.cvContact) return

        const { companyProfile: { cvContact:{ mainSkill: prevStatus} = {} } = {} } = prevProps,
        { 
            companyProfile: { cvContact:{ mainSkill: currentStatus} = {} } = {},
            actions: { getSkillTestHierarchy}
        } = this.props;
        
        if(currentStatus !== prevStatus){
            getSkillTestHierarchy(currentStatus);
        }
        
    }
    componentWillUnmount(){
        const {
            actions: { deletContactProfileCv}
        } = this.props;
        
        if(deletContactProfileCv){
            deletContactProfileCv();
        }
    }
    renderContent = () => {
        if (isMobile) {
            return <Left sideBarClassName="margin--t-44" mobileMenu={true} {...this.props} links={linksProfileSchema(this.props.i18n.translation)}/>
        }
        return null
    };
    render() {

        return (
            <TopLayout>
                {this.renderContent()}
                <CVComponent {...this.props} />
            </TopLayout>
        );
    }
}
