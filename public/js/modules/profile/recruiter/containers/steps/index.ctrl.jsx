import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Steps from "../../components/smart/steps.smart.jsx";

import companyEmployeeProgress from "../../decorators/profile.dec.jsx";

// Actions
import * as recruterAction from "../../actions/recruiter.action.js";
import { linksStepsSchema } from "../../schema/linksSteps.sc.js";
import Layout from "../../components/layouts/layout.jsx";

function mapStateToProps(state) {
    return {
        recruterProfile: state.profile.companyEmployeeProfile,
        oauth: state.oauth,
        i18n: state.i18n
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...recruterAction
            },
            dispatch
        ),
        dispatch
    };
}

@connect(
    mapStateToProps,
    mapDispatchToProps
)
@companyEmployeeProgress
class Index extends React.Component {
    render() {
        return (
            <Layout
                {...this.props}
                links={linksStepsSchema(this.props.i18n.translation)}
                progressBar={true}
            >
                <Steps {...this.props} />
            </Layout>
        );
    }
}

export default Index;
