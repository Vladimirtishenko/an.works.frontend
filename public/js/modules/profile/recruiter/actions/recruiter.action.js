import * as types from '../constants/profile.const.js';
import { SET_NOTIFICATION }  from '../../../../libraries/notification/constant/notification.const.js' 
import API from '../../../../services/api.js'

export function getEmployeeProfile(uid, companyId) {

    return dispatch => {

        (async () => {

            let employeeProfile = await API.__get('/companies/' + companyId + '/employees/' + uid, dispatch, types.COMPANY_EMPLOYEE_LOADED_FAILED);

            if (employeeProfile) {
                employeeProfile.step = _getStep(employeeProfile.profile || {});
                dispatch({
                    type: types.COMPANY_EMPLOYEE_LOADED_SUCCESS,
                    profile: employeeProfile
                })
            }
        })();

    }
}
export function getWorkSkillsDictionary() {
    return dispatch => {
        (async () => {
            let knowledges = await API.__get('dictionary/workSkills/', dispatch, SET_NOTIFICATION)
            dispatch({
                type: types.KNOWLEDGES_LOADED,
                knowledges: knowledges 
            })
        })();
    }
}

export function updateEmployeeProfile(data, companyId, email) {
    return dispatch => {
        (async () => {

            let employeeProfile = await API.__post('PATCH', 'api/companies/' + companyId + '/employee/' + email, {
                profile: data
            }, dispatch, types.COMPANY_EMPLOYEE_UPDATED_FAILED);

            if (employeeProfile) {

                dispatch({
                    type: types.COMPANY_EMPLOYEE_UPDATED_SUCCESS,
                    profile: employeeProfile,
                    step: _getStep(employeeProfile.profile || {})
                })
            }

        })();

    }
}
export function getQueue(name, filters) {
    return (dispatch, getState) => {
        (async () => {
            const { profile: { companyProfile: { filters: stateFilters, queue: { queueLabel: oldName } = {} } } } = getState();
            const flt = filters === true ? stateFilters : filters ? filters : '',
                  newName = name ? name : oldName,
                  stringFlt = flt && ('?' + flt) || '';
            dispatch({
                type: types.CANDIDATS_QUEUE_LOAD_SUCCESS,
                queue: {...queue, queueFetch: true},
                filters: flt
            })
    
            let queue = await API.__get('queue_filter/'+newName + stringFlt, dispatch, SET_NOTIFICATION);

            dispatch({
                type: types.CANDIDATS_QUEUE_LOAD_SUCCESS,
                queue: {...queue, queueFetch: true},
                filters: flt
            })

        })();

    }
}



export function getWorkSkills() {
    return dispatch => {
        (async () => {
            let knowledges = await API.__get('dictionary/workSkills/', dispatch, types.KNOWLEDGES_LOADED_FOR_HR_FAIL)
            dispatch({
                type: types.KNOWLEDGES_LOADED_FOR_HR_SUCCESS,
                knowledges: knowledges
            })

        })();

    }
}

export function updateStep(step) {

    return dispatch => {

        dispatch({
            type: types.UPDATE_STEP_DIRECTION_FOR_HR,
            direction: step
        })

    }

}

//will return data by authenticated user data
export function getRecruiterHireHistory(filters = '') {

    return (dispatch , getState) => {

        (async () => {
            const { profile: { companyEmployeeProfile: { filters: stateFilters } } } = getState();
            const flt = filters === true ? stateFilters : filters ? filters : '',
            stringFilters = flt && ('?' + flt) || '';
            
            let applicantsList = await API.__get('/view/recruiter/statistics/' + stringFilters, dispatch, null);
            dispatch({
                type: types.APLICANT_LIST_LOAD_SUCCESS,
                applicantsList: applicantsList,
                filters: flt
            })

        })();

    }
}

export function getOpenContact(companyId, stringFilters = '') {

    return (dispatch, getState) => {
        (async () => {
            const { profile: {companyProfile: {filters} } }= getState();
            const applicantsList = await API.__get('hiring_contacts/filter/company/' + companyId +'?'+ stringFilters, dispatch, null);

            dispatch({
                type: types.APLICANT_LIST_LOAD_SUCCESS,
                applicantsList: applicantsList,
                filters: filters
            })

        })();

    }
}

export function getSkillTestHierarchy(mainSkill) {

    return dispatch => {

        (async () => {

            let skillTestsHierarchy = await API.__get('dictionary/skillsTestsHierarchy/' + mainSkill, dispatch, SET_NOTIFICATION)

            if (skillTestsHierarchy) {
                dispatch({
                    type: types.SKILL_TESTS_HIERARCHY,
                    skillTestsHierarchy: skillTestsHierarchy
                })
            }
        })();

    }
}
export function getHireHistory(companyId, filters = '') {

    return (dispatch, getState) => {
        (async () => {

            const { profile: {companyProfile: {filters: stateFilters = ''}} } = getState(),
                  flt = filters === true ? stateFilters : filters ? filters : '',
                  stringFilters = flt ? '?' + filters : '',
                  applicantsList = await API.__get('hiring_contacts/filter/company/' + companyId + stringFilters, dispatch, null);

            dispatch({
                type: types.APLICANT_LIST_LOAD_SUCCESS,
                applicantsList: applicantsList,
                filters: flt
            })

        })();

    }
}
export function buyApplicant(profileId, queueLabel, companyId) {

    return dispatch => {

        (async () => {

            let result = await API.__post('POST', 'api/buy_applicant', {
                profileId: profileId,
                queueLabel: queueLabel,
                companyId: companyId
            }, dispatch, SET_NOTIFICATION);

            if (result) {

                dispatch({
                    type: types.CANDIDAT_BUY_SUCCESS,
                    buyResult: result,
                    notification: {
                        type: 'success',
                        view: 'notice',
                        message: 'Сontact was purchased!'
                    }
                })
            }
        })();

    }
}

export function updateProfilePassword(data, uid) {
    return dispatch => {
        (async () => {
            let userProfile = await API.__post(
                "PATCH",
                "api/users/" + uid,
                data,
                dispatch,
                SET_NOTIFICATION
            );

            if (userProfile) {
                dispatch({
                    type: SET_NOTIFICATION,
                    notification: {
                        type: 'success',
                        view: 'notice',
                        message: 'Password was updated!'
                    }
                });
            }
        })();
    };
}
export function getTelegramRegistrationUrl() {
    return dispatch => {
        (async () => {

            let telegramRegistrationUrl = await API.__post('POST', 'api/telegram_registration/', {}, dispatch, SET_NOTIFICATION);

            if (telegramRegistrationUrl) {
                dispatch({
                    type: types.TELERGAM_BOT_URL_LOADED_SUCCESS,
                    telegramRegistrationUrl: telegramRegistrationUrl
                });
            }
        })();
    };
}

export function isTelegramVerified() {
    return dispatch => {
        (async () => {

            let telegramVerification = await API.__get('user_telegram/', dispatch, SET_NOTIFICATION);

            if (telegramVerification) {

                dispatch({
                    type: types.TELERGAM_VERIFICATION_LOADED,
                    telegramVerification: telegramVerification
                })
            }

        })();

    }
}


export function verificateTelegramToken(token, uid) {
    return dispatch => {
        (async () => {

            let telegramVerification = await API.__post('POST', 'api/telegram_verification', {
                token: token
            }, dispatch, SET_NOTIFICATION);

            if (telegramVerification) {

                dispatch({
                    type: types.TELERGAM_VERIFICATION_LOADED,
                    telegramVerification: telegramVerification
                })
            }

        })();

    }
}

export function getBalance(companyId, filters = '') {

    return (dispatch, getState) => {

        (async () => {
            const { profile: {companyProfile: {filters: stateFilters = ''}} } = getState(),
            flt = filters === true ? stateFilters : filters ? filters : '',
            stringFilters = flt ? '?' + filters : '';
            let balance = await API.__get('companies_balance/'+ companyId + stringFilters, dispatch, SET_NOTIFICATION)

            if(balance)
            {
                dispatch({ 
                    type: types.COMPANY_BALANCE_LOADED_SUCCESS,
                    balance: balance,
                    filters: flt
                })
            }
        })();

    }
}

export function saveRating(value){

    return dispatch => {
        dispatch({
            type: types.SAVE_RATING,
            rating: value,
        })
    }

}

function _getStep(profile) {
    switch (profile.status) {
        case 'pending':
            return 1;
            break;
        default:
            return 0;
    }
}

export function updateHiringStatus(data) {
    return (dispatch, getState) => {
        (async () => {

            let res = await API.__post('PATCH', 'api/hiring_contacts/company/', data, dispatch, SET_NOTIFICATION);
            if (res) {
                getRecruiterHireHistory(true)(dispatch,getState);
            }
        })();

    }
}