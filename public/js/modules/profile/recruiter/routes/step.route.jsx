import React from "react";
import { Route, Switch } from "react-router-dom";

import Layout from "../components/layouts/layout.jsx";
import TopLayout from "../../../../components/includes/layout.jsx";
import Index from "../containers/steps/index.ctrl.jsx";
import Security from "../containers/profile/security.ctrl.jsx";

import { linksStepsSchema } from "../schema/linksSteps.sc.js";
import Rating from "../containers/profile/rating.ctrl.jsx";
import Statistics from "../containers/profile/statistics.ctrl.jsx";

const routes = [
    {
        path: "/steps/:step?",
        component: Index
    },
    {
        path: "/security",
        component: Security
    },
    {
        path: "/rating",
        component: Rating
    },
    {
        path: "/statistics",
        component: Statistics
    },
    {
        path: "*",
        component: Index
    }
];

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate(){
        window.scrollTo(0,0);
    }
    render() {
        return (
            <TopLayout>
                <Switch>
                    {routes.map((route, i) => (
                        <Route
                            key={i}
                            path={route.path}
                            exact
                            component={route.component}
                        />
                    ))}
                </Switch>
            </TopLayout>
        );
    }
}
