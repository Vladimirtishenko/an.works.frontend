import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import Info from "../containers/profile/info.ctrl.jsx";
import Main from "../containers/profile/main.ctrl.jsx";
import Rating from "../containers/profile/rating.ctrl.jsx";
import Security from "../containers/profile/security.ctrl.jsx";
import Statistics from "../containers/profile/statistics.ctrl.jsx";
import Error from "../../../../components/error/404.jsx";
import CV from "../containers/profile/cv.ctrl.jsx";
import ContactUser from "../../../../components/contactUser/ContactUser.jsx";

const routes = [
    // {
    //     path: "/",
    //     component: Main
    // },
    {
        path: "/",
        component: Info
    },
    {
        path: "/rating",
        component: Rating
    },
    {
        path: "/statistics",
        component: Statistics
    },
    {
        path: "/security",
        component: Security
    },
    {
        path: "/cv/:profileId",
        component: CV
    },
    {
        path: "/contact",
        component: ContactUser
    },
    {
        path: "*",
        component: Error
    }
];

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate(){
        window.scrollTo(0,0);
    }
    render() {
        return (
            <Switch>
                {routes.map((route, i) => ( 
                    <Route
                        key={i}
                        path={route.path}
                        exact
                        component={route.component}
                    />
                ))}
            </Switch>
        );
    }
}
