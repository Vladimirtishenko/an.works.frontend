
export const linksProfileSchema = (translation = {}) => {

    return [
        {
            name: translation.personalInfo,
            path: '/' 
        },
        {
            name: translation.statistics,
            path: '/statistics'
        },
        {
            name: translation.ranking,
            path: '/rating'
        },
        {
            separate: true,
            name: translation.security,
            path: '/security'
        }
    ]

}
