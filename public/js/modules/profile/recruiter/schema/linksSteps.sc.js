export const linksStepsSchema = (translation = {}) => {

    return [{
            step: 1,
            name: translation.personalInfo,
            path: '/steps/1'
        },
        {
            name: translation.statistics,
            hide: true,
        },
        {
            name: translation.ranking,
            hide: true,
        },
        {
            separate: true,
            name: translation.security,
            path: '/security'
        }
    ]

}