import * as types from '../constants/profile.const.js';

const initialState = {};

export default function companyEmployeeProfile(state = initialState, action) {

    switch (action.type) {

        case types.COMPANY_EMPLOYEE_LOADED_SUCCESS:
            return {
                ...state,
                ...action.profile
            };

        case types.COMPANY_EMPLOYEE_UPDATED_FAILED:
        case types.COMPANY_EMPLOYEE_LOADED_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        case types.COMPANY_EMPLOYEE_UPDATED_SUCCESS:
            return {
                ...state,
                ...action.profile,
                step: action.step
            };

        case types.SEARCHING_DATA_CHANGED:
            return {
                ...state,
                searching_data: action.searching_data
            };

        case types.CANDIDATS_QUEUE_LOAD_SUCCESS:
            return { 
                ...state,
                rankedQueue: action.rankedQueue
            };
        case types.CANDIDATS_QUEUE_LOAD_FAILED:
            return {
                ...state,
                rankedQueue: []
            };

        case types.APLICANT_LIST_LOAD_FAILED:
            return {
                ...state,
                notification: action.notification
            };

        case types.APLICANT_LIST_LOAD_SUCCESS:
            return {
                ...state,
                applicantsList: action.applicantsList,
                filters: action.filters
            }

        case types.UPDATE_STEP_DIRECTION_FOR_HR:
            return {
                ...state,
                direction: action.direction
            }
        case types.TELERGAM_VERIFICATION_LOADED:
            return {
                ...state,
                telegramVerification: action.telegramVerification,
                telegramVerificationFailed: false
            }
        case types.TELERGAM_VERIFICATION_FAILED:
            return {
                ...state,
                telegramVerificationFailed: action.notification
            }
        case types.TELERGAM_BOT_URL_LOADED_SUCCESS:
            return {
                ...state,
                telegramRegistrationUrl: action.telegramRegistrationUrl
            };
        case types.KNOWLEDGES_LOADED_FOR_HR_SUCCESS:
            return {
                ...state,
                knowledges: action.knowledges
            };
        case types.STATISTIC_VIEW_LOADED_SUCCESS:
            return {
                ...state,
                statisticData: action.statisticData
            };
        case types.CONTACT_PROFILE_APPLICANT:
            return {
                ...state,
                cvContact: action.applicantInfo
                }
        case types.COMPANY_BALANCE_LOADED_SUCCESS:
            return {
                ...state,
                balance: action.balance 
                }
        case types.SAVE_RATING:
            return{
                ...state,
                ratings : action.rating
            }
        default:
            return state
    }
}