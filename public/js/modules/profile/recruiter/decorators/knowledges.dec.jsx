import React from 'react'

const KnowledgesDictionaryHOC = (WrapedComponent) => {

  class KnowledgesDictionary extends React.Component {
      constructor(props){
          super(props);
          const {
                companyProfile: { 
                    knowledges = [] 
                }
            } = this.props;
          this.state = {
              knowledges: knowledges
          }
      }

      componentDidMount(){
          const { actions: { getWorkSkillsDictionary }, companyProfile: { knowledges = [] } } = this.props;

          if(!knowledges.length){
                getWorkSkillsDictionary();
          }

      }

      componentDidUpdate(prevProps, prevState){

          const { companyProfile: { knowledges = [] } } = this.props;

          if(knowledges.length && !prevState.knowledges){
              this.setState({
                  knowledges: true
              })
          }

      }

      render(){

          const { knowledges } = this.state;

          if(knowledges){
              return <WrapedComponent {...this.props} />
          }

          return null;

      }
  }

  return KnowledgesDictionary;

}

export default KnowledgesDictionaryHOC;
