import { connect } from 'react-redux'
import React from 'react'

const CompanyProgress = (WrapedComponent) => {

  class ProfileProgressComponent extends React.Component {

    constructor(props, context){
        super(props, context);
    }

    componentDidMount(){


        let {step} = this.props.match && this.props.match.params,
            edgeStep = 1,
            routesStep = step ? step : 1,
            direction = routesStep > edgeStep ? edgeStep : routesStep;

        this.props.actions.updateStep && this.props.actions.updateStep(direction);

    }

    shouldComponentUpdate(nextProps, nextState){

        let directionPrev = this.props.recruterProfile && this.props.recruterProfile.direction || null,
            directionNext = nextProps.recruterProfile && nextProps.recruterProfile.direction || null,
            edgeStep = nextProps.recruterProfile && nextProps.recruterProfile.step,
            {step} = this.props.match && this.props.match.params;


        if(edgeStep == 0) {
            this.props.history.push('/');
            return false;
        }

        if(directionNext != directionPrev){
            this.props.history.push(`/steps/${directionNext}`);
            return false;
        }

        if( !step || (step > edgeStep) ){
            this.props.history.push(`/steps/${edgeStep}`);
            return false;
        }

        if(step != directionNext){
            this.props.actions.updateStep && this.props.actions.updateStep(step);
            return false;
        }


        return true;

    }

    render() {

        let direction = this.props.recruterProfile && this.props.recruterProfile.direction || null;

        return direction ? <WrapedComponent {...this.props} /> : null;

    }

  }

  return ProfileProgressComponent;

}
export default CompanyProgress;
