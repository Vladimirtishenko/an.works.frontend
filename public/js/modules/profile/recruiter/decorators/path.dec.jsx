import { connect } from 'react-redux'
import React from 'react'

const companyEmployeePathComponent = (WrapedComponent) => {

  class PathComponent extends React.Component {

    constructor(props, context){
        super(props);

        this.state = {
            router: null
        }
    }

    componentDidMount(){

        let {uid, companyId} = this.props.oauth.user || {}
        this.props.actions.getEmployeeProfile(uid, companyId);

    }

    componentDidUpdate(prevProps, prevState){

        let {step} = this.props.recruterProfile || {},
            {step: previousStep} = prevProps.recruterProfile || {};

        if(step != previousStep){

            let path = step > 0 ? 'step' : 'page';

            this.setState({
                router: path
            })
        }

    }

    render() {

        let {router} = this.state;

        return router && <WrapedComponent {...this.props} router={router} /> || null;

    }

  }

  return PathComponent;

}
export default companyEmployeePathComponent;
