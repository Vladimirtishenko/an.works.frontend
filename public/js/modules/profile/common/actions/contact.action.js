import {CONTACT_PROFILE_APPLICANT}from '../constants/contact.const.js';
import { SET_NOTIFICATION }  from '../../../../libraries/notification/constant/notification.const.js';
import API from "../../../../services/api.js";

export function getContactProfileCv(profileId) {
    return dispatch => {
        (async () => {
            let applicantInfo = await API.__get('view/applicant/cv/' + profileId, dispatch, SET_NOTIFICATION);

            if(applicantInfo){
                dispatch({
                    type: CONTACT_PROFILE_APPLICANT,
                    applicantInfo: applicantInfo
                })
            }
        })();
    }
}
export function deletContactProfileCv() {
    return dispatch => {
        dispatch({
            type: CONTACT_PROFILE_APPLICANT,
            applicantInfo: {}
        })
    }
}