import {
    HINT_RIGHT_ASIDE_RATING_LAYOUTS,
    HINT_RIGHT_PARTICIPATE_RATING_LAYOUTS
} from '../constants/hint.const.js'; 

export function updateHintLayoutRating(status) {
    return dispatch => {
        let storageHint = JSON.parse(localStorage.getItem('hint'));
        localStorage.setItem('hint',JSON.stringify({...storageHint, layoutRating: true}));
        dispatch({
            type: HINT_RIGHT_ASIDE_RATING_LAYOUTS,
            hintProfileRating: status
        })
    }
}
export function updateHintParticipateRating(status) {
    return dispatch => {
        let storageHint = JSON.parse(localStorage.getItem('hint'));
        localStorage.setItem('hint',JSON.stringify({...storageHint, layoutParticipateRating: true}));
        dispatch({
            type: HINT_RIGHT_PARTICIPATE_RATING_LAYOUTS,
            hintParticipateRating: status
        })
    }
}