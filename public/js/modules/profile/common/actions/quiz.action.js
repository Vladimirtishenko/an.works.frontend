import * as types from "../constants/quiz.const.js";
import { SET_NOTIFICATION }  from '../../../../libraries/notification/constant/notification.const.js'
import API from "../../../../services/api.js";

import hosts from '../../../../configuration/quiz.json'

// Test method, should be deleted
export function getTestsFromService(filters) {
    return dispatch => {
        (async () => {
            const stringFlt = filters && ('?' + filters) || '',
                  { backend } = hosts,
                   quiz = await API.__quiz_get(
                        `${backend}/ru/quiz${filters ? `?${filters}` : ''}`,
                        dispatch,
                        SET_NOTIFICATION
                    ),
                    { items = [] } = quiz,
                    values = items.map(function (obj) { return obj._id });
        })()
    }
}

export function getSkillTestHierarchy(mainSkill, filters) {
    return dispatch => {
        (async () => {
            const stringFlt = filters && ('?' + filters) || '',
                  { backend } = hosts,
                  skillTestsHierarchy = await API.__get(
                        "dictionary/skillsTestsHierarchy/" + mainSkill + stringFlt,
                        dispatch,
                        SET_NOTIFICATION
                    ),
                    { tests = [] } = skillTestsHierarchy,
                    values = tests.map(function (obj) { return obj.testLabel }),
                    quiz = await API.__quiz_post(
                        'POST',
                        `${backend}/ru/quiz/search`,
                        {id: values},
                        dispatch,
                        SET_NOTIFICATION
                    ),
                    merged = _.map(tests, function(obj) {
                        return _.assign(obj, _.find(quiz, {
                            _id: obj.testLabel
                        }));
                    });

                    skillTestsHierarchy.tests = merged;

            if (skillTestsHierarchy) {
                dispatch({
                    type: types.SKILL_TESTS_HIERARCHY,
                    skillTestsHierarchy: skillTestsHierarchy
                });
            }
        })();
    };
}

export function getProfileResults(profileId) {
    return dispatch => {
        (async () => {
            let testsResults = await API.__get(
                "applicant_tests_results/" + profileId,
                dispatch,
                SET_NOTIFICATION
            );
            if (testsResults) {
                dispatch({
                    type: types.TESTS_RESULTS_LOADED,
                    testsResults: testsResults
                });
            }
        })();
    };
}

export function createSkillTestHierarchy(data) {
    return dispatch => {
        (async () => {
            let result = await API.__post(
                "POST",
                "api/applicant_tests_results/",
                data,
                dispatch,
                SET_NOTIFICATION
            );

            if (result) {
                getProfileResults(data.profileId)(dispatch);
            }
        })();
    };
}
