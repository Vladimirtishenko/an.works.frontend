import { BURGER_MENU_STATUS_UPDATED, COLLAPSED_MENU , REGISTRAION_MODAL_OPEN, REGISTRAION_MODAL_CLOSE} from '../constants/activity.const.js'; 

export function updateBurgerMenuStatus(status) {
    return dispatch => {
        dispatch({
            type: BURGER_MENU_STATUS_UPDATED,
            isBurgerMenuOpen: status
        })
    }
}

export function collapsedMenu(status) {
    return dispatch => {
        dispatch({
            type: COLLAPSED_MENU,
            collapsed: status
        })
    }
}

export function registrationModalOpen(){
    return dispatch => {
        dispatch({
            type: REGISTRAION_MODAL_OPEN,
            registrationModal: true
        })
    }
}

export function registrationModalClose(){
    return dispatch => {
        dispatch({
            type: REGISTRAION_MODAL_CLOSE,
            registrationModal: false
        })
    }
}