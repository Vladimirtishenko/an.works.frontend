import React from 'react'
import PopUp from './pop-up.jsx'
import {Link} from 'react-router-dom';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class InsufficientFunds extends React.Component {

    render() {
        const {closePopup,recruiter, i18n:{translation = {}} = {}} = this.props;
        return (
            <PopUp
                title={translation.dontHaveToken}
                poUpMainClass="padding--t-25 padding--b-25 "
                wrapperClass="pop-up--max-530"
                closePopup={() => closePopup()}
            >
                <div className="padding--xc-20 padding--md-xc-30 font--color-primary font--14 font--center">
                   <p>{translation.dontHaveTokenPopUp}</p>
                   {
                       !recruiter && (
                        <p>{translation.youCanBuyTokens}
                            <Link to={'/payments'} className="font--color-blue pointer--text font--underlined margin--l-5">
                                {translation.payment}
                            </Link>
                        </p>
                       ) || null
                   }
                    
                </div>
            </PopUp>
        )
    }
}

export default InsufficientFunds;