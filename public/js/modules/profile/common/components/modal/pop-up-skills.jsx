import React from 'react'
import { string } from 'prop-types'

class PopUpSkills extends React.Component { 

    static propTypes = {
        headerClass: string,
        wrapperSearch: string
    }

    static defaultProps = {
        headerClass: '',
        wrapperSearch: ''
    }

    constructor(props){
        super(props);

        this.state = {
            ...PopUpSkills.defaultProps,
            ...this.props
        };
        
        

    }

    render(){
        
        return (
           <React.Fragment>
                <div className={`pop-up ${this.state.wrapperClass}`}>
                    {/* <input type="search"/> */}
                    <div className={this.state.headerClass}>
                        <div className={this.state.wrapperSearch}>
                            <input type="search" className=""/>
                        </div>
                        <div className="fl fl--align-c col-md-4 fl--wrap">
                            <span className="nowrap--all font--20">
                                Рейтинг по технологии{" "}
                            </span>{" "}
                        </div>
                    </div>
                </div>
            </React.Fragment>


        )
    }
}

export default PopUpSkills;