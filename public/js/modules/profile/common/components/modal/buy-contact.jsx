import React from 'react'
import PopUp from './pop-up.jsx'
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class BuyContact extends React.Component {
    render() {
        const {
            i18n:{translation = {}} = {},
            closePopup,
            buyAction,
            contactInfo:{
                position = '',
                price = '',
                profileId = '',
                rank = ''
            }} = this.props;

        return (
            <PopUp
                title={translation.buyContacts}
                poUpMainClass="padding--yc-20 padding--xc-15 padding--md-xc-45 padding--md-yc-30 font--center"
                closePopup={() => closePopup()}
            >
                <p className="font--14 font--color-secondary margin--b-20 ">
                    {translation.questionOpenContact}
                </p>
                    <p className="margin--b-20">
                        <span className="font--16 font--color-primary display--b font--capitalized">{position}</span>
                        <span className="font--12 font--color-inactive">{profileId}</span>
                    </p>
                    <div className="fl fl--justify-b margin--b-20">
                        <span className="font--14 font--color-secondary">{translation.rankingPosition}</span>
                        <span className="font--16 font--500 font-color-primary">{rank}</span>
                    </div>
                    <div className="fl fl--justify-b margin--b-30">
                        <span className="font--14 font--color-secondary">{translation.openingCost}</span>
                        <span className="font--16 font--500 font-color-primary">{price}</span>
                    </div>
                    <div className="fl fl--justify-c">
                        <button className="font--uppercase margin--xc-10 btn btn--theme-blue font--10 width--px-100 height--px-20" 
                            onClick={
                                () => {
                                    buyAction(profileId, false)}
                            }
                            >{translation.yes}</button>
                    </div>
            </PopUp>
        )
    }
}

export default BuyContact;
