import React from 'react'
import PopUp from './pop-up.jsx'
import { connect } from "react-redux";
import * as i18n from '../../../../i18n/actions/i18n.action.js';
import links from '../../../../../configuration/registration.json';

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class BuyContact extends React.Component {
    render() {
        const {
            i18n:{translation = {}} = {},
            registrationModalClose
        }= this.props;

        return (
            <PopUp
                title={translation.registration}
                poUpMainClass="padding--yc-20 padding--xc-15 padding--md-xc-45 padding--md-yc-30 font--center"
                closePopup={() => registrationModalClose()}
            >
                <p className="font--14 font--color-secondary margin--b-20 ">
                    {translation.chooseWhoYou}
                </p>
                <div>
                    <ul className="list fl fl--justify-c fl--align-c fl--wrap">
                        <li className="list__item list__item--theme-btn-blue width--100 width--sm-px-160 height--px-40 margin--sm-r-30 margin--b-15 margin--sm-b-0">
                            <a className="fl fl--justify-c fl--align-c font--16 font--color-blue width--100 height--100" href={links.applicant} target="_blank">{translation.iSeekWork}</a>
                        </li>
                        <li className="list__item list__item--theme-btn-blue width--100 width--sm-px-160 height--px-40">
                            <a className="fl fl--justify-c fl--align-c font--16 font--color-blue height--100 width--100" href={links.company} target="_blank">{translation.employer}</a>
                        </li>
                    </ul>
                </div>
            </PopUp>
        )
    }
}

export default BuyContact;
