import React from 'react'
import PopUp from './pop-up.jsx'
import ComboboxComponent from '../../../../../components/form/combobox.jsx';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class Price extends React.Component {
    checkPosition(top){
        switch(top){
            case 10:
            return 'first';
            case 20:
            return 'second';
            case 30:
            return 'third';
            case 40:
            return 'fourth';
            case 50:
            return 'fifth';
            default:
            return null;
        }
    }
    render() {
        const {i18n:{ translation = {} } = {}, closePopup, priceConfiguration = [] } = this.props;

        return (
            <PopUp
                title={translation.price}
                poUpMainClass="padding--t-25 "
                wrapperClass="pop-up--max-530"
                closePopup={() => closePopup()}
            >
            <div className="fl fl--wrap fl--align-c fl--justify-c margin--b-25">
                <span className="font--14 font--color-secondary font--center margin--b-10 margin--md-0">
                    {translation.openContactApplicant}
                </span>
                {/* <ComboboxComponent
                    name="skill name"
                    wrapperClass="no-gutters text-combobox  text-combobox--center-mobile text-combobox--font-18 min-width--20-px padding--l-5"
                    list={[
                        { label: "JavaScript", name: "JavaScript" },
                        { label: "Java", name: "Java" },
                        { label: "Python", name: "Python" },
                        { label: "PHP", name: "PHP" }
                    ]}
                    value="JavaScript"
                /> */}
            </div>
            <div
                className="inadaptive-table__tr inadaptive-table__tr--bottom inadaptive-table--padding-5 border--t-1-gray-block shadow--box margin--b-10 inadaptive-table--height-70 bg--white font--center font--14"
            > 
                <div
                        data-title="Топ"
                        className="inadaptive-table__td inadaptive-table__td--md_120 inadaptive-table__td_55 fl--align-c "
                    >
                    <div className="fl fl--dir-col fl--align-c fl--justify-c font--color-white font--500">
                        <span className="font--12 font--color-secondary show--md">{translation.rankingPosition}</span>
                        <span className="font--12 font--color-secondary hide--md">{translation.ranking}</span>
                    </div>
                </div>
                <div className="fl fl--wrap width--100 height--100">
                    <div className="width--100 font--center font--12 font--color-secondary  border--b-1-gray-block fl fl--align-c fl--justify-c height--px-35">
                        {translation.opensNnumber}
                    </div>
                    <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-secondary font--14">
                        1
                    </div>
                    <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-secondary font--14">
                        2
                    </div>
                    <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-secondary font--14">
                        3
                    </div>
                    <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-secondary font--14">
                        4
                    </div>
                    <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-secondary font--14">
                        5
                    </div>
                </div>
            </div>
                {
                    priceConfiguration.length && priceConfiguration.map((item, i) =>{
                        return (
                            <div
                                key={i + Math.random()} className="inadaptive-table__tr inadaptive-table__tr--bottom-t-0 inadaptive-table--padding-5  inadaptive-table--height-45 bg--white font--center font--14"
                            > 
                                <div
                                        data-title={translation.top}
                                        className={`inadaptive-table__td inadaptive-table__rate-${this.checkPosition(item.to)} inadaptive-table__td--md_120 inadaptive-table__td_55`}
                                    >
                                    <div className="fl fl--dir-col fl--align-c fl--justify-c height--100 font--color-white font--500">
                                        <span className="font--10 font--500 font--uppercase">{translation.top}</span>
                                        <span className="font--18 font--500">{item.to}</span>
                                    </div>
                                </div>
                                <div className="fl width--100">
                                { item.openCountPrices.length && item.openCountPrices.map((item, i) =>{
                                    return (
                                        <div key={i + Math.random()} className="inadaptive-table__td inadaptive-table__td_20-p font--color-primary font--500 font--14">
                                        {item}
                                        </div>
                                    )
                                })}{
                                    
                                }
                                </div>
                            </div>
                        )
                    })
                }
                <div
                    className="inadaptive-table__tr inadaptive-table__tr--bottom-t-0 inadaptive-table--padding-5 shadow--box inadaptive-table--height-45 bg--white font--center font--14"
                > 
                    <div
                            data-title="Топ"
                            className="inadaptive-table__td inadaptive-table__rate-no-top inadaptive-table__td--md_120 inadaptive-table__td_55"
                        >
                        <div className="fl fl--align-c fl--justify-c fl--dir-col height--100 font--color-white font--500">
                            <span className="font--16 margin--md-r-5">{translation.not}</span>
                            <span className="font--16 font--uppercase">{translation.top}</span>
                        </div>
                    </div>
                    <div className="fl width--100">
                        <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-primary font--500 font--14">
                            1
                        </div>
                        <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-primary font--500 font--14">
                            1
                        </div>
                        <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-primary font--500 font--14">
                            1
                        </div>
                        <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-primary font--500 font--14">
                            1
                        </div>
                        <div className="inadaptive-table__td inadaptive-table__td_20-p font--color-primary font--500 font--14">
                            1
                        </div>
                    </div>
                </div>
            </PopUp>
        )
    }
}

export default Price;