import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class PopUp extends React.Component {

    constructor(props){
        super(props);
        this.__refs = null;
        this.handleClickOutside = ::this.handleClickOutside;
    }

	componentDidMount() {
        document.addEventListener('click', this.handleClickOutside);
        document.addEventListener('touchstart', this.handleClickOutside);
	}

	componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside);
        document.removeEventListener('touchstart',this.handleClickOutside)
    }
    handleClickOutside(event) {
        const {
            closePopup = ()=>{}
        } = this.props;
		if (this.__refs && !this.__refs.contains(event.target)) {
			closePopup();
		}
	}
    render() {
        const {
            closePopup = ()=>{},
            i18n:{translation = {}} = {},
            poUpMainClass = '',
            title = '',
            wrapperClass = '',
            icon = '',
            classHeader = '',
            cardClass = '',
            titleClass = '',
            footerWrapper = '',
            footer = ''
        } = this.props;
        return (
            <div className="modal fl fl--justify-c fl--align-c">
                <div className={`pop-up ${wrapperClass}`}  ref={(refs) => { this.__refs = refs; }}>
                    <div className={cardClass}>
                        <div className={`pop-up__head ${classHeader}`}>
                            {icon && (
                                <span
                                    className={`icon icon--main-color icon--${
                                        icon
                                    }`}
                                />
                            )}
                            <span className={`pop-up__title ${titleClass}`}>
                                {title}
                            </span>
                        </div>
                    </div>
                    <div className={poUpMainClass}>
                        {this.props.children}
                    </div>
                    <div className={`pop-up__footer ${footerWrapper}`}>
                        {footer || (
                            <span
                                className="font--14 font--color-secondary font--underlined pointer l-h--1"
                                onClick={closePopup}
                            >
                                {translation.close}
                            </span>
                        )}
                    </div>
                </div>
            </div>
        )
    }
   
};

export default PopUp;
