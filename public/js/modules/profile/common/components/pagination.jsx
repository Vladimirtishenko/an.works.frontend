import React from "react";

import {
    normalizeOffset,
    normalizeOffsetWithPageLimit,
    mergeOffsetWithFilters,
    denormalizePages
} from '../../../../helpers/filters/offset.helper.js'

class Pagination extends React.Component {
    renderInnerPagination(pages, selectedPage) {
        if (pages === 1) {
            return (
                <div className="pagination__item pagination__item_selected">
                    1
                </div>
            );
        }

        if (pages > 1 && pages <= 3) {
            const pag = [];
            for (let i = 1; i <= pages; i++) {
                pag.push(
                    <div
                        key={i}
                        className={`pagination__item ${
                            i === selectedPage
                                ? "pagination__item_selected"
                                : ""
                        }`}
                        onClick={() => this.handleChange(i)}
                    >
                        {i}
                    </div>
                );
            }

            return pag;
        }

        if (pages > 3) {
            return (
                <React.Fragment>
                    <div
                        className={`pagination__item ${
                            selectedPage === 1
                                ? "pagination__item_selected"
                                : ""
                        }`}
                        onClick={() => this.handleChange(1)}
                    >
                        1
                    </div>

                    {selectedPage - 1 > 1 && (
                        <React.Fragment>
                            <div
                                className={`pagination__item pagination__item_dots`}
                            >
                                ...
                            </div>
                            <div
                                className={`pagination__item`}
                                onClick={() =>
                                    this.handleChange(selectedPage - 1)
                                }
                            >
                                {selectedPage - 1}
                            </div>
                        </React.Fragment>
                    )}
                    {selectedPage !== 1 && selectedPage !== pages && (
                        <div
                            className={`pagination__item pagination__item_selected`}
                        >
                            {selectedPage}
                        </div>
                    )}
                    {selectedPage + 1 < pages && (
                        <React.Fragment>
                            <div
                                className={`pagination__item`}
                                onClick={() =>
                                    this.handleChange(selectedPage + 1)
                                }
                            >
                                {selectedPage + 1}
                            </div>
                            <div
                                className={`pagination__item pagination__item_dots`}
                            >
                                ...
                            </div>
                        </React.Fragment>
                    )}

                    <div
                        className={`pagination__item ${
                            selectedPage === pages
                                ? "pagination__item_selected"
                                : ""
                        }`}
                        onClick={() => this.handleChange(pages)}
                    >
                        {pages}
                    </div>
                </React.Fragment>
            );
        }
    }

    handleChange(pageNumber) {
        const {
            pages,
            actions,
            filters
        } = this.props,
        normalizedFiltersByOffset = normalizeOffsetWithPageLimit(filters, pageNumber - 1),
        mergedFilters = mergeOffsetWithFilters(filters, normalizedFiltersByOffset);

        if (pageNumber > pages || pageNumber < 1) {
            return null;
        }

        actions(mergedFilters)

    }

    render() {
        const {
            count,
            wrapperClass,
            filters
        } = this.props,
        selectedPage = normalizeOffset(filters) || 1,
        pageDenormalize = denormalizePages(filters, count);

        return (
            <div className={`${wrapperClass}`}>
                <div
                    className={`pagination__item ${
                        selectedPage > 1 ? "" : "pagination__item_disabled"
                    }`}
                    onClick={
                        selectedPage > 1
                            ? () => this.handleChange(selectedPage - 1)
                            : () => null
                    }
                >
                    <span className={`icon icon--Rating_arrow font--11`} />
                </div>
                {pageDenormalize && ::this.renderInnerPagination(pageDenormalize, selectedPage)}
                <div
                    className={`pagination__item ${
                        selectedPage < pageDenormalize ? "" : "pagination__item_disabled"
                    }`}
                    onClick={
                        selectedPage < pageDenormalize
                            ? () => this.handleChange(selectedPage + 1)
                            : () => null
                    }
                >
                    <span
                        className={`icon icon--Rating_arrow font--11 rotate--180`}
                    />
                </div>
            </div>
        );
    }
}

export default Pagination;
