import React from 'react'
import genderArray from '../../../../../../databases/general/gender.json'
import countries from '../../../../../../databases/general/countries.json'
import config from '../../../../../../configuration/rules.json';
import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RatingDescription extends React.PureComponent {

    constructor(props){
        super(props);

        this.state = {
            city: "",
            country: ""
        }

    }

    async locationMaker(){

        let {geography: {city, country}} = this.props,
            countryIndex = _.findIndex(countries, ['label', country]),
            countryObject = countries[countryIndex] || {};

        if(country && city){
            const db = await (
              await import(`../../../../../../databases/countries/${country}/cities.${country}.json`)
            ).default;


            let cityIndex = _.findIndex(db, ['label', city]),
                cityObject = db[cityIndex] || {};


            this.setState({
                city: cityObject.name || "",
                country: countryObject.name || ""
            })

        }

    }
    componentDidUpdate(prevProps, prevState){
        let comparing = _.isEqual(prevProps, this.props);
            this.locationMaker();
            if (!comparing) {
                this.setState({ ...this.state, ...this.props });
            }
        }

    componentDidMount(){
        this.locationMaker();
    }

    genderMaker(gender){

        let index = _.findIndex(genderArray, ['value', gender]),
            object = index > -1 ? genderArray[index] : {};

        return object.sub;

    }

    render(){

        let {yersOld:{from , to}, gender, wishedSalary,i18n:{translation = {}} = {} } = this.props,
            {city, country} = this.state,
            genderOfPersone = this.genderMaker(gender),
            age = from == config.maxAge ? from+'+' :from +" - " +to;
            
        return (
            <div className="fl fl--justify-b padding--20 font--18 font--bold">
                <span>{`${translation.desiredSalary} $ ${wishedSalary}`}</span>
                <div>
                    <span className="d-inline_block margin--r-15">{`${
                        genderOfPersone
                    }, ${translation.ageMan} ${age},`}</span>
                    <span className="d-inline_block">{` ${country}, ${
                        city
                    }`}</span>
                </div>
            </div>
        )
    }
}
