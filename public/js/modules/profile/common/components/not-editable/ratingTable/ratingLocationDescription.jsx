import React from "react";

import {
    findCities,
    normalizeCountryAndCities,
    mergeCountryAndCities,
    uploadDiferentCountries
} from "../../../../../../helpers/locationMapper.helper.js";
import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RatingLocationDescription extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            places: []
        };
    }

    componentDidMount() {
        this.getRelocationCities();
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.getRelocationCities();
        }
    }

    async getRelocationCities() {
        let {
            geography: {
                country,
                relocation: {
                    local,
                    abroad: { places }
                }
            }
        } = this.props; 

        if (local.type && local.cities) {
            let localNormalize = normalizeCountryAndCities(
                    country,
                    local.cities
                ),
                cityArrayNormalazed = await findCities(
                    localNormalize.cities,
                    country
                ),
                mergeForLocalView = mergeCountryAndCities(
                    localNormalize.country,
                    cityArrayNormalazed,
                    "string"
                ),
                mergeForAbroadView = await uploadDiferentCountries(
                    places,
                    "string"
                ),
                mergeLocation = [mergeForLocalView, ...mergeForAbroadView];

            this.setState({
                places: mergeLocation
            });
        }else {
            this.setState({
                places: []
            });
        }
    }

    render() {
        let { places } = this.state,
            {i18n:{translation = {}} = {}} = this.props;

        return (
            <React.Fragment>
            {
                places.length && (
                    <div className="row padding--xc-15 padding--yc-5">
                        <div className="col-md-2 font--color-secondary">
                            {translation.readyRelocate}:
                        </div>
                        <div className="col-md-10 font--left l-h--1-7 font--500">
                            {places &&
                                places.map(item => (
                                    <p
                                        className="padding--xc-5 padding--yc-2 margin--l-5 font--highlight-grey display--b-l"
                                        key={item._id}
                                    >
                                        {item.name}
                                    </p>
                                ))}
                        </div>
                    </div>) || null
            }
            </React.Fragment>
            
            
        );
    }
}
