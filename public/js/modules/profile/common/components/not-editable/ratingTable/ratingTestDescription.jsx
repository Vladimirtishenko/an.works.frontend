import React from "react";
import uniqid from 'uniqid';

import { ProgressCircle } from "../../../../../../components/widgets/progress/index.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RatingTestDescription extends React.PureComponent {
    constructor(props) {
        super(props);
        this.skillsScrollRef = React.createRef();
    }

    handleScroll(e, direction) {
        e.preventDefault();
        e.stopPropagation();

        const width = 567;
        const skillsNode = this.skillsScrollRef.current;
        const scrollWidth = skillsNode.scrollWidth;

        if (scrollWidth < width) {
            return;
        }

        if (direction === "right") {
            skillsNode.scrollLeft = skillsNode.scrollLeft + 70;
        }

        if (direction === "left") {
            skillsNode.scrollLeft = skillsNode.scrollLeft - 70;
        }
    }

    setNameToResultArray(){

        let { testsResults, skillTestsHierarchy} = this.props;

        const data = [];

        if(!testsResults || !skillTestsHierarchy) return [];

        _.forEach(testsResults, (item) => {
            const find = _.find( skillTestsHierarchy.tests, {id: item.testUid} );

            if(find){
                let obj = {
                    ...item,
                    name: find.title
                }

                data.push(obj);
            }

        })
        
        data.sort((a,b)=>{ return a.level - b.level})
        return data;
    }

    render() {
        const normalized = this.setNameToResultArray(),
        {i18n:{translation = {}} = {}} = this.props;

        return (
            <div className="fl fl--dir-col padding--t-20 box--light-grey font--14">
                <h4 className="margin--b-15 font--center">
                    {translation.testResults}
                </h4>
                <div className="container-fluid padding--r-0">
                    <div className="row width--100">
                        <div className="col-2 fl fl--dir-col fl--justify-b font--left font--color-secondary padding--b-20 hr--right-bold padding--l-60">
                            <span className="fl fl--align-c height--px-70 padding--t-25 padding--l-15">
                                {translation.score}
                            </span>
                            <span className="height--px-30 padding--t-5  padding--l-15">
                                {translation.skill}
                            </span>
                        </div>
                        <div className="col-10 fl fl--justify-a width--100  padding--r-0">
                            <div
                                className="fl fl--align-c height--100 padding--xc-10  font--20 font--bold pointer"
                                onClick={e => this.handleScroll(e, "left")}
                            >
                                <span
                                    className={`icon icon--Rating_arrow font--12 padding--xc-5`}
                                />
                            </div>
                            <div
                                className="skillsScrollBlock"
                                ref={this.skillsScrollRef}
                            >
                                <div className="skillsScrollBlock__inner fl fl--justify-st fl--align-st width--100">
                                    {normalized.length &&
                                        normalized.map(item => (
                                            <div
                                                className="skillsScrollBlock__item fl fl--dir-col fl--justify-c fl--align-c"
                                                key={uniqid(item._id)}
                                            >
                                                <div className="height--px-70">
                                                    <ProgressCircle
                                                        value={item.result}
                                                    />
                                                </div>
                                                <div className=" font--color-secondary word-break--word">
                                                    {item.name}
                                                </div>
                                            </div>
                                        )) || null
                                    }
                                </div>
                            </div>
                            <div
                                className="fl fl--align-c height--100 padding--xc-10 padding--b-10 font--20 font--bold pointer"
                                onClick={e => this.handleScroll(e, "right")}
                            >
                                <span
                                    className={`icon icon--Rating_arrow d-inline_block font--12 padding--xc-5 rotate--180`}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
