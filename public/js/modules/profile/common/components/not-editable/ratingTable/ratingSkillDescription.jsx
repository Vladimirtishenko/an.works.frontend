import React from "react";
import uniqid from 'uniqid';
import { mapperSkillsLabel } from "../../../../../../helpers/mapperSkills.helper.js";
import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RatingSkillDescription extends React.PureComponent {

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }


    render() {
        let { skills, knowledges } = this.state,
            skillsMixed = mapperSkillsLabel(skills, knowledges, [{ isLang: false}]) || [],
            {i18n:{translation = {}} = {}} = this.props;
        return (
            <div className="row padding--xc-15 padding--yc-5">
                <div className="col-md-2 font--color-secondary">{translation.skills}:</div>
                <div className="col-md-10 font--500">
                    {skillsMixed &&
                        skillsMixed.map(item => (
                            <span
                                className="display--b-l padding--xc-5 margin--b-5 padding--yc-2 margin--l-5 font--highlight-grey"
                                key={uniqid(item._id)}
                            >
                                {item.name}
                            </span>
                        ))}
                </div>
            </div>
        );
    }
}
