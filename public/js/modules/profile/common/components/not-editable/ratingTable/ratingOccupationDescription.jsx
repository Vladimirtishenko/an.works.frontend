import React from "react";

import occupationVocabularies from "../../../../../../databases/general/occupation.json";
import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RatingOccupationDescription extends React.PureComponent { 
    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }
    getOccupation(occupation) {
        if (!occupation) return "";

        let occupationList = [];

        _.forEach(occupationVocabularies, (item, i) => { 
            if(occupation[item.label]) {
                occupationList.push(item);
            }   
        })
        return occupationList;
    }

    render() {
        let { occupation } = this.state,
            ocupationObject = this.getOccupation(occupation),
            {i18n:{translation = {}} = {}} = this.props;

        return (
            <React.Fragment>
                {
                    ocupationObject.length && (
                        <div className="row padding--xc-15 padding--yc-5">
                            <div className="col-md-2 font--color-secondary">
                                {translation.employmentType}
                            </div>
                            <div className="col-md-10 font--500">
                                {ocupationObject &&
                                    ocupationObject.map((item, i) => (
                                        <span
                                            className="padding--xc-5 padding--yc-2 margin--l-5 font--highlight-grey"
                                            key={i}
                                        >
                                            {item.sub}
                                        </span>
                                    ))}
                            </div>
                        </div>
                    ) || null
                }
            </React.Fragment>
            
        );
    }
}
