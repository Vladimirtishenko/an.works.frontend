import React from "react";
import { RaitingRow } from "./raitingRow.jsx";

import LoadAnim from "../../../../../../components/widgets/loadAnim.jsx";

import {
    orderBy,
    normalizeObderBy,
    orderingByClass
} from "../../../../../../helpers/filters/orderBy.helper.js";

export class RatingTable extends React.Component {
    sortingTable(event) {
        const { target: { dataset: { sort, value } } = {} } = event,
            {
                filters,
                actions: { getQueue }
            } = this.props,
            filtersNormalizedWithNewOrderBy = normalizeObderBy(
                filters,
                sort,
                value
            );

        getQueue(null, filtersNormalizedWithNewOrderBy);
    }

    render() {
        const {
            isCompany,
            applicantsList,
            companyId,
            oauth:{
                user:{
                    companyId: companyIdR
                } = {}
            } = {},
            handleChangePopupBuyContact,
            byContacts,
            queue: {
                queueLabel,
                items = [],
                profileOppenings = [],
                priceConfiguration = [],
                queueFetch
            } = {},
            queue,
            skillTestsHierarchy,
            knowledges,
            filters,
            actions,
            youId,
            i18n:{translation = {}} = {}
        } = this.props,
            positionColClass = isCompany
                ? "inadaptive-table__td_150"
                : "inadaptive-table__td_348 inadaptive-table__td--left-position",
            justOrderBy = orderBy(filters);
        return (
            <div className="inadaptive-table col--md-12">
                <div className="inadaptive-table__thead inadaptive-table__thead_sticky-rating box--white shadow--box">
                    <div className="inadaptive-table__tr inadaptive-table--padding-15 font--12 font--color-secondary font--center">
                        <div className="inadaptive-table__td inadaptive-table__td_40" />
                        <div className="fl  width--100">
                            <div className="inadaptive-table__td inadaptive-table__td_35 margin--l-5">
                                <span>#</span>
                            </div>
                            <div
                                className={`inadaptive-table__td ${positionColClass}`}
                            >
                                {translation.posititonID}
                            </div>
                            <div className="inadaptive-table__td inadaptive-table__td_98">
                                <span
                                    onClick={::this.sortingTable}
                                    data-sort="technology"
                                    data-value={justOrderBy.technology || ""}
                                    className={`inadaptive-table__sort ${orderingByClass(
                                        justOrderBy.technology
                                    )}`}
                                >
                                    {translation.score}
                                </span>
                            </div>
                            <div className="inadaptive-table__td inadaptive-table__td_100">
                                <span
                                    onClick={::this.sortingTable}
                                    data-sort="daysLeft"
                                    data-value={justOrderBy.daysLeft || ""}
                                    className={`inadaptive-table__sort ${orderingByClass(
                                        justOrderBy.daysLeft
                                    )}`}
                                >
                                    {translation.daysLeft}
                                </span>
                            </div>
                            <div className="inadaptive-table__td inadaptive-table__td_100">
                                <span
                                    // onClick={::this.sortingTable}
                                    // data-sort="opens"
                                    // data-value={justOrderBy.opens || ""}
                                    // className={`inadaptive-table__sort ${orderingByClass(
                                    //     justOrderBy.opens
                                    // )}`}
                                >
                                    {translation.opening}
                                </span>
                            </div>
                            {isCompany && (
                                <div className="inadaptive-table__td inadaptive-table__td_100">
                                    <span
                                        // onClick={::this.sortingTable}
                                        // data-sort="price"
                                        // data-value={justOrderBy.price || ""}
                                        // className={`inadaptive-table__sort ${orderingByClass(
                                        //     justOrderBy.price
                                        // )}`}
                                    >
                                        {translation.bidsCost}
                                    </span>
                                </div>
                            )}
                            <div className="inadaptive-table__td inadaptive-table__td_98">
                                <span> {translation.myCV} </span> 
                            </div>
                            {isCompany && (
                                <div className="inadaptive-table__td inadaptive-table__td_98">
                                    <span>{translation.open}</span>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className="inadaptive-table__tbody inadaptive-table__tbody--wrapped inadaptive-table__tbody_sticky-rating">
                    { queueFetch && <LoadAnim/> || 
                        (items.length && items.map((el, index) => (
                            <RaitingRow
                                {...el}
                                handleChangePopupBuyContact={handleChangePopupBuyContact}
                                byContacts={byContacts}
                                companyId={companyId ? companyId : companyIdR}
                                knowledges={knowledges}
                                profileOppenings={profileOppenings}
                                key={index}
                                index={index}
                                isCompany={isCompany}
                                actions={actions}
                                queueLabel={queueLabel}
                                priceConfiguration={priceConfiguration}
                                skillTestsHierarchy={skillTestsHierarchy}
                                applicantsList={applicantsList}
                                youId={youId}
                            />
                        ))) || null}
                </div>
            </div>
        );
    }
}
