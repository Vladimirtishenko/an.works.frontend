import React from "react";
import { Link } from "react-router-dom";

import level from "../../../../../../databases/general/level.json";
import position from "../../../../../../databases/general/position.json";
import { extendsSingleSkill } from "../../../../../../helpers/mapperSkills.helper.js";
import {
    transformToDays
    } from '../../../../../../helpers/date.helper.js';
import {
    getTopPosition 
    } from '../../../../../../helpers/rating.helper.js';
// Additional Components
import RatingTestDescription from "./ratingTestDescription.jsx";
import RatingDescription from "./ratingDescription.jsx";
import RatingSkillDescription from "./ratingSkillDescription.jsx";
import RatingLanguageDescription from "./ratingLanguageDescription.jsx";
import RatingLocationDescription from "./ratingLocationDescription.jsx";
import RatingOccupationDescription from "./ratingOccupationDescription.jsx";
import RatingExperienceDescription from "./ratingExperienceDescription.jsx";

import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export class RaitingRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expandedInfoStatus: false
        };
    }

    toggleExpandedInfoStatus() {
        this.setState({
            ...this.state,
            expandedInfoStatus: !this.state.expandedInfoStatus
        });
    }

    checkBuyContact(data){
        if(!data) return false;
        const { applicantsList:{
            items = []
        }} = this.props;
        let find = _.findIndex(items, {applicantId: data});
        if(find != -1){
            return true;
        }else{
            return false;
        }
    }

    renderExpandedInfo(info,skillTestsHierarchy) {
        let {
                testsResults,
                skills,
                geography,
                experience,
                yersOld,
                gender,
                wishedSalary = ""
            } = info,
            { knowledges } = this.props;

        return (
            <div className="width--100">
                <RatingTestDescription testsResults={testsResults} skillTestsHierarchy={skillTestsHierarchy} />

                <div className="container-fluid font--left padding--t-25 padding--l-60">
                    <RatingSkillDescription
                        knowledges={knowledges}
                        skills={skills}
                    />

                    <RatingLanguageDescription 
                        knowledges={knowledges}
                        skills={skills}
                    />

                    <RatingOccupationDescription
                        occupation={geography.occupation}
                    />

                    <RatingLocationDescription geography={geography} />

                    <RatingExperienceDescription experience={experience} />
                </div>

                <RatingDescription
                    yersOld={yersOld} 
                    gender={gender}
                    geography={geography}
                    wishedSalary={wishedSalary}
                />
            </div>
        );
    }

    getMapPosition() {
        let {
                data: { positionLevel, mainSkill, positionTitle },
                knowledges
            } = this.props,
            positionObject = extendsSingleSkill(positionTitle, position) || "",
            levelObject = extendsSingleSkill(positionLevel, level) || "",
            skillObject = extendsSingleSkill(mainSkill, knowledges) || "",
            joinString =
                levelObject.name +
                " " +
                skillObject.name +
                " " +
                positionObject.name;

        return joinString;
    }
    getCountOpen(){
        let {
            data:{
                profileId = ''
            },
            profileOppenings = []
        } = this.props,
        count = profileOppenings.find(item => {
            return profileId == item.profileId;
        });
        return count ? count.openingsCount : 0 ;
    }
    getPrice(open,price,rank){
        if(!open && !price &&!rank) return 'бесценен'

        for(let i = 0,len = price.length; i < len ;i++){

            if(price[i].from <= rank && rank <= price[i].to){
                return price[i].openCountPrices[Number(open)];
            }else if(price[price.length - 1].to < rank){
                return 1;
            }
        }
    }
    render() {
        const {
            data,
            index,
            isCompany,
            youId = "",
            data:{
                expiredDate,
                rank = '',
                profileId = ''
            },
                skillTestsHierarchy,
                priceConfiguration,
                handleChangePopupBuyContact, 
                byContacts,
                queueLabel,
                i18n:{translation = {}} = {}
            } = this.props;
        const position = this.getMapPosition(),
        topPosition = getTopPosition(rank);
        const { expandedInfoStatus, popUpByUser } = this.state;
        const stickyClass =
            data.id === "you"
                ? "inadaptive-table__sticky-rating shadow--box"
                : "";
        const positionColClass = isCompany
            ? "inadaptive-table__td_150"
            : "inadaptive-table__td_348 inadaptive-table__td--left-position";
        const expandedInfoStatusClass = expandedInfoStatus
            ? "inadaptive-table__tr_expanded shadow--box"
            : "";
        const expandedInfoIconClass = expandedInfoStatus
            ? "inadaptive-table__sort_up"
            : "";
        let expiredDateRating = transformToDays(expiredDate);
        let open = this.getCountOpen(),
            price = this.getPrice(open,priceConfiguration,rank),
            contactOpen = isCompany ? this.checkBuyContact(profileId) : false;
        return (
            <div
                className={`inadaptive-table__tr inadaptive-table--padding-15 bg--white font--center font--14 ${stickyClass} ${expandedInfoStatusClass}`}
            >
                <div
                    data-title={translation.top}
                    className={`inadaptive-table__td inadaptive-table__rate-${topPosition.color} inadaptive-table__td_40`}
                >
                    <div className="fl fl--dir-col fl--align-c fl--justify-c height--100 font--color-white font--500">
                        <span className="font--10 font--uppercase">{topPosition.rank <= 50 ? translation.top : translation.not} </span>
                        <span className={`${topPosition.rank <= 50 ? 'font--18' : 'font--14'}`}>{topPosition.rank <= 50 ? topPosition.rank : 'ТОП' }</span>
                    </div>
                </div>
                <div className="fl fl--dir-col inadaptive-table__hack-content">
                    <div
                        className="fl fl--align-c pointer"
                        onClick={() => this.toggleExpandedInfoStatus()}
                    >
                        <div
                            data-title={translation.number}
                            className="inadaptive-table__td inadaptive-table--50 inadaptive-table__td_35 margin--l-5"
                        >
                            <span className="font--color-inactive">
                                {rank}
                            </span>
                        </div>
                        <div
                            data-title={translation.posititonID}
                            className={`inadaptive-table__td inadaptive-table--50 ${positionColClass}`}
                        >
                            <div className="fl fl--dir-col font--left ">
                                <div
                                    className={`font--16 font--500 font--color-primary font--capitalized inadaptive-table__sort word-break--word ${expandedInfoIconClass}`}
                                >
                                    {position}
                                    {(youId && youId === profileId && (
                                        <span className="margin--l-5 nowrap--all font--16 font--color-blue">
                                             ({translation.you})
                                        </span>
                                    ))}
                                </div>
                                <span className="font--12 font--color-inactive">
                                    {profileId}
                                </span>
                            </div>
                        </div>
                        <div
                            data-title={translation.score}
                            className="inadaptive-table__td inadaptive-table--50 inadaptive-table__td_98"
                        >
                            <span className={`font--20 inadaptive-table__rate-text-${topPosition.color}`}>
                                {data.rate && data.rate.toFixed(1)}
                            </span>
                        </div>
                        <div
                            data-title={translation.daysLeft}
                            className="inadaptive-table__td inadaptive-table--50 inadaptive-table__td_100"
                        >
                            <span className="font--color-primary">
                                {expiredDateRating}
                            </span>
                        </div>
                        <div
                            data-title={translation.opening}
                            className="inadaptive-table__td inadaptive-table--50 inadaptive-table__td_100"
                        >
                            <span className="font--color-primary">
                                {open || 0}
                            </span>
                        </div>
                        {isCompany && (
                            <div
                                data-title={translation.bidsCost}
                                className="inadaptive-table__td inadaptive-table--20  inadaptive-table__td_100"
                            >
                                <span className="font--color-primary">
                                    {price}
                                </span>
                            </div>
                        )}
                        <div
                            data-title={translation.myCV}
                            className="inadaptive-table__td inadaptive-table--20  inadaptive-table__td_98"
                        >
                            <Link
                                to={`/cv/${data.profileId}`}
                                className="font--color-blue link fl--tablet fl--justify-c padding--10"
                            >
                                <span className="font--16 icon icon--eye" />
                            </Link>
                        </div>
                        {isCompany && contactOpen &&  (
                            <div
                            data-title="Открыть"
                            className="inadaptive-table__td inadaptive-table--20  inadaptive-table__td_98"
                        >
                            <span
                                className="font--color-blue font--10 fl--tablet fl--justify-c"
                            >
                                {translation.opens}
                            </span>
                        </div>
                        ) || isCompany && (
                            <div
                                data-title="Открыть"
                                className="inadaptive-table__td inadaptive-table--20  inadaptive-table__td_98"
                            >
                                <span
                                    className="font--color-blue link fl--tablet fl--justify-c padding--10"
                                    onClick={(e)=>{
                                        e.stopPropagation();
                                        handleChangePopupBuyContact(true,{
                                            price : price,
                                            rank : rank,
                                            position : position,
                                            profileId : profileId,
                                            queueLabel: queueLabel
                                        });
                                    }}
                                >
                                    <span className="font--16 icon icon--payment" />
                                </span>
                            </div>
                        )}
                    </div>
                    {expandedInfoStatus && (
                        <div
                            className={`inadaptive-table__tr inadaptive-table--padding-15 bg--white font--center font--14 `}
                            key={index}
                        >
                            {this.renderExpandedInfo(data,skillTestsHierarchy)}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
