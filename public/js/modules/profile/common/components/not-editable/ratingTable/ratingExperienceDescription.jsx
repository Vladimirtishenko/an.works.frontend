import React from "react";
import {pluralizeYear,pluralizeMonth} from '../../../../../../helpers/pluralize.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../../../modules/i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RatingExperienceDescription extends React.PureComponent {

    constructor(props){
        super(props);
        this.state = {
            ...props
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let comparingProps = _.isEqual(prevProps, this.props);

        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props });
        }
    }

    monthToYear(value){

        const year = parseInt(value / 12),
                month = value % 12;

        return {year, month};

    }

    render() {
        let { experience:{it = 0} } = this.state,
            { month: monthIT, year: yearIT } = this.monthToYear(it),
            {i18n:{translation = {}} = {}} = this.props;


        return (
            <div className="row padding--xc-15 padding--yc-5">
                <div className="col-md-2 font--color-secondary">
                    {translation.experienceInIT}:
                </div>
                <div className="col-md-10 font--500 padding--l-20">
                    {yearIT} <span className="margin--r-10 font--14">{pluralizeYear(yearIT)}</span>
                    {monthIT} <span className="margin--r-10 font--14">{pluralizeMonth(monthIT)}</span>
                </div>
            </div>
        );
    }
}
