import React from "react";
import { Link } from "react-router-dom";

import Pagination from "../../pagination.jsx";
// import Price from "../../../applicant/components/modals/price.jsx";
import Price from "../../modal/price.jsx";
import BuyContact from "../../modal/buy-contact.jsx";
import InsufficientFunds from "../../modal/insufficient-funds.jsx";


import { RatingTable } from "./ratingTable.jsx";

import TokensCount from "../../tokensCount.jsx";

// Filters
import TechnologyChoise from "../../../../../../components/filters/publicFilters/technologyChoise.jsx";

import FiltersComponent from "../../../../../../components/filters/filters.jsx";

import TableFilters from "../../../../../../components/filters/tableFilters.jsx";

// Helpers

import {
    normalizeLimit,
    convertLimitToValue
} from "../../../../../../helpers/filters/limit.helper.js"

import {
    setQueueLabel
} from "../../../../../../helpers/queue.helper.js";

import {
    normalizeFilterToSending
} from '../../../../../../helpers/filters/filter.helper.js'

export default class RaitingComponent extends React.Component {
    constructor(props) {
        super(props);
        this.pageTopRef = React.createRef();
        this.state = {
            isPriceOpen: false,
            popUpByUser: false,
            contactInfo: ''
        };
    }

    handleChangePricePopupStatus(status) {
        this.setState({
            ...this.state,
            isPriceOpen: status
        });
    }
    handleChangePopupBuyContact(status, data) {
        this.setState({
            ...this.state,
            popUpByUser: status,
            contactInfo: data ? data : null
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            companyProfile:{
                queue:{
                    items: prevItem
                } = {}
            } = {}
        } = prevProps;
        const {
            companyProfile:{
                queue:{
                    items: nowItem
                } = {}
            } = {}
        } = this.props;

        if(prevItem == undefined || nowItem == undefined) return;
        
        let comparingProps = _.isEqual(prevItem, nowItem);

        if (!comparingProps) {
            this.openingContact(nowItem)
        }
    }

    openingContact(items = []){
        if(items && !items.length) return;
        let arr = [];
        const {
            companyProfile:{companyId: companyId, filters},oauth:{user:{ companyId: companyIdRecruter } = {}} = {},
            actions: { getOpenContact }
        } = this.props;
        items.forEach((items) =>{
            arr.push(items.data.profileId);
        });
        let toNormalizeFilter = normalizeFilterToSending({
            applicantId: arr
        }),
        mergedFilters = normalizeLimit(toNormalizeFilter, convertLimitToValue(filters));
        getOpenContact(companyId ? companyId : companyIdRecruter, mergedFilters);
    }

    byContacts(data){
        this.setState({
            ...this.state,
            contactInfo: data 
        });
    }

    buyAction(profileId, status) { 
        const { actions: { buyApplicant }, companyProfile:{companyId: companyId},oauth:{user:{ companyId: companyIdRecruter } = {}} = {}} = this.props;
        let {contactInfo: {queueLabel}} = this.state;
        buyApplicant(profileId, queueLabel, companyId ? companyId : companyIdRecruter);
        this.setState({
            ...this.state,
            popUpByUser: status
        });
    }

    onScrollTop() {
        window.scrollTo(0, this.pageTopRef.current.offsetTop);
    }

    onChangePaginatin(filters){
        const { actions: { getQueue }} = this.props;

        getQueue(null, filters);
    }

    render() {
        const {
            history: { goBack },
            companyProfile: {
                applicantsList = {},
                profileOppenings = [],
                filters,
                knowledges,
                queue = {},
                queue: { totalCount, queueLabel, priceConfiguration, items} = {},
                skillTestsHierarchy,
                balance:{
                    tokensCount = ''
                } = {}
            } = {},
            actions,
            recruiter = '',
            oauth:{
                user:{
                    uid = "",
                    companyId = ""
                }
            } = {}
        } = this.props,
        { isPriceOpen, popUpByUser, contactInfo} = this.state,
        limitToValue = convertLimitToValue(filters),
        skill = setQueueLabel(queueLabel, null);

        return (
            <div className="container-fluid relative" ref={this.pageTopRef}>
                <div className="row fl fl--align-c margin--b-30">
                    <div className="col-md-4 col-lg-3 margin--b-15 margin--md-b-0">
                        <span
                            className="link link__go-back icon--Rating_arrow link--blue font--12 margin--sm-b-25 margin--md-b-15"
                            onClick={goBack}
                        >
                            Назад
                        </span> 
                    </div>
                        <TechnologyChoise
                            actions={actions}
                            knowledges={knowledges}
                            skill={skill}
                        />
                    <div className="col-md-8 margin--t-20 margin--xl-t-0 offset-md-4 margin--lg-l-0 col-lg-4 fl fl--wrap fl--justify-lg-end fl--align-c">
                        <div className="col-sm-12 col-md-3 col-lg-5 padding--l-0 margin--b-15 margin--md-0 font--lg-right">
                            <span className="pointer margin--r-15 font--12 fl--inline fl--align-c icons--payment-before icons--font--16 icons--padding-r-5 font--color-blue font--color-secondary nowrap"  onClick={() => this.handleChangePricePopupStatus(true)} >
                                Прайс лист
                            </span>
                        </div>
                        <div className="col-sm-12 col-md-9 col-lg-7 padding--0 fl fl--justify-end">
                            <TokensCount tokensCount={tokensCount} history={recruiter ? false : true} />
                        </div>
                        
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 col-lg-3">
                        <FiltersComponent
                              actions={actions}
                              mainSkill={skill}
                              testHierarchy={skillTestsHierarchy}
                              knowledges={knowledges}
                              filters={filters} />
                    </div>
                    <div className="col-md-8 col-lg-9">
                        <TableFilters
                            totalAmount={totalCount}
                            actions={actions}
                            filters={filters}
                        />
                        <div className="row-container scroll--x-auto margin--xc-0">
                            <RatingTable
                                i18n={this.props.i18n}
                                applicantsList={applicantsList}
                                profileOppenings={profileOppenings}
                                actions={actions}
                                filters={filters}
                                skillTestsHierarchy={skillTestsHierarchy}
                                queue={queue}
                                knowledges={knowledges}
                                isCompany={true}
                                companyId={companyId}
                                handleChangePopupBuyContact={::this.handleChangePopupBuyContact}
                                byContacts={::this.byContacts}
                            />
                        </div>
                        <div className="row-container fl fl--justify-end">
                            {
                                (totalCount > limitToValue) && <Pagination 
                                    count={totalCount}
                                    actions={::this.onChangePaginatin}
                                    filters={filters}
                                    wrapperClass="pagination margin--yc-10"
                                /> || null
                            }
                        </div>
                    </div>
                </div>
                <div className="anchor" onClick={() => this.onScrollTop()}>
                    <span className={`icon icon--Rating_arrow rotate--90`} />
                </div>
                {isPriceOpen && (
                    <Price
                        priceConfiguration={priceConfiguration}
                        closePopup={() =>
                            this.handleChangePricePopupStatus(false)
                        }
                    />
                )}
                {contactInfo && tokensCount >= contactInfo.price && popUpByUser && (
                    <BuyContact
                        closePopup={() =>
                            this.handleChangePopupBuyContact(false)
                        }
                        contactInfo={contactInfo}
                        buyAction = {::this.buyAction}
                    />
                ) || popUpByUser && (
                    <InsufficientFunds
                        recruiter={true}
                        closePopup={() =>
                            this.handleChangePopupBuyContact(false)
                        }
                    />
                ) || null}
            </div>
        );
    }
}
