import React from "react";

export const TableControls = props => (
    <div className="fl">
        <span className="l-h--1 fl fl--align-c font--color-secondary font--12 margin--r-15">
            <span className="icon icon--Print font--18 font--color-blue margin--r-5" />
            Распечатать
        </span>
        <span className="l-h--1 fl fl--align-c font--color-secondary font--12">
            <span className="icon icon--export font--18 font--color-blue margin--r-5 " />
            Экспорт
        </span>
    </div>
);
