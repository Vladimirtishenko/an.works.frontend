import React from "react";
import { Card } from "../card.dumb.jsx";
import { DeleteButton } from "../../../../../components/form/deleteButton.jsx";
import Pagination from "../pagination.jsx";

export class Notifications extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isExpanded: false
        };
    }

    handleExpand(status) {
        this.setState({
            isExpanded: status
        });
    }

    render() {
        const { isExpanded } = this.state;
        return (
            <Card icon="notif" title="Уведомления">
                <div className="info info--shadow bg--white info--lined ">
                    <div className="fl">
                        <div className="info__icon info__icon--lg info__icon--success">
                            <span className="icon icon--alarm" />
                        </div>
                        <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                            Вас уже открыли 3 из 5 раз
                        </div>
                    </div>
                </div>
                <div className="info info--shadow bg--white info--lined ">
                    <div className="fl">
                        <div className="info__icon info__icon--lg info__icon--success">
                            <span className="icon icon--alarm" />
                        </div>
                        <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                            Вас уже открыли 3 из 5 раз
                        </div>
                    </div>
                </div>
                <div className="info info--shadow bg--white info--lined ">
                    <div className="fl">
                        <div className="info__icon info__icon--lg info__icon--success">
                            <span className="icon icon--alarm" />
                        </div>
                        <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                            Вас уже открыли 3 из 5 раз
                        </div>
                    </div>
                </div>
                {isExpanded && (
                    <div>
                        <div className="info info--shadow bg--white info--lined ">
                            <div className="fl">
                                <div className="info__icon info__icon--lg info__icon--success">
                                    <span className="icon icon--alarm" />
                                </div>
                                <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                                    Вас уже открыли 3 из 5 раз
                                </div>
                            </div>
                        </div>
                        <div className="info info--shadow bg--white info--lined ">
                            <div className="fl">
                                <div className="info__icon info__icon--lg info__icon--success">
                                    <span className="icon icon--alarm" />
                                </div>
                                <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                                    Вас уже открыли 3 из 5 раз
                                </div>
                            </div>
                        </div>
                        <div className="info info--shadow bg--white info--lined ">
                            <div className="fl">
                                <div className="info__icon info__icon--lg info__icon--success">
                                    <span className="icon icon--alarm" />
                                </div>
                                <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                                    Вас уже открыли 3 из 5 раз
                                </div>
                            </div>
                        </div>
                        <div className="info info--shadow bg--white info--lined ">
                            <div className="fl">
                                <div className="info__icon info__icon--lg info__icon--success">
                                    <span className="icon icon--alarm" />
                                </div>
                                <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                                    Вас уже открыли 3 из 5 раз
                                </div>
                            </div>
                        </div>
                        <div className="info info--shadow bg--white info--lined ">
                            <div className="fl">
                                <div className="info__icon info__icon--lg info__icon--success">
                                    <span className="icon icon--alarm" />
                                </div>
                                <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                                    Вас уже открыли 3 из 5 раз
                                </div>
                            </div>
                        </div>
                        <div className="info info--shadow bg--white info--lined ">
                            <div className="fl">
                                <div className="info__icon info__icon--lg info__icon--success">
                                    <span className="icon icon--alarm" />
                                </div>
                                <div className="info__content fl fl--align-c padding--l-20 font--color-secondary">
                                    Вас уже открыли 3 из 5 раз
                                </div>
                            </div>
                        </div>
                    </div>
                )}
                <div className="padding--5">
                    {isExpanded ? (
                        <div className="">
                            <div className="fl fl--align-c fl--justify-b padding--xc-10 font--12 font--color-inactive font--center">
                                <span>Уведомлений 10 из 20</span>
                                <Pagination
                                    wrapperClass="pagination margin--yc-10 fl--justify-c"
                                    pages={20}
                                    selectedPage={5}
                                />
                            </div>
                            <div className="fl fl--align-c fl--justify-b padding--10">
                                <span
                                    className={`font--color-blue pointer font--12`}
                                    onClick={() => this.handleExpand(false)}
                                >
                                    Скрыть
                                </span>
                                <DeleteButton
                                    className="font--12 padding--xc-5 padding--l-15"
                                    data-id={""}
                                    data-mark={""}
                                    onClick={() => null}
                                    text="Очистить все"
                                />
                            </div>
                        </div>
                    ) : (
                        <div className="padding--10">
                            <span
                                className={`font--color-blue pointer font--12`}
                                onClick={() => this.handleExpand(true)}
                            >
                                Смотреть все
                            </span>
                        </div>
                    )}
                </div>
            </Card>
        );
    }
}
