import React from "react";
import { HireRecruiter } from "../../../components/not-editable/hireHistory/hireRecruiter.jsx";
import { HireApplicant } from "../../../components/not-editable/hireHistory/hireApplicant.jsx";
import { HirePosition } from "../../../components/not-editable/hireHistory/hirePosition.jsx";
import { HireContacts } from "../../../components/not-editable/hireHistory/hireContacts.jsx";
import { HireTokens } from "../../../components/not-editable/hireHistory/hireTokens.jsx";
import { HireCV } from "../../../components/not-editable/hireHistory/hireCV.jsx";
import { HireOpeningDate } from "../../../components/not-editable/hireHistory/hireOpeningDate.jsx";
import StatisticsButtons from '../../../../applicant/components/profile/buttons/statisticsButtons.jsx';
import { RecruiterContactsInfo } from "./recruiterContactsInfo.jsx";
import {dmyDate} from '../../../../../../helpers/date.helper.js';
import { connect } from "react-redux";

import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RecruiterTableRow extends React.Component{

    callToUpdateHiringRow(isHired, companyId, applicantId){
        let data = {
            companyId: companyId,
            applicantId: applicantId,
            companyAnswer: isHired === true ? 'hired' : 'notHired'// ONe of ['hired', 'notHired']
        };
        
        const {
            updateHiringStatus
        } = this.props;

        updateHiringStatus(data);
    }
    static defaultProps = {
        hireHistory: true
    }
    statusName(value){
        const {i18n:{translation = {}} = {}} = this.props;
        const statusNames = {
            hired: translation.hired,
            notHired: translation.notHired,
            waiting: translation.discussed
        };
        return statusNames[value]
    }
    render(){
        let {
            data,
            data:{
                status
            },
            hireHistory,
            data: {additionalData: { recruiter,  company}},
            handleShowContacts,
            i18n:{translation = {}} = {}
        } = this.props;
        data.status = data.companyAnswer || 'waiting';
        return(
            <tr className="respons-flex-table__tr respons-flex-table__tr--padding-b-0 font--color-primary fl--align-lg-c">
                <td
                    className="
                    respons-flex-table__td
                    respons-flex-table__td--no-before
                    respons-flex-table--w-50
                    respons-flex-table--md-w-100
                    respons-flex-table--xl-w-14
                    respons-flex-table--font-12
                    respons-flex-table--md-d-row
                    fl--order-4
                    fl--order-md-8
                    fl--order-xl-1
                    font--color-secondary
                "
                >
                    <HireOpeningDate data={dmyDate(data.createdDate)} />
                </td>
                {
                    hireHistory && (
                        <td
                            data-title={translation.recruiter}
                            className="
                            respons-flex-table__td
                            respons-flex-table--w-50
                            respons-flex-table--md-w-30
                            respons-flex-table--xl-w-14
                            respons-flex-table--font-14
                            fl--order-7
                            fl--order-md-6 
                            fl--order-xl-1
                            font--color-primary
                            font--500
                            padding--r-10
                            word-break--word
                        "
                        >
                        <HireRecruiter data={data.additionalData.recruiter} />
                    </td>
                    ) || null
                }
               
                <td
                    data-title={translation.applicant}
                    className="
                    respons-flex-table__td
                    respons-flex-table--w-50
                    respons-flex-table--md-w-45
                    respons-flex-table--xl-w-14
                    respons-flex-table--font-14
                    fl--order-1
                    fl--order-xl-1
                    font--color-primary
                    font--500
                    padding--r-10
                    word-break--word
                    "
                >
                    <HireApplicant data={data.additionalData.applicant} />
                </td>

                <td
                    data-title={translation.workPosition}
                    className="
                        respons-flex-table__td
                        respons-flex-table--w-50
                        respons-flex-table--md-w-30
                        respons-flex-table--xl-w-17
                        fl--order-3
                        fl--order-md-2
                        fl--order-xl-1
                        font--color-primary
                        respons-flex-table--font-14
                        padding--r-10
                        padding--md-r-50
                        padding--xl-r-15
                        font--500
                        word-break--word
                    "
                >
                    <HirePosition data={data.additionalData.applicant} />
                </td>
                {
                    hireHistory && (
                        <td
                            data-title={translation.contacts}
                            className="
                            respons-flex-table__td
                            respons-flex-table--w-100
                            respons-flex-table--md-w-45
                            respons-flex-table--xl-w-10
                            respons-flex-table--font-14
                            pointer
                            padding--r-10
                            fl--order-5
                            fl--order-xl-1
                            "
                        >
                        <HireContacts
                            data={data.additionalData.applicant}
                            handleShowContacts={status =>
                                handleShowContacts(status, data.additionalData.applicant.profileId)
                            }
                        />
                    </td>
                    ) || (
                        <td
                            data-title={translation.contacts}
                            className="
                                respons-flex-table__td
                                respons-flex-table--w-100
                                respons-flex-table--md-w-45
                                respons-flex-table--xl-w-25
                                respons-flex-table--font-12
                                padding--r-10
                                fl--order-4
                                fl--order-xl-1
                            "
                        >
                        <RecruiterContactsInfo data={data.additionalData.applicant} />
                    </td>
                            )
                }
               
                <td
                    data-title={translation.bidsPerContact}
                    className="
                    respons-flex-table__td
                    respons-flex-table--w-50
                    respons-flex-table--md-w-25
                    respons-flex-table--xl-w-10
                    respons-flex-table--font-14
                    fl--order-6
                    fl--order-md-7
                    fl--order-xl-1
                    font--xl-center
                ">
                    <HireTokens data={data.additionalData.price} />
                </td>
                <td
                    data-title={translation.condition}
                    className="
                    respons-flex-table__td
                    respons-flex-table--w-50
                    respons-flex-table--md-w-25
                    respons-flex-table--xl-w-15
                    respons-flex-table--font-14
                    fl--order-2
                    fl--order-md-3
                    fl--order-xl-1
                    respons-flex-table--xl-align-center
                    font--color-inactive
                ">
                    { hireHistory && (
                         <span
                         className="font--color-blue font--uppercase font--10"
                        >
                         {this.statusName(status)}
                     </span>
                    ) 
                    || (
                        <StatisticsButtons
                            {...data}
                            callToUpdateHiringRow={::this.callToUpdateHiringRow}
                        />
                    )
                    }
                </td>
                <td
                    className="
                    respons-flex-table__td
                    respons-flex-table__td--no-margin
                    respons-flex-table__td--no-before
                    respons-flex-table--w-100-30
                    respons-flex-table--md-w-100-60
                    respons-flex-table--xl-w-5
                    respons-flex-table--font-14
                    respons-flex-table--xl-margin-0
                    fl--order-7
                    fl--order-md-8
                    fl--order-xl-1
                    respons-flex-table__eye

                "
                >
                    <HireCV data={data.additionalData.applicant} /> 
                </td>
                {/* <td

                    className="
                    respons-flex-table__td
                    respons-flex-table__td--no-margin
                    respons-flex-table__td--no-before
                    respons-flex-table--j-center
                    respons-flex-table--d-row
                    respons-flex-table--md-align-center
                    respons-flex-table--w-100
                    respons-flex-table--md-w-5
                    respons-flex-table--font-14
                    padding--yc-10
                    fl--order-8
                    fl--order-md-4
                    fl--order-xl-1
                    hide--xl
                "
                >
                    <span
                        className="
                        icon
                        icon--carret_down
                        font--6
                        font--color-light-gray
                        respons-flex-table__card--close
                    "
                    />
                </td> */}
            </tr>
        );
    }

}