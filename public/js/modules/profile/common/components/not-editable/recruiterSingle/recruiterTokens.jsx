import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RecruiterTokens extends React.Component{
    render(){
        const {
            i18n:{
                translation = '' 
            } = {},
            data:{
                spendTokensCount = ''
            } = {}
        } = this.props;
        return (
            <div className="padding--30">
                <h2 className="font--14 font--500 font--uppercase margin--b-30">
                    {translation.tokens}
                </h2>
                <div className="fl fl--justify-b margin--b-20">
                    <div className="font--12 font--color-secondary">
                        {translation.bidsSpent}
                    </div>
                    <div className="font--16 font--color-primary font--500">{spendTokensCount}</div>
                </div>
            </div>
        )
    }
}
