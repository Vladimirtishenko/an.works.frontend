import React from "react";
import Avatar from "../../not-editable/avatar.jsx"; 
import {dmyDate} from "../../../../../../helpers/date.helper.js" 
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RecruiterInfo extends React.Component{

    render(){
        const {
            i18n:{
                translation = {}
            } = {},
            data:{
                uid = '',
                profile: {
                    firstName = '',
                    lastName = '',
                    email = ''
                } = {},
                createdDate = ''
            } = {}
        } = this.props;
        return (
            <React.Fragment>
                <Avatar
                    uid={uid}
                    alt="hr"
                    className="avatar--square"
                />
                <div className="padding--0 padding--md-l-30 font--center font--md-left">
                    <div className="fl fl--dir-col fl--justify-b height--100">
                        <div>
                            <h2 className="font--18 font--500 font--color-primary margin--b-10">
                                {firstName + ' '+ lastName}
                            </h2>
                            <p className="link font--12 font--color-secondary word-break--all">
                                {email}
                            </p>
                            {/*{positions.map((p, i) => (*/}
                                {/*<p*/}
                                    {/*key={i}*/}
                                    {/*className={`font--12 ${*/}
                                        {/*i === 0*/}
                                            {/*? "font--color-inactive"*/}
                                            {/*: "font--color-secondary"*/}
                                    {/*}`}*/}
                                {/*>*/}
                                    {/*{p}*/}
                                {/*</p>*/}
                            {/*))}*/}
                        </div>
                        <p className="font--12 font--color-inactive">
                            {translation.inServiceWith+' '}
                            <span className="display--b">{dmyDate(createdDate)}</span>
                        </p>
                    </div>
                </div>
            </React.Fragment>
        )
    }

};
