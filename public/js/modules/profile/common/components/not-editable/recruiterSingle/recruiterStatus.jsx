import React from "react";

const mockName = {
    hired: "Нанят",
    notHired: "Не нанят",
    discuss: "Обсуждается"
};

export class RecruiterStatus extends React.Component {
    render() {
        return (
            <React.Fragment>
                <button className="btn btn--primary padding--y-5 margin--b-5 font--10  btn--line-height-1-7 width--px-100 ">
                    {mockName[this.props.data]}
                </button>
            </React.Fragment>
        );
    }
}
