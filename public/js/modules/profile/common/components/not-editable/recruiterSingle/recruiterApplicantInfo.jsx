import React from "react";

export const RecruiterApplicantInfo = ({ data }) => (
    <React.Fragment>
        {data.name}
        <span className="font--color-secondary font--12 font--500 margin--t-10">
            {data.id}
        </span>
    </React.Fragment>
);
