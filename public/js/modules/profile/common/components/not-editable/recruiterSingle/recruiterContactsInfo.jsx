import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export class RecruiterContactsInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isExpanded: false
        };
    }

    handleExpand(status) {
        this.setState({
            isExpanded: status
        });
    }

    render() {
        let { isExpanded } = this.state,
            { i18n:{translation = {}} = {}, data:{contactInfo:{ phoneNumber = [] , linkedinAccount = "" , skypeAccount = "" , telegramAccount = "" , viberAccount = ""} } = {}, data = {} } = this.props;

        return (
            <ul className="list-contact">
            {
                phoneNumber && (
                    <li className="margin--b-5">
                        <ul className="fl">
                            <li className="list-contact__name-contact width--40">
                                {translation.phone}:
                            </li>
                            <li className="list-contact__value-contact">
                                {phoneNumber[0]}
                            </li>
                        </ul>
                    </li>
                )
            }
                
                <li className="margin--b-5">
                    <ul className="fl">
                        <li className="list-contact__name-contact width--40">
                            {translation.emailAddress}:
                        </li>
                        <li className="list-contact__value-contact">
                            <a
                                href="mailto:hr@anworks.com"
                                className="link font--color-blue"
                            >
                                {data.email}
                            </a>
                        </li>
                    </ul>
                </li>
                {!isExpanded ? (
                    <li
                        className="width--40 font--18 pointer l-h--1 font--color-secondary"
                        onClick={() => this.handleExpand(true)}
                    >
                        ...
                    </li>
                ) : (
                    <React.Fragment>
                        {
                            telegramAccount && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">
                                            {translation.telegram}:
                                        </li>
                                        <li className="list-contact__value-contact">
                                            <a
                                                href="#"
                                                className="link font--color-blue"
                                            >
                                                {telegramAccount}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                     
                        {
                            viberAccount && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">
                                            {translation.viber}:
                                        </li>
                                        <li className="list-contact__value-contact">
                                            <a
                                                href="#"
                                                className="link font--color-blue"
                                            >
                                                {viberAccount}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                       {
                           linkedinAccount && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">
                                            {translation.linkedIn}:
                                        </li>
                                        <li className="list-contact__value-contact">
                                            <a
                                                href={linkedinAccount}
                                                className="link font--color-blue"
                                                target="_blanck"
                                            >
                                                {linkedinAccount}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                           )
                       }
                        {
                            skypeAccount && (
                                <li className="margin--b-5">
                                    <ul className="fl">
                                        <li className="list-contact__name-contact width--40">
                                            {translation.skype}:
                                        </li>
                                        <li className="list-contact__value-contact">
                                            <a
                                                href="#"
                                                className="link font--color-blue"
                                            >
                                                {skypeAccount}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            )
                        }
                        
                        <li
                            className="width--40 font--10 font--underlined pointer opacity--0-5 font--color-secondary"
                            onClick={() => this.handleExpand(false)}
                        >
                            {translation.hide}
                        </li>
                    </React.Fragment>
                )}
            </ul>
        );
    }
}
