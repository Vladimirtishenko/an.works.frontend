import React from "react";
import { Link } from "react-router-dom";

export const RecruiterCVLink = ({ data }) => (
    <Link to={`/cv/${data && data.id ? data.id : undefined}`}>
        <span className="icon icon--eye font--18 font--color-blue margin--r-10 margin--xl-0 pointer" />
    </Link>
);
