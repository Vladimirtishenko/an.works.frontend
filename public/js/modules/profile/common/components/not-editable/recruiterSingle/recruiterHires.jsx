import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class RecruiterHires extends React.Component{
    render (){

        const {
            i18n:{
                translation = {}
            } = {},
            data:{
                hired = '',
                waitingHiring = '',
                totalContacts = ''
            } = {}
        } = this.props;

        return (
            <div className="padding--30">
                <h2 className="font--14 font--500 font--uppercase margin--b-30">
                    {translation.hiring}
                </h2>
                <div className="fl fl--justify-b margin--b-20">
                    <div className="font--12 font--color-secondary">{translation.totalHired}</div>
                    <div className="font--16 font--color-primary font--500">
                        {hired}
                    </div>
                </div>
                <div className="fl fl--justify-b margin--b-20">
                    <div className="font--12 font--color-secondary">
                        {translation.progressInterviews}
                    </div>
                    <div className="font--16 font--color-primary font--500">
                        {waitingHiring}
                    </div>
                </div>
                <div className="fl fl--justify-b">
                    <div className="font--12 font--color-secondary">
                        {translation.totalContactsPurchased}
                    </div>
                    <div className="font--16 font--color-primary font--500">
                        {totalContacts}
                    </div>
                </div>
            </div>
        )
    }
}
