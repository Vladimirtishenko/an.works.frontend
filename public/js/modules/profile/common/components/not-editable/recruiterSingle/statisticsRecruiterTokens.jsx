import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class StatisticsRecruiterTokens  extends React.Component{
    render(){
        const {
            i18n:{
                translation = {}
            },
            balance,
            totalCount
        } = this.props;
        return (
            <React.Fragment>
                <div className="padding--30">
                    <h2 className="font--14 font--500 font--uppercase margin--b-30">
                        {translation.tokens}
                    </h2>
                    <div className="margin--b-20 row">
                        <div className="font--12 font--color-secondary  col-10 col-md-8 col-lg-6">
                            {translation.bidsSpent}
                        </div>
                        <div className="font--16 font--color-primary col-2 col-md-4 padding--0 font--right font--md-left col-md- font--500 col-lg-6">
                            {totalCount || 0}
                        </div>
                    </div>
                    <div className="row">
                        <div className="font--12 font--color-secondary  col-10 col-md-8 col-lg-6">
                            {translation.availableBidsLimit}
                        </div>
                        <div className="font--16 font--color-primary col-2 col-md-4 padding--0 font--right font--md-left col-md- font--500 col-lg-6">
                            {balance || 0}
                        </div>
                    </div>
                </div>
                {/*
                TODO: not mvp no delete
                <hr className="hr hr--thin margin--0" />
                <div className="padding--xc-30 padding--yc-10">
                    <div className="row">
                        <div className="font--12 font--color-primary col-10 col-md-8 col-lg-6">
                            Лимит в месяц
                        </div>
                        <div className="font--16 font--color-primary col-2 col-md-4 padding--0 font--right font--md-left col-md- font--500 col-lg-6">
                            {limit || 0}
                        </div>
                    </div>
                </div> */}
            </React.Fragment>
        )
    }
};
