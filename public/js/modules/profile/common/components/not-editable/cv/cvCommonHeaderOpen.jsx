import React from "react";
import genderArray from "../../../../../../databases/general/gender.json";
import countries from "../../../../../../databases/general/countries.json";
import {
    mapperNameMainSckill
    } from '../../../../../../helpers/mapperSkills.helper.js';

import levelDictionary from '../../../../../../databases/general/level.json';
import positionDictionary from '../../../../../../databases/general/position.json';
import {extendsSingleSkill} from '../../../../../../helpers/mapperSkills.helper.js';
import {
    transformToYear,
    separateDate} from '../../../../../../helpers/date.helper.js';
import uniqid from 'uniqid';

import { getAvatarUrlByUID} from '../../../../../../helpers/avatar.helper.js';


import {
    getGenderStaticName 
} from '../../../../../../helpers/gender.helper.js';


import Avatar from "../../not-editable/avatar.jsx";

import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect(
    mapStateToProps,
    { ...i18n }
)
class CVCommonHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            city: "",
            country: ""
        };
    }

    async locationMaker() {
        let {
                geography: { city = '', country = '' } = {}
            } = this.props,
            countryIndex = _.findIndex(countries, ["label", country]),
            countryObject = countries[countryIndex] || {};

        if (country && city) {
            const db = await (await import(`../../../../../../databases/countries/${country}/cities.${country}.json`))
                .default;

            let cityIndex = _.findIndex(db, ["label", city]),
                cityObject = db[cityIndex] || {};

            this.setState({
                city: cityObject.name || "",
                country: countryObject.name || ""
            });
        }
    }

    componentDidMount() {
        this.locationMaker();

    }

    levelDictionaryMapper(label){

        let data = extendsSingleSkill(label, levelDictionary);

        return data && data.name || '';

    }

    positionDictionaryMapper(label){

        let data = extendsSingleSkill(label, positionDictionary);

        return data && data.name || '';

    }


    render() {
        const { city, country } = this.state;
        const { 
            knowledges = [],
            gender = '',   
            mainSkill = '',
            positionLevel = '',
            positionTitle = '',
            birthday = '',
            profileId = '',
            email = '',
            firstName = '',
            lastName = '',
            contactInfo: {
                phoneNumber = [],
                telegramAccount = '',
                viberAccount = '',
                linkedinAccount = ''
            } = {},
            wishedSalary = '',
            queueData:{
                rank = '',
                expiredDate = '',
                goingToStart = '',
                rate = ''
            } = {},
            uid = '',
            i18n:{
                translation = {}
            } = {}
        } = this.props,
            genderOfPersone = getGenderStaticName(gender),
            sckills = mapperNameMainSckill(knowledges, mainSkill) || null,
            role = this.levelDictionaryMapper(positionLevel) + ' ' + sckills + ' '+  this.positionDictionaryMapper(positionTitle),
            date = separateDate(birthday),
            {
                day = '',
                month = '',
                year = ''
            } = date;

            return (
                <div className="overflow--h box--rounded box--grey">
                   <div className="fl fl--dir-col fl--dir-md-row">
                        <span
                            className={`cv-header__rating inadaptive-table__rate-no-top font--18 font--color-white`}
                        >
                            {rank || 0}
                        </span>
                       <div className="row fl--1 margin--0 fl--justify-lg-c padding--b-20 word-break--all">
                            <div className="cv-header__logo col-lg-12">
                                <img
                                    src="../../../../../../../img/logo.svg"
                                    className="img-fluid cv-header__logo-img"
                                    alt="logo"
                                />
                            </div>
                            <div className="col-12 col-md-6 col-lg-4 fl fl--dir-col fl--justify-b font--color-primary margin--b-30 margin--lg-b-0">
                               <div className="margin--b-20">
                                    <p className="fon--16 font--500 font--bold font--uppercase word-break--word">
                                            {role}
                                        <span className="display--b font--12 word-break--word">
                                            {profileId}
                                        </span> 
                                    </p>
                               </div>
                               <div className="">
                                    <span className="font--14 display--b font--500 margin--r-5">
                                       {translation.desiredSalary}
                                    </span>
                                    <span className="font--20 font--500">
                                        ${wishedSalary || 0}
                                    </span>
                                </div>
                            </div>
                            <div className="col-12 col-md-6 col-lg-3 fl fl--align-c fl--dir-col margin--b-30 margin--lg-b-0">
                                <div className="avatar avatar--static margin--b-10">
                                    <Avatar
                                        uid={uid}
                                        alt="Avatar User"
                                    />
                                </div>
                                <div className="font--uppercase font--500 font--color-primary font--16 font--center word-break--word">
                                    <p>{firstName}</p>
                                    <p>{lastName}</p>
                                </div>
                            </div>
                            <div className="col-12 col-md-6 col-lg-4 fl fl--dir-col fl--justify-b">
                                <ul className="list-contact margin--b-30">
                                    {
                                        phoneNumber.length && (
                                            <li className="margin--b-5">
                                                <ul className="fl">
                                                <li className="font--color-primary font--14 width--40">{translation.phones}:</li>
                                                   <li>
                                                       <ul>
                                                        {phoneNumber.map((item,i)=>(
                                                            <li key={i} className="font--color-primary font--14 word-break--word">
                                                                {item}
                                                            </li>
                                                        ))}
                                                       </ul>
                                                   </li>
                                                </ul>
                                            </li>
                                        ) || null
                                    }
                                    {
                                        email && (
                                            <li className="margin--b-5">
                                                <ul className="fl">
                                                    <li className="font--color-primary font--14 width--40">{translation.emailAddress}:</li>
                                                    <li className="font--14 word-break--all">
                                                        <a href={`mailto:${email}`} className="link font--color-blue word-break--word">
                                                            {email}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        ) || null
                                    }
                                    {
                                        telegramAccount && (
                                            <li className="margin--b-5">
                                                <ul className="fl">
                                                    <li className="font--color-primary font--14 width--40">{translation.telegram}:</li>
                                                    <li className="font--14 word-break--all">
                                                        <a href="#" className="link font--color-blue word-break--word">
                                                            {telegramAccount}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        ) || null
                                    }
                                    {
                                        viberAccount && (
                                        <li className="margin--b-5">
                                            <ul className="fl">
                                                <li className="font--color-primary font--14 width--40">{translation.viber}:</li>
                                                <li className="font--14 word-break--all">
                                                    <a href="#" className="link font--color-blue word-break--word">
                                                        {viberAccount}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        ) || null
                                    }
                                    {
                                        linkedinAccount && (
                                            <li className="margin--b-5">
                                                <ul className="fl">
                                                    <li className="font--color-primary font--14 width--40">{translation.linkedIn}:</li>
                                                    <li className="font--14 word-break--all">
                                                        <a href={linkedinAccount} className="link font--color-blue word-break--word">
                                                            {linkedinAccount}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        ) || null
                                    }
                                    
                                </ul>
                                <div>
                                    <p className="font--12 font--500 margin--b-5 word-break--all">{genderOfPersone}</p>
                                    <p className="font--12 font--500 margin--b-5 word-break--all">{`${country}, ${city}`}</p>
                                    <p className="font--12 font--500 margin--r-5 word-break--all">{`${translation.birthday} ${day + '.' + month + '.' + year}`}</p>
                                </div>
                            </div>  
                    </div>
                   </div>
                </div>
            );
    }
}
export default CVCommonHeader;