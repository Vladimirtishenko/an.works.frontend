import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import Inheritance from '../../../../common/components/parent/main.jsx';
import cityBd from "../../../../../../databases/countries/2/cities.2.json";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVExpirienceInfo extends Inheritance {  
    constructor(props){
        super(props); 
    }

    nameCity(city){
        let citiesIndex = city ? _.findIndex(cityBd, { label: city }) : null,
        citySingleObject = citiesIndex > -1 ? cityBd[citiesIndex] : {};
        if(citySingleObject){
            return citySingleObject.name;
        }
    }

    render() {
        const {
            data = [],
            anonim,
            i18n:{translation = {}} = {}
        } = this.props,
        Experience = this.sortByDate([...data]);

        return (
            <CVCardBlock title={translation.experience} iconClass="icon--Experience">
                {Experience.length && Experience.map((el, index, arr) => (
                    <div key={index}>
                        <div className="fl fl--align-str">
                            <div
                                className={`cv-line ${
                                    index === arr.length - 1 ? "cv-line_last" : ""
                                } margin--r-10`}
                            >
                                <span className="cv-dot " />
                            </div>
                            <div className="fl fl--dir-col padding--b-45">
                                <h4 className="font--14 font--color-blue margin--b-10">
                                    {`${el.date.start[1]} - ${el.date.now ? translation.presenTime : el.date.end[1]}`}
                                </h4>
                                <h5 className={`${el.date.now ? 'font--color-blue':' font--color-secondary ' } font--16 margin--b-5`}>
                                    {el.position}
                                </h5>
                                <h4 className="font--14 font--color-secondary margin--b-5">
                                    {anonim ? translation.company : el.company } /{" "}
                                    {el.location && el.location.city && (
                                        <span className="font--color-inactive">
                                            {translation.countryOfResidence},{" "}
                                            {this.nameCity(el.location.city)}
                                        </span>
                                    )}
                                </h4>
                                <p className="font--12 font--color-secondary">{el.description}</p>
                        </div>
                        {
                            el.remote && (
                                <div className="margin--l-auto">
                                    <span className="font--12 font--color-secondary nowrap--all">{translation.freelance}</span>
                                </div>
                            ) || null
                        }
                        </div>
                    </div>
                )) || (
                    <p className="font--14 font--color-primary">
                       {translation.noWorkExperience}
                    </p>
                ) }
            </CVCardBlock>
        );
    }
}
export default CVExpirienceInfo;