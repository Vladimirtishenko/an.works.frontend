import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import  configuration from '../../../../../../configuration/rules.json';
import {
    transformToDays
    } from '../../../../../../helpers/date.helper.js';

import {
    mapperNameMainSckill
} from '../../../../../../helpers/mapperSkills.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVRatingInfo extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const {rank = '', expiredDate = '', openCount = '', goingToStart = '',knowledges = [] , mainSkill = '',i18n:{translation = {}} = {}} = this.props,
        date = configuration.daysOfPool - transformToDays(expiredDate)  == -1 ? 0 : configuration.daysOfPool - transformToDays(expiredDate),
        sckills = mapperNameMainSckill(knowledges, mainSkill);
        return (
            <CVCardBlock title={translation.ranking} iconClass="icon--Rating">
            <ul className="list">
                <li className="list__item fl fl--justify-b margin--b-15">
                    <span className="fl fl--align-c font--14 font--color-secondary">
                        <span className="cv-dot margin--r-10" />
                        {translation.ratingType}
                    </span>
                    <span className="font--16 font--color-blue">{sckills}</span>
                </li>
                <li className="list__item fl fl--justify-b margin--b-15">
                    <span className="fl fl--align-c font--14 font--color-secondary">
                        <span className="cv-dot margin--r-10" />
                            {translation.willingnessWork}
                    </span>
                    <span className="font--16 font--color-blue">
                        {goingToStart || 0}
                    </span>
                </li>
                <li className="list__item fl fl--justify-b margin--b-15">
                    <span className="fl fl--align-c font--14 font--color-secondary">
                        <span className="cv-dot margin--r-10" />
                        {translation.rankingPosition}
                    </span>
                    <span className="font--16 font--color-blue">
                        {rank || 0}
                    </span>
                </li>
                <li className="list__item fl fl--justify-b margin--b-15">
                    <span className="fl fl--align-c font--14 font--color-secondary">
                        <span className="cv-dot margin--r-10" />
                        {translation.timeRating}
                    </span>
                    <span className="font--16 font--color-blue">
                        {date || 0}
                    </span>
                </li>
                <li className="list__item fl fl--justify-b margin--b-15">
                    <span className="fl fl--align-c font--14 font--color-secondary">
                        <span className="cv-dot margin--r-10" />
                        {translation.opensNnumber}
                    </span>
                    <span className="font--16 font--color-blue">
                        {openCount || 0}
                    </span>
                </li>
            </ul>
        </CVCardBlock>
        );
    }
}
export default CVRatingInfo;