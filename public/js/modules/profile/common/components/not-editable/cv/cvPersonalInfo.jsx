import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)

export default  class CVPersonalInfo extends React.Component {

    render(){
        const {
            about,
            achivments,
            i18n: {translation =  {}} = {},
            waitings
        } = this.props;
        return(
            about || waitings || achivments) && (
                <CVCardBlock title={translation.personalInfo}iconClass="icon--Personal">
                    {
                        about &&  (
                            <div>
                                <h4 className="font--16 font--bold margin--b-5">{translation.about}</h4>
                                <p className="font--12 font--color-secondary margin--b-25">
                                    {about}
                                </p>
                            </div>) || null
                    }
                    {
                        waitings && (
                            <div>
                                <h4 className="font--16 font--bold margin--b-5">
                                    {translation.expectationsForFuturework}
                                </h4>
                                <p className="font--12 font--color-secondary margin--b-25">
                                    {waitings}
                                </p>
                            </div>) || null
                    }        
                    {
                        achivments && (
                            <div>
                                <h4 className="font--16 font--bold margin--b-5">{translation.achievements}</h4>
                                <p className="font--12 font--color-secondary margin--b-25">
                                    {achivments}
                                </p>
                            </div>
                            ) || null
                    }
            </CVCardBlock>
            ) || null 
    }
};