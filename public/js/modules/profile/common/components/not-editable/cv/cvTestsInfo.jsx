import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import CVProgressLine from "./cvProgressLine.jsx"; 
import { ProgressCircle } from "../../../../../../components/widgets/progress/index.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVTestsInfo extends React.Component {
    constructor(props){
        super(props);
    }
    setNameToResultArray(){

        let { testsResults, skillTestsHierarchy} = this.props;

        const data = [];

        if(!testsResults || !skillTestsHierarchy) return [];

        _.forEach(testsResults, (item) => {
            const find = _.find( skillTestsHierarchy.tests, {id: item.testUid} );

            if(find){
                let obj = {
                    ...item,
                    name: find.title
                }

                data.push(obj);
            }

        })

        return data;

    }

    render() {
        const {rate, i18n:{translation = {}} = {} } = this.props,
        normalized = this.setNameToResultArray();

        return (
            <CVCardBlock
            className="print_breakeInside"
            title={translation.skillsTesting}
            iconClass="icon--Tests"
            >
                <div className="fl fl--dir-col fl--align-c">
                {rate && ( 
                    <div>
                        <ProgressCircle
                                value={rate ? parseInt(rate) : ''}
                                size={120}
                                circleWidth={14}
                                contentClass="font--24 font--color-blue"
                            />
                        
                            <h5 className="font--18 font--color-blue margin--b-15">
                                {translation.averageRating}
                            </h5>
                    </div>
                    )
                }
                </div>
                <div>
                    {normalized && normalized.map((test, index) => (
                        <CVProgressLine
                            className="margin--b-15"
                            key={index}
                            result={test.result}
                            name={test.name}
                        />
                    ))}
                </div>
            </CVCardBlock>
        );
    }
}
export default CVTestsInfo;