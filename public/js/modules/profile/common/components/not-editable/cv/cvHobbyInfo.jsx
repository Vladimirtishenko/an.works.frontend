import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";
function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
export default class CVHobbyInfo extends React.Component{

    render(){
        const {
            data,
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        return(
            <CVCardBlock title={translation.hobby} iconClass="icon--Hobbie">
                    <p
                        className="font--12 font--color-secondary margin--b-15"
                    >
                        {data}
                    </p>
            </CVCardBlock>

        )
    }
} 