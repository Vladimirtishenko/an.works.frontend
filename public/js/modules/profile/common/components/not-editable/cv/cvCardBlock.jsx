import React from "react";

const CVCardBlock = props => (
    <div className={`cv-card ${props.className}`}>
        <span className={`cv-card__icon`}>
            <span className={`icon ${props.iconClass} cv-card__icon_inner`} />
        </span>
        <h3 className="cv-card__title">
            <span className="cv-card__title-line" />
            <span className="cv-card__title-text">
                <span className="print__text_nowrap">{props.title}</span>
                {props.subtitle && <span>{props.subtitle}</span>}
            </span>
            <span className="cv-card__title-line" />
        </h3>
        <div className="cv-card__content">{props.children}</div>
    </div>
);
export default CVCardBlock;