import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import { LanguageCircle } from "../../../../../../components/widgets/progress/languageCircle.jsx";

import { mapperSkills } from "../../../../../../helpers/mapperSkills.helper.js";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVLanguagesInfo extends React.Component {
    constructor(props){
        super(props); 
    }

    render() {

        let { knowledges, i18n:{translation = {}} = {} } = this.props,
            languages = mapperSkills(this.props, knowledges, [], [{ isLang: true }] , 'array' , 1) || [];

        return (
            <CVCardBlock
                className="print_breakeInside"
                title={translation.languages}
                iconClass="icon--lang1"
            >
                <div className="fl fl--justify-c fl--align-c fl--wrap margin--yc-10">
                    {languages.length && languages.map((el, index) => (
                        <LanguageCircle
                            key={index}
                            className="language-progress"
                            value={el.value}
                            languageName={el.name}
                            size={120}
                            circleWidth={14}
                        />
                    ))}
                </div>
            </CVCardBlock>
        );
    }
}
export default CVLanguagesInfo;