import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";

import {mapperSkillsLabel} from "../../../../../../helpers/mapperSkills.helper.js";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVSkillsInfo extends React.Component {
    constructor(props){
        super(props);
    }
    render() {

        let { skills, knowledges,i18n:{translation = {}} = {} } = this.props,
            languageMixed =
                mapperSkillsLabel(skills, knowledges, [{ isLang: false }]) || [];
        return (
            <React.Fragment>
                {
                    languageMixed.length && (
                        <CVCardBlock title={translation.skills} iconClass="icon--Skills">
                            <div className="fl fl--dir-col margin--b-15">
                                {/* 
                                TODO: not mvp
                                <h5 className="font--16 font--color-secondary margin--b-5">
                                    Programming languages:
                                </h5> 
                                */}
                                <div className="fl fl--wrap">
                                    {languageMixed.map((s, index) => (
                                        <span
                                            className="font--14 font--bold font--highlight-grey padding--yc-8 padding--xc-10 margin--r-10 margin--yc-5"
                                            key={index}
                                        >
                                            {s.name}
                                        </span>
                                    ))}
                                </div>
                            </div>
                            {/* 
                            TODO: not mvp
                            <div className="fl fl--dir-col">
                                <h5 className="font--16 font--color-secondary margin--b-5">
                                    Other skills:
                                </h5>
                                <div className="fl fl--wrap">
                                    {data.map((s, index) => (
                                        <span
                                            className="font--14 font--bold font--highlight-grey padding--yc-8 padding--xc-10 margin--r-10 margin--yc-5"
                                            key={index}
                                        >
                                            {s.label}
                                        </span>
                                    ))}
                                </div>
                            </div> 
                            */}
                        </CVCardBlock>
                    ) || null
                } 
            </React.Fragment>
           
        );
    }
}
export default CVSkillsInfo;