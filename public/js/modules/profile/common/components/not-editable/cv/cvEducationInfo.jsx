import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import Inheritance from '../../../../common/components/parent/main.jsx';
import universityBd from "../../../../../../databases/countries/2/universities.2.json";
import cityBd from "../../../../../../databases/countries/2/cities.2.json";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVEducationInfo extends Inheritance {
    constructor(props){
        super(props);

    }

    checkNameUniversity(university){
        let universityIndex = university ? _.findIndex(universityBd, { label: university }) : null,
        
        universitySingleObject = universityIndex > -1 ? universityBd[universityIndex] : {};

        if(universitySingleObject){
            return universitySingleObject.name;
        }
    }

    nameCity(city){
        let citiesIndex = city ? _.findIndex(cityBd, { label: city }) : null,
        citySingleObject = citiesIndex > -1 ? cityBd[citiesIndex] : {};
        if(citySingleObject){
            return citySingleObject.name;
        }
    }


    // <div className="fl fl--dir-col padding--b-45">
    //                         <h4 className="font--14 font--color-blue margin--b-10">
    //                             {`${el.date.start[1]} - ${el.date.now ? 'Работаю сейчас': el.date.end[1]}`}
    //                         </h4>
    //                         <h5 className={`${el.date.now ? 'font--color-blue':' font--color-secondary ' } font--16 margin--b-5`}>
    //                             {el.position}
    //                         </h5>
    //                         <h4 className="font--14 font--color-secondary margin--b-5">
    //                             {anonim ? "Компания" : el.company } /{" "}
    //                             {el.location && el.location.city && (
    //                                 <span className="font--color-inactive">
    //                                     Украина,
    //                                     {this.nameCity(el.location.city)}
    //                                 </span>
    //                             )}
    //                         </h4>
    //                         <p className="font--12 font--color-secondary">{el.description}</p>
    //                     </div>

    render() {
        const {
            data = [],
            i18n:{translation = {}} = {}
        } = this.props,
        Education =  this.sortByDate([...data]);
        return (
            <CVCardBlock title={translation.education} iconClass="icon--Education">
                {
                   Education.length && Education.map((el, index, arr) => {
                        if(el.type == "university"){
                            return (
                                <div className="fl fl--align-str" key={index}>
                                <div
                                    className={`cv-line ${
                                        index === arr.length - 1 ? "cv-line_last" : ""
                                    } margin--r-10`}
                                >
                                    <span className="cv-dot " />
                                </div>
                                <div className="fl fl--dir-col padding--b-30">
                                    <h4 className={`${el.date.now ? 'font--color-blue':' font--color-secondary ' } font--16 margin--b-5`}> 
                                        {`${el.date.start[1]} - ${el.date.now ? 'Учусь сейчас': el.date.end[1]}`}
                                    </h4>
                                    <h5 className={`${el.date.now ? 'font--color-blue':' font--color-secondary ' } font--16 margin--b-5`}>Диплом</h5>
                                    <p className="font--14 font--color-secondary margin--b-5 font--bold">
                                        {`${el.degree} , ${el.specialization}`}
                                    </p>
                                    <p className="font--14 font--color-secondary font--bold">

                                        {this.checkNameUniversity(el.location.university)} /{" "}
                                        {el.location && el.location.city && (
                                            <span className="font--color-inactive">
                                                Украина,{" "}
                                                {this.nameCity(el.location.city)}
                                            </span>
                                        )} 
                                    </p>
                                </div>
                            </div>
                            )
                        }else{
                            return (
                                <div className="fl fl--align-str" key={index}>
                                    <div
                                        className={`cv-line ${
                                            index === arr.length - 1 ? "cv-line_last" : ""
                                        } margin--r-10`}
                                    >
                                        <span className="cv-dot " />
                                    </div>
                                    <div className="fl fl--dir-col padding--b-30">
                                        <h4 className="font--14 font--color-blue margin--b-15">
                                            {`${el.date.start[1]}`}
                                        </h4>
                                        <h5 className="font--16 margin--b-5 font--color-secondary ">{translation.certificate}</h5>
                                        <p className="font--14 font--color-secondary margin--b-5 font--bold">
                                            {`${el.name} , ${el.date.now ? translation.perpetual : translation.validUntil +' ' + el.date.end[1]}`}
                                        </p>
                                        <p className="font--14 font--color-secondary font--bold">
                                            {el.organisation}
                                        </p>
                                    </div>
                                </div>
                            )
                        }
                    }) || (
                        <p className="font--14 font--color-primary">
                           {translation.withoutEducation}
                        </p>
                    )
                }
            </CVCardBlock>
        )
        
    }
}
export default CVEducationInfo;