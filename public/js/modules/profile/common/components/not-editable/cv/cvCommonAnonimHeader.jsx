import React from "react";
import countries from "../../../../../../databases/general/countries.json";
import {
    mapperNameMainSckill
    } from '../../../../../../helpers/mapperSkills.helper.js';

import levelDictionary from '../../../../../../databases/general/level.json';
import positionDictionary from '../../../../../../databases/general/position.json';
import {extendsSingleSkill} from '../../../../../../helpers/mapperSkills.helper.js';

import {
    getGenderStaticName 
} from '../../../../../../helpers/gender.helper.js';

import {
    getTopPosition
    } from '../../../../../../helpers/rating.helper.js';
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect(
    mapStateToProps,
    { ...i18n }
)
class CVCommonHeaderAnonim extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            city: "",
            country: "",
            random: "",
            randomPosition: ""
        };
    }

    async locationMaker() {
        let {
                geography: { city = '', country = '' } = {}
            } = this.props,
            countryIndex = _.findIndex(countries, ["label", country]),
            countryObject = countries[countryIndex] || {};

        if (country && city) {
            const db = await (await import(`../../../../../../databases/countries/${country}/cities.${country}.json`))
                .default;

            let cityIndex = _.findIndex(db, ["label", city]),
                cityObject = db[cityIndex] || {};

            this.setState({
                city: cityObject.name || "",
                country: countryObject.name || ""
            });
        }
    }

    componentDidMount() {
        this.randomAge();
        this.locationMaker();

    }

    levelDictionaryMapper(label){

        let data = extendsSingleSkill(label, levelDictionary);

        return data && data.name || '';

    }

    positionDictionaryMapper(label){

        let data = extendsSingleSkill(label, positionDictionary);

        return data && data.name || '';

    }

    randomAge(){
        let random = (min = 0, max = 3) =>{
            let rand = min + Math.random() * (max + 1 - min);
            rand = Math.floor(rand);
            return rand;
        };
        let randPosition = random(1,3);
        
        this.setState({
            randomPosition: randPosition
        })
    }

    render() {
        const { city, country, random } = this.state;
        const { 
            knowledges = [],
            wishedSalary = '',
            positionLevel = '',
            positionTitle = '',
            mainSkill = '',
            yersOld:{
                from = '',
                to = ''
            } = {},
            geography = '',
            rank = '',
            anonim = '',
            gender = '',
            i18n:{translation = {}} = {}
        } = this.props,
        genderOfPersone = getGenderStaticName(gender),
        ranks = getTopPosition(rank),
        sckills = mapperNameMainSckill(knowledges, mainSkill) || null,
        role = this.levelDictionaryMapper(positionLevel) + ' ' + sckills + ' '+  this.positionDictionaryMapper(positionTitle),
        age = from == 73 ? from+'+' :from +" - " +to;

        if(anonim){
            return (
                <div className="cv-header box--rounded box--grey">
                    <span
                        className={`cv-header__rating inadaptive-table__rate-${ranks.color} fon--18 font--color-white`}
                    >
                        {rank || 0}
                    </span>
                    <div className="cv-header__content">
                        <div className="cv-header__text">
                            <div className="cv-header__text-item cv-header__text-item_main">
                                <span className="fon--16 font--bold font--uppercase">
                                    {role}
                                </span>
                                <span className="font--12 font--500">
                                    {/* {data.id} */}
                                </span>
                            </div>
                            <div className="cv-header__text-item">
                                <div className="cv-header__item-col">
                                    <span className="font--12 font--500 margin--r-5">
                                        {translation.desiredSalary}
                                    </span>
                                    <span className="font--16 font--500">
                                        ${wishedSalary || 0}
                                    </span>
                                </div>
                                <div className="cv-header__item-col">
                                    <span className="font--12 font--500 margin--r-5">{`${genderOfPersone}, ${age}`}</span>
                                    <span className="font--12 font--500">{`${country}, ${city}`}</span>
                                </div>
                            </div>
                        </div>
                        <span className="cv-header__logo">
                            <img
                                src="../../../../../../../img/logo.svg"
                                className="img-fluid cv-header__logo-img"
                                alt="logo"
                            />
                        </span>
                    </div>
                </div>
            );
        }else{
            return <div>Error</div>
        }

    }
}
export default CVCommonHeaderAnonim;