import React from "react";
import { Link } from "react-router-dom";

import CVPersonalInfo from "./cvPersonalInfo.jsx";
import CVLanguagesInfo from "./cvLanguagesInfo.jsx";
import CVTestsInfo from "./cvTestsInfo.jsx";
import CVSkillsInfo from "./cvSkillsInfo.jsx";
import CVRealocationInfo from "./cvRelocationInfo.jsx";
import CVExpirienceInfo from "./cvExpirienceInfo.jsx";
import CVEducationInfo from "./cvEducationInfo.jsx";
import CVRatingInfo from "./cvRatingInfo.jsx";
import CVHobbyInfo from "./cvHobbyInfo.jsx";
import CVCommonHeaderOpen from "./cvCommonHeaderOpen.jsx";
import CVCommonAnonimHeader from "./cvCommonAnonimHeader.jsx";

import { extendsSingleSkill } from "../../../../../../helpers/mapperSkills.helper.js";

export default class CVComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    getMapPosition({ positionLevel, mainSkill, positionTitle }, knowledges) {
        let positionObject = extendsSingleSkill(positionTitle, position) || "",
            levelObject = extendsSingleSkill(positionLevel, level) || "",
            skillObject = extendsSingleSkill(mainSkill, knowledges) || "",
            joinString =
                levelObject.name +
                " " +
                skillObject.name +
                " " +
                positionObject.name;

        return joinString;
    }

    render() {

        const {
            history: { goBack },
            companyProfile:{
                skillTestsHierarchy = [],
                knowledges = [],
                cvContact:{
                    contactInfo = "",
                    aboutMe:{
                        hobby = '',
                        waitings = '',
                        achivments = '',
                        about = ''
                    } = {},
                    gender = '',
                    firstName = '',
                    lastName = '',
                    birthday = '',
                    geography = {},
                    geography:{
                        city = '',
                        relocation:{
                            local = ''
                        } = {}
                    } = {},
                    experience = {},
                    educational = {},
                    positionLevel = '',
                    profileId = '',
                    positionTitle = '',
                    mainSkill = '',
                    skills = [],
                    email = '',
                    testsResults = [],
                    queueData:{
                        rank = '',
                        expiredDate = '',
                        goingToStart = '',
                        rate = ''
                    } = {},
                    queueData = {},
                    wishedSalary = '',
                    openCount = '',
                    uid = '',
                    yersOld = {}
                } = {}
            }
        } = this.props,
        anonim = contactInfo ? false : true;
        
        return (
            <div className="container">
                <div className="row margin--b-15 fl fl--align-c print_disable">
                    <div className="col-12 col-md-4 margin--b-15 margin--md-b-0"> 
                        <span
                            onClick={goBack}
                            className="link link__go-back icon--Rating_arrow link__go-back_blue link--blue font--12 margin--sm-b-15"
                        >
                            Назад
                        </span>
                    </div>
                    {/* {
                        tokensCount && (
                            <div className="col-12 offset-md-4 col-md-4  offset-lg-5  col-lg-3">
                                <div className="fl fl--align-c fl--justify-b padding--yc-5 padding--xc-15 box--white box--rounded shadow--box">
                                    <div className="fl fl--dir-col margin--r-10 font--12">
                                        <span className="font--color-secondary">
                                            Количест
                                            <wbr />
                                            во BIDS+
                                        </span>
                                        <Link to={'/payments-history'} className="font--color-green">
                                            История
                                        </Link>
                                    </div>
                                    <span className="font--28 font--bold">{tokensCount}</span>
                                </div> 
                            </div>
                        )
                    } */}
                    
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="box--white box--rounded shadow--box margin--b-15 padding--md-15">
                            <div className="padding--15">
                            
                            {
                                !contactInfo && city && (
                                    <CVCommonAnonimHeader 
                                        wishedSalary={wishedSalary}
                                        positionLevel={positionLevel}
                                        positionTitle={positionTitle}
                                        mainSkill={mainSkill}
                                        rank={rank}
                                        geography={geography}
                                        gender={gender}
                                        anonim={anonim}
                                        knowledges={knowledges}
                                        yersOld={yersOld}
                                    />
                                ) || contactInfo && city && (
                                    <CVCommonHeaderOpen 
                                        birthday={birthday}
                                        knowledges={knowledges}
                                        gender={gender}   
                                        mainSkill={mainSkill}
                                        positionLevel={positionLevel}
                                        positionTitle={positionTitle}
                                        birthday={birthday}
                                        profileId={profileId}
                                        email={email}
                                        firstName={firstName}
                                        geography={geography}
                                        lastName={lastName}
                                        contactInfo={contactInfo}
                                        wishedSalary={wishedSalary}
                                        queueData={queueData}
                                        uid={uid}
                                    />
                                ) || null
                            }
                            </div>
                            <div className="fl fl--align-st fl--wrap padding--yc-15 padding--md-xc-30 word-break--word">
                                <div className="fl fl--dir-col width--100 width--md-50 padding--xc-15">

                                    <CVPersonalInfo 
                                        waitings={waitings}
                                        achivments={achivments}
                                        about={about}
                                    />
                                    
                                    <CVLanguagesInfo
                                        knowledges={knowledges}
                                        skills={skills}
                                    />

                                   { skillTestsHierarchy && 
                                        (<CVTestsInfo
                                            rate={rate}
                                            skillTestsHierarchy={skillTestsHierarchy}
                                            testsResults={testsResults}
                                        />)
                                    }

                                    <CVSkillsInfo
                                        knowledges={knowledges}
                                        skills={skills}
                                    />
                                     {
                                        local && (
                                            <CVRealocationInfo 
                                                data={geography} 
                                            />
                                        )   
                                    }
                                </div>
                                <div className="fl fl--dir-col width--100 width--md-50 padding--xc-15">
                                    <CVExpirienceInfo 
                                        data={experience.items}
                                        anonim={anonim}
                                    />
                                    <CVEducationInfo
                                        data={educational.items}
                                    />
                                    {
                                      rank && <CVRatingInfo
                                            rank={rank}
                                            expiredDate={expiredDate}
                                            openCount={openCount}
                                            mainSkill={mainSkill}
                                            knowledges={knowledges}
                                            goingToStart={goingToStart}
                                        /> || null
                                    }
                                    {
                                        hobby && <CVHobbyInfo data={hobby} /> || null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        );
    }
}
