import React from "react";

const CVProgressLine = ({ className, result, name }) => (
    <div className={`fl fl--dir-col ${className}`}>
        <div className="fl fl--justify-b fl--align-c margin--b-5">
            <span className="font--16 font--bold">{name}</span>
            <span className="font--16 font--color-secondary">{result}</span>
        </div>
        <div className="cv-progress-line">
            <div
                className="cv-progress-line__overlay"
                style={{ width: `${100 - result}%` }}
            />
        </div>
    </div>
);
export default CVProgressLine;