import React from "react";
import CVCardBlock from "./cvCardBlock.jsx";
import {
    normalizeCountryAndCities,
    normalizeCityArray
} from "../../../../../../helpers/locationMapper.helper.js";
import occupationVocabularies from "../../../../../../databases/general/occupation.json";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
    
@connect( 
    mapStateToProps,
    { ...i18n }
)
class CVRealocationInfo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            country: '',
            city: []
        }
    }

    componentDidMount(){
        let { data:{relocation: { local = {} } = {}, country = ""} = {} } = this.props;

        if(!local || !country) return;
        
        let obj = normalizeCountryAndCities(country, local.cities);

        if(obj){
            this.citiesUpload(obj.country, obj.cities);
        }

    }

    

    async citiesUpload(country, cities){
        const db = await (
          await import(`../../../../../../databases/countries/${country.label}/cities.${country.label}.json`)
        ).default;

        let citiesList = normalizeCityArray(cities, db);

        if(citiesList){
            this.setState({
                ...citiesList,
                country: country.name,
                db
            })
        }

    }

    getOccupation(occupation) {
        if (!occupation) return "";

        let occupationList = [];

        _.forEach(occupationVocabularies, (item, i) => { 
            if(occupation[item.label]) {
                occupationList.push(item.sub);
            }   
        })
        return occupationList.join(', ');
    }

    render() {
        const {country: countrySingle, city} = this.state;
        const {
            data:{
                occupation = {},
                relocation:{
                    abroad = {},
                    local = {}
                } = {}
            } = {},
            i18n:{translation = {}} = {}
        } = this.props;
        const ocupationObject = this.getOccupation(occupation);

        return (
            <CVCardBlock
                title={translation.abilityRelocate}
                iconClass="icon--Location"
            >
               
                {
                    local.type && (
                        <div className="margin--b-25">
                            <h4 className="font--16 margin--b-10">
                                {translation.cities}{" "}
                                <span className="font--color-inactive">({translation.countryOfResidence})</span>
                            </h4>
                            <div className="fl fl--wrap">
                                {
                                    city.length && city.map((elem,i) =>(
                                        
                                            <span key={i} className="font--14 font--bold font--highlight-grey padding--yc-8 padding--xc-10 margin--r-10 margin--yc-5">
                                                {elem}
                                            </span>
                                        
                                    ))
                                }
                            </div>
                        </div>
                    )
                }
                {
                    abroad.type && (
                        <div className="margin--b-25">
                            <h4 className="font--16 margin--b-10">
                                {translation.readyToRelocateAbroad}
                            </h4>
                            
                            {/* TODO: NO MVP
                            <div className="fl fl--wrap">
                                <span
                                    className="font--14 font--bold font--highlight-grey padding--yc-8 padding--xc-10 margin--r-10 margin--yc-5"
                                >
                                    
                                </span>
                            </div>*/}
                        </div> 
                    )
                }
                <div className="margin--b-25">
                    <h4 className="font--16 margin--b-10">
                        {translation.typeOfEmployment}
                    </h4>
                        {ocupationObject}
                </div>
            </CVCardBlock>
        );
    }
}
export default CVRealocationInfo;