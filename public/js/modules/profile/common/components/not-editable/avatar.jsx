import React from 'react';
import { string } from 'prop-types';
import uniqid from 'uniqid';

import { getAvatarUrlByUID } from '../../../../../helpers/avatar.helper.js';

export default class Avatar extends React.Component {
    static propTypes = {
        alt: string.isRequired,
        uid: string,
        className: string,
        uniq: string
    }

    static defaultProps = {
        className: 'max-width--100-p max-height--100-p', 
        uid: '',
        uniq: undefined
    }

    constructor(props) {
        super(props);
        this.state = {
            avatar: ''
        }
    }

    loadImage(uid){

        if(!uid) return;

        const img = new Image(),
              src = getAvatarUrlByUID(uid),
              id = uniqid((new Date()).valueOf());

        img.src = src;

        img.onload = () => {
            this.setState({avatar: `${src}?id=${id}`});
        };

        img.onerror = () => {
            this.setState({avatar: "/img/NoPhoto.jpg"});
        };
    }

    componentDidMount(){
        const { uid } = this.props;
        this.loadImage(uid);
    }

    componentDidUpdate(prevProps){
        const { uid, uniq } = this.props;

        if(prevProps.uid !== uid){
            this.loadImage(uid);
        }

        if(prevProps.uniq !== uniq){
            this.loadImage(uid);
        }
    }

    handleError() {
        this.setState({avatar: "/img/NoPhoto.jpg"});
    }

    render(){
        const { alt, className } = this.props,
              { avatar } = this.state;

        return (avatar && <img className={className} onError={::this.handleError} src={avatar} alt={alt} />) || null;
    }
}
