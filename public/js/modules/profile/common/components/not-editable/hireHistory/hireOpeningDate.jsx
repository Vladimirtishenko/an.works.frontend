import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export class HireOpeningDate extends React.Component {
    render() {
        const {data,i18n:{translation = {}} = {}} = this.props;
        return (
            <React.Fragment>
                <span className="margin--r-5 hide--xl">{translation.openingDate}</span>
                {data}
            </React.Fragment>
        );
    }
}
