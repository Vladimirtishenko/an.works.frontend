import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export class HireCV extends React.Component { 
    render() {
        const { data, i18n:{translation = {}} = {} } = this.props;
        return (
            <Link to={`/cv/${data && data.profileId ? data.profileId : undefined}`} className="fl fl--align-c">
                <span className="icon icon--eye font--18 font--color-blue margin--r-10 margin--xl-0 pointer"/>
                <span className="l-h--1 font--color-blue font--14 hide--xl">{translation.viewCV}</span>
            </Link>
        );
    }
}
