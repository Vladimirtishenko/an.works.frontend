import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export class HireContacts extends React.Component {
    render() {
        const {
            data: { contactInfo, email },
            handleShowContacts,
            i18n:{
                translation = {}
            } = {}
        } = this.props;

        return (
            <React.Fragment>
                <span
                    className="font--color-blue font--underlined"
                    onClick={() => handleShowContacts(true)}
                >
                    {translation.display}
                </span>
            </React.Fragment>
        );
    }
}
