import React from "react";

export class HireApplicant extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <React.Fragment>
                {data.firstName +' '+data.lastName}
                <span className="font--color-inactive font--12 margin--t-5">
                    {data.profileId}
                </span>
            </React.Fragment>
        );
    }
}
