import React from "react";

const mockName = {
    hired: "Нанят",
    notHired: "Не нанят",
    waiting: "Обсуждается"
};

export class HireStatus extends React.Component {
    render() {
        return (
            <React.Fragment>
                <span className="font--color-blue font--uppercase font--10">
                    {mockName[this.props.data]}
                </span>
            </React.Fragment>
        );
    }
}
