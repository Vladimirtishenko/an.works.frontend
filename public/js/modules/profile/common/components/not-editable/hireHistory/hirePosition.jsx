import React from "react";

export class HirePosition extends React.Component {
    render() {
        return <span>{this.props.data.positionLevel+' '+this.props.data.positionTitle+' '+this.props.data.mainSkill}</span>;
    }
}
