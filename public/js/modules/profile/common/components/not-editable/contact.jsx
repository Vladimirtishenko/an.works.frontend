import React from 'react'
import { connect } from "react-redux";
import { object } from "prop-types";
import StaticItem from '../../../../../components/form/staticItem.jsx';
import StaticSocial from '../../../../../components/form/staticSocial.jsx';

import {Card} from '../../../common/components/card.dumb.jsx'
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
 
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class Contact extends React.Component {
    static propTypes = {
        i18n: object.isRequired
    };
    render(){ 

        const {
            telegramVerification:{
                isActive = ''
            } = {},
            i18n:{
                translation = {}
            } = {},
            contactInfo: {
                phoneNumber = [],
                telegramAccount,
                viberAccount,
                skypeAccount,
                linkedinAccount
            } = {},
            onEdit = true,
            notEdit
        } = this.props,
        edit = notEdit ? {iconRight:"edit", link: translation.edit || '', linkAction: onEdit} : {};
        return (
            <Card
                icon="contact"
                {...edit}
                linkComponent="contact"
                title={translation.contactsData}
                cardPadding="padding--15 padding--md-30"
            >
                <div className="row">
                    <div className="col-lg-5 margin--b-30 margin--lg-b-0">
                        <div className="font--color-inactive font--capitalized font--12 margin--b-15">{translation.phone}</div>
                        {
                            phoneNumber.map((item, i) => {
                                return (
                                    <StaticItem
                                        key={i}
                                        staticClass="font--14 font--color-primary margin--b-15"
                                        staticText={item}
                                    />
                                )
                            })
                        }
                    </div>
                    <div className="col-lg-7">
                        {
                           (telegramAccount || viberAccount || skypeAccount || linkedinAccount) ? <div className="font--color-inactive font--capitalized font--12 margin--b-10">{translation.socialNetworks}</div> : ''
                        } 
                        {
                            telegramAccount && <StaticSocial
                                wrapperClass="row fl--align-c margin--b-10"
                                label={translation.telegram}
                                labelClass="icon icon--telegram icon--size-20 fl fl--align-c col-2 col-md-4"
                                staticClass="font--14 col-10 col-md-8"
                                labelTextSpan="margin--l-10 font--color-secondary show--md font--14 font--roboto"
                                staticText={telegramAccount} 
                                staticTextWrapper="theme--social word-break--word"
                                additionalText={isActive ? `(${translation.forNotifications})`: ""}
                            /> || null
                        }
                        {
                            viberAccount && <StaticSocial
                                wrapperClass="row fl--align-c margin--b-10"
                                label={translation.viber}
                                labelClass="icon icon--viber icon--size-20 fl fl--align-c col-2 col-md-4"
                                staticClass="font--14 col-10 col-md-8"
                                labelTextSpan="margin--l-10 font--color-secondary show--md font--14 font--roboto"
                                staticText={viberAccount}
                                staticTextWrapper="theme--social word-break--word"
                            /> || null
                        }
                        {
                            skypeAccount && <StaticSocial
                                wrapperClass="row fl--align-c margin--b-10"
                                label={translation.skype}
                                labelClass="icon icon--skype icon--size-20 fl fl--align-c col-2 col-md-4"
                                staticClass="font--14 col-10 col-md-8"
                                labelTextSpan="margin--l-10 font--color-secondary show--md font--14 font--roboto"
                                staticText={skypeAccount}
                                staticTextWrapper="theme--social word-break--word"
                            /> || null
                        }
                        {
                            linkedinAccount && <StaticSocial
                                wrapperClass="row fl--align-c margin--b-10"
                                label={translation.linkedIn}
                                labelClass="icon icon--linkedIn icon--size-20 fl fl--align-c col-2 col-md-4"
                                staticClass="font--14 col-10 col-md-8"
                                labelTextSpan="margin--l-10 font--color-secondary show--md font--14 font--roboto"
                                staticText={linkedinAccount}
                                staticTextWrapper="theme--social word-break--word"
                            /> || null
                        }
                    </div>
                </div>
            </Card>
        )
    }

}
