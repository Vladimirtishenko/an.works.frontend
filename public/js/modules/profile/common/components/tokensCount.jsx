import React from "react";
import {Link} from "react-router-dom";
import { bool } from 'prop-types'
import { connect } from "react-redux";
import * as i18n from "../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class TokensCount extends React.PureComponent {
    static propTypes = {
        history: bool
    }
    static defaultProps = {
        history: true
    }

    render(){
        const { tokensCount, history, i18n:{translation = {}} = {}} = this.props;
        return (
            <div className="fl fl--align-c fl--justify-b padding--yc-5 padding--xc-15 box--white shadow--small box--rounded max-width--md-210 width--100">
                <div className="fl fl--dir-col margin--r-10 font--12">
                    <span className="font--color-secondary">
                        {translation.bIDSQuantity}+
                    </span>
                    {
                        history && (
                            <Link to={'/payments-history'}>
                                <span className="font--color-green">
                                    {translation.history}
                                </span>
                            </Link>

                        ) || null
                    }
                </div>
                <span className="font--28 font--bold">{tokensCount}</span>
            </div>
        )      
    }
}
