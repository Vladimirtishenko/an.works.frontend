import React from 'react'
import uniqid from 'uniqid';

import {
    transformToTotal,
    valueOfFormatForSorting
} from "../../../../../helpers/date.helper.js";

export default class ParentStep extends React.Component {

    activeProfile(profileArray){
        for (var i = 0; i < profileArray.length; i++) {

            let profile = this.props[profileArray[i]];

            if(profile && Object.keys(profile).length > 0){
                return profileArray[i];
            }
        }
    }

    findProfile(need){

        let profileArray = ['companyEmployeeProfile', 'companyProfile', 'applicant'],
            activeProfile = ::this.activeProfile(profileArray),
            progress = 0;


        (function findInObject(props) {
            _.forOwn(props, (value, key) => {

                if(need == key){
                    progress = value;
                }

                if(value && typeof value == 'object' && Object.keys(value).length > 0){
                    findInObject(value);
                }

            })
        })(this.props[activeProfile])


        return progress;

    }

    prevStep(entity){

        let direction = this.findProfile('direction'),
            { actions: { updateStep } } = this.props;

        updateStep(--direction);

    }

    appendFieldToProps(event){

        const { target: { dataset: { type, option, action } } } = event,
              { applicant: { profile: { [option]: { items } } }, actions } = this.props,
                obj = {
                    id: uniqid(),
                    type: type,
                    status: false
                },
                cloned = [...items];

        cloned.push(obj);

        actions[action](cloned);
    }

    removeFieldToProps(event){

        const { target: { dataset: { id, option, action, period } } } = event,
              { applicant: { profile: { [option]: { items } = {} } }, actions } = this.props,
              cloned = [...items],
              obj = cloned.filter((item) => {
                  if(item.id !== id){
                      return item;
                  }
              });

        if(period){
            const period = {it: 0, other: 0, common: 0, ...transformToTotal(obj)};
            actions[action](obj, period);
            return;
        }

        actions[action](obj);


    }

    appendField(event){

        const { target: { dataset: { mark, type } } } = event;

        if(!event || !event.target || !mark) return;

        let target = event.target,
            id = uniqid(),
            itemsArray = this.state[mark],
            pushData = {
                id: id,
                type: type
            };

        if(type && !this[type]) return;

        itemsArray.push(pushData);

        this.setState({[mark]: itemsArray});
    }

    removeFiled(event){
        const { target: { dataset: { id, mark } } } = event;

        if(id && mark){

            let newArray = _.filter(this.state[mark], (item) => {
              if(item.id != id){
                  return item;
              }
            })

            this.setState({[mark]: newArray});

        }
    }

    setNormalizeData(experience){

        let newExperienceNormalized = experience.map((item, i) => {

            let obj = {
                id: uniqid(),
                data: item
            };

            if(this.normalizeDataType){
                obj.type = ::this.normalizeDataType(item.type)
            }

            return obj;
        })

        return newExperienceNormalized;

    }

    sortByDate(exp){

        return exp.sort((a, b) => {
                    const { date: { end: ae, now: an } } = a,
                          { date: { end: be, now: bn } } = b,
                          endA = an ? valueOfFormatForSorting() : valueOfFormatForSorting(ae.join('-'), 'MM-YYYY'),
                          endB = bn ? valueOfFormatForSorting() : valueOfFormatForSorting(be.join('-'), 'MM-YYYY');

                    if (endA < endB) {
                        return 1;
                    }
                    if (endA > endB) {
                        return -1;
                    }

                    return 0;
            });
    }

}
