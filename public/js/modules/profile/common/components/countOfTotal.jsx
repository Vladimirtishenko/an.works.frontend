import React from "react";
import { connect } from "react-redux";
import * as i18n from "../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
export default class CountOfTotal extends React.PureComponent {
    static defaultProps = {
        text: "Контактов"
    }
    render(){
        const { total, limit, text, offset, i18n:{translation = {}} = {} } = this.props;
        
        let value =  limit < total ? offset ? (Number(offset) + limit) > total ? total : Number(offset) + limit : limit : total; 
        switch(true) {
            case (total == 0):
                return null;
                break;
            default:
                return (
                    <span className="font--12 font--color-inactive">
                           {`${text} ${value} ${translation.fromAmount} ${total}`}
                    </span>
                );
        }
    }
}
