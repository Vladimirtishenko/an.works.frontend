import React from 'react'
import uniqid from 'uniqid'

export const Card = (props = {}) => (
    <section id={props.id || uniqid('card-')} className={`card ${props.classWrap}`}>
        <div className={props.cardClass}>
            <div className="card__head fl fl--justify-b">
                <h2 className="title title__card fl fl--align-c">
                    <span className={`icon icon--main-color icon--${props.icon}`}></span>
                    <span className={props.titleClass}>
                        {props.title}
                    </span>
                </h2>
                
                {
                    props.iconRight && (
                        <div className="link link--mobile">
                            <span data-component={props.linkComponent} onClick={props.linkAction} className={`font--color-blue icons--top-1 icons--edit-card  icons--${props.iconRight}-before`}>
                                {props.link}
                            </span>
                        </div>
                    )
                }
               
            </div>
        </div>
        <div className={props.cardPadding}>
            {props.children}
        </div>
    </section>
)
