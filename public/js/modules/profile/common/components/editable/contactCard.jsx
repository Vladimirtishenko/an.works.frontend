import React from "react";
import { object } from "prop-types";
import { Card } from "../card.dumb.jsx";
import Phones from "./phones.jsx";

import Input from "../../../../../components/form/text.jsx";
import ActiveTelegram from "../../../applicant/components/modals/activeTelegram.jsx";
import TelegramNotif from '../../../applicant/components/modals/telegramNotif.jsx';
import { connect } from "react-redux";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
@connect(
    mapStateToProps,
    { ...i18n }
)
class ContactCard extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            contactInfo: props.contactInfo,
            isTelegramLinkClicked: false,
            isOpenTelegramPopup: false,
            btnTelegramStatus: true,
            changeTelegram: false,
            telegramVerification: props.telegramVerification,
            telegramVerificationFailed: props.telegramVerificationFailed
        };
    } 

    static defaultProps = {
        inputSizeSocial: "col-md-7 col-lg-6 col-xl-5",
        labelSizeSocial: "col-md-4 col-lg-3",
        wrapVerifideTelegram: "col-7 col-md-4 offset-md-4 margin--md-t-10 padding--md-0 margin--lg-0 padding--lg-xc-15 col-lg-2 col-xl-3",
        wrapBtnTelegram: "col-7 col-md-3 offset-md-8 col-lg-3 padding--md-0 margin--md-t-10 offset-lg-6 margin--xl-0 padding--xl-xc-15 col-xl-3"
    };
    static propTypes = {
        i18n: object.isRequired
    };

    setTelegramRedirectClicked() {
        this.setState({
            ...this.state,
            isOpenTelegramPopup: false,
            isTelegramLinkClicked: true
        });
    }
    stateBtnTelegram(value,change = true){
        let patternTeleg = /^@[A-Za-z0-9_-]{3,32}$/,
            patternMobile = /^\+?3?8?(0\d{9})$/i;
        if(patternTeleg.test(value) || patternMobile.test(value)){
            this.setState({
                ...this.state,
                btnTelegramStatus: false
            });
        }else if(!patternTeleg.test(value) || !patternMobile.test(value) && this.state.btnTelegramStatus == false){
            this.setState({
                ...this.state,
                btnTelegramStatus: true
            });
        }
        if(change){
            this.setState(state=>{
                return {
                    ...state,
                    changeTelegram: true
                }
            });
        }
    }
    componentDidMount(){
        const {contactInfo:{telegramAccount}} = this.props;
        this.stateBtnTelegram(telegramAccount,false);
    }

    componentDidUpdate(prevProps, prevState){
        let comparing =  _.isEqual(this.props,prevProps);
        if(!comparing){
            this.setState({
                ...this.state,
                telegramVerification: this.props.telegramVerification,
                telegramVerificationFailed: this.props.telegramVerificationFailed
            })
        }
    }

    onVerificationTelegramClick(e){
        e.preventDefault();
        this.setState({
            ...this.state,
            isOpenTelegramPopup: true
        });
    }

    render() {
        let { telegramVerification, telegramVerificationFailed, contactInfo, isOpenTelegramPopup, isTelegramLinkClicked, btnTelegramStatus} = this.state;
        const {
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        return (
            <Card
                icon="contact"
                title={translation.contactDetails}
                cardPadding="padding--15 padding--md-30"
            >
                <Phones phones={contactInfo} {...this.props} />

                <hr className="hr hr--fullwidth margin--b-20" />
                    <Input 
                        justState
                        validators={["isTelegram"]}
                        errorMessages={[translation.errorMessageTelegram]}
                        wrapperClass="row fl--align-c margin--b-10"
                        labelClass={`${
                            this.props.labelSizeSocial
                        } fl fl--align-c label form__label form__label--bold icon--telegram form__label--icon form__label--color-primary`}
                        inputWrapperClass={`${
                            this.props.inputSizeSocial
                        } padding--md-0 margin--b-15 margin--md-b-0`}
                        inputClass="input form__input"
                        label={translation.telegram}
                        value={contactInfo && contactInfo.telegramAccount}
                        name="contactInfo.telegramAccount"
                        onChange={::this.stateBtnTelegram}
                        readonly={!!telegramVerification && !!telegramVerification.isActive}
                    >
                        {
                            telegramVerification && (telegramVerification.isActive || telegramVerification.result == "Token is valid" ) && 
                            <div className={this.props.wrapVerifideTelegram}> 
                                <span className="icon--verified fl--inline fl--align-c font--color-green font--14 icons--margin-r-10 margin--b-10 margin--md-b-10">
                                    Verified
                                </span>
                            </div> 
                            ||
                            <div className={this.props.wrapBtnTelegram}>
                                <button onClick={::this.onVerificationTelegramClick} className={`btn width--100 height--px-40 btn--theme-blue ${btnTelegramStatus && 'btn--disabled'}`} disabled={btnTelegramStatus}>
                                    {translation.confirm}
                                </button>
                            </div>
                            || null
                        }
                    </Input>
                        
                <Input
                    validators={["maxStringLength:50"]}
                    errorMessages={[translation.errorMessageMaxLeng30]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold icon--viber form__label--icon form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    } padding--md-0`}
                    inputClass="input form__input"
                    label={translation.viber}
                    value={contactInfo && contactInfo.viberAccount}
                    name="contactInfo.viberAccount"
                />

                <Input
                    validators={["maxStringLength:60"]}
                    errorMessages={[translation.errorMessageMaxLeng60]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold form__label--icon icon--skype form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    } padding--md-0`}
                    inputClass="input form__input"
                    label={translation.skype}
                    value={contactInfo && contactInfo.skypeAccount}
                    name="contactInfo.skypeAccount"
                />

                <Input
                    validators={["isUrl"]}
                    errorMessages={[translation.errorMessageLinkedIn]}
                    wrapperClass="row fl--align-c"
                    labelClass={`${
                        this.props.labelSizeSocial
                    } fl fl--align-c label form__label form__label--bold form__label--icon icon--linkedIn form__label--color-primary`}
                    inputWrapperClass={`${
                        this.props.inputSizeSocial
                    } padding--md-0`}
                    inputClass="input form__input"
                    label={translation.linkedIn}
                    value={contactInfo && contactInfo.linkedinAccount}
                    name="contactInfo.linkedinAccount"
                />

                {isOpenTelegramPopup && (
                    <TelegramNotif
                        onclickAction={() => {
                            window.open(this.props.telegramRegistrationUrl, "_blank");
                            this.hideElements();
                            this.setTelegramRedirectClicked();
                        }}
                        onClose={() => {
                            this.hideElements();
                        }}
                    />
                )}

                {isTelegramLinkClicked && (
                    <ActiveTelegram
                        onclickAction={(token) => this.props.actions.verificateTelegramToken( token)} 
                        onClose={() => {
                            this.hideElements();
                        }}
                        verificed = {telegramVerification}
                        isFailed = {telegramVerificationFailed}
                    />
                )}

            </Card>
        );
    }

    hideElements() {
        this.setState({
            ...this.state,
            isTelegramLinkClicked: false,
            isOpenTelegramPopup: false
        });
    }
}

export default ContactCard;
