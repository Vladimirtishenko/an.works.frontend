import React from "react";
import { connect } from "react-redux";
import { object } from "prop-types";
import Combobox from "../../../../../components/form/combobox.jsx";
import DropDownList from "../../../../../components/form/dropDownList.jsx";
import ParentStep from "../../../common/components/parent/main.jsx";

import countries from "../../../../../databases/general/countries.json";
import Tooltip from "../../../../../components/widgets/toolTip.jsx";
import tooltipInfoCompany from "../../../../../databases/general/tooltipInfoCompany.json";

import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
 
@connect(
    mapStateToProps,
    { ...i18n }
)
class Location extends React.Component  {
    constructor(props) {
        super(props);

        this.state = {
            ...props,
            values: props.location,
            name: props.name,
            globalCitiesList: [],
            globalUniversitiesList: [],
            list: {
                countries: countries,
                cities: [],
                universities: [],
                universitiesList : []
            }
        };
    }
    static defaultProps = {
        wrapperClass: 'row  fl--align-c margin--b-10 combobox',
        labelClass: 'label form__label font--14 col-md-4 col-lg-3',
        className: 'col-md-6 col-xl-5 padding--md-0'
    }
    static propTypes = {
        i18n: object.isRequired
    };
    async citiesUpload(item, city) {
        if (item && item.label) {
            const db = await (await import(`../../../../../databases/countries/${
                item.label
            }/cities.${item.label}.json`)).default;

            this.setState({
                ...this.state,
                values: {
                    ...this.state.values,
                    country: item.label
                },
                list: {
                    ...this.state.list,
                    cities: db
                },
                globalCitiesList: db
            });
        }else if(item || item == ''){
            this.setState({
                ...this.state,
                values: {
                    ...this.state.values,
                    country: item,
                    city: '',
                    university: ''
                }
            })
        }

    }

    async universitiesUpload(item, university) {
        if (item && item.label && item.country_id) {
            const db = await (await import(`../../../../../databases/countries/${
                item.country_id
            }/universities.${item.country_id}.json`)).default;

            const universitiesList = db
                .filter(u =>
                    u.city_id === (this.state.values.city.label || this.state.values.city) ? true : false
                )
                .sort((u1, u2) =>
                    u1.name.toLowerCase() < u2.name.toLowerCase() ? -1 : 1
                );

            this.setState({
                ...this.state,
                values: {
                    ...this.state.values,
                    city: item.label
                },
                list: {
                    ...this.state.list,
                    universities: universitiesList ? universitiesList : db,
                    universitiesList: universitiesList ? universitiesList : []
                },
                globalUniversitiesList: db
            });
        }
    }

    onCitiesKeySlice(search) {
        let str = search.name ? search.name : search,
            list = this.state.globalCitiesList.slice(0),
            listFilter = list.filter(item => {
                if (item.name.toLowerCase().startsWith(str.toLowerCase())) {
                    return item;
                }
            });

        this.setState({
            ...this.state,
            values: {
                ...this.state.values,
                city: search.label || search
            },
            list: {
                ...this.state.list,
                cities: listFilter
            }
        });
    }

    onUniversitiesKeySlice(search) {

        if (search.length <= 2 && search.length != 0) return;
        const {
            list:{
                universities = []
            } = {}
        } = this.state;
        let str = search.name ? search.name : search,
            list = universities.slice(0),
            listFilter = list.filter(item => {
                if (item.name.toLowerCase().startsWith(str.toLowerCase())) {
                    return item;
                }
            });

        this.setState({
            ...this.state,
            values: {
                ...this.state.values,
                university: search.label || search
            },
            list: {
                ...this.state.list,
                universitiesList: listFilter
            }
        });
    }

    componentDidMount() {
        let { values } = this.state;

        if (values && values.country) {
            this.citiesUpload({ label: values.country }, values.city);
        }

        if (values && values.country && values.city && values.university) {
            this.universitiesUpload(
                { label: values.city, country_id: values.country },
                values.university
            );
        }
    }

    componentDidUpdate(prevProps,prevState){
        let { location:values } = this.props;
        let comparingProps = _.isEqual(prevProps, this.props);
        
        if (!comparingProps) {
            this.setState({ ...this.state, ...this.props, values: this.props.location, name: this.props.name,});

            if (values && values.country) {
                this.citiesUpload({ label: values.country }, values.city);
            }

            if (values && values.country && values.city && values.university) {
                this.universitiesUpload(
                    { label: values.city, country_id: values.country },
                    values.university
                );
            }
        }
    }

    render() {
        let { values, list, name, universityName,wrapperClass,labelClass,className } = this.state;
        const { withUniversity, i18n:{translation = {}} = {} } = this.props;
        return (
            <div>
                <Combobox
                    validators={['required' , 'combobox']}
                    errorMessages={[translation.errorMessageRquired , translation.errorMessageCombobox]}
                    wrapperClass={wrapperClass}
                    labelClass={labelClass}
                    className={className}
                    value={values.country || ""}
                    label={translation.country+ '*'}
                    onChange={::this.citiesUpload}
                    list={list.countries}
                    name={`${name}.country`}
                >
                   { name == 'companyInfo' &&
                        <Tooltip
                            wrapper="
                                margin--b-10
                                margin--md-b-0
                                col-2
                                order-1
                            "
                            description={translation.tooltipCountry}
                        />
                   } 
                </Combobox>
                <Combobox
                    validators={['required' , 'combobox']}
                    errorMessages={[translation.errorMessageRquired , translation.errorMessageCombobox]}
                    wrapperClass={wrapperClass}
                    labelClass={labelClass}
                    className={className}
                    value={values.city}
                    label={translation.city+ '*'}
                    disabled={values.country && values.country.length && parseInt(values.country)  ?  false : true}
                    list={list.cities}
                    onChange={(item, university) => {
                        this.onCitiesKeySlice(item);
                        if (withUniversity) {
                            this.universitiesUpload(item, university);
                            this.setState({
                                ...this.state,
                                values: {
                                    ...this.state.values,
                                    city: item,
                                    university: ''
                                }
                            })
                        }
                    }}
                    name={`${name}.city`}
                >
                { name == 'companyInfo' &&
                        <Tooltip
                            wrapper="
                                margin--b-10
                                margin--md-b-0
                                col-2
                                order-1
                            "
                            description={translation.tooltipCity}
                        />
                }
                </Combobox>
                {withUniversity && (
                    <Combobox
                        validators={['required' , 'combobox']}
                        errorMessages={[translation.errorMessageRquired , translation.errorMessageCombobox]}
                        wrapperClass={wrapperClass}
                        labelClass={labelClass}
                        className={className}
                        value={values.university}
                        label={translation.school+'*'}
                        disabled={values.city && values.city.length && parseInt(values.city)? false : true}
                        list={list.universitiesList}
                        onChange={::this.onUniversitiesKeySlice}
                        name={
                            name ? `${name}.university` : "geography.university"
                        }
                    />
                )}
            </div>
        );
    }
}

export default Location;
