import React from "react";
import { connect } from "react-redux";
import { object } from "prop-types";
import Input from "../../../../../components/form/text.jsx";
import PhoneInput from "../../../../../components/form/phone/index.jsx";
import uniqid from "uniqid";
import ParentStep from "../parent/main.jsx";
import { DeleteButton } from "../../../../../components/form/deleteButton.jsx";
import errorMessage from "../../../../../databases/errorMessage/ru/errorMessage.json";

import rules from "../../../../../configuration/rules.json";
import * as i18n from "../../../../i18n/actions/i18n.action.js";

function mapStateToProps(state) {
    return {
        i18n: state.i18n
    };
}
 
@connect(
    mapStateToProps,
    { ...i18n }
) 
class Phones extends ParentStep {
    constructor(props) {
        super(props);

        let { phoneNumber } = props.phones,
            arrayPhones =
                phoneNumber && Array.isArray(phoneNumber)
                    ? phoneNumber.clone()
                    : phoneNumber
                    ? [phoneNumber]
                    : [],
            firstNumber = arrayPhones.shift();

        this.state = {
            number: firstNumber,
            phones: arrayPhones ? ::this.setNormalizeData(arrayPhones) : []
        };
    }

    static defaultProps = {
        inputPhoneSize: 'col-md-7 col-lg-6 col-xl-5',
        labelPhoneSize: 'col-md-4 col-lg-3',
        positionBtnRemove: 'absolute--lg-r-25-p margin--md-r-17-p'
	}
    componentDidUpdate(prevProps){
        let comparingProps = _.isEqual(prevProps, this.props);

        if(!comparingProps){
            let { phoneNumber } = this.props.phones,
                arrayPhones =
                    phoneNumber && Array.isArray(phoneNumber)
                        ? phoneNumber.clone()
                        : phoneNumber
                        ? [phoneNumber]
                        : [],
                firstNumber = arrayPhones.shift();

            this.setState({
                number: firstNumber,
                phones: arrayPhones ? ::this.setNormalizeData(arrayPhones) : []
            });
        }
    }

    render(){
        const {
            i18n:{
                translation = {}
            } = {}
        } = this.props;
        let {phones, number} = this.state,
        limitPhones = phones.length < rules.limitPhones;
        return (
            <div>
                <PhoneInput  
                    validators={["required", "matchRegexp:^\\+[0-9]{8,}"]}
                    errorMessages={[ errorMessage.required, errorMessage.phone ]}
                    wrapperClass="row fl--align-c margin--b-10"
                    labelClass={`${this.props.labelPhoneSize} label form__label font--14 `}
                    inputWrapperClass={`${this.props.inputPhoneSize} padding--md-0`}
                    inputClass="input form__input margin--auto-md-l fl--order-1 margin--auto-xl-l"
                    label={translation.mainPhone+'*'}
                    value={number || ""}
                    name="contactInfo.phoneNumber[]"
                    defaultCountry="ua"
                />
                {
                    phones && phones.map((item, i) => {
                        return (<div className="row fl--align-c margin--b-10 relative" key={item.id}>
                                    <PhoneInput
                                        wrapperClass="fl fl--wrap width--100 fl--align-c "
                                        validators={['required', 'matchRegexp:^\\+[0-9]{8,}']}
                                        errorMessages={[errorMessage.required, errorMessage.phone]}
                                        label={translation.additionalPhone+'*'}
                                        name="contactInfo.phoneNumber[]"
                                        labelClass={`${this.props.labelPhoneSize} label form__label font--14 `}
                                        inputWrapperClass={`${this.props.inputPhoneSize} padding--md-0 margin--xl-l-0`}
                                        inputClass="input form__input margin--auto-xl-l fl--order-1"
                                        defaultCountry="ua"
                                        value={item && item.data || ""}
                                        />
                                        <span
                                            className={`${this.props.positionBtnRemove}
                                                link
                                                link--under
                                                link--grey
                                                font--12
                                                absolute--lg
                                                absolute--lg-yc-center
                                                margin--md-auto-l
                                                margin--lg-0
                                                margin--t-5
                                                margin--l-auto
                                                margin--r-15
                                            `}
                                            onClick={::this.removeFiled}
                                            data-id={item.id}
                                            data-mark="phones">
                                            {translation.delete}
                                        </span>
                                </div>)
                    }) || null
                }

                <div className="row margin--b-20">
                {limitPhones && ( <div className="offset-md-4 col-md-8 offset-lg-3 col-lg-9  padding--md-0">
                        <span
                            className="padding--r-20 icons--Add icons--size-16 icons--top-2 icons--margin-r-10 pointer-text font--12 font--color-blue"
                            data-mark="phones"
                            onClick={::this.appendField}
                        >
                            {translation.addPhone}
                        </span>
                    </div>) || null }
                </div>
            </div>
        );
    }
}

export default Phones;
