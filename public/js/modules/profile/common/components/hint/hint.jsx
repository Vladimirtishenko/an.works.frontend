import React from "react";

export default function hint(props){
    const {closeHint} = props;
    return(
        <div className="hint">
            <p className="hint__text margin--b-15">Вы можете принять участие в рйтинге</p>
            <button onClick={closeHint} className="hint__close"></button>
        </div>
    )
}