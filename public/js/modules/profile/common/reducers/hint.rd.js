import {
    HINT_RIGHT_ASIDE_RATING_LAYOUTS,
    HINT_RIGHT_PARTICIPATE_RATING_LAYOUTS
} from '../constants/hint.const.js';

const initialState = {
    hintProfileRating: false,
    hintParticipateRating: false
};

export default function hint(state = initialState, action) {

    switch (action.type) {
        
        case HINT_RIGHT_ASIDE_RATING_LAYOUTS:
            return {
                ...state,
                hintProfileRating: action.hintProfileRating
            };
        case HINT_RIGHT_PARTICIPATE_RATING_LAYOUTS:
            return{
                ...state,
                hintParticipateRating: action.hintParticipateRating
            }

        default:
            return state
    }
}
