import applicant from "../../applicant/reducers/profile.rd.js"
import companyProfile from "../../company/reducers/profile.rd.js"
import companyEmployeeProfile from "../../recruiter/reducers/profile.rd.js"

import React from 'react'
import {
    combineReducers
} from 'redux'

const profile = combineReducers({
    applicant,
    companyProfile,
    companyEmployeeProfile
});

export default profile;