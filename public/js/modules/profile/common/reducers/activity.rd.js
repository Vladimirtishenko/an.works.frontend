import {
    BURGER_MENU_STATUS_UPDATED,
    COLLAPSED_MENU,
    REGISTRAION_MODAL_OPEN,
    REGISTRAION_MODAL_CLOSE
} from '../constants/activity.const.js';

const initialState = {
    isBurgerMenuOpen: false,
    collapsed: true,
    registrationModal: false
};

export default function activity(state = initialState, action) {

    switch (action.type) {

        case BURGER_MENU_STATUS_UPDATED:
            return {
                ...state,
                isBurgerMenuOpen: action.isBurgerMenuOpen
            };

        case COLLAPSED_MENU:
            return {
                ...state,
                collapsed: action.collapsed
            };
        case REGISTRAION_MODAL_OPEN:
            return {
                ...state,
                registrationModal: action.registrationModal
            };

        case REGISTRAION_MODAL_CLOSE:
            return {
                ...state,
                registrationModal: action.registrationModal
            };

        default:
            return state
    }
}
