import * as types from '../constants/i18n.const.js';
import {nativeValidator} from '../../../helpers/validator.helper.js'
import rules from '../../../configuration/rules.json'

const initialState = ((t, l) => ({
    defaultLanguage: l || null,
    translation: t && nativeValidator('isJSON', t) ? JSON.parse(t) : {},
    availableLanguage: rules.availableLanguage,
    descriptionLanguage: rules.descriptionLanguage
}))(localStorage.translation, localStorage.defaultLanguage);

export default function i18n(state = initialState, action) {
    switch (action.type) {
        case types.CHANGE_LANGUAGE_FAILURE:
            return {
                ...state,
                notification: action.notification
            };
        case types.CHANGE_LANGUAGE_SUCCESS:
            return {
                ...state,
                defaultLanguage: action.defaultLanguage,
                translation: action.translation
            };
        case types.APPEND_LANGUAGE_SUCCESS:
            return {
                ...state,
                translation: {...state.translation, month: action.month}
            };

        case types.APPEND_LANGUAGE_FAIL:
            return {
                ...state,
                notification: action.notification
            };

        default:
            return state
    }
}
