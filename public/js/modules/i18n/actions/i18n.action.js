import * as types from '../constants/i18n.const.js';
import {getInstalledLanguage} from '../../../helpers/getInstalledLanguage.helper.js'

export function loadLanguage(lang) {

    return dispatch => {

        (async () => {

            let translation = (await import(`../translations/${lang}.json`)).default;

            if(translation){

              dispatch({
                  type: types.CHANGE_LANGUAGE_SUCCESS,
                  defaultLanguage: lang,
                  translation: translation
              })

            } else {

              dispatch({
                  type: types.CHANGE_LANGUAGE_FAILURE,
                  notification: {
                    type: 'error',
                    view: 'notice',
                    message: 'We couldn`t loaded translation for chosen language! Try later.'
                  }
              })

            }


          })()
    }
}

export function changeLanguage(lang) {

  return dispatch => {

      (async () => {

          if(!lang.value) return;

          let language = getInstalledLanguage(lang.value),
              translation = (await import(`../translations/${lang.value}.json`)).default;

          if(translation){

            dispatch({
                type: types.CHANGE_LANGUAGE_SUCCESS,
                defaultLanguage: language,
                translation: translation
            })

          } else {

            dispatch({
                type: types.CHANGE_LANGUAGE_FAILURE,
                notification: {
                  type: 'error',
                  view: 'notice',
                  message: 'We couldn`t loaded translation for chosen language! Try later.'
                }
            })

          }


      })()
  }

}

export function loaderMonth(lang) {

    return dispatch => {
        (async () => {

            if(!lang) return;

            let month = (await import(`../parts/month/${lang}.json`)).default;

            if(month){

              dispatch({
                  type: types.APPEND_LANGUAGE_SUCCESS,
                  month: month
              })

            } else {

              dispatch({
                  type: types.APPEND_LANGUAGE_FAIL,
                  notification: {
                    type: 'error',
                    view: 'notice',
                    message: 'We couldn`t loaded list of month for chosen language! Try later.'
                  }
              })
          }

      })()
    }


}
