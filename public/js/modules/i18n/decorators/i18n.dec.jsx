import { connect } from 'react-redux'
import React from 'react'
import {getInstalledLanguage} from '../../../helpers/getInstalledLanguage.helper.js' 

import * as loaderLanguage from '../actions/i18n.action.js' 

const withTranslation = (WrapedComponent) => {

  @connect(null, {...loaderLanguage})
  class i18nDecorator extends React.Component {

    constructor(props, context){
      super(props, context);
      this.state = {
        updatedLang: false
      }
    }

    setModified(i18n){
      localStorage.setItem('translation', JSON.stringify(i18n.translation));
      localStorage.setItem('defaultLanguage', i18n.defaultLanguage);
    }

    componentDidMount(){

      // let language = getInstalledLanguage();
      let language = 'ru';
      if(language != this.props.i18n.defaultLanguage || !this.state.updatedLang){
          this.props.loadLanguage(language);
          
      } else {
        this.setState({...this.state,languageLoaded: true});
      }

    }

    componentDidUpdate(prevProps){
      if(prevProps.i18n.defaultLanguage != this.props.i18n.defaultLanguage || !this.state.updatedLang){
        this.setModified(this.props.i18n);
        this.setState({...this.state,languageLoaded: true,updatedLang: true})
      }

    }

    render() {

      if(this.state.languageLoaded){
        return (
          <WrapedComponent {...this.props} />
        )
      }

      return (
        <div className="cssload-loader">
          <div className="cssload-inner cssload-one"></div>
          <div className="cssload-inner cssload-two"></div>
          <div className="cssload-inner cssload-three"></div>
        </div>
      )

    }

  }

  return i18nDecorator;

}
export default withTranslation;
