import { connect } from 'react-redux'
import React from 'react'

import * as loaderMonth from '../actions/i18n.action.js'

const withMonth = (Step) => {

  @connect(null, {...loaderMonth})
  class i18nMonthDecorator extends React.Component {

    constructor(props, context){
      super(props, context);
      this.state = {}
    }

    setModified(i18n){
      localStorage.setItem('translation', JSON.stringify(i18n.translation));
    }

    componentDidMount(){

      let {defaultLanguage} = this.props.i18n;

      this.props.loaderMonth(defaultLanguage);

    }

    componentDidUpdate(prevProps){
      if(!this.props.i18n.translation.month){
          let {defaultLanguage} = this.props.i18n;

          this.props.loaderMonth(defaultLanguage);
          return;
      }

      if(prevProps.i18n.translation != this.props.i18n.translation){
        this.setModified(this.props.i18n);
        this.setState({monthLoad: true})
      }

    }

    render() {

      if(this.state.monthLoad){
        return (
          <Step {...this.props} />
        )
      }

      return null;

    }

  }

  return i18nMonthDecorator;

}
export default withMonth;
