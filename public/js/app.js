'use strict';

/** @module base (Basic function) */
import './base.js';
/** @module React */
import React from 'react';
import 'lodash';
/** @module ReactDom */
import ReactDOM from 'react-dom';
/** @module Root wrapper */
import Root from './wrappers/root.jsx'

ReactDOM.render(
		<Root />,
    document.getElementById('app')
);
