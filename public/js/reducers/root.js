import React from 'react'
import {
  combineReducers
} from 'redux'

// Modules
import oauth from '../modules/oauth/reducers/oauth.rd.js'
import i18n from '../modules/i18n/reducers/i18n.rd.js'
import profile from '../modules/profile/common/reducers/index.rd.js'
import configuration from '../modules/admin/configuration/reducer.configuration.js'
import activity from '../modules/profile/common/reducers/activity.rd.js'
import hints from '../modules/profile/common/reducers/hint.rd.js'

// Libs
import notification from '../libraries/notification/reducer/index.rd.js'


const root = combineReducers({
  notification,
  oauth,
  i18n,
  profile,
  configuration,
  activity,
  hints
});

const initialState = root({}, {});

export default (state, action) => {

  if (action.type == 'LOG_OUT') {
    localStorage.removeItem('authToken');
    state = {};
  }

  return root(state, action);
}