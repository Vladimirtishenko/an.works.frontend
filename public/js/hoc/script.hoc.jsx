import React from 'react'

export default function generateAsyncScriptComponent(src) {
    return (Wrapper) => {
        return class AsyncScriptComponent extends React.Component {
            constructor(props){
                super(props)
                this.state = {
                    script: null,
                    src: src
                }

                this.resolve = ::this.resolve;
            }

            resolve(){
                this.setState({
                    script: true
                })
            }

            reject(e){
                console.error(e)
            }

            loadScript() {

                const {src} = this.state;

                this.el = document.createElement('script');

                this.el.src = src;

                this.el.addEventListener('load', this.resolve);

                this.el.addEventListener('error', this.reject);

                document.body.appendChild(this.el);

            }

            componentWillUnmount() {
                this.el.removeEventListener('load', this.resolve);
                this.el.removeEventListener('error', this.reject);

                document.body.removeChild(this.el);
            }

            componentDidMount(){

                this.loadScript()

            }

            render(){
                const {script} = this.state;

                if(script){
                    return <Wrapper {...this.props} />
                }

                return null;

            }
        }
    }
}
