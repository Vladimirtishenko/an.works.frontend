import configuration from "../configuration/host.json";

import { SET_NOTIFICATION } from '../libraries/notification/constant/notification.const.js'

const API_URL = configuration.origin;

class Api {

    static getAPIUrl()
    {
        return API_URL+'/api';
    }

    static __get(path, dispatch, type){

        return new Promise((resolve, reject) => {

            fetch(API_URL+'/api/'+path, {headers: {'X-Access-Token': localStorage.authToken}} ).then((response) => {

                Api.__actions(response, dispatch, resolve, reject, type)

            }).catch((err) => {

               dispatch({
                    type: type,
                    notification: {
                        type: "error",
                        view: 'notice',
                        message:
                            err +
                            '<br /> TypeAction: ' + type +
                            '<br /> TypeRequest: ' + API_URL+'/api/'+path +
                            '<br /> TypeMethod: ' + 'GET'
                    }

                })
            });

        })
    }

    static __files(path, file, uri){

        return new Promise((resolve, reject) => {
            let url = uri ? API_URL+'/'+uri : API_URL+'/files/'+path;

            fetch(url, {method: 'POST', body: file, headers: {'X-Access-Token': localStorage.authToken}} ).then((response) => {

                response.json().then((result) => {
                   resolve(result);
                })

            }).catch((err)=>{
               reject(err);
            });

        });

    }

    static __quiz_get(path, dispatch, type){

        return new Promise((resolve, reject) => {
            let config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${localStorage.authToken}`
                }
            }

            fetch(path, config).then((response) => {
                Api.__actions(response, dispatch, resolve, reject, type)
            }).catch((err)=>{
               dispatch({
                    type: type,
                    notification: {
                        type: "error",
                        view: 'notice',
                        message:
                            err +
                            '<br /> TypeAction: ' + type +
                            '<br /> TypeRequest: ' +  API_URL+'/'+path +
                            '<br /> TypeMethod: ' + method || 'POST'
                    }

                })
            });

        })
    }

    static __quiz_post(method, path, params, dispatch, type){

        return new Promise((resolve, reject) => {
            let config = {
                method: method || 'POST',
                headers: {
                    'Authorization': `Bearer ${localStorage.authToken}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }

            fetch(path, config).then((response) => {
                Api.__actions(response, dispatch, resolve, reject, type)
            }).catch((err)=>{
               dispatch({
                    type: type,
                    notification: {
                        type: "error",
                        view: 'notice',
                        message:
                            err +
                            '<br /> TypeAction: ' + type +
                            '<br /> TypeRequest: ' +  API_URL+'/'+path +
                            '<br /> TypeMethod: ' + method || 'POST'
                    }

                })
            });

        })


    }

    static __post(method, path, params, dispatch, type, headers){

        return new Promise((resolve, reject) => {
            let config = {
                method: method || 'POST'
            },
            auth = localStorage.authToken && (config.headers = {'X-Access-Token': `${localStorage.authToken}`}),
            body = (params && !headers) && (config.body = JSON.stringify(params)) || (config.body = params),
            head = !headers && (config.headers = {...config.headers, 'Accept': 'application/json', 'Content-Type': 'application/json'});

            fetch(API_URL+'/'+path, config ).then((response) => {

                Api.__actions(response, dispatch, resolve, reject, type)

            }).catch((err)=>{
               dispatch({
                    type: type,
                    notification: {
                        type: "error",
                        view: 'notice',
                        message:
                            err +
                            '<br /> TypeAction: ' + type +
                            '<br /> TypeRequest: ' +  API_URL+'/'+path +
                            '<br /> TypeMethod: ' + method || 'POST'
                    }

                })
            });

        })
    }

    static __unauthorized(response, dispatch){
        response.json().then((result) => {
            dispatch({
                type: 'LOG_OUT',
                notification: {
                    type: 'error',
                    view: 'notice',
                    message: result.message
                }
            })
        })
    }

    static __unassessable(response, dispatch){
        response.json().then((result) => {
            dispatch({
                type: 'LOG_OUT'
            })
        })
    }

    static __other(response, dispatch, type){
        response.json().then((result) => {
            dispatch({
                type: type,
                notification: {
                    type: "error",
                    view: 'notice',
                    message:
                        result.message  +
                        '<br /> TypeAction: ' + type
                }
            })
        })
    }

    static __simple(err, dispatch, type){
        dispatch({
            type: type,
            notification: {
                type: "error",
                view: 'notice',
                message:
                    err  +
                    '<br /> TypeAction: ' + type
            }
        })
    }

    static __actions(response, dispatch, resolve, reject, type){


        switch(response.status){
            case 401:

                Api.__unauthorized(response, dispatch);

                break;

            case 403:

                Api.__unassessable(response, dispatch);

                break;

            case 200:

                response.json().then((result) => {
                    resolve(result);
                }).catch((err) => {
                    Api.__simple(err, dispatch, type);
                })

                break;

            default:

                Api.__other(response, dispatch, type);

        }

    }
}

export default Api;
