export default function treeToArray(tree = {}, params = []) {

	let flatOptions = [];

		(function toFlat(items) {

			for (let key in items) {

				let newObject = {};

				for (let i = 0; i < params.length; i++) {
					if(items[key][params[i]]){
						newObject[params[i]] = items[key][params[i]];
					}
				}

				flatOptions.push(newObject);
				
				if(items[key].children){
					toFlat(items[key].children)
				} 

			}

		})(tree);

	return flatOptions;

}