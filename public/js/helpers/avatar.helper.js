import API from '../services/api.js'

export const getAvatarUrlByUID = (uid) => {
	return 	API.getAPIUrl()+'/static/files/uploads/users_avatars/'+uid+'.jpg';
}

export const getAvatarUrlByPath = (path) => {
	const src = /https?:\/\//.test(path);
	return 	src ? path : API.getAPIUrl()+path;
}
