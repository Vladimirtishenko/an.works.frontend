export const _getAggregatedHiringData = (items = {}) =>{

    const  {
        hired:{
            countContacts: hiredCountContacts = 0
        } = {},
        waiting:{
            countContacts: waitingCountContacts = 0
        } = {},
        total:{
            countContacts: totalCountContacts = 0,
            tokensCount: totalTokensCount = 0
        } = {}
    } = items;

    return {
        hired: hiredCountContacts,
        waitingHiring: waitingCountContacts,
        totalContacts: totalCountContacts,
        spendTokensCount: totalTokensCount
    };
}

export const getEmptyAggregatedHiringData = () =>
{
    return {
        hired: 0,
        waitingHiring: 0,
        totalContacts: 0,
        spendTokensCount: 0
    };
}
