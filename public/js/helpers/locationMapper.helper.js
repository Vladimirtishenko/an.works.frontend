import countries from '../databases/general/countries.json'
import uniqid from 'uniqid'

export const normalizeCountryAndCities = (country, cities) => {

    let countryIndex = country ? _.findIndex(countries, {'label': country} ) : null,
        countryName = countryIndex > -1 ? countries[countryIndex] : {},
        citiesList = cities || [];

    if(!countryName || !citiesList) return null;

    return {cities: citiesList, country: countryName};

}

export const normalizeCity = (cities, db) => {

    if(!db) return null;

    let citiesList = [];

    _.forEach(db, (item, i) => {
        if(cities.indexOf(item.label) > -1){
            citiesList.push(item.name);
        }
    })

    citiesList = citiesList.join(', ');

    return {city: citiesList}

}
export const normalizeCityArray = (cities, db) => {

    if(!db) return null;

    let citiesList = [];

    _.forEach(db, (item, i) => {
        if(cities.indexOf(item.label) > -1){
            citiesList.push(item.name);
        }
    })
    
    return {city: citiesList}

}

export const simplifyCityToArray = (cities) => {

    if(!cities) return null;

    const citiesArray = [];

    _.forEach(cities, (item, i) => {
        citiesArray.push(item.label);
    })

    return citiesArray;

}

export const findCitiesFromEmptyArray = async (country) => {

    if(country){
        const db = await (
                  await import(`../databases/countries/${country}/cities.${country}.json`)
                ).default;

        return db;

    }

}


export const findCities = async (cities, country) => {

    if(country && cities){
        const db = await (
                  await import(`../databases/countries/${country}/cities.${country}.json`)
                ).default;

        let citiesList = [];

        _.forEach(db, (item, i) => {
            if(cities.indexOf(item.label) > -1){
                citiesList.push({name: item.name, _id: item.label});
            }
        })

        return citiesList;

    }

}

export const mergeCountryAndCities = (country, cities, type) => {

    let countryName = country && country.name || "";

    if(!countryName) return countryName;

    switch(type) {
        case 'string':

            let string = '';

            _.forEach(cities, (item, i) => {
                string += item.name + ((cities.length - 1  !== i) ? ', ' : '');
            })

            string = countryName + ' (' + string + ')';

            return {_id: uniqid(), name: string};

        case 'object':

            let object = {};

            object._id = uniqid();
            object.country = countryName;
            object.cities = [];

            _.forEach(cities, (item, i) => {
                object.cities.push(item.name);
            })

            return object;

        default:
            return null

    }

}

export const uploadDiferentCountries = async (places, type) => {

    if(!Array.isArray(places)) return null;

    let placesObject = [],
        db = null;

    for (var i = 0; i < places.length; i++) {

        let place = places[i],
            countryIndex = place.country ? _.findIndex(countries, {'label': place.country} ) : null,
            countryName = countryIndex > -1 ? countries[countryIndex] : {},
            citiesList = place.cities && place.cities.split(',') || [],
            statnessObject = {};

        if(!countryName) continue;

        db = await (
                  await import(`../databases/countries/${place.country}/cities.${place.country}.json`)
                ).default;

        if(!db) continue;


        statnessObject.country = countryName && countryName.name;
        statnessObject._id = uniqid();
        statnessObject.cities = [];

        _.forEach(db, (item, i) => {
            if(citiesList.indexOf(item.label) > -1){
                statnessObject.cities.push(item.name);
            }
        })

        placesObject.push(statnessObject);

    }

    switch(type){
        case 'object':
            return placesObject;

        case 'string':

            let byArray = []

            _.forEach(placesObject, (item, i) => {

                let cities = item.cities.join(','),
                    country = item.country,
                    string = country + '(' + cities + ')';

                byArray.push({_id: uniqid(), name: string});
            })

            return byArray;

        default:
            return {};

    }


}
