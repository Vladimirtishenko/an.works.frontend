
export const getTopPosition = (rank = 0) => {

    if(rank > 0 && rank < 11){

        return {rank:10,color:"first"}

    }else if(rank > 10 && rank < 21){

        return {rank:20,color:"second"}

    }else if(rank > 20 && rank < 31){

        return {rank:30,color:"third"}

    }else if(rank > 30 && rank < 41){

        return {rank:40,color:"fourth"}

    }else if(rank > 40 && rank < 51){

        return {rank:50,color:"fifth"}

    }else if(rank > 50 && rank < 101){

        return {rank:100,color:"ten"}

    }else if(rank > 100){

        return {rank:100,color:"no-top"}
    }else{
        return {rank: 0,color:"error"}
    }
}