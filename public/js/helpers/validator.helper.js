import validation from 'validator';

// 1. contains(str, seed)	check if the string contains the seed.
// 2. equals(str, comparison)	check if the string matches the comparison.
// 3. isAfter(str [, date])	check if the string is a date that's after the specified date (defaults to now).
// 4. isAlpha(str [, locale])	check if the string contains only letters (a-zA-Z).

// Locale is one of ['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'bg-BG', 'cs-CZ', 'da-DK', 'de-DE', 'el-GR', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'ku-IQ', 'nb-NO', 'nl-NL', 'nn-NO', 'pl-PL', 'pt-BR', 'pt-PT', 'ru-RU', 'sk-SK', 'sr-RS', 'sr-RS@latin', 'sv-SE', 'tr-TR', 'uk-UA']) and defaults to en-US. Locale list is validator.isAlphaLocales.

// 5. isAlphanumeric(str [, locale])	check if the string contains only letters and numbers.

// Locale is one of ['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'bg-BG', 'cs-CZ', 'da-DK', 'de-DE', 'el-GR', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'ku-IQ', 'nb-NO', 'nl-NL', 'nn-NO', 'pl-PL', 'pt-BR', 'pt-PT', 'ru-RU', 'sk-SK', 'sr-RS', 'sr-RS@latin', 'sv-SE', 'tr-TR', 'uk-UA']) and defaults to en-US. Locale list is validator.isAlphanumericLocales.


// 6. isAscii(str)	check if the string contains ASCII chars only.
// 7. isBase64(str)	check if a string is base64 encoded.
// 8. isBefore(str [, date])	check if the string is a date that's before the specified date.
// 9. isBoolean(str)	check if a string is a boolean.

// 10. isCreditCard(str)	check if the string is a credit card.
// 11. isCurrency(str)	check if the string is a valid currency amount.

// 12. isDataURI(str)	check if the string is a data uri format.
// 13. isMagnetURI(str)	check if the string is a magnet uri format.
// 14. isDecimal(str)	check if the string represents a decimal number, such as 0.1, .3, 1.1, 1.00003, 4.0, etc.

// 15. isDivisibleBy(str, number)	check if the string is a number that's divisible by another.
// 16. isEmail(str [, options])	check if the string is an email.

// 17. isEmpty(str)	check if the string has a length of zero.

// 18. isFullWidth(str)	check if the string contains any full-width chars.
// 19. isHalfWidth(str)	check if the string contains any half-width chars.
// 20. isHash(str, algorithm)	check if the string is a hash of type algorithm.

// Algorithm is one of ['md4', 'md5', 'sha1', 'sha256', 'sha384', 'sha512', 'ripemd128', 'ripemd160', 'tiger128', 'tiger160', 'tiger192', 'crc32', 'crc32b']

// 21. isHexColor(str)	check if the string is a hexadecimal color.
// 22. isIP(str [, version])	check if the string is an IP (version 4 or 6).
// 23. isIPRange(str)	check if the string is an IP Range(version 4 only).

// 24. isInt(str [, options])	check if the string is an integer.

// 25. isJSON(str)	check if the string is valid JSON (note: uses JSON.parse).
// 26. isJWT(str)	check if the string is valid JWT token.
// 27. isLatLong(str) check if the string is a valid latitude-longitude coordinate in the format lat,long or lat, long.

// 28. isLowercase(str)	check if the string is lowercase.
// 29. isMACAddress(str)	check if the string is a MAC address.

// 30. isMD5(str)	check if the string is a MD5 hash.
// 31. isMimeType(str)	check if the string matches to a valid MIME type format
// 32. isMobilePhone(str, locale)	check if the string is a mobile phone number,

// (locale is either an array of locales (e.g ['sk-SK', 'sr-RS']) OR one of ['ar-AE', 'ar-DZ', 'ar-EG', 'ar-IQ', ar-JO', 'ar-KW', 'ar-SA', 'ar-SY', 'ar-TN', 'be-BY', 'bg-BG', 'bn-BD', 'cs-CZ', 'de-DE', 'da-DK', 'el-GR', 'en-AU', 'en-CA', 'en-GB', 'en-HK', 'en-IN', 'en-KE', 'en-NG', 'en-NZ', 'en-RW', 'en-SG', 'en-UG', 'en-US', 'en-TZ', 'en-ZA', 'en-ZM', 'en-PK', 'es-ES', 'es-MX','et-EE', 'fa-IR', 'fi-FI', 'fr-FR', 'he-IL', 'hu-HU', 'it-IT', 'ja-JP', 'kk-KZ', 'ko-KR', 'lt-LT', 'ms-MY', 'nb-NO', 'nn-NO', 'pl-PL', 'pt-PT', 'pt-BR', 'ro-RO', 'ru-RU', 'sk-SK', 'sr-RS', 'sv-SE', 'th-TH', 'tr-TR', 'uk-UA', 'vi-VN', 'zh-CN', 'zh-HK', 'zh-TW'] OR defaults to 'any'. If 'any' or a falsey value is used, function will check if any of the locales match).

// 33. isPort(str)	check if the string is a valid port number.
// 34. isPostalCode(str, locale)	check if the string is a postal code,

// 35. isURL(str [, options])	check if the string is an URL.

// options is an object which defaults to { protocols: ['http','https','ftp'], require_tld: true, require_protocol: false, require_host: true, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false }.

// 36. isUppercase(str)	check if the string is uppercase.\

// 37. matches(str, pattern [, modifiers])	check if string matches the pattern.

// Either matches('foo', /foo/i) or matches('foo', 'foo', 'i').

export const validator = (rules, object) => {

  let keys = Object.keys(object),
      errors = '';

  for (let key in rules) {

    if(keys.indexOf(key) > -1){
      let value = object[key],
          splitedRule = rules[key].split('|'),
          [rule, params] = splitedRule,
          field = params ? validation[rule](value, params) : validation[rule](value);

      if(!field){
        errors += `<p>Поле ${key} заполнено не верно или пустое</p>`;
      }

    }

  }

  return errors || null;

}

export const nativeValidator = (method, tested) => {

  return validation && validation[method] && validation[method](tested);

}

export const pureValidator = () => {
  return validation;
}
