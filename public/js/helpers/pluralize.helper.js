import pluralize from "pluralize-ru"
export const pluralizeDays = (day) => {
 return pluralize(day, 'сегодня', '%d День', '%d Дня', '%d Дней');
}
export const pluralizeYear = (year) => {
    return pluralize(year, 'Лет', 'Год', 'Года', 'Лет');
}
export const pluralizeMonth = (month) => {
    return pluralize(month, 'Мeсяцев', 'Месяц', 'Мeсяца', 'Мeсяцев');
}
