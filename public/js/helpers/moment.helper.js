const sorting = (params, format) => {

    let date = {
        MM: params.getMonth() + 1,
        DD: params.getDate(),
        YYYY: params.getFullYear(),
        H: params.getHours(),
        M: params.getMinutes(),
        S: params.getSeconds()
    },
    digits = format;

    for(let key in date){

        let figure = String(date[key]);

        if(figure.length == 1){
           figure = '0' + figure; 
        }

        digits = digits.replace(key, figure);

    }

    return digits;
}


class moment { 

	static toInput(date, format = 'YYYY-MM-DD'){

		let datetime = date ? new Date(date) : new Date();

		return sorting(datetime, format);

	}

	static toReplaceDateToObject(object, key){

		for (let keys in object) {
			if(keys == key && object[key] instanceof Array){

				switch(object[key].length) {
					case 1:
						object[key] = new Date(object[key].join())
					case 2:
						object[key] = new Date(object[key].join('T'))
				} 

			}
		}

		return object;

	}	


}

export default moment;