// Configuration
import ratingConfuguration from '../configuration/rating.json'

export const setQueueLabel = (label, mainSkill) => {

    const { skill } = ratingConfuguration;

    if(label) {
        const mark = label.split('_')[1];
        return mark || skill;
    }

    if(mainSkill){
        return mainSkill;
    }

    return skill;

}
