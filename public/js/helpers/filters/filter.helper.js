import queryObject from '../queryObject.js'

export const mergeGeneralFilters = (filters = '', filtersObject) => {

    const currnetParsed = queryObject.parse(filters);

    let deliveredParsed = {},
        filterCurrent = null,
        filterNew = null;

    _.forOwn(filtersObject, (item) => {
        deliveredParsed = {...deliveredParsed, ...queryObject.parse(item)}
    })

    _.forOwn(currnetParsed, (item, key) => {
        if(item && item.isJson()) {
            currnetParsed[key] = JSON.parse(item);
        }
    })

    _.forOwn(deliveredParsed, (item, key) => {
        if(item && item.isJson()) {
            deliveredParsed[key] = JSON.parse(item);
        }
    })

    let mergedObject = currnetParsed;

    mergedObject.filters = deliveredParsed.filters;
    mergedObject.testResults = deliveredParsed.testResults;

    _.forOwn(mergedObject, (item, key) => {
        mergedObject[key] = JSON.stringify(item);
    })

    return queryObject.stringify(mergedObject)

}

export const getOneOf = (filters = '', name) => {

    const jsonFilters = queryObject.parse(filters),
          filtersObject = jsonFilters.filters || {};

    try {

        let parsed = JSON.parse(filtersObject);
        return parsed[name]

    } catch(e){
        return undefined;
    }

}


export const changeOneOf = (filters = '', obj) => {

    const jsonFilters = queryObject.parse(filters),
          newValues = {...jsonFilters},
          currentFilters = newValues.filters;

    try {

      let parsed = JSON.parse(currentFilters),
          newFilters = {...parsed, ...obj};

          return queryObject.stringify({
              ...jsonFilters,
              filters: JSON.stringify(newFilters)
          })


    } catch(e){
        return queryObject.stringify({
            ...jsonFilters,
            filters: JSON.stringify(obj)
        })
    }

}

export const deteleOneOf = (filters = '', name) => {

    const jsonFilters = queryObject.parse(filters);

    delete jsonFilters[name];

    return queryObject.stringify({
        ...jsonFilters
    });

}

export const normalizeFilterToSending = (filter) => {
    const filtersNormalized = {},
          rules = {
            age: 'range',
            daysLeft: 'range',
            gender: 'equally',
            languages: 'languages',
            experience: 'empty',
            education: 'empty',
            experienceIt: 'range',
            level: 'in',
            location: 'location',
            occupation: 'in',
            applicantId: 'in',
            opens: 'range',
            sallary: 'range',
            skill: 'all',
            technology: 'range',
          },
          methods = {
            range(value, key) {
                filtersNormalized[key] = {'>=' : value.min, '<=': value.max}
            },
            equally(value, key){
                filtersNormalized[key] = value;
            },
            empty(value, key){
                filtersNormalized[key] = {notEmpty:value}
            },
            in(value, key){
                filtersNormalized[key] = {in: value}
            },
            all(value, key) {
                filtersNormalized[key] = {all: value};
            },
            location(value, key){

                const { cities, relocation: {checked: {local: checkedLocal, abroad: checkedAbroad} = {}, local} = {} } = value;

                if(cities && cities.length) {
                    filtersNormalized['currentLocation'] = {in: cities}
                }

                if(checkedLocal && local && local.length){
                    filtersNormalized['relocationTo'] = {in: local}
                }

                if(checkedAbroad) {
                    filtersNormalized['relocation'] = true
                }

            },
            languages(value, key) {

                const languages = {};

                _.forEach(value, (item, i) => {
                    languages[item.label] = {'=': item.data};
                })

                filtersNormalized['languages'] = languages;
            }
          };

        _.forOwn(filter, (value, key) => {

            const rule = rules[key] || null,
                  method = rule && methods[rule] ? methods[rule] : null;

            if(rule && method){

                method(value, key)

            }

        })

        return queryObject.stringify({
            filters: JSON.stringify(filtersNormalized)
        })

}

export const destuctingFiltersToComponent = (filters) => {

    const jsonFilters = queryObject.parse(filters);

    let filtersCurrent = jsonFilters && jsonFilters.filters ? jsonFilters.filters : '',
        parsedCurrentFilters = filtersCurrent.isJson() ? JSON.parse(filtersCurrent) : null,
        filterNormalized = {},
        rules = {
            age: 'range',
            currentLocation: 'location',
            relocation: 'location',
            relocationTo: 'location',
            daysLeft: 'range',
            education: 'empty',
            experience: 'empty',
            experienceIt: 'range',
            gender: 'equally',
            level: 'in',
            occupation: 'in',
            opens: 'range',
            sallary: 'range',
            technology: 'range',
            skill: 'all',
            languages: 'languages'
        },
        methods = {
            range(value, key) {
                filterNormalized[key] = {
                    min: value['>='],
                    max: value['<=']
                }
            },
            location(value, key) {

                if(key == 'relocation') {
                    _.set(filterNormalized, 'location.relocation.checked.abroad', true);
                }

                if(key == 'relocationTo') {
                    _.set(filterNormalized, 'location.relocation.local', value['in']);
                    _.set(filterNormalized, 'location.relocation.checked.local', true);
                }

                if(key == 'currentLocation') {
                    _.set(filterNormalized, 'location.cities', value['in']);
                }

            },
            empty(value, key) {
                var k = filterNormalized[key] = value['notEmpty'];
                filterNormalized[key] = value['notEmpty'];
            },
            equally(value, key) {
                filterNormalized[key] = value;
            },
            in(value, key) {
                filterNormalized[key] = value['in'];
            },
            all(value, key) {
                filterNormalized[key] = value['all'];
            },
            languages(value, key) {
                const languages = [];

                _.forOwn(value, (item, key) => {
                    languages.push({
                        label: key,
                        data: item['=']
                    })
                })

                filterNormalized[key] = languages;
            }
        };

        if(parsedCurrentFilters){
            _.forOwn(parsedCurrentFilters, (value, key) => {

                const rule = rules[key] || null,
                      method = rule && methods[rule] ? methods[rule] : null;

                if(rule && method){
                    method(value, key)

                }

            })
        }

        return filterNormalized;


}


export const prepare = (filters) => {
    try {

        for(let field in filters)
        {
            if(!filters.hasOwnProperty(field))
            {
                continue;
            }

            if(Array.isArray(filters[field]))
            {
                filters[field] = {
                    'in': filters[field]
                }
            }
            else if(typeof filters[field] === "object" && typeof filters[field].min !== "undefined")
            {
                filters[field] = {
                    '>=': filters[field].min,
                    '<=': filters[field].max
                }
            }
        }

        return filters;


    } catch(e){
        return filters;
    }


}
