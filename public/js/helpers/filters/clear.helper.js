import queryObject from '../queryObject.js'

export const clearAll = (filters = '', value) => {

    const jsonFilters = queryObject.parse(filters),
          newObject = {...jsonFilters};

    delete newObject['filters'];
    delete newObject['testResults'];

    return queryObject.stringify({
        ...newObject
    })

}
