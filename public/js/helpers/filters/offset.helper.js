import queryObject from '../queryObject.js'

export const normalizeOffset = (filters = '') => {

    const jsonFilters = queryObject.parse(filters),
          offsetValue = +jsonFilters.offset || 0,
          limitValue = +jsonFilters.limit || 10;

    return Math.ceil((offsetValue + limitValue)  / limitValue);

}

export const normalizeOffsetWithPageLimit = (filters = '', pageNumber) => {
   
    const jsonFilters = queryObject.parse(filters),
          limitValue = jsonFilters.limit || 10;

    return Math.ceil(pageNumber * limitValue);

}

export const mergeOffsetWithFilters = (filters = '', value) => {

    const jsonFilters = queryObject.parse(filters),
          offsetValue = jsonFilters.offset,
          limitValue = jsonFilters.limit;
    
    if(offsetValue == value) return null;

    return queryObject.stringify({
        ...jsonFilters,
        offset: value
    })

}

export const denormalizePages = (filters = '', count = 0) => {

    const jsonFilters = queryObject.parse(filters),
          limit = jsonFilters.limit || 10;

    return Math.ceil(count / limit);

}

export const convertOffsetToValue = (filters) => {

    if(!filters) return '';

    const jsonFilters = queryObject.parse(filters),
          offsetValue = jsonFilters.offset;

    return offsetValue || ''

}