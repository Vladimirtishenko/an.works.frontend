import queryObject from '../queryObject.js'

export const ordering = (value) => {
    switch(value){
        case 'ask':
            return 'desc'
        case 'desc':
            return 'ask'
        default:
            return 'desc'
    }

}

export const orderingByClass = (value) => {
    switch(value){
        case 'ask':
            return 'ask'
        case 'desc':
            return 'desc'
        default:
            return 'ask'
    }

}


export const orderBy = (filters) => {

    if(!filters)
        return {};

    const jsonFilters = queryObject.parse(filters);

    if(jsonFilters && jsonFilters.orderBy){
        try{
            return JSON.parse(jsonFilters.orderBy);
        } catch(e){
            return {}
        }

    }

    return {}

}

export const normalizeObderBy = (filters = '', name, value) => {

    const jsonFilters = queryObject.parse(filters),
          orderByObject = jsonFilters.orderBy;


    let jsonByOrderBy = null;

    try {
        // jsonByOrderBy = JSON.parse(orderByObject);

        jsonByOrderBy[name] = ordering(value)

        return queryObject.stringify({
            ...jsonFilters,
            orderBy: JSON.stringify(jsonByOrderBy)
        })

    } catch(e){

        return queryObject.stringify({
            ...jsonFilters,
            orderBy: JSON.stringify({ [name]: ordering(value) })
        })

    }

}
