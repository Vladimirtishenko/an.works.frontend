import queryObject from '../queryObject.js'

export const normalizeTestingToSending = (filter) => {

    const testsNormalized = {},
          tests = filter && filter.testing;

    if(tests && tests.length && Array.isArray(tests)){
        _.forEach(tests, (item, i) => {
            const { label, data: { min, max } } = item;
            testsNormalized[label] = {'>=' :min, '<=': max};

        })
    }

    return testsNormalized && queryObject.stringify({
        testResults: JSON.stringify(testsNormalized)
    }) || ''

}

export const destuctingTestsToComponent = (filter) => {
    const currnetParsed = queryObject.parse(filter),
          tests = currnetParsed.testResults || '',
          parsedTest = tests.isJson() ? JSON.parse(tests) : null,
          normalized = [];

      if(parsedTest && Object.keys(parsedTest).length){
          _.forOwn(parsedTest, (value, key) => {
              normalized.push({
                  label: key,
                  data: {
                      min: value['>='],
                      max: value['<=']
                  }
              })
          })
      }

      return {testing: normalized};

}
