import queryObject from '../queryObject.js'

export const normalizeLimit = (filters = '', value) => {

    const jsonFilters = queryObject.parse(filters),
          limitValue = jsonFilters.limit;

    delete jsonFilters['offset'];

    if(limitValue == value) return null;

    return queryObject.stringify({
        ...jsonFilters,
        limit: value
    })

}

export const convertLimitToValue = (filters) => {

    if(!filters) return 10;

    const jsonFilters = queryObject.parse(filters),
          limitValue = jsonFilters.limit;

    return limitValue || 10

}