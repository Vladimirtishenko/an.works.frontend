export default function typeProvide (type, value) {

	switch (type) {

		case 'array': 

			return [value];

			break;

		case 'string': 

			return String(value)

			break;

		case 'object':

			return {...value}

			break;

		case 'number':

			return +value

			break;

		default:

			return value

			break;
	}	

}