import staffLIst from "../databases/general/numberOfStaff.json";

export function normalizeStaff(staff) {
    let item = {};

        item = _.find(staffLIst, {label: Number(staff)});
        if(item){
            return item.desc;  
        }else{
            return null
        }
}