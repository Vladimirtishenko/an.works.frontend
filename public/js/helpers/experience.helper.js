export const experienceITcounter = (experience) => {

    if(!experience || !Array.isArray(experience)) return "";

    let years = 0;

    const YEAR = 365;
    const MONTH = 30.417;

    for (var i = 0; i < experience.length; i++) {

        let item = experience[i];

        if(item.type !== 'it') continue;

        let start = item.date && item.date.start,
            end = item.date && item.date.end || item.date && item.date.now,
            startToMilliseconds = (new Date(start.join('/'))).valueOf(),
            endToMilliseconds = Array.isArray(end) ? (new Date(end.join('/'))).valueOf() : (new Date()).valueOf(),
            subStract = (endToMilliseconds - startToMilliseconds) / 8.64e+7; // To days

            years = years + subStract;

    }

    let fullYear = parseInt(years / YEAR),
        leftFromYear = years % YEAR,
        fullMounth = parseInt(leftFromYear / MONTH);

    return {year: fullYear, month: fullMounth};


}
