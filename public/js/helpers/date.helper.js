import Moment from 'moment';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);

export const separateDate = (date) => {

    let staticParams = {
        day: undefined,
        month: undefined,
        year: undefined
    };

    if(date){

        let realDate = date && isNaN(+date) ? new Date(date) : date && !isNaN(+date) ? new Date(+date) : null;

        if(!realDate) return staticParams;

        staticParams.day = realDate.getDate();
        staticParams.month = realDate.getMonth() + 1;
        staticParams.year = realDate.getFullYear();

        return staticParams;

    } else {
        return staticParams;
    }

}

export const dmyDate = (date) =>{
    return moment(date).format("DD.MM.YYYY");
}
export const dmyDateFull = (date) =>{
    return moment(date).format("DD.MM.YYYY h:mm:ss");
}

export const preparationMonth = (month) => {

    let obj = [];

    _.forIn(month, (item, i) => {
        obj.push({name: item, label: i});
    })

    return obj;

}

export const getTextMonth = (label, month) => {

    let m = '',
        mIndex = _.findIndex(month, function(item) { return item.label == label; }),
        realMonth = mIndex > -1 ? month[mIndex] : null;

    return realMonth && realMonth.name || '';

}

export const createDatebyFormatMonthYear = (date, format) => {

    const momentDate = moment(date.join('-'), 'MM-YYYY').locale('ru'),
          stringDate = momentDate && momentDate.isValid() ? momentDate.format(format) : ''

    return stringDate;

}

export const createDate = (date, format, translation) => {

    if(!Array.isArray(date)) return null;

    let newDateArray = date.clone(),
        {month} = translation,
        staticParams = {
            DD: newDateArray.shift(),
            MM: month[newDateArray.shift()],
            YYYY: newDateArray.shift()
        };

        for(let key in staticParams){

            let figure = String(staticParams[key]);

            format = format.replace(key, figure);

        }

        return format;

}

export const normalizeToFromTo = (date) => {

    const [month, year] = date;

    return {
        month: month ? parseInt(month) : undefined,
        year: year ? parseInt(year): undefined
    }

}

export const transformToTotal = (items) => {

    let month = 0,
        separate = {};

    _.forEach(items, (item) => {

        const { type, date: {end: endDate = [], start: startDate = [], now: nowDate = ''} = {} } = item;

        const   start = moment(startDate.join('-'), 'MM-YYYY'),
                end = nowDate ? moment() : moment(endDate.join('-'), 'MM-YYYY'),
                range = moment().range(start, end),
                period = range.diff("month");

        month += period

        if(!separate[type]){
            separate[type] = 0;
        }

        separate[type] += period;

    })

    return {common: month, ...separate}

}

export const transformToNdaysAgo = (end, days, format) => {

    const to = moment(end).format(format),
          from = moment(end).subtract(days, 'days').format(format);

    return {from, to};

}

export const transformToDays = (end, day) => {
    const   from = moment(),
            to = end ? moment(end) : moment().add(1, 'month'),
            range = moment().range(from, to),
            period = range.diff("days",true);
            
    return Math.ceil(period);

}

export const transformToYear = (start) => {

    const   to = moment(),
            from = start ? moment(start) : moment().add(1, 'year'),
            range = moment().range(from, to),
            period = range.diff("year");

    return period;

}

export const transformToStringValue = (start = {}, end = {}) => {


    if(start.month && start.year && end.year && end.month) {
        return `${start.month}/${start.year}-${end.month}/${end.year}`
    }

    return null;

}

export const transformToStringValueNow = (start = {}, now = {}) => {


    if(start.month && start.year && now.value) {
        const now = moment().format('M/YYYY');
        return `${start.month}/${start.year}-${now}`
    }

    return null;

}

export const transformToRange = (label) => {

    let range = {
        from: '',
        to: ''
    },
    from,
    to;

    switch(label){
        case 'currentMonth':
            from = moment().startOf('month').format('YYYY-MM-DD');
            to = moment().endOf('month').format('YYYY-MM-DDT23:59:59');

            range = {">=": from, "<=": to};
            break;

        case 'previousMonth':
            from = moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD');
            to = moment().subtract(1,'months').endOf('month').format('YYYY-MM-DDT23:59:59');

            range =  {">=": from, "<=": to};
            break;

        case 'currentQuarter':
            from = moment().quarter(moment().quarter()).startOf('quarter').format('YYYY-MM-DD');
            to = moment().quarter(moment().quarter()).endOf('quarter').format('YYYY-MM-DDT23:59:59');

            range =  {">=": from, "<=": to};
            break;

        case 'currentYear':
            from = moment().startOf('year').format('YYYY-MM-DD');
            to = moment().endOf('year').format('YYYY-MM-DDT23:59:59');

            range =  {">=": from, "<=": to};
            break;

        case 'previousTwoYears':
            from = moment().subtract(2,'years').startOf('year').format('YYYY-MM-DD');
            to = moment().subtract(1,'years').endOf('year').format('YYYY-MM-DDT23:59:59');

            range =  {">=": from, "<=": to};
            break;

    }

    return range;

}

export const valueOfFormatForSorting = (date = (new Date), format = '') => {
    return moment(date, format).valueOf();
}
