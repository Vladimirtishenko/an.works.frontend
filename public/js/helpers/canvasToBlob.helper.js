export default function serializeCanvasToJson (form) {

	let canvas = form.querySelectorAll('canvas'),
		array = [],
		form_arr = new FormData();

	if(!canvas) return false;

	for (let j = 0; j < canvas.length; j++) {
		array.push(canvas[j].toDataURL());
	}	

	const dataURIToBlob = (dataURI) => {

        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type:mimeString});
	}

	if(!array.length){return null;}

	for (let i = 0; i < array.length; i++) {
		let blob = dataURIToBlob(array[i]);
		form_arr.append('upload_'+i+'_'+random(), blob);
	}


	return form_arr;

}