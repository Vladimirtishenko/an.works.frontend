import genderVocabulary from '../databases/general/gender.json' 

export const comparingGenderValue = (value, className, order) => {
    const dictionary = [...genderVocabulary];

          const newValue = [...dictionary].filter((item, i) => {

              if(item.value == value) {
                  item.checked = true;
              } else {
                  item.checked = false;
              }

              if(order !== undefined && order === i && className){
                  item.innerWrapperClass = className;
              }

              if(order === undefined || order !== i && className) {
                item.innerWrapperClass = className;
              }

              return item;

          })

    return newValue;
}

export const  getGenderStaticName = (gender) => {

    let genderDescription = gender && _.findKey(genderVocabulary, {value: gender}),
        getList = genderDescription > -1 ? genderVocabulary[genderDescription] : null;

    if(getList && getList.sub){
        return getList.sub;
    }
};