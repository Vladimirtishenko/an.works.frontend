export const mapperSkills = (profile, knowledges, returned, filter = [], method, appended) => {

    let {skills} = profile || null,
        data = returned;

    if(!skills || !knowledges) return returned;

    skills.forEach((item, i) => {

        let flt = Array.isArray(filter) ? _.assign(...filter) : _.assign([...filter]),
            key = _.findIndex(knowledges, { 'label': item.label, ...flt } ),
            obj = knowledges[key],
            extend = appended ? {...obj, ...item} : {...obj};

            if(!obj) return;

            switch(method) {
                case 'array':
                    data.push(extend);
                    break;
                case 'string':
                    data = obj.label;
                    break;
            }

    })

    return data;
}

export const extendsSingleSkill = (label, knowledges) => {

    let key = _.findIndex(knowledges, { 'label': label} ),
        item = key > -1 ? knowledges[key] : {};

    return item;

}

export const transformDataToArray = (value) => {

    let array = [];

    if(!value) return array;

    value.forEach((item, i) => {
        array.push(item.label)
    })

    return array;

}

export const extendsSkillPloainArrayToExtends = (knowledges, skills) => {

    let data = [];

    if(!skills || !knowledges) return data;

    skills.forEach((item, i) => {

        let key = _.findIndex(knowledges, { 'label': item.label || item } ),
            obj = knowledges[key];

            if(!obj) return;

            data.push(obj);

    })

    return data;


}

export const mapperSkillsSorting = (knowledges, filter) => {

    let data = [];

    if(!knowledges) return data;

    knowledges.forEach((item, i) => {

        filter.forEach((filterItem, j) => {

            const key = _.head(_.keys(filterItem)) || null;

            if(!key || typeof key !== 'string') return;

            const valueInKnowledges = item[key],
                  valueInFilter = filterItem[key];

            if(valueInKnowledges == valueInFilter){
                data.push(item);
            }


        })

    })

    return data;
}


export const mapperSkillsLabel = (skills, knowledges, filter) => {

    let data = [];

    if(!skills || !knowledges) return data;

    skills.forEach((item, i) => {

        filter.forEach((filterItem, j) => {

            let key = _.findIndex(knowledges, { 'label': item.label || item, ...filterItem} ),
                obj = knowledges[key],
                extend = {...obj, ...item};

                if(!obj) return;

                data.push(extend);

        })

    })

    return data;
}

export const  mapperNameMainSckill = (knowledges , mainSkill) =>{

    if(!mainSkill || !knowledges) return '';

    let mainScills = knowledges.filter((item, i) => {
        if (item.isMain) {
            return item;
        }
    }),
    find = _.find( mainScills, {label: mainSkill} );
    if(find){
        return find.name;
    }else{
        return '';
    }
}

export const skillsComparing = (serializedForm, knowledges) => {

    let skills = [],
        mainSkillSingle = null;

    _.forOwn(serializedForm, (all, i) => {

        if(i == 'language'){

            _.forOwn(all, (single) => {

                let item = single,
                    rightIndex = _.findIndex(knowledges, { 'label': item.label} ),
                    obj = knowledges[rightIndex];

                skills.push({...obj, value: item.level})

            })

        }

        if(i == 'skills') {

            let {mainSkill, scope} = all;

            mainSkillSingle = mainSkill;

            scope && scope.split(',') && scope.split(',').forEach((item) => {

                let rightIndexForScope = _.findIndex(knowledges, { 'label': item} ),
                    obj = knowledges[rightIndexForScope];

                obj && skills.push({...obj})

            })

        }

    })

    return {mainSkill: mainSkillSingle, skills: skills};

}

export const skillsFromExperienceMapper = (mainSkill, skill) => {

    let skillsList = [],
        skillsString = skill && skill.split(',') || [],
        skilsMain = [mainSkill],
        mergeSkills = _.unionWith(skilsMain, skillsString, _.isEqual);

        _.forEach(mergeSkills, (item, i) => {
            skillsList.push({label: item})
        })

        return skillsList;
}

export const labelsToDefaults = (obj, defaults) => {

    const merged = {}

    _.forOwn(obj, (item, key) => {
        if(!item && defaults[key]){
            merged[key] = defaults[key]
        } else {
            merged[key] = item;
        }
    })

    return merged;

}
