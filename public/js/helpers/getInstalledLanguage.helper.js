import rules from '../configuration/rules.json';

/**
 * Return language key after check Local Storage, Navigator
 *
 * @return {string}
 */
export const getInstalledLanguage = (language) => {

  let verifiedLanguage = language,
      localStorageLanguage = localStorage.getItem('defaultLanguage'),
      navigationLanguage = navigator.language,
      indexLanguage = navigationLanguage && navigationLanguage.indexOf('-'),
      defaultLanguage = rules.defaultLanguage,
      availableLanguage = rules.availableLanguage;

  /**
   * Check language avability in Function arguments
   */
  if(verifiedLanguage && availableLanguage.indexOf(verifiedLanguage) > -1){
    return verifiedLanguage;
  }

  /**
   * Check language avability in Local Storage
   */
  if(localStorageLanguage && availableLanguage.indexOf(localStorageLanguage) > -1) {
    return localStorageLanguage;
  }

/**
 * Check language avability in Navigator
 */
  if(navigationLanguage){
    let key = indexLanguage > -1 ? navigationLanguage.slice(0, indexLanguage) : navigationLanguage;
    return availableLanguage.indexOf(key) > -1 ? key : defaultLanguage;
  }

  return defaultLanguage;

}
