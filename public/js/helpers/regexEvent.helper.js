export const checkUrl = (url, id, subId = null) => {

	const regexp = /url\[(.*?)\]/;

	let [ ,path] = url.match(regexp) || [];
	path =  path && path.replace(':id', id);
	if(subId)
	{
        path = path.replace(':subId', subId)
	}
	return path || '#';

}


export const checkEvent = (events, fn) => {

	const regexp = /action\[(.*?)\]\[(.*?)\]/;

	let [ ,event, handler] = events.match(regexp) || [],
		eventObject = {};

	if(event && handler) {
		eventObject[event] = fn[handler];
	}

	return eventObject;

}