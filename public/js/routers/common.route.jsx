import React from 'react'
import {IncludedProfile} from '../modules/profile/profile.poor.jsx'
import {IncludedAdmin} from '../modules/admin/admin.poor.jsx'

export const CombineRoutes = (props) => {
    const { oauth: { user: { role = '' } = {} } = {} } = props;
    switch (role) {
        case 'root':
            return <IncludedAdmin {...props} />;
        default:
            return <IncludedProfile {...props} />;
    }
}
