import React from 'react'

import { withRouter, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import {CombineRoutes} from '../routers/common.route.jsx'

import oauth from '../modules/oauth/decorators/oauth.dec.jsx';
import i18n from '../modules/i18n/decorators/i18n.dec.jsx';

function mapStateToProps(state) {
    return {
    	...state
    }
}

@withRouter
@connect(mapStateToProps)
@i18n
@oauth 
export default class Main extends React.Component {

	constructor(props){
		super(props);
	}

	render() {
		return (
            <CombineRoutes {...this.props} />
        ) 

	}

}
