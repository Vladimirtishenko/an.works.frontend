
class CircleProgressBar {

    static defaultProps = {
        colors: ['#9400D3', '#4B0082', '#0000FF'],
        x: null,
        y: null,
        radius: 120,
        lineWidth: 15,
        trackWidth: 10,
        frameInterval: 10,
        frameStep: 0.1,
        startPosition: -(Math.PI / 2),
        lineCap: 'round',
        trackLineColor: '#eee',
        maxColorsCountByValue: {
            0.05: 2,
            0.2: 2,
            0.3: 3,
            0.4: 4,
            0.6: 5
        }
    }

    constructor(canvas, options){

        try {
            this._value = 0;
            this._canvas = canvas;
            this._context = canvas.getContext('2d');
            this._currentPartLength = 0.1;
            this._colorsCount = null;

            this._options = {...CircleProgressBar.defaultProps, ...options};

            this._context.lineCap = this._options.lineCap;

            if (!this._options.x) {
                this._options.x = this._canvas.width / 2;
            }

            if (!this._options.y) {
                this._options.y = this._canvas.height / 2;
            }

            this.sendEvent('init', this._options);
        } catch (e) {

        }

    }

    /**
     * Get current value
     * @returns {number}
     */
    getValue () {
        return this._value;
    };

    /**
     * Set value
     * @param value
     */
    setValue (value) {
        value = parseFloat(value);
        if (isNaN(value) || value < 0) {
            value = 0;
        } else if (value > 1) {
            value = 1;
        }

        this._value = value;

        // render value
        this.draw();
    };

    /**
     * Set current arc length
     * @param {number} value
     * @returns {boolean}
     */
    setCurrentPartLength (value)
    {
        value = parseFloat(value);
        if (isNaN(value)) {
            return false;
        }
        this._currentPartLength = value;
        return true;
    };

    /**
     * Send event
     * @param {string} eventName
     * @param {Object} data
     */
    sendEvent (eventName, data)
    {
        try {
            let event = new CustomEvent('circleProgressBar.' + eventName, {detail: data});
            this._canvas.dispatchEvent(event);
        } catch (e) {

        }
    };

    /**
     * Getting gradient colors
     * @returns {Array}
     */
    getColors () {

        let colors = this.clone(this._options.colors),
            value = this.getValue();
        // for work properly colors count must be more 2
        // one color
        if (!this.isArray(colors)) {
            colors = [colors];
        }

        // cut spare colors
        let colorsCountByValue = this._options.maxColorsCountByValue;
        let maxColors = null;
        if (colorsCountByValue) {
            Object.keys(colorsCountByValue).map(function(objectKey, index) {
                let maxColorsValue = colorsCountByValue[objectKey];
                if (!maxColors && value <= objectKey) {
                    maxColors = maxColorsValue;
                }
            });
        }

        if (maxColors > 0) {
            colors = this.cutArrayFromCenter(colors, maxColors);
        }

        // 1 colors - adding 1 copies
        if (colors.length === 1) {
            colors.push(colors[0]);
        }

        // 2 colors and value == 1 - adding 2 copies
        if (colors.length === 2 && value == 1) {
            colors.push(colors[1]);
            colors.unshift(colors[0]);
        }

        return colors;
    };

    /**
     * Draw progress bar for current value and options
     */
    draw () {

        let context = this._context,
            startColor = null,
            endColor = null,
            colors = this.getColors(),
            currentPartLength = this._currentPartLength,
            value = this.getValue();

        if (this._colorsCount > 0) {
            currentPartLength = currentPartLength * (this._colorsCount / colors.length);
        }

        this._colorsCount = colors.length;
        let partLength = (((2 * Math.PI) / (colors.length - 1))) * value;

        if (partLength <= 0) {
            partLength = 0.03;
        }

        let partLengthStep = this._options.frameStep / colors.length,
            lineWidth = this._options.lineWidth,
            trackWidth = this._options.trackWidth,
            trackLineColor = this._options.trackLineColor,
            startPosition = this._options.startPosition,
            gradient = null,
            x = this._options.x,
            y = this._options.y,
            radius = this._options.radius;


        let acrInterval = setInterval (() => {
            let start = startPosition;

            if (currentPartLength === partLength) {
                clearInterval(acrInterval);
                this.sendEvent('afterDraw', {
                    this: this
                });
                return false;
            }

            let partLengthDelta = Math.abs(currentPartLength - partLength);
            if (partLengthDelta < partLengthStep) {
                partLengthStep = partLengthDelta;
            }

            if (currentPartLength > partLength) {
                partLengthStep = -1 * Math.abs(partLengthStep);
            } else {
                partLengthStep = Math.abs(partLengthStep);
            }

            currentPartLength += partLengthStep;

            this.clear();

            if (trackLineColor) {
                this.drawArc(x, y, radius, (Math.PI / 180) * 270, (Math.PI / 180) * (270 + 360), trackLineColor, trackWidth);
            }

            for (let i = 0; i < colors.length - 1; i++) {
                startColor = colors[i];
                endColor = colors[(i + 1)];

                // x start / end of the next arc to draw
                let xStart = x + Math.cos(start) * radius;
                let xEnd = x + Math.cos(start + currentPartLength) * radius;
                // y start / end of the next arc to draw
                let yStart = y + Math.sin(start) * radius;
                let yEnd = y + Math.sin(start + currentPartLength) * radius;

                gradient = context.createLinearGradient(xStart, yStart, xEnd, yEnd);
                gradient.addColorStop(0, startColor);
                gradient.addColorStop(1, endColor);

                this.drawArc(x, y, radius, start, start + currentPartLength, gradient, lineWidth);

                start += currentPartLength;
            }

            this.setCurrentPartLength(currentPartLength);

            this.sendEvent('afterFrameDraw', {
                this: this,
                progress: currentPartLength / partLength
            });

        }, this._options.frameInterval);
    };

    /**
     * Draw arc
     * @param {number} x
     * @param {number} y
     * @param {number} radius
     * @param {number} start
     * @param {number} end
     * @param strokeStyle
     * @param {number} lineWidth
     * @param {number} trackWidth
     */
     drawArc (x, y, radius, start, end, strokeStyle, lineWidth, trackWidth, process, value) {
        this._context.beginPath();
        this._context.strokeStyle = strokeStyle;
        this._context.arc(x, y, radius, start, end);
        this._context.lineWidth = lineWidth;
        this._context.trackWidth = trackWidth;
        this._context.stroke();
        this._context.closePath();

    };

    /**
     * Clear rect with progress bar
     */
    clear () {
        let outerRadius = this._options.radius + this._options.lineWidth;
        let x = this._options.x,
            y = this._options.y;
        this._context.clearRect(x - outerRadius, y - outerRadius, x + outerRadius, y + outerRadius);
    };

    /**
     * Check param fo array
     * @param obj
     * @returns {boolean}
     */
    isArray (obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    };

    /**
     * Check param for object
     * @param obj
     * @returns {boolean}
     */
    isObject (obj) {
        if (this.isArray(obj)) {
            return false;
        }

        let type = typeof obj;
        return type === 'object' && !!obj;
    };

    /**
     * Cut array from center
     * @param {Array} array
     * @param {int} maxLength
     * @returns {Array}
     */
    cutArrayFromCenter (array, maxLength) {
        if (!this.isArray(array)) {
            return false;
        }
        let length = array.length;
        if (length <= maxLength) {
            return array;
        }

        let delta = length - maxLength;
        let result = [];
        array.forEach(function(element, index, array) {
            if (index == 0 || delta < index) {
                result.push(element);
            }
        });

        return result;
    };

    /**
     * Clone object
     * @param obj
     */
    clone (obj) {
        return JSON.parse(JSON.stringify(obj));
    };

};

export default CircleProgressBar;
