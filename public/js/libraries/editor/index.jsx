import React from 'react'

import configuration from './configuration/config.editor.json'
import upload from './actions/upload.js'
import tinymce from 'tinymce';

import EditorComponent from './components/editor.jsx';

import 'tinymce/themes/modern';
import 'tinymce/skins/lightgray/skin.min.css'
import 'tinymce/skins/lightgray/content.min.css'

import 'tinymce/plugins/paste/plugin'
import 'tinymce/plugins/link/plugin'
import 'tinymce/plugins/image/plugin'
import 'tinymce/plugins/autoresize/plugin'
import 'tinymce/plugins/code/plugin'
import 'tinymce/plugins/lists/plugin'
import 'tinymce/plugins/insertdatetime/plugin'
import 'tinymce/plugins/print/plugin'
import 'tinymce/plugins/hr/plugin'
import 'tinymce/plugins/table/plugin'
import 'tinymce/plugins/template/plugin'
import 'tinymce/plugins/textcolor/plugin'
import 'tinymce/plugins/wordcount/plugin'

class Editor extends React.Component {

	constructor(props){
		super(props);

		this.state = {
			content: this.props.value || "",
			keys: random(),
		};
		this.editor = null;
	}

	getContent()
	{
		return this.state.content;
	}

    setContent(content)
    {
        this.editor.setContent(content, {format: 'raw'});
        this.handleEditorChange(this.editor.getContent());
    }

	handleEditorChange(content) {
	    this.setState({ content: content });
	}

	componentDidMount(){

		tinymce.init({
		  ...configuration,
	      selector: '.'+this.props.uniq,
	      images_upload_handler: upload.bind(null, this.props.uploadPath),
	      setup: editor => {
		  	this.editor = editor;
	        this.setState({ editor });
	        editor.on('keyup change', () => {
	          	this.handleEditorChange(editor.getContent());
	        });
	      }
	    });
	}

	render(){
	 	return (<EditorComponent {...this.props} {...this.state} />)
	}

}

export default Editor;