import React from 'react'

class EditorComponent extends React.Component {

	render(){

		let {name, uniq, className, label, content, keys} = this.props;

		return (
			<div>
	          <div className={className}>
	              <div className="">
	                  <label className="">{label}</label>
	                  <textarea className={uniq} value={content} key={keys} name={name} readOnly>{content}</textarea>
	              </div>
	          </div>
	          <div className="clear"></div>
	        </div>
		)
	}

}

export default EditorComponent;