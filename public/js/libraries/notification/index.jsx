import React from 'react'

import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as notificationSetter from './action/notification.action.js'

import Notification from './components/notification.jsx'
import Modal from '../../components/widgets/modal.jsx'

const E = 'error';
const S = 'success';

function mapStateToProps (state) {
  return {...state.notification}
}

function mapDispatchToProps(dispatch){
	return {
		actions: bindActionCreators({
			...notificationSetter
		}, dispatch),
		dispatch
	}
}

@connect(mapStateToProps, mapDispatchToProps)
export default class EventHandling extends React.Component {

	constructor(props){
		super(props);
	}

	render(){

		let {onError, onSuccess, notification} = this.props;

		if(!notification) return null;

		let	{type, message, view} = notification,
			ErrorComponent = onError && onError.component ? onError.component : Notification,
			SuccessComponent = onSuccess && onSuccess.component && onSuccess.component.el ? onSuccess.component : null;

		if(!type || !message || !view) return null;

		return (

			<div>
				{
					type == E ? 
					<ErrorComponent {...this.props} /> :
					type == S && SuccessComponent ?
					<div>
						<ErrorComponent {...this.props} />
						<Modal><SuccessComponent.el {...SuccessComponent.props} /></Modal>
					</div> : 
					type == S ? 
					<div>
						<ErrorComponent {...this.props} />
					</div> : null
				}
			</div>

			
		)

	}
}

EventHandling.propTypes = { 
  onError: PropTypes.object.isRequired, 
  onSuccess: PropTypes.object.isRequired 
}; 