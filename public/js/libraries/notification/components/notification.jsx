import React from 'react';
import { oneOfType, string, object } from 'prop-types';

class Notification extends React.Component {
	static propTypes = {
		notification: oneOfType([object]).isRequired
	}

	constructor(props) {
		super(props);
		this.__refs = null;
		this.handleClickOutside = ::this.handleClickOutside;
	}

	createMarkup(message) {
	  return {__html: message};
	}

	autoHintClose(){
		try {
			clearTimeout(this.timer);
		} catch(e){
			console.warn(e)
		}

		this.timer = setTimeout(() => {
			this.props.actions.notificationSetter(null)
		}, 5000)
	}

	componentDidMount() {
		document.addEventListener('mousedown', this.handleClickOutside);
		this.autoHintClose();
	}

	componentWillUnmount() {
		clearTimeout(this.timer);
		document.removeEventListener('mousedown', this.handleClickOutside);
	}

	handleClickOutside(event) {
		if (this.__refs && !this.__refs.contains(event.target)) {
			this.close();
		}
	}

	close() {
		const { actions: { notificationSetter } } = this.props;
		notificationSetter(null);
	}

    render() {

		const { notification: { type, message } } = this.props,
				ico = type == 'error' ? 'bug_report' : 'done_all';

		return (
			<div>
	            <div ref={(refs) => { this.__refs = refs; }} className={`notification fl fl--align-c notification--${type}`}>
	                <i className="notification__icon"></i>
	                <button type="button" aria-hidden="true" onClick={::this.close} className="notification__close">×</button>
	                <p className="notification__text word-break--all" dangerouslySetInnerHTML={this.createMarkup(message)}></p>
	            </div>
	        </div>
	    )

    }


}

export default Notification;
