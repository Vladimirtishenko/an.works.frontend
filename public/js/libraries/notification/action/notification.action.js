import * as types from '../constant/notification.const.js';

export function notificationSetter(error) {

	return (dispatch, getState) => {

		dispatch({
			type: types.SET_NOTIFICATION,
			notification: {...error}
		})
	}

}
