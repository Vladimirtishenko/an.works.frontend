import form from './form.jsx'
import component from './component.jsx'

export const ValidatorForm = form;
export const ValidatorComponent = component;
